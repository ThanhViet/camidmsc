package com.metfone.selfcare.ui.tabvideo.activity.createChannel;

import com.metfone.selfcare.model.tab_video.Channel;

public interface CreateChannelPresenter {
    void initData(Channel channel);

    void updateOrCreateChannel(String s, String s1, String absoluteFile, boolean showAvatar);

}

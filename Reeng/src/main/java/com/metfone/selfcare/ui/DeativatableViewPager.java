/**
 * Copyright (C) 2016 Samsung Electronics Co., Ltd. All rights reserved.
 * <p/>
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 * <p/>
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 * <p/>
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */

package com.metfone.selfcare.ui;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class DeativatableViewPager extends ViewPager {
    private boolean mIsPagingDisabled = false;

    public DeativatableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DeativatableViewPager(Context context) {
        super(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mIsPagingDisabled) {
            return super.onTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!mIsPagingDisabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void setPagingEnable(boolean isEnable) {
        mIsPagingDisabled = !isEnable;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (!mIsPagingDisabled ) {
            return super.dispatchKeyEvent(event);
        } else {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    break;

                default:
                    return super.dispatchKeyEvent(event);
            }
        }

        return false;
    }
}

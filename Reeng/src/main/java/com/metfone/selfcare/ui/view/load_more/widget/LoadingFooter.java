package com.metfone.selfcare.ui.view.load_more.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;

public class LoadingFooter extends RelativeLayout {

    protected State mState = State.Loading;
    protected Status mStatus = Status.NORMAL;

    private View loading;
    private View end;
    private View notInternet;

    public LoadingFooter(Context context, Status mStatus) {
        super(context);
        this.mStatus = mStatus;
        init(context);
    }

    public LoadingFooter(Context context) {
        super(context);
        init(context);
    }

    public LoadingFooter(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LoadingFooter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        inflate(context, mStatus == Status.NORMAL ? R.layout.sample_common_list_footer : R.layout.layout_footer_video, this);
        loading = findViewById(R.id.loading);
        end = findViewById(R.id.end);
        notInternet = findViewById(R.id.notInternet);
        setState(State.Normal);
        setOnClickListener(null);
    }

    public State getState() {
        return mState;
    }

    public void setState(State status) {
        if (mState == status) return;
        mState = status;
        switch (status) {
            case Loading:
                loading.setVisibility(VISIBLE);
                end.setVisibility(INVISIBLE);
                notInternet.setVisibility(INVISIBLE);
                break;
            default:
                loading.setVisibility(INVISIBLE);
                end.setVisibility(INVISIBLE);
                notInternet.setVisibility(INVISIBLE);
                break;
        }
    }

    public enum State {
        Normal, TheEnd, Loading, NetWorkError
    }

    public enum Status {
        NORMAL, TAB_VIDEO
    }
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/24
 *
 */

package com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload;

import androidx.fragment.app.Fragment;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.di.MvpPresenter;
import com.metfone.selfcare.model.tab_video.Channel;

import java.util.ArrayList;

public interface TopMoneyUploadContact {

    interface TopMoneyUploadView extends BaseView {

        void bindData(ArrayList<Channel> channels, boolean loadMore);

        void loadDataCompleted();
    }

    interface TopMoneyUploadPresenter extends MvpPresenter {
        void getTopMoneyUpload();

        void getRefreshTopMoneyUpload();

        void getMoreTopMoneyUpload();

        void openDetail(BaseSlidingFragmentActivity activity, Channel channel);

        boolean isErrorApi();
    }

    interface TopMoneyUploadProvider {

        Fragment provideFragment();
    }
}

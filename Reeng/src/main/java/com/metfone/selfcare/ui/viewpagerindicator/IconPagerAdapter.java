package com.metfone.selfcare.ui.viewpagerindicator;

public interface IconPagerAdapter {
    /**
     * Get icon representing the page at {@code index} in the adapter.
     */
//    int getIconResId(int index);

    // From PagerAdapter
    int getCount();

    IconIndicator getIconIndicator(int index);
}

package com.metfone.selfcare.ui.tabvideo.listener;

import com.metfone.selfcare.common.utils.listener.Listener;
import com.metfone.selfcare.model.tab_video.Channel;

/**
 * Created by HoangAnhTuan on 3/26/2018.
 */

public interface OnChannelChangedDataListener extends Listener {

    void onChannelCreate(Channel channel);

    void onChannelUpdate(Channel channel);

    void onChannelSubscribeChanged(Channel channel);
}

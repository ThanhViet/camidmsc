package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.PopupHelper;

public class DialogCallOption extends Dialog implements View.OnClickListener {
    private LinearLayout mViewCallFree, mViewCallOut, mViewCallVideo;
    protected PopupHelper.DialogCallOptionListener clickHandler;
    protected ThreadMessage entry;

    public DialogCallOption(@NonNull BaseSlidingFragmentActivity context,
                            @NonNull ThreadMessage threadMessage,
                            @NonNull PopupHelper.DialogCallOptionListener listener) {
        super(context, R.style.DialogFullscreen);
        this.clickHandler = listener;
        this.entry = threadMessage;
        setCancelable(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_fragment_call_option);
        mViewCallFree = (LinearLayout) findViewById(R.id.popup_call_option_free);
        mViewCallOut = (LinearLayout) findViewById(R.id.popup_call_option_out);
        mViewCallVideo = (LinearLayout) findViewById(R.id.popup_call_option_video);
        mViewCallVideo.setVisibility(View.GONE);
        initEvent();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.popup_call_option_free:
                clickHandler.onCallFree(entry);
                break;
            case R.id.popup_call_option_out:
                clickHandler.onCallOut(entry);
                break;
            case R.id.popup_call_option_video:
                clickHandler.onVideoCall(entry);
                break;
        }
        dismiss();
    }

    private void initEvent() {
        mViewCallFree.setOnClickListener(this);
        mViewCallOut.setOnClickListener(this);
        mViewCallVideo.setOnClickListener(this);
    }
}

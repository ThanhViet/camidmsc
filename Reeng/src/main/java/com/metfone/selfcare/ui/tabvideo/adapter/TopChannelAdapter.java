/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/23
 *
 */

package com.metfone.selfcare.ui.tabvideo.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.holder.TopChannelDetailHolder;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelListener;

public class TopChannelAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {
    private OnChannelListener onItemListener;

    public void setOnItemListener(OnChannelListener onItemListener) {
        this.onItemListener = onItemListener;
    }

    public TopChannelAdapter(Activity activity) {
        super(activity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TopChannelDetailHolder(layoutInflater, parent, onItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Object item = getItem(position);
        if (item instanceof Channel) {
            ((TopChannelDetailHolder) holder).bindData((Channel) item);
        }
    }

    public Object getItem(int position) {
        if (items.size() > position && position >= 0) return items.get(position);
        return null;
    }
}

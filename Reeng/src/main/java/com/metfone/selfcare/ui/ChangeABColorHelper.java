package com.metfone.selfcare.ui;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.view.View;

import com.metfone.selfcare.helper.home.TabHomeHelper.HomeTab;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 8/21/2017.
 */

public class ChangeABColorHelper {

    public static void changeTabColor(View abView, HomeTab toTab,
                                      String colorWapTo, HomeTab fromTab, String colorWapFrom) {
        if (toTab == fromTab && toTab != HomeTab.tab_wap) return;
        String currentColor = getColorTab(fromTab, colorWapFrom);
        String nextColor = getColorTab(toTab, colorWapTo);
        doAnimation(abView, currentColor, nextColor, 1);
    }

    private static String getColorTab(HomeTab tab, String colorWap) {
        switch (tab) {
            case tab_chat:
                return "#5E48CD";
            case tab_call:
                return "#FB7B42";
            case tab_video:
                return "#00ADEF";
            case tab_stranger:
                return "#ff64c8";
            case tab_hot:
                return "#EC3D5A";
            case tab_more:
                return "#fead2d";
            case tab_movie:
                return "#282828";
            case tab_music:
                return "#7ab014";
            case tab_news:
                return "#2b276d";
            case tab_security:
                return "#489b90";
            case tab_home:
                return "#5cd2ff";
            case tab_wap:
                return colorWap;
            case tab_tiins:
                return "#e53d7e";
            case tab_selfcare:
                return "#f17e25";
            default:
                return "#5E48CD";
        }
    }

    private static void doAnimation(final View abView, String fromColor, String toColor, float alpha) {
        if (abView == null || Utilities.isEmpty(fromColor) || Utilities.isEmpty(toColor)) return;
//        if(alpha == 0f || alpha == 1f) return;
        if (fromColor.equals(toColor)) return;
        final float[] from = new float[3],
                to = new float[3];

        Color.colorToHSV(Color.parseColor(fromColor), from);
        Color.colorToHSV(Color.parseColor(toColor), to);

//        final float[] hsv  = new float[3];
//        hsv[0] = from[0] + (to[0] - from[0]) * alpha;
//        hsv[1] = from[1] + (to[1] - from[1]) * alpha;
//        hsv[2] = from[2] + (to[2] - from[2]) * alpha;
//        abView.setBackgroundColor(Color.HSVToColor(hsv));

        ValueAnimator anim = ValueAnimator.ofObject(new ArgbEvaluator(), Color.parseColor(fromColor), Color.parseColor(toColor));
//        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);   // animate from 0 to 1
        anim.setDuration(2000);

        // for 300 ms

//        final float[] hsv = new float[3];                  // transition color
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Transition along each axis of HSV (hue, saturation, value)
               /* hsv[0] = from[0] + (to[0] - from[0]) * animation.getAnimatedFraction();
                hsv[1] = from[1] + (to[1] - from[1]) * animation.getAnimatedFraction();
                hsv[2] = from[2] + (to[2] - from[2]) * animation.getAnimatedFraction();

                abView.setBackgroundColor(Color.HSVToColor(hsv));*/
                abView.setBackgroundColor((int) animation.getAnimatedValue());
            }
        });
        anim.start();
    }
}

package com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.holder.ChannelHeaderHolder;
import com.metfone.selfcare.ui.tabvideo.holder.LoadMoreHolder;
import com.metfone.selfcare.ui.tabvideo.holder.VideoNormalHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class SubscribeChannelAdapter extends BaseAdapter {

    private static final int HEARD = 1;
    private static final int EMPTY = 2;

    public SubscribeChannelAdapter(BaseSlidingFragmentActivity activity) {
        super(activity);
    }

    private boolean isTabVideo;

    public void setTabVideo(boolean tabVideo) {
        isTabVideo = tabVideo;
    }

    @Override
    public int getItemViewType(int position) {
        ItemObject itemObject = itemObjects.get(position);
        if (itemObject.getInfo() instanceof ChannelHeaderHolder.ChannelObject)
            return HEARD;
        if (itemObject.getType() == Type.LOAD_MORE)
            return LOAD_MORE;
        if (itemObject.getType() == Type.EMPTY)
            return EMPTY;
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEARD:
                return new ChannelHeaderHolder(activity, layoutInflater, parent, (ChannelHeaderHolder.OnChannelHeaderListener) onItemListener);
            case LOAD_MORE:
                return new LoadMoreHolder(layoutInflater, parent);
            case EMPTY:
                return new EmptyHolder(layoutInflater, parent);
            default:
                return new VideoNormalHolder(layoutInflater, parent, (VideoNormalHolder.OnVideoNormalListener) onItemListener, activity);
        }
    }

    @Override
    protected Object getChangePayload(ItemObject oldItem, ItemObject newItem) {
        Bundle bundle = new Bundle();

        Object oldObject = oldItem.getInfo();
        Object newObject = newItem.getInfo();

        if (oldObject instanceof VideoNormalHolder.VideoObject && newObject instanceof VideoNormalHolder.VideoObject) {
            VideoNormalHolder.VideoObject oldVideoObject = (VideoNormalHolder.VideoObject) oldObject;
            VideoNormalHolder.VideoObject newVideoObject = (VideoNormalHolder.VideoObject) newObject;
            if (!oldVideoObject.getTextLike().equals(newVideoObject.getTextLike()))
                bundle.putBoolean(Constants.TabVideo.LIKE, true);
            if (!oldVideoObject.getTextComment().equals(newVideoObject.getTextComment()))
                bundle.putBoolean(Constants.TabVideo.COMMENT, true);
            if (!oldVideoObject.getTextShare().equals(newVideoObject.getTextShare()))
                bundle.putBoolean(Constants.TabVideo.SHARE, true);
            if (!oldVideoObject.getVideo().isPlaying() == newVideoObject.getVideo().isPlaying())
                bundle.putBoolean(Constants.TabVideo.PLAY, true);
        }
        if (bundle.isEmpty()) return null;
        return bundle;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) super.onBindViewHolder(holder, position, payloads);
        else {
            Bundle bundle = (Bundle) payloads.get(0);
            if (holder instanceof VideoNormalHolder) {
                VideoNormalHolder videoHolder = (VideoNormalHolder) holder;
                if (bundle.getBoolean(Constants.TabVideo.LIKE, false))
                    videoHolder.bindHear();
                if (bundle.getBoolean(Constants.TabVideo.SHARE, false))
                    videoHolder.bindShare();
                if (bundle.getBoolean(Constants.TabVideo.COMMENT, false))
                    videoHolder.bindComment();
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAdapter.ViewHolder holder, int position) {
        ItemObject itemObject = itemObjects.get(position);
        Clone itemClone = itemObject.getInfo();
        if (holder instanceof ChannelHeaderHolder && itemClone instanceof ChannelHeaderHolder.ChannelObject)
            ((ChannelHeaderHolder) holder).bindData((ChannelHeaderHolder.ChannelObject) itemClone, isTabVideo);
        else if (holder instanceof VideoNormalHolder && itemClone instanceof VideoNormalHolder.VideoObject)
            ((VideoNormalHolder) holder).bindData((VideoNormalHolder.VideoObject) itemClone);
        else if (holder instanceof EmptyHolder) {
            ((EmptyHolder) holder).bindData();
        }
    }

    @Override
    public void pauseRequests() {
        super.pauseRequests();
        VideoNormalHolder.pauseRequests();
    }

    @Override
    public void resumeRequests() {
        super.resumeRequests();
        VideoNormalHolder.resumeRequests();
    }

    public static class EmptyHolder extends BaseAdapter.ViewHolder {

        @BindView(R.id.empty_layout)
        LinearLayout llRoot;
        @BindView(R.id.tvEmptyTitle)
        TextView tvEmptyTitle;
        @BindView(R.id.tvEmptyDesc)
        TextView tvEmptyDes;
        @BindView(R.id.imgType)
        AppCompatImageView imgType;

        EmptyHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.include_loading_view_v5, parent, false));
            ButterKnife.bind(this, itemView);
        }

        public void bindData() {
            if (llRoot != null) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.topMargin = ViewUtils.dpToPx(60);
                llRoot.setLayoutParams(layoutParams);
            }
            if (tvEmptyTitle != null) {
                tvEmptyTitle.setText(ApplicationController.self().getString(R.string.no_followed_channel_video));
                tvEmptyTitle.setVisibility(View.VISIBLE);
            }
            if (tvEmptyDes != null) {
                tvEmptyDes.setText(ApplicationController.self().getString(R.string.no_followed_channel_video_des));
                tvEmptyDes.setVisibility(View.VISIBLE);
            }
            if (imgType != null) {
                imgType.setVisibility(View.VISIBLE);
                imgType.setImageResource(R.drawable.ic_empty_video);
            }
        }
    }

    interface OnItemSubscribeChannelListener extends OnItemListener,
            ChannelHeaderHolder.OnChannelHeaderListener,
            VideoNormalHolder.OnVideoNormalListener {
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/8/12
 *
 */

package com.metfone.selfcare.ui.tabvideo.listener;

public interface ChooseEpisodeListener {

    void clickEpisode(int position, boolean selectCurrent);
}

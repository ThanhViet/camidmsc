package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device;

import com.metfone.selfcare.di.BaseView;

import java.util.List;

public interface UploadVideoDeviceView extends BaseView {
    void updateDataCategories(List<String> categories);

    void showMessage(int i);

    void hideLoading();

    void showMessage(String s);

    void hideDialogLoading();

    void showDialogLoading();

    void finish();

    void showMessageCategory();

    void updateProgress(int percentage);

    void showDialogSuccess();
}

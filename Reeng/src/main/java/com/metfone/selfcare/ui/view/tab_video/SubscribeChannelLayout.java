package com.metfone.selfcare.ui.view.tab_video;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.core.content.ContextCompat;

/**
 * quản lý toàn bộ tác vụ đăng ký kênh
 */
public class SubscribeChannelLayout extends RelativeLayout {

    private Context mContext;
    private TextView textView;

    private Channel mChannel;
    private Channel myChannel;

    private SubscribeChannelListener subscribeChannelListener;

    public SubscribeChannelLayout(Context context) {
        this(context, null);
    }

    public SubscribeChannelLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SubscribeChannelLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        inflate(context, R.layout.layout_subscribe_channel, this);
        textView = findViewById(R.id.tv_subscriptions_channel);
        textView.setOnClickListener(view -> {
            if (mChannel == null || Utilities.isEmpty(mChannel.getId())
                    || subscribeChannelListener == null || positiveListener == null)
                return;
            if (mChannel.getTypeChannel() == Channel.TypeChanel.OPEN_APP.VALUE) {
                subscribeChannelListener.onOpenApp(mChannel, mChannel.isInstall());
//                handlerTypeOpenApp(true);
            } else {
                if (mChannel.isFollow()) {
                    if (!showConfirm()) {
                        positiveListener.onPositive(new Object());
                    }
                } else {
                    positiveListener.onPositive(new Object());
                }
            }

        });

        myChannel = SharedPrefs.getInstance().get(Constants.TabVideo.CACHE_MY_CHANNEL_INFO, Channel.class);
        if (myChannel == null) {
            myChannel = new Channel();
        }

        setVisibility(View.GONE);
    }

    public void setChannel(Channel channel) {
        if (mChannel != null && channel != null && Utilities.notEmpty(mChannel.getId()) && mChannel.getId().equals(channel.getId())
                && (myChannel == null || !mChannel.getId().equals(myChannel.getId()))) {
            setVisibility(View.VISIBLE);
        } else {
            setVisibility(View.GONE);
        }
        mChannel = channel;
        schedule();
    }

    public void setSubscribeChannelListener(SubscribeChannelListener subscribeChannelListener) {
        this.subscribeChannelListener = subscribeChannelListener;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        schedule();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clear();
    }

    private void schedule() {
        if (mChannel == null
                || Utilities.isEmpty(mChannel.getId())
                || Utilities.equals(mChannel.getId(), myChannel.getId())) {
            return;
        }
        if (mChannel.getTypeChannel() != Channel.TypeChanel.OPEN_APP.VALUE) {
            handlerTypeOther();
        } else {
            removeCallbacks(handelRunnable);
            postDelayed(handelRunnable, 300);
        }
    }

    private void clear() {
        removeCallbacks(handelRunnable);
    }

    private Runnable handelRunnable = new Runnable() {
        @Override
        public void run() {
            if (mChannel == null || Utilities.isEmpty(mChannel.getId())) return;
            asyncSetText(textView, mChannel, Executors.newFixedThreadPool(5));
        }
    };

    private void asyncSetText(TextView textView, final Channel channel, Executor bgExecutor) {
        final WeakReference<TextView> textViewRef = new WeakReference<>(textView);
        bgExecutor.execute(() -> {
            TextView textView12 = textViewRef.get();
            if (textView12 == null) return;
            final boolean isInstall = isInstalled(textView12.getContext(), channel.getPackageAndroid());
            textView12.post(() -> {
                TextView textView1 = textViewRef.get();
                if (textView1 == null) return;
                channel.setInstall(isInstall);
                handlerTypeOpenApp(isInstall);
            });
        });
    }

    public static boolean isInstalled(Context context, String uri) {
        try {
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException | RuntimeException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void handlerTypeOther() {
        if (mChannel == null || Utilities.isEmpty(mChannel.getId())) return;
        setVisibility(VISIBLE);
        if (mChannel.isFollow()) {
            textView.setText(R.string.unsubscribeChannel);
            textView.setBackgroundResource(R.drawable.xml_background_btn_unsubscribe);
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.v5_text));
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_v5_subscribe, 0, 0, 0);
            textView.setCompoundDrawablePadding(16);
        } else {
            textView.setText(R.string.subscribeChannel);
            textView.setBackgroundResource(R.drawable.xml_background_episode_press);
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.videoColorAccent));
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_v5_add, 0, 0, 0);
            textView.setCompoundDrawablePadding(16);
        }
    }

    private void handlerTypeOpenApp(boolean isInstall) {
        if (mChannel == null || Utilities.isEmpty(mChannel.getId())) return;
        setVisibility(VISIBLE);
        if (isInstall) {
            textView.setText(R.string.openApp);
            textView.setBackgroundResource(R.drawable.xml_background_btn_unsubscribe);
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.videoColorNormal));
        } else {
            textView.setText(R.string.install);
            textView.setBackgroundResource(R.drawable.xml_background_btn_subscribe);
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.videoColorAccent));
        }
    }

    private boolean showConfirm() {
        try {
            DialogConfirm dialogConfirm = new DialogConfirm(mContext, true);
            dialogConfirm.setLabel(mContext.getString(R.string.video_channel_un_follow, mChannel.getName()));
            dialogConfirm.setMessage(mContext.getString(R.string.unSubscriptionConfigMessage));
            dialogConfirm.setNegativeLabel(mContext.getString(R.string.video_channel_un_follow_cancel));
            dialogConfirm.setPositiveLabel(mContext.getString(R.string.video_channel_un_follow_ok));
            dialogConfirm.setPositiveListener(positiveListener);
            dialogConfirm.show();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private PositiveListener<Object> positiveListener = new PositiveListener<Object>() {
        @Override
        public void onPositive(Object object) {
            if (subscribeChannelListener == null) return;
            if (!ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                mChannel.setFollow(!mChannel.isFollow());
                mChannel.setNumFollow(mChannel.isFollow() ? mChannel.getNumfollow() + 1 : mChannel.getNumfollow() - 1);
                handlerTypeOther();
            }
            subscribeChannelListener.onSub(mChannel);
        }
    };

    public interface SubscribeChannelListener {
        void onOpenApp(Channel channel, boolean isInstall);

        void onSub(Channel channel);
    }
}

package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.view.load_more.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 8/14/2018.
 */

public class DialogChangePrefixNumber extends Dialog implements View.OnClickListener {

    private String msg, title;
    private BaseSlidingFragmentActivity activity;
    private NegativeListener<Object> negativeListener;
    private PositiveListener<Object> positiveListener;
    private DismissListener dismissListener;

    private Button mBtnNegative, mBtnPositive;
    private TextView tvTitle;
    private Object mEntry;

    private RecyclerView rvNumber;
    private PrefixChangeNumberAdapter adapter;
    private HeaderAndFooterRecyclerViewAdapter wrapperAdapter;
    private ArrayList<Object> listData;

    public DialogChangePrefixNumber(BaseSlidingFragmentActivity activity, boolean isCancelable, ArrayList<Object> list) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        this.listData = list;
        setCancelable(isCancelable);
    }

    public DialogChangePrefixNumber setMessage(String message) {
        this.msg = message;
        return this;
    }

    public DialogChangePrefixNumber setTitle(String title) {
        this.title = title;
        return this;
    }

    public DialogChangePrefixNumber setNegativeListener(NegativeListener listener) {
        this.negativeListener = listener;
        return this;
    }

    public DialogChangePrefixNumber setPositiveListener(PositiveListener<Object> listener) {
        this.positiveListener = listener;
        return this;
    }

    public DialogChangePrefixNumber setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    public DialogChangePrefixNumber setEntry(Object entry) {
        this.mEntry = entry;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_change_prefix_number);
        findComponentViews();
        drawDetail();
        setListener();
    }

    @Override
    public void dismiss() {
        Log.d("DialogConfirm", "dismiss");
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_confirm_button_negative:
                if (negativeListener != null) {
                    negativeListener.onNegative(mEntry);
                }
                break;
            case R.id.dialog_confirm_button_positive:
                if (positiveListener != null) {
                    positiveListener.onPositive(mEntry);
                }
                break;
        }
        dismiss();
    }

    private void findComponentViews() {
        mBtnNegative = (Button) findViewById(R.id.dialog_confirm_button_negative);
        mBtnPositive = (Button) findViewById(R.id.dialog_confirm_button_positive);
        tvTitle = findViewById(R.id.dialog_confirm_label);
        rvNumber = findViewById(R.id.rvNumber);
        /*rvNumber.setNestedScrollingEnabled(false);
        rvNumber.setHasFixedSize(true);*/
        LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams) rvNumber.getLayoutParams();
        if (listData.size() > 2) {
            ll.height = activity.getResources().getDimensionPixelOffset(R.dimen.width_button_send_gift);
            rvNumber.setLayoutParams(ll);
            rvNumber.requestLayout();
        } else if(listData.size()==1 && TextUtils.isEmpty(msg)){
            ll.height = activity.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_80);
            rvNumber.setLayoutParams(ll);
            rvNumber.requestLayout();
        }
        rvNumber.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
//        rvNumber.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
    }

    private void setListener() {
        mBtnPositive.setOnClickListener(this);
        mBtnNegative.setOnClickListener(this);
    }

    private void drawDetail() {
        adapter = new PrefixChangeNumberAdapter();
        wrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(adapter);
        rvNumber.setAdapter(wrapperAdapter);
        if(!TextUtils.isEmpty(msg)){
            TextView tvMsg = new TextView(activity);
            tvMsg.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tvMsg.setTextColor(ContextCompat.getColor(activity, R.color.text_ab_title));
            tvMsg.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimensionPixelSize(R.dimen.mocha_text_size_level_2));
            tvMsg.setText(msg);
            wrapperAdapter.addHeaderView(tvMsg);
        }
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }
        /*android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="content"
                android:textColor="#5c5e6e"
                android:textSize="@dimen/mocha_text_size_level_2"*/
    }

    private class PrefixChangeNumberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(activity).inflate(R.layout.holder_change_prefix_number, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((ViewHolder) holder).setIslastItem(position == (listData.size() - 1));
            ((ViewHolder) holder).setElement(listData.get(position));
        }

        @Override
        public int getItemCount() {
            if (listData == null || listData.isEmpty()) return 0;
            return listData.size();
        }
    }

    private class ViewHolder extends BaseViewHolder {

        private TextView tvName, tvOldNumb, tvNewNumb;
        private View divider;
        private boolean islastItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvOldNumb = itemView.findViewById(R.id.tvOldNumb);
            tvNewNumb = itemView.findViewById(R.id.tvNewNumb);
            divider = itemView.findViewById(R.id.viewDivider);
        }

        public void setIslastItem(boolean islastItem) {
            this.islastItem = islastItem;
        }

        @Override
        public void setElement(Object obj) {
            if (obj instanceof PhoneNumber) {
                PhoneNumber p = (PhoneNumber) obj;
                tvName.setVisibility(View.VISIBLE);
                tvOldNumb.setVisibility(View.VISIBLE);
                tvNewNumb.setVisibility(View.VISIBLE);
                tvName.setText(p.getName());
                tvOldNumb.setText(p.getJidNumber());
                tvNewNumb.setText(p.getNewNumber());
            } else if (obj instanceof NonContact) {
                NonContact nonContact = (NonContact) obj;
                tvName.setVisibility(View.GONE);
                tvOldNumb.setVisibility(View.VISIBLE);
                tvNewNumb.setVisibility(View.VISIBLE);
                tvOldNumb.setText(nonContact.getJidNumber());
                tvNewNumb.setText(nonContact.getNewJidNumber());
            }
            if (islastItem) {
                divider.setVisibility(View.GONE);
            } else {
                divider.setVisibility(View.VISIBLE);
            }
        }
    }


}

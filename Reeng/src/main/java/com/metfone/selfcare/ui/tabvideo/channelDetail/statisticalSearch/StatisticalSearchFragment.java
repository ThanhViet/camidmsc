package com.metfone.selfcare.ui.tabvideo.channelDetail.statisticalSearch;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.VideoRevenue;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.adapter.RevenueAdapter;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class StatisticalSearchFragment extends BaseFragment implements TextWatcher,
        SwipeRefreshLayout.OnRefreshListener,
        ApiCallbackV2<ArrayList<VideoRevenue>> {

    private static final String CHANNEL = "channel";
    private static final int LIMIT = 20;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.btnSearch)
    TextView btnSearch;
    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    @Nullable
    SwipeRefreshLayout refreshLayout;
    @Nullable
    Unbinder unbinder;

    public static StatisticalSearchFragment newInstance(Channel channel) {

        Bundle args = new Bundle();
        args.putSerializable(CHANNEL, channel);
        StatisticalSearchFragment fragment = new StatisticalSearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private @Nullable
    Channel mChannel;
    private Object header;

    private VideoApi mVideoApi;
    private RevenueAdapter mRevenueAdapter;
    private ArrayList<Object> feeds;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        header = new Object();
        Bundle bundle = getArguments();
        mChannel = (Channel) (bundle != null ? bundle.getSerializable(CHANNEL) : null);
        mVideoApi = application.getApplicationComponent().providerVideoApi();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistical_search, container, false);
        unbinder = ButterKnife.bind(this, view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        if (edtSearch != null) {
            edtSearch.removeTextChangedListener(this);
        }
        if (refreshLayout != null) {
            refreshLayout.setOnRefreshListener(null);
        }
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        initView();
    }

    @OnClick(R.id.iv_back)
    public void onIvBackClicked() {
        activity.onBackPressed();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (recyclerView == null || refreshLayout == null) return;
        refreshLayout.removeCallbacks(refreshRunnable);
        refreshLayout.postDelayed(refreshRunnable, 300);
    }

    @Override
    public void onSuccess(String lastIdStr, ArrayList<VideoRevenue> results) {
        if (recyclerView == null || mRevenueAdapter == null) return;

        if (feeds == null) feeds = new ArrayList<>();
        feeds.clear();

        if (Utilities.notEmpty(results)) {
            feeds.add(header);
            for (VideoRevenue video : results) {
                if (feeds.contains(video)) continue;
                feeds.add(video);
            }
        }
        mRevenueAdapter.bindData(feeds);
        recyclerView.stopScroll();

        if (Utilities.notEmpty(feeds))
            hideError();
        else
            onError("");
    }

    @Override
    public void onError(String s) {
        if (Utilities.notEmpty(feeds) || recyclerView == null) return;
        showError();
        if (!NetworkHelper.isConnectInternet(activity)) {
            showErrorNetwork();
            hideErrorDataEmpty();
        } else {
            showErrorDataEmpty();
            hideErrorNetwork();
        }
    }

    @Override
    public void onComplete() {
        if (refreshLayout == null) return;
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        if (refreshLayout != null) {
            refreshLayout.setRefreshing(false);
        }
    }

    private void initView() {
        hideError();

        mRevenueAdapter = new RevenueAdapter(activity);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mRevenueAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setPadding(Utilities.dpToPixels(4, getResources()), 0, Utilities.dpToPixels(4, getResources()), 0);

        if (refreshLayout != null) {
            refreshLayout.setOnRefreshListener(this);
            refreshLayout.removeCallbacks(refreshRunnable);
            refreshLayout.post(refreshRunnable);
        }

        edtSearch.addTextChangedListener(this);
        emptyText.setText(R.string.typing_no_result);
        edtSearch.requestFocus();
    }

    private Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            if (refreshLayout == null) return;
            refreshLayout.setRefreshing(true);
            loadData();
        }
    };

    private void loadData() {
        if (mChannel == null || edtSearch == null || edtSearch.getText() == null) return;
        mVideoApi.getVideoRevenuesByChannelIdV2(mChannel.getId(), Utilities.convertQuery(edtSearch.getText().toString()), 0, LIMIT, "", this);
    }

    private void showErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.VISIBLE);
    }

    private void hideErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.GONE);
    }

    private void showErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.VISIBLE);
        emptyRetryText2.setVisibility(View.VISIBLE);
    }

    private void hideErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.GONE);
        emptyRetryText2.setVisibility(View.GONE);
    }

    private void showError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.GONE);
    }
}

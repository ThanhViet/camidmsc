package com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.holder.ChannelNormalHolder;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.SubscribeChannelActivity;
import com.metfone.selfcare.ui.view.load_more.OnEndlessScrollListener;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChannelManagementFragment extends BaseFragment implements ChannelManagementContact.ChannelManagementView, ChannelManagementContact.ChannelManagementProvider {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_channels_title)
    TextView tvChannel;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    Unbinder unbinder;

    @Inject
    ChannelManagementContact.ChannelManagementPresenter presenter;

    private boolean isLoading = false;
    private boolean isLoadMore = false;

    private ChannelAdapter channelAdapter;
    private BaseAdapter.ItemObject loadMoreObject;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<BaseAdapter.ItemObject> itemObjects;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);

        loadMoreObject = new BaseAdapter.ItemObject();
        loadMoreObject.setId(String.valueOf(System.currentTimeMillis()));
        loadMoreObject.setInfo(new BaseAdapter.DefaultClone());
        loadMoreObject.setType(BaseAdapter.Type.LOAD_MORE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channel_management, container, false);
        unbinder = ButterKnife.bind(this, view);
        view.setOnClickListener(view1 -> {
        });
        initView();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public Fragment provideFragment() {
        isVisibleToUser = true;
        return this;
    }

    private void initView() {
        itemObjects = new ArrayList<>();
        channelAdapter = new ChannelAdapter(activity);
        channelAdapter.setOnItemListener(onItemChannelNormalListener);

        linearLayoutManager = new LinearLayoutManager(application);
        linearLayoutManager.setInitialPrefetchItemCount(5);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(channelAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(null);
        recyclerView.setLayoutAnimation(null);
        recyclerView.setItemViewCacheSize(5);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(mEndlessRecyclerOnScrollListener);

        swipeRefreshLayout.post(LoadingRunnable);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
    }

    private Runnable LoadingRunnable = new Runnable() {
        @Override
        public void run() {
            if (isLoading)
                return;
            isLoading = true;
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(true);
            if (presenter != null)
                presenter.getChannelUserFollow();
        }
    };

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (isLoading)
                return;
            isLoading = true;
            if (presenter != null)
                presenter.getRefreshChannelUserFollow();
        }
    };


    private OnEndlessScrollListener mEndlessRecyclerOnScrollListener = new OnEndlessScrollListener(3) {
        private int currentState;

        @Override
        public void onLoadNextPage(View view) {
            super.onLoadNextPage(view);
            if (isLoading)
                return;
            if (isLoadMore) {
                isLoading = true;
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setEnabled(false);
                if (presenter != null)
                    presenter.getMoreChannelUserFollow();
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (channelAdapter == null || linearLayoutManager == null)
                return;
            if (currentState == RecyclerView.SCROLL_STATE_IDLE && newState == RecyclerView.SCROLL_STATE_DRAGGING) {// dừng tất cả các tiền trình đang load ảnh
                channelAdapter.pauseRequests();
            } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {// khôi phuc các tiền trình load ảnh
                channelAdapter.resumeRequests();
                int first = linearLayoutManager.findFirstVisibleItemPosition();
                int last = linearLayoutManager.findLastVisibleItemPosition();
                last = Math.min(last, channelAdapter.getItemCount() - 1);
                for (int i = first; i <= last; i++) {
                    updateUiImage(i);
                }

            }
            currentState = newState;
        }

        private void updateUiImage(int position) {
            if (recyclerView != null) {
                RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);
                if (viewHolder instanceof ChannelNormalHolder) {
                    ((ChannelNormalHolder) viewHolder).bindImageChannel();
                }
            }
        }
    };

    private ChannelAdapter.OnItemChannelListener onItemChannelNormalListener = new ChannelAdapter.OnItemChannelListener() {

        @Override
        public void onOpenApp(Channel channel, boolean isInstall) {
        }

        @Override
        public void onSub(Channel channel) {
            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                activity.showDialogLogin();
            else
                presenter.subscribe(activity, channel);
        }

        @Override
        public void onClick(Channel channel) {
            if (activity instanceof SubscribeChannelActivity)
                ((SubscribeChannelActivity) activity).notifyOpenChannel(channel);
            presenter.openDetail(activity, channel);
        }
    };

    @Override
    public void bindData(ArrayList<Channel> channels, boolean loadMore) {
        isLoadMore = loadMore;
        isLoading = false;

        if (itemObjects == null)
            itemObjects = new ArrayList<>();
        else
            itemObjects.clear();
        int followedCount = 0;
        if (Utilities.notEmpty(channels))
            for (int i = 0; i < channels.size(); i++) {
                Channel channel = channels.get(i);
                if (channel.isFollow()) {
                    followedCount++;
                }
                itemObjects.add(ChannelNormalHolder.ChannelObject.provideItemObject(channel, i, activity));
            }

        itemObjects.remove(loadMoreObject);
        if (loadMore)
            itemObjects.add(loadMoreObject);

        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setEnabled(true);
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
        if (channelAdapter != null)
            channelAdapter.updateData(itemObjects);
        if (tvChannel != null && itemObjects != null && !itemObjects.isEmpty()) {
            tvChannel.setText(getString(R.string.channel_follow_count, followedCount));
            tvChannel.setVisibility(View.VISIBLE);
        }

        if (isLoadMore && itemObjects != null && itemObjects.size() < 20 && mEndlessRecyclerOnScrollListener != null && recyclerView != null)
            mEndlessRecyclerOnScrollListener.onLoadNextPage(recyclerView);
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        activity.onBackPressed();
    }

}

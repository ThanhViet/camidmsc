package com.metfone.selfcare.ui.guestbook;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.util.Log;

import java.io.File;

/**
 * Created by toanvk2 on 4/14/2017.
 */
public class CropActivity extends BaseSlidingFragmentActivity implements
        CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnCropImageCompleteListener,
        View.OnClickListener {
    private static final String TAG = CropActivity.class.getSimpleName();
    public static final String FILE_PATH = "file_path";
    public static final String FILE_OUTPUT_PATH = "file_output_path";
    public static final String FILE_OUTPUT_WIDTH = "file_width";
    public static final String FILE_OUTPUT_HEIGHT = "file_height";
    //private CropDemoPreset mDemoPreset;
    private ApplicationController mApplication;
    private Resources mRes;
    private CropImageView mCropView;
    private String filePath;
    private String outPutPath;
    private boolean isCropCircle = false;
    private ImageView mAbBack, mAbCircle, mAbRec;
    private TextView mAbCrop;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_book_crop);
        mApplication = (ApplicationController) getApplicationContext();
        mRes = mApplication.getResources();
        findComponentViews();
        initActionBar();
        getData(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(FILE_PATH, filePath);
        outState.putString(FILE_OUTPUT_PATH, outPutPath);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        if (mCropView != null) {
            mCropView.setOnSetImageUriCompleteListener(null);
            mCropView.setOnCropImageCompleteListener(null);
        }
        super.onDestroy();
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        Log.d(TAG, "onSetImageUriComplete: " + error);
        if (error == null) {
            setEnableOrDisableActionBar(true);
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                onBackPressed();
                break;
            case R.id.ab_crop_action_text:
                mCropView.getCroppedImageAsync();
                break;
            case R.id.ab_crop_circle:
                setOptionCircle(true);
                break;
            case R.id.ab_crop_rec:
                setOptionCircle(false);
                break;
        }
    }

    private void initActionBar() {
        LayoutInflater mLayoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View abView = findViewById(R.id.tool_bar);
        setToolBar(abView);
        setCustomViewToolBar(mLayoutInflater.inflate(R.layout.ab_guest_book_crop, null));
        mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbCircle = (ImageView) abView.findViewById(R.id.ab_crop_circle);
        mAbRec = (ImageView) abView.findViewById(R.id.ab_crop_rec);
        mAbCrop = (TextView) abView.findViewById(R.id.ab_crop_action_text);
        mAbBack.setOnClickListener(this);
        mAbCrop.setOnClickListener(this);
        mAbCircle.setOnClickListener(this);
        mAbRec.setOnClickListener(this);
        setEnableOrDisableActionBar(false);
    }

    private void setEnableOrDisableActionBar(boolean enable) {
        mAbCrop.setEnabled(enable);
        mAbRec.setEnabled(enable);
        mAbCircle.setEnabled(enable);
        if (enable) {
            if (isCropCircle) {
                mAbRec.setColorFilter(ContextCompat.getColor(CropActivity.this, R.color.undo_disable));
                mAbCircle.setColorFilter(ContextCompat.getColor(CropActivity.this, R.color.bg_mocha));
            } else {
                mAbRec.setColorFilter(ContextCompat.getColor(CropActivity.this, R.color.bg_mocha));
                mAbCircle.setColorFilter(ContextCompat.getColor(CropActivity.this, R.color.undo_disable));
            }
        } else {
            mAbRec.setColorFilter(ContextCompat.getColor(CropActivity.this, R.color.undo_disable));
            mAbCircle.setColorFilter(ContextCompat.getColor(CropActivity.this, R.color.undo_disable));
        }
    }

    private void setOptionCircle(boolean isCircle) {
        isCropCircle = isCircle;
        if (isCircle) {
            mCropView.setCropShape(CropImageView.CropShape.OVAL);
            mCropView.setAspectRatio(1, 1);
            mCropView.setFixedAspectRatio(true);
        } else {
            mCropView.setCropShape(CropImageView.CropShape.RECTANGLE);
            mCropView.setAspectRatio(1, 1);
            mCropView.setFixedAspectRatio(false);
        }
        setEnableOrDisableActionBar(true);
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            filePath = savedInstanceState.getString(FILE_PATH);
            outPutPath = savedInstanceState.getString(FILE_OUTPUT_PATH);
        } else if (getIntent() != null) {
            filePath = getIntent().getStringExtra(FILE_PATH);
            outPutPath = getIntent().getStringExtra(FILE_OUTPUT_PATH);
        }
        Uri uri = FileHelper.fromFile(mApplication, new File(filePath));
        mCropView.setImageUriAsync(uri);
    }

    private void findComponentViews() {
        mCropView = (CropImageView) findViewById(R.id.guest_book_crop_view);
        mCropView.setOnSetImageUriCompleteListener(this);
        mCropView.setOnCropImageCompleteListener(this);
        int minSize = mRes.getDimensionPixelSize(R.dimen.margin_more_content_30);
        mCropView.setMinCropResultSize(minSize, minSize);
        mCropView.setMaxZoom(3);
        //mCropView.setMinimumWidth(minSize);
    }

    private void setImageUri(Uri imageUri) {
        mCropView.setImageUriAsync(imageUri);
    }

    private void handleCropResult(CropImageView.CropResult result) {

        if (result.getError() == null) {
            /*int width = result.getBitmap().getWidth();
            int height = result.getBitmap().getHeight();
            //Uri outUri = FileHelper.fromFile(mApplication, new File(outPutPath));
            Bitmap bitmap = mCropView.getCropShape() == CropImageView.CropShape.OVAL
                    ? CropImage.toOvalBitmap(result.getBitmap()) : result.getBitmap();
            ImageHelper.getInstance(mApplication).saveBitmapToPath(bitmap, outPutPath, Bitmap.CompressFormat.PNG);
            if (bitmap != null) {
                bitmap.recycle();
            }
            returnIntent.putExtra(FILE_OUTPUT_PATH, outPutPath);
            returnIntent.putExtra(FILE_OUTPUT_WIDTH, width);
            returnIntent.putExtra(FILE_OUTPUT_HEIGHT, height);*/
            SaveImageAsyncTask asyncTask = new SaveImageAsyncTask(result, mCropView.getCropShape());
            asyncTask.execute();
        } else {
            Log.e(TAG, "Failed to crop image", result.getError());
            Intent returnIntent = new Intent();
            setResult(RESULT_CANCELED, returnIntent);
            onBackPressed();
        }
    }

    private class SaveImageAsyncTask extends AsyncTask<Void, Void, Void> {
        private CropImageView.CropResult result;
        private CropImageView.CropShape cropShape;
        private int width;
        private int height;

        public SaveImageAsyncTask(CropImageView.CropResult result, CropImageView.CropShape cropShape) {
            this.result = result;
            this.cropShape = cropShape;
        }

        @Override
        protected void onPreExecute() {
            showLoadingDialog(null, R.string.waiting);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            width = result.getBitmap().getWidth();
            height = result.getBitmap().getHeight();
            //Uri outUri = FileHelper.fromFile(mApplication, new File(outPutPath));
            Bitmap bitmap = cropShape == CropImageView.CropShape.OVAL
                    ? CropImage.toOvalBitmap(result.getBitmap()) : result.getBitmap();
            ImageHelper.getInstance(mApplication).saveBitmapToPath(bitmap, outPutPath, Bitmap.CompressFormat.PNG);
            if (bitmap != null) {
                bitmap.recycle();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideLoadingDialog();
            Intent returnIntent = new Intent();
            returnIntent.putExtra(FILE_OUTPUT_PATH, outPutPath);
            returnIntent.putExtra(FILE_OUTPUT_WIDTH, width);
            returnIntent.putExtra(FILE_OUTPUT_HEIGHT, height);
            setResult(RESULT_OK, returnIntent);
            onBackPressed();
        }
    }
}
package com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail;

import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.common.utils.image.ImageLoader;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.di.BasePresenter;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter.ItemObject;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.model.VideoObject;
import com.metfone.selfcare.util.FilterContentSeenUtil;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;

public class VideoDetailPresenterImpl extends BasePresenter<VideoDetailView> implements VideoDetailPresenter {

    private static final String TAG = "VideoDetailPresenterImp";

    private int offset = 0;
    private String lastId = "";
    private String titleSearch = "";
    private String queryRecommendation = "";
    private String videoType = "";
    private String videoUrl = "";
    private String firstId = "";
    private ItemObject firstItemObject;
    private ArrayList<ItemObject> items;
    private boolean isLoadMore = false;
    private boolean isCallApiError = false;
    private String categoryId = "";
    private String currentIdVideoPlaying;
    private Disposable disposablePlayVideo;
    private FilterContentSeenUtil filterContentSeenUtil;

    /**
     * Callback lấy dữ liệu thông thường
     */
    private OnVideoCallback onGetVideoCallback = new OnVideoCallback() {

        @Override
        public void onGetVideosSuccess(ArrayList<Video> list) {
            updateData(list);
        }

        @Override
        public void onGetVideosError(String s) {
            isCallApiError = true;
        }

        @Override
        public void onGetVideosComplete() {

        }
    };

    public VideoDetailPresenterImpl(VideoDetailView view, ApplicationController application) {
        super(view, application);
        items = new ArrayList<>();
    }

    @Override
    public void setVideo(Video video) {
        firstId = video.getId();
        titleSearch = video.getTitle();
        queryRecommendation = video.getQueryRecommendation();
        videoType = video.getSourceType();
        videoUrl = video.getLink();
        categoryId = video.getCategoryId();
        firstItemObject = new ItemObject();
        firstItemObject.setId(video.getId());
        firstItemObject.setInfo(provideVideoObject(video, -1));
        items.add(firstItemObject);
        view.updateData(items, isLoadMore);
    }

    @Override
    public void getVideoData(Video video) {
        videoApi.getMovieInfo(video, new ApiCallbackV2<Video>() {
            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onSuccess(String lastId, Video video) {
                firstItemObject.setInfo(provideVideoObject(video, -1));
                if (Utilities.notEmpty(items)) {
                    if (items.get(0).getId().equals(video.getId())) {
                        items.set(0, firstItemObject);
                    }
                }
                view.updateData(items, isLoadMore);
            }
        });
    }

    @Override
    public void getData(int limit) {
        videoApi.getVideosRelationship(compositeDisposable, titleSearch, videoType, videoUrl, categoryId
                , offset, limit, lastId, queryRecommendation, onGetVideoCallback);
    }

//    @Override
//    public void seenVideo(Video video) {
//        videoApi.callApiLogView(video);
//    }

    @Override
    public void getDataLoadMore(int limit) {
        offset = offset + limit;
        getData(limit);
    }

    @Override
    public void likeVideo(Video video) {
        listenerUtils.notifyVideoLikeChangedData(video);
        videoApi.likeOrUnlikeVideo(video);
    }

    @Override
    public void shareVideo(BaseSlidingFragmentActivity activity, Video video) {
        if (video.getIsPrivate() == 1) {
            activity.showToast(R.string.video_private_toast);
            return;
        }
        utils.openShareMenu(activity, video);
    }

    @Override
    public void saveVideo(Video video) {
        listenerUtils.notifyVideoSaveChangedData(video);
        videoApi.addOrRemoveSaveVideo(video);
    }

    @Override
    public void openComment(BaseSlidingFragmentActivity activity, Video video) {
        utils.openCommentVideo(activity, video);
    }

    @Override
    public void subscription(Channel channel) {
        listenerUtils.notifyChannelSubscriptionsData(channel);
        channelApi.callApiSubOrUnsubChannel(channel.getId(), channel.isFollow());
    }

    @Override
    public void openChannel(BaseSlidingFragmentActivity activity, Channel channel) {
        utils.openChannelInfo(activity, channel);
    }

//    @Override
//    public void callApiLog(Video currentVideo) {
//        videoApi.callApiLog(currentVideo);
//    }

    @Override
    protected void videoChangeData(Video video, Type type) {
        super.videoChangeData(video, type);
        boolean update = false;
        for (int i = 0; i < items.size(); i++) {
            ItemObject item = items.get(i);
            String itemId = item.getId();

            if (itemId.equals(video.getId())) {//Kiểm tra item có cùng id với video không
                Object info = item.getInfo();

                if (info instanceof VideoObject) {//Kiểm tra info có là kiểu VideoObject

                    if (!update) update = true;//Chuyển trạng thái update sang true ( cần cập nhật )

                    VideoObject videoObject = (VideoObject) info;
                    videoObject.setUpdate(true);

                    if (type == Type.LIKE) {//Cập nhập dữ liệu like

                        videoObject.getVideo().setLike(video.isLike());
                        videoObject.getVideo().setTotalLike(video.getTotalLike());
                        videoObject.setTextLike(Utilities.shortenLongNumber(video.getTotalLike()));
                    } else if (type == Type.SHARE) {// Cập nhật dữ liệu comment

                        videoObject.getVideo().setShare(video.isShare());
                        videoObject.getVideo().setTotalShare(video.getTotalShare());
                        videoObject.setTextShare(Utilities.shortenLongNumber(video.getTotalShare()));
                    } else if (type == Type.COMMENT) {// Cập nhật dữ liệu share

                        videoObject.getVideo().setTotalComment(video.getTotalComment());
                        videoObject.setTextComment(Utilities.shortenLongNumber(video.getTotalComment()));
                    } else if (type == Type.SAVE) {// Cập nhật dữ liệu SAVE

                        videoObject.getVideo().setSave(video.isSave());
                    }
                }
            } else
                continue;
            items.set(i, item);//cập nhật lại dữ liệu của item đó
        }
        if (update) view.updateData(items, isLoadMore);//Cập nhật dữ liệu
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        super.onChannelSubscribeChanged(channel);
        boolean update = false;
        for (int i = 0; i < items.size(); i++) {
            ItemObject item = items.get(i);
            Object info = item.getInfo();

            if (info instanceof VideoObject) {//Kiểm tra info có là kiểu VideoObject

                VideoObject videoObject = (VideoObject) info;
                Video video = videoObject.getVideo();
                Channel itemChannel = video.getChannel();

                if (!TextUtils.isEmpty(itemChannel.getId()) && itemChannel.getId().equals(channel.getId())) {

                    if (!update) update = true;//Chuyển trạng thái update sang true ( cần cập nhật )

                    itemChannel.setNumFollow(channel.getNumfollow());
                    itemChannel.setFollow(channel.isFollow());
                    videoObject.setTextSubscription(String.format(application.getString(R.string.people_subscription), Utilities.shortenLongNumber(itemChannel.getNumfollow())));
                    videoObject.setUpdate(true);
                } else
                    continue;
            } else
                continue;
            items.set(i, item);//cập nhật lại dữ liệu của item đó
        }
        if (update) view.updateData(items, isLoadMore);//Cập nhật dữ liệu
    }

    @Override
    public void onInternetChanged() {
        super.onInternetChanged();
        if (isCallApiError && items != null && items.size() <= 1 && NetworkHelper.isConnectInternet(application)) {
            lastId = "";
            offset = 0;
            getData(2);
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        onGetVideoCallback = null;
    }

    /**
     * cập nhật dữ liệu vào giao diện
     *
     * @param list danh sách dữ liệu mới nhất
     */
    private void updateData(ArrayList<Video> list) {
        if (firstItemObject != null && offset == 0) {
            items.clear();
            items.add(firstItemObject);
        }
        list = getFilterContentSeenUtil().filterList(list);
        for (int i = 0; i < list.size(); i++) {
            Video video = list.get(i);
            if (video.getIsPrivate() == 1 && application.getReengAccountBusiness() != null && !application.getReengAccountBusiness().isCBNV()) {
                continue;
            }
            if (!firstId.equals(video.getId()))
                items.add(provideItem(video, i));
        }
        if (!list.isEmpty()) {
            lastId = list.get(list.size() - 1).getLastId();
        }
        isLoadMore = !list.isEmpty();
        view.updateData(items, isLoadMore);
    }

    /**
     * chuyển đổi video sang ItemObject
     *
     * @param video    thông tin video
     * @param position vị trí của item
     * @return ItemObject
     */
    private ItemObject provideItem(Video video, int position) {
        ItemObject item = new ItemObject();
        item.setId(video.getId());
        item.setInfo(provideVideoObject(video, position));
        return item;
    }

    /**
     * chuyển đổi video sang videoObject
     *
     * @param video    thông tin video
     * @param position vị trí của item
     * @return VideoObject
     */
    private VideoObject provideVideoObject(Video video, int position) {
        if (video.getChannel() == null)
            video.setChannel(new Channel());

        if (Double.isNaN(video.getAspectRatio()))
            video.setAspectRatio(((double) 16) / 9);

        int thumbnail = getThumbnail(position);
        ImageLoader videoImageLoader = ImageManager.showImageWithThumb(video.getImagePath(), video.getImage_path_small(), thumbnail);

        video.setSave(videoApi.isSave(video));
        VideoObject videoObject = new VideoObject();
        videoObject.setVideo(video);
        videoObject.setTextShare(Utilities.shortenLongNumber(video.getTotalShare()));
        videoObject.setTextLike(Utilities.shortenLongNumber(video.getTotalLike()));
        videoObject.setTextComment(Utilities.shortenLongNumber(video.getTotalComment()));
        long views = video.getTotalView();
        videoObject.setTextView(String.format((views <= 1) ? application.getString(R.string.view)
                : application.getString(R.string.video_views), Utilities.getTotalView(views)));
        videoObject.setTextSubscription(String.format(application.getString(R.string.people_subscription), Utilities.shortenLongNumber(video.getChannel().getNumfollow())));
        videoObject.setDesCollapse(provideDescription(video.getDescription(), application.getString(R.string.readMore)));
        videoObject.setThumbnail(thumbnail);
        videoObject.setVideoLoader(/*imageUtils.load(video.getImagePath(), thumbnail, widthScreen, (int) (widthScreen / video.getAspectRatio()))*/videoImageLoader);
        videoObject.setChannelLoader(imageUtils.loadCircle(video.getChannel().getUrlImage(), thumbnail, widthScreen / 4));
        return videoObject;
    }

    /**
     * chuyển đối description sang description thu gọn
     *
     * @param description miêu tả
     * @param readMore    đuôi
     * @return description thu gọn
     */
    private SpannableStringBuilder provideDescription(String description, String readMore) {
        if (TextUtils.isEmpty(description)) {
            return new SpannableStringBuilder();
        } else {
            int trimEndIndex = 100;
            SpannableStringBuilder spannableStringBuilder;
            if (trimEndIndex < description.length()) {
                spannableStringBuilder = new SpannableStringBuilder(description, 0, trimEndIndex).append("...  ").append(readMore);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), spannableStringBuilder.length() - readMore.length(), spannableStringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                spannableStringBuilder = new SpannableStringBuilder().append(description);
            }
            return spannableStringBuilder;
        }
    }

    @Override
    public void setupData(ArrayList<Video> list) {

        if (items == null) items = new ArrayList<>();
        else items.clear();
        isLoadMore = false;
        if (Utilities.notEmpty(list)) {
            Video firstElement = list.remove(0);
            list = getFilterContentSeenUtil().filterList(list);
            list.add(0, firstElement);
            for (int i = 0; i < list.size(); i++) {
                Video video = list.get(i);
                if (video == null || video.getIsPrivate() == 1 && application.getReengAccountBusiness() != null && !application.getReengAccountBusiness().isCBNV()) {
                    continue;
                }
                items.add(provideItem(video, i));
            }
            lastId = list.get(list.size() - 1).getLastId();
        }
        view.updateData(items, isLoadMore);
    }

    @Override
    public void addVideoToListSeen(String id) {
        getFilterContentSeenUtil().addContentSeen(id);
    }

    public FilterContentSeenUtil getFilterContentSeenUtil() {
        if (filterContentSeenUtil == null) {
            filterContentSeenUtil = new FilterContentSeenUtil(Constants.CacheSeen.CACHE_IDS_VIDEO);
        }
        return filterContentSeenUtil;
    }
}

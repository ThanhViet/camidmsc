package com.metfone.selfcare.ui.ListviewExpandable;

/**
 * Created by thanhnt72 on 6/21/2016.
 */
import androidx.annotation.NonNull;

public interface ListViewWrapperSetter {

    void setListViewWrapper(@NonNull final ListViewWrapper listViewWrapper);
}

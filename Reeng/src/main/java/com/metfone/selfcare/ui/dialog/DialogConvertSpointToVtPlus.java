package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 2/19/2020.
 */

public class DialogConvertSpointToVtPlus extends Dialog implements View.OnClickListener, TextWatcher {
    protected Button mBtnNegative, mBtnPositive;
    protected EditText mEdtInput;
    private BaseSlidingFragmentActivity activity;
    private String label;
    private String msg;
    private String negativeLabel;
    private String positiveLabel;
    private NegativeListener<String> negativeListener;
    private PositiveListener<String> positiveListener;
    private DismissListener dismissListener;
    private boolean isSelectAll = false;
    private TextView mTvwTitle, mTvwMessageTop;
    private CheckBox cbUseAllSpoint;
    private boolean autoDismiss = true;
    private int currentSelection;
    private long currentPoint;


    public DialogConvertSpointToVtPlus(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity/*, R.style.DialogFullscreen*/);
        this.activity = activity;
        setCancelable(isCancelable);
    }

    public DialogConvertSpointToVtPlus setLabel(String label) {
        this.label = label;
        return this;
    }

    public DialogConvertSpointToVtPlus setMessage(String message) {
        this.msg = message;
        return this;
    }


    public DialogConvertSpointToVtPlus setNegativeLabel(String label) {
        this.negativeLabel = label;
        return this;
    }

    public DialogConvertSpointToVtPlus setPositiveLabel(String label) {
        this.positiveLabel = label;
        return this;
    }
    public DialogConvertSpointToVtPlus setPoint(long point) {
        this.currentPoint = point;
        return this;
    }

    public DialogConvertSpointToVtPlus setNegativeListener(NegativeListener listener) {
        this.negativeListener = listener;
        return this;
    }

    public DialogConvertSpointToVtPlus setPositiveListener(PositiveListener<String> listener) {
        this.positiveListener = listener;
        return this;
    }

    public DialogConvertSpointToVtPlus setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    public DialogConvertSpointToVtPlus setSelectAll(boolean selectAll) {
        isSelectAll = selectAll;
        return this;
    }

    public DialogConvertSpointToVtPlus setAutoDismiss(boolean autoDismiss) {
        this.autoDismiss = autoDismiss;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_convert_spoint_to_vtplus);
        findComponentViews();
        drawDetail();
        setListener();
    }

    @Override
    public void dismiss() {
        Log.d("DialogConvertSpointToVtPlus", "dismiss");
        InputMethodUtils.hideSoftKeyboard(mEdtInput, activity);
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_button_negative:

                if (negativeListener != null) {
                    negativeListener.onNegative(null);
                }
                break;
            case R.id.dialog_button_positive:
                String input = mEdtInput.getText().toString().trim();
                int inputInt = Integer.valueOf(input);
                if (inputInt == 0) {
                    activity.showToast(R.string.accumulate_must_greater_zero);
                    return;
                }
                long currentSpoint = Integer.valueOf(AccumulatePointHelper.getInstance(ApplicationController.self()).getTotalPoint());
                if(currentSpoint <= 0){
                    currentSpoint = currentPoint;
                }
                if (inputInt > currentSpoint) {
                    activity.showToast(R.string.accumulate_not_enought_spoint);
                    return;
                }
                if (positiveListener != null) {
                    positiveListener.onPositive(input);
                }
                break;
        }
        if (autoDismiss)
            dismiss();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        enablePositiveButton(s.toString().trim());
        cbUseAllSpoint.setChecked(false);

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private void findComponentViews() {
        mTvwTitle = (TextView) findViewById(R.id.dialog_title);
        mEdtInput = (EditText) findViewById(R.id.dialog_input);
        cbUseAllSpoint = findViewById(R.id.cb_convert_all);
        mTvwMessageTop = (TextView) findViewById(R.id.dialog_message_top);
        mBtnNegative = (Button) findViewById(R.id.dialog_button_negative);
        mBtnPositive = (Button) findViewById(R.id.dialog_button_positive);
    }

    private void setListener() {
        mBtnPositive.setOnClickListener(this);
        mBtnNegative.setOnClickListener(this);
        mEdtInput.addTextChangedListener(this);

        cbUseAllSpoint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mEdtInput.removeTextChangedListener(DialogConvertSpointToVtPlus.this);
                    String point = AccumulatePointHelper.getInstance(ApplicationController.self()).getTotalPoint();
                    mEdtInput.setText(point);
                    mEdtInput.setSelection(point.length());
                    enablePositiveButton(point);
                    mEdtInput.addTextChangedListener(DialogConvertSpointToVtPlus.this);
                }
            }
        });
    }

    private void drawDetail() {
        if (TextUtils.isEmpty(label)) {
            mTvwTitle.setVisibility(View.GONE);
        } else {
            mTvwTitle.setVisibility(View.VISIBLE);
            mTvwTitle.setText(label);
        }
        if (TextUtils.isEmpty(msg)) {
            mTvwMessageTop.setVisibility(View.GONE);
        } else {
            mTvwMessageTop.setText(msg);
        }
        if (TextUtils.isEmpty(negativeLabel)) {
            mBtnNegative.setVisibility(View.GONE);
        } else {
            mBtnNegative.setVisibility(View.VISIBLE);
            mBtnNegative.setText(negativeLabel);
        }
        if (TextUtils.isEmpty(positiveLabel)) {
            mBtnPositive.setVisibility(View.GONE);
        } else {
            mBtnPositive.setVisibility(View.VISIBLE);
            mBtnPositive.setText(positiveLabel);
        }
        mEdtInput.requestFocus();
        InputMethodUtils.showSoftKeyboard(activity, mEdtInput);
        this.enablePositiveButton("");
    }

    private void enablePositiveButton(String currentContent) {
        if (TextUtils.isEmpty(currentContent)) {
            mBtnPositive.setEnabled(false);
            mBtnPositive.setTextColor(ContextCompat.getColor(activity, R.color.text_disable));
        } else {
            mBtnPositive.setEnabled(true);
            mBtnPositive.setTextColor(ContextCompat.getColor(activity, R.color.bg_mocha));
        }
    }
}

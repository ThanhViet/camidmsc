package com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ashokvarma.bottomnavigation.utils.Utils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class VideoLibraryAdapter extends RecyclerView.Adapter<VideoLibraryAdapter.VideoHolder> {

    private ArrayList<Video> videos;
    private Activity activity;

    private VideoLibraryAdapter.OnItemVideoClickListener listener;

    public VideoLibraryAdapter(Activity activity) {
        this.videos = new ArrayList<>();
        this.activity = activity;
    }

    public void setListener(VideoLibraryAdapter.OnItemVideoClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public VideoLibraryAdapter.VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = activity.getLayoutInflater().inflate(R.layout.item_video_library, parent, false);
        return new VideoLibraryAdapter.VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoLibraryAdapter.VideoHolder holder, int position) {
        holder.updateData(position);
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public void setData(ArrayList<Video> list) {
        videos.clear();
        videos.addAll(list);
        notifyDataSetChanged();
    }

    class VideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        @BindView(R.id.ivImage)
        ImageView ivImage;
        @BindView(R.id.tvChannel)
        TextView tvChannel;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.iv_more)
        ImageView ivMore;
        @BindView(R.id.tv_datetime)
        TextView tvDatetime;
        @BindView(R.id.tvNumberSeen)
        TextView tvNumberSeen;
        @BindView(R.id.tv_duration)
        @Nullable
        TextView tvDuration;

        private Video video;

        VideoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            if (ivMore != null) ivMore.setOnClickListener(this);
        }

        @SuppressWarnings("deprecation")
        void updateData(int position) {
            video = videos.get(position);
            Channel channel = video.getChannel();
            tvTitle.setText(video.getTitle());
            long totalView = video.getTotalView();
            tvNumberSeen.setText(String.format((totalView <= 1) ? activity.getString(R.string.view)
                    : activity.getString(R.string.video_views), Utilities.getTotalView(totalView)));
            if (video.getPublishTime() > 0) {
                if (tvDatetime != null) {
                    tvDatetime.setVisibility(View.VISIBLE);
                    tvDatetime.setText(DateTimeUtils.calculateTime(activity.getResources(), video.getPublishTime()));
                }
            } else {
                if (tvDatetime != null) tvDatetime.setVisibility(View.GONE);
            }
            if (channel != null && !TextUtils.isEmpty(channel.getName())) {
                tvChannel.setVisibility(View.VISIBLE);
                tvChannel.setText(channel.getName());
            } else {
                tvChannel.setVisibility(View.GONE);
            }
            if (tvDuration != null) {
                if (!TextUtils.isEmpty(video.getDuration())) {
                    tvDuration.setText(video.getDuration());
                    tvDuration.setVisibility(View.VISIBLE);
                } else {
                    tvDuration.setVisibility(View.GONE);
                }
            }
            ImageManager.with()
                    .setTransformations(new RoundedCornersTransformation(Utils.dp2px(activity, 8), 0))
                    .setUrl(video.getImagePath())
                    .setWithCrossFade(true)
                    .build()
                    .into(ivImage);
//
//            Glide.with(activity)
//                    .asDrawable()
//                    .transition(withCrossFade())
//                    .apply(new RequestOptions()
//                            .placeholder(video.getThumbnail())
//                            .centerCrop()
//                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
//                    .load(video.getImagePath())
//                    .into(ivImage);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                if (view.getId() == R.id.iv_more) {
                    if (listener != null) listener.onMoreClicked(video);
                    return;
                }
                if (video.getChannel() != null) {
                    video.getChannel().setUrlImage("");
                    video.getChannel().setTypeChannel(Channel.TypeChanel.TYPE_DEFAULT.VALUE);
                }
                listener.onItemClicked(video);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (listener != null) listener.onItemLongClicked(video);
            return false;
        }
    }

    public interface OnItemVideoClickListener {

        void onItemClicked(Video video);

        void onItemLongClicked(Video video);

        void onMoreClicked(Video video);
    }

}
package com.metfone.selfcare.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.widget.RadioGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

public class DialogBackupSettingSelect extends DialogFragment {
    public static final String TAG = DialogBackupSettingSelect.class.getSimpleName();
    BackupSettingListener mListener = null;
    int type;

    public static final int TYPE_AUTO_BACKUP = 1;
    public static final int TYPE_NETWORK_BACKUP = 2;
    private int selectedId = R.id.radio_default;

    public static DialogBackupSettingSelect newInstance(BackupSettingListener listener, int type, int selectedId) {
        DialogBackupSettingSelect dialog =  new DialogBackupSettingSelect();
        dialog.mListener = listener;
        dialog.type = type;
        dialog.selectedId = selectedId;
        return dialog;
    }

    @Override
    public void dismissAllowingStateLoss() {
        try {
            super.dismissAllowingStateLoss();
        } catch (Exception e) {
            Log.e(TAG, "dismissAllowingStateLoss ", e);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int layoutId = R.layout.dialog_backup_auto_setting;
        if (type == TYPE_NETWORK_BACKUP) {
            layoutId = R.layout.dialog_backup_network_setting;
        }
        View contentView = getActivity().getLayoutInflater().inflate(layoutId, null);
        RadioGroup radioGroup = (RadioGroup) contentView.findViewById(R.id.radio_group_backup);

        radioGroup.check(getId(selectedId));
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if (mListener != null) {
                    mListener.onBackupSelected(type, getPosition(id));
                    mListener = null;
                    dismissAllowingStateLoss();
                }
            }
        });

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(contentView);
        alertDialog.setCancelable(false);
        Dialog dialog = alertDialog.create();
        //dialog.getWindow().getAttributes().windowAnimations = R.style.PermissionDialogAnimation;
        return dialog;
    }

    private int getPosition(int id) {
        if (id == R.id.radio1) return 1;
        if (id == R.id.radio2) return 2;
        if (id == R.id.radio3) return 3;
        if (id == R.id.radio_default) return 0;
        return 0;
    }

    private int getId(int pos) {
        if (pos == 1) return R.id.radio1;
        if (pos == 2) return R.id.radio2;
        if (pos == 3) return R.id.radio3;
        if (pos == 0) return R.id.radio_default;
        return R.id.radio_default;
    }

    @Override
    public void onDestroy() {
        mListener = null;
        super.onDestroy();
    }

    public interface BackupSettingListener {
        void onBackupSelected(int type, int postion);
    }
}
package com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail;

import com.metfone.selfcare.model.tab_video.Video;

public interface OnItemFilmRelatedListener {
    void onItemFilmRelatedClick(Video video);
}

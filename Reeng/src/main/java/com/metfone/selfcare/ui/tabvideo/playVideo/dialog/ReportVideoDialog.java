package com.metfone.selfcare.ui.tabvideo.playVideo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.util.Utilities;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class ReportVideoDialog extends Dialog {

    @BindView(R.id.rg_report)
    RadioGroup rgReport;
    @BindView(R.id.scroll_view)
    NestedScrollView scrollView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    String title;

    private Map<String, String> mapReport = new HashMap<>();
    private Video currentVideo;
    private VideoApi videoApi;
    private Context mContext;

    public ReportVideoDialog(@NonNull Context context) {
        super(context);
        mContext = context;
        ApplicationController mApp = ApplicationController.self();
        if (mApp != null)
            videoApi = mApp.getApplicationComponent().providerVideoApi();

        mapReport.put(context.getString(R.string.report_lag), "RP07");
        mapReport.put(context.getString(R.string.report_license), "RP05");
        mapReport.put(context.getString(R.string.report_misleading), "RP06");
        mapReport.put(context.getString(R.string.report_violence), "RP02");
        mapReport.put(context.getString(R.string.report_reactionary), "RP03");
//        mapReport.put(context.getString(R.string.report_child_abuse), "RP04");
        mapReport.put(context.getString(R.string.report_sex), "RP01");
    }

    public ReportVideoDialog setCurrentVideo(Video currentVideo) {
        this.currentVideo = currentVideo;
        return this;
    }

    public ReportVideoDialog setTitleDialog(String title) {
        this.title = title;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_report_video);
        ButterKnife.bind(this);
        Resources resources = getContext().getResources();
        if (resources != null && resources.getConfiguration().orientation == ORIENTATION_LANDSCAPE) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) scrollView.getLayoutParams();
            layoutParams.height = Utilities.dpToPixels(200, resources);
            scrollView.setLayoutParams(layoutParams);
        }
        if (tvTitle != null && title != null) tvTitle.setText(title);
    }

    @OnClick({R.id.btn_negative, R.id.btn_positive})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_negative:
                break;
            case R.id.btn_positive:
                logReport();
                break;
        }
        dismiss();
    }

    private void logReport() {
        if (rgReport != null && mapReport != null && videoApi != null && currentVideo != null) {
            int radioButtonID = rgReport.getCheckedRadioButtonId();
            RadioButton radioButton = rgReport.findViewById(radioButtonID);
            String reportName = radioButton.getText().toString();
            String reportId = mapReport.get(reportName);
            videoApi.reportVideo(currentVideo.getId(), currentVideo.getTitle(), currentVideo.getLink(), currentVideo.getOriginalPath(), reportId, reportName);
            if (mContext == null) return;
            Toast.makeText(mContext, mContext.getString(R.string.report_sent_succesfully), Toast.LENGTH_LONG).show();
        }
    }
}

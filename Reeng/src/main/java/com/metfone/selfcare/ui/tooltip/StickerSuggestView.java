/*
 * Copyright 2013 Niek Haarman
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metfone.selfcare.ui.tooltip;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.metfone.selfcare.adapter.EmoticonsGridAdapter;
import com.metfone.selfcare.adapter.SuggestStickerAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.ui.viewpagerindicator.CirclePageIndicator;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Collection;

public class StickerSuggestView extends LinearLayout implements
        ViewTreeObserver.OnPreDrawListener,
        View.OnClickListener {
    private static final String TAG = StickerSuggestView.class.getSimpleName();
    public static final String TRANSLATION_Y_COMPAT = "translationY";
    public static final String TRANSLATION_X_COMPAT = "translationX";
    public static final String SCALE_X_COMPAT = "scaleX";
    public static final String SCALE_Y_COMPAT = "scaleY";
    public static final String ALPHA_COMPAT = "alpha";

    private ViewGroup mContentView;
    private Context mContext;
    private StickerSuggestTip mStickerSuggestTip;
    private SuggestStickerAdapter mAdapter;
    private ViewPager mViewPager;
    private CirclePageIndicator mIndicator;
    private EmoticonsGridAdapter.KeyClickListener mKeyclickListener;

    private View mView;

    private boolean mDimensionsKnown;
    private int mRelativeMasterViewY;

    private int mRelativeMasterViewX;
    private int mWidth;
    private int mHeight;

    private int mTotalstiker = 0;
    private int mNumPage = 1;
    private ArrayList<StickerItem> mStikerList;

    private OnToolTipViewClickedListener mListener;

    public StickerSuggestView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Log.d(TAG, "init");
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 400));
        setOrientation(VERTICAL);
        LayoutInflater.from(getContext()).inflate(R.layout.layout_stiker_preview, this, true);
        mContentView = (ViewGroup) findViewById(R.id.content_view);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        setOnClickListener(this);
        getViewTreeObserver().addOnPreDrawListener(this);
    }

    @Override
    public boolean onPreDraw() {
        Log.d(TAG, "onPreDraw");
        getViewTreeObserver().removeOnPreDrawListener(this);
        mDimensionsKnown = true;
        mWidth = mContentView.getWidth();
        mHeight = mWidth / 4 + 10;
        //ViewGroup.LayoutParams contentLayoutParams = mContentView.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
        layoutParams.width = mWidth;
        //layoutParams.height = mHeight;
        //contentLayoutParams.height = mHeight;
        setLayoutParams(layoutParams);
        if (mStickerSuggestTip != null) {
            applyToolTipPosition();
        }
        return true;
    }

    public void setToolTip(StickerSuggestTip stickerSuggestTip, final View view) {
        mStickerSuggestTip = stickerSuggestTip;
        mView = view;

        if (mStickerSuggestTip.getColor() != 0) {
            setColor(mStickerSuggestTip.getColor());
        }

        if (mStickerSuggestTip.getContentView() != null) {
            setContentView(mStickerSuggestTip.getContentView());
        }

        if (mStickerSuggestTip.getStikerList() != null) {
            mStikerList = mStickerSuggestTip.getStikerList();
        }

        if (mStickerSuggestTip.getContext() != null) {
            mContext = mStickerSuggestTip.getContext();
        }
        if (mStickerSuggestTip.getmKeyclickListener() != null) {
            mKeyclickListener = mStickerSuggestTip.getmKeyclickListener();
        }

        if (mDimensionsKnown) {
            calculateStickerPage();
            applyToolTipPosition();
        }
    }

    private void calculateStickerPage() {
        if (mStikerList != null && !mStikerList.isEmpty()) {
            mTotalstiker = mStikerList.size();
            mNumPage = mTotalstiker / 4 + 1;
        }
    }


    private void applyToolTipPosition() {
        Log.d(TAG, "applyToolTipPosition");
        final int[] masterViewScreenPosition = new int[2];
        mView.getLocationOnScreen(masterViewScreenPosition);

        final Rect viewDisplayFrame = new Rect();
        mView.getWindowVisibleDisplayFrame(viewDisplayFrame);

        final int[] parentViewScreenPosition = new int[2];
        ((View) getParent()).getLocationOnScreen(parentViewScreenPosition);

        final int masterViewWidth = mView.getWidth();
//        final int masterViewHeight = mView.getHeight();

        mRelativeMasterViewX = masterViewScreenPosition[0] - parentViewScreenPosition[0];
        mRelativeMasterViewY = masterViewScreenPosition[1] - parentViewScreenPosition[1];
        final int relativeMasterViewCenterX = mRelativeMasterViewX + masterViewWidth / 2;

        int toolTipViewAboveY = mRelativeMasterViewY - getHeight();
//        int toolTipViewBelowY = Math.max(0, mRelativeMasterViewY + masterViewHeight);

        int toolTipViewX = Math.max(0, relativeMasterViewCenterX - mWidth / 2);
        if (toolTipViewX + mWidth > viewDisplayFrame.right) {
            toolTipViewX = viewDisplayFrame.right - mWidth;
        }

        setX(toolTipViewX);
        setPointerCenterX(relativeMasterViewCenterX);



        int toolTipViewY;
        /*boolean showBelow = false;
        Log.d(TAG,"showBelow: " + showBelow);
        if (showBelow) {
            toolTipViewY = toolTipViewBelowY;
        } else {*/
            toolTipViewY = toolTipViewAboveY;
        //}

        mAdapter = new SuggestStickerAdapter(mStikerList, mKeyclickListener, mContext);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(0);
        mIndicator.setViewPager(mViewPager);

        if (mStickerSuggestTip.getAnimationType() == StickerSuggestTip.AnimationType.NONE) {
            ViewHelper.setTranslationY(this, toolTipViewY);
            ViewHelper.setTranslationX(this, toolTipViewX);
        } else {
            Collection<Animator> animators = new ArrayList<>(5);

            if (mStickerSuggestTip.getAnimationType() == StickerSuggestTip.AnimationType.FROM_MASTER_VIEW) {
                animators.add(ObjectAnimator.ofInt(this, TRANSLATION_Y_COMPAT, mRelativeMasterViewY + mView.getHeight() / 2 - getHeight() / 2, toolTipViewY));
                animators.add(ObjectAnimator.ofInt(this, TRANSLATION_X_COMPAT, mRelativeMasterViewX + mView.getWidth() / 2 - mWidth / 2, toolTipViewX));
            } else if (mStickerSuggestTip.getAnimationType() == StickerSuggestTip.AnimationType.FROM_TOP) {
                animators.add(ObjectAnimator.ofFloat(this, TRANSLATION_Y_COMPAT, 0, toolTipViewY));
            }

            animators.add(ObjectAnimator.ofFloat(this, SCALE_X_COMPAT, 0, 1));
            animators.add(ObjectAnimator.ofFloat(this, SCALE_Y_COMPAT, 0, 1));

            animators.add(ObjectAnimator.ofFloat(this, ALPHA_COMPAT, 0, 1));

            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animators);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                animatorSet.addListener(new AppearanceAnimatorListener(toolTipViewX, toolTipViewY));
            }

            animatorSet.start();
        }
    }

    public void setPointerCenterX(final int pointerCenterX) {
    }

    public void setOnToolTipViewClickedListener(final OnToolTipViewClickedListener listener) {
        mListener = listener;
    }

    public void setColor(final int color) {
        mContentView.setBackgroundColor(color);
    }

    private void setContentView(final View view) {
        mContentView.removeAllViews();
        mContentView.addView(view);
    }

    public void remove() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
            setX(params.leftMargin);
            setY(params.topMargin);
            params.leftMargin = 0;
            params.topMargin = 0;
            setLayoutParams(params);
        }

        if (mStickerSuggestTip.getAnimationType() == StickerSuggestTip.AnimationType.NONE) {
            if (getParent() != null) {
                ((ViewManager) getParent()).removeView(this);
            }
        } else {
            Collection<Animator> animators = new ArrayList<>(5);
            if (mStickerSuggestTip.getAnimationType() == StickerSuggestTip.AnimationType.FROM_MASTER_VIEW) {
                animators.add(ObjectAnimator.ofInt(this, TRANSLATION_Y_COMPAT, (int) getY(), mRelativeMasterViewY + mView.getHeight() / 2 - getHeight() / 2));
                animators.add(ObjectAnimator.ofInt(this, TRANSLATION_X_COMPAT, (int) getX(), mRelativeMasterViewX + mView.getWidth() / 2 - mWidth / 2));
            } else {
                animators.add(ObjectAnimator.ofFloat(this, TRANSLATION_Y_COMPAT, getY(), 0));
            }

            animators.add(ObjectAnimator.ofFloat(this, SCALE_X_COMPAT, 1, 0));
            animators.add(ObjectAnimator.ofFloat(this, SCALE_Y_COMPAT, 1, 0));

            animators.add(ObjectAnimator.ofFloat(this, ALPHA_COMPAT, 1, 0));

            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animators);
            animatorSet.addListener(new DisappearanceAnimatorListener());
            animatorSet.start();
        }
    }

    @Override
    public void onClick(final View view) {
        //remove();
        if (mListener != null) {
            mListener.onToolTipViewClicked(this);
        }
    }

    /**
     * Convenience method for getting X.
     */
    @SuppressLint("NewApi")
    @Override
    public float getX() {
        float result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            result = super.getX();
        } else {
            result = ViewHelper.getX(this);
        }
        return result;
    }

    /**
     * Convenience method for setting X.
     */
    @SuppressLint("NewApi")
    @Override
    public void setX(final float x) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            super.setX(x);
        } else {
            ViewHelper.setX(this, x);
        }
    }

    /**
     * Convenience method for getting Y.
     */
    @SuppressLint("NewApi")
    @Override
    public float getY() {
        float result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            result = super.getY();
        } else {
            result = ViewHelper.getY(this);
        }
        return result;
    }

    /**
     * Convenience method for setting Y.
     */
    @SuppressLint("NewApi")
    @Override
    public void setY(final float y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            super.setY(y);
        } else {
            ViewHelper.setY(this, y);
        }
    }

    public interface OnToolTipViewClickedListener {
        void onToolTipViewClicked(StickerSuggestView toolTipView);
    }

    private class AppearanceAnimatorListener extends AnimatorListenerAdapter {

        private final float mToolTipViewX;
        private final float mToolTipViewY;

        AppearanceAnimatorListener(final float fToolTipViewX, final float fToolTipViewY) {
            mToolTipViewX = fToolTipViewX;
            mToolTipViewY = fToolTipViewY;
        }

        @Override
        public void onAnimationStart(final Animator animation) {
        }

        @Override
        @SuppressLint("NewApi")
        public void onAnimationEnd(final Animator animation) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
            params.leftMargin = (int) mToolTipViewX;
            params.topMargin = (int) mToolTipViewY;
            setX(0);
            setY(0);
            setLayoutParams(params);
        }

        @Override
        public void onAnimationCancel(final Animator animation) {
        }

        @Override
        public void onAnimationRepeat(final Animator animation) {
        }
    }

    private class DisappearanceAnimatorListener extends AnimatorListenerAdapter {

        @Override
        public void onAnimationStart(final Animator animation) {
        }

        @Override
        public void onAnimationEnd(final Animator animation) {
            if (getParent() != null) {
                ((ViewManager) getParent()).removeView(StickerSuggestView.this);
            }
        }

        @Override
        public void onAnimationCancel(final Animator animation) {
        }

        @Override
        public void onAnimationRepeat(final Animator animation) {
        }
    }
}

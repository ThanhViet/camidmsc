package com.metfone.selfcare.ui.chatviews;

import android.content.Context;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;

/**
 * Created by thanhnt72 on 1/8/2018.
 */

public class MultiLineEdittextTag extends TagsCompletionView {

    private static final String TAG = MultiLineEdittextTag.class.getSimpleName();
    private boolean mIsSend = false;
    private int mActionEditerInfo;

    public MultiLineEdittextTag(Context context) {
        super(context);
    }

    public MultiLineEdittextTag(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiLineEdittextTag(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setEditerAction(boolean isSend, int actionEditerInfo) {
        mIsSend = isSend;
        mActionEditerInfo = actionEditerInfo;
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        InputConnection connection = super.onCreateInputConnection(outAttrs);

        if (mIsSend) {
            int imeActions = outAttrs.imeOptions & EditorInfo.IME_MASK_ACTION;
            if ((imeActions & mActionEditerInfo) != 0) {
                // clear the existing action
                outAttrs.imeOptions ^= imeActions;
                // set the DONE action
                outAttrs.imeOptions |= mActionEditerInfo;
            }
            if ((outAttrs.imeOptions & EditorInfo.IME_FLAG_NO_ENTER_ACTION) != 0) {
                outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
            }
        }

        return connection;
    }

    public void setDropDownBackgroundResource(@DrawableRes int resId) {
        if(Version.hasM()){
            setDropDownBackgroundDrawable(ContextCompat.getDrawable(getContext(), resId));
        } else {
            super.setDropDownBackgroundResource(resId);
        }
    }
}

package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 4/21/2018.
 */

public class DialogMessage extends Dialog {
    private BaseSlidingFragmentActivity activity;
    private String label;
    private String msg;
    private String labelButton;
    private DismissListener dismissListener;
    private int gravity;
    private boolean useHtml = false;

    private Button mBtnOk;
    private TextView mTvwTitle, mTvwMessage;
    private View mViewDivider;

    private NegativeListener<Object> negativeListener;

    public DialogMessage(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        setCancelable(isCancelable);
    }

    public DialogMessage setLabel(String label) {
        this.label = label;
        return this;
    }

    public DialogMessage setMessage(String message) {
        this.msg = message;
        return this;
    }

    public DialogMessage setLabelButton(String labelButton) {
        this.labelButton = labelButton;
        return this;
    }

    public DialogMessage setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    public DialogMessage setNegativeListener(NegativeListener listener) {
        this.negativeListener = listener;
        return this;
    }

    public DialogMessage setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public DialogMessage setUseHtml(boolean useHtml) {
        this.useHtml = useHtml;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_message);
        findComponentViews();
        drawDetail();
        if (gravity > 0) {
            mTvwMessage.setGravity(Gravity.CENTER);
            gravity = 0;
        }
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (negativeListener != null) {
                    negativeListener.onNegative(null);
                    dismiss();
                } else
                    dismiss();
            }
        });
    }

    @Override
    public void dismiss() {
        Log.d("DialogConfirm", "dismiss");
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    private void findComponentViews() {
        mTvwTitle = findViewById(R.id.dialog_confirm_label);
        mTvwMessage = findViewById(R.id.dialog_confirm_message);
        mViewDivider = findViewById(R.id.dialog_confirm_divider);
        mBtnOk = findViewById(R.id.dialog_button_ok);
    }

    private void drawDetail() {
        if (TextUtils.isEmpty(label)) {
            mTvwTitle.setVisibility(View.GONE);
        } else {
            mTvwTitle.setVisibility(View.VISIBLE);
            mTvwTitle.setText(label);

        }
        if (TextUtils.isEmpty(msg)) {
            mTvwMessage.setVisibility(View.GONE);
        } else {
            mTvwMessage.setVisibility(View.VISIBLE);
            if (useHtml)
                mTvwMessage.setText(Html.fromHtml(msg));
            else
                mTvwMessage.setText(msg);
        }
        if (TextUtils.isEmpty(labelButton)) {
            mBtnOk.setText(activity.getResources().getString(R.string.ok));
        } else {
            mBtnOk.setText(labelButton);
        }
    }
}

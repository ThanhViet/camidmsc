package com.metfone.selfcare.ui.recyclerview;

import android.content.Context;
import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;

/**
 * Created by toanvk2 on 10/11/2016.
 */

public class DividerStrangerDecoration extends RecyclerView.ItemDecoration {
    private HeaderAndFooterRecyclerViewAdapter adapter;
    private int gridSpacing;
    private int listSpacing;
    private boolean isGrid;

    public DividerStrangerDecoration(Context context, HeaderAndFooterRecyclerViewAdapter adapter, boolean isGrid) {
        this.adapter = adapter;
        this.gridSpacing = context.getResources().getDimensionPixelSize(R.dimen.divider_grid_stranger_size) / 2;
        this.listSpacing = context.getResources().getDimensionPixelSize(R.dimen.divider_height);
        this.isGrid = isGrid;
    }

    public void seGrid(boolean isGrid) {
        this.isGrid = isGrid;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        boolean isHeaderOrFooter = adapter.isHeader(position) || adapter.isFooter(position);
        if (isHeaderOrFooter) {
            outRect.set(0, 0, 0, 0);
        } else if (isGrid) {
            outRect.set(0, gridSpacing, 0, 0);
        } else {
            outRect.set(0, 0, 0, listSpacing);
        }
    }
}
package com.metfone.selfcare.ui.ListviewExpandable;

/**
 * Created by thanhnt72 on 6/21/2016.
 */
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

public interface ListViewWrapper {

    @NonNull
    ViewGroup getListView();

    @Nullable
    View getChildAt(int index);

    int getFirstVisiblePosition();

    int getLastVisiblePosition();

    int getCount();

    int getChildCount();

    int getHeaderViewsCount();

    int getPositionForView(@NonNull View view);

    @Nullable
    ListAdapter getAdapter();

    void smoothScrollBy(int distance, int duration);
}
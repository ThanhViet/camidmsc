package com.metfone.selfcare.ui.dialog;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;

import java.util.ArrayList;

/*created by huongnd38 on 23/10/2018*/

public class PermissionSettingIntroDialog {

    public static final String TAG = PermissionSettingIntroDialog.class.getSimpleName();
    ListView mListView;
    PermissionListAdapter mAdapter;
    ArrayList<PermissionItem> permissionItems = new ArrayList<>();
    int permissionCode;
    View mUnderstoodBtn;
    private Context mContext;
    TextView tvHeader;
    ArrayList<String> permissions = new ArrayList<>();

    public PermissionSettingIntroDialog(@NonNull Context context) {
        mContext = context;
    }

    public static void showDialog(Context context, ArrayList<String> permissions, int permissionCode) {
        PermissionSettingIntroDialog dialogFragment = new PermissionSettingIntroDialog(context);
        dialogFragment.mContext = context;
        dialogFragment.permissionCode = permissionCode;
        dialogFragment.permissions = permissions;
        dialogFragment.builDialog().show();
    }

    private Dialog builDialog() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_permission_setting_intro, null);
        mListView = (ListView) contentView.findViewById(R.id.permission_setting_listview);
        mUnderstoodBtn = contentView.findViewById(R.id.btn_understood_permission);
        tvHeader = (TextView) contentView.findViewById(R.id.dialog_permission_settings_header);
        mAdapter = new PermissionListAdapter(mContext, permissionItems);

        setListData();
        setHeaderText();
        mListView.setAdapter(mAdapter);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setView(contentView);
        alertDialog.setCancelable(false);
        final Dialog dialog = alertDialog.create();

        mUnderstoodBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                    if (mContext != null && mContext instanceof BaseSlidingFragmentActivity) {
                        ((BaseSlidingFragmentActivity) mContext).finish();
                    }
                    mContext = null;
                }
            }
        });
        return dialog;
    }

    private void setListData() {
        permissionItems.clear();

        switch (permissionCode) {
            case Constants.PERMISSION.PERMISSION_REQUEST_ALL:
                if (permissions != null && permissions.contains(Manifest.permission.READ_PHONE_STATE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_phone, mContext.getString(R.string.permission_setting_phone)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_CONTACTS)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_contact, mContext.getString(R.string.permission_setting_contact)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                }
//                if (permissions != null && (permissions.contains(Manifest.permission.RECEIVE_SMS) || permissions.contains(Manifest.permission.READ_SMS))) {
//                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_sms, mContext.getString(R.string.permission_setting_sms)));
//                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_LOCATION:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_location, mContext.getString(R.string.permission_setting_location)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_RECORD_AUDIO:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_mic, mContext.getString(R.string.permission_setting_mic)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_camera, mContext.getString(R.string.permission_setting_camera)));
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_VIDEO:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_camera, mContext.getString(R.string.permission_setting_camera)));
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_contact, mContext.getString(R.string.permission_setting_contact)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_SAVE_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_contact, mContext.getString(R.string.permission_setting_contact)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_DELETE_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_contact, mContext.getString(R.string.permission_setting_contact)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_WRITE_STORAGE:
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                }
//                if (permissions != null && (permissions.contains(Manifest.permission.RECEIVE_SMS) || permissions.contains(Manifest.permission.READ_SMS))) {
//                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_sms, mContext.getString(R.string.permission_setting_sms)));
//                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_WRITE_CONTACT:
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_CONTACTS)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_contact, mContext.getString(R.string.permission_setting_contact)));
                }
//                if (permissions != null && (permissions.contains(Manifest.permission.RECEIVE_SMS) || permissions.contains(Manifest.permission.READ_SMS))) {
//                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_sms, mContext.getString(R.string.permission_setting_sms)));
//                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO_GROUP_AVATAR: //quyen Camera
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_camera, mContext.getString(R.string.permission_setting_camera)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_GALLERY: //quyen bo nho WRITE_EXTERNAL_STORAGE
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_FILE: //quyen bo nho WRITE_EXTERNAL_STORAGE
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_FILE_DOWNLOAD: //quyen bo nho WRITE_EXTERNAL_STORAGE
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_RECEIVE_MESSAGE:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_sms, mContext.getString(R.string.permission_setting_sms)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_PHONE:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_phone, mContext.getString(R.string.permission_setting_phone)));
                break;



            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO_AND_STORAGE:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_camera, mContext.getString(R.string.permission_setting_camera)));
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO:
                if (permissions != null && permissions.contains(Manifest.permission.CAMERA)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_camera, mContext.getString(R.string.permission_setting_camera)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.RECORD_AUDIO)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_mic, mContext.getString(R.string.permission_setting_mic)));
                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_STORAGE_AND_CONTACT:
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_CONTACTS)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_contact, mContext.getString(R.string.permission_setting_contact)));
                }
//                if (permissions != null && (permissions.contains(Manifest.permission.RECEIVE_SMS) || permissions.contains(Manifest.permission.READ_SMS))) {
//                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_sms, mContext.getString(R.string.permission_setting_sms)));
//                }
                break;

            default:
                if (permissions != null && permissions.contains(Manifest.permission.READ_PHONE_STATE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_phone, mContext.getString(R.string.permission_setting_phone)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_CONTACTS)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_contact, mContext.getString(R.string.permission_setting_contact)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_storage, mContext.getString(R.string.permission_setting_storage)));
                }
//                if (permissions != null && (permissions.contains(Manifest.permission.RECEIVE_SMS) || permissions.contains(Manifest.permission.READ_SMS))) {
//                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_sms, mContext.getString(R.string.permission_setting_sms)));
//                }
                if (permissions != null && (permissions.contains(Manifest.permission.ACCESS_COARSE_LOCATION) || permissions.contains(Manifest.permission.ACCESS_FINE_LOCATION))) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_location, mContext.getString(R.string.permission_setting_location)));
                }
                if (permissions != null && (permissions.contains(Manifest.permission.RECORD_AUDIO))) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_mic, mContext.getString(R.string.permission_setting_mic)));
                }
                if (permissions != null && (permissions.contains(Manifest.permission.CAMERA))) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_settings_camera, mContext.getString(R.string.permission_setting_camera)));
                }
                break;
        }
    }

    public void setHeaderText() {
        if (mContext == null) return;
        if (permissions != null && permissions.size() == 1) {
            String permission = permissions.get(0);
            if (permission == null) return;
            String headerPermission = "";
            if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                headerPermission = mContext.getString(R.string.permission_setting_phone);
            } else if (permission.equals(Manifest.permission.WRITE_CONTACTS) || permission.equals(Manifest.permission.READ_CONTACTS)) {
                headerPermission = mContext.getString(R.string.permission_setting_contact);
            } else if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                headerPermission = mContext.getString(R.string.permission_setting_storage);
            }
//            else if (permission.equals(Manifest.permission.RECEIVE_SMS) || permission.equals(Manifest.permission.READ_SMS)) {
//                headerPermission = mContext.getString(R.string.permission_setting_sms);
//            }
            else if (permission.equals(Manifest.permission.CAMERA)) {
                headerPermission = mContext.getString(R.string.permission_setting_camera);
            } else if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION) || permission.equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                headerPermission = mContext.getString(R.string.permission_setting_location);
            } else if (permission.equals(Manifest.permission.RECORD_AUDIO)) {
                headerPermission = mContext.getString(R.string.permission_setting_mic);
            }
            if (tvHeader != null && !TextUtils.isEmpty(headerPermission)) {
                tvHeader.setText(mContext.getString(R.string.permission_setting_header, headerPermission));
            }
        }
    }

    public static class PermissionListAdapter extends BaseAdapter {
        LayoutInflater mLayoutInflater;

        PermissionListAdapter(Context context, ArrayList<PermissionItem> list) {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.list = list;
        }

        ArrayList<PermissionItem> list;

        @Override
        public int getCount() {
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {
            if (list != null && position < list.size()) {
                return list.get(position);
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.item_permission_setting_intro, parent, false);
                holder = new ViewHolder();
                holder.imagePermission = (ImageView) convertView.findViewById(R.id.item_permission_image);
                holder.tvPermission = (TextView) convertView.findViewById(R.id.item_permission_text);
                holder.imagePermissionStep = (ImageView) convertView.findViewById(R.id.item_permission_step_2);
                holder.imagePermissionHandle = (ImageView) convertView.findViewById(R.id.item_permission_image_handler);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.imagePermissionStep.setVisibility(position == 0 ? View.VISIBLE : View.INVISIBLE);
            holder.imagePermissionHandle.setVisibility(position == getCount() - 1? View.VISIBLE : View.INVISIBLE);
            holder.tvPermission.setText(list.get(position).permission);
            holder.imagePermission.setImageResource(list.get(position).permissionImageId);
            return convertView;
        }
    }

    private static class PermissionItem {
        String permission;
        int permissionImageId;

        public PermissionItem(int permissionImageId, String permission) {
            this.permission = permission;
            this.permissionImageId = permissionImageId;
        }
    }

    private static class ViewHolder {
        ImageView imagePermission;
        TextView tvPermission;
        ImageView imagePermissionStep;
        ImageView imagePermissionHandle;
    }

}

package com.metfone.selfcare.ui.recyclerview;

import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.util.Log;

public abstract class HidingScrollListener extends RecyclerView.OnScrollListener {

    private static final String TAG = HidingScrollListener.class.getSimpleName();
    private static final int HIDE_THRESHOLD = 20;
    private int scrolledDistance = 0;
    private int hideThreshold;
    private boolean controlsVisible = true;
    private boolean needProcess;

    public HidingScrollListener() {
        hideThreshold = HIDE_THRESHOLD;
    }

    public HidingScrollListener(int distance) {
        hideThreshold = distance;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        Log.i(TAG, "onScrollStateChanged scrollState = " + newState);
        needProcess = newState != RecyclerView.SCROLL_STATE_DRAGGING;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (scrolledDistance > hideThreshold && controlsVisible && needProcess) {
            onHide();
            controlsVisible = false;
            scrolledDistance = 0;
        } else if (scrolledDistance < -hideThreshold && !controlsVisible && needProcess) {
            onShow();
            controlsVisible = true;
            scrolledDistance = 0;
        }

        if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
            scrolledDistance += dy;
        }
    }

    public abstract void onHide();

    public abstract void onShow();

}


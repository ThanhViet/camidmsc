package com.metfone.selfcare.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.TagHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.metfone.selfcare.helper.Constants.MENU.SEND_MESSAGE;
import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

/**
 * Created by huongnd on 11/28/2018.
 */

public class BottomSheetContextMenu implements PopupListener {
    private static final String TAG = BottomSheetContextMenu.class.getSimpleName();
    private static final int GRID_SPAN_SIZE = 12;
    private static final int MAX_COLUMN_IN_ROW = 4;
    private BottomSheetDialog bottomSheetDialog;
    private  View viewParent;
    private ReactionPopup mReactionPopup;
    private View closeSheetView;
    private RecyclerView recyclerView;
    private ReengMessage mReengMessage;
    private ThreadMessage mThreadMessage;
    private Context mContext;
    private ApplicationController mApplication;
    private CircleImageView imgAvatar;
    private EllipsisTextView tvContent;
    private TextView tvAvatar;
    private View popupAnchorView, avatarFrame;
    private View bottomSheetHeader;

    public BottomSheetContextMenu(BaseSlidingFragmentActivity context, final ThreadMessage threadMessage, final ReengMessage reengMessage, ArrayList<ItemContextMenu> arrayList, ClickListener.IconListener callback, ReactionPopup.OnReactImageClickListener listener) {
        viewParent = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_context_menu, null, false);
        mContext = context;
        mApplication = (ApplicationController) context.getApplicationContext();
        closeSheetView = viewParent.findViewById(R.id.image_close_sheet);
        imgAvatar = viewParent.findViewById(R.id.image_avatar);
        tvAvatar = viewParent.findViewById(R.id.text_avatar);
        tvContent = viewParent.findViewById(R.id.tv_content);
        bottomSheetHeader = viewParent.findViewById(R.id.bottom_sheet_header);
        avatarFrame = viewParent.findViewById(R.id.avatar_frame);
        popupAnchorView = viewParent.findViewById(R.id.reaction_popup_anchor_view);
        mThreadMessage = threadMessage;
        mReengMessage = reengMessage;
        recyclerView = viewParent.findViewById(R.id.recycler_menu);
        //tvContent.setText(reengMessage.getContent());
        String content = /*reengMessage.getContent();*/ mApplication.getMessageBusiness().getContentOfMessage(mReengMessage, mApplication.getResources(), mApplication);
        if (!TextUtils.isEmpty(content)) {
            if (reengMessage.getMessageType() != ReengMessageConstant.MessageType.text) {
                content = "[" + content + "]";
            }
            tvContent.setEmoticon(context, content, content.hashCode(), content);
        }
        setFriendAvatar(imgAvatar, tvAvatar, reengMessage, threadMessage);
        bottomSheetDialog = new CustomBottomSheetDialog(context, R.style.BottomSheetDialogTheme) {
            @Override
            public void dismiss() {
                super.dismiss();
                try {
                    if (mReactionPopup != null) {
                        mReactionPopup.setParent(null);
                        mReactionPopup.dismiss();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "dismiss: Exception!", e);
                }
                mReactionPopup = null;
                mReengMessage = null;
                mThreadMessage = null;
                mContext = null;
            }
        };

        bottomSheetDialog.setContentView(viewParent);
        if (needToShowReaction()) {
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            int myReactionPoint = mApplication.getMessageBusiness().getReactionByNumber(reengMessage.getId(), reengMessage.getPacketId(), myNumber);
            mReactionPopup = new ReactionPopup(context).setAnchorView(popupAnchorView).setCancelable(false).setReengMessage(mReengMessage).setReactionClickListener(listener).setReactionPoint(myReactionPoint);
            mReactionPopup.setParent(this);
        }

        closeSheetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bottomSheetDialog != null) {
                    bottomSheetDialog.dismiss();
                }
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 4, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(new ContextMenuItemAdapter(context, arrayList, callback, this));
    }

    private boolean needToShowReaction() {
        return mReengMessage != null && mReengMessage.isReactionType();
    }

    public static BottomSheetContextMenu showContextMenu(BaseSlidingFragmentActivity context, ThreadMessage threadMessage, ReengMessage reengMessage, ArrayList<ItemContextMenu> list, ClickListener.IconListener callback, ReactionPopup.OnReactImageClickListener listener) {
        BottomSheetContextMenu bottomSheetContextMenu = new BottomSheetContextMenu(context, threadMessage, reengMessage, list, callback, listener);
        bottomSheetContextMenu.show();
        return bottomSheetContextMenu;
    }

    private void show() {
        if (bottomSheetDialog == null) return;
        bottomSheetDialog.show();
        if (popupAnchorView != null) {
            popupAnchorView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mReactionPopup != null) {
                        mReactionPopup.showToTopOfBottomSheet();
                    }
                }
            }, 150);
        }
    }

    public void dismiss() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.dismiss();
            bottomSheetDialog = null;
        }
    }

    @Override
    public void dismissPopup() {
        try {
            dismiss();
        } catch (Exception e) {
        }
    }

    private static class ContextMenuItemAdapter extends RecyclerView.Adapter<ContextMenuItemAdapter.ViewHolder> {

        private ArrayList<ItemContextMenu> mData;
        private LayoutInflater mInflater;
        private ClickListener.IconListener clickHandler = null;
        private WeakReference<BottomSheetContextMenu> mParent;
        private Context mContext;

        ContextMenuItemAdapter(Context context, ArrayList<ItemContextMenu> data, ClickListener.IconListener clickHandler, BottomSheetContextMenu parent) {
            this.mInflater = LayoutInflater.from(context);
            this.mContext = context;
            this.mData = data;
            this.clickHandler = clickHandler;
            mParent = new WeakReference<>(parent);
        }

        private ViewHolder.OnItemClickListener listener = new ViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                if (mData == null || mData.size() <= pos) return;
                final ItemContextMenu item = mData.get(pos);
                Log.d("lap: ",item.getActionTag()+""+mData.get(pos).getText()+mData.get(pos).getObj());
                if (clickHandler != null) {
                    if (item != null) {
                        if (item.getActionTag() == Constants.MENU.MESSAGE_DELETE) {
                            ItemContextMenu itemContextMenu = null;
                            for (ItemContextMenu itemMenu : mData) {
                                if (itemMenu.getActionTag() == Constants.MENU.MESSAGE_RESTORE) {
                                    itemContextMenu = itemMenu;
                                    Log.d("lap: ", itemMenu.toString() + "");
                                    Log.d("lap: ", mData.toString() + "");
                                    break;
                                }
                            }
                            final ItemContextMenu retoreItem = itemContextMenu;
                            BottomSheetDeleteItemMessage mBottomSheetDelete = new BottomSheetDeleteItemMessage(mContext, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //delete_for_me
                                    Log.d("lap: ", " ok");
                                    if (clickHandler != null && item != null) {
                                        clickHandler.onIconClickListener(null, item.getObj(), item.getActionTag());
                                    }
                                    clickHandler = null;
                                    BottomSheetDeleteItemMessage.dismisss();

                                }
                            }, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //delete_for_all
                                    if (clickHandler != null) {
                                        clickHandler.onIconClickListener(null, item.getObj(), Constants.MENU.MESSAGE_RESTORE);
                                    }
                                    clickHandler = null;
                                    BottomSheetDeleteItemMessage.dismisss();
                                }
                            });
                            mBottomSheetDelete.show();
                        } else if (item.getActionTag() == Constants.MENU.MESSAGE_SHARE) {
                            clickHandler.onIconClickListener(null, item.getObj(), Constants.MENU.MESSAGE_SHARE);
                        } else {
                            clickHandler.onIconClickListener(null, item.getObj(), item.getActionTag());
                            clickHandler = null;
                        }
                    }
                }
                if (mParent != null) {
                    BottomSheetContextMenu parent = mParent.get();
                    if (parent != null) {
                        parent.dismiss();
                    }
                    mParent.clear();
                    mParent = null;
                }
                mContext = null;
            }
        };

        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.message_context_menu_grid_item, parent, false);
            return new ViewHolder(view, listener);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            ItemContextMenu item = getItem(position);
            if (item != null) {
                holder.tvContextMenu.setText(item.getText());
                if (item.getImageRes() > 0) {
                    holder.imgContextMenu.setImageResource(item.getImageRes());
                }
            }
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return mData.size();
        }

        // stores and recycles views as they are scrolled off screen
        public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView tvContextMenu;
            ImageView imgContextMenu;
            OnItemClickListener listener;

            ViewHolder(View itemView, OnItemClickListener listener) {
                super(itemView);
                tvContextMenu = itemView.findViewById(R.id.tv_context_menu);
                imgContextMenu = itemView.findViewById(R.id.img_context_menu);
                this.listener = listener;
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition());
                }
                listener = null;
            }

            private interface OnItemClickListener {
                void onItemClick(int pos);
            }
        }

        ItemContextMenu getItem(int position) {
            return mData.get(position);
        }
    }

    protected void setFriendAvatar(CircleImageView mImgAvatar, TextView tvAvatar, final ReengMessage message, ThreadMessage threadMessage) {
        if (threadMessage == null || message == null || mContext == null || mApplication == null || mThreadMessage == null) {
            return;
        }
        int size = (int) mContext.getResources().getDimension(R.dimen.avatar_small_size);
        final String number = message.getSender();
        if (message.getMessageType() == ReengMessageConstant.MessageType.notification || message.getMessageType() == ReengMessageConstant.MessageType.notification_fake_mo || TextUtils.isEmpty(number)) {
            if (avatarFrame != null) {
                avatarFrame.setVisibility(View.GONE);
            }
            if (tvContent != null) {
                tvContent.setVisibility(View.GONE);
            }
            if (closeSheetView != null) {
                closeSheetView.setVisibility(View.GONE);
            }
            if (bottomSheetHeader != null) {
                bottomSheetHeader.setVisibility(View.GONE);
            }
            return;
        }
        if (number != null && number.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
            mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar, mApplication.getReengAccountBusiness().getCurrentAccount());
            return;
        }
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(message.getSender());
        int mThreadType = mThreadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, size, threadMessage.getServerId(), null, false);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            if (TextUtils.isEmpty(number)) {        //tin nhan tu admin rooms
                String avatarUrl;
                if (!TextUtils.isEmpty(message.getSenderAvatar())) {
                    avatarUrl = message.getSenderAvatar();
                } else {
                    avatarUrl = mApplication.getOfficerBusiness().getOfficerAvatarByServerId(threadMessage.getServerId());
                }
                mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, avatarUrl, size);
            } else {
                //tin nhan tu thanh vien khac
                int sizeSmall = (int) mContext.getResources().getDimension(R.dimen.avatar_thumbnail_size);
                mApplication.getAvatarBusiness().setMemberRoomChatAvatar(mImgAvatar, tvAvatar, message.getSender(), message.getSenderName(), phoneNumber, sizeSmall, message.getSenderAvatar());
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, tvAvatar, phoneNumber, size);
            } else {
                mApplication.getAvatarBusiness().setUnknownNumberAvatarGroup(mImgAvatar, tvAvatar, number, message.getSenderAvatar(), size);
            }
        } else {
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, tvAvatar, phoneNumber, size);
            } else {
                if (threadMessage.isStranger()) {
                    mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar, tvAvatar, threadMessage.getStrangerPhoneNumber(),
                            message.getSender(), message.getSender(), null, size);
                } else {
                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatar, tvAvatar, number, size);
                }
            }
        }
    }

    public static class CustomBottomSheetDialog extends BottomSheetDialog {

        public CustomBottomSheetDialog(@NonNull Context context, int theme) {
            super(context, theme);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public void show() {
            super.show();
            try {
                FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            } catch (Exception e) {
                Log.d(TAG, "show: Can not expand", e);
            }
        }

        @Override
        protected void onStop() {
            super.onStop();
        }
    }
}

package com.metfone.selfcare.ui.tabvideo.channelDetail.movie;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.adapter.MovieAdapter;
import com.metfone.selfcare.ui.tabvideo.fragment.BaseViewStubFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChangedDataListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MovieChannelFragment extends BaseViewStubFragment implements SwipeRefreshLayout.OnRefreshListener,
        ApiCallbackV2<ArrayList<Video>>,
        OnChannelChangedDataListener,
        OnVideoChangedDataListener,
        OnInternetChangedListener, OnClickContentMovie, OnClickMoreItemListener {

    private static final String CHANNEL = "channel";
    private static final int LIMIT = 21;

    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @BindView(R.id.tvEmptyTitle)
    TextView tvEmptyTitle;
    @BindView(R.id.tvEmptyDes)
    TextView tvEmptyDes;
    @BindView(R.id.icEmpty)
    ImageView icEmpty;
    @BindView(R.id.btnUpload)
    RoundTextView btnUpload;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    Unbinder unbinder;

    public static MovieChannelFragment newInstance(Channel channel) {

        Bundle args = new Bundle();
        args.putSerializable(CHANNEL, channel);
        MovieChannelFragment fragment = new MovieChannelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private @Nullable
    Channel mChannel;
    private Object loadMore;

    private ListenerUtils listenerUtils;
    private VideoApi mVideoApi;

    private MovieChannelAdapter mMovieAdapter;

    private int offset = 0;
    private String lastId = "";
    private ArrayList<Object> feeds;
    private boolean loading = false;
    private boolean isLoadMore = false;

    private boolean isGetInfoSuccess = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadMore = new Object();
        Bundle bundle = getArguments();
        mChannel = (Channel) (bundle != null ? bundle.getSerializable(CHANNEL) : null);
        listenerUtils = application.getApplicationComponent().providerListenerUtils();
        mVideoApi = application.getApplicationComponent().providerVideoApi();

        listenerUtils.addListener(this);
    }

    @Override
    protected void onCreateViewAfterViewStubInflated(View inflatedView, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, inflatedView);
        initView();
    }

    @Override
    protected int getViewStubLayoutResource() {
        return R.layout.fragment_channel_video;
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null)
            listenerUtils.removerListener(this);
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        initView();
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    @Override
    public void onSuccess(String lastIdStr, ArrayList<Video> results) {
        if (recyclerView == null || mMovieAdapter == null) return;
        isGetInfoSuccess = true;
        lastId = lastIdStr;

        ArrayList<Video> videos = provideVideo(results);

        if (feeds == null) feeds = new ArrayList<>();
        if (offset == 0) feeds.clear();

        if (Utilities.isEmpty(feeds)) {
            feeds.add(MovieAdapter.TYPE_SPACE_HEADER);
        }

        isLoadMore = results.size() >= LIMIT;
        feeds.remove(loadMore);

        if (Utilities.notEmpty(videos)) {
            for (Video video : videos) {
                if (feeds.contains(video)) continue;
                feeds.add(video);
            }
        }

        if (isLoadMore) {
            feeds.add(loadMore);
        } else {
            feeds.add(MovieAdapter.TYPE_SPACE_BOTTOM);
        }
        recyclerView.stopScroll();
        mMovieAdapter.bindData(feeds);

        if (!isFeedEmpty())
            hideError();
        else
            onError("");
    }

    @OnClick(R.id.btnUpload)
    public void onBtnUploadClicked() {
        DialogUtils.onClickUpload(activity);
    }

    private boolean isFeedEmpty() {
        if (Utilities.isEmpty(feeds)) {
            return true;
        }
        if (feeds.size() == 1 && feeds.contains(MovieAdapter.TYPE_SPACE_HEADER)) {
            return true;
        }
        if (feeds.size() == 2 && feeds.contains(MovieAdapter.TYPE_SPACE_HEADER) && feeds.contains(MovieAdapter.TYPE_SPACE_BOTTOM)) {
            return true;
        }
        return false;
    }

    @Override
    public void onError(String s) {
        isGetInfoSuccess = false;
        if (!isFeedEmpty()) return;
        showError();
        if (!NetworkHelper.isConnectInternet(activity)) {
            showErrorNetwork();
            hideErrorDataEmpty();
        } else {
            showErrorDataEmpty();
            hideErrorNetwork();
        }
    }

    @Override
    public void onComplete() {
        loading = false;
        if (refreshLayout == null) return;
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onChannelCreate(Channel channel) {
    }

    @Override
    public void onChannelUpdate(Channel channel) {
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {

    }

    @Override
    public void onVideoLikeChanged(Video video) {

    }

    @Override
    public void onVideoShareChanged(Video video) {

    }

    @Override
    public void onVideoCommentChanged(Video video) {

    }

    @Override
    public void onVideoSaveChanged(Video video) {

    }

    @Override
    public void onVideoWatchLaterChanged(Video video) {

    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(activity) || isGetInfoSuccess || Utilities.notEmpty(feeds))
            return;
        isGetInfoSuccess = true;
        initView();
    }

    private void initView() {
        hideError();

        mMovieAdapter = new MovieChannelAdapter(activity);
        mMovieAdapter.setListener(this);
        mMovieAdapter.setOnLoadMoreListener(onLoadMoreListener);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 2);
        gridLayoutManager.setSpanSizeLookup(mSpanSizeLookup);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mMovieAdapter);
        recyclerView.setHasFixedSize(true);
//        recyclerView.setPadding(Utilities.dpToPixels(4, getResources()), 0, Utilities.dpToPixels(4, getResources()), 0);

        refreshLayout.removeCallbacks(refreshRunnable);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(refreshRunnable);
    }

    private GridLayoutManager.SpanSizeLookup mSpanSizeLookup = new GridLayoutManager.SpanSizeLookup() {

        @Override
        public int getSpanSize(int position) {
            Object object = feeds.get(position);
            if (object instanceof Video) {
                return 1;
            } else if (Utilities.equals(object, MovieAdapter.TYPE_SPACE_HEADER)) {
                return 2;
            } else if (Utilities.equals(object, MovieAdapter.TYPE_SPACE_BOTTOM)) {
                return 2;
            } else {
                return 2;
            }
        }
    };

    private Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(true);
            loadData();
        }
    };

    private BaseAdapterV2.OnLoadMoreListener onLoadMoreListener = () -> {
        if (loading) return;
        if (!isLoadMore) return;
        loadMoreData();
    };

    @Override
    public void onClickMovieItem(Object item, int position) {
        if (item instanceof Video) {
            ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail(activity, (Video) item);
        }
    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        if (item instanceof Video) {
            Movie movie = Video.video2Movie((Video) item);
            DialogUtils.showOptionVideoMovieItem(activity, movie, this);
        }
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {

    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            if (menuId != Constants.MENU.MENU_EXIT && ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
                return;
            }
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Movie) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                        new WSOnMedia(application).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Movie) {
                        new MovieApi().insertWatchLater((Movie) object, new ApiCallbackV2<String>() {

                            @Override
                            public void onError(String s) {

                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onSuccess(String msg, String result) {
                                if (activity != null)
                                    activity.showToast(R.string.add_later_success);
                            }
                        });
                    }
                    break;
            }
        }
    }

    private ArrayList<Video> provideVideo(ArrayList<Video> results) {
        ArrayList<Video> videos = new ArrayList<>();
        if (mChannel == null) return videos;

        if (mChannel.isMyChannel()) {
            videos.addAll(results);
        } else {
            for (Video result : results) {
                if (result != null /*&& result.getItemStatus() == Video.Status.APPROVED.VALUE*/ && result.getFilmGroup() != null) {
                    videos.add(result);
                }
            }
        }
        return videos;
    }

    private void loadData() {
        if (loading) return;
        if (mChannel == null || mVideoApi == null) return;
        loading = true;
        mVideoApi.getMoviesByChannelIdV2(mChannel.getId(), offset, LIMIT, lastId, this);
    }

    private void refreshData() {
        offset = 0;
        lastId = "";
        loadData();
    }

    private void loadMoreData() {
        offset = offset + LIMIT;
        loadData();
    }

    private void showErrorDataEmpty() {
        if (tvEmptyTitle == null || tvEmptyDes == null) return;
        tvEmptyTitle.setVisibility(View.VISIBLE);
        if (icEmpty != null) icEmpty.setVisibility(View.VISIBLE);
        if (mChannel != null && mChannel.isMyChannel()) {
            tvEmptyTitle.setText(getString(R.string.no_channel_video));
            tvEmptyDes.setVisibility(View.VISIBLE);
            tvEmptyDes.setText(getString(R.string.no_channel_video_des));
            if (btnUpload != null) btnUpload.setVisibility(View.VISIBLE);
        } else {
            tvEmptyTitle.setText(getString(R.string.no_channel_contain_video));
            tvEmptyDes.setVisibility(View.GONE);
        }
    }

    private void hideErrorDataEmpty() {
        if (tvEmptyTitle == null) return;
        tvEmptyTitle.setVisibility(View.GONE);
        if (tvEmptyDes == null) return;
        tvEmptyDes.setVisibility(View.GONE);
    }

    private void showErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.VISIBLE);
        emptyRetryText2.setVisibility(View.VISIBLE);
    }

    private void hideErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.GONE);
        emptyRetryText2.setVisibility(View.GONE);
    }

    private void showError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.GONE);
    }

}

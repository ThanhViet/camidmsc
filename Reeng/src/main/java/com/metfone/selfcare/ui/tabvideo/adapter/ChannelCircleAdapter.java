package com.metfone.selfcare.ui.tabvideo.adapter;

import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.holder.ChannelCircleHolder;

public class ChannelCircleAdapter extends BaseAdapter {

    public ChannelCircleAdapter(BaseSlidingFragmentActivity activity) {
        super(activity);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChannelCircleHolder(layoutInflater, parent, (OnItemChannelCircleListener) onItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemObject itemObject = itemObjects.get(position);
        Clone itemClone = itemObject.getInfo();
        if (holder instanceof ChannelCircleHolder && itemClone instanceof Channel) {
            ((ChannelCircleHolder) holder).bindData((Channel) itemClone);
//            ((ChannelCircleHolder) holder).showTitleChannel(false);
        }
    }

    public interface OnItemChannelCircleListener extends OnItemListener, ChannelCircleHolder.OnChannelCircleListener {
    }

}

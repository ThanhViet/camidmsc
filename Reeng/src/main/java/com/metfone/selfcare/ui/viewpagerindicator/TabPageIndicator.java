package com.metfone.selfcare.ui.viewpagerindicator;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * This widget implements the dynamic action bar tab behavior that can change
 * across different configurations or circumstances.
 */
public class TabPageIndicator extends HorizontalScrollView implements
        PageIndicator {
    private static final CharSequence EMPTY_TITLE = "";
    private static final String TAG = "TabPageIndicator";
    private final IcsLinearLayout mTabLayout;
    private IconPagerAdapter iconAdapter;
    private ViewPager mViewPager;
    private OnPageChangeListener mListener;
    private int mMaxTabWidth;
    private int mSelectedTabIndex;
    private OnTabReselectedListener mTabReselectedListener;
    private final OnClickListener mTabClickListener;
    private OnTabSelectedListener mTabSelectedListener;
    private final OnClickListener mIconTabClickListener;
    private ArrayList<IconTabViewV1> iconTabViewArrayList;
    private Runnable mTabSelector;

    public interface OnTabReselectedListener {
        /**
         * Callback when the selected tab has been reselected.
         *
         * @param position Position of the current center item.
         */
        void onTabReselected(int position);
    }

    public TabPageIndicator(Context context) {
        this(context, null);

    }

    public TabPageIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mTabClickListener = new OnClickListener() {
            public void onClick(View view) {
                TabView tabView = (TabView) view;
                int oldSelected = TabPageIndicator.this.mViewPager.getCurrentItem();
                int newSelected = tabView.getIndex();
                TabPageIndicator.this.mViewPager.setCurrentItem(newSelected);
                if (oldSelected == newSelected && TabPageIndicator.this.mTabReselectedListener != null) {
                    TabPageIndicator.this.mTabReselectedListener.onTabReselected(newSelected);
                }

            }
        };
        this.mIconTabClickListener = new OnClickListener() {
            public void onClick(View view) {
                IconTabViewV1 tabView = (IconTabViewV1) view;
                int oldSelected = TabPageIndicator.this.mViewPager.getCurrentItem();
                int newSelected = tabView.getIndex();
                TabPageIndicator.this.mViewPager.setCurrentItem(newSelected);
                if (oldSelected == newSelected && TabPageIndicator.this.mTabReselectedListener != null) {
                    TabPageIndicator.this.mTabReselectedListener.onTabReselected(newSelected);
                }

            }
        };
        this.setHorizontalScrollBarEnabled(false);
        this.mTabLayout = new IcsLinearLayout(context, R.attr.vpiTabPageIndicatorStyle);
        this.addView(this.mTabLayout, new LayoutParams(-2, -1));
    }

    private void switchSelectedState(int oldItemIndex, int newItemIndex) {
        /*IconIndicator iconOldIndicator = this.iconAdapter.getIconIndicator(oldItemIndex);
        IconIndicator iconNewIndicator = this.iconAdapter.getIconIndicator(newItemIndex);
        IconTabViewV1 oldIconTabView = (IconTabViewV1) this.iconTabViewArrayList.get(oldItemIndex);
        IconTabViewV1 newIconTabView = (IconTabViewV1) this.iconTabViewArrayList.get(newItemIndex);
        if (iconOldIndicator.getTypeResource() == 0) {
        }

        if (iconNewIndicator.getTypeResource() == 0) {
        }*/
    }

    private void focusToTab(int tabIndex) {
        int oldSelected = this.mViewPager.getCurrentItem();
        Iterator it = this.iconTabViewArrayList.iterator();
        while (it.hasNext()) {
            IconTabViewV1 icon = (IconTabViewV1) it.next();
            if (icon.mIndex == tabIndex) {
                icon.setBackgroundResource(R.color.icon_selected);
            } else {
                icon.setBackgroundResource(R.color.icon_normal);
            }
        }
        this.switchSelectedState(oldSelected, tabIndex);
    }

    public void setOnTabReselectedListener(OnTabReselectedListener listener) {
        this.mTabReselectedListener = listener;
    }

    public void setOnTabSelectedListener(OnTabSelectedListener listener) {
        this.mTabSelectedListener = listener;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        boolean lockedExpanded = widthMode == MeasureSpec.EXACTLY;
        this.setFillViewport(lockedExpanded);
        int childCount = this.mTabLayout.getChildCount();
        if (childCount <= 1 || widthMode != MeasureSpec.EXACTLY && widthMode != MeasureSpec.AT_MOST) {
            this.mMaxTabWidth = -1;
            Log.i("TabPageIndicator", "mMaxTabWidth====== else " + this.mMaxTabWidth);
        } else {
            this.mMaxTabWidth = (int) ((float) MeasureSpec.getSize(widthMeasureSpec) * 0.15F);
            Log.i("TabPageIndicator", "mMaxTabWidth======childCount > 2 " + this.mMaxTabWidth);
        }

        int oldWidth = this.getMeasuredWidth();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int newWidth = this.getMeasuredWidth();
        if (lockedExpanded && oldWidth != newWidth) {
            this.setCurrentItem(this.mSelectedTabIndex);
        }

    }

    private void animateToTab(int position) {
        final View tabView = this.mTabLayout.getChildAt(position);
        if (this.mTabSelector != null) {
            this.removeCallbacks(this.mTabSelector);
        }

        this.mTabSelector = new Runnable() {
            public void run() {
                int scrollPos = tabView.getLeft() - (TabPageIndicator.this.getWidth() - tabView.getWidth()) / 2;
                TabPageIndicator.this.smoothScrollTo(scrollPos, 0);
                TabPageIndicator.this.mTabSelector = null;
            }
        };
        this.post(this.mTabSelector);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mTabSelector != null) {
            this.post(this.mTabSelector);
        }

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mTabSelector != null) {
            this.removeCallbacks(this.mTabSelector);
        }

    }

    private void addTab(int index, CharSequence text, IconIndicator iconIndicator) {
        Log.d("TabPageIndicator", "addTabaddTabaddTabaddTab" + index);
        IconTabViewV1 layout = new IconTabViewV1(this.getContext());
        layout.mIndex = index;
        layout.mType = iconIndicator.getTypeResource();
        layout.setFocusable(true);
        layout.setOnClickListener(this.mIconTabClickListener);
        layout.setClickable(true);
        if (iconIndicator.getTypeResource() == 0) {
            layout.imgUnselected.setImageResource(iconIndicator.getIdResIndicator());
            layout.imgSelected.setImageResource(iconIndicator.getIdResIndicatorSelected());
            ImageLoaderManager.getInstance(getContext()).cancelDisplayTag(layout.imgUnselected);
            ImageLoaderManager.getInstance(getContext()).cancelDisplayTag(layout.imgSelected);
        } else {
            ImageLoaderManager.getInstance(getContext()).displayStickerIndicator(layout.imgUnselected, null);
            ImageLoaderManager.getInstance(getContext()).displayStickerIndicator(layout.imgSelected, iconIndicator.getUrlIndicator());
        }

        android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams(0, -1, 1.0F);
        params.topMargin = 1;
        params.leftMargin = (int) this.getResources().getDimension(R.dimen.tab_padding);
        params.rightMargin = (int) this.getResources().getDimension(R.dimen.tab_padding);
        this.mTabLayout.addView(layout, params);
        this.iconTabViewArrayList.add(layout);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        if (this.mListener != null) {
            this.mListener.onPageScrollStateChanged(arg0);
        }
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        if (this.mListener != null) {
            this.mListener.onPageScrolled(arg0, arg1, arg2);
        }
    }

    @Override
    public void onPageSelected(int arg0) {
        this.setCurrentItem(arg0);
        if (this.mListener != null) {
            this.mListener.onPageSelected(arg0);
        }
    }

    @Override
    public void setViewPager(ViewPager view) {
        if (this.mViewPager != view) {
            if (this.mViewPager != null) {
                this.mViewPager.setOnPageChangeListener((OnPageChangeListener) null);
            }

            PagerAdapter adapter = view.getAdapter();
            if (adapter == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            } else {
                this.mViewPager = view;
                view.setOnPageChangeListener(this);
                this.iconTabViewArrayList = new ArrayList();
                this.notifyDataSetChanged();
            }
        }
    }

    public void notifyDataSetChanged() {
        this.mTabLayout.removeAllViews();
        PagerAdapter adapter = this.mViewPager.getAdapter();
        this.iconAdapter = null;
        if (adapter instanceof IconPagerAdapter) {
            this.iconAdapter = (IconPagerAdapter) adapter;
        }

        int count = adapter.getCount();

        for (int i = 0; i < count; ++i) {
            CharSequence title = adapter.getPageTitle(i);
            if (title == null) {
                title = EMPTY_TITLE;
            }

            IconIndicator icon = null;
            if (this.iconAdapter != null) {
                icon = this.iconAdapter.getIconIndicator(i);
            }

            this.addTab(i, title, icon);
        }

        if (this.mSelectedTabIndex > count) {
            this.mSelectedTabIndex = count - 1;
        }

        this.setCurrentItem(this.mSelectedTabIndex);
        this.requestLayout();
    }

    @Override
    public void setViewPager(ViewPager view, int initialPosition) {
        setViewPager(view);
        setCurrentItem(initialPosition);
    }

    private class IconTabViewHoler {
        public ImageView imgUnselected;
        public ImageView imgSelected;

        private IconTabViewHoler() {
        }
    }

    @Override
    public void setCurrentItem(int item) {
        if (this.mViewPager == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        } else {
            this.mSelectedTabIndex = item;
            this.mViewPager.setCurrentItem(item);
            this.focusToTab(this.mSelectedTabIndex);
            int tabCount = this.mTabLayout.getChildCount();

            for (int i = 0; i < tabCount; ++i) {
                View child = this.mTabLayout.getChildAt(i);
                boolean isSelected = i == item;
                child.setSelected(isSelected);
                if (isSelected) {
                    this.animateToTab(item);
                }
            }

        }
    }

    private class IconTabView extends ImageView {
        private int mIndex;

        public IconTabView(Context context) {
            super(context, (AttributeSet) null, R.attr.vpiTabPageIndicatorStyle);
        }

        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(TabPageIndicator.this.mMaxTabWidth, MeasureSpec.EXACTLY), heightMeasureSpec);
        }

        public int getIndex() {
            return this.mIndex;
        }
    }

    private class IconTabViewV1 extends RelativeLayout {
        private ImageView imgUnselected;
        private ImageView imgSelected;
        private int mIndex;
        private int mType;

        public IconTabViewV1(Context context) {
            super(context, (AttributeSet) null, R.attr.vpiTabPageIndicatorStyle);
            this.initView();
        }

        public int getmType() {
            return this.mType;
        }

        public void setmType(int mType) {
            this.mType = mType;
        }

        private void initView() {
            View view = inflate(this.getContext(), R.layout.icon_indicator_view, (ViewGroup) null);
            this.imgUnselected = (ImageView) view.findViewById(R.id.imageView);
            this.imgSelected = (ImageView) view.findViewById(R.id.imageViewSelected);
            this.imgUnselected.setVisibility(GONE);
            this.addView(view);
        }

        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(TabPageIndicator.this.mMaxTabWidth, MeasureSpec.EXACTLY), heightMeasureSpec);
        }

        public int getIndex() {
            return this.mIndex;
        }

        public void setSelected(boolean b) {
            if (this.mType == 0) {
                if (b) {
                    this.imgSelected.setVisibility(VISIBLE);
                    this.imgUnselected.setVisibility(GONE);
                } else {
                    this.imgSelected.setVisibility(GONE);
                    this.imgUnselected.setVisibility(VISIBLE);
                }

            }
        }
    }

    public interface OnTabSelectedListener {
        void onTabSelected(int var1);
    }

    @Override
    public void setOnPageChangeListener(OnPageChangeListener listener) {
        mListener = listener;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/8/12
 *
 */

package com.metfone.selfcare.ui.tabvideo.holder;

import android.app.Activity;
import android.content.res.Resources;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.tabvideo.listener.ChooseEpisodeListener;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.OnClick;

public class VideoEpisodePlayerHolder extends BaseAdapter.ViewHolder {
    private final String TAG = "VideoEpisodePlayerHolder";
    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_duration)
    @Nullable
    TextView tvDuration;
    @BindView(R.id.tv_description)
    @Nullable
    TextView tvDescription;

    private boolean isMovies;
    private Resources resources;
    private ChooseEpisodeListener listener;
    private int position;

    public VideoEpisodePlayerHolder(View view, Activity activity, ChooseEpisodeListener listener, boolean isMovies) {
        super(view);
        this.resources = activity.getResources();
        this.listener = listener;
        this.isMovies = isMovies;
        int width = Utilities.getWidthVideoEpisode();
        ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
        layoutParams.width = width;
        viewRoot.setLayoutParams(layoutParams);
        viewRoot.requestLayout();
    }

    @Override
    public void bindData(Object item, int position) {
        this.position = position;
    }

    public void bindData(Object item, int position, boolean isSelected) {
        viewRoot.setSelected(isSelected);
        this.position = position;
        if (item instanceof Video) {
            Video data = (Video) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getTitle());
//                tvTitle.setTextColor(resources.getColor(isSelected ? R.color.bg_mocha : R.color.title_video_episode));
                tvTitle.setTextColor(resources.getColor(R.color.title_video_episode));
            }
            if (tvDescription != null) {
                String desc = data.getDescription();
                tvDescription.setText(desc);
//                tvDescription.setTextColor(resources.getColor(isSelected ? R.color.bg_mocha : R.color.desc_video_episode));
                tvDescription.setTextColor(resources.getColor(R.color.desc_video_episode));
                tvDescription.setVisibility(TextUtils.isEmpty(desc) ? View.INVISIBLE : View.VISIBLE);
            }
            tvDuration.setText(TimeHelper.secondToHourMinite(Integer.parseInt(data.getDuration())));
            ImageBusiness.setVideoEpisode(ivCover, data.getImagePath());
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener != null) {
            listener.clickEpisode(position, false);
        }
    }
}

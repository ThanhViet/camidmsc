package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 10/4/2017.
 */

public class DialogLoading extends Dialog {
    private BaseSlidingFragmentActivity activity;
    private DismissListener dismissListener;
    private ProgressLoading mPrgressLoading;

    public DialogLoading(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        setCancelable(isCancelable);
    }

    public DialogLoading setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_loading);
        findComponentViews();
    }

    @Override
    public void dismiss() {
        Log.d("DialogEditText", "dismiss");
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    private void findComponentViews() {
        mPrgressLoading = (ProgressLoading) findViewById(R.id.dialog_progress);
    }
}

package com.metfone.selfcare.ui.tabvideo.listener;

import com.metfone.selfcare.common.utils.listener.Listener;
import com.metfone.selfcare.model.tab_video.Video;

/**
 * Created by HoangAnhTuan on 3/26/2018.
 */

public interface OnVideoChangedDataListener extends Listener {
    void onVideoLikeChanged(Video video);

    void onVideoShareChanged(Video video);

    void onVideoCommentChanged(Video video);

    void onVideoSaveChanged(Video video);

    void onVideoWatchLaterChanged(Video video);
}

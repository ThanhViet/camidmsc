package com.metfone.selfcare.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk on 7/20/2014.
 */
public class PopupZodiac extends Dialog {
    private static final String TAG = PopupZodiac.class.getSimpleName();
    private static int CONTENT_MAX_LINE = 5;
    protected Object friendObj;
    protected ApplicationController mApplication;
    protected BaseSlidingFragmentActivity mActivity;
    protected String percent, content;
    protected ZodiacListener mListener;
    // variable
    private AvatarBusiness mAvatarBusiness;
    private Resources mRes;
    private FrameLayout mFraParent, mFraAvatar;
    private LinearLayout mLlButton;
    private CircleImageView mImgMyAvatar, mImgFriendAvatar;
    private TextView mTvwPercent, mTvwContent;
    private ImageView mImgShare, mImgSend;
    private int width, height;
    private float percentTextSize, contentTextSize;
    private InitTypeFaceAsyncTask initTypeFaceAsyncTask;

    public PopupZodiac(BaseSlidingFragmentActivity mActivity, Object friendObj, String percent,
                       String content, ZodiacListener listener) {
        super(mActivity, R.style.DialogFullscreen);
        this.mActivity = mActivity;
        mApplication = (ApplicationController) mActivity.getApplication();
        this.friendObj = friendObj;
        this.percent = percent;
        this.content = content;
        this.mListener = listener;
        this.setCancelable(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_zodiac);
        mFraParent = findViewById(R.id.zodiac_parent_frame);
        mFraAvatar = findViewById(R.id.zodiac_avatar_layout);
        mLlButton = findViewById(R.id.zodiac_button_layout);
        mImgMyAvatar = findViewById(R.id.zodiac_avatar_me);
        mImgFriendAvatar = findViewById(R.id.zodiac_avatar_friend);
        mTvwPercent = findViewById(R.id.zodiac_percent);
        mTvwContent = findViewById(R.id.zodiac_content);
        mImgShare = findViewById(R.id.zodiac_button_share);
        mImgSend = findViewById(R.id.zodiac_button_send);

        String currentLanguage = LocaleManager.getLanguage(mApplication);
        if ("vi".equals(currentLanguage)) {
            mImgShare.setImageResource(R.drawable.ic_share_zodiac);
            mImgSend.setImageResource(R.drawable.ic_send_zodiac);
        } else {
            mImgShare.setImageResource(R.drawable.ic_share_zodiac_en);
            mImgSend.setImageResource(R.drawable.ic_send_zodiac_en);
        }

        setCloseWhenTouchListener(mFraParent);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        drawLayoutParams();
        initEvent();
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                stopInitType();
            }
        });
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        stopInitType();
    }


    private void drawLayoutParams() {
        mAvatarBusiness = mApplication.getAvatarBusiness();
        mRes = mApplication.getResources();
        //int width = mRes.getDimensionPixelOffset(R.dimen.zodiac_width);
        width = (mApplication.getWidthPixels() * 80) / 100;
        Log.d(TAG, "width: " + width);
        height = (width * 4) / 3;
        int avatarFramWidth = (width * 360) / 1000;
        int avatarHeight = (width * 200) / 1000;
        int avatarFramTop = (width * 125) / 1000;// ==text percent height
        int percentHeight = (width * 128) / 1000;
        int percentWidth = (width * 425) / 1000;
        int percentTop = (width * 325) / 1000;
        int contentWidth = (width * 580) / 1000;
        int contentHeight = (width * CONTENT_MAX_LINE * 86) / 1000;
        int contentTop = (width * 465) / 1000;
        int buttonWidth = (width * 840) / 1000;
        int buttonHeight = (width * 185) / 1000;
        percentTextSize = (percentHeight * 75) / 100;
        contentTextSize = ((contentHeight / CONTENT_MAX_LINE) * 78) / 100;
        // set margin
        ViewGroup.LayoutParams parentParams = mFraParent.getLayoutParams();
        FrameLayout.LayoutParams avatarFramParams = (FrameLayout.LayoutParams) mFraAvatar.getLayoutParams();
        FrameLayout.LayoutParams avatarMeParams = (FrameLayout.LayoutParams) mImgMyAvatar.getLayoutParams();
        FrameLayout.LayoutParams avatarFriendParams = (FrameLayout.LayoutParams) mImgFriendAvatar.getLayoutParams();
        FrameLayout.LayoutParams percentParams = (FrameLayout.LayoutParams) mTvwPercent.getLayoutParams();
        FrameLayout.LayoutParams contentParams = (FrameLayout.LayoutParams) mTvwContent.getLayoutParams();
        FrameLayout.LayoutParams buttonLlParams = (FrameLayout.LayoutParams) mLlButton.getLayoutParams();
        parentParams.height = height;
        parentParams.width = width;
        avatarFramParams.width = avatarFramWidth;
        avatarFramParams.height = avatarHeight;
        avatarFramParams.setMargins(0, avatarFramTop, 0, 0);
        avatarMeParams.width = avatarHeight;
        avatarMeParams.height = avatarHeight;
        avatarFriendParams.width = avatarHeight;
        avatarFriendParams.height = avatarHeight;
        percentParams.width = percentWidth;
        percentParams.height = percentHeight;
        percentParams.setMargins(0, percentTop, 0, 0);
        contentParams.width = contentWidth;
        contentParams.height = contentHeight;
        contentParams.setMargins(0, contentTop, 0, 0);
        buttonLlParams.width = buttonWidth;
        buttonLlParams.height = buttonHeight;
        iniTypeFace((float) percentWidth, (float) (contentWidth * CONTENT_MAX_LINE));
    }

    /**
     * init event for controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private void initEvent() {
       /* mTvwPercent.setText(percent);
        mTvwContent.setText(content);
        mTvwPercent.setTextSize(TypedValue.COMPLEX_UNIT_PX, percentTextSize);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, contentTextSize);*/
        //Log.d(TAG, "percentTextSize: " + percentTextSize + " ,contentTextSize: " + contentTextSize);
        mAvatarBusiness.setMyAvatar(mImgMyAvatar, mApplication.getReengAccountBusiness().getCurrentAccount());
        int sizeAvatar = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mAvatarBusiness.displayAvatarContact(friendObj, mImgFriendAvatar, null, sizeAvatar);
       /* DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) mApplication.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        int sizeAvatar = displaymetrics.widthPixels - 40;*/
       /* if (mPhoneNumber instanceof PhoneNumber) {
            mLastChange = ((PhoneNumber) mPhoneNumber).getLastChangeAvatar();
            mJidNumber = ((PhoneNumber) mPhoneNumber).getJidNumber();
            urlAvatar = mApplication.getAvatarBusiness().getAvatarUrl(mLastChange, mJidNumber, sizeAvatar);
        } else if (mPhoneNumber instanceof StrangerPhoneNumber) {// stranger
            urlAvatar = ((StrangerPhoneNumber) mPhoneNumber).getFriendAvatarUrl();
        } else {
            urlAvatar = "";
        }*/
        mImgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onShare(getBitmapFromView());
//                    mListener.onShare(loadBitmapFromView(mFraAvatar));
                }
                stopInitType();
                dismiss();
            }
        });
        mImgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String saveFile = saveLayoutToFile();
                dismiss();
                if (mListener == null) {
                    return;
                }
                if (TextUtils.isEmpty(saveFile)) {
                    mListener.onError();
                } else {
                    mListener.onSend(saveFile);
                }
                stopInitType();
                /*mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                });*/
            }
        });
    }

    private void setCloseWhenTouchListener(View rootView) {
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                stopInitType();
                dismiss();
                return false;
            }
        });
    }

    private String saveLayoutToFile() {
        mFraParent.setBackgroundColor(ContextCompat.getColor(mApplication, R.color.white));
        mLlButton.setVisibility(View.GONE);
        return ImageHelper.getInstance(mApplication).saveViewToImageFile(mFraParent, width, height);
    }

    private Bitmap getBitmapFromView() {
        mFraParent.setBackgroundColor(ContextCompat.getColor(mApplication, R.color.white));
        mLlButton.setVisibility(View.GONE);
        Bitmap scaledBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(scaledBitmap);
        mFraParent.draw(canvas);
        return scaledBitmap;
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    private void stopInitType() {
        if (initTypeFaceAsyncTask != null) {
            initTypeFaceAsyncTask.cancel(true);
            initTypeFaceAsyncTask = null;
        }
    }

    private void iniTypeFace(float percentW, float contentW) {
        if (initTypeFaceAsyncTask != null) {
            initTypeFaceAsyncTask.cancel(true);
            initTypeFaceAsyncTask = null;
        }
        initTypeFaceAsyncTask = new InitTypeFaceAsyncTask(percentW, contentW);
        initTypeFaceAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class InitTypeFaceAsyncTask extends AsyncTask<Void, Void, Void> {
        private Typeface font;
        private float percentWidth, contentWidth;

        public InitTypeFaceAsyncTask(float percentWidth, float contentWidth) {
            this.percentWidth = percentWidth;
            this.contentWidth = contentWidth;
        }

        @Override
        protected Void doInBackground(Void... params) {
            long t = System.currentTimeMillis();
            font = Typeface.createFromAsset(mApplication.getAssets(), "fonts/iCiel_Alina.otf");
//            font = Typeface.createFromAsset(mApplication.getAssets(), "fonts/iCiel_Rukola.ttf");
            percentTextSize = calculatorTextSize(percent, font, percentWidth, percentTextSize);
            contentTextSize = calculatorTextSize(content, font, contentWidth, contentTextSize);
            Log.d(TAG, "init calculatorTextSize take: " + (System.currentTimeMillis() - t));
            Log.d(TAG, "percentTextSize after: " + percentTextSize + " , contentTextSize: " + contentTextSize);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mTvwPercent.setTypeface(font);
            mTvwContent.setTypeface(font);
            mTvwPercent.setText(percent);
            mTvwContent.setText(content);
            mTvwPercent.setTextSize(TypedValue.COMPLEX_UNIT_PX, percentTextSize);
            mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, contentTextSize);
        }
    }

    private Paint p = new Paint();

    private float calculatorTextSize(String input, Typeface typeface, float maxWidth, float maxSize) {
        p.setTypeface(typeface);
        float textSize = maxSize;
        p.setTextSize(textSize);
        while (textSize > 24f && p.measureText(input) > maxWidth) {
            textSize = textSize - 3.0f;
            p.setTextSize(textSize);
        }
        return textSize;
    }

    public interface ZodiacListener {
        //void onShare(String filePath);
        void onShare(Bitmap bitmap);

        void onSend(String filePath);

        void onError();
    }
}
package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.BPlusResult;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;


public class DialogTransferBankPlus extends Dialog implements View.OnClickListener, TextWatcher {
    private static final String TAG = DialogTransferBankPlus.class.getSimpleName();
    public static final int TAB_PAY = 1;
    public static final int TAB_CLAIM = 2;
    private BaseSlidingFragmentActivity activity;
    private Resources mRes;
    private String friendJid;
    private String friendName;
    private NegativeListener<String> negativeListener;
    private PositiveListener<BPlusResult> positiveListener;
    private DismissListener dismissListener;
    private int currentTab = TAB_PAY;

    private TextView mAbCancel;
    private ImageView mAbSend;
    private TextView mTabPay, mTabClaim, mTvwFriendName, mTvwGuide, mTvwReveivedLabel;
    private CircleImageView mImgAvatar;
    private EditText mEdtInputAmount, mEdtInputDesc;

    public DialogTransferBankPlus(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        this.currentTab = TAB_PAY;
        this.mRes = activity.getResources();
        setCancelable(isCancelable);
    }

    public DialogTransferBankPlus setFriendJid(String friendJid) {
        this.friendJid = friendJid;
        return this;
    }

    public DialogTransferBankPlus setFriendName(String friendName) {
        this.friendName = friendName;
        return this;
    }

    public DialogTransferBankPlus setNegativeListener(NegativeListener listener) {
        this.negativeListener = listener;
        return this;
    }

    public DialogTransferBankPlus setPositiveListener(PositiveListener<BPlusResult> listener) {
        this.positiveListener = listener;
        return this;
    }

    public DialogTransferBankPlus setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_transfer_bank_plus);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        //changeStatusBar(R.color.transparent);
        findComponentViews();
        drawDetail();
        setListener();
    }

    @Override
    public void dismiss() {
        Log.d("DialogTransferBankPlus", "dismiss");
        InputMethodUtils.hideSoftKeyboard(mEdtInputAmount, activity);
        InputMethodUtils.hideSoftKeyboard(mEdtInputDesc, activity);
        InputMethodUtils.hideSoftKeyboard(activity);
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.transfer_bplus_cancel:
                if (negativeListener != null) {
                    negativeListener.onNegative(null);
                }
                dismiss();
                break;
            case R.id.transfer_bplus_send:
                if (positiveListener != null) {
                    String amoutStr = mEdtInputAmount.getText().toString().trim();
                    long amount = TextHelper.convertTextDecemberToLong(amoutStr, 0);
                    String desc = mEdtInputDesc.getText().toString().trim();
                    if (amount > 0 && !TextUtils.isEmpty(desc)) {
                        String transferId = String.valueOf(TimeHelper.getCurrentTime());//TODO gen  id khi tạo được thông tin chuyển
                        BPlusResult result = new BPlusResult(friendJid, friendName, amount, amoutStr, desc, transferId, currentTab);
                        positiveListener.onPositive(result);
                        dismiss();
                    } else {
                        activity.showToast(R.string.bplus_input_valid);
                    }
                } else {
                    dismiss();
                }
                break;
            case R.id.transfer_bplus_tab_pay:
                currentTab = TAB_PAY;
                drawStateTab();
                break;
            case R.id.transfer_bplus_tab_claim:
                currentTab = TAB_CLAIM;
                drawStateTab();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //enablePositiveButton();
    }

    @Override
    public void afterTextChanged(Editable s) {
        mEdtInputAmount.removeTextChangedListener(this);
        try {
            String input = s.toString().trim();
            int cp = mEdtInputAmount.getSelectionStart();
            int inLen = input.length();
            String output = TextHelper.formatTextDecember(input);
            mEdtInputAmount.setText(output);
            int outLen = mEdtInputAmount.getText().length();
            int sel = (cp + (outLen - inLen));
            if (sel > 0 && sel <= outLen) {
                mEdtInputAmount.setSelection(sel);
            } else {
                mEdtInputAmount.setSelection(outLen - 1);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        mEdtInputAmount.addTextChangedListener(this);
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private void findComponentViews() {
        View rootView = findViewById(R.id.transfer_bplus_parent);
        //View fakeStatus = findViewById(R.id.transfer_bplus_fake_statusbar);
        mAbCancel = (TextView) findViewById(R.id.transfer_bplus_cancel);
        mAbSend = (ImageView) findViewById(R.id.transfer_bplus_send);
        mTabPay = (TextView) findViewById(R.id.transfer_bplus_tab_pay);
        mTabClaim = (TextView) findViewById(R.id.transfer_bplus_tab_claim);
        mImgAvatar = (CircleImageView) findViewById(R.id.transfer_bplus_friend_avatar);
        mTvwFriendName = (TextView) findViewById(R.id.transfer_bplus_friend_name);
        mEdtInputAmount = (EditText) findViewById(R.id.transfer_bplus_input_amount);
        mEdtInputDesc = (EditText) findViewById(R.id.transfer_bplus_input_desc);
        mTvwGuide = (TextView) findViewById(R.id.transfer_bplus_text_guide);
        mTvwReveivedLabel = (TextView) findViewById(R.id.transfer_bplus_received_text);
        //InputMethodUtils.hideKeyboardWhenTouch(rootView, activity);
        /*if (Version.hasN()) {
            fakeStatus.setVisibility(View.GONE);
        } else {
            fakeStatus.setVisibility(View.VISIBLE);
        }*/
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodUtils.hideSoftKeyboard(activity);
                return false;
            }
        });
    }

    private void setListener() {
        mAbCancel.setOnClickListener(this);
        mAbSend.setOnClickListener(this);
        mTabPay.setOnClickListener(this);
        mTabClaim.setOnClickListener(this);
        mEdtInputAmount.addTextChangedListener(this);
    }

    private void drawDetail() {
        mTvwFriendName.setText(friendName);
        ApplicationController application = (ApplicationController) activity.getApplicationContext();
        int size = (int) application.getResources().getDimension(R.dimen.avatar_small_size);
        PhoneNumber phoneNumber = application.getContactBusiness().getPhoneNumberFromNumber(friendJid);
        if (phoneNumber != null) {
            application.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, null, phoneNumber, size);
        } else {
            application.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatar, null, friendJid, size);
        }
        drawStateTab();
        //InputMethodUtils.showSoftKeyboard(activity, mEdtInputAmount);
    }

    private void drawStateTab() {
        if (currentTab == TAB_PAY) {
            mTabPay.setTextColor(ContextCompat.getColor(activity, R.color.bg_mocha));
            mTabClaim.setTextColor(ContextCompat.getColor(activity, R.color.text_ab_desc));
            mEdtInputDesc.setHint(mRes.getString(R.string.bplus_hint_desc_pay));
            mTvwGuide.setText(mRes.getString(R.string.bplus_guide_pay));
            mTvwReveivedLabel.setText(mRes.getString(R.string.bplus_receiver_pay));
        } else {
            mTabPay.setTextColor(ContextCompat.getColor(activity, R.color.text_ab_desc));
            mTabClaim.setTextColor(ContextCompat.getColor(activity, R.color.bg_mocha));
            mEdtInputDesc.setHint(mRes.getString(R.string.bplus_hint_desc_claim));
            mTvwGuide.setText(mRes.getString(R.string.bplus_guide_claim));
            mTvwReveivedLabel.setText(mRes.getString(R.string.bplus_receiver_claim));
        }
    }

    private void changeStatusBar(int colorRes) {
        //code works on android 4.4.4 & later
        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            //set color
            // clear FLAG_TRANSLUCENT_STATUS flag:
            if (window != null) {
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                if (Build.VERSION.SDK_INT >= 21) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(ContextCompat.getColor(getContext(), colorRes));
                }
            }
        }
    }
}
package com.metfone.selfcare.ui.dialog;

/**
 * Created by toanvk2 on 7/13/2017.
 */

public interface PositiveListener<T> {
    void onPositive(T result);
}

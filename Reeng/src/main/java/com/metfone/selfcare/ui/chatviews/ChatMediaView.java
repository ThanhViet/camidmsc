package com.metfone.selfcare.ui.chatviews;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.ImageAndVideoPagerAdapter;
import com.metfone.selfcare.adapter.MediaPreviewAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ThanhNT72 on 5/30/2015.
 */
public class ChatMediaView {
    private String TAG = ChatMediaView.class.getSimpleName();
    //    private HorizontalListView mListview;
    private MediaPreviewAdapter mAdapter;
    private ArrayList<ImageInfo> arrayListMedia;
    private ArrayList<ImageInfo> arrayListImage;
    private ArrayList<ImageInfo> arrayListVideo;
    private Activity activity;
    private ImageView mImgPreviewAll;
    private View mediaView;
    private Handler mHandle;
    private OnMediaListener listener;
    private ViewPager mViewPagerMedia;
    private ImageAndVideoPagerAdapter mImageAndVideoPagerAdapter;

    public ChatMediaView(BaseSlidingFragmentActivity activity, View parentLayout, OnMediaListener listener) {
        this.activity = activity;
        mediaView = parentLayout;
        this.listener = listener;
        arrayListMedia = new ArrayList<>();
        arrayListImage = new ArrayList<>();
        arrayListVideo = new ArrayList<>();
        mHandle = new Handler();
//        mListview = (HorizontalListView) parentLayout.findViewById(R.id.listview_horizon);
        mImgPreviewAll = (ImageView) parentLayout.findViewById(R.id.img_preview_all);
        mViewPagerMedia = (ViewPager) parentLayout.findViewById(R.id.chat_detail_image_and_video_pager);
        initData();
        setListener();
    }

    private void setListener() {
        mImgPreviewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onOpenPreviewMedia();
                }
            }
        });
        mViewPagerMedia.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                Log.i(TAG, "onPageSelected " + i); //here
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    public void onResume() {
        Log.i(TAG, "onResume");
        if (mImageAndVideoPagerAdapter != null) {
            mImageAndVideoPagerAdapter.notifyDataSetChanged();
        } else {
            initData();
        }
    }

    private void initData() {
        mHandle.post(new Runnable() {
            @Override
            public void run() {
                long beginTime = System.currentTimeMillis();
                getImages(activity);
                arrayListMedia.addAll(arrayListImage);
                if (Config.Server.SEND_VIDEO_ENABLE) {
                    getVideos(activity);
                    arrayListMedia.addAll(arrayListVideo);
                }
                Collections.sort(arrayListMedia);
                Log.i(TAG, "get images and video take " + (System.currentTimeMillis() - beginTime) + " ms");
                mImageAndVideoPagerAdapter = new ImageAndVideoPagerAdapter(activity, arrayListMedia, listener);
                mViewPagerMedia.setAdapter(mImageAndVideoPagerAdapter);
                mViewPagerMedia.setOffscreenPageLimit(0); //ko an
            }
        });
    }

    public void notifyChangePageAdapter() {
        if (mImageAndVideoPagerAdapter != null) {
            mImageAndVideoPagerAdapter.notifyDataSetChanged();
        }
    }

    public void getImages(Context context) {
        Cursor cursor = null;
        try {
            String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_TAKEN,
                    MediaStore.Images.Media._ID,};
            String orderBy = MediaStore.Images.Media.DATE_TAKEN + " DESC";
            cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    columns,
                    null,
                    null,
                    orderBy);
            if (cursor == null) return;
            if (cursor.moveToFirst()) {
                final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                final int dateColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_TAKEN);
                do {
                    String data = cursor.getString(dataColumn);
                    long date = cursor.getLong(dateColumn);
                    ImageInfo mediaFile = new ImageInfo(data, date);
                    arrayListImage.add(mediaFile);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void getVideos(Context context) {
        final String[] columns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media.SIZE,
                MediaStore.Video.Media.DURATION, MediaStore.Video.Media.DATE_ADDED, MediaStore.Video.Media.RESOLUTION,
                MediaStore.Video.Media._ID,};
        final String orderBy = MediaStore.Video.Media.DATE_ADDED + " DESC";
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                columns,
                null,
                null,
                orderBy);

        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            final int dateColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED);
            int sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE);
            int durationColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION);
            int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            int resColum = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.RESOLUTION);

            do {
                String data = cursor.getString(dataColumn);
                long date = cursor.getLong(dateColumn);
                long size = cursor.getLong(sizeColumn);
                long duration = cursor.getLong(durationColumn);
                int durationInSecond = Math.round(duration / 1000.0f);
                int id = cursor.getInt(idColumn);
                String videoContentURI = "content://media/external/video/media/" + id;
                String res = cursor.getString(resColum);
                ImageInfo mediaFile = new ImageInfo(data, size, durationInSecond, videoContentURI, date, res);
                Log.i("----------", mediaFile.toString());

//                String extension = mediaFile.getImagePath().substring(mediaFile.getImagePath().lastIndexOf("."));
//                if(extension == Constants.FILE.GP_FILE_SUFFIX || extension == Constants.FILE.MP4_FILE_SUFFIX){
//                    arrayListVideo.add(mediaFile);
//                }

                if (mediaFile.getFileSize() > 0) arrayListVideo.add(mediaFile);
            } while (cursor.moveToNext());
        }
        cursor.close();
        Log.i("----------", "size: " + arrayListVideo.size());
    }


    public interface OnMediaListener {
        void onSendImageMessage(String filePath);

        void onOpenPreviewMedia();

        void onSendVideo(ImageInfo imageInfo);
    }
}

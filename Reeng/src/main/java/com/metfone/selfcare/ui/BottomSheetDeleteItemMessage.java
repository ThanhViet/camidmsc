package com.metfone.selfcare.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

public class BottomSheetDeleteItemMessage extends BottomSheetDialogFragment {
    private View.OnClickListener mClickDeleteForMe;
    private View.OnClickListener mClickDeleteForEveryone;
    private Context mContext;
    private LinearLayout mDeleteForMe;
    private LinearLayout mDeleteForEveryone;
    private static BottomSheetDialog bottomSheetDialog;
    private View view;

    public BottomSheetDeleteItemMessage(Context context,View.OnClickListener onClickDeleteForMe, View.OnClickListener onClickDeleteForEveryone){
        this.mContext=context;
        this.mClickDeleteForMe = onClickDeleteForMe;
        this.mClickDeleteForEveryone =onClickDeleteForEveryone;
        view = LayoutInflater.from(mContext).inflate(R.layout.bottom_sheet_delete_item_message, null, false);
        mDeleteForMe = view.findViewById(R.id.Ll_delete_for_me);
        mDeleteForEveryone = view.findViewById(R.id.Ll_delete_for_everyone);
        mDeleteForMe.setOnClickListener(mClickDeleteForMe);
        mDeleteForEveryone.setOnClickListener(mClickDeleteForEveryone);
        bottomSheetDialog = new BottomSheetContextMenu.CustomBottomSheetDialog(mContext, R.style.BottomSheetDialogTheme) {
            @Override
            public void dismiss() {
                super.dismiss();

            }
        };

        bottomSheetDialog.setContentView(view);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = LayoutInflater.from(mContext).inflate(R.layout.bottom_sheet_delete_item_message, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setGravity(Gravity.BOTTOM);
        }

        return view;
    }
    public void show() {
        if (bottomSheetDialog == null) return;
        bottomSheetDialog.show();
    }
    public static void dismisss() {
        bottomSheetDialog.dismiss();
    }
}

package com.metfone.selfcare.ui.tabvideo.holder;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TopChannelDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.iv_channel)
    CircleImageView ivChannel;
    @BindView(R.id.v_status)
    View vStatus;
    @BindView(R.id.tv_channel)
    TextView tvChannel;

    private Channel currentChannel;
    private OnChannelListener onItemChannelListener;

    public TopChannelDetailHolder(@NonNull LayoutInflater layoutInflater, @NonNull ViewGroup parent, OnChannelListener onChannelListener) {
        super(layoutInflater.inflate(R.layout.holder_top_channel_detail, parent, false));
        ButterKnife.bind(this, itemView);
        this.onItemChannelListener = onChannelListener;
    }

    public void bindData(Channel itemChannel) {
        currentChannel = itemChannel;
        tvChannel.setText(itemChannel.getName());
//        ImageManager.showImage(itemChannel.getUrlImage(), ivChannel);
        ImageBusiness.setAvatarChannel(ivChannel, itemChannel.getUrlImage());
        bindStatus();
    }

    private void bindStatus() {
        if (currentChannel != null)
            if (currentChannel.isHaveNewVideo())
                vStatus.setVisibility(View.VISIBLE);
            else
                vStatus.setVisibility(View.GONE);
    }

    @OnClick({R.id.root_item})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.root_item:
                currentChannel.setHaveNewVideo(false);
                bindStatus();
                ChannelDataSource.getInstance().saveTimeNewChannel(currentChannel);

                if (onItemChannelListener != null)
                    onItemChannelListener.onClick(currentChannel);
                break;
        }
    }

}
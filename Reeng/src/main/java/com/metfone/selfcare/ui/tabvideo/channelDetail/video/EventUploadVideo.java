package com.metfone.selfcare.ui.tabvideo.channelDetail.video;

public class EventUploadVideo {
    private boolean showUpload;

    public EventUploadVideo(boolean showUpload) {
        this.showUpload = showUpload;
    }

    public boolean isShowUpload() {
        return showUpload;
    }

    public void setShowUpload(boolean showUpload) {
        this.showUpload = showUpload;
    }
}

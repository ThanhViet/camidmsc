package com.metfone.selfcare.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.database.model.MediaModel;

/**
 * Created by tungt on 3/18/2016.
 */
public class UploadSongPopupFragment extends DialogFragment {
    private static final String TAG = UploadSongPopupFragment.class.getSimpleName();
    private static final String STR_PERCENT = "%d";
    private Context context;
    private Button mBtnYes, mBtnNo;
    private TextView mTvwContent, mTvPercent, mTvStatus;
    private ProgressBar mProgressBar;
    protected String contentString;
    private OnClick mClickListener;
    //private LocalSongInfo mSongInfo;
    private MediaModel resultSongModel;
    private boolean dismiss;

    public UploadSongPopupFragment() {
    }

    public static UploadSongPopupFragment newInstance(String content, boolean dismiss, OnClick listener, LocalSongInfo song) {
        UploadSongPopupFragment fragment = new UploadSongPopupFragment();
        fragment.contentString = content;
        fragment.mClickListener = listener;
        //fragment.mSongInfo = song;
        fragment.dismiss = dismiss;
        fragment.setCancelable(false);
        return fragment;
    }

    public static UploadSongPopupFragment newInstance(String content, OnClick listener, LocalSongInfo song) {
        return newInstance(content, true, listener, song);
    }

    public void setResultSongModel(MediaModel songModel) {
        this.resultSongModel = songModel;
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = initView(inflater, container);
        initListener();
        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(false);
        }
        return v;
    }

    private void initListener() {
        mBtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onBtnNoClicked();
                }
                if (dismiss) dismiss();
            }
        });
        mBtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onBtnYesClicked(resultSongModel);
                }
                if (dismiss) dismiss();
            }
        });
    }

    private View initView(LayoutInflater inflater, ViewGroup container) {
        View v = inflater.inflate(R.layout.popup_fragment_upload_song, container, false);
        mTvwContent = v.findViewById(R.id.tv_title);
        mBtnYes = v.findViewById(R.id.btn_yes);
        mBtnNo = v.findViewById(R.id.btn_no);
        mBtnNo.setVisibility(View.VISIBLE);
        mBtnYes.setVisibility(View.GONE);
        mTvPercent = v.findViewById(R.id.tv_percent);
        mTvStatus = v.findViewById(R.id.tv_status);
        mProgressBar = v.findViewById(R.id.progress_bar);
        mProgressBar.setMax(100);
        mTvwContent.setText(contentString);
        return v;
    }

    @SuppressLint("SetTextI18n")
    public void setPercent(int percent) {
        if (percent >= 100) percent = 99;// upload xong data cho response
        if (mProgressBar != null && mTvPercent != null) {
            mProgressBar.setProgress(percent);
            mTvPercent.setText(" (" + percent + "%" + ")");
        }
        if (mTvStatus != null && mTvStatus.getVisibility() != View.GONE)
            mTvStatus.setVisibility(View.GONE);
    }

    public void setComplete() {
        mProgressBar.setProgress(100);
        mBtnYes.setVisibility(View.VISIBLE);
//        setStatus(R.string.text_upload_sucess);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void setStatus(int id) {
        if (mTvStatus != null && context != null) {
            mTvStatus.setVisibility(View.VISIBLE);
            mTvStatus.setText(context.getResources().getString(id));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public interface OnClick {
        void onBtnYesClicked(MediaModel songModel);

        void onBtnNoClicked();
    }
}
package com.metfone.selfcare.ui.tabvideo.holder;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.OnClick;

public class VideoHolder extends BaseAdapterV2.ViewHolder {

    @BindView(R.id.iv_video)
    ImageView ivVideo;
    @BindView(R.id.iv_play)
    ImageView ivPlay;
    @BindView(R.id.iv_watch_later)
    ImageView ivWatchLater;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.frame_video)
    RelativeLayout frameVideo;
    @BindView(R.id.iv_like)
    ImageView ivLike;
    @BindView(R.id.tv_number_like)
    TextView tvNumberLike;
    @BindView(R.id.iv_comment)
    ImageView ivComment;
    @BindView(R.id.tv_number_comment)
    TextView tvNumberComment;
    @BindView(R.id.iv_share)
    ImageView ivShare;
    @BindView(R.id.tv_number_share)
    TextView tvNumberShare;
    @BindView(R.id.tv_channel)
    TextView tvChannel;
    @BindView(R.id.iv_channel)
    ImageView ivChannel;
    @BindView(R.id.root_item_video)
    LinearLayout rootItemVideo;
    @BindView(R.id.iv_live_stream)
    @Nullable
    View ivLiveStream;

    private @NonNull
    BaseSlidingFragmentActivity mActivity;
    private @Nullable
    Video mVideo;
    private @Nullable
    Channel mChannel;
    private @Nullable
    OnVideoListener mOnVideoListener;

    public VideoHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
        super(layoutInflater.inflate(R.layout.item_video, parent, false));
        mActivity = (BaseSlidingFragmentActivity) activity;
        ViewGroup.LayoutParams layoutParams = ivVideo.getLayoutParams();
        layoutParams.width = ScreenManager.getWidth(activity);
        layoutParams.height = ScreenManager.getWidth(activity) * 9 / 16;
        ivVideo.setLayoutParams(layoutParams);
        ivPlay.setVisibility(View.GONE);
    }

    public void setOnVideoListener(@Nullable OnVideoListener onVideoListener) {
        mOnVideoListener = onVideoListener;
    }

    @Override
    public void bindData(ArrayList<Object> items, int position, @NonNull List<Object> payloads) {
        super.bindData(items, position, payloads);

        bindLike();
        bindShare();
        bindComment();
        bindWatchLate();
    }

    @Override
    public void bindData(ArrayList<Object> items, int position) {
        super.bindData(items, position);
        Object item = items.get(position);
        if (item instanceof Video) {
            mVideo = (Video) item;
            mChannel = mVideo.getChannel();

            bindVideo();
            bindChannel();
        }
    }

    private void bindVideo() {
        if (mVideo != null) {

            bindLike();
            bindShare();
            bindComment();
            bindWatchLate();

            tvTitle.setText(mVideo.getTitle());
            ImageManager.showImageNormalV2(mVideo.getImagePath(), mVideo.getImage_path_small(), ivVideo);
            if (ivLiveStream != null)
                ivLiveStream.setVisibility(mVideo.isLive() ? View.VISIBLE : View.GONE);
        }
    }

    private void bindWatchLate() {
        if (mVideo != null) {
            ivWatchLater.setImageResource(mVideo.isWatchLater() ? R.drawable.ic_tab_video_watch_later_press : R.drawable.ic_tab_video_watch_later);
        }
    }

    private void bindShare() {
        if (mVideo != null) {
            ExecutorService executor = Executors.newFixedThreadPool(5);
            asyncSetNumber(tvNumberShare, new Handler(), mVideo.getTotalShare(), executor);
            executor.shutdown();
        }
    }

    private void bindComment() {
        if (mVideo != null) {
            ExecutorService executor = Executors.newFixedThreadPool(5);
            asyncSetNumber(tvNumberComment, new Handler(), mVideo.getTotalComment(), executor);
            executor.shutdown();
        }
    }

    private void bindLike() {
        if (mVideo != null) {
            ivLike.setImageResource(mVideo.isLike() ? R.drawable.ic_video_item_video_hear_press : R.drawable.ic_video_item_video_hear);
            ExecutorService executor = Executors.newFixedThreadPool(5);
            asyncSetNumber(tvNumberLike, new Handler(), mVideo.getTotalLike(), executor);
            executor.shutdown();
        }
    }

    private void bindChannel() {
        if (mChannel != null) {
            tvChannel.setText(mChannel.getName());
            ImageManager.showImageCircleV2(mChannel.getUrlImage(), ivChannel);
        }
    }

    private void asyncSetNumber(TextView textView, final Handler handler, final long numberView, Executor executor) {
        final WeakReference<TextView> textViewRef = new WeakReference<>(textView);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                TextView textView = textViewRef.get();
                if (textView == null) return;
                Context context = textView.getContext();
                if (context == null) return;

                final String numberStr = Utilities.shortenLongNumber(numberView);

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        TextView textView = textViewRef.get();
                        if (textView == null) return;
                        textView.setText(numberStr);
                    }
                });
            }
        });
    }

    @OnClick(R.id.iv_watch_later)
    public void onIvWatchLaterClicked() {
        if (mVideo != null && mOnVideoListener != null) {
            mVideo.setWatchLater(!mVideo.isWatchLater());
            mOnVideoListener.onWatchLate(mVideo);
            bindWatchLate();
        }
    }

    @OnClick(R.id.iv_like)
    public void onIvLikeClicked() {
        like();
    }

    @OnClick(R.id.tv_number_like)
    public void onTvNumberLikeClicked() {
        like();
    }

    @OnClick(R.id.iv_comment)
    public void onIvCommentClicked() {
        comment();
    }

    @OnClick(R.id.tv_number_comment)
    public void onTvNumberCommentClicked() {
        comment();
    }

    @OnClick(R.id.iv_share)
    public void onIvShareClicked() {
        share();
    }

    @OnClick(R.id.tv_number_share)
    public void onTvNumberShareClicked() {
        share();
    }

    @OnClick(R.id.tv_channel)
    public void onTvChannelClicked() {
        openChannel();
    }

    @OnClick(R.id.iv_channel)
    public void onIvChannelClicked() {
        openChannel();
    }

    @OnClick(R.id.root_item_video)
    public void onRootItemVideoClicked() {
        if (mVideo == null) return;
        if (mOnVideoListener != null) {
            mOnVideoListener.onClick(mVideo);
        }
    }

    private void like() {
        if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
            mActivity.showDialogLogin();
        else {
            if (mVideo == null) return;
            mVideo.setLike(!mVideo.isLike());
            mVideo.setTotalLike(mVideo.isLike() ? mVideo.getTotalLike() + 1 : mVideo.getTotalLike() - 1);
            bindLike();
            if (mOnVideoListener != null) {
                mOnVideoListener.onLike(mVideo);
            }
        }
    }

    private void share() {
        if (mVideo == null) return;
        if (mOnVideoListener != null) {
            mOnVideoListener.onShare(mVideo);
        }
    }

    private void comment() {
        if (mVideo == null) return;
        if (mOnVideoListener != null) {
            mOnVideoListener.onComment(mVideo);
        }
    }

    private void openChannel() {
        if (mVideo == null) return;
        Channel channel = mVideo.getChannel();
        if (channel == null) return;
        if (mOnVideoListener != null) {
            mOnVideoListener.onOpenChannel(channel);
        }
    }

    public interface OnVideoListener {

        void onClick(Video video);

        void onLike(Video video);

        void onShare(Video video);

        void onComment(Video video);

        void onWatchLate(Video video);

        void onOpenChannel(Channel channel);
    }
}

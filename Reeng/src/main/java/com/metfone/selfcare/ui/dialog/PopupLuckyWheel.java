package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 9/6/2016.
 */
public class PopupLuckyWheel extends Dialog implements View.OnClickListener {
    private static final String TAG = PopupLuckyWheel.class.getSimpleName();
    private ApplicationController mApplication;
    private BaseSlidingFragmentActivity mActivity;
    private NegativeListener mNegativeListener;
    private NeutralListener mNeutralListener;
    private PositiveListener mPositiveListener;
    private String iconUrl, title, desc, negative, neutral, positive;
    private int iconDrawableId = -1;
    // variable
    private Button mBtnNegative, mBtnNeutral, mBtnPositive;
    private TextView mTvwTitle, mTvwDesc;
    private ImageView mImgHeader, mImgClose;
    private boolean isHideClose = false;
    private CloseListener mCloseListener;

    public PopupLuckyWheel(BaseSlidingFragmentActivity activity) {
        super(activity, R.style.DialogFullscreen);
        this.mActivity = activity;
        this.mApplication = (ApplicationController) mActivity.getApplication();
        this.isHideClose = false;
        this.setCancelable(false);
    }

    public PopupLuckyWheel(BaseSlidingFragmentActivity activity, boolean isHideClose) {
        super(activity, R.style.DialogFullscreen);
        this.mActivity = activity;
        this.mApplication = (ApplicationController) mActivity.getApplication();
        this.isHideClose = isHideClose;
        this.setCancelable(false);
    }

    public PopupLuckyWheel setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
        return this;
    }

    public PopupLuckyWheel setIconResource(int drawableId) {
        this.iconDrawableId = drawableId;
        return this;
    }

    public PopupLuckyWheel setTitle(String title) {
        this.title = title;
        return this;
    }

    public PopupLuckyWheel setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public PopupLuckyWheel setNegative(String label, NegativeListener listener) {
        this.negative = label;
        this.mNegativeListener = listener;
        return this;
    }

    public PopupLuckyWheel setNeutral(String label, NeutralListener listener) {
        this.neutral = label;
        this.mNeutralListener = listener;
        return this;
    }

    public PopupLuckyWheel setPositive(String label, PositiveListener listener) {
        this.positive = label;
        this.mPositiveListener = listener;
        return this;
    }

    public PopupLuckyWheel setClose(CloseListener listener) {
        this.mCloseListener = listener;
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_lucky_wheel);
        findComponentView();
        drawDetail();
        setListener();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        dismiss();
        switch (v.getId()) {
            case R.id.popup_lw_negative_button:
                mNegativeListener.onNegative();
                break;
            case R.id.popup_lw_neutral_button:
                mNeutralListener.onNeutral();
                break;
            case R.id.popup_lw_positive_button:
                mPositiveListener.onPositive();
                break;
            case R.id.popup_lw_ic_close:
                if (mCloseListener != null)
                    mCloseListener.onClose();
                this.dismiss();
                break;
            default:
                break;
        }
    }

    private void findComponentView() {
        mImgClose = findViewById(R.id.popup_lw_ic_close);
        mImgHeader = findViewById(R.id.popup_lw_header_image);
        mTvwTitle = findViewById(R.id.popup_lw_title);
        mTvwDesc = findViewById(R.id.popup_lw_desc);
        mBtnNegative = findViewById(R.id.popup_lw_negative_button);
        mBtnNeutral = findViewById(R.id.popup_lw_neutral_button);
        mBtnPositive = findViewById(R.id.popup_lw_positive_button);
    }

    private void setListener() {
        mBtnNegative.setOnClickListener(this);
        mBtnNeutral.setOnClickListener(this);
        mBtnPositive.setOnClickListener(this);
        mImgClose.setOnClickListener(this);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //TODO something
            }
        });
    }

    private void drawDetail() {
        if (TextUtils.isEmpty(desc)) desc = " ";
        if (isHideClose) {
            mImgClose.setVisibility(View.GONE);
        } else {
            mImgClose.setVisibility(View.VISIBLE);
        }
        if (iconDrawableId != -1) {
            mImgHeader.setVisibility(View.VISIBLE);
            mImgHeader.setImageDrawable(ContextCompat.getDrawable(mApplication, iconDrawableId));
            ImageLoaderManager.getInstance(mApplication).cancelDisplayTag(mImgHeader);
        } else if (TextUtils.isEmpty(iconUrl)) {
            mImgHeader.setVisibility(View.GONE);
            ImageLoaderManager.getInstance(mApplication).cancelDisplayTag(mImgHeader);
        } else {
            mImgHeader.setVisibility(View.VISIBLE);
            ImageLoaderManager.getInstance(mApplication).displayLuckyWheelIcon(mImgHeader, iconUrl);
        }
        if (TextUtils.isEmpty(title)) {
            mTvwTitle.setVisibility(View.GONE);
            mTvwDesc.setTextColor(ContextCompat.getColor(mApplication, R.color.text_black));
        } else {
            mTvwTitle.setVisibility(View.VISIBLE);
            mTvwTitle.setText(title);
            mTvwDesc.setTextColor(ContextCompat.getColor(mApplication, R.color.text_gray));
        }
        mTvwDesc.setText(desc);
        if (TextUtils.isEmpty(negative)) {
            mBtnNegative.setVisibility(View.GONE);
        } else {
            mBtnNegative.setVisibility(View.VISIBLE);
            mBtnNegative.setText(negative);
        }
        if (TextUtils.isEmpty(neutral)) {
            mBtnNeutral.setVisibility(View.GONE);
        } else {
            mBtnNeutral.setVisibility(View.VISIBLE);
            mBtnNeutral.setText(neutral);
        }
        mBtnPositive.setText(positive);
    }

    public interface NegativeListener {
        void onNegative();
    }

    public interface NeutralListener {
        void onNeutral();
    }

    public interface PositiveListener {
        void onPositive();
    }

    public interface CloseListener {
        void onClose();
    }
}
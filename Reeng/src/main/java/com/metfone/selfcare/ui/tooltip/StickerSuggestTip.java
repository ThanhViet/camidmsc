package com.metfone.selfcare.ui.tooltip;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;

import com.metfone.selfcare.adapter.EmoticonsGridAdapter;
import com.metfone.selfcare.database.model.StickerItem;

import java.util.ArrayList;

public class StickerSuggestTip {

    public enum AnimationType {
        FROM_MASTER_VIEW,
        FROM_TOP,
        NONE
    }

    private CharSequence mText;
    private int mTextResId;
    private int mColor;
    private int mTextColor;
    private View mContentView;
    private Context mContext;
    private EmoticonsGridAdapter.KeyClickListener mKeyclickListener;

    private AnimationType mAnimationType;
    private boolean mShouldShowShadow;
    private Typeface mTypeface;
    private ArrayList<StickerItem> mStikerList;

    /**
     * Creates a new StickerSuggestTip without any values.
     */
    public StickerSuggestTip() {
        mText = null;
        mTypeface = null;
        mTextResId = 0;
        mColor = 0;
        mContentView = null;
        mStikerList = null;

        mAnimationType = AnimationType.NONE;
    }

    public StickerSuggestTip withStikerList(final ArrayList<StickerItem> list){
        mStikerList = new ArrayList<>(list);
        return this;
    }

    public StickerSuggestTip withContext(final Context context){
        mContext = context;
        return this;
    }

    public StickerSuggestTip withListener(final EmoticonsGridAdapter.KeyClickListener listener){
        mKeyclickListener = listener;
        return this;
    }

    /**
     * Set the text to show. Has no effect when a content View is set using setContentView().
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withText(final CharSequence text) {
        mText = text;
        mTextResId = 0;
        return this;
    }

    /**
     * Set the text resource id to show. Has no effect when a content View is set using setContentView().
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withText(final int resId) {
        mTextResId = resId;
        mText = null;
        return this;
    }

    /**
     * Set the text resource id to show and the custom typeface for that view. Has no effect when a content View is set using setContentView().
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withText(final int resId, final Typeface tf) {
        mTextResId = resId;
        mText = null;
        withTypeface(tf);
        return this;
    }

    /**
     * Set the color of the StickerSuggestTip. Default is white.
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withColor(final int color) {
        mColor = color;
        return this;
    }

    /**
     * Set the text color of the StickerSuggestTip. Default is white.
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withTextColor(final int color) {
        mTextColor = color;
        return this;
    }

    /**
     * Set a custom content View for the StickerSuggestTip. This will cause any text that has been set to be ignored.
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withContentView(final View view) {
        mContentView = view;
        return this;
    }

    /**
     * Set the animation type for the StickerSuggestTip. Defaults to {@link AnimationType#FROM_MASTER_VIEW}.
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withAnimationType(final AnimationType animationType) {
        mAnimationType = animationType;
        return this;
    }

    /**
     * Set to show a shadowView below the StickerSuggestTip.
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withShadow() {
        mShouldShowShadow = true;
        return this;
    }

    /**
     * Set to NOT show a shadowView below the StickerSuggestTip.
     *
     * @return this StickerSuggestTip to build upon.
     */
    public StickerSuggestTip withoutShadow() {
        mShouldShowShadow = false;
        return this;
    }

    /**
     * @param typeface the typeface to set
     */
    public void withTypeface(final Typeface typeface) {
        mTypeface = typeface;
    }

    public CharSequence getText() {
        return mText;
    }

    public int getTextResId() {
        return mTextResId;
    }

    public int getColor() {
        return mColor;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public View getContentView() {
        return mContentView;
    }

    public AnimationType getAnimationType() {
        return mAnimationType;
    }

    public boolean shouldShowShadow() {
        return mShouldShowShadow;
    }

    public ArrayList<StickerItem> getStikerList() {
        return mStikerList;
    }

    public Context getContext() {
        return mContext;
    }

    public EmoticonsGridAdapter.KeyClickListener getmKeyclickListener() {
        return mKeyclickListener;
    }

    /**
     * @return the typeface
     */
    public Typeface getTypeface() {
        return mTypeface;
    }
}

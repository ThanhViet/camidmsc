package com.metfone.selfcare.ui;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

public class ReengViewPager extends ViewPager {
    private static final String TAG = ReengViewPager.class.getSimpleName();
    private boolean swipe = true;
    private float aspect = 0;

    private static final int VERTICAL = 0;
    private static final int HORIZONTAL = 0;

    public boolean isSwipe() {
        return swipe;
    }

    public void setSwipe(boolean swipe) {
        this.swipe = swipe;
    }

    public ReengViewPager(Context context) {
        super(context);
        this.swipe = true;
    }

    public ReengViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.swipe = true;
        TypedArray arr = context.obtainStyledAttributes(attrs, new int[]{R.attr.aspect});
        aspect = arr.getFloat(0, aspect);
        arr.recycle();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (swipe) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        try {
            if (swipe) {
                return super.onInterceptTouchEvent(event);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (Float.compare(aspect, 0) != 0) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int widthMode = MeasureSpec.getMode(widthMeasureSpec);
            int height = MeasureSpec.getSize(heightMeasureSpec);
            int heightMode = MeasureSpec.getMode(heightMeasureSpec);

            if (widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST) {
                height = calculate(width, aspect, VERTICAL);
            } else if (heightMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.AT_MOST) {
                width = calculate(height, aspect, HORIZONTAL);
            } else {
                throw new IllegalArgumentException("Either width or height should have exact value");
            }

            int specWidth = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
            int specHeight = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
            super.onMeasure(specWidth, specHeight);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }


    }

//    @Override
//    protected void onDraw(Canvas canvas) {
//        Path clipPath = new Path();
//        clipPath.addRoundRect(new RectF(canvas.getClipBounds()), Utilities.dpToPixel(R.dimen.ab_home_conner,
// getContext().getResources()), Utilities.dpToPixel(R.dimen.ab_home_conner, getContext().getResources()), Path
// .Direction.CW);
//        canvas.clipPath(clipPath);
//        super.onDraw(canvas);
//    }

    private int calculate(int size, float aspect, int direction) {
        int wp = getPaddingLeft() + getPaddingRight();
        int hp = getPaddingTop() + getPaddingBottom();
        return direction == VERTICAL
                ? Math.round((size - wp) / aspect) + hp
                : Math.round((size - hp) * aspect) + wp;
    }

    public void setAspect(float aspect) {
        this.aspect = aspect;
        invalidate();
    }
}

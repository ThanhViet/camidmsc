package com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallback;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.di.BasePresenter;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Collections;

public class ChannelManagementPresenterImpl extends BasePresenter<ChannelManagementContact.ChannelManagementView> implements ChannelManagementContact.ChannelManagementPresenter {

    private ArrayList<Channel> channels;
    private int offset = 0;
    private boolean errorApi = false;
    private boolean isLoadMore = false;
    private ChannelUserFollowCallback mChannelUserFollowCallback;
    private Channel myChannel;

    public ChannelManagementPresenterImpl(ChannelManagementContact.ChannelManagementView view, ApplicationController application) {
        super(view, application);
        mChannelUserFollowCallback = new ChannelUserFollowCallback();
        myChannel = utils.getChannelInfo();
    }

    @Override
    public void getChannelUserFollow() {
        videoApi.getChannelUserFollow(offset, mChannelUserFollowCallback);
    }

    @Override
    public void getRefreshChannelUserFollow() {
        offset = 0;
        getChannelUserFollow();
    }

    @Override
    public void getMoreChannelUserFollow() {
        getChannelUserFollow();
    }

    @Override
    public void subscribe(BaseSlidingFragmentActivity activity, Channel channel) {
        channelApi.callApiSubOrUnsubChannel(channel.getId(), channel.isFollow());
        listenerUtils.notifyChannelSubscriptionsData(channel);
    }

    @Override
    public void openDetail(BaseSlidingFragmentActivity activity, Channel channel) {
        utils.openChannelInfo(activity, channel);
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        super.onChannelSubscribeChanged(channel);
        if (Utilities.notEmpty(channels)) {
            for (Channel itemChannel : channels) {
                if (itemChannel.getId().equals(channel.getId())) {
                    itemChannel.setFollow(channel.isFollow());
                    itemChannel.setNumFollow(channel.getNumfollow());
                }
            }
            if (view != null)
                view.bindData(channels, isLoadMore);
        }
    }

    @Override
    public void onInternetChanged() {
        super.onInternetChanged();
        if (NetworkHelper.isConnectInternet(application) && Utilities.isEmpty(channels) && errorApi)
            getChannelUserFollow();
    }

    public class ChannelUserFollowCallback implements ApiCallback {

        public void onSuccess(String lastIdStr, ArrayList<Channel> results) {
            errorApi = false;
            isLoadMore = false;

            if (channels == null)
                channels = new ArrayList<>();
            if (offset == 0)
                channels.clear();

            if (Utilities.notEmpty(results) && Collections.disjoint(channels, results)) {
                for (Channel result : results) {
                    long timeLocalNewVideo = ChannelDataSource.getInstance().getTimeNewChannel(result.getId());
                    result.setHaveNewVideo(result.getLastPublishVideo() > timeLocalNewVideo);
                    if (myChannel != null && !Utilities.isEmpty(myChannel.getId()) && myChannel.getId().equals(result.getId()))
                        result.setMyChannel(true);
                }
                channels.addAll(results);
                offset = offset + 20;
                isLoadMore = true;
            }
            if (view != null)
                view.bindData(channels, isLoadMore);
        }

        @Override
        public void onError(String s) {
            errorApi = true;
        }

        @Override
        public void onComplete() {

        }
    }
}

package com.metfone.selfcare.ui.view;


import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.util.FontUtils;

/**
 * TextView use font default base on language:
 * - English: SF Pro Text
 * - Cambodia: Nokora
 *
 * font-style: normal (default)
 * font-weight: normal, 500, 600
 *
 * if (font-style = normal) {
 *     if (font-weight = normal) -> SF Pro Text Regular(en), Nokora Regular(km)
 *     if (font-weight = 500) -> SF Pro Text Medium(en), Nokora Bold(km)
 *     if (font-weight = 600) -> SF Pro Text Semi bold(en), Nokora Bold(km)
 * }
 *
 * set font-weight: font-weight figma: normal -> app:cifWeight="normal"
 *                  font-weight figma: 500 -> app:cifWeight="w500"
 *                  font-weight figma: 600 -> app:cifWeight="w600"
 *
 */
public class CamIdTextView extends AppCompatTextView {

    public CamIdTextView(@NonNull Context context) {
        super(context, null);

        FontUtils.applyCustomFont(this, context, null);
    }

    public CamIdTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);

        FontUtils.applyCustomFont(this, context, attrs);
    }

    public CamIdTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        FontUtils.applyCustomFont(this, context, attrs);
    }
}

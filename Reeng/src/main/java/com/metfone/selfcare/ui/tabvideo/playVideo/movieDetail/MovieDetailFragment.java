package com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.esport.common.Constant;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.EpisodeAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.MochaAdsClient;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.common.utils.player.MochaPlayer;
import com.metfone.selfcare.common.utils.player.MochaPlayerUtil;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.BackStackHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.BannerVideo;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Resolution;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.share.listener.ShareBusinessListener;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.FullPlayerListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.OptionsVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.QualityVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.ReportVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.VideoFullScreenPlayerDialog;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.ui.view.LinkTextView;
import com.metfone.selfcare.ui.view.tab_video.CampaignLayout;
import com.metfone.selfcare.ui.view.tab_video.SubscribeChannelLayout;
import com.metfone.selfcare.ui.view.tab_video.VideoPlaybackControlView;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.vtm.adslib.AdsCommon;
import com.vtm.adslib.AdsRewardedHelper;
import com.vtm.adslib.template.TemplateView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MovieDetailFragment extends BaseFragment implements MovieDetailContact.MovieDetailView
        , OnChannelChangedDataListener, OnVideoChangedDataListener, OnInternetChangedListener
        , LinkTextView.OnReadMoreListener, LinkTextView.OnLinkListener
        , OnItemVideoRelatedListener, OnItemFilmRelatedListener, ApiCallbackV2<ArrayList<Movie>>
        , FullPlayerListener.ProviderFullScreen,
        FullPlayerListener.OnActionFullScreenListener,
        FullPlayerListener.OnFullScreenListener, EpisodeAdapter.OnItemEpisodeListener,
        ShareBusinessListener {

    protected Video currentVideo;
    protected String playerName;
    protected ArrayList<Video> videos;
    protected MochaPlayer mPlayer;
    @BindView(R.id.layout_root)
    View rootView;
    @Nullable
    @BindView(R.id.iv_background)
    ImageView ivBackground;
    @Nullable
    @BindView(R.id.dark)
    View dark;
    @Nullable
    @BindView(R.id.iv_icon_banner)
    ImageView ivIconBanner;
    @Nullable
    @BindView(R.id.tv_content_banner)
    TextView tvContentBanner;
    @Nullable
    @BindView(R.id.btn_banner)
    RoundTextView btnBanner;
    @Nullable
    @BindView(R.id.frame_banner)
    LinearLayout frameBanner;
    @Nullable
    @BindView(R.id.iv_icon_banner_conform)
    ImageView ivIconBannerConform;
    @Nullable
    @BindView(R.id.tv_content_banner_conform)
    TextView tvContentBannerConform;
    @Nullable
    @BindView(R.id.tv_btn_banner_conform)
    RoundTextView tvBtnBannerConform;
    @Nullable
    @BindView(R.id.frame_banner_conform)
    LinearLayout frameBannerConform;
    @Nullable
    @BindView(R.id.root_frame_banner)
    RelativeLayout rootFrameBanner;
    @Nullable
    @BindView(R.id.space)
    Space space;
    @Nullable
    @BindView(R.id.tvSubscriptionsChannel)
    TextView tvSubscriptionsChannel;
    @Nullable
    @BindView(R.id.ivChannel)
    RoundedImageView ivChannel;
    @Nullable
    @BindView(R.id.tvChannelName)
    TextView tvChannelName;
    @Nullable
    @BindView(R.id.tvNumberSubscriptionsChannel)
    TextView tvNumberSubscriptionsChannel;
    @Nullable
    @BindView(R.id.reChannelInfo)
    RelativeLayout reChannelInfo;
    @Nullable
    @BindView(R.id.ivVideo)
    ImageView ivVideo;
    @Nullable
    @BindView(R.id.frVideo)
    FrameLayout frVideo;
    @Nullable
    @BindView(R.id.frController)
    FrameLayout frController;
    @Nullable
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @Nullable
    @BindView(R.id.tvNumberSeen)
    TextView tvNumberSeen;
    @Nullable
    @BindView(R.id.tvPublish)
    TextView tvPublish;
    @Nullable
    @BindView(R.id.tvNumberVideos)
    TextView tvNumberVideos;
    @Nullable
    @BindView(R.id.tvDescription)
    LinkTextView tvDescription;
    @Nullable
    @BindView(R.id.ivHear)
    ImageView ivHear;
    @Nullable
    @BindView(R.id.tvNumberHear)
    TextView tvNumberHear;
    @Nullable
    @BindView(R.id.llControllerHear)
    LinearLayout llControllerHear;
    @Nullable
    @BindView(R.id.ivComment)
    ImageView ivComment;
    @Nullable
    @BindView(R.id.tvNumberComment)
    TextView tvNumberComment;
    @Nullable
    @BindView(R.id.llControllerComment)
    LinearLayout llControllerComment;
    @Nullable
    @BindView(R.id.ivShare)
    ImageView ivShare;
    @Nullable
    @BindView(R.id.tvNumberShare)
    TextView tvNumberShare;
    @Nullable
    @BindView(R.id.llControllerShare)
    LinearLayout llControllerShare;
    @Nullable
    @BindView(R.id.ivSave)
    ImageView ivSave;
    @Nullable
    @BindView(R.id.tvSaveStatus)
    TextView tvSaveStatus;
    @Nullable
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @Nullable
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @Nullable
    @BindView(R.id.btn_subscribe_channel)
    SubscribeChannelLayout subscribeChannelLayout;
    @Nullable
    @BindView(R.id.ll_subscription)
    FrameLayout llSubscription;
    @Nullable
    @BindView(R.id.layout_episode)
    View layoutEpisode;
    @Nullable
    @BindView(R.id.tv_list_episodes)
    TextView tvListEpisodes;
    CampaignLayout campaignLayout;
    @Nullable
    @BindView(R.id.root_channel_info)
    RelativeLayout rootChannelInfo;
    @BindView(R.id.layout_ads)
    @Nullable
    TemplateView layout_ads;

    private Unbinder unbinder;
    @Inject
    @Nullable
    MovieDetailContact.MovieDetailPresenter presenter;
    private EpisodeAdapter episodeAdapter;
    private FilmRelatedAdapter filmRelatedAdapter;
    private VideoRelatedAdapter videoRelatedAdapter;
    private ArrayList<Video> videoRelated;
    private ArrayList<Video> filmRelated;
    private ArrayList<String> listGotTheVideoInformation = new ArrayList<>();
    private ListenerUtils listenerUtils;
    private boolean isAdsConform = false;
    private boolean isAdsCompleted = false;
    private boolean isAdsShow = false;
    private boolean isDestroy = false;
    private long timeStartShowAds = 2 * 1000L;
    private long timeEndShowAds = 60 * 1000L;
    private String channelId;
    private BannerVideo bannerVideo;
    private BannerVideo mBannerVideo;
    private boolean isMini = false;
    private boolean isPause = false;// trạng thái pause màn hình
    private boolean isFullScreen = false;
    private boolean isCanSetMediaSource = false;
    private boolean popOut = false;
    private VideoPlaybackControlView.CallBackListener callBackListener;
//    private ViewPagerAdapter viewPagerAdapter;

    private ClickListener.IconListener mIconListener = new ClickListener.IconListener() {

        @Override
        public void onIconClickListener(View view, Object entry, int menuId) {
            switch (menuId) {
                case Constants.MENU.POPUP_CONFIRM_REGISTER_VIP:
                    handleRegisterVip(application.getConfigBusiness().getSubscriptionConfig().getCmd());
                    break;
            }
        }
    };
    private Runnable adsSchedule = new Runnable() {
        @Override
        public void run() {
            if (rootView == null) return;
            if (mPlayer.getDuration() == C.INDEX_UNSET)
                rootView.postDelayed(this, 100);
            else if (mPlayer.getCurrentPosition() < timeStartShowAds) {
                rootView.postDelayed(this, 100);
            } else if (mPlayer.getCurrentPosition() < timeEndShowAds && mPlayer.getDuration() > timeEndShowAds) {
                if (!isAdsShow) {
                    int number = SharedPrefs.getInstance().get(MochaAdsClient.CHANNEL_BANNER_VIDEO + channelId, Integer.class, 1);
                    if (number == 1 || number == 3 || number == 5 || number == 7)
                        showAds(bannerVideo);
                    else
                        isAdsShow = true;
                    SharedPrefs.getInstance().put(MochaAdsClient.CHANNEL_BANNER_VIDEO + channelId, number > 7 ? number : number + 1);
                }
                rootView.postDelayed(this, 100);
            } else if (!isAdsConform) {
                hideAds();
            }
        }
    };
    private Player.EventListener eventListener = new Player.DefaultEventListener() {

        private int currentState;

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG, "onPlayerStateChanged playWhenReady: " + playWhenReady + ", playbackState: " + playbackState);
            if (!isFullScreen && currentState != Player.STATE_ENDED && playbackState == Player.STATE_ENDED) {
                handlerEnd();
            }
            currentState = playbackState;
        }
    };

    private Runnable infoRunnable = new Runnable() {
        @Override
        public void run() {
            if (presenter != null)
                presenter.getMoveInfo(currentVideo);
        }
    };
    private SubscribeChannelLayout.SubscribeChannelListener mSubscribeChannelListener = new SubscribeChannelLayout.SubscribeChannelListener() {
        @Override
        public void onOpenApp(Channel channel, boolean isInstall) {
            if (currentVideo == null || channel == null || !channel.equals(currentVideo.getChannel()))
                return;
            openApp(channel);
        }

        @Override
        public void onSub(Channel channel) {
            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                activity.showDialogLogin();
            else {
                bindDataUiChannel(channel);
                presenter.sub(channel);
            }
        }
    };
    private MochaAdsClient.MochaAdsEvent.OnMochaAdsListener mochaAdsListener = new MochaAdsClient.MochaAdsEvent.OnMochaAdsListener() {
        @Override
        public void onHideAdsBy(String adsId) {
            if (rootFrameBanner == null) return;
            rootFrameBanner.setVisibility(View.GONE);
        }
    };
    private VideoFullScreenPlayerDialog dialogFullScreenPlayer;
    private long mLastClickTime = 0;
    private boolean playWhenReady = false;
    private Dialog optionsVideoDialog;
    private Dialog reportVideoDialog;
    private Dialog qualityVideoDialog;
    private Dialog speedVideoDialog;
    private OrientationEventListener orientationEventListener;
    private int lastOrientation;
    private Runnable runnableWhenLandscape = () -> {
        if (callBackListener != null) callBackListener.onFullScreen();
    };
    private Runnable runnableEnableRotateSensor = () -> {
        if (orientationEventListener != null && orientationEventListener.canDetectOrientation() && (dialogFullScreenPlayer == null || !dialogFullScreenPlayer.isShowing())) {
            orientationEventListener.enable();
        }
    };
    private ShareContentBusiness shareBusiness;

    public static MovieDetailFragment newInstance(Video video, String playerTag, boolean fromLoading) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.TabVideo.VIDEO, video);
        bundle.putString(Constants.TabVideo.PLAYER_TAG, playerTag);
        bundle.putBoolean(Constants.TabVideo.PLAYER_INIT, fromLoading);

        MovieDetailFragment fragment = new MovieDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void initOrientationListener() {
        if (activity != null) {
            lastOrientation = activity.getRequestedOrientation();
            orientationEventListener = new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_UI) {
                int curOrientation;

                @Override
                public void onOrientationChanged(int orientation) {
                    if ((orientation >= 0 && orientation <= 45) || (orientation > 315 && orientation <= 360)) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    } else if (orientation > 45 && orientation <= 135) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    } else if (orientation > 135 && orientation <= 225) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    } else if (orientation > 225 && orientation <= 315) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    } else {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
                    }
                    if (curOrientation != lastOrientation) {
                        lastOrientation = curOrientation;
                        if (rootView != null) {
                            rootView.removeCallbacks(runnableWhenLandscape);
                            if ((curOrientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE || curOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) && currentVideo != null && currentVideo.isVideoLandscape()
                                    && !DeviceUtils.isDeviceLockRotate(activity)
                                    && (mPlayer != null && !mPlayer.isAdDisplayed())) {
                                rootView.postDelayed(runnableWhenLandscape, 300);
                            }
                        }
                    }
                }
            };
            orientationEventListener.disable();
        }
        enableRotateSensor();
    }

    public void enableRotateSensor() {
        if (recyclerView != null) {
            recyclerView.removeCallbacks(runnableEnableRotateSensor);
            recyclerView.postDelayed(runnableEnableRotateSensor, 600);
        }
    }

    public void disableRotateSensor() {
        if (recyclerView != null)
            recyclerView.removeCallbacks(runnableEnableRotateSensor);
        if (orientationEventListener != null) orientationEventListener.disable();
    }

    private void initVideoPlaybackListener() {
        callBackListener = new VideoPlaybackControlView.DefaultCallbackListener() {

            @Override
            public void onFullScreen() {
                dismissAllDialogs();
                disableRotateSensor();
                if (dialogFullScreenPlayer != null) dialogFullScreenPlayer.dismiss();
                if (activity == null || activity.isFinishing()) return;
                boolean isLandscape = currentVideo != null && currentVideo.isVideoLandscape();
                //activity.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                isFullScreen = true;
                dialogFullScreenPlayer = new VideoFullScreenPlayerDialog(activity);
                dialogFullScreenPlayer.setOnActionFullScreenListener(MovieDetailFragment.this);
                dialogFullScreenPlayer.setOnFullScreenListener(MovieDetailFragment.this);
                dialogFullScreenPlayer.setOnShowListener(dialog -> {
                    if (rootView != null) rootView.setVisibility(View.INVISIBLE);
                });
                dialogFullScreenPlayer.setProviderFullScreen(MovieDetailFragment.this);
                dialogFullScreenPlayer.setCurrentVideo(currentVideo);
                dialogFullScreenPlayer.setPlayerName(playerName);
                dialogFullScreenPlayer.setMovies(true);
                dialogFullScreenPlayer.setLandscape(isLandscape);
                if (Utilities.notEmpty(videos) && videos.contains(currentVideo)) {
                    int currentEpisode = videos.indexOf(currentVideo);
                    dialogFullScreenPlayer.setVideos(videos, true);
                    dialogFullScreenPlayer.setCurrentEpisode(currentEpisode);
                }
                dialogFullScreenPlayer.show();
            }

            @Override
            public void onPlayPause(boolean state) {
                if (currentVideo != null) currentVideo.setPause(!state);
            }

            @Override
            public void onHaveSeek(boolean flag) {
                if (mPlayer != null) mPlayer.onHaveSeek(flag);
            }

            @Override
            public void onMoreClick() {
                handlerMore();
            }

            @Override
            public void onQuality() {
                handlerQuality();
            }

            @Override
            public void onPlayNextVideo() {
                if (isDestroy || isPause) return;
                if (Utilities.notEmpty(videos) && videos.contains(currentVideo)) {
                    int position = videos.indexOf(currentVideo);
                    int newPosition = position + 1;
                    if (newPosition < videos.size()) {
                        currentVideo = videos.get(newPosition);
                        if (isFullScreen) isCanSetMediaSource = false;
                        playVideo(currentVideo);
                    }
                }
            }

            @Override
            public void onPlayPreviousVideo() {
                if (isDestroy || isPause) return;
                if (Utilities.notEmpty(videos) && videos.contains(currentVideo)) {
                    int position = videos.indexOf(currentVideo);
                    int newPosition = position - 1;
                    if (newPosition < videos.size()) {
                        currentVideo = videos.get(newPosition);
                        if (isFullScreen) isCanSetMediaSource = false;
                        playVideo(currentVideo);
                    }
                }
            }

            @Override
            public void onReplay() {
                if (!isFullScreen && mPlayer != null) {
                    mPlayer.logStart(currentVideo);
                }
            }
        };

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            playerName = bundle.getString(Constants.TabVideo.PLAYER_TAG);

            if (Utilities.isEmpty(playerName)) {
                isCanSetMediaSource = true;
                playerName = String.valueOf(System.nanoTime());
            } else {
                isCanSetMediaSource = bundle.getBoolean(Constants.TabVideo.PLAYER_INIT, false);
            }
            initVideoPlaybackListener();
            mPlayer = MochaPlayerUtil.getInstance().providePlayerBy(playerName);
            mPlayer.addListener(eventListener);
            mPlayer.addControllerListener(callBackListener);
            mPlayer.getPlayerView().setUseController(true);
            mPlayer.getPlayerView().setEnabled(true);
            mPlayer.getPlayerView().enableFast(false);
            mPlayer.getPlayerView().getController().setVisibility(View.VISIBLE);
            mPlayer.updatePlaybackState();
            currentVideo = (Video) bundle.getSerializable(Constants.TabVideo.VIDEO);
        }
        isDestroy = false;
        activityComponent.inject(this);
        listenerUtils = application.getListenerUtils();
        listenerUtils.addListener(this);

        boolean isAdsShow = FirebaseRemoteConfig.getInstance().getLong(AdsCommon.KEY_FIREBASE.AD_REWARD_SHOW) == 1;
        if (isAdsShow) {
            String adUnit = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_REWARD);
            AdsManager.getInstance().initAdsReward(adUnit);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        MochaAdsClient.MochaAdsEvent.getInstance().addListener(mochaAdsListener);
        return view;
    }

    @Override
    public void onDestroyView() {
        if (mPlayer != null) {
            if (!isMini)
                MochaPlayerUtil.getInstance().removerPlayerBy(playerName);
            if (mPlayer != null && callBackListener != null)
                mPlayer.removeControllerListener(callBackListener);
            if (mPlayer != null && eventListener != null)
                mPlayer.removeListener(eventListener);
        }
        isDestroy = true;
        listenerUtils.removerListener(this);
        MochaAdsClient.MochaAdsEvent.getInstance().removeListener(mochaAdsListener);
        if (presenter != null) {
            presenter.dispose();
        }
//        if (nestedScrollView != null && nestedScrollView.getViewTreeObserver() != null) {
//            nestedScrollView.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangeListener);
//        }
        if (unbinder != null) {
            unbinder.unbind();
        }
        dismissAllDialogs();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        isDestroy = true;
        if (campaignLayout != null && !isMini)
            campaignLayout.hideTextRun();
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        if (rootView != null) rootView.postDelayed(() -> initOrientationListener(), 1000);
    }

    @Override
    public void onChannelCreate(Channel channel) {

    }

    @Override
    public void onChannelUpdate(Channel channel) {

    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        onChannelSubscribeChanged(videos, episodeAdapter, channel);
        onChannelSubscribeChanged(videoRelated, videoRelatedAdapter, channel);
        onChannelSubscribeChanged(filmRelated, filmRelatedAdapter, channel);
    }

    private void onChannelSubscribeChanged(ArrayList<Video> videos, @Nullable RecyclerView.Adapter adapter, Channel channel) {
        if (Utilities.notEmpty(videos)) {
            for (int i = 0; i < videos.size(); i++) {
                Video feedVideo = videos.get(i);
                Channel feedChannel = feedVideo.getChannel();
                if (!Utilities.equals(feedChannel.getId(), channel.getId())) continue;
                feedChannel.setFollow(channel.isFollow());
                feedChannel.setNumFollow(channel.getNumfollow());
                assert adapter != null;
                adapter.notifyItemChanged(i, feedChannel);
            }
        }
    }

    @Override
    public void onVideoLikeChanged(Video video) {
        onVideoLikeChanged(videos, episodeAdapter, video);
        onVideoLikeChanged(videoRelated, videoRelatedAdapter, video);
        onVideoLikeChanged(filmRelated, filmRelatedAdapter, video);
    }

    private void onVideoLikeChanged(ArrayList<Video> videos, @Nullable RecyclerView.Adapter adapter, Video video) {
        if (Utilities.notEmpty(videos) && videos.contains(video)) {
            for (int i = 0; i < videos.size(); i++) {
                Video feedVideo = videos.get(i);
                if (!Utilities.equals(feedVideo.getId(), video.getId())) continue;
                feedVideo.setTotalLike(video.getTotalLike());
                feedVideo.setLike(video.isLike());
                if (adapter != null) {
                    adapter.notifyItemChanged(i, feedVideo);
                }
            }
        }
    }

    @Override
    public void onVideoShareChanged(Video video) {
        onVideoShareChanged(videos, episodeAdapter, video);
        onVideoShareChanged(videoRelated, videoRelatedAdapter, video);
        onVideoShareChanged(filmRelated, filmRelatedAdapter, video);
    }

    private void onVideoShareChanged(ArrayList<Video> videos, @Nullable RecyclerView.Adapter adapter, Video video) {
        if (Utilities.notEmpty(videos) && videos.contains(video)) {
            for (int i = 0; i < videos.size(); i++) {
                Video feedVideo = videos.get(i);
                if (!Utilities.equals(feedVideo.getId(), video.getId())) continue;
                feedVideo.setTotalShare(video.getTotalShare());
                feedVideo.setShare(video.isShare());
                if (adapter != null) {
                    adapter.notifyItemChanged(i, feedVideo);
                }
            }
        }
    }

    @Override
    public void onVideoCommentChanged(Video video) {
        onVideoCommentChanged(videos, episodeAdapter, video);
        onVideoCommentChanged(videoRelated, videoRelatedAdapter, video);
        onVideoCommentChanged(filmRelated, filmRelatedAdapter, video);
    }

    private void onVideoCommentChanged(ArrayList<Video> videos, @Nullable RecyclerView.Adapter adapter, Video video) {
        if (Utilities.notEmpty(videos) && videos.contains(video)) {
            for (int i = 0; i < videos.size(); i++) {
                Video feedVideo = videos.get(i);
                if (!Utilities.equals(feedVideo.getId(), video.getId())) continue;
                feedVideo.setTotalComment(video.getTotalComment());
                if (adapter != null) {
                    adapter.notifyItemChanged(i, feedVideo);
                }
            }
        }
    }

    @Override
    public void onVideoSaveChanged(Video video) {
        onVideoSaveChanged(videos, episodeAdapter, video);
        onVideoSaveChanged(videoRelated, videoRelatedAdapter, video);
        onVideoSaveChanged(filmRelated, filmRelatedAdapter, video);
    }

    private void onVideoSaveChanged(ArrayList<Video> videos, @Nullable RecyclerView.Adapter adapter, Video video) {
        if (Utilities.notEmpty(videos) && videos.contains(video)) {
            for (int i = 0; i < videos.size(); i++) {
                Video feedVideo = videos.get(i);
                if (!Utilities.equals(feedVideo.getId(), video.getId())) continue;
                feedVideo.setSave(video.isSave());
                if (adapter != null) {
                    adapter.notifyItemChanged(i, feedVideo);
                }
            }
        }
    }

    @Override
    public void onVideoWatchLaterChanged(Video video) {
        onVideoWatchLaterChanged(videos, episodeAdapter, video);
        onVideoWatchLaterChanged(videoRelated, videoRelatedAdapter, video);
        onVideoWatchLaterChanged(filmRelated, filmRelatedAdapter, video);
    }

    private void onVideoWatchLaterChanged(ArrayList<Video> videos, @Nullable RecyclerView.Adapter adapter, Video video) {
        if (Utilities.notEmpty(videos) && videos.contains(video)) {
            for (int i = 0; i < videos.size(); i++) {
                Video feedVideo = videos.get(i);
                if (!Utilities.equals(feedVideo.getId(), video.getId())) continue;
                feedVideo.setWatchLater(video.isWatchLater());
                if (adapter != null) {
                    adapter.notifyItemChanged(i, feedVideo);
                }
            }
        }
    }

    @Override
    public void onInternetChanged() {

    }

    @Override
    public void onSuccess(String lastId, ArrayList<Movie> movies) throws JSONException {

    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onComplete() {

    }

    private void initView() {
        mPlayer.addPlayerViewTo(frVideo);
        episodeAdapter = new EpisodeAdapter(activity);
        episodeAdapter.setOnItemEpisodeListener(this);
        BaseAdapter.setupGridRecycler(getActivity(), recyclerView, null, episodeAdapter, 5, R.dimen.v5_spacing_normal, true);
//        gridLayoutManager = new GridLayoutManager(activity, 5);
//        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(episodeAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        subscribeChannelLayout.setSubscribeChannelListener(mSubscribeChannelListener);

        if (ivClose != null) {
            ivClose.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    onMiniClicked();
                }
            });
        }

        if (currentVideo != null && presenter != null) {
            presenter.setVideo(currentVideo);
            if (currentVideo.getFilmGroupIdInt() > 0) presenter.getVolumeFilmGroups();
        }

        if (listGotTheVideoInformation == null)
            listGotTheVideoInformation = new ArrayList<>();

        if (currentVideo != null) {
            listGotTheVideoInformation.add(currentVideo.getId());
        }
        playVideo(currentVideo);

        tvDescription.setOnLinkListener(this);
        tvDescription.setOnReadMoreListener(this);

        if (currentVideo.getChannel() == null || Utilities.isEmpty(currentVideo.getChannel().getId())) {
            rootChannelInfo.setVisibility(View.GONE);
        } else {
            rootChannelInfo.setVisibility(View.VISIBLE);
        }

        //Handle show two tab (episodes/other videos)
//        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
//        if (currentVideo != null && currentVideo.getFilmGroupIdInt() > 0) {
//            FilmEpisodesFragment filmEpisodesFragment = FilmEpisodesFragment.newInstance(currentVideo);
//            filmEpisodesFragment.setEpisodeAdapter(episodeAdapter);
//            viewPagerAdapter.addFragment(filmEpisodesFragment, getString(R.string.list_episodes));
//            if (layoutOneTab != null) layoutOneTab.setVisibility(View.GONE);
//        } else {
//            mTabLayout.setVisibility(View.GONE);
//            if (layoutOneTab != null) layoutOneTab.setVisibility(View.VISIBLE);
//        }
//        appBarLayout.setExpanded(true);
//        viewPagerAdapter.addFragment(VideoChannelFragment.newInstance(currentVideo.getChannel(), false), getString(R.string.other_video));
//
//        viewPager.setAdapter(viewPagerAdapter);
//        mTabLayout.setupWithViewPager(viewPager);
        LuckyWheelHelper.getInstance(application).doMission(Constants.LUCKY_WHEEL.ITEM_WATCH_VIDEO); // TODO: 5/22/2020 doMission ITEM_WATCH_VIDEO
    }

    public void playVideo(Video video) {
        if (video == null || mPlayer == null) return;
        rootFrameBanner.setVisibility(View.GONE);
        if (application != null)
            application
                    .getPref()
                    .edit()
                    .putLong(Constants.TabVideo.WATCH_VIDEO, System.currentTimeMillis())
                    .apply();

        bindDataUiDetail(video);
        updateUiItemSelect(video);
        if (mPlayer.getControlView() != null) {
//            if (mPlayer.getCurrentPosition() == 0) {
            campaignLayout = mPlayer.getControlView().getCampaignLayout();
            campaignLayout.setCampaign(video.getCampaign());
            campaignLayout.setPlayer(mPlayer);
            try {
                String config = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_TEXT_RUN_CONFIG);
                if (!TextUtils.isEmpty(config) && !"-".equals(config)) {
                    JSONObject jsonObject = new JSONObject(config);
                    //{"start_time":5, "replay_time":30, "duration_time":20}
                    int startTime = jsonObject.optInt("start_time");
                    int replayTime = jsonObject.optInt("replay_time");
                    int durationTime = jsonObject.optInt("duration_time");
                    campaignLayout.setStartTime(startTime);
                    campaignLayout.setDurationTime(durationTime);
                    campaignLayout.setReplayTime(replayTime);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            campaignLayout.showTextRun();
//            }
        }
        if (mPlayer.getPlayerView() != null) {
            if (mPlayer.getPlaybackState() == Player.STATE_READY) {
                mPlayer.getPlayerView().setCover(video.getImagePath());
            } else {
                mPlayer.getPlayerView().showCover(video.getImagePath());
            }
            View quality = mPlayer.getPlayerView().getQualityView();
            if (quality != null) {
                quality.setVisibility(video.isHasListResolution() ? View.VISIBLE : View.GONE);
            }
        }
        if (isCanSetMediaSource) {
            video.setFromSource(Constants.LOG_SOURCE_TYPE.TYPE_MOCHA_VIDEO);
            mPlayer.prepare(video);
            if (!isPause)
                mPlayer.setPlayWhenReady(true);
        }
        isCanSetMediaSource = true;
        handlerMochaAds();
    }

    private void handlerMochaAds() {
        bannerVideo = SharedPrefs.getInstance().get(MochaAdsClient.BANNER_VIDEO, BannerVideo.class);
        if (bannerVideo == null)
            return;
        BannerVideo.Filter filter = bannerVideo.getFilter();
        if (filter == null)
            return;
        if (Utilities.notEmpty(filter.getChannelId())) {
            if (!filter.getChannelId().contains(currentVideo.getChannel().getId()))
                return;
            else
                channelId = currentVideo.getChannel().getId();
        } else {
            channelId = "";
        }
        if (Utilities.notEmpty(filter.getNetworkType())) {
            boolean networkAccept = false;
            for (String s : filter.getNetworkType()) {
                if (s.toUpperCase().equals(NetworkHelper.getTextTypeConnectionForLog(application).toUpperCase()))
                    networkAccept = true;
            }
            if (!networkAccept) return;
        }
        if (Utilities.notEmpty(filter.getIsVip())) {
            String isVip = application.getReengAccountBusiness().isVip() ? "1" : "0";
            boolean vipAccept = false;
            for (String s : filter.getIsVip()) {
                if (s.toUpperCase().equals(isVip.toUpperCase()))
                    vipAccept = true;
            }
            if (!vipAccept) return;
        }
        if (bannerVideo.getlStartTime() > System.currentTimeMillis() || bannerVideo.getlEndTime() < System.currentTimeMillis())
            return;

        timeStartShowAds = bannerVideo.getIntervalItem() * 1000L;
        timeEndShowAds = bannerVideo.getDisplayTime() * 1000L;

        if (timeStartShowAds == timeEndShowAds) return;
        if (rootView == null) return;

        rootView.removeCallbacks(adsSchedule);
        if (mPlayer.getCurrentPosition() < timeStartShowAds) {
            rootView.post(adsSchedule);
        } else if (mPlayer.getCurrentPosition() > timeEndShowAds) {
            hideAds();
        }
    }

    public void showAds(BannerVideo bannerVideo) {
        if (isAdsShow || isAdsCompleted || rootFrameBanner == null || rootFrameBanner.getVisibility() == View.VISIBLE || bannerVideo == null)
            return;

        isAdsShow = true;
        mBannerVideo = bannerVideo;
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) rootFrameBanner.getLayoutParams();
        layoutParams.width = ScreenManager.getWidth(getActivity());
        layoutParams.height = layoutParams.width * 180 / 1080;
        rootFrameBanner.setLayoutParams(layoutParams);

        if (Utilities.notEmpty(bannerVideo.getDisplay().getBackground())) {
            ivBackground.setVisibility(View.VISIBLE);
            ImageManager.showImage(bannerVideo.getDisplay().getBackground(), ivBackground);
        } else {
            ivBackground.setVisibility(View.GONE);
        }
        showBanner();

        rootFrameBanner.setVisibility(View.VISIBLE);
    }

    private void showBanner() {
        if (Utilities.notEmpty(mBannerVideo.getDisplay().getContent())) {
            dark.setVisibility(View.VISIBLE);
            tvContentBanner.setVisibility(View.VISIBLE);
            tvContentBanner.setText(mBannerVideo.getDisplay().getContent());
        } else {
            dark.setVisibility(View.GONE);
            tvContentBanner.setVisibility(View.GONE);
        }
        if (Utilities.notEmpty(mBannerVideo.getDisplay().getIcon())) {
            ivIconBanner.setVisibility(View.VISIBLE);
            ImageManager.showImageNotCenterCrop(mBannerVideo.getDisplay().getIcon(), ivIconBanner);
        } else {
            ivIconBanner.setVisibility(View.GONE);
        }
        btnBanner.setText(mBannerVideo.getDisplay().getLabelBtn());
    }

    private void showBannerConfig() {
        if (mBannerVideo.getActFakeMOInline() != null) {
            if (Utilities.notEmpty(mBannerVideo.getActFakeMOInline().getContentConfirm())) {
                dark.setVisibility(View.VISIBLE);
                tvContentBannerConform.setVisibility(View.VISIBLE);
                tvContentBannerConform.setText(mBannerVideo.getActFakeMOInline().getContentConfirm());
//                tvContentBannerConform.setText(mBannerVideo.getDisplay().getContent());
            } else {
                dark.setVisibility(View.GONE);
                tvContentBannerConform.setVisibility(View.GONE);
            }
            if (Utilities.notEmpty(mBannerVideo.getActFakeMOInline().getIconConfirm())) {
                ivIconBannerConform.setVisibility(View.VISIBLE);
                ImageManager.showImageNotCenterCrop(mBannerVideo.getActFakeMOInline().getIconConfirm(), ivIconBannerConform);
            } else {
                ivIconBannerConform.setVisibility(View.GONE);
            }
            tvBtnBannerConform.setText(mBannerVideo.getActFakeMOInline().getLabelConfirm());
        } else if (mBannerVideo.getActSMS() != null) {
            rootFrameBanner.setVisibility(View.GONE);
            MochaAdsClient.MochaAdsEvent.getInstance().notifyHideAdsBy("");
            NavigateActivityHelper.openAppSMS(activity, mBannerVideo.getActSMS().getSmsCodes(), mBannerVideo.getActSMS().getSmsCommand());
        }
    }

    public void hideAds() {
        if (rootFrameBanner != null)
            rootFrameBanner.setVisibility(View.GONE);
    }

    private void bindDataUiDetail(Video video) {
        /*if (video.getIsPrivate() == 1) {
            llControllerHear.setVisibility(View.GONE);
            llControllerShare.setVisibility(View.GONE);
            llControllerComment.setVisibility(View.GONE);
        } else {*/
        llControllerHear.setVisibility(View.VISIBLE);
        llControllerShare.setVisibility(View.VISIBLE);
        llControllerComment.setVisibility(View.VISIBLE);
//        }
        Utilities.setSizeFrameVideo(activity, frController, currentVideo.getAspectRatio());
        if (subscribeChannelLayout != null)
            subscribeChannelLayout.setChannel(video.getChannel());
        bindDataUiChannel(video.getChannel());
        ImageManager.showImage(video.getImagePath(), ivVideo);

        if (tvTitle != null) {
            if (Utilities.notEmpty(video.getTitle())) {
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(video.getTitle());
            } else {
                tvTitle.setVisibility(View.GONE);
            }
        }

        if (tvDescription != null) {
            tvDescription.setColorReadMore(R.color.v5_text_2);
            tvDescription.asyncSetText(video.getDescription(), video.isCollapse());
        }
        bindDataUiLike(video);
        bindDataUiShare(video);
        bindDataUiView(video);
        bindDataUiComment(video);
        bindDataUiSave(video);

        bindAds();
    }

    private void bindDataUiSave(Video video) {
        if (ivSave == null) return;
        if (video.isSave()) {
            ivSave.setImageResource(R.drawable.ic_del_option);
            tvSaveStatus.setText(getString(R.string.delete));
        } else {
            ivSave.setImageResource(R.drawable.ic_add_option);
            tvSaveStatus.setText(getString(R.string.save));
        }
    }

    private void bindDataUiView(Video video) {
        if (tvNumberSeen != null) {
            long views = video.getTotalView();
            tvNumberSeen.setText(String.format((views <= 1) ? application.getString(R.string.view)
                    : application.getString(R.string.video_views), Utilities.getTotalView(views)));
        }
        if (tvPublish != null) {
            if (video.getPublishTime() > 0) {
                if (tvPublish != null) {
                    tvPublish.setVisibility(View.VISIBLE);
                    tvPublish.setText(DateTimeUtils.calculateTime(activity.getResources(), video.getPublishTime()));
                }
            } else {
                if (tvPublish != null) tvPublish.setVisibility(View.GONE);
            }
        }
    }

    private void bindDataUiComment(Video video) {
        if (tvNumberComment != null)
            tvNumberComment.setText(Utilities.shortenLongNumber(video.getTotalComment()));
    }

    private void bindDataUiShare(Video video) {
//        if (tvNumberShare != null)
//            tvNumberShare.setText(Utilities.shortenLongNumber(video.getTotalShare()));
    }

    private void bindDataUiLike(Video video) {
        if (tvNumberHear != null)
            tvNumberHear.setText(Utilities.shortenLongNumber(video.getTotalLike()));
        if (ivHear != null)
            if (video.isLike())
                ivHear.setImageResource(R.drawable.ic_v5_heart_active);
            else
                ivHear.setImageResource(R.drawable.ic_favior__new);
    }

    private void bindDataUiChannel(Channel channel) {
        if (channel == null || Utilities.isEmpty(channel.getId())) {
            return;
        }
        if (tvChannelName != null)
            tvChannelName.setText(channel.getName());
        if (tvNumberSubscriptionsChannel != null)
            tvNumberSubscriptionsChannel.setText(String.format(application.getString(R.string.people_subscription), Utilities.shortenLongNumber(channel.getNumfollow())));
        ImageBusiness.setAvatarChannel(ivChannel, channel.getUrlImage());
        if (channel.getNumVideo() > 0) {
            if (tvNumberVideos != null) {
                tvNumberVideos.setVisibility(View.VISIBLE);
                if (channel.getNumVideo() == 1)
                    tvNumberVideos.setText(activity.getString(R.string.music_total_video, channel.getNumVideo()));
                else
                    tvNumberVideos.setText(activity.getString(R.string.music_total_videos, channel.getNumVideo()));
            }
        } else {
            if (tvNumberVideos != null) tvNumberVideos.setVisibility(View.GONE);
        }
    }

    private void updateUiItemSelect(Video videoV2) {
        if (Utilities.notEmpty(videos) && videoV2 != null && Utilities.notEmpty(videoV2.getId())) {
            for (int i = 0; i < videos.size(); i++) {
                Video video = videos.get(i);
                if (videoV2.getId().equals(video.getId()))
                    video.setPlaying(true);
                else
                    video.setPlaying(false);
            }
            if (episodeAdapter != null) episodeAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void bindData(ArrayList<Video> results, boolean loadMore) {
        if (videos == null) {
            videos = new ArrayList<>();
        } else {
            videos.clear();
        }
        if (Utilities.notEmpty(results)) {
            for (Video video : results) {
                if (Utilities.isEmpty(video.getChapter()) || videos.contains(video)) continue;
                if (Utilities.isEmpty(video.getUrlAds()) && currentVideo != null && Utilities.notEmpty(currentVideo.getUrlAds())) {
                    video.setUrlAds(currentVideo.getUrlAds());
                }
                if (Utilities.isEmpty(video.getUrlAdsEnd()) && currentVideo != null && Utilities.notEmpty(currentVideo.getUrlAdsEnd())) {
                    video.setUrlAdsEnd(currentVideo.getUrlAdsEnd());
                }
                if (Utilities.equals(video.getId(), currentVideo.getId())) {
                    currentVideo.setChapter(video.getChapter());
                    currentVideo.setUrlAds(video.getUrlAds());
                    currentVideo.setUrlAdsEnd(video.getUrlAdsEnd());
                    videos.add(currentVideo);
                } else {
                    videos.add(video);
                }
            }
            if (layoutEpisode != null) layoutEpisode.setVisibility(View.VISIBLE);
            if (episodeAdapter != null) {
                ArrayList<Object> items = new ArrayList<>();
                items.addAll(videos);
                episodeAdapter.updateData(items);
            }
            if (dialogFullScreenPlayer != null && dialogFullScreenPlayer.isShowing()) {
                if (Utilities.notEmpty(videos) && videos.contains(currentVideo)) {
                    int currentEpisode = videos.indexOf(currentVideo);
                    dialogFullScreenPlayer.setVideos(videos, true);
                    dialogFullScreenPlayer.setCurrentEpisode(currentEpisode);
                }
            }
        } else {
            if (layoutEpisode != null) layoutEpisode.setVisibility(View.GONE);
        }
        updateUiItemSelect(currentVideo);
    }

    @Override
    public void updateUiChannel() {
        if (currentVideo == null || currentVideo.getChannel() == null) return;
        bindDataUiChannel(currentVideo.getChannel());
    }

    @Override
    public void updateUiVideo() {
        if (currentVideo == null) return;
        bindDataUiLike(currentVideo);
        bindDataUiSave(currentVideo);
        bindDataUiSave(currentVideo);
        bindDataUiShare(currentVideo);
        bindDataUiComment(currentVideo);
    }

    @Override
    public void updateUiVideo(Video video) {
        if (video == null || Utilities.isEmpty(video.getId())) return;
        if (currentVideo == null || Utilities.isEmpty(currentVideo.getId())) return;

        if (listGotTheVideoInformation == null) {
            listGotTheVideoInformation = new ArrayList<>();
        }
        listGotTheVideoInformation.add(video.getId());

        currentVideo.setTotalComment(video.getTotalComment());
        currentVideo.setTotalShare(video.getTotalShare());
        currentVideo.setTotalLike(video.getTotalLike());
        currentVideo.setLike(video.isLike());
        currentVideo.setShare(video.isShare());
        updateUiVideo();
    }

    @Override
    public void handlerLikeVideo(Video video) {
        if (presenter != null)
            presenter.like(activity, video);
    }

    @Override
    public void handlerShareVideo(Video video) {
        if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
            activity.showDialogLogin();
        } else {
            if (shareBusiness != null) {
                shareBusiness.dismissAll();
            }
            shareBusiness = new ShareContentBusiness(activity, video);
            shareBusiness.setPlayingState(mPlayer != null && mPlayer.getPlayWhenReady());
            shareBusiness.setShareBusinessListener(this);
            shareBusiness.showPopupShareContent();
        }
    }

    private void handlerEnd() {
        if (isDestroy) return;
        if (!isPause && Utilities.notEmpty(videos) && videos.contains(currentVideo)) {
            int position = videos.indexOf(currentVideo);
            if (position + 1 < videos.size()) {
                Video newVideo = videos.get(position + 1);
                currentVideo = newVideo;
                playVideo(newVideo);
                return;
            }
        }
        if (!isPause && Utilities.notEmpty(videoRelated) && activity != null) {
            int position = videoRelated.indexOf(currentVideo);
            if (position + 1 < videoRelated.size()) {
                Video newVideo = videoRelated.get(position + 1);

                utils.openVideoDetail(activity, newVideo);
                activity.finish();
                return;
            }
        }
        if (mPlayer != null) {
            mPlayer.showReplay(false);
            mPlayer.logEnd();
        }
        if (currentVideo != null) currentVideo.setPause(true);
    }

    @Override
    public void onPlayNextVideoForward(Video nextVideo) {
        currentVideo = nextVideo;
        playVideo(nextVideo);
    }

    @Override
    public void onPlayEpisode(Video video, int position) {
        currentVideo = video;
        playVideo(video);
    }

    @Override
    public void onChangeSubtitleAudio(Video video) {

    }

    @Override
    public Video provideVideoForward() {
        if (Utilities.notEmpty(videos) && videos.contains(currentVideo)) {
            int position = videos.indexOf(currentVideo);
            if (position + 1 < videos.size()) {
                return videos.get(position + 1);
            }
        }
        if (Utilities.notEmpty(videoRelated)) {
            int position = videoRelated.indexOf(currentVideo);
            if (position + 1 < videoRelated.size()) {
                return videoRelated.get(position + 1);
            }
        }
        return null;
    }

    @Override
    public Video provideCurrentVideo() {
        return currentVideo;
    }

    @OnClick({R.id.reChannelInfo,
            R.id.llControllerHear,
            R.id.llControllerComment,
            R.id.llControllerShare,
            R.id.llControllerSave,
            R.id.ivSave,
            R.id.tvDescription,
            R.id.btn_banner,
            R.id.tv_btn_banner_conform})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.reChannelInfo:
                if (presenter != null) {
                    presenter.openDetail(activity, currentVideo.getChannel());
                    if (isOpenMini()) {
                        openMini(Constants.TabVideo.COMMENT);
                    } else {
                        autoPauseVideo();
                    }
                }
                break;
            case R.id.llControllerHear:
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                    activity.showDialogLogin();
                else {
                    currentVideo.setLike(!currentVideo.isLike());
                    currentVideo.setTotalLike(currentVideo.isLike() ? currentVideo.getTotalLike() + 1 : currentVideo.getTotalLike() - 1);
                    bindDataUiLike(currentVideo);
                    if (presenter != null) {
                        presenter.like(activity, currentVideo);
                    }
                }
                break;
            case R.id.tvDescription:
                currentVideo.setCollapse(false);
                tvDescription.asyncSetText(currentVideo.getDescription(), false);
                break;
            case R.id.root_comment:
            case R.id.llControllerComment:
                if (presenter != null) {
                    presenter.comment(activity, currentVideo);
                    if (isOpenMini()) {
                        openMini(Constants.TabVideo.COMMENT);
                    } else {
                        autoPauseVideo();
                    }
                }
                break;
            case R.id.llControllerShare:
                handlerShareVideo(currentVideo);
                break;
            case R.id.llControllerSave:
            case R.id.ivSave:
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    activity.showDialogLogin();
                } else {
                    if (presenter != null) {
                        currentVideo.setSave(!currentVideo.isSave());
                        bindDataUiSave(currentVideo);
                        presenter.save(currentVideo);
                        activity.showToast(currentVideo.isSave() ? R.string.videoSavedToLibrary : R.string.videoRemovedFromSaved);
                    }
                }
                break;
            case R.id.btn_banner:
                isAdsConform = true;
                showBannerConfig();
                frameBanner.setVisibility(View.GONE);
                frameBannerConform.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_btn_banner_conform:
                isAdsCompleted = true;
                MochaAdsClient.MochaAdsEvent.getInstance().notifyHideAdsBy("");
                ReportHelper.checkShowConfirmOrRequestFakeMo((ApplicationController) activity.getApplication(), activity, null, mBannerVideo.getActFakeMOInline().getCommand(), "banner_video");
                rootFrameBanner.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onLink(String content, int type) {
        if (tvDescription == null || currentVideo == null) return;
        if (type == Constants.SMART_TEXT.TYPE_MOCHA) {
            DeepLinkHelper.getInstance().openSchemaLink(activity, content);
        } else if (type == Constants.SMART_TEXT.TYPE_URL) {
            Utilities.openLink(activity, content);
        }
    }

    @Override
    public void onReadMore() {
        if (tvDescription == null || currentVideo == null) return;
        currentVideo.setCollapse(false);
        tvDescription.asyncSetText(currentVideo.getDescription(), currentVideo.isCallLogEnd());
    }

    private void openApp(Channel channel) {
        if (channel == null || TextUtils.isEmpty(channel.getPackageAndroid())) return;
        Utilities.openApp(activity, channel.getPackageAndroid());
    }

    @Override
    public void onItemVideoRelatedClick(Video video) {
//        currentVideo = video;
//        playVideo(currentVideo);
        utils.openVideoDetail(activity, video);
        activity.finish();
    }

    @Override
    public void onItemFilmRelatedClick(Video video) {
        utils.openVideoDetail(activity, video);
        activity.finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPlayer != null) {
            mPlayer.setPaused(false);
            mPlayer.setMini(false);
            if (mPlayer.isAdDisplayed()) {
                if (mPlayer.getAdPlayer() != null) mPlayer.getAdPlayer().resume();
            } else {
                if (playWhenReady) {
                    mPlayer.setPlayWhenReady(true);
                } else {
                    if (!mPlayer.getPlayWhenReady() && mPlayer.getPlayerView() != null
                            && mPlayer.getPlaybackState() != Player.STATE_IDLE
                            && mPlayer.getPlaybackState() != Player.STATE_BUFFERING)
                        mPlayer.getPlayerView().showController();
                }
            }
        }
        isPause = false;
        enableRotateSensor();
        playWhenReady = false;
    }

    @Override
    public void onPause() {
        disableRotateSensor();
        if (mPlayer != null) {
            mPlayer.setPaused(true);
            mPlayer.setMini(isMini);
            if (!isMini) {
                if (mPlayer.isAdDisplayed()) {
                    if (mPlayer.getAdPlayer() != null) mPlayer.getAdPlayer().pause();
                }
                if (mPlayer.getPlayWhenReady()) playWhenReady = true;
                mPlayer.setPlayWhenReady(false);
            }
        }
        isPause = true;
        super.onPause();
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDismiss() {
        if (rootView != null) rootView.setVisibility(View.VISIBLE);
        dismissAllDialogs();
        isFullScreen = false;
        if (activity != null) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            lastOrientation = activity.getRequestedOrientation();
        }
        if (mPlayer != null && frController != null && currentVideo != null) {
            mPlayer.getPlayerView().setRefreshMode((float) currentVideo.getAspectRatio());
            Utilities.setSizeFrameVideo(activity, frController, currentVideo.getAspectRatio());
            mPlayer.addPlayerViewTo(frVideo);
        }
        enableRotateSensor();
    }

    public void onMiniClicked() {
        if (isOpenMini()) openMini("");
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Hien popup xin quyen
            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
            dialogConfirm.setLabel(getString(R.string.permission_allow_floating_view));
            dialogConfirm.setMessage(getString(R.string.permission_allow_floating_view_content));
            dialogConfirm.setUseHtml(true);
            dialogConfirm.setNegativeLabel(getString(R.string.cancel));
            dialogConfirm.setPositiveLabel(getString(R.string.ok));
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    if (activity != null && isAdded()) {
                        autoPauseVideo();
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + activity.getPackageName()));
                        startActivityForResult(intent, 110);
                    }
                }
            });
            dialogConfirm.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    activity.finish();
                }
            });
            dialogConfirm.show();
        } else
            activity.finish();
    }

    /**
     * kiểm tra xem có thể mở mini không
     *
     * @return trạng thái có thể mở mini
     */
    public boolean isOpenMini() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(activity);
    }

    /**
     * mở màn hình mini
     */
    private void openMini(String action) {
        if (activity != null) {
            if (dialogFullScreenPlayer != null) dialogFullScreenPlayer.dismiss();
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                activity.finish();
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            isMini = true;
            VideoService.start(application, currentVideo, playerName, action);

            if (popOut) {
                activity.finish();
                if (!BackStackHelper.getInstance().checkIsLastInStack(application, VideoPlayerActivity.class.getName())) {
                    /*
                     * nếu VideoPlayerActivity không phải là phần tử cuổi cùng
                     * thì sẽ thực hiên ACTION_MAIN
                     */
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            /*
                             * về màn hình home của thiết bị
                             */
                            if (application != null) {
                                Intent startMain = new Intent(Intent.ACTION_MAIN);
                                startMain.addCategory(Intent.CATEGORY_HOME);
                                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                application.startActivity(startMain);
                            }
                        }
                    }, 300);
                }
            } else {
                if (BackStackHelper.getInstance().checkIsLastInStack(application, VideoPlayerActivity.class.getName())) {
                    /*
                     * nếu là activity cuối cùng vì mở màn hình home
                     */
                    activity.goToHome();
                } else {
                    /*
                     * nếu không phải là activity cuối cùng thì back
                     */
                    activity.finish();
                }
            }
        }
    }

    public void handlerMore() {
        if (activity == null || activity.isFinishing()) return;
        if (optionsVideoDialog != null && optionsVideoDialog.isShowing())
            optionsVideoDialog.dismiss();

        optionsVideoDialog = new OptionsVideoDialog(activity, true)
                .setHasTitle(false)
                .setListItem(providerContextMenus(activity))
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onItemClick(int itemId, Object entry) {
                        switch (itemId) {
                            case Constants.MENU.MENU_REPORT_VIDEO:
                                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                                    activity.showDialogLogin();
                                else
                                    reportVideo();
                                break;
                            case Constants.MENU.MENU_SPEED_VIDEO:
                                speedVideo();
                                break;
                            case Constants.MENU.MENU_MINI_VIDEO:
                                popOut = true;
                                openMini("");
                                break;
                            case Constants.MENU.MENU_VIP_VIDEO:
                                registerVideo();
                                break;
                        }
                    }
                });
        optionsVideoDialog.show();
    }

    /**
     * cung cấp item cho menu more
     *
     * @param activity activity
     * @return ArrayList
     */
    private ArrayList<ItemContextMenu> providerContextMenus(BaseSlidingFragmentActivity activity) {
        ArrayList<ItemContextMenu> mOverFlowItems = new ArrayList<>();
        mOverFlowItems.add(providerContextMenu(activity, R.string.report_video, R.drawable.ic_flag_option, Constants.MENU.MENU_REPORT_VIDEO));
        if (!(currentVideo != null && currentVideo.isLive()))
            mOverFlowItems.add(providerContextMenu(activity, R.string.speed_video, R.drawable.ic_play_speed_option, Constants.MENU.MENU_SPEED_VIDEO));

        if (isOpenMini())
            mOverFlowItems.add(providerContextMenu(activity, R.string.pop_out, R.drawable.ic_v5_pop_out, Constants.MENU.MENU_MINI_VIDEO));

        if (ApplicationController.self() != null
                && !ApplicationController.self().getReengAccountBusiness().isVip()
                && ApplicationController.self().getReengAccountBusiness().isVietnam()) {
            String title = application.getConfigBusiness().getSubscriptionConfig().getTitle();
            mOverFlowItems.add(providerContextMenu(activity, title, R.drawable.ic_v5_video_vip, Constants.MENU.MENU_VIP_VIDEO));
        }
        mOverFlowItems.add(providerContextMenu(activity, R.string.btn_cancel_option, R.drawable.ic_close_option, Constants.MENU.MENU_EXIT));
        return mOverFlowItems;
    }

    /**
     * cung cấp ItemContextMenu
     *
     * @param activity     activity
     * @param mStringRes   mStringRes
     * @param mDrawableRes mDrawableRes
     * @param itemId       itemId
     * @return ItemContextMenu
     */
    private ItemContextMenu providerContextMenu(BaseSlidingFragmentActivity activity, @StringRes int mStringRes, @DrawableRes int mDrawableRes, int itemId) {
        return providerContextMenu(activity, activity.getString(mStringRes), mDrawableRes, itemId);
    }

    /**
     * cung cấp ItemContextMenu
     *
     * @param activity     activity
     * @param text         text
     * @param mDrawableRes mDrawableRes
     * @param itemId       itemId
     * @return ItemContextMenu
     */
    private ItemContextMenu providerContextMenu(BaseSlidingFragmentActivity activity, String text, @DrawableRes int mDrawableRes, int itemId) {
        return new ItemContextMenu(text, mDrawableRes, null, itemId);
    }

    /**
     * chất lượng video
     */
    public void handlerQuality() {
        if (activity == null || activity.isFinishing()) return;
        if (qualityVideoDialog != null && qualityVideoDialog.isShowing())
            qualityVideoDialog.dismiss();

        qualityVideoDialog = new QualityVideoDialog(activity)
                .setCurrentVideo(currentVideo)
                .setOnQualityVideoListener(new QualityVideoDialog.OnQualityVideoListener() {
                    @Override
                    public void onQualityVideo(int idx, Video video, Resolution resolution) {
                        if (currentVideo != null && video != null
                                && !TextUtils.isEmpty(currentVideo.getId()) && !TextUtils.isEmpty(video.getId())
                                && currentVideo.getId().equals(video.getId())) {
                            if (currentVideo.getIndexQuality() == idx)
                                return;
                            currentVideo.setIndexQuality(idx);
                            if (application != null)
                                application.setConfigResolutionVideo(resolution.getKey());
                            long position;
                            long duration;
                            if (mPlayer != null) {
                                position = mPlayer.getCurrentPosition();
                                duration = mPlayer.getDuration();
                                mPlayer.prepare(resolution.getVideoPath(), video.getSubtitle(""));
                                mPlayer.seekTo(Math.min(position, duration));
                            }

                        }
                    }
                });
        qualityVideoDialog.show();
    }

    /**
     * chỉnh tốc độ video
     */
    private void speedVideo() {
        if (speedVideoDialog != null && speedVideoDialog.isShowing())
            speedVideoDialog.dismiss();

        if (mPlayer != null) {
            speedVideoDialog = DialogUtils.showSpeedDialog(activity, mPlayer.getPlaybackParameters().speed, (object, menuId) -> {
                if (menuId == Constants.MENU.MENU_SETTING && object instanceof Float) {
                    if (mPlayer.getVideo() == null || !Utilities.equals(mPlayer.getVideo(), currentVideo))
                        return;
                    PlaybackParameters param = new PlaybackParameters((Float) object);
                    mPlayer.setPlaybackParameters(param);
                }
            });
        }
    }

    /**
     * đăng ký gói video vip
     */
    private void registerVideo() {
        if (activity == null || activity.isFinishing())
            return;
        String labelOK = getResources().getString(R.string.register);
        String labelCancel = getResources().getString(R.string.cancel);
        String title = application.getConfigBusiness().getSubscriptionConfig().getTitle();
        String msg = application.getConfigBusiness().getSubscriptionConfig().getConfirm();
        PopupHelper.getInstance().showDialogConfirm(activity, title, msg, labelOK, labelCancel, mIconListener, null, Constants.MENU.POPUP_CONFIRM_REGISTER_VIP);

    }

    /**
     * đăng ký gói video vip
     *
     * @param id id
     */
    private void handleRegisterVip(String id) {
        if (!TextUtils.isEmpty(id)) {
            ReportHelper.checkShowConfirmOrRequestFakeMo(application, activity, application.getConfigBusiness().getSubscriptionConfig().getReconfirm(), id, "home_menu");
        }
    }

    /**
     * report video
     */
    private void reportVideo() {
        if (activity == null || activity.isFinishing()) return;
        if (reportVideoDialog != null && reportVideoDialog.isShowing())
            reportVideoDialog.dismiss();

        reportVideoDialog = new ReportVideoDialog(activity)
                .setCurrentVideo(currentVideo)
                .setTitleDialog(application.getResources().getString(R.string.title_report_movie));
        reportVideoDialog.show();
    }

    protected void autoPauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPaused(true);
            if (mPlayer.getAdPlayer() != null && mPlayer.isAdDisplayed()) {
                mPlayer.getAdPlayer().pause();
            }
            playWhenReady = mPlayer.getPlayWhenReady();
            mPlayer.setPlayWhenReady(false);
        }
        isPause = true;
    }

    @Override
    public void onItemEpisodeClicked(@NonNull Video video) {
        if (AdsRewardedHelper.getInstance().canShowAd()) {
            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
            dialogConfirm.setLabel(getString(R.string.app_name));
            dialogConfirm.setMessage("Hãy thư giãn một đoạn quảng cáo để tiếp tục xem phim miễn phí bạn nhé!");
            dialogConfirm.setUseHtml(true);
            dialogConfirm.setNegativeLabel(getString(R.string.cancel));
            dialogConfirm.setPositiveLabel("Xem quảng cáo");
            dialogConfirm.setPositiveListener(result -> {
                AdsManager.getInstance().showAdsReward(new AdsRewardedHelper.AdsRewardListener() {
                    @Override
                    public void onAdClosed() {

                    }

                    @Override
                    public void onAdShow() {

                    }

                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem reward) {
                        playEpisode(video);
                    }
                });
            });
            dialogConfirm.setNegativeListener(result -> {
                if (dialogConfirm != null)
                    dialogConfirm.dismiss();
            });
            dialogConfirm.show();
        } else {
            playEpisode(video);
            initOrientationListener();
        }
    }

    private void playEpisode(Video video) {
        if (currentVideo != null && Utilities.equals(currentVideo, video)) return;
        if (listGotTheVideoInformation == null)
            listGotTheVideoInformation = new ArrayList<>();
        if (listGotTheVideoInformation.indexOf(video.getId()) < 0) {
            video.setTotalComment(0);
            video.setTotalShare(0);
            video.setTotalLike(0);
            video.setLike(false);
            video.setShare(false);
            if (recyclerView != null) {
                recyclerView.removeCallbacks(infoRunnable);
                recyclerView.postDelayed(infoRunnable, 300);
            }
        }
        currentVideo = video;
        playVideo(video);
    }

    @Override
    public void onShowShareDialog() {
        if (mPlayer != null) mPlayer.setPlayWhenReady(false);
    }

    @Override
    public void onDismissShareDialog(boolean isPlayingState) {
        if (isPause) playWhenReady = isPlayingState;
        else {
            if (mPlayer != null) mPlayer.setPlayWhenReady(isPlayingState);
        }
    }

    private void dismissAllDialogs() {
        if (optionsVideoDialog != null && optionsVideoDialog.isShowing()) {
            optionsVideoDialog.dismiss();
            optionsVideoDialog = null;
        }
        if (reportVideoDialog != null && reportVideoDialog.isShowing()) {
            reportVideoDialog.dismiss();
            reportVideoDialog = null;
        }
        if (speedVideoDialog != null && speedVideoDialog.isShowing()) {
            speedVideoDialog.dismiss();
            speedVideoDialog = null;
        }
        if (qualityVideoDialog != null && qualityVideoDialog.isShowing()) {
            qualityVideoDialog.dismiss();
            qualityVideoDialog = null;
        }
        if (shareBusiness != null) {
            shareBusiness.dismissAll();
            shareBusiness = null;
        }
    }

    private void bindAds() {
        if (layout_ads != null)
            AdsManager.getInstance().showAdsNative(layout_ads);
    }
}

package com.metfone.selfcare.ui.tabvideo;

import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.di.ActivityComponent;
import com.metfone.selfcare.di.ActivityModule;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.di.DaggerActivityComponent;
import com.metfone.selfcare.util.Log;

/**
 * Created by tuanha00 on 3/22/2018.
 */

public class BaseFragment extends Fragment implements BaseView {
    protected String TAG = getClass().getSimpleName();
    protected ApplicationController application;
    protected BaseSlidingFragmentActivity activity;
    protected ActivityComponent activityComponent;
    protected boolean runnable;
    protected Utils utils;
    protected boolean isVisibleToUser;
    protected boolean isDataInitiated;
    protected boolean isViewInitiated;
    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "-------------------- onCreate");
        runnable = true;
        activity = (BaseSlidingFragmentActivity) getActivity();
        application = (ApplicationController) activity.getApplication();
        activityComponent = DaggerActivityComponent
                .builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(application.getApplicationComponent())
                .build();

        utils = application.getApplicationComponent().providesUtils();
        mFragmentManager = getParentFragmentManager();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "-------------------- onActivityCreated isVisibleToUser: " + isVisibleToUser);
        this.isViewInitiated = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        Log.i(TAG, "-------------------- setUserVisibleHint: " + isVisibleToUser + ", canLazyLoad: " + canLazyLoad());
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "-------------------- onDestroyView");
        runnable = false;
        isViewInitiated = false;
        isDataInitiated = false;
        super.onDestroyView();
    }

    public boolean canLazyLoad() {
        return !isDataInitiated;
    }

    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull boolean isBackStack) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(containerViewId, fragment, fragment.getClass().getName());
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (isBackStack)
            mFragmentTransaction.addToBackStack(fragment.getClass().getName());
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void popBackStackFragment() {
        mFragmentManager.popBackStack();
    }
}

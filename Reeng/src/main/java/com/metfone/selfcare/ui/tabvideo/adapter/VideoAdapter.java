package com.metfone.selfcare.ui.tabvideo.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.ChannelListObject;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.model.tab_video.VideoBannerItem;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.holder.BannerHolder;
import com.metfone.selfcare.ui.tabvideo.holder.TopChannelHolder;
import com.metfone.selfcare.ui.tabvideo.holder.VideoHolder;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelListener;

import java.util.List;

public class VideoAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {

    public static final int TYPE_BANNER = 0;
    public static final int TYPE_VIDEO = 1;
    public static final int TYPE_CHANNEL = 2;
    private OnChannelListener mOnChannelListener;
    private @Nullable
    VideoHolder.OnVideoListener mOnVideoListener;

    public VideoAdapter(Activity act) {
        super(act);
    }

    public void setOnVideoListener(@Nullable VideoHolder.OnVideoListener onVideoListener) {
        mOnVideoListener = onVideoListener;
    }

    public void setOnChannelListener(@Nullable OnChannelListener onChannelListener) {
        mOnChannelListener = onChannelListener;
    }

    public Object getItem(int position) {
        if (items.size() > position && position >= 0) return items.get(position);
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof ChannelListObject) {
            return TYPE_CHANNEL;
        } else if (item instanceof Video) {
            return TYPE_VIDEO;
        } else if (item instanceof VideoBannerItem) {
            return TYPE_BANNER;
        } else if (item != null && item.equals(TYPE_LOAD_MORE)) {
            return TYPE_LOAD_MORE;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CHANNEL:
                return new TopChannelHolder(activity, layoutInflater, parent, mOnChannelListener);
            case TYPE_VIDEO:
                return new VideoHolder(activity, layoutInflater, parent);
            case TYPE_BANNER:
                return new BannerHolder(activity, layoutInflater, parent);
            case TYPE_LOAD_MORE:
                return new LoadMoreHolder(activity, layoutInflater, parent);
            default:
                return new ViewHolder(layoutInflater.inflate(R.layout.holder_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(items, position, payloads);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TopChannelHolder) {
            Object item = getItem(position);
            if (item instanceof ChannelListObject) {
                ((TopChannelHolder) holder).bindData(((ChannelListObject) item).getChannelsListObject(), position);
            }
        } else if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(items, position);
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onViewAttachedToWindow();
        }
        if (holder instanceof VideoHolder) {
            ((VideoHolder) holder).setOnVideoListener(mOnVideoListener);
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onViewDetachedFromWindow();
        }
        if (holder instanceof VideoHolder) {
            ((VideoHolder) holder).setOnVideoListener(null);
        }
        super.onViewDetachedFromWindow(holder);
    }

}

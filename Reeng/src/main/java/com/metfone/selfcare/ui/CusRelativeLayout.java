package com.metfone.selfcare.ui;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by huybq7 on 9/18/2014.
 */
public class CusRelativeLayout extends RelativeLayout {
    private final String TAG = CusRelativeLayout.class.getSimpleName();
    //
    private final int KEYBOARD_NOT_SET = -1;
    private final int KEYBOARD_HIDDEN = 0;
    private final int KEYBOARD_SHOW = 1;
    private int isHiddenKeyboard = KEYBOARD_NOT_SET;
    private int mContentHeight;
    //
    private DisplayMetrics displayMetrics;
    private WindowManager wm;

    public interface IKeyboardChanged {
        /**
         * listener when system keyboard hidden
         */
        void onSystemKeyboardHidden();

        /**
         * listener when content Height changed
         *
         * @param currentContentHeight
         */
        void onChangeContentHeight(int currentContentHeight);

        /**
         * Listener when system keyboard show
         *
         * @param currentContentHeight:
         * @param keyBoardHeight
         */
        void onSystemKeyboardShown(int currentContentHeight, int keyBoardHeight);
    }

    private ArrayList<IKeyboardChanged> mKeyboardListener = new ArrayList<>();

    public CusRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        registerScreenSizeListener();
    }

    public CusRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        registerScreenSizeListener();
    }

    public CusRelativeLayout(Context context) {
        super(context);
        registerScreenSizeListener();
    }

    private void registerScreenSizeListener() {
        displayMetrics = new DisplayMetrics();
        Log.d(TAG, "registerScreenSizeListener");
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                getWindowVisibleDisplayFrame(r);
                wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
                wm.getDefaultDisplay().getMetrics(displayMetrics);
                int screenHeight = displayMetrics.heightPixels;
                int screenWidth = displayMetrics.widthPixels;
                /*int screenHeight = getRootView().getHeight();
                int screenWidth = getRootView().getWidth();*/
                // kiem tra an hien ban phim trong cung 1 trang thay portrait/langscape
                if (r.right - r.left != screenWidth) {
                    Log.d(TAG, "return because r.right - r.left != screenWidth");
                    // truong hop khi xoay man hinh.
                    return;
                }
                int systemKeyboardHeight = screenHeight - r.bottom;
                Log.i(TAG, "screenHeight = " + screenHeight + " - r.bottom = " + r.bottom);
                int contentHeight = getCurrentContentHeight(r.bottom, screenHeight, systemKeyboardHeight);

                if (systemKeyboardHeight > 0) {
                    Log.d(TAG, "notifyKeyboardShown systemKeyboardHeight = " + systemKeyboardHeight + " - contentHeight = " + contentHeight);
                    notifyKeyboardShown(contentHeight, systemKeyboardHeight);
                } else {
                    Log.d(TAG, "notifyKeyboardHidden systemKeyboardHeight = " + systemKeyboardHeight + " - contentHeight = " + contentHeight);
                    notifyKeyboardHidden();
                }
                notifyChangeContentHeight(contentHeight);
            }
        });
    }

    private int getCurrentContentHeight(int bottomOfContent, int screenHeight, int systemKeyboardHeight) {
        int actionBarBottom = bottomOfContent - getHeight();
        if (actionBarBottom < 0) {
            actionBarBottom = screenHeight - getHeight();
        }
        int contentHeight_ = screenHeight - actionBarBottom - systemKeyboardHeight;
        if (contentHeight_ <= 0) {
            // khi o landscape mode tinh lai contentHeight
            contentHeight_ = getHeight();
        }
        return contentHeight_;
    }

    public void addKeyboardStateChangedListener(IKeyboardChanged listener) {
        mKeyboardListener.add(listener);
    }

    public void removeKeyboardStateChangedListener(IKeyboardChanged listener) {
        mKeyboardListener.remove(listener);

    }

    /**
     * Notify system keyboard hidden
     */
    private void notifyKeyboardHidden() {
        if (isHiddenKeyboard != KEYBOARD_HIDDEN) {
            isHiddenKeyboard = KEYBOARD_HIDDEN;
            for (IKeyboardChanged listener : mKeyboardListener) {
                if (listener != null) listener.onSystemKeyboardHidden();
            }
        }
    }

    /**
     * @param currentHeight
     */
    private void notifyChangeContentHeight(int currentHeight) {
        if (currentHeight > 0 && mContentHeight != currentHeight) {
            mContentHeight = currentHeight;
            for (IKeyboardChanged listener : mKeyboardListener) {
                if (listener != null) listener.onChangeContentHeight(currentHeight);
            }
        }
    }

    /**
     * notify system keyboard show
     *
     * @param currentHeight
     * @param keyBoardHeight
     * @return
     */
    private void notifyKeyboardShown(int currentHeight, int keyBoardHeight) {
        if (isHiddenKeyboard != KEYBOARD_SHOW) {
            isHiddenKeyboard = KEYBOARD_SHOW;
            mContentHeight = currentHeight;
            for (IKeyboardChanged listener : mKeyboardListener) {
                if (listener != null)
                    listener.onSystemKeyboardShown(currentHeight, keyBoardHeight);
            }
        } else {
            Log.d(TAG, "notifyKeyboardShown no listener");
        }
    }

}
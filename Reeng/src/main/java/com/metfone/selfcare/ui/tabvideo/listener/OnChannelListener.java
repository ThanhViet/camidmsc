package com.metfone.selfcare.ui.tabvideo.listener;

import com.metfone.selfcare.model.tab_video.Channel;

public interface OnChannelListener {

    void onClick(Channel channel);
}

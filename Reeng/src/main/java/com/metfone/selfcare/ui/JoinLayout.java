package com.metfone.selfcare.ui;

public class JoinLayout {

    public static final float variationCircleCrop = 0.0f;

    public static int max() {
        return 4;
    }

    private static final float[] SIZE_ = {
            0.95f,
            0.6f};

    public static float[] sizeCircleCrop(int count, int index, float dimension) {
        float variation = variationCircleCrop * dimension;
        switch (count) {
            case 1:
                return sizeCircleCrop1(index, dimension, variation);
            case 2:
                return sizeCircleCrop2(index, dimension, variation);
            case 3:
                return sizeCircleCrop3(index, dimension, variation);
            case 4:
                return sizeCircleCrop4(index, dimension, variation);
            default:
                return sizeCircleCrop4(index, dimension, variation);
        }
    }

    private static float[] sizeCircleCrop4(int index, float dimension, float variation) {
        float cd = dimension * SIZE_[0];
        // Diameter of the circle
        float cdSmall = dimension * (SIZE_[1]);
        //
        float delta = (cdSmall - (dimension * SIZE_[0] / 2));

        switch (index) {
            case 0:
                return new float[]{delta / 2 + cd / 2, delta / 2 + cd / 2, cd / 2};
            case 1:
                return new float[]{delta / 2, delta / 2 + cd / 2, cd / 2};
            case 2:
                return new float[]{delta / 2 + cd / 2, delta / 2, cd / 2};
            case 3:
                return new float[]{delta / 2, delta / 2, cd / 2};
            default:
                return new float[]{cd / 2f, cd / 2f, cd};
        }
    }

    private static float[] sizeCircleCrop3(int index, float dimension, float variation) {
        float cd = dimension * SIZE_[0];
        // Diameter of the circle
        float cdSmall = dimension * (SIZE_[1]);
        //
        float delta = (cdSmall - (dimension * SIZE_[0] / 2));

        switch (index) {
            case 0:
                return new float[]{3 * cd / 4f, cd / 2f, cd / 2f};
            case 1:
                return new float[]{delta / 2, delta / 2 + cd / 2, cd / 2};
            case 2:
                return new float[]{delta / 2, delta / 2, cd / 2};
            default:
                return new float[]{cd / 2f, cd / 2f, cd};
        }
    }

    private static float[] sizeCircleCrop2(int index, float dimension, float variation) {
        float cd = dimension * SIZE_[0];

        switch (index) {
            case 0:
                return new float[]{3 * cd / 4f, cd / 2f, cd / 2f};
            case 1:
                return new float[]{cd / 4f, cd / 2f, cd / 2f};
            default:
                return new float[]{cd / 4f, cd / 2f, cd / 2f};
        }

    }

    private static float[] sizeCircleCrop1(int index, float dimension, float variation) {
        float cd = dimension * SIZE_[0];
        return new float[]{cd / 2f, cd / 2f, cd / 2f};
    }

    public static float[] sizeCrop(int count, int index, float dimension) {
        switch (count) {
            case 1:
                return sizeCrop1(index, dimension);
            case 2:
                return sizeCrop2(index, dimension);
            case 3:
                return sizeCrop3(index, dimension);
            case 4:
                return sizeCrop4(index, dimension);
            default:
                return sizeCrop4(index, dimension);
        }
    }

    private static float[] sizeCrop4(int index, float dimension) {
        float cd = dimension * SIZE_[0];
        // Diameter of the circle
        float cdSmall = dimension * (SIZE_[1]);
        //
        float delta = (cdSmall - (dimension * SIZE_[0] / 2));

        switch (index) {
            case 0:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
            case 1:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
            case 2:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
            case 3:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
            default:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
        }
    }

    private static float[] sizeCrop3(int index, float dimension) {
        float cd = dimension * SIZE_[0];
        // Diameter of the circle
        float cdSmall = dimension * (SIZE_[1]);
        //
        float delta = (cdSmall - (dimension * SIZE_[0] / 2));

        switch (index) {
            case 0:
                return new float[]{cd / 4, 0f, cd / 4 + cd / 2, cd};
            case 1:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
            case 2:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
            default:
                return new float[]{delta / 2, delta / 2, delta / 2 + cd / 2, delta / 2 + cd / 2};
        }
    }

    private static float[] sizeCrop2(int index, float dimension) {
        float cd = dimension * SIZE_[0];

        switch (index) {
            case 0:
                return new float[]{cd / 4, 0f, cd / 4 + cd / 2, cd};
            case 1:
                return new float[]{cd / 4, 0f, cd / 4 + cd / 2, cd};
            default:
                return new float[]{cd / 4, 0f, cd / 4 + cd / 2, cd};
        }
    }

    private static float[] sizeCrop1(int index, float dimension) {
        float cd = dimension * SIZE_[0];
        return new float[]{0f, 0f, cd, cd};
    }

    public static float[] getOffset(float dimension) {
        // Diameter of the circle
        float cd = dimension * SIZE_[0];
        float offset = (dimension - cd) / 2;
        return new float[]{offset, offset};
    }

    public static float[] offsetForcus(int count, int index, float dimension) {
        switch (count) {
            case 1:
                return offset1(index, dimension);
            case 2:
                return offset2(index, dimension);
            case 3:
                return offset3(index, dimension);
            /*case 4:
                return offset4(index, dimension);*/
            default:
                return offset3(index, dimension);
        }
    }

    /**
     * 4 Photos
     *
     * @param index Subscript
     * @return Subscript index X，Y Axis coordinate
     */
    private static float[] offset4(int index, float dimension) {
        float cd = dimension * SIZE_[0];
        // Diameter of the circle
        float cdSmall = dimension * (SIZE_[1]);
        //
        float delta = (cdSmall - (dimension * SIZE_[0] / 2));
        // Margins
        float offset = (dimension - cd) / 2;

        float x1 = 0f + offset - delta / 2;
        float y1 = 0f + offset - delta / 2;

        float x2 = 0f + offset + cd / 2 - delta / 2;
        float y2 = 0f + offset - delta / 2;

        float x3 = 0f + offset + cd / 2 - delta / 2;
        float y3 = 0f + offset + cd / 2 - delta / 2;

        float x4 = 0f + offset - delta / 2;
        float y4 = 0f + offset + cd / 2 - delta / 2;

        switch (index) {
            case 0:
                return new float[]{x1, y1, SIZE_[1]};
            case 1:
                return new float[]{x2, y2, SIZE_[1]};
            case 2:
                return new float[]{x4, y4, SIZE_[1]};
            case 3:
                return new float[]{x3, y3, SIZE_[1]};
            default:
                return new float[]{x1, y1, SIZE_[1]};
        }
    }

    /**
     * 3 Photos
     *
     * @param index Subscript
     * @return Subscript index X，Y Axis coordinate
     */
    private static float[] offset3(int index, float dimension) {

        float cd = dimension * SIZE_[0];
        // Diameter of the circle
        float cdSmall = dimension * (SIZE_[1]);
        //
        float delta = (cdSmall - (dimension * SIZE_[0] / 2));
        // Margins
        float offset = (dimension - cd) / 2;

        float x1 = 0f + offset - cd / 4;
        float y1 = 0f + offset;
        // X coordinate of the second round
        float x2 = 0f + offset + cd / 2 - delta / 2;
        // Y coordinate of the second round
        float y2 = 0f + offset - delta / 2;
        // X coordinate of the third round
        float x3 = x2;
        float y3 = 0f + offset + cd / 2 - delta / 2;

        switch (index) {
            case 0:
                return new float[]{x1, y1, SIZE_[0]};
            case 1:
                return new float[]{x2, y2, SIZE_[1]};
            case 2:
                return new float[]{x3, y3, SIZE_[1]};
            default:
                break;
        }
        return new float[]{x1, y1, SIZE_[0]};
    }

    /**
     * 2 Photos
     *
     * @param index Subscript
     * @return Subscript index X，Y Axis coordinate
     */
    private static float[] offset2(int index, float dimension) {
        // Diameter of the circle
        float cd = dimension * SIZE_[0];
        float offset = (dimension - cd) / 2;
        float x1 = 0 + offset - cd / 4;
        float y1 = 0 + offset;
        float x2 = 0 + offset + cd / 4;
        float y2 = 0 + offset;
        switch (index) {
            case 0:
                return new float[]{x1, y1, SIZE_[0]};
            case 1:
                return new float[]{x2, y2, SIZE_[0]};
            default:
                break;
        }
        return new float[]{0f, 0f, SIZE_[0]};
    }

    /**
     * 1 Photos
     *
     * @param index Subscript
     * @return Subscript index X，Y Axis coordinate
     */
    private static float[] offset1(int index, float dimension) {
        // Diameter of the circle
        float cd = dimension * SIZE_[0];
        float offset = (dimension - cd) / 2;
        return new float[]{offset, offset, SIZE_[0]};
    }
}

package com.metfone.selfcare.ui.tabvideo.listener;

import com.metfone.selfcare.model.tab_video.Video;

public interface OnVideoListener {
    void onClick(Video video);

    void onHear(Video video);

    void onShare(Video video);

    void onComment(Video video);

    void onMore(Video video);
}

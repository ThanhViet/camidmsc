package com.metfone.selfcare.ui;

import android.app.Activity;
import android.content.res.Configuration;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.util.Log;

/**
 * Created by huybq7 on 2/13/2015.
 */
public class ScreenStateInfo {
    private static final String TAG = ScreenStateInfo.class.getSimpleName();
    private ApplicationController mApplication;
    private int notifyBarHeight = 20;
    private int mScreenOrientation = -1;
    private int mTopChatBarPortrait;
    private int mTopChatBarLandScape;
    private int mTopChatBarLandScapeTapLet;
    private int abHeight;
    private int chatBarHeightDefault;
    private boolean isTabLet = false;

    public ScreenStateInfo(ApplicationController application, BaseSlidingFragmentActivity activity) {
        this.mApplication = application;
        if (activity != null && activity.getResources() != null && activity.getResources().getConfiguration() != null) {
            mScreenOrientation = activity.getResources().getConfiguration().orientation;
        } else {
            mScreenOrientation = mApplication.getResources().getConfiguration().orientation;
        }
        isTabLet = DeviceHelper.isTablet(mApplication);
        mTopChatBarPortrait = initTopChatBarPortrait();
        int topChatBarPrefLandScape = InputMethodUtils.getIntPreferenceHeightPref(mApplication,
                Constants.PREFERENCE.PREF_TAPLET_LANGSCAPE_KEYBOARD_OFFSET_HEIGHT, 0);
        if (topChatBarPrefLandScape > 0) {   // da luu
            mTopChatBarLandScapeTapLet = topChatBarPrefLandScape;
        } else {// chua luu
            mTopChatBarLandScapeTapLet = initTopChatBarLangScape();
        }
        mTopChatBarLandScape = initTopChatBarLangScape();//default
    }

    /**
     * @param orientation
     * @param isInit:     =false: khi init; =true: khi rotate screen
     */
    public void changeOrientation(Activity activity, int orientation, boolean isInit) {
        if (mScreenOrientation == orientation) {
            return;
        }
        mScreenOrientation = orientation;
        Log.d(TAG, "ChangeOrientation: " + orientation);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            int topChatBarPref = InputMethodUtils.getIntPreferenceHeightPref(activity,
                    Constants.PREFERENCE.PREF_KEYBOARD_OFFSET_HEIGHT, 0);
            Log.d(TAG, "ChangeOrientation:P " + topChatBarPref);
            if (topChatBarPref > 0) {// da luu
                mTopChatBarPortrait = topChatBarPref;
            } else {// chua luu
                mTopChatBarPortrait = initTopChatBarPortrait();//default
            }
            Log.d(TAG, "ChangeOrientation mTopChatBarPortrait: " + mTopChatBarPortrait);
        } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (isTabLet) {
                int topChatBarPrefLandScape = InputMethodUtils.getIntPreferenceHeightPref(activity,
                        Constants.PREFERENCE.PREF_TAPLET_LANGSCAPE_KEYBOARD_OFFSET_HEIGHT, 0);
                if (topChatBarPrefLandScape > 0) {   // da luu
                    mTopChatBarLandScapeTapLet = topChatBarPrefLandScape;
                } else {// chua luu
                    mTopChatBarLandScapeTapLet = initTopChatBarLangScape();
                }
            } else if (mTopChatBarLandScape <= 0) {
                mTopChatBarLandScape = initTopChatBarLangScape();
            }
            Log.d(TAG, "mTopChatBarLandScape: " + mTopChatBarLandScape);
        }
    }

    public int getScreenOrientation() {
        return mScreenOrientation;
    }

    public int getTopChatBarPopup() {
        if (mScreenOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (isTabLet) {
                Log.d(TAG, "CusKeyboardController mTopChatBarLandScapeTapLet: " + mTopChatBarLandScapeTapLet);
                return mTopChatBarLandScapeTapLet;
            } else {
                Log.d(TAG, "CusKeyboardController mTopChatBarLandScape: " + mTopChatBarLandScape);
                return mTopChatBarLandScape;
            }
        } else {
            Log.d(TAG, "CusKeyboardController mTopChatBarPortrait: " + mTopChatBarPortrait);
            return mTopChatBarPortrait;
        }
    }

    public void setTopChatBatPopup(Activity activity, int mTopChatBatPopup, boolean isUpdatePref) {
        if (mScreenOrientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d(TAG, "setTopChatBatPopup:P  " + mTopChatBatPopup);
            mTopChatBarPortrait = mTopChatBatPopup;
            if (isUpdatePref) {
                InputMethodUtils.setIntPreferenceHeightPref(activity,
                        Constants.PREFERENCE.PREF_KEYBOARD_OFFSET_HEIGHT,
                        mTopChatBatPopup);
            }
        } else if (mScreenOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d(TAG, "setTopChatBatPopup:UserActionAdapter  " + mTopChatBatPopup);
            if (isTabLet) {
                mTopChatBarLandScapeTapLet = mTopChatBatPopup;
                if (isUpdatePref) {
                    InputMethodUtils.setIntPreferenceHeightPref(activity,
                            Constants.PREFERENCE.PREF_TAPLET_LANGSCAPE_KEYBOARD_OFFSET_HEIGHT,
                            mTopChatBatPopup);
                }
            } else {
                mTopChatBarLandScape = mTopChatBatPopup;
            }
        }
    }

    private int initTopChatBarLangScape() {
        return (mApplication.getWidthPixels() / 2) - abHeight - notifyBarHeight;
    }

    private int initTopChatBarPortrait() {
        double density = mApplication.getDensity();
        int keyboardHeightPortrait = Constants.PREF_DEFAULT.KEYBOARD_HEIGHT_DEFAULT * 1;
        abHeight = Constants.PREF_DEFAULT.CHAT_BAR_HEIGHT_DEFAULT * 1; // measure
        chatBarHeightDefault = Constants.PREF_DEFAULT.CHAT_BAR_HEIGHT_DEFAULT * 1; // measure
        if (density > 0) {
            keyboardHeightPortrait = (int) (Constants.PREF_DEFAULT.KEYBOARD_HEIGHT_DEFAULT * density);
            abHeight = (int) (Constants.PREF_DEFAULT.CHAT_BAR_HEIGHT_DEFAULT * density);
            chatBarHeightDefault = (int) (Constants.PREF_DEFAULT.CHAT_BAR_HEIGHT_DEFAULT * density);
            notifyBarHeight = (int) (density * 24);
        }
        int topOffsetChatBatPref = InputMethodUtils.getIntPreferenceHeightPref(mApplication,
                Constants.PREFERENCE.PREF_KEYBOARD_OFFSET_HEIGHT, 0);
        if (topOffsetChatBatPref > 50) {
            return topOffsetChatBatPref;
        } else {
            return mApplication.getHeightPixels() - keyboardHeightPortrait - abHeight - chatBarHeightDefault;
        }
    }

    public int getContentHeightDefault() {
        return mApplication.getHeightPixels() - abHeight;
    }
}
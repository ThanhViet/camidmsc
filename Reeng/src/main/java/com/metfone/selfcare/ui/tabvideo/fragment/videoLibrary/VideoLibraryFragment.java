package com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.database.constant.VideoConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayout;
import com.metfone.selfcare.ui.tabvideo.fragment.BaseViewStubFragment;
import com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary.adapter.VideoLibraryAdapter;
import com.metfone.selfcare.ui.view.load_more.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.view.load_more.RecyclerViewUtils;
import com.metfone.selfcare.ui.view.load_more.widget.LoadingFooter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class VideoLibraryFragment extends BaseViewStubFragment implements VideoLibraryView,
        VideoLibraryAdapter.OnItemVideoClickListener,
        Utils.OnConfigRemoveVideoListener, OnClickMoreItemListener {

    @BindView(R.id.recyclerViewVideo)
    RecyclerView recyclerViewVideo;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.swipyRefreshLayoutVideo)
    SwipyRefreshLayout swipyRefreshLayoutVideo;
    @BindView(R.id.rootNotifyNoData)
    View rootNotifyNoData;
    @BindView(R.id.tvNoDataInfo)
    TextView tvNoDataInfo;
    @BindView(R.id.tvAddFavorite)
    TextView tvAddFavorite;
    @BindView(R.id.tvNoDataInfoTitle)
    TextView tvNoDataInfoTitle;
    @BindView(R.id.ivSave)
    ImageView ivSave;
    @BindView(R.id.tvSave)
    TextView tvSave;
    Unbinder unbinder;


    public static Bundle arguments(String value) {
        return new Bundler()
                .putString(Constants.TabVideo.TYPE, value)
                .get();
    }

    public static VideoLibraryFragment newInstance(String value) {
        VideoLibraryFragment fragment = new VideoLibraryFragment();
        fragment.setArguments(arguments(value));
        return fragment;
    }

    private String type = "";
    private boolean viewInit = false;// cờ thông báo giao diện đã được vẽ lên

    private VideoLibraryAdapter videoLibraryAdapter;

    @Inject
    VideoLibraryPresenter videoLibraryPresenter;

    @Inject
    Utils utils;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);

        if (getArguments() != null) {
            type = getArguments().getString(Constants.TabVideo.TYPE);
        }

        videoLibraryAdapter = new VideoLibraryAdapter(activity);
        videoLibraryAdapter.setListener(this);
    }

    @Override
    protected void onCreateViewAfterViewStubInflated(View inflatedView, Bundle savedInstanceState) {
        if (!runnable) return;
        unbinder = ButterKnife.bind(this, inflatedView);
        initView();
        videoLibraryPresenter.getVideoByType(type);
    }

    @Override
    protected int getViewStubLayoutResource() {
        return R.layout.fragment_video_library;
    }

    /**
     * khởi tạo giao diện
     */
    private void initView() {

        initViewDataEmpty();
        initViewContent();
        swipyRefreshLayoutVideo.setEnabled(false);

        viewInit = true;
    }

    private void initVideoCount(int size) {
        if (size < 1) {
            tvCount.setVisibility(View.GONE);
            return;
        }
        tvCount.setVisibility(View.VISIBLE);
        if (size == 1)
            tvCount.setText(getString(R.string.total_video, size));
        else
            tvCount.setText(getString(R.string.total_videos, size));
    }

    /**
     * khởi tạo giao diện nội dung
     */
    private void initViewContent() {

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(activity, R.anim.layout_animation_fall_down);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setInitialPrefetchItemCount(5);
        recyclerViewVideo.setLayoutManager(linearLayoutManager);
        recyclerViewVideo.setAdapter(new HeaderAndFooterRecyclerViewAdapter(videoLibraryAdapter));
        recyclerViewVideo.setHasFixedSize(true);
        recyclerViewVideo.setLayoutAnimation(controller);
        RecyclerViewUtils.setFooterView(recyclerViewVideo, new LoadingFooter(activity));

    }

    /**
     * khởi tạo giao diện thông báo không có dữ liệu
     */
    private void initViewDataEmpty() {
        if (VideoConstant.Type.LATER.VALUE.equals(type)) {
            tvNoDataInfoTitle.setText(R.string.video_library_watch_later_empty_title);
            tvNoDataInfo.setText(R.string.video_library_watch_later_empty);
            ivSave.setImageResource(R.drawable.ic_movie_later_option);
            tvSave.setText(getString(R.string.watchLater));
            tvAddFavorite.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_movie_later_option, 0);
        } else {
            tvNoDataInfoTitle.setText(R.string.video_library_saved_empty_title);
            tvNoDataInfo.setText(R.string.video_library_saved_empty);
            ivSave.setImageResource(R.drawable.ic_v5_add_menu);
            tvSave.setText(R.string.save);
            tvAddFavorite.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_v5_add_menu, 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        videoLibraryPresenter.removerListener();
    }

    @Override
    public void showVideo(ArrayList<Video> videos) {
        if (!viewInit || videoLibraryAdapter == null || recyclerViewVideo == null || rootNotifyNoData == null)
            return;
        videoLibraryAdapter.setData(videos);
        recyclerViewVideo.scheduleLayoutAnimation();
        initVideoCount(videos.size());
        if (videos.isEmpty()) {
            rootNotifyNoData.setVisibility(View.VISIBLE);
        } else {
            rootNotifyNoData.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onItemClicked(Video video) {
        video.setDescription("");
        utils.openVideoDetail(activity, video);
    }

    private Video video;

    @Override
    public void onItemLongClicked(Video video) {
        this.video = video;
        utils.openConfigRemove(activity, this);
    }

    @Override
    public void onMoreClicked(Video video) {
        utils.showOptionLibraryVideoItem(activity, video, type, this);
    }

    @Override
    public void onConfigRemoveVideo() {
        videoLibraryPresenter.removerVideo(video);
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            if (menuId != Constants.MENU.MENU_EXIT && ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
                return;
            }
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_REMOVE_SAVED:
                case Constants.MENU.MENU_REMOVE_WATCHED:
                    if (object instanceof Video) {
                        videoLibraryPresenter.removerVideo((Video) object);
                    }
                    break;

            }
        }
    }
}

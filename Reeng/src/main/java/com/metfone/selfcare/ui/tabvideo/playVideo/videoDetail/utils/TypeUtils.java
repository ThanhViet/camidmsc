package com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.utils;

import java.util.HashMap;
import java.util.Map;

public class TypeUtils {

    private static TypeUtils ourInstance;

    public static TypeUtils getInstance() {
        if (ourInstance == null) ourInstance = new TypeUtils();
        return ourInstance;
    }

    private Map<Double, Integer> doubleIntegerMap;
    private int typeNumber = 0;

    private TypeUtils() {
        this.doubleIntegerMap = new HashMap<>();
    }

    public int getType(double aspectRatio) {
        Integer type = doubleIntegerMap.get(aspectRatio);
        if (type == null || type == -1) {
            doubleIntegerMap.put(aspectRatio, typeNumber);
            type = typeNumber;
            typeNumber++;
        }
        return type;
    }

    public double getAspectRatio(int type) {
        Double aspectRatio = 0d;
        for (Map.Entry<Double, Integer> entry : doubleIntegerMap.entrySet()) {
            if (entry.getValue() == type) {
                aspectRatio = entry.getKey();
                break;
            }
        }
        if (aspectRatio == null || aspectRatio.isNaN() || aspectRatio <= 0)
            aspectRatio = (double) 16 / 9;

        return aspectRatio;
    }
}

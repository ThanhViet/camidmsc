package com.metfone.selfcare.ui.easyvideoplayer;

/** @author Aidan Follestad (afollestad) */
public interface EasyVideoProgressCallback {

  void onVideoProgressUpdate(int position, int duration);
}

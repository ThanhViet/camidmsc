package com.metfone.selfcare.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.listeners.ClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk on 9/29/2014.
 */
public class PopupWindowOverFlow extends PopupWindow {
    private View mButtonView;
    private View popupView;
    private Context context;
    private ArrayList<ItemContextMenu> listItem;
    private MenuAdapter menuAdapter = null;
    private ClickListener.IconListener clickHandler = null;
    private ListView listView;
    private Drawable bg;

    public PopupWindowOverFlow() {
        super();
    }

    public PopupWindowOverFlow(View contentView) {
        super(contentView);
    }

    public static PopupWindowOverFlow newInstance(Context context, View buttonView,
                                                  ClickListener.IconListener callBack,
                                                  ArrayList<ItemContextMenu> listItem) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.popup_overflow_menu, null, false);
        PopupWindowOverFlow popup = new PopupWindowOverFlow(view);
        popup.context = context;
        popup.mButtonView = buttonView;
        popup.listItem = listItem;
        popup.clickHandler = callBack;
        ApplicationController application = (ApplicationController) context.getApplicationContext();
        int widthDevice = application.getWidthPixels();
        //int widthScreen = LayoutHelper.getWidthScreen(context);
        popup.setWidth((widthDevice / 2) + 40);
        popup.setHeight(ActionBar.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(false);
        popup.setOutsideTouchable(true);
        popup.bg = ContextCompat.getDrawable(context, R.drawable.transparent);
        //popup.bg = ContextCompat.getDrawable(context, R.drawable.abc_popup_background_mtrl_mult);
        popup.setBackgroundDrawable(popup.bg);
        return popup;
    }

    @Override
    public void dismiss() {
        this.bg = null;
        super.dismiss();
    }

    public void setListItem(ArrayList<ItemContextMenu> listItem) {
        this.listItem = listItem;
    }

    public void setCallBack(ClickListener.IconListener callBack) {
        this.clickHandler = callBack;
    }

    public void notifyAdapterChange(ArrayList<ItemContextMenu> listItem) {
        this.listItem = listItem;
        if (menuAdapter != null) {
            menuAdapter.setListItem(listItem);
            menuAdapter.notifyDataSetChanged();
        }
    }

    public void showPopup() {
        popupView = initControl();
        setContentView(popupView);
        setAnimationStyle(R.style.AnimationOverFlow);

        mButtonView.postDelayed(new Runnable() {
            @Override
            public void run() {
                int[] locationParent = new int[2];
                mButtonView.getLocationOnScreen(locationParent);
                showAtLocation(mButtonView, Gravity.TOP | Gravity.RIGHT, 0, locationParent[1]);
            }
        }, 80);
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private View initControl() {
//        View view = inflater.inflate(R.layout.popup_overflow_menu, null, false);
        View view = getContentView();
        menuAdapter = new MenuAdapter(context);
        menuAdapter.setListItem(listItem);
        listView = (ListView) view.findViewById(R.id.menu_listview);
        // add footer and header
        listView.setAdapter(menuAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dismiss();
                ItemContextMenu item = new ItemContextMenu();
                if (parent != null) {
                    item = (ItemContextMenu) parent.getItemAtPosition(position);
                }
                if (clickHandler != null && item != null) {
                    clickHandler.onIconClickListener(view, item.getObj(), item.getActionTag());
                }
            }
        });
        return view;
    }

    /**
     * obj item
     */
    protected static class MenuAdapter extends BaseAdapter {
        private LayoutInflater layoutInflater;
        private Context mContext;
        private ArrayList<ItemContextMenu> mItems = new ArrayList<>();

        public MenuAdapter(Context context) {
            this.mContext = context;
            this.layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        // set list item
        public void setListItem(ArrayList<ItemContextMenu> list) {
            mItems = list;
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            ItemContextMenu item = (ItemContextMenu) getItem(position);
            if (item != null) {
                return item.getActionTag();
            } else {
                return -1;
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ItemContextMenu item = (ItemContextMenu) getItem(position);
            if (convertView == null) {
                convertView = layoutInflater.inflate(
                        R.layout.item_overflow_menu, parent, false);
            }
            TextView itemText = (TextView) convertView
                    .findViewById(R.id.item_overflow_menu_text);
            ImageView itemIcon = (ImageView) convertView
                    .findViewById(R.id.item_overflow_menu_icon);
            itemText.setText(item.getText());
            if (item.getImageRes() != -1) {
                itemIcon.setVisibility(View.VISIBLE);
                itemIcon.setImageResource(item.getImageRes());
            } else {
                itemIcon.setVisibility(View.GONE);
            }
            convertView.setTag(item);
            return convertView;
        }
    }
}
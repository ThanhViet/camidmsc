package com.metfone.selfcare.ui;

import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.MessageHelper;

/**
 * Created by huongnd on 11/28/2018.
 */

public class ReactionPopup implements PopupListener {
    private View mAnchorView = null;
    private View mContentView;
    private ReengMessage mReengMessage;
    private BottomSheetContextMenu mParent;
    private PopupWindow mPopup;
    private OnReactImageClickListener mListener;
    private ScaleAnimation mScaleZoomAnimation = new ScaleAnimation(0.2f, 1f, 0.2f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
    //ScaleAnimation scaleShrinkAnimation = new ScaleAnimation(2f, 1f, 2f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 1f);
    private ImageView mLike, mLove, mHuh, mSurprise, mSad, mSmile;
    private ImageView mLikePoint, mLovePoint, mHuhPoint, mSurprisePoint, mSadPoint, mSmilePoint;

    private BaseSlidingFragmentActivity activity;

    public ReactionPopup(BaseSlidingFragmentActivity context) {
        this.activity = context;
        mContentView = LayoutInflater.from(context).inflate(R.layout.popup_reaction_layout, null, false);
        mPopup = new PopupWindow(mContentView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopup.setContentView(mContentView);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mPopup.setElevation(10);
        }
        mPopup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mLike = (ImageView) mContentView.findViewById(R.id.like_item);
        mLove = (ImageView) mContentView.findViewById(R.id.love_item);
        mHuh = (ImageView) mContentView.findViewById(R.id.huh_item);
        mSurprise = (ImageView) mContentView.findViewById(R.id.surprise_item);
        mSad = (ImageView) mContentView.findViewById(R.id.sad_item);
        mSmile = (ImageView) mContentView.findViewById(R.id.smile_item);

        mLikePoint = (ImageView) mContentView.findViewById(R.id.like_item_point);
        mLovePoint = (ImageView) mContentView.findViewById(R.id.love_item_point);
        mHuhPoint = (ImageView) mContentView.findViewById(R.id.huh_item_point);
        mSurprisePoint = (ImageView) mContentView.findViewById(R.id.surprise_item_point);
        mSadPoint = (ImageView) mContentView.findViewById(R.id.sad_item_point);
        mSmilePoint = (ImageView) mContentView.findViewById(R.id.smile_item_point);

        mLike.setOnClickListener(onClickListener);
        mLove.setOnClickListener(onClickListener);
        //mDislike.setOnClickListener(onClickListener);
        mHuh.setOnClickListener(onClickListener);
        mSurprise.setOnClickListener(onClickListener);
        mSad.setOnClickListener(onClickListener);
        mSmile.setOnClickListener(onClickListener);
    }

    public ReactionPopup setReactionPoint(int point) {
        switch (point) {
            case 0:
                if (mLikePoint != null) {
                    mLikePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 1:
                if (mLovePoint != null) {
                    mLovePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 2:
                if (mSmilePoint != null) {
                    mSmilePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 3:
                if (mSurprisePoint != null) {
                    mSurprisePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 4:
                if (mSadPoint != null) {
                    mSadPoint.setVisibility(View.VISIBLE);
                }
                break;
            case 5:
                if (mHuhPoint != null) {
                    mHuhPoint.setVisibility(View.VISIBLE);
                }
                break;
            default:
                break;
        }
        return this;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == null || mListener == null) return;
            switch (v.getId()) {
                case R.id.like_item:
                    MessageHelper.showAnimReation(activity, mAnchorView, R.drawable.ic_reaction_like);
                    mListener.onReactImageClick(mReengMessage, ReengMessageConstant.Reaction.LIKE);
                    playReactionSoundEffect(mLikePoint);
                    break;

                case R.id.love_item:
                    MessageHelper.showAnimReation(activity, mAnchorView, R.drawable.ic_reaction_love);
                    mListener.onReactImageClick(mReengMessage, ReengMessageConstant.Reaction.LOVE);
                    playReactionSoundEffect(mLovePoint);
                    break;

                case R.id.huh_item:
                    MessageHelper.showAnimReation(activity, mAnchorView, R.drawable.ic_reaction_huh);
                    mListener.onReactImageClick(mReengMessage, ReengMessageConstant.Reaction.HUH);
                    playReactionSoundEffect(mHuhPoint);
                    break;

                case R.id.surprise_item:
                    MessageHelper.showAnimReation(activity, mAnchorView, R.drawable.ic_reaction_surprise);
                    mListener.onReactImageClick(mReengMessage, ReengMessageConstant.Reaction.SURPRISE);
                    playReactionSoundEffect(mSurprisePoint);
                    break;

                case R.id.sad_item:
                    MessageHelper.showAnimReation(activity, mAnchorView, R.drawable.ic_reaction_sad);
                    mListener.onReactImageClick(mReengMessage, ReengMessageConstant.Reaction.SAD);
                    playReactionSoundEffect(mSadPoint);
                    break;

                case R.id.smile_item:
                    MessageHelper.showAnimReation(activity, mAnchorView, R.drawable.ic_reaction_smile);
                    mListener.onReactImageClick(mReengMessage, ReengMessageConstant.Reaction.SMILE);
                    playReactionSoundEffect(mSmilePoint);
                    break;

                default:
                    break;
            }

            dismiss();
        }
    };


    private void playReactionSoundEffect(View view) {
        try {
            boolean isCancelReaction = view != null && view.getVisibility() == View.VISIBLE;
            MediaPlayer mp = MediaPlayer.create(ApplicationController.self(), isCancelReaction ? R.raw.reactions_cancel : R.raw.reactions_click);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                    mp.release();
                    mp = null;
                }

            });
            mp.setVolume(0.75f, 0.75f);
            mp.start();
        } catch (Exception e) {
        }
    }


    public ReactionPopup setReengMessage(ReengMessage reengMessage) {
        this.mReengMessage = reengMessage;
        return this;
    }

    public ReactionPopup setReactionClickListener(OnReactImageClickListener listener) {
        this.mListener = listener;
        return this;
    }

    public void setParent(BottomSheetContextMenu parent) {
        mParent = parent;
    }

    public static void showReactionPopup(BaseSlidingFragmentActivity context, View anchorView) {
        new ReactionPopup(context).setCancelable(true).setAnchorView(anchorView).show();
    }

    public static void showReactionPopup(BaseSlidingFragmentActivity context, View anchorView, ReengMessage reengMessage) {
        new ReactionPopup(context).setReengMessage(reengMessage).setCancelable(true).setAnchorView(anchorView).show();
    }

    public static ReactionPopup showReactionPopup(BaseSlidingFragmentActivity context, View anchorView, ReengMessage reengMessage, OnReactImageClickListener listener) {
        ReactionPopup reactionPopup = new ReactionPopup(context).setReengMessage(reengMessage).setCancelable(true).setReactionClickListener(listener).setAnchorView(anchorView);
        reactionPopup.show();
        return reactionPopup;
    }

    public static void showReactionPopupToTopOfAnchorView(BaseSlidingFragmentActivity context, View anchorView, ReengMessage reengMessage, OnReactImageClickListener listener) {
        new ReactionPopup(context).setReengMessage(reengMessage).setCancelable(true).setReactionClickListener(listener).setAnchorView(anchorView).showToTopOfAnchorView();
    }

    public static void showReactionPopupToTopOfAnchorView(BaseSlidingFragmentActivity context, View anchorView) {
        new ReactionPopup(context).setAnchorView(anchorView).showToTopOfAnchorView();
    }

    public static void showReactionPopupToTopOfAnchorView(BaseSlidingFragmentActivity context, View anchorView, boolean cancelable) {
        new ReactionPopup(context).setAnchorView(anchorView).setCancelable(cancelable).showToTopOfAnchorView();
    }

    public ReactionPopup setAnchorView(View view) {
        mAnchorView = view;
        return this;
    }

    public void show() {
        /*mPopup.showAsDropDown(mAnchorView);*/
        if (mAnchorView == null) return;
        View view = mPopup.getContentView();
        view.measure(0, 0);
        int popupWidth = view.getMeasuredWidth();
        mPopup.showAsDropDown(mAnchorView, -popupWidth + mAnchorView.getWidth(), 0);
        /*likeImageView.setBackgroundResource(R.drawable.like_image);
        AnimationDrawable frameAnimation = (AnimationDrawable) likeImageView.getBackground();
        frameAnimation.start();*/
        mScaleZoomAnimation.setDuration(400);
        mLove.startAnimation(mScaleZoomAnimation);
        mSmile.startAnimation(mScaleZoomAnimation);
        mSurprise.startAnimation(mScaleZoomAnimation);
        mHuh.startAnimation(mScaleZoomAnimation);
        mSad.startAnimation(mScaleZoomAnimation);
        mLike.startAnimation(mScaleZoomAnimation);
        //mDislike.startAnimation(mScaleZoomAnimation);
    }

    public void showToTopOfBottomSheet() {
        if (mAnchorView == null) return;
        View view = mPopup.getContentView();
        view.measure(0, 0);
        int popupWidth = view.getMeasuredWidth();
        int xoff = (-popupWidth) / 2;
        mPopup.showAsDropDown(mAnchorView, xoff, -view.getMeasuredHeight() - 40);

        /*likeImageView.setBackgroundResource(R.drawable.like_image);
        AnimationDrawable frameAnimation = (AnimationDrawable) likeImageView.getBackground();
        frameAnimation.start();*/
        mScaleZoomAnimation.setDuration(400);
        mLove.startAnimation(mScaleZoomAnimation);
        mSmile.startAnimation(mScaleZoomAnimation);
        mSurprise.startAnimation(mScaleZoomAnimation);
        mHuh.startAnimation(mScaleZoomAnimation);
        mSad.startAnimation(mScaleZoomAnimation);
        mLike.startAnimation(mScaleZoomAnimation);
    }

    public void showToTopOfAnchorView() {
        Rect location = locateView(mAnchorView);
        if (location == null) return;
        mPopup.showAtLocation(mAnchorView, Gravity.TOP | Gravity.LEFT, location.right, location.bottom);
        /*likeImageView.setBackgroundResource(R.drawable.like_image);
        AnimationDrawable frameAnimation = (AnimationDrawable) likeImageView.getBackground();
        frameAnimation.start();*/
        mScaleZoomAnimation.setDuration(400);
        mLove.startAnimation(mScaleZoomAnimation);
        mSmile.startAnimation(mScaleZoomAnimation);
        mSurprise.startAnimation(mScaleZoomAnimation);
        mHuh.startAnimation(mScaleZoomAnimation);
        mSad.startAnimation(mScaleZoomAnimation);
        mLike.startAnimation(mScaleZoomAnimation);
        //mDislike.startAnimation(mScaleZoomAnimation);
    }

    public void dismiss() {
        if (mPopup != null) {
            mPopup.dismiss();
        }
        if (mParent != null) {
            mParent.dismiss();
        }
        mListener = null;
        mParent = null;
        mReengMessage = null;
    }

    public ReactionPopup setCancelable(boolean cancelable) {
        if (mPopup != null) {
            mPopup.setOutsideTouchable(cancelable);
        }
        return this;
    }

    public Rect locateView(View v) {
        if (v == null || mPopup == null) return null;
        int[] loc_int = new int[2];
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        View view = mPopup.getContentView();
        view.measure(0, 0);
        int popupWidth = view.getMeasuredWidth();
        int popupHeight = view.getMeasuredHeight();
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left - popupWidth + v.getWidth();
        location.bottom = location.top - popupHeight - 20;
        return location;
    }

    @Override
    public void dismissPopup() {
        try {
            dismiss();
        } catch (Exception e) {
        }
    }

    public interface OnReactImageClickListener {
        void onReactImageClick(ReengMessage reengMessage, ReengMessageConstant.Reaction reaction);

    }
}

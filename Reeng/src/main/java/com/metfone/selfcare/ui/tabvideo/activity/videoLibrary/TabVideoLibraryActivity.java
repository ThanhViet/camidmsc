package com.metfone.selfcare.ui.tabvideo.activity.videoLibrary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.VideoConstant;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary.VideoLibraryFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TabVideoLibraryActivity extends BaseActivity implements TabVideoLibraryView {

    public static void start(Context context) {
        Intent starter = new Intent(context, TabVideoLibraryActivity.class);
        context.startActivity(starter);
    }

    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_library);
        ButterKnife.bind(this);

        viewPager.setAdapter(createAdapterPage());
        viewPager.setOffscreenPageLimit(2);
        viewPager.setCurrentItem(0);
        tab.setupWithViewPager(viewPager);
    }

    private PagerAdapter createAdapterPage() {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(this);
        creator.add(FragmentPagerItem.of(getString(R.string.watchLater), VideoLibraryFragment.class, VideoLibraryFragment.arguments(VideoConstant.Type.LATER.VALUE)));
        creator.add(FragmentPagerItem.of(getString(R.string.saved), VideoLibraryFragment.class, VideoLibraryFragment.arguments(VideoConstant.Type.SAVED.VALUE)));
        return new FragmentStatePagerItemAdapter(getSupportFragmentManager(), creator.create());
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }
}

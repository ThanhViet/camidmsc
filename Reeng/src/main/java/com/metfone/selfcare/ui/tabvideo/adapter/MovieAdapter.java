package com.metfone.selfcare.ui.tabvideo.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.content.MovieDetailHolder;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.util.Utilities;

public class MovieAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {

    public static final int TYPE_SPACE_HEADER = -2;
    public static final int TYPE_SPACE_BOTTOM = -1;
    public static final int TYPE_LOAD_MORE = 0;
    public static final int NORMAL = 1;
    private OnClickContentMovie listener;

    public MovieAdapter(Activity act) {
        super(act);
    }

    public void setListener(OnClickContentMovie listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object object = items.get(position);
        if (object instanceof Video) {
            return NORMAL;
        } else if (Utilities.equals(object, TYPE_SPACE_HEADER)) {
            return TYPE_SPACE_HEADER;
        } else if (Utilities.equals(object, TYPE_SPACE_BOTTOM)) {
            return TYPE_SPACE_BOTTOM;
        } else {
            return TYPE_LOAD_MORE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case NORMAL:
                return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_movie, parent, false), activity, listener).setShowPoster(true);
            case TYPE_SPACE_HEADER:
                return new SpaceHolder(activity, layoutInflater, parent, R.color.videoShimmerBackgroundColor);
            case TYPE_SPACE_BOTTOM:
                return new SpaceHolder(activity, layoutInflater, parent, android.R.color.white);
            default:
                return new LoadMoreHolder(activity, layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieDetailHolder) {
            ((MovieDetailHolder) holder).bindData(getItem(position), position);
        } else if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(items, position);
        }
    }

    public Object getItem(int position) {
        if (items != null && items.size() > position && position >= 0)
            return items.get(position);
        return null;
    }

    public static class SpaceHolder extends LoadMoreHolder {

        SpaceHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent, int color) {
            super(layoutInflater.inflate(R.layout.item_channel_space, parent, false));
            itemView.setBackgroundColor(ContextCompat.getColor(activity, color));
        }
    }

//    public static class MovieHolder extends ViewHolder implements View.OnClickListener {
//
//        @BindView(R.id.iv_movie)
//        ImageView ivMovie;
//        @BindView(R.id.tv_number)
//        TextView tvNumber;
//        @BindView(R.id.frame_video)
//        FrameLayout frameVideo;
//        @BindView(R.id.tv_title)
//        TextView tvTitle;
//
//        private Video mVideo;
//        private Activity mActivity;
//
//        MovieHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
//            super(layoutInflater.inflate(R.layout.item_movie, parent, false));
//
//            mActivity = activity;
//
//            int width = (ScreenManager.getWidth(activity) - Utilities.dpToPixels(8, activity.getResources()) * 4) / 3;
//            int height = (int) (width * 1.5);
//
//            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) frameVideo.getLayoutParams();
//            layoutParams.height = height;
//            layoutParams.width = width;
//            frameVideo.setLayoutParams(layoutParams);
//
//            itemView.setOnClickListener(this);
//        }
//
//        @Override
//        public void bindData(ArrayList<Object> items, int position) {
//            Object item = items.get(position);
//            if (item instanceof Video) {
//
//                mVideo = (Video) item;
//
//                if (mVideo.getFilmGroup() != null && Utilities.notEmpty(mVideo.getFilmGroup().getCurrentVideo())) {
//                    tvNumber.setVisibility(View.VISIBLE);
//                    tvNumber.setText(mVideo.getFilmGroup().getCurrentVideo());
//                } else
//                    tvNumber.setVisibility(View.GONE);
//
//                if (mVideo.getFilmGroup() != null)
//                    tvTitle.setText(mVideo.getFilmGroup().getGroupName());
//                if (mVideo.getFilmGroup() != null)
//                    ImageManager.showImageRounded(mVideo.getFilmGroup().getGroupImage(), ivMovie);
//            }
//        }
//
//        @Override
//        public void onClick(View v) {
//            ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail((BaseSlidingFragmentActivity) mActivity, mVideo);
////            VideoPlayerActivity.start(mActivity, mVideo, "", true);
//        }
//    }
}

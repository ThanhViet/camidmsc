package com.metfone.selfcare.ui.tabvideo.playVideo;

import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.di.MvpPresenter;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;

public interface VideoPlayerContact {

    interface VideoPlayerPresenter extends MvpPresenter {
        void getVideoDetail(Video currentVideo);
    }

    interface VideoPlayerView extends BaseView {
        void openVideoDetail(Video video);

        void openMovieDetail(Movie movie);

        void openDialogConfirmBack();

        void openMovieDetail(Movie movie, ArrayList<Movie> allEpisode);
    }
}

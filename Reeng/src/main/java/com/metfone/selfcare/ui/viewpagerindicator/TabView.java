package com.metfone.selfcare.ui.viewpagerindicator;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.metfone.selfcare.R;

public class TabView extends TextView {
	private int mIndex;
	private int mMaxTabWidth = 300;
	public TabView(Context context) {
		super(context, null, R.attr.vpiTabPageIndicatorStyle);
	}
	
	public TabView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TabView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		// Re-measure if we went beyond our maximum size.
		if (mMaxTabWidth > 0 && getMeasuredWidth() > mMaxTabWidth) {
			super.onMeasure(MeasureSpec.makeMeasureSpec(mMaxTabWidth,
					MeasureSpec.EXACTLY), heightMeasureSpec);
		}
	}
	public void setIndex(int i){
		mIndex = i;
	}
	public int getIndex() {
		return mIndex;
	}
	
}

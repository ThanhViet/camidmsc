package com.metfone.selfcare.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import android.view.MotionEvent;
import android.widget.SectionIndexer;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/6/15.
 */
public class IndexScroller {
    private Resources mRes;
    private float mIndexbarWidth;
    private float mIndexbarMarginRight;
    private float mIndexbarMarginText;
    private float mIndexbarMarginIndex;
    private float mIndexRound;
    private float mPreviewPadding;
    private float mDensity;
    private float mScaledDensity;
    private float mAlphaRate;
    private int mState = STATE_HIDDEN;
    private int mListViewWidth;
    private int mListViewHeight;
    private int mCurrentSection = -1;
    private boolean mIsIndexing = false;
    private RecyclerView mRecycle = null;
    private SectionIndexer mIndexer = null;
    private String[] mSections = null;
    private RectF mIndexbarRect;
    private boolean enablePreview = true;
    private Context mContext;

    private static final int STATE_HIDDEN = 0;
    private static final int STATE_SHOWING = 1;
    private static final int STATE_SHOWN = 2;
    private static final int STATE_HIDING = 3;
    private static final int STATE_INVISIBLE = 4;

    public IndexScroller(Context context, RecyclerView recycle) {
        this.mContext=context;
        mRes = context.getResources();
        mDensity = mRes.getDisplayMetrics().density;
        mScaledDensity = mRes.getDisplayMetrics().scaledDensity;
        mRecycle = recycle;
        setAdapter(mRecycle.getAdapter());
        mIndexbarWidth = 18 * mDensity;
        mIndexbarMarginIndex = 5 * mDensity;
        mIndexbarMarginRight = 2 * mDensity;
        mIndexbarMarginText = 6 * mDensity;
        mPreviewPadding = 15 * mDensity;
        mIndexRound = 4 * mDensity;
    }

    public void draw(Canvas canvas) {
        if (mState == STATE_HIDDEN) {
            return;
        }
        // mAlphaRate determines the rate of opacity
        Paint indexbarPaint = new Paint();
        // color index
        indexbarPaint.setColor(ContextCompat.getColor(mContext, R.color.bg_mocha));
        indexbarPaint.setAlpha((int) (127 * mAlphaRate));
        indexbarPaint.setAntiAlias(true);
        canvas.drawRoundRect(mIndexbarRect, mIndexRound, mIndexRound, indexbarPaint);
        if (mSections != null && mSections.length > 0) {
            // Preview is shown when mCurrentSection is set
            if (mCurrentSection >= 0) {
                // preview
                if (enablePreview) {
                    Paint previewPaint = new Paint();
                    previewPaint.setColor(ContextCompat.getColor(mContext, R.color.bg_mocha));
                    previewPaint.setAlpha(96);
                    previewPaint.setAntiAlias(true);
                    previewPaint.setShadowLayer(3, 0, 0, Color.argb(64, 0, 0, 0));
                    Paint previewTextPaint = new Paint();
                    previewTextPaint.setColor(ContextCompat.getColor(mContext, R.color.white));
                    previewTextPaint.setAntiAlias(true);
                    previewTextPaint.setTextSize(50 * mScaledDensity);
                    float previewTextWidth = previewTextPaint.measureText(mSections[mCurrentSection]);
                    float previewSize = 2 * mPreviewPadding + previewTextPaint.descent() - previewTextPaint.ascent();
                    RectF previewRect = new RectF((mListViewWidth - previewSize) / 2
                            , (mListViewHeight - previewSize) / 2
                            , (mListViewWidth - previewSize) / 2 + previewSize
                            , (mListViewHeight - previewSize) / 2 + previewSize);
                    canvas.drawRoundRect(previewRect, mIndexRound, mIndexRound, previewPaint);
                    canvas.drawText(mSections[mCurrentSection], previewRect.left + (previewSize - previewTextWidth) / 2 - 1
                            , previewRect.top + mPreviewPadding - previewTextPaint.ascent() + 1, previewTextPaint);
                }
            }
            Paint indexPaint = new Paint();
            indexPaint.setColor(ContextCompat.getColor(mContext, R.color.white));
            indexPaint.setAlpha((int) (255 * mAlphaRate));
            indexPaint.setAntiAlias(true);
            indexPaint.setTextSize(13 * mScaledDensity);
            float sectionHeight = (mIndexbarRect.height() - 2 * mIndexbarMarginText) / mSections.length;
            float paddingTop = (sectionHeight - (indexPaint.descent() - indexPaint.ascent())) / 2;
            for (int i = 0; i < mSections.length; i++) {
                float paddingLeft = (mIndexbarWidth - indexPaint.measureText(mSections[i])) / 2;
                canvas.drawText(mSections[i], mIndexbarRect.left + paddingLeft
                        , mIndexbarRect.top + mIndexbarMarginText + sectionHeight * i + paddingTop - indexPaint.ascent(), indexPaint);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (mRecycle == null) return false;
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // If down event occurs inside index bar region, start indexing
                if (mState == STATE_INVISIBLE) {// truong hop invisible scroll thi ko xu ly
                    return false;
                } else if (mState != STATE_HIDDEN && contains(ev.getX(), ev.getY())) {
                    setState(STATE_SHOWN);
                    // It demonstrates that the motion event started from index bar
                    mIsIndexing = true;
                    // Determine which section the point is in, and move the list to that section
                    mCurrentSection = getSectionByPoint(ev.getY());
                    mRecycle.scrollToPosition(mIndexer.getPositionForSection(mCurrentSection));
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (mIsIndexing) {
                    // If this event moves inside index bar
                    if (contains(ev.getX(), ev.getY())) {
                        // Determine which section the point is in, and move the list to that section
                        mCurrentSection = getSectionByPoint(ev.getY());
                        mRecycle.scrollToPosition(mIndexer.getPositionForSection(mCurrentSection));
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mIsIndexing) {
                    mIsIndexing = false;
                    mCurrentSection = -1;
                }
                if (mState == STATE_SHOWN) {
                    setState(STATE_HIDING);
                }
                break;
        }
        return false;
    }

    public void show() {
        // state invisible khong hien
        if (mState == STATE_INVISIBLE) {
            Log.i("Scroller", "state == invisible");
        } else if (mState == STATE_HIDDEN) {
            setState(STATE_SHOWING);
        } else if (mState == STATE_HIDING) {
            setState(STATE_HIDING);
        }
    }

    public void hide() {
        if (mState == STATE_SHOWN) {
            setState(STATE_HIDING);
        }
    }

    public void invisible() {
//        if (mState == STATE_SHOWN||mState == STATE_SHOWN)
        if (mState != STATE_INVISIBLE && mState != STATE_HIDDEN) {
            setState(STATE_HIDDEN);
        }
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        // hide scroll
        hide();
        mListViewWidth = w;
        mListViewHeight = h;
        mIndexbarRect = new RectF(w - mIndexbarMarginRight - mIndexbarWidth
                , mIndexbarMarginIndex
                , w - mIndexbarMarginRight
                , h - mIndexbarMarginIndex);
    }

    public void setAdapter(Adapter adapter) {
        if (adapter instanceof SectionIndexer) {
            mIndexer = (SectionIndexer) adapter;
            mSections = (String[]) mIndexer.getSections();
            if (mSections == null || mSections.length <= 0) {
                mState = STATE_INVISIBLE;
            } else {
                mState = STATE_HIDDEN;
            }
        }
    }

    private void setState(int state) {
        if (state < STATE_HIDDEN || state > STATE_HIDING) {
            return;
        }
        mState = state;
        switch (mState) {
            case STATE_HIDDEN:
                // Cancel any fade effect
                mHandler.removeMessages(0);
                break;
            case STATE_SHOWING:
                // Start to fade in
                mAlphaRate = 0;
                fade(0);
                break;
            case STATE_SHOWN:
                // Cancel any fade effect
                mHandler.removeMessages(0);
                break;
            case STATE_HIDING:
                // Start to fade out after three seconds
                mAlphaRate = 1;
                fade(3000);
                break;
        }
    }

    private boolean contains(float x, float y) {
        // Determine if the point is in index bar region, which includes the right margin of the bar
        return (x >= mIndexbarRect.left && y >= mIndexbarRect.top && y <= mIndexbarRect.top + mIndexbarRect.height());
    }

    private int getSectionByPoint(float y) {
        if (mSections == null || mSections.length == 0) {
            return 0;
        }
        if (y < mIndexbarRect.top + mIndexbarMarginText) {
            return 0;
        }
        if (y >= mIndexbarRect.top + mIndexbarRect.height() - mIndexbarMarginText) {
            return mSections.length - 1;
        }
        return (int) ((y - mIndexbarRect.top - mIndexbarMarginText) / ((mIndexbarRect.height() - 2 * mIndexbarMarginText) / mSections.length));
    }

    private void fade(long delay) {
        mHandler.removeMessages(0);
        mHandler.sendEmptyMessageAtTime(0, SystemClock.uptimeMillis() + delay);
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mRecycle == null) return;
            switch (mState) {
                case STATE_SHOWING:
                    // Fade in effect
                    mAlphaRate += (1 - mAlphaRate) * 0.2;
                    if (mAlphaRate > 0.9) {
                        mAlphaRate = 1;
                        setState(STATE_SHOWN);
                    }
                    mRecycle.invalidate();
                    fade(10);
                    break;
                case STATE_SHOWN:
                    // If no action, hide automatically
                    setState(STATE_HIDING);
                    break;
                case STATE_HIDING:
                    // Fade out effect
                    mAlphaRate -= mAlphaRate * 0.2;
                    if (mAlphaRate < 0.1) {
                        mAlphaRate = 0;
                        setState(STATE_HIDDEN);
                    }
                    mRecycle.invalidate();
                    fade(10);
                    break;
                default:
                    break;
            }
        }
    };
}

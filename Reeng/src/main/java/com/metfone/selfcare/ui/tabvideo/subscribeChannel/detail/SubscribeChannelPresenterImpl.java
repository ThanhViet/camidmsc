package com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallback;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.di.BasePresenter;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class SubscribeChannelPresenterImpl extends BasePresenter<SubscribeChannelContact.SubscribeChannelView> implements SubscribeChannelContact.SubscribeChannelPresenter {

    private final VideoNewPublishCallback mVideoNewPublishCallback;
    private final ChannelUserFollowCallback mChannelUserFollowCallback;
    private ArrayList<Video> videos;
    private ArrayList<Channel> channels;
    private String lastId = "";
    private int offset = 0;
    private boolean isLoadMore = false;
    private boolean errorApi = false;

    public SubscribeChannelPresenterImpl(SubscribeChannelContact.SubscribeChannelView view, ApplicationController application) {
        super(view, application);
        mVideoNewPublishCallback = new VideoNewPublishCallback();
        mChannelUserFollowCallback = new ChannelUserFollowCallback();
        videos = new ArrayList<>();
        channels = new ArrayList<>();
    }

    @Override
    public void openDetail(BaseSlidingFragmentActivity activity, Video video) {
        utils.openVideoDetail(activity, video);
    }

    @Override
    public void notifyLike(BaseSlidingFragmentActivity activity, Video video) {
        videoApi.likeOrUnlikeVideo(video);
        listenerUtils.notifyVideoLikeChangedData(video);
    }

    @Override
    public void openShare(BaseSlidingFragmentActivity activity, Video video) {
        utils.openShareMenu(activity, video);
    }

    @Override
    public void openComment(BaseSlidingFragmentActivity activity, Video video) {
        utils.openCommentVideo(activity, video);
    }

    @Override
    public void openMore(BaseSlidingFragmentActivity activity, Video video) {
        view.showDialogMoreOption(video);
    }

    @Override
    public void openDetail(BaseSlidingFragmentActivity activity, Channel channel) {
        utils.openChannelInfo(activity, channel);
    }

    @Override
    public void onInternetChanged() {
        super.onInternetChanged();
        if (NetworkHelper.isConnectInternet(application) && Utilities.isEmpty(videos) && errorApi)
            getVideoNewPublish();
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        super.onChannelSubscribeChanged(channel);
    }

    @Override
    protected void videoChangeData(Video video, Type type) {
        super.videoChangeData(video, type);
        if (Utilities.notEmpty(videos)) {
            for (Video itemVideo : videos) {
                if (video.getId().equals(itemVideo.getId())) {
                    switch (type) {
                        case LIKE:
                            itemVideo.setLike(video.isLike());
                            itemVideo.setTotalLike(video.getTotalLike());
                            break;
                        case SHARE:
                            itemVideo.setShare(video.isShare());
                            itemVideo.setTotalShare(video.getTotalShare());
                            break;
                        case COMMENT:
                            itemVideo.setTotalComment(video.getTotalComment());
                            break;
                        case WATCH_LATER:
                            itemVideo.setWatchLater(video.isWatchLater());
                            break;
                    }
                }
            }
            if (view != null)
                view.bindData(videos, isLoadMore);
        }
    }

    @Override
    public void getChannelUserFollow() {
        videoApi.getChannelUserFollow(0, mChannelUserFollowCallback);
    }

    @Override
    public void getVideoNewPublish() {
        videoApi.getVideoNewPublish(lastId, offset, mVideoNewPublishCallback);
    }

    @Override
    public void getRefreshVideoNewPublish() {
        lastId = "";
        offset = 0;
        getVideoNewPublish();
    }

    @Override
    public void getMoreVideoNewPublish() {
        getVideoNewPublish();
    }

    public class ChannelUserFollowCallback implements ApiCallback {

        public void onSuccess(ArrayList<Channel> results) {
            if (channels == null)
                channels = new ArrayList<>();
            else
                channels.clear();

            if (Utilities.notEmpty(results)) {
                for (Channel channel : results) {
                    long timeLocalNewVideo = ChannelDataSource.getInstance().getTimeNewChannel(channel.getId());
                    channel.setHaveNewVideo(channel.getLastPublishVideo() > timeLocalNewVideo);
                }
                channels.addAll(results);
            }
            if (view != null)
                view.bindData(channels);
            if (Utilities.isEmpty(channels) && view != null)
                view.showNotifyDataEmpty();
        }

        @Override
        public void onError(String s) {

        }

        @Override
        public void onComplete() {
            if (view != null) view.loadChannelUserFollowCompleted();
        }
    }

    public class VideoNewPublishCallback implements ApiCallback {

        public void onSuccess(String lastIdStr, ArrayList<Video> results) {
            errorApi = false;
            isLoadMore = false;
            lastId = lastIdStr;

            if (videos == null)
                videos = new ArrayList<>();
            if (offset == 0)
                videos.clear();

            if (results != null && !results.isEmpty()) {
                isLoadMore = true;
                offset = offset + 20;
                videos.addAll(results);
            }

            if (view != null)
                view.bindData(videos, isLoadMore);
        }

        @Override
        public void onError(String s) {
            errorApi = true;
            if (Utilities.isEmpty(videos) && view != null)
                view.showNotifyNotNetwork();
        }

        @Override
        public void onComplete() {
            if (view != null) view.loadVideoNewPublishCompleted();
        }
    }

}

package com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail;

import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter.ItemObject;

import java.util.ArrayList;

public interface VideoDetailView extends BaseView {
    /**
     * cập nhật dữ liệu vào giao diện
     *
     * @param items      danh sách dữ liệu
     * @param isLoadMore trạng thái loadMore
     */
    void updateData(ArrayList<ItemObject> items, boolean isLoadMore);

}

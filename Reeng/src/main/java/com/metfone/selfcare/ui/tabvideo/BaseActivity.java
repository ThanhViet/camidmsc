package com.metfone.selfcare.ui.tabvideo;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.di.ActivityComponent;
import com.metfone.selfcare.di.ActivityModule;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.di.DaggerActivityComponent;

/**
 * Created by tuanha00 on 3/22/2018.
 */

public class BaseActivity extends BaseSlidingFragmentActivity implements BaseView {

    protected boolean runnable;

    protected ApplicationController application;
    protected ActivityComponent activityComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (ApplicationController) getApplication();
        activityComponent = DaggerActivityComponent
                .builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(application.getApplicationComponent())
                .build();
        runnable = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        runnable = false;
    }

    public ApplicationController getApplicationController() {
        return application;
    }
}

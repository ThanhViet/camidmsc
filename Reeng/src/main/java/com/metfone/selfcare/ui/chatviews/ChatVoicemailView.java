package com.metfone.selfcare.ui.chatviews;

import android.Manifest;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.StopWatch;
import com.metfone.selfcare.helper.voicemail.VoicemailRecorder;
import com.metfone.selfcare.ui.ProgressWheel;
import com.metfone.selfcare.ui.ScreenStateInfo;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.util.Log;

import java.io.File;

import io.reactivex.functions.Consumer;

/**
 * Created by thaodv on 5/28/2015.
 */
public class ChatVoicemailView {
    private static final String TAG = ChatVoicemailView.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private ChatActivity mActivity;
    private TextView tvElapsedTime, sendVoicemailNotice, mTvwIntro;
    private ImageView sendVoicemailIcon;
    private View animationView;
    private ProgressWheel progressWheel;
    private RelativeLayout progressWheelLayout;
    private boolean isClickVoiceMail = false;
    private boolean isVoicemailRecording = false;
    private VoicemailRecorder voicemailRecorder;
    private Handler mHandler;
    private StopWatch stopWatch;
    private OnVoicemailListener voicemailListener;
    private ScreenStateInfo mScreenStateInfo;
    private int progressSizePortrait, progressSizeLandScape;
    private int iconVoiceWidthPortrait, iconVoiceWidthLandScape;
    private int iconVoiceHeightPortrait, iconVoiceHeightLandScape;
    private Animation wheelAnimation;

    private RxPermissions rxPermissions;

    public ChatVoicemailView(ChatActivity activity, OnVoicemailListener listener, ScreenStateInfo screenStateInfo) {
        mActivity = activity;
        mApplication = (ApplicationController) activity.getApplication();
        mRes = activity.getResources();
        mHandler = new Handler();
        voicemailListener = listener;
        mScreenStateInfo = screenStateInfo;
        initSize();

        rxPermissions = new RxPermissions(activity);
        rxPermissions.setLogging(BuildConfig.DEBUG);

        wheelAnimation = AnimationUtils.loadAnimation(activity,R.anim.wheel_record);
    }

    private void initSize() {
        //progress
        progressSizePortrait = (int) mRes.getDimension(R.dimen.reeng_progress_wheel_diameter);
        progressSizeLandScape = mApplication.getWidthPixels() / 3;
        //icon
        iconVoiceWidthPortrait = (int) mRes.getDimension(R.dimen.progress_voice_mail_icon_w);
        iconVoiceHeightPortrait = (int) mRes.getDimension(R.dimen.progress_voice_mail_icon_h);
        iconVoiceHeightLandScape = progressSizeLandScape / 4;
        iconVoiceWidthLandScape = (iconVoiceHeightLandScape * 7) / 9;
    }

    public void setChatVoicemailView(View voiceView) {
        tvElapsedTime = voiceView.findViewById(R.id.voicemail_send_stopwatch);
        sendVoicemailNotice = voiceView.findViewById(R.id.send_voicemail_notice);
        sendVoicemailIcon = voiceView.findViewById(R.id.voicemail_send_mic);
        mTvwIntro = voiceView.findViewById(R.id.tvw_voicemail_intro);
        progressWheelLayout = voiceView.findViewById(R.id.progress_voicemail_layout);
        animationView = voiceView.findViewById(R.id.animationView);
        progressWheel = voiceView.findViewById(R.id.progress_voicemail);
        progressWheel.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                Log.i(TAG, "long click");

                if (rxPermissions.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && rxPermissions.isGranted(Manifest.permission.RECORD_AUDIO)) {
                    startVoicemailRecording(progressWheel);
                } else
                    rxPermissions
                            .requestEach(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.RECORD_AUDIO)
                            .subscribe(new Consumer<Permission>() {
                                @Override
                                public void accept(Permission permission) throws Exception {
                                    android.util.Log.i(TAG, "Permission result " + permission);
                                    if (Manifest.permission.RECORD_AUDIO.equals(permission.name)) {
                                        if (permission.granted) {
                                            Log.i(TAG, "permission.granted: " + permission);
//                                            startVoicemailRecording(progressWheel);
                                        } else if (permission.shouldShowRequestPermissionRationale) {
                                            // Denied permission without ask never again
                                            Log.i(TAG, "Denied permission without ask never again");
                                        } else {
                                            // Denied permission with ask never again
                                            // Need to go to the settings
                                            showDialogNeverShowPermissionAgain();
                                        }
                                    }

                                }
                            });


                /*if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    PermissionHelper.requestPermission(mActivity,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Constants.PERMISSION.PERMISSION_REQUEST_WRITE_STORAGE);
                } else if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.RECORD_AUDIO)) {
                    PermissionHelper.requestPermission(mActivity,
                            Manifest.permission.RECORD_AUDIO,
                            Constants.PERMISSION.PERMISSION_REQUEST_RECORD_AUDIO);
                } else {
                    startVoicemailRecording(progressWheel);
                }*/
                return false;
            }
        });

        progressWheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isClickVoiceMail) {
                    mTvwIntro.setText(mRes.getString(R.string.record_voicemail_intro_recoding_0));
                }
            }
        });
        changeLayoutParamProgress(mScreenStateInfo.getScreenOrientation());
        // set su kien keyDown xuong progressWheel
        progressWheel.setOnTouchListener(new View.OnTouchListener() {
            private Rect rect;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Log.i(TAG, "setOnTouchListener: "+event.getpo());
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        //TODO cmt
                        //    mKeyboardController.setChatbarStateDisable();
                        progressWheel.setBackgroundResource(R.drawable.progress_wheel_background_pressed);
                        v.setPressed(true);
                        rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                        isClickVoiceMail = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (isVoicemailRecording) {
                            stopVoicemailRecording(); // stop voicemail recording
                            if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
                                deleteRecordedFile();
                            } else {
                                sendVoicemail();
                            }
                            resetSendVoicemailUI();
                        }
                        sendVoicemailNotice.setText(R.string.send_voicemail_notice_hold);
                        mTvwIntro.setText(mRes.getString(R.string.record_voicemail_intro_recoding_0));
                        //mKeyboardController.setActiveIntroText(false, "");
                        progressWheel.setBackgroundResource(R.drawable.selector_progress_wheel);
                        // color
                        progressWheel.setContourColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                        progressWheel.setBarColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                        //TODO cmt
//                        mKeyboardController.setChatbarStateWhenDisableFinish();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
                            if (isVoicemailRecording) {
                                mTvwIntro.setText(mRes.getString(R.string.record_voicemail_intro_recoding_2));
                                sendVoicemailNotice.setText(mRes.getString(R.string.send_voicemail_notice_hold));
                                // color
                                progressWheel.setContourColor(ContextCompat.getColor(mActivity, R.color.red));
                                progressWheel.setBarColor(ContextCompat.getColor(mActivity, R.color.red));
                            }
                        } else {
                            if (isVoicemailRecording) {
                                mTvwIntro.setText(mRes.getString(R.string.record_voicemail_intro_recoding_1));
                                sendVoicemailNotice.setText(mRes.getString(R.string.send_voicemail_notice_release));
                                // color
                                progressWheel.setContourColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                                progressWheel.setBarColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                            }
                        }
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        if (isVoicemailRecording) {
                            stopVoicemailRecording(); // stop voicemail recording
                            sendVoicemail();
                            resetSendVoicemailUI();
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    private void showDialogNeverShowPermissionAgain() {
        Log.i(TAG, "Permission denied, can't enable the camera");
        DialogMessage dialogMessage = new DialogMessage(mActivity, true);
        dialogMessage.setMessage(mRes.getString(R.string.need_permission_micro));
        dialogMessage.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", mApplication.getPackageName(), null);
                intent.setData(uri);
                mActivity.startActivity(intent);
            }
        });
        dialogMessage.show();
    }

    private void startVoicemailRecording(final ProgressWheel progressWheel) {
        //Pause nhac khi ghi am
        animationView.setVisibility(View.VISIBLE);
        animationView.startAnimation(wheelAnimation);
        if (mApplication.getPlayMusicController() != null) {
            if (mApplication.getPlayMusicController().isPlaying()) {
                Log.i(TAG, "Media is playing");
                mApplication.getPlayMusicController().setStateResumePlaying(true);
                mApplication.getPlayMusicController().toggleMusic();
            }
            mApplication.getPlayMusicController().setOtherAudioPlaying(true);
        }
        VideoService.stop(mApplication);
        //show Intro
        mTvwIntro.setText(mRes.getString(R.string.record_voicemail_intro_recoding_1));
        isVoicemailRecording = true;
        sendVoicemailIcon.setPressed(true);
        tvElapsedTime.setText("00:00");
        progressWheel.setPressed(true);
        //progressWheel.setContourColor(getResources().getColor(R.color.white));
        sendVoicemailNotice.setText(mRes.getString(R.string.send_voicemail_notice_release));
        //        khong cho khoa man hinh khi dang ghi am
        mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        voicemailRecorder = new VoicemailRecorder();
        voicemailRecorder.startRecording();
        stopWatch = new StopWatch();
        stopWatch.start();
        final int miliSecondToSleep = 100;
        new Thread(new Runnable() {
            public void run() {
                int recordLength = 0;
                while (isVoicemailRecording) {
                    recordLength += miliSecondToSleep;
                    if (stopWatch.getElapsedTime() <= (Constants.MESSAGE.VOICEMAIL_MAX_LENGTH)) {
                        int progress = 360 * recordLength
                                / Constants.MESSAGE.VOICEMAIL_MAX_LENGTH;
                        Log.d(TAG, "Update-Progressbar");
                        progressWheel.setProgress(progress);
                        progressWheel.incrementProgress();
                        mHandler.post(new Runnable() {
                            public void run() {
                                if (!TextUtils.isEmpty(tvElapsedTime.getText()))
                                    tvElapsedTime.setText(stopWatch.getElapsedTimeInMMSSForm());
                            }
                        });
                    } else { // stop recording because timeout
                        stopVoicemailRecording(); // stop voicemail recording
                        resetSendVoicemailUI();
                        sendVoicemail();
                    }
                    try {
                        Thread.sleep(miliSecondToSleep);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
        }).start();

    }

    public void stopVoicemailRecording() {
        stopWatch.stop();
        isVoicemailRecording = false;
        if (voicemailRecorder != null) {
            voicemailRecorder.stopRecording();
        }
        // cho phep khoa man hinh tro lai
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                mActivity.getWindow().clearFlags(
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
        //resume nhac khi dung khi am
        if (mApplication.getPlayMusicController() != null) {
            mApplication.getPlayMusicController().checkAndResumeMusic();
        }
        animationView.setVisibility(View.INVISIBLE);
        animationView.clearAnimation();
    }

    private void sendVoicemail() {
        // check if error on recording
        if (!voicemailRecorder.isRecordSuccess()) {
            Log.d(TAG, "!voicemailRecorder.isRecordSuccess()");
            onRecordFailure();
            resetSendVoicemailUI();
            /*if (mKeyboardController != null) {
                View voiceView = mKeyboardController.getLayoutVoice();
                ProgressWheel progressWheel = (ProgressWheel) voiceView.findViewById(R.id.progress_voicemail);
                resetSendVoicemailUI(progressWheel);
                mKeyboardController.hideSoftAndCustomKeyboard();
            }*/
            return;
        }
        File recordedFile = voicemailRecorder.getRecordedAudioFile();
        if (recordedFile == null) {
            onRecordFailure();
            return;
        }
        if ((int) stopWatch.getElapsedTimeSecs() < Constants.FILE.VOICE_MIN_DURATION) {
            //xoa file nay di
            deleteRecordedFile();
            return;
        }
        isClickVoiceMail = true; // click - send
        //TODO send voicemail
        voicemailListener.onSendVoicemailMessage(recordedFile.getAbsolutePath(), (int) stopWatch.getElapsedTimeSecs()
                , recordedFile.getName());
    }

    public void resetSendVoicemailUI() {
        if (progressWheel != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    tvElapsedTime.setText("");
                    sendVoicemailNotice.setText(mActivity.getString(R.string.send_voicemail_notice_hold));
                    mTvwIntro.setText(mRes.getString(R.string.record_voicemail_intro_recoding_0));
                    progressWheel.setProgress(0);
                    progressWheel.setPressed(false);
                    // color
                    progressWheel.setContourColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                    progressWheel.setBarColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                    progressWheel.setBackgroundResource(R.drawable.selector_progress_wheel);
                    sendVoicemailIcon.setPressed(false);
                }
            });
        }
    }

    public void deleteRecordedFile() {
        if (voicemailRecorder == null)
            return;
        File recordedFile = voicemailRecorder.getRecordedAudioFile();
        if (recordedFile != null && recordedFile.exists()) {
            recordedFile.delete();
        }
    }

    public boolean isVoicemailRecording() {
        return isVoicemailRecording;
    }

    private void onRecordFailure() {
        mActivity.showToast(R.string.record_voicemail_fail);
    }

    public void onResume() {

    }

    public void changeLayoutParamProgress(int orientation) {
        if (progressWheelLayout == null || progressWheel == null ||
                sendVoicemailIcon == null || tvElapsedTime == null || sendVoicemailNotice == null) {
            return;
        }
        LinearLayout.LayoutParams paramsProgressLayout = (LinearLayout.LayoutParams) progressWheelLayout
                .getLayoutParams();
        RelativeLayout.LayoutParams paramsProgress = (RelativeLayout.LayoutParams) progressWheel.getLayoutParams();
        LinearLayout.LayoutParams paramsProgressIcon = (LinearLayout.LayoutParams) sendVoicemailIcon.getLayoutParams();
        if (!DeviceHelper.isTablet(mApplication) && orientation == Configuration.ORIENTATION_LANDSCAPE) {
            paramsProgressLayout.height = progressSizeLandScape;
            paramsProgressLayout.width = progressSizeLandScape;
            paramsProgress.height = progressSizeLandScape;
            paramsProgress.width = progressSizeLandScape;
            paramsProgressIcon.height = iconVoiceHeightLandScape;
            paramsProgressIcon.width = iconVoiceWidthLandScape;
            tvElapsedTime.setTextSize(11.0f);
            sendVoicemailNotice.setTextSize(10.0f);
            mTvwIntro.setTextSize(11.0f);
        } else {
            paramsProgressLayout.height = progressSizePortrait;
            paramsProgressLayout.width = progressSizePortrait;
            paramsProgress.height = progressSizePortrait;
            paramsProgress.width = progressSizePortrait;
            paramsProgressIcon.height = iconVoiceHeightPortrait;
            paramsProgressIcon.width = iconVoiceWidthPortrait;
            tvElapsedTime.setTextSize(14.0f);
            sendVoicemailNotice.setTextSize(14.0f);
            mTvwIntro.setTextSize(14.0f);
        }
    }

    public interface OnVoicemailListener {
        void onSendVoicemailMessage(String filePath, int elapsedTimeSecs, String fileName);
    }
}
package com.metfone.selfcare.ui;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.metfone.selfcare.util.Log;

public class PreviewPager extends ViewPager {

    private static final String TAG = PreviewPager.class.getSimpleName();

    private boolean isLocked;

    public PreviewPager(Context context) {
        super(context);
        isLocked = false;
    }

    public PreviewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        isLocked = false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!isLocked) {
            try {
                return super.onInterceptTouchEvent(ev);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException", e);
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isLocked) {
            return super.onTouchEvent(event);
        }
        return false;
    }
}

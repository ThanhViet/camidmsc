package com.metfone.selfcare.ui.tabvideo.activity.uploadVideo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.callback.OnCategoryCallback;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.model.tab_video.Category;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device.UploadVideoDeviceFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.metfone.selfcare.helper.Constants.TabVideo.CURRENT_POSITION;
import static com.metfone.selfcare.helper.Constants.TabVideo.UPLOAD_YOUTUBE;

public class UploadVideoActivity extends BaseActivity {

    private int mCurrentPosition = UPLOAD_YOUTUBE;

    public static void start(Context context) {
        Intent starter = new Intent(context, UploadVideoActivity.class);
        context.startActivity(starter);
    }

    public static void start(Context context, int currentPosition) {
        Intent starter = new Intent(context, UploadVideoActivity.class);
        starter.putExtra(CURRENT_POSITION, currentPosition);
        context.startActivity(starter);
    }

    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.swipyRefreshLayout)
    SwipeRefreshLayout swipyRefreshLayout;

    @Inject
    VideoApi videoApi;

    @Inject
    Utils utils;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
        setContentView(R.layout.activity_upload_video);
        ButterKnife.bind(this);
        if (getIntent() != null && getIntent().hasExtra(CURRENT_POSITION)) {
            mCurrentPosition = getIntent().getIntExtra(CURRENT_POSITION, UPLOAD_YOUTUBE);
        }
        swipyRefreshLayout.post(callAdiRunnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (swipyRefreshLayout != null && callAdiRunnable != null)
            swipyRefreshLayout.removeCallbacks(callAdiRunnable);
    }

    private Runnable callAdiRunnable = new Runnable() {
        @Override
        public void run() {
            if (swipyRefreshLayout != null)
                swipyRefreshLayout.setRefreshing(true);
            if (videoApi != null)
                videoApi.getCategoryUpload(categoryCallback);
        }
    };

    private OnCategoryCallback categoryCallback = new OnCategoryCallback() {
        @Override
        public void onGetCategoriesSuccess(ArrayList<Category> list, ArrayList<AdsRegisterVip> listAds) {
            if (utils != null && viewPager != null && tab != null && swipyRefreshLayout != null) {
                swipyRefreshLayout.setRefreshing(false);
                swipyRefreshLayout.setEnabled(false);

                utils.saveCategories(list);
                PagerAdapter adapter = createAdapterPage();
                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(2);
                viewPager.setCurrentItem(mCurrentPosition >= adapter.getCount() ? 0 : mCurrentPosition);

                tab.setupWithViewPager(viewPager);
            }
        }

        @Override
        public void onGetCategoriesError(String s) {
            if (runnable) {
                showToast(R.string.e601_error_but_undefined);
                finish();
            }
        }

        @Override
        public void onGetCategoriesComplete() {

        }
    };

    private PagerAdapter createAdapterPage() {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(this);
        //creator.add(FragmentPagerItem.of(getString(R.string.uploadFromYoutube), UploadVideoYoutubeFragment.class));
        creator.add(FragmentPagerItem.of(getString(R.string.uploadFromDevice), UploadVideoDeviceFragment.class));
        return new FragmentStatePagerItemAdapter(getSupportFragmentManager(), creator.create());
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        hideKeyboard();
        finish();
    }

    @Override
    public void onBackPressed() {
        hideKeyboard();
        super.onBackPressed();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (inputMethodManager != null && view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}

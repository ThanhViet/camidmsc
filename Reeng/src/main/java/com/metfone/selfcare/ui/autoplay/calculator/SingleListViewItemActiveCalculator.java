package com.metfone.selfcare.ui.autoplay.calculator;

import android.view.View;


import com.metfone.selfcare.ui.autoplay.items.ListItem;
import com.metfone.selfcare.ui.autoplay.items.ListItemData;
import com.metfone.selfcare.ui.autoplay.utils.ItemsPositionGetter;
import com.metfone.selfcare.ui.autoplay.utils.ScrollDirectionDetector;
import com.metfone.selfcare.util.Log;

import java.util.List;

/**
 * Created by toanvk2 on 1/18/2018.
 */

public class SingleListViewItemActiveCalculator extends BaseItemsVisibilityCalculator {
    private static final String TAG = SingleListViewItemActiveCalculator.class.getSimpleName();
    private static final int INACTIVE_LIST_ITEM_VISIBILITY_PERCENTS = 70;
    private final Callback<ListItem> mCallback;
    private List<? extends ListItem> mListItems;
    private ScrollDirectionDetector.ScrollDirection mScrollDirection = ScrollDirectionDetector.ScrollDirection.DOWN;

    private final ListItemData mCurrentItem = new ListItemData();

    public SingleListViewItemActiveCalculator(List<? extends ListItem> listItems) {
        mCallback = new DefaultSingleItemCalculatorCallback();
        mListItems = listItems;
    }

    public void changeListItem(List<? extends ListItem> listItems) {
        this.mListItems = listItems;
    }

    @Override
    protected void onStateTouchScroll(ItemsPositionGetter itemsPositionGetter) {
        Log.i(TAG, ">> onStateTouchScroll, mScrollDirection " + mScrollDirection);
        ListItemData listItemData = mCurrentItem;
        Log.i(TAG, "onStateTouchScroll, listItemData " + listItemData);
        calculateActiveItem(itemsPositionGetter, listItemData);
        Log.i(TAG, "<< onStateTouchScroll, mScrollDirection " + mScrollDirection);
    }

    private void findNextItem(ItemsPositionGetter itemsPositionGetter, ListItemData currentIem, ListItemData outNextItemData) {
        int nextItemVisibilityPercents = 0;
        int nextItemIndex = currentIem.getIndex() + 1;
        Log.i(TAG, "findNextItem, nextItemIndex " + nextItemIndex);
        if (nextItemIndex < mListItems.size()) {
            int indexOfCurrentView = itemsPositionGetter.indexOfChild(currentIem.getView());
            Log.i(TAG, "findNextItem, indexOfCurrentView " + indexOfCurrentView);
            if (indexOfCurrentView >= 0) {
                View nextView = itemsPositionGetter.getChildAt(indexOfCurrentView + 1);
                if (nextView != null) {
                    ListItem next = mListItems.get(nextItemIndex);
                    Log.i(TAG, "findNextItem, next " + next + ", nextView " + nextView);
                    nextItemVisibilityPercents = next.getVisibilityPercents(nextView);
                    outNextItemData.fillWithData(nextItemIndex, nextView);
                } else {
                    Log.i(TAG, "findNextItem, nextView null. There is no view next to current");
                }
            } else {
                Log.i(TAG, "findNextItem, current view is no longer attached to listView");
            }
        }
        Log.i(TAG, "findNextItem, nextItemVisibilityPercents " + nextItemVisibilityPercents);
    }

    private void findPreviousItem(ItemsPositionGetter itemsPositionGetter, ListItemData currentIem, ListItemData outPreviousItemData) {
        int previousItemVisibilityPercents = 0;
        int previousItemIndex = currentIem.getIndex() - 1;
        Log.i(TAG, "findPreviousItem, previousItemIndex " + previousItemIndex);
        if (previousItemIndex >= 0) {
            int indexOfCurrentView = itemsPositionGetter.indexOfChild(currentIem.getView());
            Log.i(TAG, "findPreviousItem, indexOfCurrentView " + indexOfCurrentView);
            if (indexOfCurrentView > 0) {
                View previousView = itemsPositionGetter.getChildAt(indexOfCurrentView - 1);
                ListItem previous = mListItems.get(previousItemIndex);
                Log.i(TAG, "findPreviousItem, previous " + previous + ", previousView " + previousView);
                previousItemVisibilityPercents = previous.getVisibilityPercents(previousView);
                outPreviousItemData.fillWithData(previousItemIndex, previousView);
            } else {
                Log.i(TAG, "findPreviousItem, current view is no longer attached to listView");
            }
        }
        Log.i(TAG, "findPreviousItem, previousItemVisibilityPercents " + previousItemVisibilityPercents);
    }

    @Override
    public void onScrollStateIdle(ItemsPositionGetter itemsPositionGetter, int firstVisiblePosition, int lastVisiblePosition) {
        Log.i(TAG, "onScrollStateIdle, firstVisiblePosition " + firstVisiblePosition + ", lastVisiblePosition " + lastVisiblePosition);
        calculateMostVisibleItem(itemsPositionGetter, firstVisiblePosition, lastVisiblePosition);
    }

    private void calculateMostVisibleItem(ItemsPositionGetter itemsPositionGetter, int firstVisiblePosition, int lastVisiblePosition) {
        ListItemData mostVisibleItem = getMockCurrentItem(itemsPositionGetter, firstVisiblePosition, lastVisiblePosition);
        int maxVisibilityPercents = mostVisibleItem.getVisibilityPercents(mListItems);
        switch (mScrollDirection) {
            case UP:
                bottomToTopMostVisibleItem(itemsPositionGetter, maxVisibilityPercents, mostVisibleItem);
                break;
            case DOWN:
                topToBottomMostVisibleItem(itemsPositionGetter, maxVisibilityPercents, mostVisibleItem);
                break;
            default:
                throw new RuntimeException("not handled mScrollDirection " + mScrollDirection);
        }
        Log.i(TAG, "topToBottomMostVisibleItem, mostVisibleItem " + mostVisibleItem);
        if (mostVisibleItem.isMostVisibleItemChanged()) {
            Log.i(TAG, "topToBottomMostVisibleItem, item changed");
            setCurrentItem(mostVisibleItem);
        } else {
            Log.i(TAG, "topToBottomMostVisibleItem, item not changed");
        }
    }

    private void topToBottomMostVisibleItem(ItemsPositionGetter itemsPositionGetter, int maxVisibilityPercents, ListItemData outMostVisibleItem) {
        int mostVisibleItemVisibilityPercents = maxVisibilityPercents;
        int currentItemVisibilityPercents;
        for (int indexOfCurrentItem = itemsPositionGetter.getFirstVisiblePosition(), indexOfCurrentView = itemsPositionGetter.indexOfChild(outMostVisibleItem.getView())
             ; indexOfCurrentView < itemsPositionGetter.getChildCount() // iterating via listView Items
                ; indexOfCurrentItem++, indexOfCurrentView++) {
            Log.i(TAG, "topToBottomMostVisibleItem, indexOfCurrentView " + indexOfCurrentView);
            ListItem listItem = mListItems.get(indexOfCurrentItem);
            View currentView = itemsPositionGetter.getChildAt(indexOfCurrentView);
            currentItemVisibilityPercents = listItem.getVisibilityPercents(currentView);
            Log.i(TAG, "topToBottomMostVisibleItem, currentItemVisibilityPercents " + currentItemVisibilityPercents);
            Log.i(TAG, "topToBottomMostVisibleItem, mostVisibleItemVisibilityPercents " + mostVisibleItemVisibilityPercents);
            if (currentItemVisibilityPercents > mostVisibleItemVisibilityPercents) {
                mostVisibleItemVisibilityPercents = currentItemVisibilityPercents;
                outMostVisibleItem.fillWithData(indexOfCurrentItem, currentView);
            }
        }
        View currentItemView = mCurrentItem.getView();
        View mostVisibleView = outMostVisibleItem.getView();
        // set if newly found most visible view is different from previous most visible view
        boolean itemChanged = currentItemView != mostVisibleView;
        Log.i(TAG, "topToBottomMostVisibleItem, itemChanged " + itemChanged);
        outMostVisibleItem.setMostVisibleItemChanged(itemChanged);
        Log.i(TAG, "topToBottomMostVisibleItem, outMostVisibleItem index " + outMostVisibleItem.getIndex() + ", outMostVisibleItem view " + outMostVisibleItem.getView());
    }

    private void bottomToTopMostVisibleItem(ItemsPositionGetter itemsPositionGetter, int maxVisibilityPercents, ListItemData outMostVisibleItem) {
        int mostVisibleItemVisibilityPercents = maxVisibilityPercents;

        int currentItemVisibilityPercents;
        for (int indexOfCurrentItem = itemsPositionGetter.getLastVisiblePosition(), indexOfCurrentView = itemsPositionGetter.indexOfChild(outMostVisibleItem.getView())
             ; indexOfCurrentView >= 0 // iterating via listView Items
                ; indexOfCurrentItem--, indexOfCurrentView--) {
            Log.i(TAG, "bottomToTopMostVisibleItem, indexOfCurrentView " + indexOfCurrentView);
            ListItem listItem = mListItems.get(indexOfCurrentItem);
            View currentView = itemsPositionGetter.getChildAt(indexOfCurrentView);
            currentItemVisibilityPercents = listItem.getVisibilityPercents(currentView);
            Log.i(TAG, "bottomToTopMostVisibleItem, currentItemVisibilityPercents " + currentItemVisibilityPercents);

            if (currentItemVisibilityPercents > mostVisibleItemVisibilityPercents) {
                mostVisibleItemVisibilityPercents = currentItemVisibilityPercents;
                outMostVisibleItem.fillWithData(indexOfCurrentItem, currentView);
            }
            View currentItemView = mCurrentItem.getView();
            View mostVisibleView = outMostVisibleItem.getView();
            // set if newly found most visible view is different from previous most visible view
            boolean itemChanged = currentItemView != mostVisibleView;
            Log.i(TAG, "topToBottomMostVisibleItem, itemChanged " + itemChanged);
            outMostVisibleItem.setMostVisibleItemChanged(itemChanged);
        }
        Log.i(TAG, "bottomToTopMostVisibleItem, outMostVisibleItem " + outMostVisibleItem);
    }

    private ListItemData getMockCurrentItem(ItemsPositionGetter itemsPositionGetter, int firstVisiblePosition, int lastVisiblePosition) {
        Log.i(TAG, "getMockCurrentItem, mScrollDirection " + mScrollDirection
                + " ,firstVisiblePosition " + firstVisiblePosition
                + " ,lastVisiblePosition " + lastVisiblePosition);
        ListItemData mockCurrentItemData;
        switch (mScrollDirection) {
            case UP:
                int lastVisibleItemIndex;
                if (lastVisiblePosition < 0/*-1 may be returned from ListView*/) {
                    lastVisibleItemIndex = firstVisiblePosition;
                } else {
                    lastVisibleItemIndex = lastVisiblePosition;
                }
                mockCurrentItemData = new ListItemData().fillWithData(lastVisibleItemIndex, itemsPositionGetter.getChildAt(itemsPositionGetter.getChildCount() - 1));
                break;
            case DOWN:
                mockCurrentItemData = new ListItemData().fillWithData(firstVisiblePosition, itemsPositionGetter.getChildAt(0/*first visible*/));
                break;

            default:
                throw new RuntimeException("not handled mScrollDirection " + mScrollDirection);
        }
        return mockCurrentItemData;
    }

    private void calculateActiveItem(ItemsPositionGetter itemsPositionGetter, ListItemData listItemData) {
        int currentItemVisibilityPercents = listItemData.getVisibilityPercents(mListItems);
        Log.i(TAG, "calculateActiveItem, mScrollDirection " + mScrollDirection);
        ListItemData neighbourItemData = new ListItemData();
        switch (mScrollDirection) {
            case UP:
                findPreviousItem(itemsPositionGetter, listItemData, neighbourItemData);
                break;
            case DOWN:
                findNextItem(itemsPositionGetter, listItemData, neighbourItemData);
                break;
        }
        Log.i(TAG, "calculateActiveItem, currentItemVisibilityPercents " + currentItemVisibilityPercents);
        if (neighbourItemData.isAvailable()) {
            int neighbourItemVisibilityPercents = neighbourItemData.getVisibilityPercents(mListItems);
            if (/*neighbourItemVisibilityPercents == 100 ||*/ neighbourItemVisibilityPercents > currentItemVisibilityPercents) {
                setCurrentItem(neighbourItemData);
            }
        }
        /*if (enoughPercentsForDeactivation(currentItemVisibilityPercents) && neighbourItemData.isAvailable()) {
            // neighbour item become active (current)
            setDeactivateCurrentItem(listItemData);
            setCurrentItem(neighbourItemData);
        } *//*else if (neighbourItemData.isAvailable()){
            setDeactivateCurrentItem(neighbourItemData);
        }*/
    }

    private boolean enoughPercentsForDeactivation(int visibilityPercents) {
        boolean enoughPercentsForDeactivation = visibilityPercents <= INACTIVE_LIST_ITEM_VISIBILITY_PERCENTS;
        Log.i(TAG, "enoughPercentsForDeactivation " + enoughPercentsForDeactivation);
        return enoughPercentsForDeactivation;
    }

    @Override
    protected void onStateFling(ItemsPositionGetter itemsPositionGetter) {
        setDeactivateCurrentItem(mCurrentItem);
        //mCallback.deactivateCurrentItem(mListItems.get(currentItemData.getIndex()), currentItemData.getView(), currentItemData.getIndex());
    }

    @Override
    public void onScrollDirectionChanged(ScrollDirectionDetector.ScrollDirection scrollDirection) {
        Log.i(TAG, "onScrollDirectionChanged, scrollDirection " + scrollDirection);
        mScrollDirection = scrollDirection;
    }

    private void setCurrentItem(ListItemData newCurrentItem) {
        setDeactivateCurrentItem(mCurrentItem);
        Log.i(TAG, "setCurrentItem, newCurrentItem " + newCurrentItem);
        int itemPosition = newCurrentItem.getIndex();
        View view = newCurrentItem.getView();
        mCurrentItem.fillWithData(itemPosition, view);
        mCallback.activateNewCurrentItem(mListItems.get(itemPosition), view, itemPosition);
    }

    private void setDeactivateCurrentItem(ListItemData item) {
        Log.i(TAG, "setDeactivateCurrentItem, item " + item);
        if (item.isAvailable()) {
            int itemPosition = item.getIndex();
            View view = item.getView();
            //mCurrentItem.fillWithData(itemPosition, view);
            mCallback.deactivateCurrentItem(mListItems.get(itemPosition), view, itemPosition);
        }
    }
}
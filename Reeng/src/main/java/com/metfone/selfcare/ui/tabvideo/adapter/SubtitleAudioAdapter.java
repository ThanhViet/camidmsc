package com.metfone.selfcare.ui.tabvideo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabMovie.TrackItem;

import java.util.List;
import java.util.Locale;

public class SubtitleAudioAdapter extends RecyclerView.Adapter<SubtitleAudioAdapter.SubtitleAudioHolder> {

    private List<TrackItem> mListSubtitleAudio;
    private String selectCode = "";
    private ISubtitleAudioCallback mTitleCallback;
    private Context mContext;

    public SubtitleAudioAdapter(Context context, List<TrackItem> listSubtitleAudio, String selectCode, ISubtitleAudioCallback iSubtitleAudioCallback) {
        this.mContext = context;
        this.mListSubtitleAudio = listSubtitleAudio;
        this.selectCode = selectCode;
        this.mTitleCallback = iSubtitleAudioCallback;
    }

    @NonNull
    @Override
    public SubtitleAudioHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subtitle_audio, parent, false);
        return new SubtitleAudioHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubtitleAudioHolder holder, int position) {
        TrackItem subtitleAudio = mListSubtitleAudio.get(position);
        if (subtitleAudio.getLanguageCode().equals(TrackItem.OFF)) {
            holder.mTvTitle.setText(mContext.getString(R.string.off));
        } else {
            Locale loc = new Locale(subtitleAudio.getLanguageCode());
            String languageName = loc.getDisplayName();
            holder.mTvTitle.setText(languageName);
        }
        if (subtitleAudio.getLanguageCode().equals(selectCode)) {
            holder.mIvCheck.setImageResource(R.drawable.ic_check);
        } else {
            holder.mIvCheck.setImageResource(R.drawable.transparent);
        }
    }

    @Override
    public int getItemCount() {
        return mListSubtitleAudio.size();
    }

    public class SubtitleAudioHolder extends RecyclerView.ViewHolder {

        public TextView mTvTitle;
        public ImageView mIvCheck;

        public SubtitleAudioHolder(@NonNull View itemView) {
            super(itemView);
            mTvTitle = itemView.findViewById(R.id.tv_title);
            mIvCheck = itemView.findViewById(R.id.iv_choose);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TrackItem subtitleAudio = mListSubtitleAudio.get(getAdapterPosition());
                    mTitleCallback.selectItem(subtitleAudio);
                    notifyDataSetChanged();
                }
            });
        }
    }

    public interface ISubtitleAudioCallback {
        void selectItem(TrackItem trackItem);
    }

}

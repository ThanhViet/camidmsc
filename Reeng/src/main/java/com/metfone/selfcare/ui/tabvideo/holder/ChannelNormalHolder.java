package com.metfone.selfcare.ui.tabvideo.holder;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageLoader;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelListener;
import com.metfone.selfcare.ui.view.tab_video.SubscribeChannelLayout;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChannelNormalHolder extends BaseAdapter.ViewHolder {

    private static boolean pause = false;

    /**
     * khôi phục lại các tiền trình tải ảnh
     */
    public static void resumeRequests() {
        pause = false;
    }

    /**
     * dừng tiến trình load ảnh
     */
    public static void pauseRequests() {
        pause = true;
    }

    @BindView(R.id.v_status)
    View vStatus;
    @BindView(R.id.iv_channel)
    RoundedImageView ivChannel;
    @BindView(R.id.tv_channel)
    TextView tvChannel;
    @BindView(R.id.tv_number_follow)
    TextView tvNumberFollow;
    @BindView(R.id.tv_number_video)
    TextView tvNumberVideo;
    @BindView(R.id.btn_subscriptions_channel)
    SubscribeChannelLayout btnSubscriptionsChannel;
    @BindView(R.id.root_item)
    LinearLayout rootItem;

    private ChannelObject channelObject;
    private ImageLoader channelImageLoader;
    private Channel currentChannel;
    private OnItemChannelNormalListener onItemChannelListener;

    public ChannelNormalHolder(@NonNull LayoutInflater layoutInflater, @NonNull ViewGroup parent, OnItemChannelNormalListener onItemChannelListener) {
        super(layoutInflater.inflate(R.layout.item_channel_normal, parent, false));
        ButterKnife.bind(this, itemView);

        this.btnSubscriptionsChannel.setSubscribeChannelListener(onItemChannelListener);
        this.onItemChannelListener = onItemChannelListener;
    }

    public void bindData(ChannelObject itemChannelObject) {
        channelObject = itemChannelObject;
        currentChannel = channelObject.getChannel();
        channelImageLoader = channelObject.getChannelLoader();

        tvChannel.setText(currentChannel.getName());

        bindImageChannel();
        bindStatus();
        bindSub();
    }

    public void bindImageChannel() {
        if (channelImageLoader != null)
            if (pause) {
                if (channelImageLoader.isCompleted())
                    channelImageLoader.into(ivChannel);
                else
                    ivChannel.setImageResource(channelImageLoader.getThumb());
            } else {
                channelImageLoader.into(ivChannel);
            }
    }

    public void bindSub() {
        btnSubscriptionsChannel.setChannel(currentChannel);
        tvNumberFollow.setText(channelObject.getTextFollow());
        if (tvNumberVideo != null) {
            tvNumberVideo.setVisibility(View.VISIBLE);
            if (channelObject.getChannel().getNumVideo() <= 1)
                tvNumberVideo.setText(tvNumberVideo.getContext().getString(R.string.music_total_video, channelObject.getChannel().getNumVideo()));
            else
                tvNumberVideo.setText(tvNumberVideo.getContext().getString(R.string.music_total_videos, channelObject.getChannel().getNumVideo()));
        }
    }

    private void bindStatus() {
        if (currentChannel != null)
            if (currentChannel.isHaveNewVideo())
                vStatus.setVisibility(View.VISIBLE);
            else
                vStatus.setVisibility(View.GONE);
    }

    @OnClick({R.id.root_item})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.root_item:

                currentChannel.setHaveNewVideo(false);
                bindStatus();
                ChannelDataSource.getInstance().saveTimeNewChannel(currentChannel);

                if (onItemChannelListener != null)
                    onItemChannelListener.onClick(currentChannel);
                break;
        }
    }

    public static class ChannelObject implements BaseAdapter.Clone {

        private Channel channel;

        private String textFollow;

        private ImageLoader channelLoader;

        public Channel getChannel() {
            return channel;
        }

        public void setChannel(Channel channel) {
            this.channel = channel;
        }

        public String getTextFollow() {
            return textFollow;
        }

        void setTextFollow(String textFollow) {
            this.textFollow = textFollow;
        }

        ImageLoader getChannelLoader() {
            return channelLoader;
        }

        void setChannelLoader(ImageLoader channelLoader) {
            this.channelLoader = channelLoader;
        }

        @Override
        public BaseAdapter.Clone clone() {
            try {
                ChannelObject channels = new ChannelObject();
                channels.channel = channel;
                channels.textFollow = textFollow;
                channels.channelLoader = channelLoader;
                return channels;
            } catch (Exception e) {
                return this;
            }
        }

        @Override
        public String toString() {
            return "ChannelObject{" +
                    "channel=" + channel +
                    ", textFollow='" + textFollow + '\'' +
                    '}';
        }

        public static BaseAdapter.ItemObject provideItemObject(Channel channel, int position, Activity activity) {
            BaseAdapter.ItemObject item = new BaseAdapter.ItemObject();
            item.setId(channel.getId());
            item.setInfo(provideChannelObject(channel, position, activity));
            return item;
        }

        @SuppressWarnings("deprecation")
        static ChannelObject provideChannelObject(Channel channel, int position, Activity activity) {

            int thumb = ImageManager.with().build().provideThumbError(position % ImageManager.thumbs.length - 1);

            ImageLoader channelLoader = ImageManager.with()
                    .setUrl(channel.getUrlImage())
                    .setSize(ScreenManager.getWidth(activity) / 4, ScreenManager.getWidth(activity) / 4)
                    .setTransformations(new CenterCrop())
                    .setThumbError(thumb)
                    .build()
                    .provideImageLoader();

            ChannelObject channelObject = new ChannelObject();
            channelObject.setChannel(channel);
            channelObject.setChannelLoader(channelLoader);
            channelObject.setTextFollow(getTextFollow(channel.getNumfollow()));
            return channelObject;
        }

        private static String getTextFollow(long numFollow) {
            if (ApplicationController.self() != null)
                return String.format(ApplicationController.self().getString(R.string.people_subscription), Utilities.shortenLongNumber(numFollow));
            return String.valueOf(numFollow);
        }

    }


    public interface OnItemChannelNormalListener extends BaseAdapter.OnItemListener, OnChannelListener, SubscribeChannelLayout.SubscribeChannelListener {
    }
}

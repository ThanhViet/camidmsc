package com.metfone.selfcare.ui.popup;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.asksira.loopingviewpager.LoopingViewPager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rd.PageIndicatorView;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.popup.PopupIntroDetail;
import com.metfone.selfcare.database.model.popup.PopupIntroModel;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.ui.roundview.RoundRelativeLayout;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 10/8/2018.
 */

public class PopupIntro extends Dialog {

    private static final String TAG = PopupIntro.class.getSimpleName();

    protected ApplicationController mApplication;
    protected BaseSlidingFragmentActivity mActivity;
    @BindView(R.id.ivClose)
    ImageView ivClose;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.vpContent)
    LoopingViewPager vpContent;
    @BindView(R.id.indicator)
    PageIndicatorView indicator;
    @BindView(R.id.rlContent)
    RoundRelativeLayout rlContent;
    @BindView(R.id.btnDeeplink)
    RoundTextView btnDeeplink;
    @BindView(R.id.btnDeeplinkNext)
    RoundTextView btnDeeplinkNext;
    private PopupIntroModel popupIntroModel;

    private IntroViewPagerAdapter adapter;
    private Unbinder unbinder;
    private int currentPage;

    ClickButtonListener clickButtonListener;


    public PopupIntro(BaseSlidingFragmentActivity mActivity, PopupIntroModel popupIntroModel) {
        super(mActivity, R.style.DialogFullscreen);
        this.mActivity = mActivity;
        mApplication = (ApplicationController) mActivity.getApplication();
        this.popupIntroModel = popupIntroModel;
        this.setCancelable(true);
    }

    public void setClickButtonListener(ClickButtonListener clickButtonListener) {
        this.clickButtonListener = clickButtonListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_intro);
        unbinder = ButterKnife.bind(this);
        adapter = new IntroViewPagerAdapter();
        vpContent.setAdapter(adapter);
//        indicator.setViewPager(vpContent);

        tvTitle.setText(popupIntroModel.getTitle());
        btnDeeplink.setText(popupIntroModel.getLabelBtn());

        if (TextUtils.isEmpty(popupIntroModel.getLabelBtnNext()))
            btnDeeplinkNext.setVisibility(View.GONE);
        else {
            btnDeeplinkNext.setVisibility(View.VISIBLE);
            btnDeeplinkNext.setText(popupIntroModel.getLabelBtnNext());
        }

        indicator.setCount(popupIntroModel.getIntros().size());
//        indicator.setViewPager(vpContent);
        vpContent.setIndicatorPageChangeListener(new LoopingViewPager.IndicatorPageChangeListener() {
            @Override
            public void onIndicatorProgress(int selectingPosition, float progress) {
//                indicator.setProgress(selectingPosition, progress);
            }

            @Override
            public void onIndicatorPageChange(int newIndicatorPosition) {
                indicator.setSelection(newIndicatorPosition);
            }
        });
    }

    @OnClick({R.id.ivClose, R.id.btnDeeplink, R.id.btnDeeplinkNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                dismiss();
                break;
            case R.id.btnDeeplink:
                if (!TextUtils.isEmpty(popupIntroModel.getDeeplinkBtn()))
                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, popupIntroModel.getDeeplinkBtn());
                else if (clickButtonListener != null)
                    clickButtonListener.onClickBtnDeeplink();

                dismiss();
                break;
            case R.id.btnDeeplinkNext:
                if (!TextUtils.isEmpty(popupIntroModel.getDeeplinkBtnNext()))
                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, popupIntroModel.getDeeplinkBtnNext());
                else if (clickButtonListener != null)
                    clickButtonListener.onClickBtnDeeplinkNext();
                dismiss();
                break;
        }
    }


    class IntroViewPagerAdapter extends LoopingPagerAdapter<PopupIntroDetail> {
        TextView firstLine;
        TextView secondLine;
        ImageView image;

        public IntroViewPagerAdapter() {
            super(mApplication, popupIntroModel.getIntros(), true);
        }


        @Override
        protected View inflateView(int viewType, ViewGroup container, int listPosition) {
            return LayoutInflater.from(context).inflate(R.layout.item_pager_intro, container, false);
        }

        @Override
        protected void bindView(View itemView, int listPosition, int viewType) {
            firstLine = itemView.findViewById(R.id.tvFirstLine);
            secondLine = itemView.findViewById(R.id.tvSecondLine);
            image = itemView.findViewById(R.id.ivThumb);


            PopupIntroDetail popupIntroDetail = popupIntroModel.getIntros().get(listPosition);
            if (TextUtils.isEmpty(popupIntroDetail.getTitle())) {
                firstLine.setVisibility(View.GONE);
            } else {
                firstLine.setVisibility(View.VISIBLE);
                firstLine.setText(popupIntroDetail.getTitle());
            }
            secondLine.setText(popupIntroDetail.getDesc());
            if (!TextUtils.isEmpty(popupIntroDetail.getImage()))
                Glide.with(mApplication).load(popupIntroDetail.getImage()).apply(
                        new RequestOptions().placeholder(R.color.bg_onmedia_content_item)
                                .error(R.color.bg_onmedia_content_item)).into(image);
            else
                Glide.with(mApplication).load(popupIntroDetail.getResId()).apply(
                        new RequestOptions().placeholder(R.color.bg_onmedia_content_item)
                                .error(R.color.bg_onmedia_content_item)).into(image);
        }
    }

    public interface ClickButtonListener {
        public void onClickBtnDeeplink();

        public void onClickBtnDeeplinkNext();
    }
}

package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * thông báo khi người dùng muốn hủy tính năng upload video lên media
 */
public class NotifyUploadVideoOnMediaDialog extends Dialog {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.checkbox_say_again)
    CheckBox checkboxSayAgain;
    @BindView(R.id.tv_negative)
    TextView tvNegative;
    @BindView(R.id.tv_positive)
    TextView tvPositive;

    private int textTitle;
    private int textMessage;
    private int textNegative;
    private int textPositive;
    private int textSayAgain;

    private NotifyUploadVideoOnMediaListener mNotifyUploadVideoOnMediaListener;

    public NotifyUploadVideoOnMediaDialog(@NonNull Context context) {
        super(context);
    }

    public void setTextTitle(@StringRes int textTitle) {
        this.textTitle = textTitle;
    }

    public void setTextMessage(@StringRes int textMessage) {
        this.textMessage = textMessage;
    }

    public void setTextNegative(@StringRes int textNegative) {
        this.textNegative = textNegative;
    }

    public void setTextPositive(@StringRes int textPositive) {
        this.textPositive = textPositive;
    }

    public void setTextSayAgain(@StringRes int textSayAgain) {
        this.textSayAgain = textSayAgain;
    }

    public void setmNotifyUploadVideoOnMediaListener(NotifyUploadVideoOnMediaListener mNotifyUploadVideoOnMediaListener) {
        this.mNotifyUploadVideoOnMediaListener = mNotifyUploadVideoOnMediaListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_notify_upload_video_on_media);
        ButterKnife.bind(this);
        tvTitle.setText(textTitle);
        tvMessage.setText(textMessage);
        tvNegative.setText(textNegative);
        tvPositive.setText(textPositive);
        checkboxSayAgain.setText(textSayAgain);

        tvNegative.setOnClickListener(mOnClickListener);
        tvPositive.setOnClickListener(mOnClickListener);
        checkboxSayAgain.setOnCheckedChangeListener(mOnCheckedChangeListener);
    }

    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (mNotifyUploadVideoOnMediaListener != null)
                mNotifyUploadVideoOnMediaListener.onShowAgainClicked(b);
        }
    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mNotifyUploadVideoOnMediaListener != null)
                if (view == tvNegative)
                    mNotifyUploadVideoOnMediaListener.onNegativeClicked();
                else if (view == tvPositive)
                    mNotifyUploadVideoOnMediaListener.onPositiveClicked();
                dismiss();
        }
    };

    public interface NotifyUploadVideoOnMediaListener {
        void onNegativeClicked();

        void onPositiveClicked();

        void onShowAgainClicked(boolean isChecked);
    }
}

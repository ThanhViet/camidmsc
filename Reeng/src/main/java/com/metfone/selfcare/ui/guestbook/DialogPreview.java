package com.metfone.selfcare.ui.guestbook;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.editor.sticker.StickerView;
import com.android.editor.sticker.utils.PreviewImageView;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 4/24/2017.
 */
public class DialogPreview extends DialogFragment implements View.OnClickListener {
    private static final String TAG = DialogPreview.class.getSimpleName();
    private ApplicationController mApplication;
    private BaseSlidingFragmentActivity mParentActivity;
    private GuestBookHelper mGuestBookHelper;
    private Resources mRes;
    protected StickerView stickerView;
    protected String bookId;
    private PreviewImageView mImgPreview;
    private View mViewFooterBack, mViewFooterSave, mViewFooterVote, mViewFooterShare;
    private TextView mTvwFooterVote;
    private ImageView mImgFooterVote;
    private Bitmap bitmap;
    //
    private int isVoted = -1;
    private int totalVote = -1;

    public static DialogPreview newInstance(StickerView stickerView, String bookId) {
        DialogPreview fragment = new DialogPreview();
        fragment.stickerView = stickerView;
        fragment.bookId = bookId;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        mApplication = (ApplicationController) context.getApplicationContext();
        mParentActivity = (BaseSlidingFragmentActivity) context;
        mRes = mApplication.getResources();
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_preview, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null || stickerView == null) {
            return;
        }
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        mImgPreview = (PreviewImageView) view.findViewById(R.id.dialog_preview_image);
        View mViewFooter = view.findViewById(R.id.dialog_preview_image_footer);
        mViewFooterBack = mViewFooter.findViewById(R.id.guest_book_footer_preview_back);
        mViewFooterSave = mViewFooter.findViewById(R.id.guest_book_footer_preview_save);
        mViewFooterVote = mViewFooter.findViewById(R.id.guest_book_footer_preview_vote);
        mViewFooterShare = mViewFooter.findViewById(R.id.guest_book_footer_preview_share);
        mTvwFooterVote = (TextView) mViewFooter.findViewById(R.id.guest_book_footer_preview_vote_text);
        mImgFooterVote = (ImageView) mViewFooter.findViewById(R.id.guest_book_footer_preview_vote_icon);

        drawDetail();
        setListener();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("saveState", "save");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.guest_book_footer_preview_back:
                dismiss();
                break;
            case R.id.guest_book_footer_preview_save:
                String fileName = ImageHelper.IMAGE_GUEST_BOOK + System.currentTimeMillis() + Constants.FILE.JPEG_FILE_SUFFIX;
                ImageHelper.saveBitmapToGallery(mParentActivity, fileName, bitmap, mApplication);
                break;
            case R.id.guest_book_footer_preview_vote:
                handleVote();
                break;
            case R.id.guest_book_footer_preview_share:
                mParentActivity.shareImageFacebook(bitmap, mRes.getString(R.string.guest_book_share_facebook_title));
                break;
        }
    }

    private void drawDetail() {
        if (bitmap != null) {
            bitmap.recycle();
        }
        ViewGroup.LayoutParams params = mImgPreview.getLayoutParams();
        params.height = mGuestBookHelper.getPageHeight();
        params.width = mGuestBookHelper.getPageWidth();
        //
        bitmap = Bitmap.createBitmap(stickerView.getWidth(), stickerView.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        stickerView.draw(canvas);
        mImgPreview.setImageBitmap(bitmap);
        //
        mViewFooterVote.setEnabled(false);
        //mImgFooterVote.setImageResource(R.drawable.ic_guest_book_footer_vote);
        mImgFooterVote.setColorFilter(ContextCompat.getColor(mApplication, R.color.onmedia_bg_button));
        //mTvwFooterVote.setVisibility(View.GONE);
        mTvwFooterVote.setText("0");
        //
        mGuestBookHelper.requestVoteDetail(bookId, getVoteDetailListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // clearing memory on exit, cos manipulating with text uses bitmaps extensively
        // this does not frees memory immediately, but still can help
        if (bitmap != null) {
            Log.d(TAG, "bitmap.recycle");
            bitmap.recycle();
            bitmap = null;
        }
        // TODO comment when commit ibm
        System.gc();
        Runtime.getRuntime().gc();
    }

    @Override
    public void dismiss() {
        Log.d(TAG, "dismiss");
        super.dismiss();
    }

    @Override
    public void onDetach() {
        // release links
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                // remove dim
                WindowManager.LayoutParams windowParams = window.getAttributes();
                window.setDimAmount(0.0F);
                window.setAttributes(windowParams);
            }
        }
    }

    private void setListener() {
        mViewFooterBack.setOnClickListener(this);
        mViewFooterSave.setOnClickListener(this);
        mViewFooterVote.setOnClickListener(this);
        mViewFooterShare.setOnClickListener(this);
    }

    private GuestBookHelper.GetVoteDetailListener getVoteDetailListener = new GuestBookHelper.GetVoteDetailListener() {
        @Override
        public void onSuccess(int total, int isVote) {
            isVoted = isVote;
            totalVote = total;
            drawVoteDetail();
        }

        @Override
        public void onError(int error) {
            mParentActivity.showToast(R.string.request_send_error);
        }
    };

    private void handleVote() {
        if (isVoted == 1) {
            mParentActivity.showToast(R.string.guest_book_you_voted);
        } else {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            mGuestBookHelper.requestVoteBook(bookId, new GuestBookHelper.UpdateStateListener() {
                @Override
                public void onSuccess() {
                    mParentActivity.hideLoadingDialog();
                    isVoted = 1;
                    totalVote++;
                    drawVoteDetail();
                }

                @Override
                public void onError(int error) {
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(R.string.request_send_error);
                }
            });
        }
    }

    private void drawVoteDetail() {
        mViewFooterVote.setEnabled(true);
        if (totalVote >= 0) {
            //mTvwFooterVote.setVisibility(View.VISIBLE);
            mTvwFooterVote.setText(String.valueOf(totalVote));
        } else {
            mTvwFooterVote.setText("0");
            //mTvwFooterVote.setVisibility(View.GONE);
        }
        if (isVoted == 1) {
            mImgFooterVote.setColorFilter(ContextCompat.getColor(mApplication, R.color.guest_book_vote));
            //mImgFooterVote.setImageResource(R.drawable.ic_guest_book_footer_vote);
        } else {
            mImgFooterVote.setColorFilter(ContextCompat.getColor(mApplication, R.color.onmedia_bg_button));
            //mImgFooterVote.setImageResource(R.drawable.ic_guest_book_footer_vote_non);
        }
    }
}
package com.metfone.selfcare.ui.autoplay.utils;

import android.view.View;

import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/18/2018.
 */

public class ScrollDirectionDetector {
    private static final String TAG = ScrollDirectionDetector.class.getSimpleName();

    private final OnDetectScrollListener mOnDetectScrollListener;

    private int mOldTop;
    private int mOldFirstVisibleItem;

    private ScrollDirection mOldScrollDirection = null;

    public ScrollDirectionDetector(OnDetectScrollListener onDetectScrollListener) {
        mOnDetectScrollListener = onDetectScrollListener;
    }

    public interface OnDetectScrollListener {
        void onScrollDirectionChanged(ScrollDirection scrollDirection);
    }

    public enum ScrollDirection {
        UP, DOWN
    }

    public void onDetectedListScroll(ItemsPositionGetter itemsPositionGetter, int firstVisibleItem) {
        Log.i(TAG, ">> onDetectedListScroll, firstVisibleItem " + firstVisibleItem + ", mOldFirstVisibleItem " + mOldFirstVisibleItem);
        View view = itemsPositionGetter.getChildAt(0);
        int top = (view == null) ? 0 : view.getTop();
        Log.i(TAG, "onDetectedListScroll, view " + view + ", top " + top + ", mOldTop " + mOldTop);
        if (firstVisibleItem == mOldFirstVisibleItem) {
            if (top > mOldTop) {
                onScrollUp();
            } else if (top < mOldTop) {
                onScrollDown();
            }
        } else {
            if (firstVisibleItem < mOldFirstVisibleItem) {
                onScrollUp();
            } else {
                onScrollDown();
            }
        }
        mOldTop = top;
        mOldFirstVisibleItem = firstVisibleItem;
    }

    private void onScrollDown() {
        Log.i(TAG, "onScroll Down");
        if (mOldScrollDirection != ScrollDirection.DOWN) {
            mOldScrollDirection = ScrollDirection.DOWN;
            mOnDetectScrollListener.onScrollDirectionChanged(ScrollDirection.DOWN);
        }
    }

    private void onScrollUp() {
        Log.i(TAG, "onScroll Up");
        if (mOldScrollDirection != ScrollDirection.UP) {
            mOldScrollDirection = ScrollDirection.UP;
            mOnDetectScrollListener.onScrollDirectionChanged(ScrollDirection.UP);
        }
    }
}

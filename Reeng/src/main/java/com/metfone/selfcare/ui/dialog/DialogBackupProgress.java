package com.metfone.selfcare.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.backup_restore.backup.BackupManager;
import com.metfone.selfcare.util.Log;

public class DialogBackupProgress extends Dialog implements DialogInterface.OnKeyListener {
    public static final String TAG = DialogBackupProgress.class.getSimpleName();
    private Activity activity;
    TextView mTvBackupProgress;

    public DialogBackupProgress(Activity activity) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && BackupManager.isBackupInProgress()) {
            return true; //prevent back when forceRequestPermission
        }
        return false;
    }

    @Override
    public void dismiss() {
        try {
            activity = null;
            super.dismiss();
        } catch (Exception e) {
            Log.e(TAG, "dismissAllowingStateLoss ", e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_backup_progress);

        mTvBackupProgress = (TextView) findViewById(R.id.tv_backup_progress);
        mTvBackupProgress.setText(activity.getResources().getString(R.string.bk_preparing_data_wait) + " (" + 0 + "%)");

        setCancelable(false);
        setOnKeyListener(this);
        setCanceledOnTouchOutside(false);
        //getWindow().getAttributes().windowAnimations = R.style.PermissionDialogAnimation;
    }

    public void updateProgress(int percent) {
        if (percent > 100) percent = 100;
        if (mTvBackupProgress != null) {
            try {
                mTvBackupProgress.setText(activity.getResources().getString(R.string.bk_preparing_data_wait) + " (" + percent + "%)");
            } catch (Exception e) {
                Log.e(TAG, "updateProgress: Has exception " + e.toString());
            }
        }
    }

    public void updateUploadProgress(int percent) {
        if (percent > 100) percent = 100;
        if (mTvBackupProgress != null) {
            try {
                mTvBackupProgress.setText(activity.getResources().getString(R.string.bk_backing_up_wait) + " (" + percent + "%)");
            } catch (Exception e) {
                Log.e(TAG, "updateUploadProgress: Has exception " + e.toString());
            }
        }
    }
}

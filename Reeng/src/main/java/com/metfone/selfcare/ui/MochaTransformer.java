package com.metfone.selfcare.ui;

import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 7/26/2017.
 */

public class MochaTransformer implements ViewPager.PageTransformer {
    private static final String TAG = MochaTransformer.class.getSimpleName();

    public MochaTransformer() {

    }

   /* protected void onTransform(View view, float position) {

        transformPage(view, position);
    }*/

    @Override
    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        //Log.d(TAG, "setPageTransformer: " + position + " view: " + view.getId() + " : current Page: " + pager.getCurrentItem());
        if (position <= -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(0);
            if (Version.hasLollipop()) {
                view.setTranslationX(-pageWidth);
            } else {
                view.setTranslationX(0);
            }
        } else if (position <= 0) { // [-1,0]
            // Use the default slide transition when moving to the left page
           /* view.setAlpha(1);
            view.setTranslationX(0);
            view.setScaleX(1);
            view.setScaleY(1);*/
            if (position > -1 && position <= 0) {
                view.setAlpha(1 + position);
                view.setTranslationX(pageWidth * -position);
            } else {
                view.setAlpha(1);
                //view.setTranslationX(0);
            }
            //view.setTranslationX(pageWidth * position);
            Log.d(TAG, "setPageTransformer: " + position);
        } else if (position <= 1) { // (0,1]
            Log.d(TAG, "setPageTransformer+: " + position);
            // Fade the page out.
            view.setAlpha(1);
            //view.setAlpha(1);
            //view.setTranslationX(pageWidth * -position);
            view.setTranslationX(0);
            // Counteract the default slide transition
            // view.setTranslationX(pageWidth * -position);
            //view.setPivotX(pageWidth * -position);
            //view.setTranslationX(0);
            // Scale the page down (between MIN_SCALE and 1)
           /* float scaleFactor = MIN_SCALE
                    + (1 - MIN_SCALE) * (1 - Math.abs(position));
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);*/

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(0);
        }
    }

    /*public void transformPage(View view, float position) {
        Log.d(TAG, "setPageTransformer: " + position + " : current Page: " + pager.getCurrentItem());
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(0);

        } else if (position <= 1) { // [-1,1]
            // Modify the default slide transition to shrink the page as well
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
            float vertMargin = pageHeight * (1 - scaleFactor) / 2;
            float horzMargin = pageWidth * (1 - scaleFactor) / 2;
            if (position < 0) {
                view.setTranslationX(horzMargin - vertMargin / 2);
            } else {
                view.setTranslationX(-horzMargin + vertMargin / 2);
            }

            // Scale the page down (between MIN_SCALE and 1)
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

            // Fade the page relative to its size.
            view.setAlpha(MIN_ALPHA +
                    (scaleFactor - MIN_SCALE) /
                            (1 - MIN_SCALE) * (1 - MIN_ALPHA));

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(0);
        }
    }*/

   /* @Override
    public boolean isPagingEnabled() {
        return true;
    }*/
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/24
 *
 */

package com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.holder.TopMoneyUploadHeaderHolder;
import com.metfone.selfcare.ui.tabvideo.holder.TopMoneyUploadHolder;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.SubscribeChannelActivity;
import com.metfone.selfcare.ui.view.load_more.OnEndlessScrollListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class TopMoneyUploadFragment extends BaseFragment implements TopMoneyUploadContact.TopMoneyUploadView
        , TopMoneyUploadContact.TopMoneyUploadProvider {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout)
    @Nullable
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_progress)
    @Nullable
    View progressEmpty;
    @BindView(R.id.empty_text)
    @Nullable
    TextView tvEmpty;
    @BindView(R.id.empty_retry_text1)
    @Nullable
    TextView tvEmptyRetry1;
    @BindView(R.id.empty_retry_text2)
    @Nullable
    TextView tvEmptyRetry2;
    @BindView(R.id.empty_retry_button)
    @Nullable
    ImageView btnEmptyRetry;
    @BindView(R.id.empty_layout)
    @Nullable
    View viewEmpty;
    @BindView(R.id.progress_loading)
    @Nullable
    View progressLoading;

    @Inject
    TopMoneyUploadContact.TopMoneyUploadPresenter presenter;

    private Unbinder unbinder;
    private boolean isLoading = false;
    private boolean isLoadMore = false;
    private TopMoneyUploadAdapter adapter;
    private BaseAdapter.ItemObject loadMoreObject;
    private LinearLayoutManager layoutManager;
    private ArrayList<BaseAdapter.ItemObject> itemObjects;
    private Runnable loadingRunnable = new Runnable() {
        @Override
        public void run() {
            if (isLoading)
                return;
            isLoading = true;
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(true);
            if (presenter != null) presenter.getTopMoneyUpload();
        }
    };
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (isLoading)
                return;
            isLoading = true;
            if (presenter != null) presenter.getRefreshTopMoneyUpload();
        }
    };
    private OnEndlessScrollListener mEndlessRecyclerOnScrollListener = new OnEndlessScrollListener(3) {
        private int currentState;

        @Override
        public void onLoadNextPage(View view) {
            super.onLoadNextPage(view);
            if (isLoading)
                return;
            if (isLoadMore) {
                isLoading = true;
                if (swipeRefreshLayout != null) swipeRefreshLayout.setEnabled(false);
                if (presenter != null) presenter.getMoreTopMoneyUpload();
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (adapter == null || layoutManager == null)
                return;
            if (currentState == RecyclerView.SCROLL_STATE_IDLE && newState == RecyclerView.SCROLL_STATE_DRAGGING) {// dừng tất cả các tiền trình đang load ảnh
                adapter.pauseRequests();
            } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {// khôi phuc các tiền trình load ảnh
                adapter.resumeRequests();
                int first = layoutManager.findFirstVisibleItemPosition();
                int last = layoutManager.findLastVisibleItemPosition();
                last = Math.min(last, adapter.getItemCount() - 1);
                for (int i = first; i <= last; i++) {
                    updateUiImage(i);
                }
            }
            currentState = newState;
        }

        private void updateUiImage(int position) {
            if (recyclerView != null) {
                RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);
                if (viewHolder instanceof TopMoneyUploadHolder) {
                    ((TopMoneyUploadHolder) viewHolder).bindAvatar();
                } else if (viewHolder instanceof TopMoneyUploadHeaderHolder) {
                    ((TopMoneyUploadHeaderHolder) viewHolder).bindAvatar();
                }
            }
        }
    };
    private TopMoneyUploadAdapter.OnItemChannelListener onItemChannelNormalListener = new TopMoneyUploadAdapter.OnItemChannelListener() {

        @Override
        public void onClick(Channel channel) {
            if (activity instanceof SubscribeChannelActivity)
                ((SubscribeChannelActivity) activity).notifyOpenChannel(channel);
            if (presenter != null) presenter.openDetail(activity, channel);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
        loadMoreObject = new BaseAdapter.ItemObject();
        loadMoreObject.setId(String.valueOf(System.currentTimeMillis()));
        loadMoreObject.setInfo(new BaseAdapter.DefaultClone());
        loadMoreObject.setType(BaseAdapter.Type.LOAD_MORE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_money_upload, container, false);
        unbinder = ButterKnife.bind(this, view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public Fragment provideFragment() {
        isVisibleToUser = true;
        return this;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        if (canLazyLoad() && swipeRefreshLayout != null) {
            swipeRefreshLayout.post(loadingRunnable);
        }
    }

    private void initView() {
        itemObjects = new ArrayList<>();
        adapter = new TopMoneyUploadAdapter(activity);
        adapter.setOnItemListener(onItemChannelNormalListener);
        layoutManager = new CustomLinearLayoutManager(application);
        layoutManager.setInitialPrefetchItemCount(5);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(null);
        recyclerView.setLayoutAnimation(null);
        recyclerView.setItemViewCacheSize(5);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(mEndlessRecyclerOnScrollListener);
        if (swipeRefreshLayout != null) swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
    }

    @Override
    public void bindData(ArrayList<Channel> channels, boolean loadMore) {
        isLoadMore = loadMore;
        isLoading = false;
        if (itemObjects == null) itemObjects = new ArrayList<>();
        else itemObjects.clear();
        if (Utilities.notEmpty(channels))
            for (int i = 0; i < channels.size(); i++) {
                Channel channel = channels.get(i);
                if (channel != null)
                    itemObjects.add(TopMoneyUploadHolder.ChannelObject.provideItemObject(channel, i, activity));
            }
        itemObjects.remove(loadMoreObject);
        if (loadMore) itemObjects.add(loadMoreObject);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setEnabled(true);
            swipeRefreshLayout.setRefreshing(false);
        }
        if (adapter != null) adapter.updateData(itemObjects);
        if (isLoadMore && itemObjects != null && itemObjects.size() < 20 && mEndlessRecyclerOnScrollListener != null && recyclerView != null)
            mEndlessRecyclerOnScrollListener.onLoadNextPage(recyclerView);
    }

    @Override
    public void loadDataCompleted() {
        isLoading = false;
        if (Utilities.notEmpty(itemObjects)) {
            showLoadedSuccess();
        } else if (presenter != null) {
            if (presenter.isErrorApi())
                showLoadedError();
            else
                showLoadedEmpty();
        }
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        activity.onBackPressed();
    }

    protected void showLoading() {
        Log.i(TAG, "showLoading");
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(true);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.VISIBLE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

    protected void showLoadedEmpty() {
        Log.i(TAG, "showLoadedEmpty");
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (tvEmpty != null) {
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText(R.string.no_data);
        }
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

    protected void showLoadedSuccess() {
        Log.i(TAG, "showLoadedSuccess");
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

    protected void showLoadedError() {
        Log.i(TAG, "showLoadedError");
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (NetworkHelper.isConnectInternet(activity)) {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.VISIBLE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        } else {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.VISIBLE);
        }
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

}
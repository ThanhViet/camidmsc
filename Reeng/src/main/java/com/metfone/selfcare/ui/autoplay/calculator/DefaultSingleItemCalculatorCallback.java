package com.metfone.selfcare.ui.autoplay.calculator;

import android.view.View;

import com.metfone.selfcare.ui.autoplay.items.ListItem;


/**
 * Created by toanvk2 on 1/18/2018.
 */

public class DefaultSingleItemCalculatorCallback implements Callback<ListItem> {

    @Override
    public void activateNewCurrentItem(ListItem newListItem, View newView, int newViewPosition) {
        newListItem.setActive(newView, newViewPosition);
    }

    @Override
    public void deactivateCurrentItem(ListItem listItemToDeactivate, View view, int position) {
        listItemToDeactivate.deactivate(view, position);
    }
}

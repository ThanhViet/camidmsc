package com.metfone.selfcare.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;

/**
 * Created by HaiKE on 10/09/2015.
 */
public class CustomView169 extends RelativeLayout {
    private Context mContext;
    private boolean isRatio;

    public CustomView169(Context context) {
        super(context);
        this.mContext = context;
        init(null);
    }

    public CustomView169(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(attrs);
    }

    public CustomView169(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomView169(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.customImageView);
            isRatio = a.getBoolean(R.styleable.customImageView_isRatio, false);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h;
        if (isRatio) {
            h = (w * 9) / 16;
        } else {
            h = MeasureSpec.getSize(heightMeasureSpec);
        }
        this.setMeasuredDimension(w, h);
    }

    public void setImageRatio(boolean boo) {
        this.isRatio = boo;
    }
}

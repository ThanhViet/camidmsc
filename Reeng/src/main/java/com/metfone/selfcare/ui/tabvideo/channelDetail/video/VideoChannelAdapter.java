package com.metfone.selfcare.ui.tabvideo.channelDetail.video;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChannelListener;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;

public class VideoChannelAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {

    public static final int TYPE_SPACE_HEADER = -2;
    public static final int TYPE_SPACE_BOTTOM = -1;
    public static final int LOAD_MORE = 0;
    public static final int NORMAL = 1;
    private OnVideoChannelListener onVideoChannelListener;
    private Channel mChannel;

    VideoChannelAdapter(Activity act) {
        super(act);
    }

    public void setOnVideoChannelListener(OnVideoChannelListener onVideoChannelListener) {
        this.onVideoChannelListener = onVideoChannelListener;
    }

    @Override
    public int getItemViewType(int position) {
        Object object = items.get(position);
        if (object instanceof Video) {
            return NORMAL;
        } else if (Utilities.equals(object, TYPE_SPACE_HEADER)) {
            return TYPE_SPACE_HEADER;
        } else if (Utilities.equals(object, TYPE_SPACE_BOTTOM)) {
            return TYPE_SPACE_BOTTOM;
        } else {
            return LOAD_MORE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case NORMAL:
                return new VideoHolder(activity, layoutInflater, parent, onVideoChannelListener);
            case TYPE_SPACE_HEADER:
                return new SpaceHolder(activity, layoutInflater, parent, R.color.v5_cancel);
            case TYPE_SPACE_BOTTOM:
                return new SpaceHolder(activity, layoutInflater, parent, android.R.color.white);
            default:
                return new LoadMoreHolder(activity, layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VideoHolder) {
            ((VideoHolder) holder).setChannel(mChannel);
            ((VideoHolder) holder).bindData(items, position);
        }
    }

    public Object getItem(int position) {
        if (items.size() > position && position >= 0) {
            return items.get(position);
        }
        return null;
    }

    public void setChannel(Channel channel) {
        mChannel = channel;
    }

    public static class SpaceHolder extends LoadMoreHolder {

        SpaceHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent, int color) {
            super(layoutInflater.inflate(R.layout.item_channel_space_v5, parent, false));
            itemView.setBackgroundColor(ContextCompat.getColor(activity, color));
        }
    }

    public static class VideoHolder extends ViewHolder {
        @BindView(R.id.ivImage)
        ImageView ivImage;
        @BindView(R.id.iv_more)
        ImageView ivMore;
        @BindView(R.id.tvChannel)
        TextView tvChannel;
        @BindView(R.id.tv_date_publish)
        TextView tvDatetime;
        @BindView(R.id.tv_duration)
        TextView tvDuration;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvNumberSeen)
        TextView tvNumberSeen;
        @BindView(R.id.iv_approved)
        TextView ivApproved;
        @BindView(R.id.iv_live_stream)
        @Nullable
        View ivLiveStream;

        private Channel mChannel;

        public VideoHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent, final OnVideoChannelListener listener) {
            super(layoutInflater.inflate(R.layout.item_video_channel, parent, false));
            itemView.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickPlaylistVideo(view, getAdapterPosition());
                }
            });
            if (ivMore != null) {
                ivMore.setOnClickListener(view -> {
                    if (listener != null) listener.onClickMorePlaylistVideo(view, getAdapterPosition());
                });
            }
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            Object item = items.get(position);
            if (item instanceof Video) {
                Video mVideo = (Video) item;
                tvTitle.setText(mVideo.getTitle());
                ImageBusiness.setSong(mVideo.getImagePath(), ivImage, position, Utilities.dpToPx(11));
//                if (!TextUtils.isEmpty(mVideo.getChannelName())) {
//                    tvChannel.setVisibility(View.VISIBLE);
//                    tvChannel.setText(mVideo.getChannelName());
//                } else {
//                    tvChannel.setVisibility(View.GONE);
//                }
                if (tvDatetime != null && mVideo.getPublishTime() > 0) {
                    tvDatetime.setVisibility(View.VISIBLE);
                    tvDatetime.setText(DateTimeUtils.calculateTime(tvDatetime.getResources(), mVideo.getPublishTime()));
                }
                if (tvDuration != null) {
                    if(!TextUtils.isEmpty(mVideo.getDuration())) {
                        tvDuration.setVisibility(View.VISIBLE);
                        tvDuration.setText(mVideo.getDuration());
                    } else {
                        tvDuration.setVisibility(View.GONE);
                    }
                }
                /*ExecutorService executor = Executors.newFixedThreadPool(5);
                asyncSetText(tvNumberSeen, mVideo.getTotalView(), executor);
                executor.shutdown();*/
                tvNumberSeen.setText(String.format((mVideo.getTotalView() <= 1) ? tvNumberSeen.getContext().getString(R.string.view)
                        : tvNumberSeen.getContext().getString(R.string.video_views), Utilities.getTotalView(mVideo.getTotalView())));
                if (ivLiveStream != null)
                    ivLiveStream.setVisibility(mVideo.isLive() ? View.VISIBLE : View.GONE);
                if (mChannel != null && mChannel.isMyChannel() && ivApproved != null) {
                    if (mVideo.getItemStatus() == Video.Status.APPROVED.VALUE) {
                        ivApproved.setVisibility(View.VISIBLE);
                        ivApproved.setBackground(ivApproved.getResources().getDrawable(R.drawable.bg_approved));
                        ivApproved.setText(R.string.approved);
                        tvTitle.setTextColor(ivApproved.getResources().getColor(R.color.v5_text));
                    }
                    if (mVideo.getItemStatus() == Video.Status.NOT_APPROVED.VALUE) {
                        ivApproved.setVisibility(View.VISIBLE);
                        ivApproved.setBackground(ivApproved.getResources().getDrawable(R.drawable.bg_wait_approve));
                        ivApproved.setText(R.string.wait_for_approved);
                        tvTitle.setTextColor(ivApproved.getResources().getColor(R.color.v5_text_30));
                    }
                }
            }
        }

        private void asyncSetText(TextView textView, final long numberView, Executor executor) {
            final WeakReference<TextView> textViewRef = new WeakReference<>(textView);
            executor.execute(() -> {
                TextView textView12 = textViewRef.get();
                if (textView12 == null) return;
                textView12.post(() -> {
                    TextView textView1 = textViewRef.get();
                    if (textView1 == null) return;
                    textView1.setText(String.format((numberView <= 1) ? textView1.getContext().getString(R.string.view)
                            : textView1.getContext().getString(R.string.video_views), Utilities.getTotalView(numberView)));
                });
            });
        }

        public void setChannel(Channel channel) {
            mChannel = channel;
        }
    }
}

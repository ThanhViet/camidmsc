/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2020/4/15
 *
 */

package com.metfone.selfcare.ui.snackbar;

public enum Gravity {

    TOP("top"),
    BOTTOM("bottom"),
    CENTER("center"),
    ;

    public String VALUE;

    Gravity(String value) {
        this.VALUE = value;
    }
}

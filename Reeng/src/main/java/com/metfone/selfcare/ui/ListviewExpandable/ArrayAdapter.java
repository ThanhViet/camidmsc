package com.metfone.selfcare.ui.ListviewExpandable;

/**
 * Created by thanhnt72 on 6/21/2016.
 */

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("UnusedDeclaration")
public abstract class ArrayAdapter<T> extends BaseAdapter implements Swappable, Insertable<T> {

    @NonNull
    private final List<T> mItems;

    private BaseAdapter mDataSetChangedSlavedAdapter;

    /**
     * Creates a new ArrayAdapter with an empty {@code List}.
     */
    protected ArrayAdapter() {
        this(null);
    }

    /**
     * Creates a new ArrayAdapter, using (a copy of) given {@code List}, or an empty {@code List} if objects = null.
     */
    protected ArrayAdapter(@Nullable final List<T> objects) {
        if (objects != null) {
            mItems = objects;
        } else {
            mItems = new ArrayList<>();
        }
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    @NonNull
    public T getItem(final int position) {
        return mItems.get(position);
    }

    /**
     * Returns the items.
     */
    @NonNull
    public List<T> getItems() {
        return mItems;
    }

    /**
     * Appends the specified element to the end of the {@code List}.
     *
     * @param object the object to add.
     *
     * @return always true.
     */
    public boolean add(@NonNull final T object) {
        boolean result = mItems.add(object);
        notifyDataSetChanged();
        return result;
    }

    @Override
    public void add(final int index, @NonNull final T item) {
        mItems.add(index, item);
        notifyDataSetChanged();
    }

    /**
     * Adds the objects in the specified collection to the end of this List. The objects are added in the order in which they are returned from the collection's iterator.
     *
     * @param collection the collection of objects.
     *
     * @return {@code true} if this {@code List} is modified, {@code false} otherwise.
     */
    public boolean addAll(@NonNull final Collection<? extends T> collection) {
        boolean result = mItems.addAll(collection);
        notifyDataSetChanged();
        return result;
    }

    public boolean contains(final T object) {
        return mItems.contains(object);
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public boolean remove(@NonNull final Object object) {
        boolean result = mItems.remove(object);
        notifyDataSetChanged();
        return result;
    }

    @NonNull
    public T remove(final int location) {
        T result = mItems.remove(location);
        notifyDataSetChanged();
        return result;
    }

    @Override
    public void swapItems(final int positionOne, final int positionTwo) {
        T firstItem = mItems.set(positionOne, getItem(positionTwo));
        notifyDataSetChanged();
        mItems.set(positionTwo, firstItem);
    }

    public void propagateNotifyDataSetChanged(@NonNull final BaseAdapter slavedAdapter) {
        mDataSetChangedSlavedAdapter = slavedAdapter;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        if (mDataSetChangedSlavedAdapter != null) {
            mDataSetChangedSlavedAdapter.notifyDataSetChanged();
        }
    }
}

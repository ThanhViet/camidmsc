package com.metfone.selfcare.ui.recyclerview;

import android.view.View;

/**
 * Created by thanhnt72 on 9/28/2016.
 */

public interface RecyclerClickListener {
    void onClick(View v, int pos,Object object);

    void onLongClick(View v, int pos,Object object);
}
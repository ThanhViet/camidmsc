package com.metfone.selfcare.ui.dialog;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/*created by huongnd38 on 23/10/2018*/

public class PermissionDialog extends DialogFragment implements DialogInterface.OnKeyListener {

    public static final String TAG = PermissionDialog.class.getSimpleName();
    ListView mListView;
    PermissionListAdapter mAdapter;
    ArrayList<PermissionItem> permissionItems = new ArrayList<>();
    int permissionCode;
    View mCancelBtn, mAllowBtn;
    boolean forceRequestPermission = false; //if true, it will always request permission event if user not click on allow button
    List<String> permissions;
    CallBack mListener;
    boolean isAllowClicked = false;
    int allowClickCount = 0;
    TextView tvPermissionDescription;
    /*public static PermissionDialog newInstance(int permissionCode, CallBack listener) {
        PermissionDialog dialogFragment = new PermissionDialog();
        dialogFragment.permissionCode = permissionCode;
        dialogFragment.mListener = listener;
        return dialogFragment;
    }*/

    public static PermissionDialog newInstance(int permissionCode, boolean forceRequestPermission, CallBack listener) {
        PermissionDialog dialogFragment = new PermissionDialog();
        dialogFragment.permissionCode = permissionCode;
        dialogFragment.mListener = listener;
        dialogFragment.forceRequestPermission = forceRequestPermission;
        return dialogFragment;
    }

    public static PermissionDialog newInstance(int permissionCode, boolean forceRequestPermission, List<String> permissions, CallBack listener) {
        PermissionDialog dialogFragment = new PermissionDialog();
        dialogFragment.permissionCode = permissionCode;
        dialogFragment.mListener = listener;
        dialogFragment.forceRequestPermission = forceRequestPermission;
        dialogFragment.permissions = permissions;
        return dialogFragment;
    }

    public static void dismissDialogPermission(FragmentManager fragmentManager){
        if (fragmentManager != null) {
            try {
                DialogFragment dialogFragment = (DialogFragment) fragmentManager.findFragmentByTag(PermissionDialog.TAG);
                if (dialogFragment != null) {
                    dialogFragment.dismissAllowingStateLoss();
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        /*if(keyCode == KeyEvent.KEYCODE_BACK) {
            return forceRequestPermission; //prevent back when forceRequestPermission
        }*/
        return false;
    }

    public interface CallBack {
        void onPermissionAllowClick(boolean allow, int clickCount);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (permissions != null && !permissions.isEmpty()) {
            if (PermissionHelper.verifyPermissions(getContext(), permissions)) {
                dismissAllowingStateLoss();
            }
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        try {
            super.dismissAllowingStateLoss();
        } catch (Exception e) {
            Log.e(TAG, "dismissAllowingStateLoss ", e);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View contentView = getActivity().getLayoutInflater().inflate(R.layout.dialog_permission, null, false);


        mListView = (ListView) contentView.findViewById(R.id.permission_listview);
        mCancelBtn = contentView.findViewById(R.id.btn_cancel_permission);
        mAllowBtn = contentView.findViewById(R.id.btn_request_permission);
        tvPermissionDescription = (TextView) contentView.findViewById(R.id.tv_permission_description);
        if (forceRequestPermission) mCancelBtn.setVisibility(View.GONE);
        mCancelBtn.setVisibility(View.GONE);
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mListener != null) {
//                    mListener.onPermissionAllowClick(false, allowClickCount);
//                    mListener = null;
//                }
                mListener = null;
                dismissAllowingStateLoss();
            }
        });

        mAllowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onPermissionAllowClick(true, allowClickCount);
                    isAllowClicked = true;
                    allowClickCount++;
                }

                mListener = null;
//                dismissAllowingStateLoss();
            }
        });

        mAdapter = new PermissionListAdapter(getContext(), permissionItems);

        setListData();
        mListView.setAdapter(mAdapter);
        setHeaderText();
        int style = Build.VERSION.SDK_INT == Build.VERSION_CODES.M ? R.style.PermissionDialog_m : R.style.PermissionDialog;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity(), style);
        alertDialog.setView(contentView);
        alertDialog.setCancelable(!forceRequestPermission);
        alertDialog.setOnKeyListener(this);
        Dialog dialog = alertDialog.create();

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.PermissionDialogAnimation;
        return dialog;
    }

    @Override
    public void onDestroy() {
        if (mListener != null && forceRequestPermission && !isAllowClicked) {
            mListener.onPermissionAllowClick(false, allowClickCount);
        }
        mListener = null;
        super.onDestroy();
    }

    private void setListData() {
        permissionItems.clear();

        switch (permissionCode) {
            case Constants.PERMISSION.PERMISSION_REQUEST_ALL:
//                if (permissions != null && permissions.contains(Manifest.permission.READ_SMS)) {
//                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_smsout)));
//                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_smsin, getString(R.string.permission_to_smsin)));
//                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_CONTACTS)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_sync, getString(R.string.permission_sync_and_connect_friend)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.READ_PHONE_STATE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_send_receive_msg_and_make_call)));
                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
                    permissionItems.add(new PermissionItem(R.drawable.ic_permission_upload, getString(R.string.permission_to_upload_video)));
                }
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_LOCATION:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_share_location, getString(R.string.permission_to_share)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_location_find_friends, getString(R.string.permission_to_find_friends)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_RECORD_AUDIO:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_to_free_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_camera, getString(R.string.permission_to_video_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_mic, getString(R.string.permission_to_send_voice_msg)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_camera, getString(R.string.permission_to_video_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_take_photo, getString(R.string.permission_to_take_photo)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_VIDEO:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_camera, getString(R.string.permission_to_video_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_take_photo, getString(R.string.permission_to_take_photo)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_sync, getString(R.string.permission_sync_and_connect_friend)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_sync_and_connect_friend)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_free_msg_call)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_SAVE_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_sync, getString(R.string.permission_sync_and_connect_friend)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_sync_and_connect_friend)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_free_msg_call)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_DELETE_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_sync, getString(R.string.permission_sync_and_connect_friend)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_sync_and_connect_friend)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_free_msg_call)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_WRITE_STORAGE:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_upload, getString(R.string.permission_to_upload_video)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_WRITE_CONTACT:
            case Constants.PERMISSION.PERMISSION_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_sync, getString(R.string.permission_sync_and_connect_friend)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_sync_and_connect_friend)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_free_msg_call)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO_GROUP_AVATAR: //quyen Camera
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_camera, getString(R.string.permission_to_video_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_take_photo, getString(R.string.permission_to_take_photo)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_GALLERY: //quyen bo nho WRITE_EXTERNAL_STORAGE
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_upload, getString(R.string.permission_to_upload_video)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_FILE: //quyen bo nho WRITE_EXTERNAL_STORAGE
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_upload, getString(R.string.permission_to_upload_video)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_FILE_DOWNLOAD: //quyen bo nho WRITE_EXTERNAL_STORAGE
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_upload, getString(R.string.permission_to_upload_video)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_RECEIVE_MESSAGE:
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_send_and_receive_msg)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_smsout)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_smsin, getString(R.string.permission_to_smsin)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_PHONE:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_to_free_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_camera, getString(R.string.permission_to_video_call)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO_AND_STORAGE:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_take_photo, getString(R.string.permission_to_take_photo)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_call, getString(R.string.permission_to_free_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_camera, getString(R.string.permission_to_video_call)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_mic, getString(R.string.permission_to_send_voice_msg)));
                break;

            case Constants.PERMISSION.PERMISSION_REQUEST_STORAGE_AND_CONTACT:
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_sync, getString(R.string.permission_sync_and_connect_friend)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
                permissionItems.add(new PermissionItem(R.drawable.ic_permission_upload, getString(R.string.permission_to_upload_video)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_chat, getString(R.string.permission_to_free_msg_call)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_file, getString(R.string.permission_send_receive_img_and_files)));
//                permissionItems.add(new PermissionItem(R.drawable.ic_permission_upload, getString(R.string.permission_to_upload_video)));
                break;
        }
    }

    private void setHeaderText() {
        if (permissionCode == Constants.PERMISSION.PERMISSION_REQUEST_ALL) {
            if (tvPermissionDescription != null) {
                String permissionString = "";
                if (permissions != null && (permissions.contains(Manifest.permission.READ_CONTACTS) || permissions.contains(Manifest.permission.WRITE_CONTACTS))) {
                    if (!TextUtils.isEmpty(permissionString))
                        permissionString += ", ";
                    permissionString += getContext().getString(R.string.permission_setting_contact);
                }
//                if (permissions != null && permissions.contains(Manifest.permission.READ_SMS)) {
//                    if(!TextUtils.isEmpty(permissionString))
//                        permissionString += ", ";
//                    permissionString += getContext().getString(R.string.permission_setting_sms);
//                }
                if (permissions != null && permissions.contains(Manifest.permission.READ_PHONE_STATE)) {
                    if (!TextUtils.isEmpty(permissionString))
                        permissionString += ", ";
                    permissionString += getContext().getString(R.string.permission_setting_phone);
                }
                if (permissions != null && permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (!TextUtils.isEmpty(permissionString))
                        permissionString += ", ";
                    permissionString += getContext().getString(R.string.permission_setting_storage);
                }

                if (TextUtils.isEmpty(permissionString)) {
                    tvPermissionDescription.setText(getString(R.string.permission_allow_access_to));
                } else {
                    tvPermissionDescription.setText(getString(R.string.permission_all_description_custom, permissionString));
                }

//                if (permissions != null && permissions.contains(Manifest.permission.READ_SMS)) {
//                    tvPermissionDescription.setText(getString(R.string.permission_all_description));
//                }
//                else
//                {
//                    tvPermissionDescription.setText(getString(R.string.permission_all_description_custom));
//                }
            }
        } else {
            if (tvPermissionDescription != null) {
                tvPermissionDescription.setText(getString(R.string.permission_allow_access_to));
            }
        }
    }

    public static class PermissionListAdapter extends BaseAdapter {
        LayoutInflater mLayoutInflater;

        PermissionListAdapter(Context context, ArrayList<PermissionItem> list) {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.list = list;
        }

        ArrayList<PermissionItem> list;

        @Override
        public int getCount() {
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {
            if (list != null && position < list.size()) {
                return list.get(position);
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.item_request_permission, parent, false);
                holder = new ViewHolder();
                holder.imagePermission = (ImageView) convertView.findViewById(R.id.item_permission_image);
                holder.tvPermission = (TextView) convertView.findViewById(R.id.item_permission_text);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvPermission.setText(list.get(position).permission);
            holder.imagePermission.setImageResource(list.get(position).permissionImageId);
            return convertView;
        }
    }

    private static class PermissionItem {
        String permission;
        int permissionImageId;

        public PermissionItem(int permissionImageId, String permission) {
            this.permission = permission;
            this.permissionImageId = permissionImageId;
        }
    }

    private static class ViewHolder {
        ImageView imagePermission;
        TextView tvPermission;
    }

}

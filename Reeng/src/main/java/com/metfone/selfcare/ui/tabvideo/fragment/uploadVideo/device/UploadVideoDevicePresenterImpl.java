package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpProgressCallBack;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.model.tab_video.Category;
import com.metfone.selfcare.model.tab_video.Video;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UploadVideoDevicePresenterImpl implements UploadVideoDevicePresenter, OnVideoCallback {

    private static final int MAX_SIZE_DEFAULT = 200;
    private ApplicationController application;
    private UploadVideoDeviceView uploadVideoDeviceView;

    private Utils utils;
    private VideoApi videoApi;
    private Http requestUpload;

    private boolean cancel;
    private boolean isUploadOnMedia;

    public UploadVideoDevicePresenterImpl(ApplicationController application, BaseView baseView) {
        this.application = application;
        this.uploadVideoDeviceView = (UploadVideoDeviceView) baseView;

        this.utils = application.getApplicationComponent().providesUtils();
        this.videoApi = application.getApplicationComponent().providerVideoApi();
    }

    @Override
    public void getCategories() {
        if (utils == null || uploadVideoDeviceView == null) return;
        ArrayList<Category> categories = utils.getCategories();
        List<String> list = new ArrayList<>();
        list.add(application.getString(R.string.selectCategory));
        for (Category category : categories) {
            String name = category.getName().toLowerCase();
            String first = name.substring(0, 1);
            name = first.toUpperCase() + name.substring(1);
            list.add(name);
        }
        uploadVideoDeviceView.updateDataCategories(list);
    }

    @Override
    public void uploadVideo(boolean uploadOnMedia, String title, String description, String url, Object selectedItem) {
        isUploadOnMedia = uploadOnMedia;
        if (uploadVideoDeviceView == null || utils == null) return;
        title = title.trim();
        description = description.trim();
        if (TextUtils.isEmpty(url)) {
            uploadVideoDeviceView.showMessage(R.string.urlVideoNotEmpty);
            return;
        }

        File file = new File(url);
        if (!file.exists()) {
            uploadVideoDeviceView.showMessage(R.string.video_not_exists);
            return;
        }

        long sizeInBytes = file.length();
        long sizeInMb = sizeInBytes / (1024 * 1024);
        if (sizeInMb > MAX_SIZE_DEFAULT) {
            uploadVideoDeviceView.showMessage(R.string.videoTooLarge);
            return;
        }
        if (TextUtils.isEmpty(title)) {
            uploadVideoDeviceView.showMessage(R.string.titleVideoNotEmpty);
            return;
        }
        if (TextUtils.isEmpty(description)) {
            uploadVideoDeviceView.showMessage(R.string.desVideoNotEmpty);
            return;
        }

        String categoryId = "";
        ArrayList<Category> categories = utils.getCategories();
        for (Category category : categories) {
            if (category.getName().toLowerCase().equals(((String) selectedItem).toLowerCase())) {
                categoryId = category.getId();
                break;
            }
        }
        if (categoryId.isEmpty()) {
            uploadVideoDeviceView.showMessage(R.string.you_don_t_select_the_video_for_the_video);
//            uploadVideoDeviceView.showMessageCategory();
            return;
        }

        uploadVideoDeviceView.showDialogLoading();

        Video video = new Video();
        video.setOriginalPath(url);
        video.setTitle(title);
        video.setDescription(description);
        uploadVideo(video, categoryId);
    }

    private void uploadVideo(final Video video, final String categoryId) {
//        if(Utilities.checkAndGetNewPath(video.getOriginalPath(), application.getApplicationContext(), false).equals("false")){
//            if(uploadVideoDeviceView != null){
//                uploadVideoDeviceView.showMessage("you need change path file to valid ");
//            }
//            return;
//        }
        if (requestUpload != null) requestUpload.cancel();
        cancel = false;
        requestUpload = videoApi.uploadVideo(video, categoryId, new HttpProgressCallBack() {

            @Override
            public void onSuccess(String data) {
                if (videoApi == null) return;
                try {
                    JSONObject js = new JSONObject(data);
                    int code = js.optInt("errorCode");
                    if (code == 0) {
                        String url = js.optString("lavatar");
                        String itemvideo = js.getString("itemvideo");
                        video.setOriginalPath(url);
                        videoApi.uploadVideoInfo(video, isUploadOnMedia, categoryId, itemvideo, UploadVideoDevicePresenterImpl.this);
                    } else {
                        onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                        onGetVideosComplete();
                    }
                } catch (Exception e) {
                    onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                    onGetVideosComplete();
                }
            }

            @Override
            public void onProgressUpdate(int position, int sum, int percentage) {
                if (uploadVideoDeviceView != null) uploadVideoDeviceView.updateProgress(percentage);
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
//                onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                onGetVideosError(message);
                onGetVideosComplete();
            }
        });
    }

    @Override
    public void dismissUpload() {
        if (requestUpload != null) {
            requestUpload.cancel();
            requestUpload = null;
        }
        uploadVideoDeviceView.hideDialogLoading();
        cancel = true;
    }

    @Override
    public void onGetVideosSuccess(ArrayList<Video> list) {
        if (uploadVideoDeviceView == null) return;
        uploadVideoDeviceView.hideDialogLoading();
        uploadVideoDeviceView.showDialogSuccess();
//        uploadVideoDeviceView.showMessage(R.string.uploadVideoSuccess);
//        uploadVideoDeviceView.finish();
    }

    @Override
    public void onGetVideosError(String s) {
        if (uploadVideoDeviceView == null) return;
        uploadVideoDeviceView.hideDialogLoading();
        if (!cancel) uploadVideoDeviceView.showMessage(s);
    }

    @Override
    public void onGetVideosComplete() {
        if (uploadVideoDeviceView == null) return;
        uploadVideoDeviceView.hideLoading();
    }
}

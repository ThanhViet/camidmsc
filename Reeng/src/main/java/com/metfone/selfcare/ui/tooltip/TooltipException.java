package com.metfone.selfcare.ui.tooltip;

/**
 * Created by toanvk2 on 3/2/2016.
 */
public class TooltipException extends RuntimeException {
    public TooltipException(String e) {
        super(e);
    }

    public TooltipException() {
        super("TooltipException");
    }
}
package com.metfone.selfcare.ui.tabvideo.holder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.adapter.ChannelCircleAdapter;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.metfone.selfcare.ui.tabvideo.holder.ChannelHeaderHolder.ChannelObject.provideItemObject;

public class ChannelHeaderHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_see_all)
    TextView tvSeeAll;
    @BindView(R.id.channels_recycler_view)
    RecyclerView channelsRecyclerView;
    @BindView(R.id.tv_channels_title)
    TextView tvChannelsTitle;
    @BindView(R.id.tv_videos_title)
    TextView tvVideosTitle;

    private ChannelCircleAdapter channelAdapter;
    private OnChannelHeaderListener onChannelHeaderListener;

    public ChannelHeaderHolder(@NonNull BaseSlidingFragmentActivity context, @NonNull LayoutInflater layoutInflater, @NonNull ViewGroup parent, OnChannelHeaderListener onChannelHeaderListener) {
        super(layoutInflater.inflate(R.layout.item_subscribe_channel_header, parent, false));
        ButterKnife.bind(this, itemView);
        this.onChannelHeaderListener = onChannelHeaderListener;
        channelAdapter = new ChannelCircleAdapter(context);
        channelAdapter.setOnItemListener(onChannelHeaderListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManager.setInitialPrefetchItemCount(5);

        channelsRecyclerView.setLayoutManager(linearLayoutManager);
        channelsRecyclerView.setAdapter(channelAdapter);
        channelsRecyclerView.setHasFixedSize(true);
        channelsRecyclerView.setItemAnimator(null);
        channelsRecyclerView.setLayoutAnimation(null);
        channelsRecyclerView.setItemViewCacheSize(5);
        channelsRecyclerView.setDrawingCacheEnabled(true);

        itemView.setVisibility(View.GONE);
    }

    @OnClick(R.id.tv_see_all)
    public void onViewClicked() {
        if (onChannelHeaderListener != null)
            onChannelHeaderListener.onClickSeeAll();
    }

    public void bindData(ChannelObject itemChannelObject, boolean isTabVideo) {
        ArrayList<Channel> channelArrayList = itemChannelObject.getChannels();
        ArrayList<BaseAdapter.ItemObject> itemObjects = new ArrayList<>();

        if (Utilities.notEmpty(channelArrayList)) {
            if (itemView != null)
                itemView.setVisibility(View.VISIBLE);
            for (Channel channel : channelArrayList) {
                itemObjects.add(provideItemObject(channel));
            }
            channelAdapter.updateData(itemObjects);
        } else {
            if (itemView != null)
                itemView.setVisibility(View.GONE);
        }
        if (isTabVideo) {
            tvSeeAll.setVisibility(View.GONE);
            tvVideosTitle.setVisibility(View.GONE);
            tvChannelsTitle.setText(R.string.title_channel_on_video);
        } else {
            tvSeeAll.setVisibility(View.VISIBLE);
            tvVideosTitle.setVisibility(View.VISIBLE);
            tvChannelsTitle.setText(tvChannelsTitle.getContext().getString(R.string.channel_follow_count, itemObjects.size()));
        }
    }

    public static class ChannelObject implements BaseAdapter.Clone {

        private ArrayList<Channel> channels;

        ChannelObject(ArrayList<Channel> channels) {
            this.channels = channels;
        }

        public ArrayList<Channel> getChannels() {
            return channels;
        }

        public void setChannels(ArrayList<Channel> channels) {
            this.channels = channels;
        }

        @Override
        public BaseAdapter.Clone clone() {
            try {
                if (channels == null)
                    channels = new ArrayList<>();
                ArrayList<Channel> newChannel = new ArrayList<>();
                for (int i = 0; i < channels.size(); i++) {
                    Channel channel = channels.get(i);
                    newChannel.add(channel.clone());
                }
                return new ChannelObject(newChannel);
            } catch (Exception e) {
                return this;
            }
        }

        @Override
        public String toString() {
            return "ChannelObject{" +
                    "channels=" + channels +
                    '}';
        }

        public static BaseAdapter.ItemObject provideItemObject(ArrayList<Channel> channels) {
            BaseAdapter.ItemObject item = new BaseAdapter.ItemObject();
            item.setId(String.valueOf(System.currentTimeMillis()));
            item.setInfo(new ChannelHeaderHolder.ChannelObject(channels));
            return item;
        }

        static BaseAdapter.ItemObject provideItemObject(Channel channel) {
            BaseAdapter.ItemObject item = new BaseAdapter.ItemObject();
            item.setId(channel.getId());
            item.setInfo(channel);
            return item;
        }

    }

    public interface OnChannelHeaderListener extends BaseAdapter.OnItemListener, ChannelCircleAdapter.OnItemChannelCircleListener {
        void onClickSeeAll();
    }
}
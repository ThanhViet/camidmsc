package com.metfone.selfcare.ui.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.metfone.selfcare.util.FontUtils;

/**
 * EditText use font default base on language:
 * - English: SF Pro Text
 * - Cambodia: Nokora
 *
 * font-style: normal (default)
 * font-weight: normal, 500, 600
 *
 * if (font-style = normal) {
 *     if (font-weight = normal) -> SF Pro Text Regular(en), Nokora Regular(km)
 *     if (font-weight = 500) -> SF Pro Text Medium(en), Nokora Bold(km)
 *     if (font-weight = 600) -> SF Pro Text Semi bold(en), Nokora Bold(km)
 * }
 *
 * set font-weight: font-weight figma: normal -> app:cifWeight="normal"
 *                  font-weight figma: 500 -> app:cifWeight="w500"
 *                  font-weight figma: 600 -> app:cifWeight="w600"
 *
 */
public class CamIdEditText extends AppCompatEditText {
    public CamIdEditText(@NonNull Context context) {
        super(context);

        FontUtils.applyCustomFont(this, context, null);
    }

    public CamIdEditText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        FontUtils.applyCustomFont(this, context, attrs);
    }

    public CamIdEditText(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        FontUtils.applyCustomFont(this, context, attrs);
    }
}

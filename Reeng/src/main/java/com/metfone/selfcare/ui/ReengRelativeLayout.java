package com.metfone.selfcare.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

/**
 * Created by HaiKE on 8/1/17.
 */

public class ReengRelativeLayout extends RelativeLayout {

    private boolean needCirle = true;

    private static final String TAG = ReengRelativeLayout.class.getSimpleName();

    public void setNeedCirle(boolean needCirle) {
        this.needCirle = needCirle;
    }

    public ReengRelativeLayout(Context context) {
        super(context);
        setup();
    }

    public ReengRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public ReengRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    public ReengRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup();
    }

    private void setup() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            setLayerType(LAYER_TYPE_SOFTWARE, null);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        Paint paint = new Paint();
//        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        paint.setAntiAlias(true);
//        paint.setColor(getContext().getResources().getColor(R.color.transparent));
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN.SRC_IN));
        if (needCirle) {
            try {
                float radius = getContext().getResources().getDimension(R.dimen.ab_home_conner);
                Path path = getPath(radius, true, true, false, false);
                canvas.clipPath(path);
            } catch (Exception e) {
                Log.e(TAG, "clipPath() not supported", e);
            }
        }
        super.onDraw(canvas);
    }

    private Path getPath(float radius, boolean topLeft, boolean topRight,
                         boolean bottomRight, boolean bottomLeft) {

        final Path path = new Path();
        final float[] radii = new float[8];

        if (topLeft) {
            radii[0] = radius;
            radii[1] = radius;
        }

        if (topRight) {
            radii[2] = radius;
            radii[3] = radius;
        }

        if (bottomRight) {
            radii[4] = radius;
            radii[5] = radius;
        }

        if (bottomLeft) {
            radii[6] = radius;
            radii[7] = radius;
        }

        path.addRoundRect(new RectF(0, 0, getWidth(), getHeight()),
                radii, Path.Direction.CW);

        return path;
    }
}

package com.metfone.selfcare.ui.autoplay.calculator;

import android.view.View;

import com.metfone.selfcare.ui.autoplay.items.ListItem;

/**
 * Created by toanvk2 on 1/18/2018.
 */

public interface Callback<T extends ListItem> {
    void activateNewCurrentItem(T newListItem, View currentView, int position);

    void deactivateCurrentItem(T listItemToDeactivate, View view, int position);
}

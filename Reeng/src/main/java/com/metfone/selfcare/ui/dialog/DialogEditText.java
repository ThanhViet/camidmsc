package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 8/15/14.
 */
public class DialogEditText extends Dialog implements View.OnClickListener, TextWatcher {
    protected Button mBtnNegative, mBtnPositive;
    protected EditText mEdtInput;
    private BaseSlidingFragmentActivity activity;
    private String label;
    private String msg;
    private String textHint;
    private String negativeLabel;
    private String positiveLabel;
    private NegativeListener<String> negativeListener;
    private PositiveListener<String> positiveListener;
    private DismissListener dismissListener;
    private String oldContent;
    private int maxLength = -1;
    private int inputType = -1;
    private boolean checkEnable = false;
    private boolean multiLine = false;
    private boolean isSelectAll = false;
    private TextView mTvwTitle, mTvwMessage, mTvwMessageTop;
    private boolean autoDismiss = true;
    private boolean setTextFlag = true;
    private int currentSelection;

    private boolean contentTop = false;

    public DialogEditText(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        setCancelable(isCancelable);
    }

    public DialogEditText setCheckEnable(boolean checkEnable) {
        this.checkEnable = checkEnable;
        return this;
    }

    public DialogEditText setMultiLine(boolean multiLine) {
        this.multiLine = multiLine;
        return this;
    }

    public DialogEditText setLabel(String label) {
        this.label = label;
        return this;
    }

    public DialogEditText setContentTop(boolean contentTop) {
        this.contentTop = contentTop;
        return this;
    }

    public DialogEditText setMessage(String message) {
        this.msg = message;
        return this;
    }

    public DialogEditText setTextHint(String textHint) {
        this.textHint = textHint;
        return this;
    }

    public DialogEditText setOldContent(String oldContent) {
        this.oldContent = oldContent;
        return this;
    }

    public DialogEditText setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        return this;
    }

    public DialogEditText setInputType(int inputType) {
        this.inputType = inputType;
        return this;
    }

    public DialogEditText setNegativeLabel(String label) {
        this.negativeLabel = label;
        return this;
    }

    public DialogEditText setPositiveLabel(String label) {
        this.positiveLabel = label;
        return this;
    }

    public DialogEditText setNegativeListener(NegativeListener listener) {
        this.negativeListener = listener;
        return this;
    }

    public DialogEditText setPositiveListener(PositiveListener<String> listener) {
        this.positiveListener = listener;
        return this;
    }

    public DialogEditText setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    public DialogEditText setSelectAll(boolean selectAll) {
        isSelectAll = selectAll;
        return this;
    }

    public DialogEditText setAutoDismiss(boolean autoDismiss) {
        this.autoDismiss = autoDismiss;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_edit_text);
        findComponentViews();
        drawDetail();
        setListener();
    }

    @Override
    public void dismiss() {
        Log.d("DialogEditText", "dismiss");
        InputMethodUtils.hideSoftKeyboard(mEdtInput, activity);
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_button_negative:
                if (negativeListener != null) {
                    negativeListener.onNegative(null);
                }
                break;
            case R.id.dialog_button_positive:
                if (positiveListener != null) {
                    String input = mEdtInput.getText().toString().trim();
                    positiveListener.onPositive(input);
                }
                break;
        }
        if (autoDismiss)
            dismiss();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        enablePositiveButton(s.toString().trim());
        if (maxLength > 0) {
            if (s.toString().length() >= maxLength) {
                String msg = String.format(activity.getResources().getString(R.string.toast_type_max_length), maxLength);
                activity.showToast(msg);

                // set the text to a string max length 10:
                if (setTextFlag) {
                    setTextFlag = false;
                    currentSelection = mEdtInput.getSelectionEnd();
                    if (currentSelection > maxLength) currentSelection = maxLength;
                    mEdtInput.setText(s.subSequence(0, maxLength));
                    mEdtInput.setSelection(currentSelection);
                } else {
                    setTextFlag = true;
                }
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private void findComponentViews() {
        mTvwTitle = (TextView) findViewById(R.id.dialog_title);
        mEdtInput = (EditText) findViewById(R.id.dialog_input);
        mTvwMessage = (TextView) findViewById(R.id.dialog_message);
        mTvwMessageTop = (TextView) findViewById(R.id.dialog_message_top);
        mBtnNegative = (Button) findViewById(R.id.dialog_button_negative);
        mBtnPositive = (Button) findViewById(R.id.dialog_button_positive);
    }

    private void setListener() {
        mBtnPositive.setOnClickListener(this);
        mBtnNegative.setOnClickListener(this);
        mEdtInput.addTextChangedListener(this);
    }

    private void drawDetail() {
        if (multiLine) {
            mEdtInput.setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            //mEdtInput.setVerticalScrollBarEnabled(true);
            mEdtInput.setSingleLine(false);
            mEdtInput.setMaxLines(10);
        } else {
            mEdtInput.setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            //mEdtInput.setHorizontalScrollBarEnabled(true);
            mEdtInput.setSingleLine(true);
        }
        if (inputType != -1) {
            mEdtInput.setInputType(inputType);
        }
        if (TextUtils.isEmpty(label)) {
            mTvwTitle.setVisibility(View.GONE);
        } else {
            mTvwTitle.setVisibility(View.VISIBLE);
            mTvwTitle.setText(label);
        }
        if (TextUtils.isEmpty(msg)) {
            mTvwMessageTop.setVisibility(View.GONE);
            mTvwMessage.setVisibility(View.GONE);
        } else {
            if (contentTop) {
                mTvwMessageTop.setVisibility(View.VISIBLE);
                mTvwMessage.setVisibility(View.GONE);
                mTvwMessageTop.setText(msg);
            } else {
                mTvwMessageTop.setVisibility(View.GONE);
                mTvwMessage.setVisibility(View.VISIBLE);
                mTvwMessage.setText(msg);
            }
        }
        if (TextUtils.isEmpty(textHint)) {
            mEdtInput.setHint("");
        } else {
            mEdtInput.setHint(textHint);
        }
        if (!TextUtils.isEmpty(oldContent)) {
            mEdtInput.setText(oldContent);
            if (isSelectAll) {
                mEdtInput.selectAll();
            } else {
                mEdtInput.setSelection(oldContent.length());
            }
        }
        if (TextUtils.isEmpty(negativeLabel)) {
            mBtnNegative.setVisibility(View.GONE);
        } else {
            mBtnNegative.setVisibility(View.VISIBLE);
            mBtnNegative.setText(negativeLabel);
        }
        if (TextUtils.isEmpty(positiveLabel)) {
            mBtnPositive.setVisibility(View.GONE);
        } else {
            mBtnPositive.setVisibility(View.VISIBLE);
            mBtnPositive.setText(positiveLabel);
        }
        mEdtInput.requestFocus();
        InputMethodUtils.showSoftKeyboard(activity, mEdtInput);
        this.enablePositiveButton("");
    }

    private void enablePositiveButton(String currentContent) {
        if (checkEnable) {
            if (TextUtils.isEmpty(currentContent)) {
                mBtnPositive.setEnabled(false);
            } else if (TextUtils.isEmpty(oldContent)) {
                mBtnPositive.setEnabled(true);
            } else if (currentContent.equals(oldContent)) {
                mBtnPositive.setEnabled(false);
            } else {
                mBtnPositive.setEnabled(true);
            }
        } else {
            mBtnPositive.setEnabled(true);
        }
    }
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/24
 *
 */

package com.metfone.selfcare.ui.tabvideo.holder;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageLoader;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TopMoneyUploadHolder extends BaseAdapter.ViewHolder {

    private static boolean pause = false;
    @BindView(R.id.iv_avatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_score)
    TextView tvScore;
    @BindView(R.id.tv_position)
    TextView tvPosition;

    private Channel currentChannel;
    private OnItemChannelNormalListener onItemChannelListener;
    private ImageLoader imageLoader;

    public TopMoneyUploadHolder(@NonNull LayoutInflater layoutInflater, @NonNull ViewGroup parent, OnItemChannelNormalListener onItemChannelListener) {
        super(layoutInflater.inflate(R.layout.holder_top_money_upload, parent, false));
        ButterKnife.bind(this, itemView);
        this.onItemChannelListener = onItemChannelListener;
    }

    /**
     * khôi phục lại các tiền trình tải ảnh
     */
    public static void resumeRequests() {
        pause = false;
    }

    /**
     * dừng tiến trình load ảnh
     */
    public static void pauseRequests() {
        pause = true;
    }

    public void bindData(ChannelObject channelObject, int position) {
        imageLoader = channelObject.getChannelLoader();
        currentChannel = channelObject.getChannel();
        tvName.setText(currentChannel.getName());
        tvPosition.setText(String.valueOf(position + 3));
        if (TextUtils.isEmpty(channelObject.getTextTotalPoint())) {
            tvScore.setVisibility(View.GONE);
        } else {
            tvScore.setVisibility(View.VISIBLE);
            tvScore.setText(channelObject.getTextTotalPoint());
        }
        bindAvatar();
    }

    @OnClick({R.id.root_item})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.root_item:
                if (currentChannel != null) {
                    currentChannel.setHaveNewVideo(false);
                    ChannelDataSource.getInstance().saveTimeNewChannel(currentChannel);
                    if (onItemChannelListener != null)
                        onItemChannelListener.onClick(currentChannel);
                }
                break;
        }
    }

    public void bindAvatar() {
        if (imageLoader != null && ivAvatar != null)
            if (pause) {
                if (imageLoader.isCompleted())
                    imageLoader.into(ivAvatar);
                else
                    ivAvatar.setImageResource(imageLoader.getThumb());
            } else {
                imageLoader.into(ivAvatar);
            }
    }

    public interface OnItemChannelNormalListener extends BaseAdapter.OnItemListener, OnChannelListener {
    }

    public static class ChannelObject implements BaseAdapter.Clone {
        private Channel channel;
        private String textTotalPoint;
        private ImageLoader channelLoader;

        public static BaseAdapter.ItemObject provideItemObject(Channel channel, int position, Activity activity) {
            BaseAdapter.ItemObject item = new BaseAdapter.ItemObject();
            item.setId(channel.getId());
            item.setInfo(provideChannelObject(channel, position, activity));
            return item;
        }

        static ChannelObject provideChannelObject(Channel channel, int position, Activity activity) {
            int thumb = ImageManager.with().build().provideThumbError(position % ImageManager.thumbs.length - 1);
            ImageLoader channelLoader = ImageManager.with()
                    .setUrl(channel.getUrlImage())
                    .setSize(ScreenManager.getWidth(activity) / 4, ScreenManager.getWidth(activity) / 4)
                    .setTransformations(new CenterCrop())
                    .setThumbError(thumb)
                    .build()
                    .provideImageLoader();
            ChannelObject channelObject = new ChannelObject();
            channelObject.setChannel(channel);
            channelObject.setChannelLoader(channelLoader);
            return channelObject;
        }

        static String getTextTotalPoint(Channel channel) {
            if (channel == null) return "";
            return channel.getTextTotalPoint();
        }

        public Channel getChannel() {
            return channel;
        }

        public void setChannel(Channel channel) {
            this.channel = channel;
            textTotalPoint = getTextTotalPoint(channel);
        }

        public String getTextTotalPoint() {
            return textTotalPoint;
        }

        ImageLoader getChannelLoader() {
            return channelLoader;
        }

        void setChannelLoader(ImageLoader channelLoader) {
            this.channelLoader = channelLoader;
        }

        @Override
        public BaseAdapter.Clone clone() {
            try {
                ChannelObject channels = new ChannelObject();
                channels.channel = channel;
                channels.textTotalPoint = textTotalPoint;
                channels.channelLoader = channelLoader;
                return channels;
            } catch (Exception e) {
                return this;
            }
        }

        @Override
        public String toString() {
            return "ChannelObject{" +
                    "channel=" + channel +
                    ", textTotalPoint='" + textTotalPoint + '\'' +
                    '}';
        }

    }
}

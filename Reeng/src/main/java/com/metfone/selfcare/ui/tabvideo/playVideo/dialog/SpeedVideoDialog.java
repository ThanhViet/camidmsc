package com.metfone.selfcare.ui.tabvideo.playVideo.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SpeedVideoDialog extends BottomSheetDialog {

    @BindView(R.id.frame_content)
    LinearLayout frameContent;

    private OnSpeedVideoListener mOnSpeedVideoListener;
    private float speed = 1.0f;
    private ArrayList<Float> keys;
    private ArrayList<String> values;
    private Activity context;

    private View bottomSheet;

    public SpeedVideoDialog(@NonNull Activity context) {
        super(context);
        this.context = context;
        keys = new ArrayList<>();
        values = new ArrayList<>();

        add(0.25f, "0.25x");
        add(0.5f, "0.5x");
        add(0.75f, "0.75x");
        add(1.0f, context.getString(R.string.normal));
        add(1.25f, "1.25x");
        add(1.5f, "1.5x");
//        add(1.75f, "1.75x");
        add(2.0f, "2x");
    }

    private void add(float key, String value) {
        keys.add(key);
        values.add(value);
    }

    public void setOnSpeedVideoListener(OnSpeedVideoListener mOnSpeedVideoListener) {
        this.mOnSpeedVideoListener = mOnSpeedVideoListener;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_speed_video);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);

        View view;
        TextView tvCheckBox;
        ImageView ivCheckBox;
        for (int i = 0; i < keys.size(); i++) {
            float key = keys.get(i);
            String value = values.get(i);

            view = LayoutInflater.from(context).inflate(R.layout.layout_radio_button, null, false);
            view.setOnClickListener(mOnClickListener);
            view.setId(i);

            ivCheckBox = view.findViewById(R.id.iv_check_box);

            tvCheckBox = view.findViewById(R.id.tv_check_box);
            tvCheckBox.setText(value);

            if (key == speed)
                checkView(ivCheckBox, tvCheckBox);
            else
                ivCheckBox.setImageResource(R.drawable.ic_circle_unselected);

            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            frameContent.addView(view, layoutParams);
        }

        bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Resources resources = getContext().getResources();
                if (bottomSheet != null && resources != null && bottomSheet.getWidth() > bottomSheet.getHeight()) {
                    BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                    int height = ScreenManager.getWidth(context) / 2;

                    if (bottomSheetBehavior != null)
                        if (height > bottomSheet.getHeight())
                            bottomSheetBehavior.setPeekHeight(bottomSheet.getHeight());
                        else
                            bottomSheetBehavior.setPeekHeight(height);
                }
            }
        });
    }

    private void checkView(ImageView ivCheckBox, TextView tvCheckBox) {
//        ivCheckBox.setImageResource(R.drawable.ic_checkbox_video);
        ivCheckBox.setImageResource(R.drawable.ic_circle_selected);
        tvCheckBox.setTextColor(ContextCompat.getColor(context, R.color.videoColorSelect));
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mOnSpeedVideoListener != null && frameContent != null) {

                int position = frameContent.indexOfChild(view);

                int idx = Math.min(Math.max(position, 0), keys.size() - 1);

                mOnSpeedVideoListener.onSpeedVideo(keys.get(idx));
            }
            dismiss();
        }
    };

    public interface OnSpeedVideoListener {
        void onSpeedVideo(float speed);
    }
}

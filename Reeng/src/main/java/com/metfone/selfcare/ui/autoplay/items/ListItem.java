package com.metfone.selfcare.ui.autoplay.items;

import android.view.View;

/**
 * Created by toanvk2 on 1/18/2018.
 */

public interface ListItem {
    int getVisibilityPercents(View view);
    void setActive(View newActiveView, int newActiveViewPosition);
    void deactivate(View currentView, int position);
}

package com.metfone.selfcare.ui.dialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.dialog.BottomSheetMenuAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/7/2017.
 */

public class BottomSheetMenu extends BottomSheetDialog {
    private static final String TAG = BottomSheetMenu.class.getSimpleName();
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ArrayList<ItemContextMenu> listItem;
    private BottomSheetListener mCallBack;
    private RecyclerView mRecyclerView;

    private View bottomSheet;
    private boolean isFullScreenVideo;

    public BottomSheetMenu(@NonNull BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity);
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplicationContext();
        setCancelable(isCancelable);
    }

    public BottomSheetMenu setListItem(ArrayList<ItemContextMenu> items) {
        this.listItem = items;
        return this;
    }

    public BottomSheetMenu setListener(BottomSheetListener listener) {
        this.mCallBack = listener;
        return this;
    }

    public BottomSheetMenu setFullScreenVideo(boolean fullScreenVideo) {
        this.isFullScreenVideo = fullScreenVideo;
        return this;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.bottom_sheet_menu);
        Window window = getWindow();
        if (window != null) {
            if (isFullScreenVideo)
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            else
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        findComponentViews();
        setAdapter();
        /*drawDetail();
        setListener();*/

        bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        setOnShowListener(dialogInterface -> {
            if (bottomSheet != null) {
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                if (bottomSheetBehavior != null) {
                    if (isFullScreenVideo) {
                        bottomSheetBehavior.setPeekHeight(1000);
                    } else {
                        if (DeviceUtils.isLandscape(activity)) {
                            int height = ScreenManager.getHeight() - Utilities.dpToPx(56);
                            bottomSheetBehavior.setPeekHeight(height);
                        } else {
                            int height = ScreenManager.getHeight() * 3 / 4;
                            bottomSheetBehavior.setPeekHeight(height);
                        }
                    }
                }
//                    Resources resources = getContext().getResources();
//                    if (isFullScreenVideo) {
//                        if (bottomSheetBehavior == null) return;
//                        bottomSheetBehavior.setPeekHeight(1000);
//                    } else if (resources != null && bottomSheet.getWidth() > bottomSheet.getHeight()) {
//                        int height = ScreenManager.getWidth(activity) / 2;
//                        if (bottomSheetBehavior != null)
//                            if (height > bottomSheet.getHeight())
//                                bottomSheetBehavior.setPeekHeight(bottomSheet.getHeight());
//                            else
//                                bottomSheetBehavior.setPeekHeight(height);
//                    }
            }
        });
    }

    @Override
    public void dismiss() {
        Log.d("BottomSheetMenu", "dismiss");
        super.dismiss();
        /*if (dismissListener != null) {
            dismissListener.onDismiss();
        }*/
    }

    private void findComponentViews() {
        mRecyclerView = findViewById(R.id.bottom_sheet_recycler_view);
    }

    private void setAdapter() {
        BottomSheetMenuAdapter mAdapter = new BottomSheetMenuAdapter(listItem);
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                Log.d(TAG, "onClick");
                if (mCallBack != null) {
                    ItemContextMenu item = (ItemContextMenu) object;
                    mCallBack.onItemClick(item.getActionTag(), item.getObj());
                }
                dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
        mRecyclerView.setAdapter(mAdapter);
    }
}

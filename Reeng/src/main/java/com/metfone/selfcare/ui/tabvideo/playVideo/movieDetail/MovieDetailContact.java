package com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.di.MvpPresenter;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;

public interface MovieDetailContact {

    interface MovieDetailView extends BaseView {
        void bindData(ArrayList<Video> videos, boolean loadMore);

        void updateUiChannel();

        void updateUiVideo();

        void updateUiVideo(Video result);
    }

    interface MovieDetailPresenter extends MvpPresenter {

        void setVideo(Video currentVideo);

        void openDetail(BaseSlidingFragmentActivity activity, Channel channel);

        void getVolumeFilmGroups();

        void getMoveInfo(Video video);

        void like(BaseSlidingFragmentActivity activity, Video video);

        void comment(BaseSlidingFragmentActivity activity, Video currentVideo);

        void save(Video currentVideo);

        void sub(Channel channel);
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/27
 *
 */

package com.metfone.selfcare.ui.tabvideo.listener;

import com.metfone.selfcare.model.tab_video.Video;

public interface FullPlayerListener {
    public interface OnActionFullScreenListener {

        /**
         * thông báo chia sẻ video
         *
         * @param currentVideo currentVideo
         */
        void handlerLikeVideo(Video currentVideo);

        /**
         * thông báo chia sẻ video
         *
         * @param currentVideo currentVideo
         */
        void handlerShareVideo(Video currentVideo);
    }

    public interface OnFullScreenListener {
        /**
         * huy đia log
         */
        void onDismiss();

        /**
         * thông báo play video tiếp theo
         *
         * @param nextVideo thông tin video
         */
        void onPlayNextVideoForward(Video nextVideo);

        void onPlayEpisode(Video video, int position);

        void onChangeSubtitleAudio(Video video);
    }

    public interface ProviderFullScreen {
        /**
         * cung cấp video tiếp theo
         *
         * @return thông tin video
         */
        Video provideVideoForward();

        Video provideCurrentVideo();
    }
}

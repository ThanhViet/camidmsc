package com.metfone.selfcare.ui.dialog;

/**
 * Created by HaiKE on 7/13/2017.
 */

public interface HyperlinkListener {
    void onClickHyperLink(String link);
}

package com.metfone.selfcare.ui.tabvideo.listener;

import com.metfone.selfcare.common.utils.listener.Listener;

/**
 * Created by HoangAnhTuan on 3/26/2018.
 */

public interface OnTabListener extends Listener {
    void onTabReselected(int currentPosition);

    void onTabSelected(int position);

    void onDisableLoading();
}

package com.metfone.selfcare.ui.autoplay.utils;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/18/2018.
 */

public class RecyclerViewItemPositionGetter implements ItemsPositionGetter {
    private static final String TAG = RecyclerViewItemPositionGetter.class.getSimpleName();

    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;

    public RecyclerViewItemPositionGetter(LinearLayoutManager layoutManager, RecyclerView recyclerView) {
        mLayoutManager = layoutManager;
        mRecyclerView = recyclerView;
    }

    @Override
    public View getChildAt(int position) {
        View view = mLayoutManager.getChildAt(position);
        Log.i(TAG, "mRecyclerView getChildAt, position " + position + ", view " + view);
        Log.i(TAG, "mLayoutManager getChildAt, position " + position + ", view " + mLayoutManager.getChildAt(position));
        return view;
    }

    @Override
    public int indexOfChild(View view) {
        int indexOfChild = mRecyclerView.indexOfChild(view);
        return indexOfChild;
    }

    @Override
    public int getChildCount() {
        int childCount = mRecyclerView.getChildCount();
        Log.i(TAG, "getChildCount, mRecyclerView " + childCount);
        Log.i(TAG, "getChildCount, mLayoutManager " + mLayoutManager.getChildCount());
        return childCount;
    }

    @Override
    public int getLastVisiblePosition() {
        return mLayoutManager.findLastVisibleItemPosition();
    }

    @Override
    public int getFirstVisiblePosition() {
        Log.i(TAG, "getFirstVisiblePosition, findFirstVisibleItemPosition " + mLayoutManager.findFirstVisibleItemPosition());
        return mLayoutManager.findFirstVisibleItemPosition();
    }
}

package com.metfone.selfcare.ui.tabvideo.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.Window;
import android.view.WindowManager;

public abstract class BaseDialog extends Dialog {

    private boolean isFullScreen = false;
    private int height;
    private int width;

    public BaseDialog(@NonNull Context context) {
        super(context);
    }

    public BaseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected BaseDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setFullScreen(boolean fullScreen) {
        isFullScreen = fullScreen;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    private Window window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        window = getWindow();
        if (isFullScreen)
            setFullScreen();
        setContentView(getLayoutResId());
        setLayoutParam();

        initView();
    }

    /**
     * cung cấp layout
     *
     * @return LayoutResId
     */
    protected abstract int getLayoutResId();

    /**
     * khởi tao view
     */
    protected abstract void initView();

    void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (window != null) {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        }
    }

    void setLayoutParam() {
        if (window != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(window.getAttributes());
            layoutParams.width = width;
            layoutParams.height = height;
            layoutParams.dimAmount = 0f;
            window.setAttributes(layoutParams);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

}

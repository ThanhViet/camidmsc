package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.youtubes;

public interface UploadVideoYoutubePresenter {
    void getCategories();

    void uploadVideo(String s, boolean uploadOnMedia, Object item);
}

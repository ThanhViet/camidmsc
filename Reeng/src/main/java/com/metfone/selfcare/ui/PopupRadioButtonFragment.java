package com.metfone.selfcare.ui;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.metfone.selfcare.adapter.RadioButtonAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.listeners.ClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/9/14.
 */
public class PopupRadioButtonFragment extends DialogFragment {
    private String menuTitle = "test";
    private RadioButtonAdapter menuAdapter = null;
    private ClickListener.IconListener clickHandler = null;
    private Context mContext;

    /**
     * @param context
     */

    public static PopupRadioButtonFragment newInstance(Context context, String title, ArrayList<ItemContextMenu> listItem,
                                                       ClickListener.IconListener callBack) {
        PopupRadioButtonFragment dialog = new PopupRadioButtonFragment();
        dialog.mContext = context;
        dialog.menuAdapter = new RadioButtonAdapter(context);
        dialog.menuTitle = title;
        dialog.clickHandler = callBack;
        dialog.menuAdapter.setListItem(listItem);
        dialog.setCancelable(true);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = initControl(inflater, container);
        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(true);
        }
//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
        return v;
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private View initControl(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.popup_context_menu, null, false);
        EllipsisTextView title = (EllipsisTextView) view
                .findViewById(R.id.context_menu_title);
        if (menuTitle != null) {
            title.setVisibility(View.VISIBLE);
            title.setEmoticon(mContext, menuTitle, menuTitle.hashCode(), menuTitle);
        } else {
            title.setVisibility(View.GONE);
        }
        ListView listItem = (ListView) view
                .findViewById(R.id.context_menu_listview);
        // add footer and header
        listItem.setAdapter(menuAdapter);
        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemContextMenu item = (ItemContextMenu) parent.getItemAtPosition(position);
                if (clickHandler != null) {
                    clickHandler.onIconClickListener(view, item.getObj(), item.getActionTag());
                }
                dismiss();
            }
        });
        return view;
    }
}
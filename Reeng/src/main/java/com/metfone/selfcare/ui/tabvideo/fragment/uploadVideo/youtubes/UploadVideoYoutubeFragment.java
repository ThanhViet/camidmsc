package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.youtubes;

import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.NotifyUploadVideoOnMediaDialog;
import com.metfone.selfcare.util.Log;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class UploadVideoYoutubeFragment extends BaseFragment implements UploadVideoYoutubeView,
        AdapterView.OnItemSelectedListener {

    @BindView(R.id.edLinkYouTube)
    EditText edLinkYouTube;
    @BindView(R.id.spCategory)
    Spinner spCategory;
    @BindView(R.id.btnUpload)
    Button btnUpload;
    @BindView(R.id.tvNotifyCategoryEmpty)
    TextView tvNotifyCategoryEmpty;
    @BindView(R.id.checkbox_upload_onmedia)
    CheckBox checkboxUploadOnmedia;
    Unbinder unbinder;

    @Inject
    UploadVideoYoutubePresenter uploadVideoYoutubePresenter;
    @BindView(R.id.tvTerm)
    TextView tvTerm;

    private boolean finish = false;
    private boolean start = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload_video_youtube, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        uploadVideoYoutubePresenter.getCategories();
        checkboxUploadOnmedia.setChecked(true);
        checkboxUploadOnmedia.setOnCheckedChangeListener(mOnCheckedChangeListener);
        tvTerm.setPaintFlags(tvTerm.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public void updateDataCategories(List<String> categories) {
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(activity, R.layout.simple_list_item_categories, categories);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setOnItemSelectedListener(this);
        spCategory.setAdapter(stringArrayAdapter);
    }

    @Override
    public void showMessage(int message) {
        activity.showToast(message);
    }

    @Override
    public void showMessage(String s) {
        Log.i(UploadVideoYoutubeFragment.class.getSimpleName(), "showMessage: " + s);
        activity.showToast(s);
    }

    @Override
    public void showDialogLoading() {
        activity.showLoadingDialog("", R.string.loading);
    }

    @Override
    public void hideDialogLoading() {
        activity.hideLoadingDialog();
    }

    @Override
    public void finish() {
        if (finish) return;
        finish = true;
        activity.finish();
    }

    @Override
    public void showMessageCategory() {
        tvNotifyCategoryEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSuccess() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 10 * 1000);

        DialogConfirm dialogConfirm = new DialogConfirm(activity, false);
        dialogConfirm.setLabel(getString(R.string.notification));
        dialogConfirm.setMessage(getString(R.string.upload_video_success));
        dialogConfirm.setPositiveLabel(getString(R.string.close));
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {

            @Override
            public void onPositive(Object result) {
                finish();
            }
        });
        dialogConfirm.setDismissListener(new DismissListener() {

            @Override
            public void onDismiss() {
                finish();
            }
        });
        dialogConfirm.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnUpload, R.id.tvTerm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpload:
                uploadVideoYoutubePresenter.uploadVideo(edLinkYouTube.getText().toString(), checkboxUploadOnmedia.isChecked(), spCategory.getSelectedItem());
                break;
            case R.id.tvTerm:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://survey?ref=http://m.video.mocha.com.vn/theleupload.html");
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (start) {
            start = false;
            return;
        }
        if (i > 0) tvNotifyCategoryEmpty.setVisibility(View.INVISIBLE);
        else tvNotifyCategoryEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }


//    private boolean showNotify = false;

    private void showNotifyUploadVideoOnMedia() {
//        showNotify = true;
        NotifyUploadVideoOnMediaDialog notifyUploadVideoOnMediaDialog = new NotifyUploadVideoOnMediaDialog(activity);
        notifyUploadVideoOnMediaDialog.setTextTitle(R.string.title_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextMessage(R.string.message_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextSayAgain(R.string.say_again_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextNegative(R.string.negative_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextPositive(R.string.positive_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setmNotifyUploadVideoOnMediaListener(mNotifyUploadVideoOnMediaListener);
        notifyUploadVideoOnMediaDialog.show();
    }

    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            boolean enable = SharedPrefs.getInstance().get(Constants.TabVideo.ENABLE_NOTIFY_UPLOAD_VIDEO_ON_MEDIA, Boolean.class, true);
            if (enable && !b /*&& !showNotify*/)
                showNotifyUploadVideoOnMedia();
        }
    };

    private NotifyUploadVideoOnMediaDialog.NotifyUploadVideoOnMediaListener mNotifyUploadVideoOnMediaListener = new NotifyUploadVideoOnMediaDialog.NotifyUploadVideoOnMediaListener() {
        @Override
        public void onNegativeClicked() {
            if (checkboxUploadOnmedia != null)
                checkboxUploadOnmedia.setChecked(true);
//            showNotify = false;
        }

        @Override
        public void onPositiveClicked() {
            if (checkboxUploadOnmedia != null)
                checkboxUploadOnmedia.setChecked(false);
//            showNotify = false;
        }

        @Override
        public void onShowAgainClicked(boolean isChecked) {
            SharedPrefs.getInstance().put(Constants.TabVideo.ENABLE_NOTIFY_UPLOAD_VIDEO_ON_MEDIA, !isChecked);
        }
    };
}

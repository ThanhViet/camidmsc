package com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement;

import androidx.fragment.app.Fragment;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.di.MvpPresenter;
import com.metfone.selfcare.model.tab_video.Channel;

import java.util.ArrayList;

public interface ChannelManagementContact {

    interface ChannelManagementView extends BaseView {

        void bindData(ArrayList<Channel> channels, boolean loadMore);

    }

    interface ChannelManagementPresenter extends MvpPresenter {
        void getChannelUserFollow();

        void getRefreshChannelUserFollow();

        void getMoreChannelUserFollow();

        void subscribe(BaseSlidingFragmentActivity activity, Channel channel);

        void openDetail(BaseSlidingFragmentActivity activity, Channel channel);
    }

    interface ChannelManagementProvider {

        Fragment provideFragment();
    }
}

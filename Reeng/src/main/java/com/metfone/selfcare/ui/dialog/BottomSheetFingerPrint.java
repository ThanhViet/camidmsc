package com.metfone.selfcare.ui.dialog;

import android.content.DialogInterface;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.fingerprint.FingerPrintHelper;
import com.metfone.selfcare.ui.CusBottomSheetDialog;
import com.metfone.selfcare.util.Log;

public class BottomSheetFingerPrint extends FingerprintManagerCompat.AuthenticationCallback {
    private static final String TAG = BottomSheetFingerPrint.class.getSimpleName();
    TextView tvFingerPrintStatus;
    View btnLoginByPassword;
    BaseSlidingFragmentActivity activity;
    CusBottomSheetDialog bottomSheetDialog;
    FingerPrintHelper fingerPrintHelper;
    FingerPrintCallback callback;
    private boolean isShowing = false;

    public BottomSheetFingerPrint(final BaseSlidingFragmentActivity context, FingerPrintCallback fingerPrintCallback) {
        View viewParent = LayoutInflater.from(context).inflate(R.layout.dialog_fingerprint, null, false);
        bottomSheetDialog = new CusBottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        bottomSheetDialog.setContentView(viewParent);
        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                try {
                    FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
                    BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                    bottomSheetDialog.setLockDragging(true);
                    fingerPrintHelper = new FingerPrintHelper();
                    fingerPrintHelper.authenticate(context, BottomSheetFingerPrint.this);
                    isShowing = true;
                } catch (Exception e) {
                    Log.d(TAG, "show: Can not expand", e);
                }
            }
        });

        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (fingerPrintHelper != null) {
                    fingerPrintHelper.cancelFingerPrintAuth();
                }
                fingerPrintHelper = null;
                btnLoginByPassword = null;
                tvFingerPrintStatus = null;
                activity = null;
                callback = null;
                isShowing = false;
            }
        });
        this.activity = context;
        this.callback =  fingerPrintCallback;
        init(viewParent);
    }

    protected void init(View viewParent) {
        tvFingerPrintStatus = (TextView) viewParent.findViewById(R.id.tv_fingerprint_status);
        btnLoginByPassword = viewParent.findViewById(R.id.btnLoginByPassword);
        btnLoginByPassword.setOnClickListener(onClickListener);
    }

    public boolean isShowing() {
        return isShowing;
    }

    public void dismiss() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.dismiss();
            bottomSheetDialog = null;
        }
        if (fingerPrintHelper != null) {
            fingerPrintHelper.cancelFingerPrintAuth();
        }
        callback = null;
        fingerPrintHelper = null;
        btnLoginByPassword = null;
        tvFingerPrintStatus = null;
        activity = null;
        isShowing = false;
    }

    public void show() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.show();
        }
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (activity == null) return;
            switch (v.getId()) {
                case R.id.btnLoginByPassword:
                    dismiss();
                    break;

                default:
                    dismiss();
                    break;
            }
        }
    };

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        if (tvFingerPrintStatus != null) {
            tvFingerPrintStatus.setText(errString);
        }
        if (callback != null) {
            callback.onAuthenticationError(errMsgId, errString);
        }
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        if (tvFingerPrintStatus != null) {
            tvFingerPrintStatus.setText(helpString);
        }
        if (callback != null) {
            callback.onAuthenticationHelp(helpMsgId, helpString);
        }
    }

    @Override
    public void onAuthenticationFailed() {
        if (tvFingerPrintStatus != null && activity != null) {
            tvFingerPrintStatus.setText(activity.getString(R.string.fingerprint_failed));
        }
        if (callback != null) {
            callback.onAuthenticationFailed();
        }
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        if (callback != null) {
            callback.onAuthenticationSucceeded(result);
        }
    }

    public interface FingerPrintCallback {
        void onAuthenticationError(int errMsgId, CharSequence errString);

        void onAuthenticationHelp(int helpMsgId, CharSequence helpString);

        void onAuthenticationFailed();

        void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result);
    }
}

package com.metfone.selfcare.ui.guestbook;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.metfone.selfcare.adapter.guestbook.SelectEmoAdapter;
import com.metfone.selfcare.adapter.guestbook.SelectEmoPagerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.EmoCollection;
import com.metfone.selfcare.database.model.guestbook.EmoItem;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.OnSelectSticker;
import com.metfone.selfcare.ui.viewpagerindicator.IconIndicator;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class DialogSelectEmoSticker extends DialogFragment implements SelectEmoAdapter.OnEmoItemClick {
    private static final String TAG = DialogSelectEmoSticker.class.getSimpleName();
    private Context mContext;
    private OnSelectSticker callback;
    private ViewPager mViewPager;
    private EmoPageIndicator mIndicator;
    private SelectEmoPagerAdapter mAdapter;
    protected ArrayList<EmoCollection> emoCollections;

    public static DialogSelectEmoSticker newInstance(ArrayList<EmoCollection> collections) {
        DialogSelectEmoSticker fragment = new DialogSelectEmoSticker();
        fragment.emoCollections = collections;
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSelectSticker) {
            this.callback = (OnSelectSticker) context;
            this.mContext = context;
        } else {
            throw new IllegalStateException(context.getClass().getName()
                    + " must implement " + OnSelectSticker.class.getName());
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(true);
        }*/
        return inflater.inflate(R.layout.dialog_select_emo_sticker, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getData(savedInstanceState);
        mViewPager = (ViewPager) view.findViewById(R.id.dialog_emo_pager);
        mIndicator = (EmoPageIndicator) view.findViewById(R.id.dialog_emo_indicator);
        drawDetail();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void getData(Bundle savedInstanceState) {
       /* if (savedInstanceState != null) {
            inputFont = savedInstanceState.getString(Constants.INPUT_FONT);
            dialogTitle = savedInstanceState.getString(Constants.DIALOG_TITLE_LABEL);
        } else if (getArguments() != null) {
            inputFont = getArguments().getString(Constants.INPUT_FONT);
            dialogTitle = getArguments().getString(Constants.DIALOG_TITLE_LABEL);
        }*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // clearing memory on exit, cos manipulating with text uses bitmaps extensively
        // this does not frees memory immediately, but still can help
       /* System.gc();
        Runtime.getRuntime().gc();*/
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                int height = ((ApplicationController) getContext().getApplicationContext()).getHeightPixels();
                height = (height * 2) / 3;
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, height);
                // remove background
                /*window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);*/
                // remove dim
                /*window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/
                /*WindowManager.LayoutParams windowParams = window.getAttributes();
                window.setDimAmount(0.0F);
                window.setAttributes(windowParams);*/
            }
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onDetach() {
        // release links
        this.callback = null;
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }

    @Override
    public void onEmoClick(EmoItem item) {
        callback.onEmoItemSelected(item);
        dismiss();
    }

    private void drawDetail() {
        initAdapter();
        setListener();
    }

    private void initAdapter() {
        //emoCollections.add(new EmoCollection());
        mAdapter = new SelectEmoPagerAdapter(mContext, emoCollections, getListIcon(emoCollections), this);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mIndicator.setViewPager(mViewPager);
        mIndicator.notifyDataSetChanged();
        mIndicator.setCurrentItem(0);
        mViewPager.setCurrentItem(0);
    }

    private void setListener() {

    }

    private ArrayList<IconIndicator> getListIcon(ArrayList<EmoCollection> collections) {
        ArrayList<IconIndicator> icons = new ArrayList<>();
        for (EmoCollection collection : collections) {
            String avatar = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(collection.getAvatar());
            icons.add(new IconIndicator(avatar, avatar));
        }
        return icons;
    }
}
package com.metfone.selfcare.ui.view.drag_select;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by huongnd38 on 02.01.2019.
 */

public class DragSelectionProcessor implements DragSelectTouchListener.OnAdvancedDragSelectListener {

    /**
     *  Different existing selection modes
     */
    public static class Mode
    {
        /**
         * simply selects each item you go by and unselects on move back
         */
        public static final int SIMPLE = 1;
        /**
         * toggles each items original state, reverts to the original state on move back
         */
        public static final int TOGGLE_AND_UNDO = 2;
        /**
         * toggles the first item and applies the same state to each item you go by and applies inverted state on move back
         */
        public static final int FIRST_ITEM_DEPENDENT = 3;
        /**
         * toggles the item and applies the same state to each item you go by and reverts to the original state on move back
         */
        public static final int FIRST_ITEM_DEPENDENT_TOGGLE_AND_UNDO = 4;
    }

    private int mMode;
    private ISelectionHandler mSelectionHandler;
    private ISelectionStartFinishedListener mStartFinishedListener;
    private HashSet<Integer> mOriginalSelection;
    private boolean mFirstWasSelected;
    private boolean mCheckSelectionState = false;

    /**
     * @param selectionHandler the handler that takes care to handle the selection events
     */
    public DragSelectionProcessor(ISelectionHandler selectionHandler)
    {
        mMode = Mode.SIMPLE;
        mSelectionHandler = selectionHandler;
        mStartFinishedListener = null;
    }

    /**
     * @param mode the mode in which the selection events should be processed
     * @return this
     */
    public DragSelectionProcessor withMode(int mode)
    {
        mMode = mode;
        return this;
    }

    /**
     * @param startFinishedListener a listener that get's notified when the drag selection is started or finished
     * @return this
     */
    public DragSelectionProcessor withStartFinishedListener(ISelectionStartFinishedListener startFinishedListener)
    {
        mStartFinishedListener = startFinishedListener;
        return this;
    }

    /**
     * If this is enabled, the processor will check if an items selection state is toggled before notifying the {@link ISelectionHandler}
     * @param check true, if this check should be enabled
     * @return this
     */
    public DragSelectionProcessor withCheckSelectionState(boolean check)
    {
        mCheckSelectionState = check;
        return this;
    }

    @Override
    public void onSelectionStarted(int start)
    {
        mOriginalSelection = new HashSet<>();
        Set<Integer> selected = mSelectionHandler.getSelection();
        if (selected != null)
            mOriginalSelection.addAll(selected);
        mFirstWasSelected = mOriginalSelection.contains(start);

        switch (mMode)
        {
            case Mode.SIMPLE:
            {
                mSelectionHandler.updateSelection(start, start, true, true);
                break;
            }
            case Mode.TOGGLE_AND_UNDO:
            {
                mSelectionHandler.updateSelection(start, start, !mOriginalSelection.contains(start), true);
                break;
            }
            case Mode.FIRST_ITEM_DEPENDENT:
            {
                mSelectionHandler.updateSelection(start, start, !mFirstWasSelected, true);
                break;
            }
            case Mode.FIRST_ITEM_DEPENDENT_TOGGLE_AND_UNDO:
            {
                mSelectionHandler.updateSelection(start, start, !mFirstWasSelected, true);
                break;
            }
        }
        if (mStartFinishedListener != null)
            mStartFinishedListener.onSelectionStarted(start, mFirstWasSelected);
    }

    @Override
    public void onSelectionFinished(int end)
    {
        mOriginalSelection = null;

        if (mStartFinishedListener != null)
            mStartFinishedListener.onSelectionFinished(end);
    }

    @Override
    public void onSelectChange(int start, int end, boolean isSelected)
    {
        switch (mMode)
        {
            case Mode.SIMPLE:
            {
                if (mCheckSelectionState)
                    checkedUpdateSelection(start, end, isSelected);
                else
                    mSelectionHandler.updateSelection(start, end, isSelected, false);
                break;
            }
            case Mode.TOGGLE_AND_UNDO:
            {
                for (int i = start; i <= end; i++)
                    checkedUpdateSelection(i, i, isSelected ? !mOriginalSelection.contains(i) :  mOriginalSelection.contains(i));
                break;
            }
            case Mode.FIRST_ITEM_DEPENDENT:
            {
                checkedUpdateSelection(start, end, isSelected ? !mFirstWasSelected :  mFirstWasSelected);
                break;
            }
            case Mode.FIRST_ITEM_DEPENDENT_TOGGLE_AND_UNDO:
            {
                for (int i = start; i <= end; i++)
                    checkedUpdateSelection(i, i, isSelected ? !mFirstWasSelected :  mOriginalSelection.contains(i));
                break;
            }
        }
    }

    private void checkedUpdateSelection(int start, int end, boolean newSelectionState)
    {
        if (mCheckSelectionState)
        {
            for (int i = start; i <= end; i++)
            {
                if (mSelectionHandler.isSelected(i) != newSelectionState)
                    mSelectionHandler.updateSelection(i, i, newSelectionState, false);
            }
        }
        else
            mSelectionHandler.updateSelection(start, end, newSelectionState, false);
    }

    public interface ISelectionHandler
    {
        /**
         * @return the currently selected items => can be ignored for {@link Mode#SIMPLE} and {@link Mode#FIRST_ITEM_DEPENDENT}
         */
        Set<Integer> getSelection();

        /**
         * only used, if {@link DragSelectionProcessor#withCheckSelectionState(boolean)} was enabled
         * @param index the index which selection state wants to be known
         * @return the current selection state of the passed in index
         */
        boolean isSelected(int index);

        /**
         * update your adapter and select select/unselect the passed index range, you be get a single for all modes but {@link Mode#SIMPLE} and {@link Mode#FIRST_ITEM_DEPENDENT}
         *
         * @param start      the first item of the range who's selection state changed
         * @param end         the last item of the range who's selection state changed
         * @param isSelected      true, if the range should be selected, false otherwise
         * @param calledFromOnStart true, if it was called from the {@link DragSelectionProcessor#onSelectionStarted(int)} event
         */
        void updateSelection(int start, int end, boolean isSelected, boolean calledFromOnStart);
    }

    public interface ISelectionStartFinishedListener
    {
        /**
         * @param start      the item on which the drag selection was started at
         * @param originalSelectionState the original selection state
         */
        void onSelectionStarted(int start, boolean originalSelectionState);

        /**
         * @param end      the item on which the drag selection was finished at
         */
        void onSelectionFinished(int end);
    }
}


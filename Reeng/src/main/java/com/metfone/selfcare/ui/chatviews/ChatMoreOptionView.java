package com.metfone.selfcare.ui.chatviews;

import android.view.View;
import android.widget.LinearLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;

/**
 * Created by thanhnt72 on 6/29/2015.
 */
public class ChatMoreOptionView {
    private static final String TAG = ChatMoreOptionView.class.getSimpleName();
    private LinearLayout mLayoutSendLocation, mLayoutSendContact, mLayoutSendEsport;
    private OnMoreOptionListener moreOptionListener;

    public ChatMoreOptionView(ApplicationController app, View parentLayout, OnMoreOptionListener listener) {
        mLayoutSendLocation = (LinearLayout) parentLayout.findViewById(R.id.layout_send_location);
        mLayoutSendContact = (LinearLayout) parentLayout.findViewById(R.id.layout_send_contact);
        mLayoutSendEsport = parentLayout.findViewById(R.id.layout_send_esport);
        this.moreOptionListener = listener;
        setViewListener();
    }

    private void setViewListener() {

        mLayoutSendContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (moreOptionListener != null) {
                    moreOptionListener.onSendContact();
                }
            }
        });

        mLayoutSendLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (moreOptionListener != null) {
                    moreOptionListener.onShareLocation();
                }
            }
        });

        mLayoutSendEsport.setOnClickListener(vv->{
            if(moreOptionListener != null){
                moreOptionListener.onShareEsport();
            }
        });
    }

    public interface OnMoreOptionListener {
        void onSendContact();

        void onShareLocation();

        void onShareEsport();
    }
}
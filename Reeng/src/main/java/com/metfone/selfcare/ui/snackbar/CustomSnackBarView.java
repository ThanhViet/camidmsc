/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2020/4/14
 *
 */

package com.metfone.selfcare.ui.snackbar;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;

public class CustomSnackBarView extends ConstraintLayout {
    private TextView tvTitle;
    private TextView btnSubmit;
    private View constraintView;

    public CustomSnackBarView(Context context) {
        super(context);
        init();
    }

    public CustomSnackBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomSnackBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View.inflate(getContext(), R.layout.view_custom_snackbar, this);
        constraintView = findViewById(R.id.snack_constraint);
        tvTitle = findViewById(R.id.tv_title);
        btnSubmit = findViewById(R.id.button_submit);
    }

    public void setTitle(String title) {
        if (tvTitle != null) {
            if (TextUtils.isEmpty(title)) tvTitle.setText("");
            else tvTitle.setText(title);
        }
    }

    public void setTitle(int resString) {
        if (tvTitle != null) {
            if (resString <= 0) tvTitle.setText("");
            else tvTitle.setText(resString);
        }
    }

    public void setButtonAction(String title) {
        if (btnSubmit != null) {
            if (TextUtils.isEmpty(title)) {
                btnSubmit.setVisibility(GONE);
                btnSubmit.setText("");
            } else {
                btnSubmit.setVisibility(VISIBLE);
                btnSubmit.setText(title);
            }
        }
    }

    public void setButtonAction(int resString) {
        if (btnSubmit != null) {
            if (resString <= 0) {
                btnSubmit.setVisibility(GONE);
                btnSubmit.setText("");
            } else {
                btnSubmit.setVisibility(VISIBLE);
                btnSubmit.setText(resString);
            }
        }
    }

    public void setButtonActionListener(OnClickListener listener) {
        if (btnSubmit != null) btnSubmit.setOnClickListener(listener);
    }

    public View getConstraintView() {
        return constraintView;
    }
}

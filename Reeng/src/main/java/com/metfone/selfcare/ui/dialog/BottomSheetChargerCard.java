package com.metfone.selfcare.ui.dialog;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.encrypt.RSAEncrypt;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 3/12/2018.
 */

public class BottomSheetChargerCard extends BottomSheetDialog implements View.OnClickListener {

    private static final String TAG = BottomSheetChargerCard.class.getSimpleName();

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ChargerCardListener mCallBack;

    private RelativeLayout mViewCard;
    private TextView mTvwTitle;
    private TextView mTvwClose;
    private LinearLayout mViewSelectPayment;
    private RoundLinearLayout mViewPaymentVisa;
    private RoundLinearLayout mViewPaymentCardMobile;
    private EditText mEdtCardNumber, mEdtCardSeri;
    private RoundTextView mBtnCharger;
    private TextView mTvwPaymentCard;

    public BottomSheetChargerCard(@NonNull BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity);
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplicationContext();
        setCancelable(isCancelable);
    }

    public BottomSheetChargerCard setListener(ChargerCardListener listener) {
        this.mCallBack = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.bottom_sheet_charger_card);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        findComponentViews();
        setViewListener();
    }

    private void setViewListener() {
        mBtnCharger.setOnClickListener(this);
        mViewPaymentVisa.setOnClickListener(this);
        mViewPaymentCardMobile.setOnClickListener(this);
        mTvwClose.setOnClickListener(this);
    }

    private void findComponentViews() {
        mEdtCardNumber = findViewById(R.id.edt_card_number);
        mEdtCardSeri = findViewById(R.id.edt_card_seri);
        mBtnCharger = findViewById(R.id.btn_charger);

        mTvwPaymentCard = findViewById(R.id.tvPaymentCard);

        mViewCard = findViewById(R.id.layout_card);
        mViewSelectPayment = findViewById(R.id.view_select_payment);
        mViewPaymentVisa = findViewById(R.id.round_visa);
        mViewPaymentCardMobile = findViewById(R.id.round_card_mobile);
        mTvwTitle = findViewById(R.id.tvw_info_card_title);
        mTvwClose = findViewById(R.id.tvw_close_charger_card);

        if (mApplication.getReengAccountBusiness().isVietnam()) {
            mViewPaymentCardMobile.setVisibility(View.VISIBLE);
            mTvwPaymentCard.setText(mApplication.getResources().getString(R.string.title_avno_visa));
        } else {
            mTvwPaymentCard.setText(mApplication.getResources().getString(R.string.title_avno_visa_not_vn));
            mViewPaymentCardMobile.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_charger:
                onClickChargerButton();
                break;

            case R.id.round_visa:
                String url = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                        .AVNO_PAYMENT_WAPSITE);
                if (TextUtils.isEmpty(url)) {
                    activity.showToast(R.string.e601_error_but_undefined);
                }
                url = url + "?search=" + Uri.encode(RSAEncrypt.getInStance(mApplication).encrypt(mApplication, mApplication
                        .getReengAccountBusiness().getJidNumber()));
                Log.i(TAG, "URL: " + url);
                UrlConfigHelper.gotoWebViewFullScreenPayment(mApplication, activity, url);
                dismiss();
                break;
            case R.id.round_card_mobile:
                mViewSelectPayment.setVisibility(View.GONE);
                mViewCard.setVisibility(View.VISIBLE);
                mTvwTitle.setText(mApplication.getResources().getString(R.string.avno_card_info_title));
                break;
            case R.id.tvw_close_charger_card:
                dismiss();
                break;
            default:
                break;
        }
    }

    private void onClickChargerButton() {
        String cardNumber = mEdtCardNumber.getText().toString();
        String cardSeri = mEdtCardSeri.getText().toString();
        if (TextUtils.isEmpty(cardNumber) && TextUtils.isEmpty(cardSeri)) {
            activity.showToast(R.string.avno_toast_empty_card_info);
            setViewClickChangeBackgroundError(mEdtCardNumber);
            setViewClickChangeBackgroundError(mEdtCardSeri);
        } else if (TextUtils.isEmpty(cardNumber)) {
            activity.showToast(R.string.avno_toast_empty_card_number);
            setViewClickChangeBackgroundError(mEdtCardNumber);
        } else if (TextUtils.isEmpty(cardSeri)) {
            activity.showToast(R.string.avno_toast_empty_card_seri);
            setViewClickChangeBackgroundError(mEdtCardSeri);
        } else {
            if (mCallBack != null) {
                mCallBack.onClickBtnCharger(cardNumber, cardSeri);
            }
        }
    }

    private void setViewClickChangeBackgroundError(View view) {
        /*view.setBackgroundResource(R.drawable.xml_bg_input_avno_card_error);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackgroundResource(R.drawable.xml_bg_input_avno_card);
            }
        });*/
    }

    public interface ChargerCardListener {
        void onClickBtnCharger(String cardNumber, String cardSeri);
    }
}

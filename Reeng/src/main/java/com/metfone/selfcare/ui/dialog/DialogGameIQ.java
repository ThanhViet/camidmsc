package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.roundview.RoundTextView;

/**
 * Created by thanhnt72 on 10/25/2018.
 */

public class DialogGameIQ extends Dialog {
    private BaseSlidingFragmentActivity activity;
    private String msg;
    private DismissListener dismissListener;

    private RoundTextView mBtnWatch, mBtnPlayContinue;
    private TextView mTvwTitle, mTvwMessage;
    private View mViewDivider;
    private DialogGameIQListener listener;

    private boolean canUseHeart;
    private int timeRemainInSecond = 5;


    public DialogGameIQ(BaseSlidingFragmentActivity activity, boolean isCancelable, boolean canUseHeart) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        this.canUseHeart = canUseHeart;
        setCancelable(isCancelable);
    }

    public DialogGameIQ setMessage(String message) {
        this.msg = message;
        return this;
    }


    public DialogGameIQ setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    public DialogGameIQ setDialogGameIQListener(DialogGameIQListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_gameiq);
        findComponentViews();
        drawDetail();
        mBtnWatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClickWatch();
                }
                dismiss();
            }
        });

        mBtnPlayContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClickUseHeart();
                }
                dismiss();
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }


    private void findComponentViews() {
        mTvwTitle = (TextView) findViewById(R.id.dialog_confirm_label);
        mTvwMessage = (TextView) findViewById(R.id.dialog_confirm_message);
        mViewDivider = findViewById(R.id.dialog_confirm_divider);
        mBtnWatch = findViewById(R.id.tvwWatch);
        mBtnPlayContinue = findViewById(R.id.tvwPlayContinue);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            timeRemainInSecond--;
            setTextBtnPlay(timeRemainInSecond);
            if (timeRemainInSecond == 0) {
                mBtnPlayContinue.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                }, 200);
            } else {
                mBtnPlayContinue.postDelayed(runnable, 1000);
            }
        }
    };


    private void drawDetail() {
        mTvwTitle.setText(activity.getResources().getString(R.string.title_incorrect_game_iq));
        mTvwMessage.setText(msg);
        mBtnWatch.setText(activity.getResources().getString(R.string.button_incorrect_game_iq));
        mBtnPlayContinue.setVisibility(View.VISIBLE);
        setTextBtnPlay(timeRemainInSecond);
        if (canUseHeart)
            mBtnPlayContinue.postDelayed(runnable, 1000);

    }

    private void setTextBtnPlay(int timeRemain) {
        if (canUseHeart)
            mBtnPlayContinue.setText(String.format(activity.getResources().getString(R.string.button_play_continue_game_iq), timeRemain));
        else
            mBtnPlayContinue.setText(activity.getResources().getString(R.string.button_invite_game_iq));
    }

    public interface DialogGameIQListener {
        public void onClickWatch();

        public void onClickUseHeart();
    }
}


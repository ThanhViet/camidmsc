package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.ratingbar.SimpleRatingBar;

/**
 * Created by HaiKE on 3/14/18.
 */

public abstract class DialogRating extends Dialog implements View.OnClickListener {
    private WebView webview;
    private TextView btnCancel;
    private TextView btnDone;
    private TextView tvQuality;
    private SimpleRatingBar ratingBar;
    private Context context;
    private int ratingValue = 0;

    public DialogRating(Context context, int themeResId) {
        super(context, themeResId);
    }

    public DialogRating(Context context) {
        super(context, R.style.style_dialog2);
        setContentView(R.layout.dialog_rating);
        this.context = context;
        init();
    }

    public DialogRating(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
        init();
    }

    private void init() {
        ratingBar = findViewById(R.id.ratingBar);
        tvQuality = findViewById(R.id.tvQuality);
        btnDone = findViewById(R.id.btnDone);
        btnCancel = findViewById(R.id.btnCancel);
        btnDone.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        btnCancel.setText(context.getString(R.string.cancel).toUpperCase());
        btnDone.setText(context.getString(R.string.action_done).toUpperCase());

        btnDone.setAlpha(0.6f);
        btnDone.setEnabled(false);

        ratingBar.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                ratingValue = (int) ratingBar.getRating();
                if(ratingValue > 0)
                {
                    btnDone.setAlpha(1f);
                    btnDone.setEnabled(true);
                }
                else
                {
                    btnDone.setAlpha(0.6f);
                    btnDone.setEnabled(false);
                }

                if(ratingValue == 1)
                {
                    tvQuality.setText(context.getString(R.string.call_quality_vote_1));
                }
                else if(ratingValue == 2)
                {
                    tvQuality.setText(context.getString(R.string.call_quality_vote_2));
                }
                else if(ratingValue == 3)
                {
                    tvQuality.setText(context.getString(R.string.call_quality_vote_3));
                }
                else if(ratingValue == 4)
                {
                    tvQuality.setText(context.getString(R.string.call_quality_vote_4));
                }
                else if(ratingValue == 5)
                {
                    tvQuality.setText(context.getString(R.string.call_quality_vote_5));
                }
                else
                {
                    tvQuality.setText("");
                }
            }
        });

//        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
//                ratingValue = ratingBar.getProgress();
//                if(ratingValue > 0)
//                {
//                    btnDone.setAlpha(1f);
//                    btnDone.setEnabled(true);
//                }
//                else
//                {
//                    btnDone.setAlpha(0.6f);
//                    btnDone.setEnabled(false);
//                }
//
//                if(ratingValue == 1)
//                {
//                    tvQuality.setText(context.getString(R.string.call_quality_vote_1));
//                }
//                else if(ratingValue == 2)
//                {
//                    tvQuality.setText(context.getString(R.string.call_quality_vote_2));
//                }
//                else if(ratingValue == 3)
//                {
//                    tvQuality.setText(context.getString(R.string.call_quality_vote_3));
//                }
//                else if(ratingValue == 4)
//                {
//                    tvQuality.setText(context.getString(R.string.call_quality_vote_4));
//                }
//                else if(ratingValue == 5)
//                {
//                    tvQuality.setText(context.getString(R.string.call_quality_vote_5));
//                }
//                else
//                {
//                    tvQuality.setText("");
//                }
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                onCancelRating();
                dismiss();
                break;
            case R.id.btnDone:
                onSubmitRating(ratingValue);
                dismiss();
                break;
        }
    }

//    CountDownTimer mTimer = new CountDownTimer(15000, 1000) {
//        @Override
//        public void onTick(long millisUntilFinished) {
//
//        }
//
//        @Override
//        public void onFinish() {
//            onTimeOutRating();
//            dismiss();
//        }
//    };

//    @Override
//    public void show() {
//        super.show();
//        if(mTimer != null)
//            mTimer.start();
//    }
//
//    @Override
//    public void dismiss() {
//        if(mTimer != null)
//        {
//            mTimer.cancel();
//        }
//        super.dismiss();
//    }

    abstract public void onSubmitRating(int value);
    abstract public void onCancelRating();
//    abstract public void onTimeOutRating();

}
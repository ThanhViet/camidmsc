package com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallback;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.di.BasePresenter;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONException;

import java.util.ArrayList;

public class MovieDetailPresenterImpl extends BasePresenter<MovieDetailContact.MovieDetailView> implements MovieDetailContact.MovieDetailPresenter {

    private VolumeFilmGroupsCallback volumeFilmGroupsCallback;
    private Video currentVideo;
    private ArrayList<Video> videos;

    private boolean isCallApiError = false;

    public MovieDetailPresenterImpl(MovieDetailContact.MovieDetailView view, ApplicationController application) {
        super(view, application);
        volumeFilmGroupsCallback = new VolumeFilmGroupsCallback();
    }

    @Override
    public void setVideo(Video video) {
        video.setSave(videoApi.isSave(video));
        currentVideo = video;
    }

    @Override
    public void openDetail(BaseSlidingFragmentActivity activity, Channel channel) {
        utils.openChannelInfo(activity, channel);
    }

    @Override
    public void getVolumeFilmGroups() {
        if (currentVideo == null) return;
        videoApi.getVolumeFilmGroups(currentVideo.getFilmGroup().getGroupId(), volumeFilmGroupsCallback);
//        if (TextHelper.getInstant().isLinkMoviesDetail(currentVideo.getLink())) {
//            videoApi.getVolumeFilmGroups(currentVideo.getFilmGroup().getGroupId(), volumeFilmGroupsCallback);
//        } else {
//            videoApi.getVolumeFilmGroups(currentVideo.getFilmGroup().getGroupId(), volumeFilmGroupsCallback);
//        }
    }

    @Override
    public void getMoveInfo(Video video) {
        videoApi.getMovieInfo(video, new ApiCallbackV2<Video>() {
            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onSuccess(String lastId, Video video) throws JSONException {
                if (view != null)
                    view.updateUiVideo(video);
            }
        });
    }

    @Override
    public void like(BaseSlidingFragmentActivity activity, Video video) {
        videoApi.likeOrUnlikeVideo(video);
        listenerUtils.notifyVideoLikeChangedData(video);
    }

    @Override
    public void comment(BaseSlidingFragmentActivity activity, Video video) {
        utils.openCommentVideo(activity, video);
    }

    @Override
    public void save(Video video) {
        videoApi.addOrRemoveSaveVideo(video);
    }

    @Override
    public void sub(Channel channel) {
        channelApi.callApiSubOrUnsubChannel(channel.getId(), channel.isFollow());
        listenerUtils.notifyChannelSubscriptionsData(channel);
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        super.onChannelSubscribeChanged(channel);
        updateStateSubscription(currentVideo.getChannel(), channel);
        if (Utilities.notEmpty(videos))
            for (Video video : videos) {
                updateStateSubscription(video.getChannel(), channel);
            }

        if (view != null)
            view.updateUiChannel();
    }

    @Override
    protected void videoChangeData(Video newVideo, Type type) {
        super.videoChangeData(newVideo, type);
        updateData(currentVideo, newVideo, type);

        if (Utilities.notEmpty(videos))
            for (Video video : videos) {
                updateData(video, newVideo, type);
            }
        if (view != null)
            view.updateUiVideo();
    }

    @Override
    public void onInternetChanged() {
        super.onInternetChanged();
        if (isCallApiError && NetworkHelper.isConnectInternet(application) && Utilities.isEmpty(videos))
            getVolumeFilmGroups();
    }

    private void updateStateSubscription(Channel oldChannel, Channel newChannel) {
        if (oldChannel == null || Utilities.isEmpty(oldChannel.getId())) return;
        if (newChannel == null || Utilities.isEmpty(newChannel.getId())) return;
        if (!newChannel.getId().equals(oldChannel.getId())) return;

        oldChannel.setNumFollow(newChannel.getNumfollow());
        oldChannel.setFollow(newChannel.isFollow());

    }

    private void updateData(Video oldVideo, Video newVideo, Type type) {
        if (oldVideo == null || Utilities.isEmpty(oldVideo.getId())) return;
        if (newVideo == null || Utilities.isEmpty(newVideo.getId())) return;
        if (!newVideo.getId().equals(oldVideo.getId())) return;

        if (type == Type.LIKE) {//Cập nhập dữ liệu like
            oldVideo.setLike(newVideo.isLike());
            oldVideo.setTotalLike(newVideo.getTotalLike());
        } else if (type == Type.SHARE) {// Cập nhật dữ liệu comment
            oldVideo.setShare(newVideo.isShare());
            oldVideo.setTotalShare(newVideo.getTotalShare());
        } else if (type == Type.COMMENT) {// Cập nhật dữ liệu share
            oldVideo.setTotalComment(newVideo.getTotalComment());
        } else if (type == Type.SAVE) {// Cập nhật dữ liệu SAVE
            oldVideo.setSave(newVideo.isSave());
        }

    }

    public class VolumeFilmGroupsCallback implements ApiCallback {

        public void onSuccess(ArrayList<Video> results) {
            isCallApiError = false;
            if (videos == null)
                videos = new ArrayList<>();
            else
                videos.clear();

            ArrayList<Video> datas = new ArrayList<>();
            if (Utilities.notEmpty(results))
                for (Video result : results) {
                    if (Utilities.notEmpty(result.getChapter())) {
                        result.setSave(videoApi.isSave(result));
                        result.setFilmGroup(currentVideo.getFilmGroup());
                        result.setChannel(currentVideo.getChannel());
                        datas.add(result);
                    }
                }

            videos.addAll(datas);

            if (view != null)
                view.bindData(videos, false);
        }

        @Override
        public void onError(String s) {
            isCallApiError = true;
        }

        @Override
        public void onComplete() {

        }
    }

}

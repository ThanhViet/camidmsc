package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.youtubes;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.model.tab_video.Category;
import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;
import java.util.List;

public class UploadVideoYoutubePresenterImpl implements UploadVideoYoutubePresenter, OnVideoCallback {

    private ApplicationController application;
    private UploadVideoYoutubeView uploadVideoYoutubeView;

    private Utils utils;
    private VideoApi videoApi;
    private boolean isUploadOnMedia;

    public UploadVideoYoutubePresenterImpl(ApplicationController application, BaseView baseView) {
        this.application = application;
        this.uploadVideoYoutubeView = (UploadVideoYoutubeView) baseView;

        this.utils = application.getApplicationComponent().providesUtils();
        this.videoApi = application.getApplicationComponent().providerVideoApi();
    }

    @Override
    public void getCategories() {
        ArrayList<Category> categories = utils.getCategories();
        List<String> list = new ArrayList<>();
        list.add(application.getString(R.string.selectCategory));
        for (Category category : categories) {
            String name = category.getName().toLowerCase();
            String first = name.substring(0, 1);
            name = first.toUpperCase() + name.substring(1);
            list.add(name);
        }
        uploadVideoYoutubeView.updateDataCategories(list);
    }

    @Override
    public void uploadVideo(String link, boolean isUploadOnMedia, Object item) {
        this.isUploadOnMedia = isUploadOnMedia;
        link = link.trim();
        if (TextUtils.isEmpty(link)) {
            uploadVideoYoutubeView.showMessage(R.string.urlVideoNotEmpty);
            return;
        }

        if (!TextHelper.getInstant().isYoutubeUrl(link.trim())) {
            uploadVideoYoutubeView.showMessage(R.string.video_warning_url);
            return;
        }
        String categoryId = "";
        ArrayList<Category> categories = utils.getCategories();
        for (Category category : categories) {
            if (category.getName().toLowerCase().equals(((String) item).toLowerCase())) {
                categoryId = category.getId();
                break;
            }
        }
        if (categoryId.isEmpty()) {
            uploadVideoYoutubeView.showMessageCategory();
            return;
        }

        uploadVideoYoutubeView.showDialogLoading();
        videoApi.uploadVideoYoutube(link, categoryId, isUploadOnMedia,this);
    }

    @Override
    public void onGetVideosSuccess(ArrayList<Video> list) {
        if (uploadVideoYoutubeView == null) return;
        uploadVideoYoutubeView.hideDialogLoading();
        uploadVideoYoutubeView.showSuccess();
//        uploadVideoYoutubeView.showMessage(R.string.upload_video_success);
//        uploadVideoYoutubeView.finish();
    }

    @Override
    public void onGetVideosError(String s) {
        uploadVideoYoutubeView.hideDialogLoading();
        uploadVideoYoutubeView.showMessage(s);
    }

    @Override
    public void onGetVideosComplete() {
    }
}

package com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.adapter;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.MochaAdsClient;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageLoader;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.model.tab_video.BannerVideo;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.model.VideoObject;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.utils.TypeUtils;
import com.metfone.selfcare.ui.view.LinkTextView;
import com.metfone.selfcare.ui.view.tab_video.SubscribeChannelLayout;
import com.metfone.selfcare.util.Utilities;
import com.vtm.adslib.template.TemplateView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoDetailAdapter extends BaseAdapter {

    private final LruCache<Integer, Bitmap> lruCache;
    private boolean isAdsConform = false;
    private boolean isAdsCompleted = false;
    private boolean isAdsShow = false;
    private String avatarPath;

    public VideoDetailAdapter(BaseSlidingFragmentActivity activity) {
        super(activity);
        this.lruCache = new LruCache<>(1024 * 1024);
        avatarPath = ((ApplicationController) activity.getApplication()).getReengAccountBusiness().getCurrentAccount().getAvatarPath();
        Resources resources = activity.getResources();

        lruCache.put(R.drawable.ic_video_item_video_hear_press, BitmapFactory.decodeResource(resources, R.drawable.ic_video_item_video_hear_press));
        lruCache.put(R.drawable.ic_favior__new, BitmapFactory.decodeResource(resources, R.drawable.ic_favior__new));
        lruCache.put(R.drawable.ic_tab_video_save_press, BitmapFactory.decodeResource(resources, R.drawable.ic_tab_video_save_press));
        lruCache.put(R.drawable.ic_tab_video_save, BitmapFactory.decodeResource(resources, R.drawable.ic_tab_video_save));
        lruCache.put(R.drawable.error, BitmapFactory.decodeResource(resources, R.drawable.error));
        lruCache.put(R.drawable.error_2, BitmapFactory.decodeResource(resources, R.drawable.error_2));
        lruCache.put(R.drawable.error_3, BitmapFactory.decodeResource(resources, R.drawable.error_3));
        lruCache.put(R.drawable.error_4, BitmapFactory.decodeResource(resources, R.drawable.error_4));
        lruCache.put(R.drawable.error_5, BitmapFactory.decodeResource(resources, R.drawable.error_5));
        lruCache.put(R.drawable.error_6, BitmapFactory.decodeResource(resources, R.drawable.error_6));
        lruCache.put(R.drawable.error_7, BitmapFactory.decodeResource(resources, R.drawable.error_7));
        lruCache.put(R.drawable.error_8, BitmapFactory.decodeResource(resources, R.drawable.error_8));
        lruCache.put(R.drawable.error_9, BitmapFactory.decodeResource(resources, R.drawable.error_9));
        lruCache.put(R.drawable.error_10, BitmapFactory.decodeResource(resources, R.drawable.error_10));
        lruCache.put(R.drawable.error_11, BitmapFactory.decodeResource(resources, R.drawable.error_11));

    }

    public boolean isAdsConform() {
        return isAdsConform;
    }

    public void setAdsConform(boolean adsConform) {
        isAdsConform = adsConform;
    }

    public boolean isAdsShow() {
        return isAdsShow;
    }

    public void setAdsShow(boolean adsShow) {
        isAdsShow = adsShow;
    }

    @Override
    public int getItemViewType(int position) {
        /*
         * set type cho item video
         */
        Clone info = itemObjects.get(position).getInfo();
        if (info instanceof VideoObject) {
            VideoObject videoObject = (VideoObject) info;
            return TypeUtils.getInstance().getType(videoObject.getVideo().getAspectRatio());
        }
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case LOAD_MORE:
                view = activity.getLayoutInflater().inflate(R.layout.layout_video_detail_loading, parent, false);
                return new LoadMoreHolder(view);
            case END:
                view = activity.getLayoutInflater().inflate(R.layout.layout_video_detail_loading, parent, false);
                return new EndHolder(view);
            default:
                view = activity.getLayoutInflater().inflate(R.layout.item_video_detail, parent, false);
                return new VideoHolder(view, viewType);
        }
    }

    @Override
    protected Object getChangePayload(ItemObject oldItem, ItemObject newItem) {
        Bundle bundle = new Bundle();

        Object oldObject = oldItem.getInfo();
        Object newObject = newItem.getInfo();

        if (oldObject instanceof VideoObject && newObject instanceof VideoObject) {
            VideoObject oldVideoObject = (VideoObject) oldObject;
            VideoObject newVideoObject = (VideoObject) newObject;
            if (!oldVideoObject.getTextLike().equals(newVideoObject.getTextLike()))
                bundle.putBoolean(Constants.TabVideo.LIKE, true);
            if (!oldVideoObject.getTextComment().equals(newVideoObject.getTextComment()))
                bundle.putBoolean(Constants.TabVideo.COMMENT, true);
            if (!oldVideoObject.getTextShare().equals(newVideoObject.getTextShare()))
                bundle.putBoolean(Constants.TabVideo.SHARE, true);
            if (!oldVideoObject.getTextSubscription().equals(newVideoObject.getTextSubscription()))
                bundle.putBoolean(Constants.TabVideo.SUBSCRIPTION, true);
            if (oldVideoObject.getVideo().isSave() != newVideoObject.getVideo().isSave())
                bundle.putBoolean(Constants.TabVideo.SAVE, true);
            if (!oldVideoObject.getVideo().isPlaying() == newVideoObject.getVideo().isPlaying())
                bundle.putBoolean(Constants.TabVideo.PLAY, true);
        }
        if (bundle.isEmpty()) return null;
        return bundle;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemObject item = itemObjects.get(position);
        if (holder instanceof VideoHolder && item.getInfo() instanceof VideoObject) {
            VideoHolder videoHolder = (VideoHolder) holder;
            videoHolder.bindData((VideoObject) item.getInfo(), position);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) super.onBindViewHolder(holder, position, payloads);
        else {
            Bundle bundle = (Bundle) payloads.get(0);
            if (holder instanceof VideoHolder) {
                VideoHolder videoHolder = (VideoHolder) holder;
                if (bundle.getBoolean(Constants.TabVideo.LIKE, false))
                    videoHolder.updateUiLike();
                if (bundle.getBoolean(Constants.TabVideo.SHARE, false))
                    videoHolder.updateUiShare();
                if (bundle.getBoolean(Constants.TabVideo.COMMENT, false))
                    videoHolder.updateUiComment();
                if (bundle.getBoolean(Constants.TabVideo.SAVE, false))
                    videoHolder.updateUiSave();
                if (bundle.getBoolean(Constants.TabVideo.SUBSCRIPTION, false))
                    videoHolder.updateUiSubscription();
                if (bundle.getBoolean(Constants.TabVideo.PLAY, false))
                    videoHolder.updateUiHide();
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (lruCache != null) {
            Map<Integer, Bitmap> snapshot = lruCache.snapshot();
            for (Integer id : snapshot.keySet()) {
                Bitmap bitmap = lruCache.get(id);
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
            lruCache.evictAll();
        }
    }

    public interface OnItemVideoClickListener extends OnItemListener {

        void onItemClicked(VideoHolder video);

        void onBtnLikeClicked(Video video);

        void onBtnCommentClicked(Video video);

        void onBtnShareClicked(Video video);

        void onBtnSaveClicked(Video video);

        void onBtnSubscriptionClicked(Channel channel);

        void onChannelInfoClicked(Channel channel);

    }

    class FooterHolder extends ViewHolder {
        View rootFooter;

        FooterHolder(View itemView) {
            super(itemView);
            rootFooter = itemView.findViewById(R.id.videoShimmerFrameLayout);
        }
    }

    class LoadMoreHolder extends FooterHolder {

        LoadMoreHolder(View itemView) {
            super(itemView);
        }
    }

    class EndHolder extends FooterHolder {

        EndHolder(View itemView) {
            super(itemView);
            rootFooter.setVisibility(View.INVISIBLE);
        }
    }

    @SuppressLint("UseSparseArrays")
    public class VideoHolder extends ViewHolder implements SubscribeChannelLayout.SubscribeChannelListener, LinkTextView.OnReadMoreListener, LinkTextView.OnLinkListener {

        @BindView(R.id.space)
        Space space;
        @BindView(R.id.tvSubscriptionsChannel)
        TextView tvSubscriptionsChannel;
        @BindView(R.id.ll_subscription)
        FrameLayout llSubscription;
        @BindView(R.id.ivChannel)
        RoundedImageView ivChannel;
        @BindView(R.id.tvChannelName)
        TextView tvChannelName;
        @BindView(R.id.tvNumberSubscriptionsChannel)
        TextView tvNumberSubscriptionsChannel;
        @BindView(R.id.btn_subscribe_channel)
        SubscribeChannelLayout btnSubscribeChannel;
        @BindView(R.id.reChannelInfo)
        RelativeLayout reChannelInfo;
        @BindView(R.id.ivVideo)
        ImageView ivVideo;
        @BindView(R.id.frVideo)
        FrameLayout frVideo;
        @BindView(R.id.frController)
        FrameLayout frController;
        @BindView(R.id.iv_background)
        ImageView ivBackground;
        @BindView(R.id.iv_icon_banner)
        ImageView ivIconBanner;
        @BindView(R.id.tv_content_banner)
        TextView tvContentBanner;
        @BindView(R.id.btn_banner)
        RoundTextView btnBanner;
        @BindView(R.id.frame_banner)
        LinearLayout frameBanner;
        @BindView(R.id.iv_icon_banner_conform)
        ImageView ivIconBannerConform;
        @BindView(R.id.tv_content_banner_conform)
        TextView tvContentBannerConform;
        @BindView(R.id.tv_btn_banner_conform)
        RoundTextView tvBtnBannerConform;
        @BindView(R.id.frame_banner_conform)
        LinearLayout frameBannerConform;
        @BindView(R.id.root_frame_banner)
        RelativeLayout rootFrameBanner;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.llDescription)
        LinearLayout llDescription;
        @BindView(R.id.tv_total_view)
        TextView tvTotalView;
        @BindView(R.id.tvDescription)
        LinkTextView tvDescription;
        @BindView(R.id.tv_datetime)
        TextView tvDatetime;
        @BindView(R.id.tvNumberVideos)
        TextView tvNumberVideos;
        @BindView(R.id.ivHear)
        AppCompatImageView ivHear;
        @BindView(R.id.tvNumberHear)
        TextView tvNumberHear;
        @BindView(R.id.llControllerHear)
        LinearLayout llControllerHear;
        @BindView(R.id.ivComment)
        AppCompatImageView ivComment;
        @BindView(R.id.tvNumberComment)
        TextView tvNumberComment;
        @BindView(R.id.llControllerComment)
        LinearLayout llControllerComment;
        @BindView(R.id.ivShare)
        AppCompatImageView ivShare;
        @BindView(R.id.tvNumberShare)
        TextView tvNumberShare;
        @BindView(R.id.llControllerShare)
        LinearLayout llControllerShare;
        @BindView(R.id.ivSave)
        ImageView ivSave;
        @BindView(R.id.iv_user_avatar)
        CircleImageView ivUserAvatar;
        @BindView(R.id.root_comment)
        LinearLayout rootComment;
        @BindView(R.id.vLine)
        View vLine;
        @BindView(R.id.main)
        LinearLayout main;
        @BindView(R.id.hide)
        View hide;
        @BindView(R.id.dark)
        View dark;
        @BindView(R.id.layout_ads)
        TemplateView layout_ads;

        private VideoObject videoObject;
        private Channel channel;
        private Video video;

        private ImageLoader videoLoader;
        private ImageLoader channelLoader;
        private int viewType;
        private BannerVideo mBannerVideo;
        private MochaAdsClient.MochaAdsEvent.OnMochaAdsListener mochaAdsListener = new MochaAdsClient.MochaAdsEvent.OnMochaAdsListener() {
            @Override
            public void onHideAdsBy(String adsId) {
                if (rootFrameBanner == null) return;
                rootFrameBanner.setVisibility(View.GONE);
            }
        };
        private View.OnAttachStateChangeListener onAttachStateChangeListener = new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                MochaAdsClient.MochaAdsEvent.getInstance().addListener(mochaAdsListener);
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                MochaAdsClient.MochaAdsEvent.getInstance().removeListener(mochaAdsListener);
            }
        };

        public VideoHolder(View itemView, int viewType) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.viewType = viewType;
            Utilities.setSizeFrameVideo(activity, frController, TypeUtils.getInstance().getAspectRatio(viewType));
            btnSubscribeChannel.setVisibility(View.GONE);
            tvSubscriptionsChannel.setVisibility(View.GONE);

            btnSubscribeChannel.setSubscribeChannelListener(this);
            llControllerComment.setOnClickListener(this);
            tvBtnBannerConform.setOnClickListener(this);
            llControllerShare.setOnClickListener(this);
            llControllerHear.setOnClickListener(this);
            reChannelInfo.setOnClickListener(this);
            rootComment.setOnClickListener(this);
            btnBanner.setOnClickListener(this);
            ivSave.setOnClickListener(this);
            hide.setOnClickListener(this);

            itemView.addOnAttachStateChangeListener(onAttachStateChangeListener);

            tvDescription.setOnLinkListener(this);
            tvDescription.setOnClickListener(this);
            tvDescription.setOnReadMoreListener(this);
        }

        void bindData(VideoObject videoObject, int position) {
            this.videoObject = videoObject;
            this.video = videoObject.getVideo();
            this.channel = video.getChannel();

            this.videoLoader = videoObject.getVideoLoader();
            this.channelLoader = videoObject.getChannelLoader();

            /*if (video.getIsPrivate() == 1) {
                ivSave.setVisibility(View.GONE);
                llControllerHear.setVisibility(View.GONE);
                llControllerShare.setVisibility(View.GONE);
                llControllerComment.setVisibility(View.GONE);
            } else {*/
            ivSave.setVisibility(View.VISIBLE);
            llControllerHear.setVisibility(View.VISIBLE);
            llControllerShare.setVisibility(View.VISIBLE);
            llControllerComment.setVisibility(View.VISIBLE);
//            }

            hideAds();

            HashMap<Integer, Object> map = new HashMap<>();
            map.put(0, video);
            map.put(1, position);
            frVideo.setTag(map);

            if (position == 0)
                space.setVisibility(View.VISIBLE);
            else
                space.setVisibility(View.GONE);

            if (TextUtils.isEmpty(video.getTitle()))
                tvTitle.setVisibility(View.GONE);
            else {
                tvTitle.setText(video.getTitle());
                tvTitle.setVisibility(View.VISIBLE);
            }
            tvDescription.setColorReadMore(R.color.v5_text_5);
            tvDescription.asyncSetText(video.getDescription(), videoObject.isCollapse());

            if (channel.getNumVideo() > 0) {
                if (tvNumberVideos != null) {
                    tvNumberVideos.setVisibility(View.VISIBLE);
                    if (channel.getNumVideo() == 1)
                        tvNumberVideos.setText(activity.getString(R.string.music_total_video, channel.getNumVideo()));
                    else
                        tvNumberVideos.setText(activity.getString(R.string.music_total_videos, channel.getNumVideo()));
                }
            } else {
                if (tvNumberVideos != null) tvNumberVideos.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(channel.getName()))
                tvChannelName.setText(channel.getName());

            boolean showDot = false;
            if (tvTotalView != null) {
                if (video.getTotalView() <= 0)
                    tvTotalView.setVisibility(View.GONE);
                else {
                    tvTotalView.setVisibility(View.VISIBLE);
                    tvTotalView.setText(videoObject.getTextView());
                    showDot = true;
                }
            }
            if (tvDatetime != null) {
                if (video.getPublishTime() > 0) {
                    tvDatetime.setVisibility(View.VISIBLE);
                    tvDatetime.setText(DateTimeUtils.calculateTime(activity.getResources(), video.getPublishTime()));
                    tvDatetime.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? activity.getResources().getDrawable(R.drawable.ic_v5_dos_video) : null, null, null, null);
                } else {
                    tvDatetime.setVisibility(View.GONE);
                }
            }
            if (TextUtils.isEmpty(avatarPath)) {
                ivUserAvatar.setVisibility(View.GONE);
            } else {
                ivUserAvatar.setVisibility(View.VISIBLE);
                ImageManager.showImageLocal(avatarPath, ivUserAvatar);
            }
            updateUi();

            bindAds(position);
        }

        private void bindAds(int pos) {
            long adsDisplayCount = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_DISPLAY_COUNT);
            if (layout_ads != null && (pos == 0 || (adsDisplayCount > 0 && pos % adsDisplayCount == 0))) {
//                AdsManager.getInstance().showAdsBanner(layout_ads, new AdsListener() {
//                    @Override
//                    public void onAdClosed() {
//
//                    }
//
//                    @Override
//                    public void onAdShow() {
//                        if (rootFrameBanner != null)
//                            rootFrameBanner.setVisibility(View.GONE);
//                    }
//                });

                AdsManager.getInstance().showAdsNative(layout_ads);
            }
        }

        void updateUi() {
            videoObject.setUpdate(false);
            btnSubscribeChannel.setChannel(channel);

            updateUiHide();
            updateUiSubscription();
            updateUiComment();
            updateUiShare();
            updateUiImage();
            updateUiLike();
            updateUiSave();

        }

        private void updateUiSubscription() {
            if (channel.getNumfollow() <= 0)
                tvNumberSubscriptionsChannel.setVisibility(View.GONE);
            else {
                tvNumberSubscriptionsChannel.setVisibility(View.VISIBLE);
                tvNumberSubscriptionsChannel.setText(videoObject.getTextSubscription());
            }
        }

        private void updateUiComment() {
            tvNumberComment.setText(videoObject.getTextComment());
        }

        private void updateUiShare() {
            tvNumberShare.setText(videoObject.getTextShare());
        }

        private void updateUiImage() {
//            if (pause) {
//                if (!videoLoader.isAdsCompleted()) {
//                    showImageResource(ivVideo, videoObject.getThumbnail());
//                } else {
            videoLoader.into(ivVideo);
//                }
//                if (TextUtils.isEmpty(channel.getId()))
//                    showImageResource(ivChannel, R.mipmap.ic_launcher);
//                else {
//                    if (!channelLoader.isAdsCompleted()) {
//                        showImageResource(ivChannel, videoObject.getThumbnail());
//                    } else {
            channelLoader.into(ivChannel);
//                    }
//                }
//            } else {
//                videoLoader.into(ivVideo);
//                if (TextUtils.isEmpty(channel.getId()))
//                    showImageResource(ivChannel, R.mipmap.ic_launcher);
//                else
//                    channelLoader.into(ivChannel);
//            }
        }

        private void updateUiSave() {
            if (video == null || channel == null || TextUtils.isEmpty(channel.getId())) {//video.getIsPrivate() == 1 ||
                ivSave.setVisibility(View.GONE);
                return;
            } else
//                ivSave.setVisibility(View.VISIBLE);
                ivSave.setVisibility(View.GONE);

            if (lruCache != null && video != null)
                if (video.isSave())
                    showImageResource(ivSave, R.drawable.ic_tab_video_save_press);
                else
                    showImageResource(ivSave, R.drawable.ic_tab_video_save);
        }

        private void updateUiLike() {
            tvNumberHear.setText(videoObject.getTextLike());
            if (lruCache != null && video != null)
                if (video.isLike())
                    showImageResource(ivHear, R.drawable.ic_v5_like_active_video);
                else
                    showImageResource(ivHear, R.drawable.ic_v5_like_normal_video);
        }

        /**
         * tối màn hình item
         */
        public void updateUiHide() {
            if (video != null)
                if (video.isPlaying())
                    hide.setVisibility(View.INVISIBLE);
                else
                    hide.setVisibility(View.VISIBLE);
        }

        /**
         * cập nhật ảnh video
         */
        public void updateUiImageVideo() {
//            if (videoLoader != null)
//                videoLoader.into(ivVideo);
        }

        /**
         * cập nhật ảnh channel
         */
        public void updateUiImageChannel() {
//            if (channelLoader != null && channel != null)
//                if (TextUtils.isEmpty(channel.getId()))
//                    showImageResource(ivChannel, R.mipmap.ic_launcher);
//                else
//                    channelLoader.into(ivChannel);
        }

        /**
         * cung cấp frame video
         *
         * @return frame
         */
        public ViewGroup provideVideoFrame() {
            return frVideo;
        }

        /**
         * hiển thị hình ảnh của icon
         *
         * @param imageView imageView
         * @param resource  resource
         */
        private void showImageResource(ImageView imageView, @DrawableRes int resource) {
            if (lruCache == null) return;
            Bitmap bitmap = lruCache.get(resource);
            if (bitmap != null)
                imageView.setImageBitmap(bitmap);
            else
                imageView.setImageResource(resource);
        }

        @Override
        public void onClick(View view) {
            if (onItemListener == null || video == null || channel == null || videoObject == null)
                return;
            switch (view.getId()) {
                case R.id.hide:
                    if (onItemListener instanceof OnItemVideoClickListener)
                        ((OnItemVideoClickListener) onItemListener).onItemClicked(this);
                    break;
                case R.id.ivSave:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        video.setSave(!video.isSave());
                        updateUiSave();
                        if (onItemListener instanceof OnItemVideoClickListener)
                            ((OnItemVideoClickListener) onItemListener).onBtnSaveClicked(video);
                    }
                    break;
                case R.id.tvDescription:
                    if (videoObject == null || tvDescription == null || video == null) return;
                    videoObject.setCollapse(false);
                    tvDescription.asyncSetText(video.getDescription(), videoObject.isCollapse());
                    break;
                case R.id.reChannelInfo:
                    if (onItemListener instanceof OnItemVideoClickListener)
                        ((OnItemVideoClickListener) onItemListener).onChannelInfoClicked(channel);
                    break;
                case R.id.llControllerHear:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        video.setLike(!video.isLike());
                        video.setTotalLike(video.isLike() ? video.getTotalLike() + 1 : video.getTotalLike() - 1);
                        updateUiLike();
                        if (onItemListener instanceof OnItemVideoClickListener)
                            ((OnItemVideoClickListener) onItemListener).onBtnLikeClicked(video);
                    }
                    break;
                case R.id.root_comment:
                    if (onItemListener instanceof OnItemVideoClickListener)
                        ((OnItemVideoClickListener) onItemListener).onBtnCommentClicked(video);
                    break;
                case R.id.llControllerComment:
                    if (onItemListener instanceof OnItemVideoClickListener)
                        ((OnItemVideoClickListener) onItemListener).onBtnCommentClicked(video);
                    break;
                case R.id.llControllerShare:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        if (onItemListener instanceof OnItemVideoClickListener)
                            ((OnItemVideoClickListener) onItemListener).onBtnShareClicked(video);
                    }
                    break;
                case R.id.btn_banner:
                    isAdsConform = true;
                    showBannerConfig();
                    frameBanner.setVisibility(View.GONE);
                    frameBannerConform.setVisibility(View.VISIBLE);
                    break;
                case R.id.tv_btn_banner_conform:
                    isAdsCompleted = true;
                    MochaAdsClient.MochaAdsEvent.getInstance().notifyHideAdsBy("");
                    ReportHelper.checkShowConfirmOrRequestFakeMo((ApplicationController) activity.getApplication(), activity, null, mBannerVideo.getActFakeMOInline().getCommand(), "banner_video");
                    rootFrameBanner.setVisibility(View.GONE);
                    break;
            }
        }

        @Override
        public void onLink(String content, int type) {
            if (videoObject == null || tvDescription == null || video == null) return;
            if (type == Constants.SMART_TEXT.TYPE_MOCHA) {
                DeepLinkHelper.getInstance().openSchemaLink(activity, content);
            } else if (type == Constants.SMART_TEXT.TYPE_URL) {
                Utilities.openLink(activity, content);
            }
        }

        @Override
        public void onReadMore() {
            if (videoObject == null || tvDescription == null || video == null) return;
            videoObject.setCollapse(false);
            tvDescription.asyncSetText(video.getDescription(), videoObject.isCollapse());
        }

        @Override
        public void onOpenApp(Channel channel, boolean isInstall) {
            if (channel == null || !channel.equals(this.channel)) return;
            Utilities.openApp(activity, channel.getPackageAndroid());
        }

        @Override
        public void onSub(Channel channel) {
            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                activity.showDialogLogin();
            else {
                if (channel == null || !channel.equals(this.channel)) return;
                subChannel(channel);
            }
        }

        private void subChannel(Channel channel) {
            updateUiSubscription();
            if (onItemListener instanceof OnItemVideoClickListener)
                ((OnItemVideoClickListener) onItemListener).onBtnSubscriptionClicked(channel);
        }

        public void showAds(BannerVideo bannerVideo) {
            if (isAdsShow || isAdsCompleted || rootFrameBanner == null || rootFrameBanner.getVisibility() == View.VISIBLE || bannerVideo == null)
                return;

            isAdsShow = true;
            mBannerVideo = bannerVideo;
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) rootFrameBanner.getLayoutParams();
            layoutParams.width = ScreenManager.getWidth(activity);
            layoutParams.height = layoutParams.width * 180 / 1080;
            rootFrameBanner.setLayoutParams(layoutParams);

            if (Utilities.notEmpty(bannerVideo.getDisplay().getBackground())) {
                ivBackground.setVisibility(View.VISIBLE);
                ImageManager.showImage(bannerVideo.getDisplay().getBackground(), ivBackground);
            } else {
                ivBackground.setVisibility(View.GONE);
            }
            showBanner();

            rootFrameBanner.setVisibility(View.VISIBLE);
        }

        private void showBanner() {
            if (Utilities.notEmpty(mBannerVideo.getDisplay().getContent())) {
                dark.setVisibility(View.VISIBLE);
                tvContentBanner.setVisibility(View.VISIBLE);
                tvContentBanner.setText(mBannerVideo.getDisplay().getContent());
            } else {
                dark.setVisibility(View.GONE);
                tvContentBanner.setVisibility(View.GONE);
            }
            if (Utilities.notEmpty(mBannerVideo.getDisplay().getIcon())) {
                ivIconBanner.setVisibility(View.VISIBLE);
                ImageManager.showImageNotCenterCrop(mBannerVideo.getDisplay().getIcon(), ivIconBanner);
            } else {
                ivIconBanner.setVisibility(View.GONE);
            }
            btnBanner.setText(mBannerVideo.getDisplay().getLabelBtn());
        }

        private void showBannerConfig() {
            if (mBannerVideo.getActFakeMOInline() != null) {
                if (Utilities.notEmpty(mBannerVideo.getActFakeMOInline().getContentConfirm())) {
                    dark.setVisibility(View.VISIBLE);
                    tvContentBannerConform.setVisibility(View.VISIBLE);
//                    tvContentBannerConform.setText(mBannerVideo.getDisplay().getContent());//Text hien theo Fake MO
                    tvContentBannerConform.setText(mBannerVideo.getActFakeMOInline().getContentConfirm());//Text hien theo Fake MO
                } else {
                    dark.setVisibility(View.GONE);
                    tvContentBannerConform.setVisibility(View.GONE);
                }
                if (Utilities.notEmpty(mBannerVideo.getActFakeMOInline().getIconConfirm())) {
                    ivIconBannerConform.setVisibility(View.VISIBLE);
                    ImageManager.showImageNotCenterCrop(mBannerVideo.getActFakeMOInline().getIconConfirm(), ivIconBannerConform);
                } else {
                    ivIconBannerConform.setVisibility(View.GONE);
                }
                tvBtnBannerConform.setText(mBannerVideo.getActFakeMOInline().getLabelConfirm());
            } else if (mBannerVideo.getActSMS() != null) {
                rootFrameBanner.setVisibility(View.GONE);
                MochaAdsClient.MochaAdsEvent.getInstance().notifyHideAdsBy("");
                NavigateActivityHelper.openAppSMS(activity, mBannerVideo.getActSMS().getSmsCodes(), mBannerVideo.getActSMS().getSmsCommand());
            }
        }

        public void hideAds() {
            if (rootFrameBanner != null)
                rootFrameBanner.setVisibility(View.GONE);
        }

        public FrameLayout getControllerFrame() {
            return frController;
        }

    }

}

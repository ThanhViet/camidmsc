package com.metfone.selfcare.ui.tabvideo.channelDetail.statistical;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.channel.ChannelApi;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.ChannelRevenue;
import com.metfone.selfcare.model.tab_video.ChannelStatistical;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.tabvideo.adapter.RevenueAdapter;
import com.metfone.selfcare.ui.tabvideo.channelDetail.ChannelDetailActivity;
import com.metfone.selfcare.ui.tabvideo.fragment.BaseViewStubFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class StatisticalChannelFragment extends BaseViewStubFragment implements SwipeRefreshLayout.OnRefreshListener,
        AdapterView.OnItemSelectedListener,
        ApiCallbackV2<ChannelStatistical> {

    private static final String CHANNEL = "channel";

    @BindView(R.id.tv_total_revenue_now)
    TextView tvTotalRevenueNow;
    @BindView(R.id.ll_total_revenue_now)
    LinearLayout llTotalRevenueNow;
    @BindView(R.id.sp_filter_estimated_revenue)
    Spinner spFilterEstimatedRevenue;
    @BindView(R.id.tv_number_spoint)
    TextView tvNumberSpoint;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rec_revenue_details)
    RecyclerView recRevenueDetails;
    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_see_all)
    TextView tvSeeAll;
    @BindView(R.id.ll_header_evenue_details)
    LinearLayout llHeaderEvenueDetails;
    @BindView(R.id.ll_info_evenue_details)
    LinearLayout llInfoEvenueDetails;
    Unbinder unbinder;

    public static StatisticalChannelFragment newInstance(Channel channel) {
        Bundle args = new Bundle();
        args.putSerializable(CHANNEL, channel);
        StatisticalChannelFragment fragment = new StatisticalChannelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private @Nullable
    ChannelApi mChannelApi;
    private @Nullable
    Channel mChannel;
    private @Nullable
    ChannelStatistical mChannelStatistical;

    private RevenueAdapter revenueAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mChannel = (Channel) (bundle != null ? bundle.getSerializable(CHANNEL) : null);
        mChannelApi = application.getApplicationComponent().providerChannelApi();
    }

    @Override
    protected void onCreateViewAfterViewStubInflated(View inflatedView, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, inflatedView);
        initView();
    }

    @Override
    protected int getViewStubLayoutResource() {
        return R.layout.fragment_channel_statistical;
    }

    @Override
    public void onDestroyView() {
        if (refreshLayout != null)
            refreshLayout.setOnRefreshListener(null);
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        initView();
    }

    @OnClick(R.id.tv_see_all)
    public void onTvSeeAllClicked() {
        ((ChannelDetailActivity) activity).addStatisticalDetail();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        bindEstimatedRevenue();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onRefresh() {
        if (refreshLayout != null)
            refreshLayout.setRefreshing(false);
    }

    @Override
    public void onSuccess(String lastId, ChannelStatistical channelStatistical) {
        if (channelStatistical == null || recRevenueDetails == null) return;
        mChannelStatistical = channelStatistical;
        tvTotalRevenueNow.setText(Html.fromHtml(provideStr(R.string.channel_detail_total_revenue_now, channelStatistical.getTotalPoint())));

        List<String> list = new ArrayList<>();
        for (ChannelRevenue channelRevenue : channelStatistical.getChannelRevenue()) {
            list.add(channelRevenue.getTitle());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(activity, R.layout.custom_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFilterEstimatedRevenue.setAdapter(dataAdapter);
        bindEstimatedRevenue();
        spFilterEstimatedRevenue.setSelection(Math.min(2, channelStatistical.getChannelRevenue().size() - 1));

        ArrayList<Object> items = new ArrayList<>();
        items.add(new Object());
        items.addAll(channelStatistical.getVideoRevenue());
        revenueAdapter.bindData(items);

        llContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String s) {
        showError();
        if (!NetworkHelper.isConnectInternet(activity)) {
            showErrorNetwork();
            hideErrorDataEmpty();
        } else {
            showErrorDataEmpty();
            hideErrorNetwork();
        }
    }

    @Override
    public void onComplete() {
        if (refreshLayout == null) return;
        refreshLayout.setRefreshing(false);
        if (revenueAdapter.getItemCount() <= 1) {
            llHeaderEvenueDetails.setVisibility(View.GONE);
            llInfoEvenueDetails.setVisibility(View.GONE);
        } else {
            llHeaderEvenueDetails.setVisibility(View.VISIBLE);
            llInfoEvenueDetails.setVisibility(View.VISIBLE);
        }
    }

    private void initView() {
        hideError();

        spFilterEstimatedRevenue.setOnItemSelectedListener(this);
        revenueAdapter = new RevenueAdapter(activity);
        recRevenueDetails.setLayoutManager(new LinearLayoutManager(activity));
        recRevenueDetails.setHasFixedSize(true);
        recRevenueDetails.setAdapter(revenueAdapter);
        recRevenueDetails.setNestedScrollingEnabled(false);

        refreshLayout.removeCallbacks(refreshRunnable);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(refreshRunnable);

    }

    private Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            refreshLayout.setRefreshing(true);
            loadData();
        }
    };

    private void loadData() {
        if (mChannel == null || mChannelApi == null) return;
        mChannelApi.getStatistical(mChannel.getId(), this);
    }

    private void bindEstimatedRevenue() {
        if (mChannelStatistical == null) return;
        for (ChannelRevenue channelRevenue : mChannelStatistical.getChannelRevenue()) {
            if (channelRevenue.getTitle().equals(spFilterEstimatedRevenue.getSelectedItem().toString())) {
                tvNumberSpoint.setText(channelRevenue.getSpoint());
                tvDate.setText(channelRevenue.getRangeTime());
            }
        }
    }

    private void showErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.VISIBLE);
    }

    private void hideErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.GONE);
    }

    private void showErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.VISIBLE);
        emptyRetryText2.setVisibility(View.VISIBLE);
    }

    private void hideErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.GONE);
        emptyRetryText2.setVisibility(View.GONE);
    }

    private void showError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.GONE);
    }

    private String provideStr(int key, String value) {
        return String.format("<b><font color=\"#6D6D6D\">%s: </font></b> <font color=\"#6D6D6D\">%s</font>", getString(key), value);
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2020/4/14
 *
 */

package com.metfone.selfcare.ui.snackbar;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

import java.lang.ref.WeakReference;

public class CustomSnackBar extends BaseTransientBottomBar<CustomSnackBar> {
    private final String TAG = "CustomSnackBar-" + System.currentTimeMillis();
    private CustomSnackBarView snackBarView;
    private OnSnackBarListener onSnackBarListener;
    private boolean enableListener = true;
    private boolean isFirstDismiss;

    public CustomSnackBar(@NonNull ViewGroup parent, @NonNull CustomSnackBarView content, ContentViewCallback callback) {
        super(parent, content, callback);
        this.snackBarView = content;
        this.isFirstDismiss = true;
        getView().setBackgroundColor(ContextCompat.getColor(parent.getContext(), android.R.color.transparent));
        getView().setPaddingRelative(0, 0, 0, 0);
    }

    private static ViewGroup findSuitableParent(View view) {
        if (view instanceof ViewGroup) return (ViewGroup) view;
        ViewGroup fallback = null;
        do {
            if (view instanceof CoordinatorLayout) {
                // We've found a CoordinatorLayout, use it
                return (ViewGroup) view;
            } else if (view instanceof FrameLayout) {
                if (view.getId() == android.R.id.content) {
                    // If we've hit the decor content view, then we didn't find a CoL in the
                    // hierarchy, so use it.
                    return (ViewGroup) view;
                } else {
                    // It's not the content view but we'll use it as our fallback
                    fallback = (ViewGroup) view;
                }
            }

            if (view != null) {
                // Else, we will loop and crawl up the view hierarchy and try to find a parent
                final ViewParent parent = view.getParent();
                view = parent instanceof View ? (View) parent : null;
            }
        } while (view != null);

        // If we reach here then we didn't find a CoL or a suitable content view so we'll fallback
        return fallback;
    }

    public static CustomSnackBar make(View view) {
        // inflate custom layout
        ViewGroup parent = findSuitableParent(view);
        if (parent == null) {
            throw new IllegalArgumentException("No suitable parent found from the given view. "
                    + "Please provide a valid view.");
        }

        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        CustomSnackBarView snackBarView = (CustomSnackBarView) inflater.inflate(R.layout.layout_custom_snackbar, parent, false);
        CustomViewCallback viewCallback = new CustomViewCallback(snackBarView);
        CustomSnackBar snackBar = new CustomSnackBar(parent, snackBarView, viewCallback);
        viewCallback.setSnackBar(new WeakReference<>(snackBar));
        return snackBar;
    }

    public CustomSnackBar setText(String text) {
        if (snackBarView != null) snackBarView.setTitle(text);
        return this;
    }

    public CustomSnackBar setText(int resText) {
        if (snackBarView != null) snackBarView.setTitle(resText);
        return this;
    }

    public CustomSnackBar setAction(String text) {
        if (snackBarView != null) snackBarView.setButtonAction(text);
        return this;
    }

    public CustomSnackBar setAction(int resText) {
        if (snackBarView != null) snackBarView.setButtonAction(resText);
        return this;
    }

    public CustomSnackBar setAction(View.OnClickListener listener) {
        if (snackBarView != null) snackBarView.setOnClickListener(listener);
        return this;
    }

    public CustomSnackBar setAction(String text, View.OnClickListener listener) {
        if (snackBarView != null) {
            snackBarView.setButtonAction(text);
            snackBarView.setButtonActionListener(listener);
        }
        return this;
    }

    public CustomSnackBar setAction(int resText, View.OnClickListener listener) {
        if (snackBarView != null) {
            snackBarView.setButtonAction(resText);
            snackBarView.setButtonActionListener(listener);
        }
        return this;
    }

    public CustomSnackBar setOnSnackBarListener(OnSnackBarListener onSnackBarListener) {
        this.onSnackBarListener = onSnackBarListener;
        return this;
    }

    public CustomSnackBar setEnableListener(boolean enableListener) {
        this.enableListener = enableListener;
        return this;
    }

    @Override
    public void show() {
        super.show();
        Log.d(TAG, "show");
        if (enableListener && onSnackBarListener != null) onSnackBarListener.onShowSnack();
    }

    @Override
    public void dismiss() {
        Log.d(TAG, "dismiss");
        if (isFirstDismiss) {
            isFirstDismiss = false;
            if (enableListener && onSnackBarListener != null) onSnackBarListener.onDismissSnack();
        }
        super.dismiss();
    }

    public CustomSnackBar setGravity(Gravity gravity) {
        switch (gravity) {
            case TOP: {
                try {
                    View baseView = getView();
                    ViewGroup.LayoutParams layoutParams = baseView.getLayoutParams();
                    if (layoutParams instanceof RelativeLayout.LayoutParams) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layoutParams;
                        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                        baseView.setLayoutParams(params);
                    } else if (layoutParams instanceof FrameLayout.LayoutParams) {
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layoutParams;
                        params.gravity = android.view.Gravity.TOP;
                        baseView.setLayoutParams(params);
                    } else if (layoutParams instanceof LinearLayout.LayoutParams) {
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layoutParams;
                        params.gravity = android.view.Gravity.TOP;
                        baseView.setLayoutParams(params);
                    }
                } catch (ClassCastException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case CENTER: {
                try {
                    View baseView = getView();
                    ViewGroup.LayoutParams layoutParams = baseView.getLayoutParams();
                    if (layoutParams instanceof RelativeLayout.LayoutParams) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layoutParams;
                        params.addRule(RelativeLayout.CENTER_IN_PARENT);
                        baseView.setLayoutParams(params);
                    } else if (layoutParams instanceof FrameLayout.LayoutParams) {
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layoutParams;
                        params.gravity = android.view.Gravity.CENTER;
                        baseView.setLayoutParams(params);
                    } else if (layoutParams instanceof LinearLayout.LayoutParams) {
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layoutParams;
                        params.gravity = android.view.Gravity.CENTER;
                        baseView.setLayoutParams(params);
                    }
                } catch (ClassCastException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case BOTTOM: {
                try {
                    View baseView = getView();
                    ViewGroup.LayoutParams layoutParams = baseView.getLayoutParams();
                    if (layoutParams instanceof RelativeLayout.LayoutParams) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layoutParams;
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        baseView.setLayoutParams(params);
                    } else if (layoutParams instanceof FrameLayout.LayoutParams) {
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layoutParams;
                        params.gravity = android.view.Gravity.BOTTOM;
                        baseView.setLayoutParams(params);
                    } else if (layoutParams instanceof LinearLayout.LayoutParams) {
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layoutParams;
                        params.gravity = android.view.Gravity.BOTTOM;
                        baseView.setLayoutParams(params);
                    }
                } catch (ClassCastException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return this;
    }

    public interface OnSnackBarListener {
        void onShowSnack();

        void onDismissSnack();
    }

    private static class CustomViewCallback implements ContentViewCallback {
        private final String TAG = "CustomViewCallback-" + System.currentTimeMillis();
        private WeakReference<CustomSnackBar> snackBar;
        private CustomSnackBarView snackBarView;
        private boolean isFirst;

        public CustomViewCallback(CustomSnackBarView snackBarView) {
            this.snackBarView = snackBarView;
            this.isFirst = true;
        }

        public void setSnackBar(WeakReference<CustomSnackBar> snackBar) {
            this.snackBar = snackBar;
        }

        @Override
        public void animateContentIn(int delay, int duration) {
            Log.d(TAG, "animateContentIn delay: " + delay + ", duration: " + duration);
            if (snackBarView != null) {
                View view = snackBarView.getConstraintView();
                if (view != null) {
                    ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, View.SCALE_X, 0f, 1f);
                    ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, View.SCALE_Y, 0f, 1f);
                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.setDuration(duration);
                    animatorSet.setInterpolator(new OvershootInterpolator());
                    animatorSet.playTogether(scaleX, scaleY);
                    animatorSet.setStartDelay(delay);
                }
            }
        }

        @Override
        public void animateContentOut(int delay, int duration) {
            Log.d(TAG, "animateContentOut delay: " + delay + ", duration: " + duration);
            if (isFirst) {
                isFirst = false;
                if (snackBar != null && snackBar.get() != null && snackBar.get().isShown())
                    snackBar.get().dismiss();
            }
        }
    }
}

package com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.model;

import android.text.SpannableStringBuilder;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.image.ImageLoader;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;

public class VideoObject implements BaseAdapter.Clone{

    private Video video = null;

    private String textLike = "";
    private String textComment = "";
    private String textShare = "";
    private String textView = "";
    private String textSubscription = "";

    private CharSequence desCollapse = new SpannableStringBuilder();// nội dung được thu gọn

    private ImageLoader videoLoader;
    private ImageLoader channelLoader;

    private boolean isInstall = false;
    private boolean isCollapse = true;

    private int thumbnail = R.drawable.error;

    private boolean isUpdate = false;

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getTextLike() {
        return textLike;
    }

    public void setTextLike(String textLike) {
        this.textLike = textLike;
    }

    public String getTextComment() {
        return textComment;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }

    public String getTextShare() {
        return textShare;
    }

    public void setTextShare(String textShare) {
        this.textShare = textShare;
    }

    public String getTextView() {
        return textView;
    }

    public void setTextView(String textView) {
        this.textView = textView;
    }

    public String getTextSubscription() {
        return textSubscription;
    }

    public void setTextSubscription(String textSubscription) {
        this.textSubscription = textSubscription;
    }

    public CharSequence getDesCollapse() {
        return desCollapse;
    }

    public void setDesCollapse(CharSequence desCollapse) {
        this.desCollapse = desCollapse;
    }

    public ImageLoader getVideoLoader() {
        return videoLoader;
    }

    public void setVideoLoader(ImageLoader videoLoader) {
        this.videoLoader = videoLoader;
    }

    public ImageLoader getChannelLoader() {
        return channelLoader;
    }

    public void setChannelLoader(ImageLoader channelLoader) {
        this.channelLoader = channelLoader;
    }

    public boolean isInstall() {
        return isInstall;
    }

    public void setInstall(boolean install) {
        isInstall = install;
    }

    public boolean isCollapse() {
        return isCollapse;
    }

    public void setCollapse(boolean collapse) {
        isCollapse = collapse;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }

    @Override
    public VideoObject clone() {
        try {
            VideoObject videoObject = new VideoObject();
            Video temVideo;
            if (video == null) {
                temVideo = new Video();
            } else {
                temVideo = video;
            }
            videoObject.video = temVideo.clone();
//            videoObject.video = video;
            videoObject.textLike = textLike;
            videoObject.textComment = textComment;
            videoObject.textShare = textShare;
            videoObject.textView = textView;
            videoObject.desCollapse = desCollapse;
            videoObject.textSubscription = textSubscription;
            videoObject.videoLoader = videoLoader;
            videoObject.channelLoader = channelLoader;
            videoObject.isInstall = isInstall;
            videoObject.isCollapse = isCollapse;
            videoObject.thumbnail = thumbnail;
            videoObject.isUpdate = isUpdate;
            return videoObject;
        } catch (Exception e) {
            return this;
        }
    }

    @Override
    public String toString() {
        return "VideoObject{" +
                "video=" + video +
                ", textLike='" + textLike + '\'' +
                ", textComment='" + textComment + '\'' +
                ", textShare='" + textShare + '\'' +
                ", textView='" + textView + '\'' +
                ", textSubscription='" + textSubscription + '\'' +
                ", isInstall=" + isInstall +
                ", isCollapse=" + isCollapse +
                ", thumbnail=" + thumbnail +
                ", isUpdate=" + isUpdate +
                '}';
    }
}

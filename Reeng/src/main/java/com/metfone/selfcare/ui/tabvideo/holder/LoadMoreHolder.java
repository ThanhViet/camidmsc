package com.metfone.selfcare.ui.tabvideo.holder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;

public class LoadMoreHolder extends BaseAdapter.ViewHolder {
    public LoadMoreHolder(@NonNull LayoutInflater layoutInflater, @NonNull ViewGroup parent) {
        super(layoutInflater.inflate(R.layout.item_onmedia_loading_footer, parent, false));

    }

    public void bindData(){
        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        if (layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
            StaggeredGridLayoutManager.LayoutParams staggeredGridParams = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
            staggeredGridParams.setFullSpan(true);
            itemView.setLayoutParams(staggeredGridParams);
        }
    }
}

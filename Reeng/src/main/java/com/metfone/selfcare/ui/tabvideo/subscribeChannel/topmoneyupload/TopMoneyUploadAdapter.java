/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/24
 *
 */

package com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload;

import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.holder.LoadMoreHolder;
import com.metfone.selfcare.ui.tabvideo.holder.TopMoneyUploadHeaderHolder;
import com.metfone.selfcare.ui.tabvideo.holder.TopMoneyUploadHolder;

import java.util.List;

class TopMoneyUploadAdapter extends BaseAdapter {
    private final int HEADER = 1;
    private final int NORMAL = 2;
    private final int EMPTY = 0;

    TopMoneyUploadAdapter(BaseSlidingFragmentActivity activity) {
        super(activity);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return HEADER;
        ItemObject itemObject = getItem(position);
        if (itemObject != null) {
            if (itemObject.getType() == Type.LOAD_MORE) return LOAD_MORE;
            return NORMAL;
        }
        return EMPTY;
    }

    public ItemObject getItem(int position) {
        if (position >= 0) position += 2;
        if (position >= 3 && itemObjects.size() > position) return itemObjects.get(position);
        return null;
    }

    @Override
    public int getItemCount() {
        if (itemObjects != null && itemObjects.size() >= 3) {
            return itemObjects.size() - 2;
        }
        return 0;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER:
                return new TopMoneyUploadHeaderHolder(layoutInflater, parent, (TopMoneyUploadHolder.OnItemChannelNormalListener) onItemListener);
            case NORMAL:
                return new TopMoneyUploadHolder(layoutInflater, parent, (TopMoneyUploadHolder.OnItemChannelNormalListener) onItemListener);
            case LOAD_MORE:
                return new LoadMoreHolder(layoutInflater, parent);
            default:
                return new ViewHolder(layoutInflater.inflate(R.layout.holder_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof TopMoneyUploadHeaderHolder) {
            if (itemObjects.size() >= 3) {
                List<ItemObject> list = itemObjects.subList(0, 3);
                ((TopMoneyUploadHeaderHolder) holder).bindData(list);
            }
        } else if (holder instanceof TopMoneyUploadHolder) {
            ItemObject itemObject = getItem(position);
            if (itemObject != null) {
                Clone itemClone = itemObject.getInfo();
                if (itemClone instanceof TopMoneyUploadHolder.ChannelObject) {
                    ((TopMoneyUploadHolder) holder).bindData((TopMoneyUploadHolder.ChannelObject) itemClone, position);
                }
            }
        }
    }

    @Override
    public void pauseRequests() {
        super.pauseRequests();
        TopMoneyUploadHolder.pauseRequests();
        TopMoneyUploadHeaderHolder.pauseRequests();
    }

    @Override
    public void resumeRequests() {
        super.resumeRequests();
        TopMoneyUploadHolder.resumeRequests();
        TopMoneyUploadHeaderHolder.resumeRequests();
    }

    interface OnItemChannelListener extends TopMoneyUploadHolder.OnItemChannelNormalListener {
    }
}

package com.metfone.selfcare.ui.autoplay;

import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.onmedia.AutoPlayVideoModel;
import com.metfone.selfcare.database.model.onmedia.SieuHaiModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.MediaRequestHelper;
import com.metfone.selfcare.holder.onmedia.AutoPlayVideoHolder;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 1/22/2018.
 */

public class AutoPlayManager {
    private static final String TAG = AutoPlayManager.class.getSimpleName();
    private static AutoPlayManager mInstance;
    private ApplicationController mApplication;
    private AutoPlayVideoHolder mCurrentHolder;
    private AutoPlayVideoModel mCurrentModel;

    private MediaStateListener mediaStateListener;
    private Handler mHandler;
    private WSOnMedia wsOnMedia;

    private boolean isPause = false;
    private boolean isAutoPause = false;

    public synchronized static AutoPlayManager getInstance(ApplicationController app) {
        if (mInstance == null) {
            mInstance = new AutoPlayManager(app);
        }
        return mInstance;
    }

    private AutoPlayManager(ApplicationController app) {
        mHandler = new Handler(Looper.getMainLooper());
        mApplication = app;
        wsOnMedia = new WSOnMedia(mApplication);
    }

    public void release() {
        //mHandler = null;
        stopTimer();
        mHandler.removeCallbacks(mAutoHideControlTask);
        if (mCurrentHolder != null && mCurrentHolder.getMediaPlayer() != null) {
            mCurrentHolder.getMediaPlayer().stop();
            mCurrentHolder.getMediaPlayer().release();
        }
        mCurrentHolder = null;
        mCurrentModel = null;
    }

    public void onPause() {
        isPause = true;
        if (mCurrentHolder != null
                && mCurrentHolder.getMediaPlayer() != null
                && mCurrentHolder.getMediaPlayer().isPlaying()) {
            mCurrentHolder.getMediaPlayer().pause();
            isAutoPause = true;
        }
        stopTimer();
    }

    public void onResume() {
        isPause = false;
        if (mCurrentHolder != null
                && isAutoPause
                && mCurrentHolder.getMediaPlayer() != null) {
            isAutoPause = false;
            mCurrentHolder.getMediaPlayer().start();
        }
        startTimer();
    }

    public void toggleVideo() {
        if (mCurrentModel != null && mCurrentHolder != null && mCurrentHolder.getMediaPlayer() != null) {
            if (mCurrentHolder.getMediaPlayer().isPlaying()) {
                mCurrentHolder.getMediaPlayer().pause();
                mCurrentModel.setUserPause(true);
            } else {
                mCurrentHolder.getMediaPlayer().start();
                if (mCurrentModel.getCurrentDuration() > 0) {
                    mCurrentHolder.getMediaPlayer().seekTo(mCurrentModel.getCurrentDuration());
                    mCurrentModel.setCurrentDuration(-1);
                }
                mCurrentModel.setUserPause(false);
            }
        } else if (mCurrentHolder != null) {
            mCurrentHolder.forcePlayVideo();
        } else {
            Log.d(TAG, "toggleVideo error");
        }
    }

    public void seekVideo(int progress) {
        if (mCurrentModel != null && mCurrentHolder != null && mCurrentHolder.getMediaPlayer() != null) {
            int totalDuration = mCurrentHolder.getMediaPlayer().getDuration();
            int currentPosition = Utilities.progressToTimer(progress, totalDuration);
            mCurrentHolder.getMediaPlayer().seekTo(currentPosition);
            if (mCurrentModel.isUserPause()) {
                mCurrentHolder.getMediaPlayer().pause();
            }
        }
        startTimer();
    }

    public void setMediaStateListener(MediaStateListener mediaStateListener) {
        this.mediaStateListener = mediaStateListener;
    }

    public AutoPlayVideoHolder getCurrentHolder() {
        return mCurrentHolder;
    }

    public AutoPlayVideoModel getCurrentModel() {
        return mCurrentModel;
    }

    public boolean isPause() {
        return isPause;
    }

    public void setCurrentItem(AutoPlayVideoHolder holder, AutoPlayVideoModel model) {
        if (mCurrentHolder != null && mCurrentHolder.isActive()) {
            Log.d(TAG, "***************");
            mCurrentHolder.onDeActive();
        }
        this.mCurrentHolder = holder;
        this.mCurrentModel = model;
    }

    public void notifyPrepared(MediaPlayer mp) {
        startTimer();
        if (mediaStateListener != null) {
            mediaStateListener.onPrepared(mp);
        }
    }

    public void notifyError() {
        if (mediaStateListener != null) {
            mediaStateListener.onError();
        }
    }

    public void notifySizeChanged(int w, int h) {
        if (mediaStateListener != null) {
            mediaStateListener.onVideoSizeChanged(w, h);
        }
    }

    public void stopTimer() {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    public void startTimer() {
        mHandler.removeCallbacks(mUpdateTimeTask);
        if (mCurrentHolder != null) {
            mHandler.postDelayed(mUpdateTimeTask, Constants.ONMEDIA.ONE_SECOND);
        }
    }

    private Runnable mUpdateTimeTask = new Runnable() {

        public void run() {
            if (mCurrentHolder != null) {
                mCurrentHolder.drawTimeAndProgress();
            }
            if (mediaStateListener != null) {
                mediaStateListener.updateProgress(false);
            }
            reportWatchingVideo();
            if (mHandler != null)
                mHandler.postDelayed(this, Constants.ONMEDIA.ONE_SECOND);
        }
    };

    private Runnable mAutoHideControlTask = new Runnable() {
        @Override
        public void run() {
            if (mCurrentHolder != null) {
                mCurrentHolder.hideControlTask();
            }
            if (mediaStateListener != null) {
                mediaStateListener.updateProgress(true);
            }
        }
    };

    public void showOrHideControl(ImageView btnPlay, View progressControl, boolean isShow) {
        if (isShow) {
            btnPlay.setVisibility(View.VISIBLE);
            progressControl.setVisibility(View.VISIBLE);
            mHandler.removeCallbacks(mAutoHideControlTask);
            mHandler.postDelayed(mAutoHideControlTask, 3000);
        } else {
            btnPlay.setVisibility(View.GONE);
            progressControl.setVisibility(View.GONE);
            mHandler.removeCallbacks(mAutoHideControlTask);
        }
    }

    public void showOrHideControlFull(ImageView btnPlay, View progressControl, View videoDetail, boolean isShow) {
        if (isShow) {
            btnPlay.setVisibility(View.VISIBLE);
            videoDetail.setVisibility(View.VISIBLE);
            progressControl.setVisibility(View.VISIBLE);
            mHandler.removeCallbacks(mAutoHideControlTask);
            mHandler.postDelayed(mAutoHideControlTask, 3000);
        } else {
            btnPlay.setVisibility(View.GONE);
            progressControl.setVisibility(View.GONE);
            videoDetail.setVisibility(View.GONE);
            mHandler.removeCallbacks(mAutoHideControlTask);
        }
    }

    public void drawTimeAndProgress(AutoPlayVideoModel model, AutoPlayVideoHolder holder,
                                    SeekBar seekBar, TextView current, TextView duration) {
        if (model == null || holder == null) return;
        MediaPlayer player = holder.getMediaPlayer();
        int totalDuration;
        int currentDuration;
        int progress;
        if (model.getTotalDuration() <= 0) {
            if (player == null || !holder.isPrepared()) {
                totalDuration = -1;
            } else {
                totalDuration = player.getDuration();
                model.setTotalDuration(totalDuration);
            }
        } else {
            totalDuration = model.getTotalDuration();
        }
        /*if (player != null && player.isPlaying()) {

        }*/
        /*currentDuration = model.getCurrentDuration();
        if (currentDuration < 0 || player == null) {
            currentDuration = 0;
        } else {
            currentDuration = player.getCurrentPosition();
        }*/
        if (model.getCurrentDuration() >= 0) {
            currentDuration = model.getCurrentDuration();
        } else {
            currentDuration = player != null ? player.getCurrentPosition() : 0;
        }
        /*if (player == null *//*|| !holder.isPrepared()*//*) {
            currentDuration = model.getCurrentDuration() >= 0 ? model.getCurrentDuration() : 0;
            //currentDuration = 0;
        } else {
            currentDuration = player.getCurrentPosition();
        }*/
        String totalDurationStr;
        if (totalDuration <= 0) {
            progress = 0;
            totalDurationStr = "--:--";
        } else {
            progress = Utilities.getProgressPercentage(currentDuration, totalDuration);
            totalDurationStr = Utilities.milliSecondsToTimer(totalDuration);
        }
        Log.d(TAG, "drawTimeAndProgress: " + currentDuration);
        current.setText(Utilities.milliSecondsToTimer(currentDuration));
        duration.setText(totalDurationStr);
        seekBar.setProgress(progress);
    }

    public void adjustAspectRatio(TextureView textureView, int viewWidth, int viewHeight, int videoWidth, int videoHeight) {
        final double aspectRatio = (double) videoHeight / videoWidth;
        int newWidth, newHeight;
        if (viewHeight > (int) (viewWidth * aspectRatio)) {
            // limited by narrow width; restrict height
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        } else {
            // limited by short height; restrict width
            newWidth = (int) (viewHeight / aspectRatio);
            newHeight = viewHeight;
        }
        final int xoff = (viewWidth - newWidth) / 2;
        final int yoff = (viewHeight - newHeight) / 2;
        final Matrix txform = new Matrix();
        textureView.getTransform(txform);
        txform.setScale((float) newWidth / viewWidth, (float) newHeight / viewHeight);
        txform.postTranslate(xoff, yoff);
        textureView.setTransform(txform);
    }

    public void getDetailVideo(ApplicationController application, final AutoPlayVideoModel model) {
        String videoId = model.getOnMediaModel().getFeedContent().getItemId();
        MediaRequestHelper.getInstance(application).getDetailVideoSieuHai(
                String.valueOf(videoId), new MediaRequestHelper.GetDetailVideoListener() {
                    @Override
                    public void onResponseGetDetailVideo(SieuHaiModel sieuHaiModel) {
                        model.updateMediaUrl(sieuHaiModel.getVideoUrl());
                    }

                    @Override
                    public void onErrorGetDetailVideo(int errorCode) {

                    }
                });
    }

    public void reportPlayVideo(AutoPlayVideoModel model, String mediaUrl) {
        if (model.isLogPlay()) {
            Log.d(TAG, "ready log play this video: " + model.getOnMediaModel().getFeedContent().getItemName());
        } else {
            model.setLogPlay(true);
            wsOnMedia.logViewSieuHai(model.getOnMediaModel().getFeedContent().getItemId(), mediaUrl,
                    Constants.SieuHai.SieuHaiEnum.LogStreaming);
        }
    }

    public void reportWatchingVideo() {
        if (mCurrentModel != null && mCurrentHolder != null && mCurrentHolder.getMediaPlayer() != null) {
            long time = mCurrentHolder.getMediaPlayer().getCurrentPosition();
            if (time >= 5000) {
                if (!mCurrentModel.isLog5s()) {
                    mCurrentModel.setLog5s(true);
                    wsOnMedia.logViewSieuHai(mCurrentModel.getOnMediaModel().getFeedContent().getItemId(),
                            mCurrentModel.getOnMediaModel().getFeedContent().getMediaUrl(),
                            Constants.SieuHai.SieuHaiEnum.log5s);
                }
                if (time >= 30000 && !mCurrentModel.isLog30s()) {
                    mCurrentModel.setLog30s(true);
                    wsOnMedia.logViewSieuHai(mCurrentModel.getOnMediaModel().getFeedContent().getItemId(),
                            mCurrentModel.getOnMediaModel().getFeedContent().getMediaUrl(),
                            Constants.SieuHai.SieuHaiEnum.log30s);
                }
            }
        }
    }

    public interface MediaStateListener {
        void updateProgress(boolean hideControl);

        void onPrepared(MediaPlayer mp);

        void onError();

        void onVideoSizeChanged(int w, int h);
    }
}
package com.metfone.selfcare.ui.tabvideo.listener;

import com.metfone.selfcare.common.utils.listener.Listener;

/**
 * Created by tuanha00 on 3/28/2018.
 */

public interface OnInternetChangedListener extends Listener{
    void onInternetChanged();
}

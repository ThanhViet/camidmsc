package com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.holder.ChannelNormalHolder;
import com.metfone.selfcare.ui.tabvideo.holder.LoadMoreHolder;

import java.util.List;

class ChannelAdapter extends BaseAdapter {

    ChannelAdapter(BaseSlidingFragmentActivity activity) {
        super(activity);
    }

    @Override
    public int getItemViewType(int position) {
        ItemObject itemObject = itemObjects.get(position);
        if (itemObject.getType() == Type.LOAD_MORE) return LOAD_MORE;
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case LOAD_MORE:
                return new LoadMoreHolder(layoutInflater, parent);
            default:
                return new ChannelNormalHolder(layoutInflater, parent, (ChannelNormalHolder.OnItemChannelNormalListener) onItemListener);
        }
    }

    @Override
    protected Object getChangePayload(ItemObject oldItem, ItemObject newItem) {
        Bundle bundle = new Bundle();

        Object oldObject = oldItem.getInfo();
        Object newObject = newItem.getInfo();

        if (oldObject instanceof ChannelNormalHolder.ChannelObject && newObject instanceof ChannelNormalHolder.ChannelObject) {
            ChannelNormalHolder.ChannelObject oldVideoObject = (ChannelNormalHolder.ChannelObject) oldObject;
            ChannelNormalHolder.ChannelObject newVideoObject = (ChannelNormalHolder.ChannelObject) newObject;
            if (!oldVideoObject.getTextFollow().equals(newVideoObject.getTextFollow()))
                bundle.putBoolean(Constants.TabVideo.SUBSCRIPTION, true);
        }
        if (bundle.isEmpty()) return null;
        return bundle;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) super.onBindViewHolder(holder, position, payloads);
        else {
            Bundle bundle = (Bundle) payloads.get(0);
            if (holder instanceof ChannelNormalHolder) {
                ChannelNormalHolder videoHolder = (ChannelNormalHolder) holder;
                if (bundle.getBoolean(Constants.TabVideo.SUBSCRIPTION, false))
                    videoHolder.bindSub();
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemObject itemObject = itemObjects.get(position);
        Clone itemClone = itemObject.getInfo();
        if (holder instanceof ChannelNormalHolder && itemClone instanceof ChannelNormalHolder.ChannelObject) {
            ((ChannelNormalHolder) holder).bindData((ChannelNormalHolder.ChannelObject) itemClone);
        }
    }

    @Override
    public void pauseRequests() {
        super.pauseRequests();
        ChannelNormalHolder.pauseRequests();
    }

    @Override
    public void resumeRequests() {
        super.resumeRequests();
        ChannelNormalHolder.resumeRequests();
    }

    interface OnItemChannelListener extends ChannelNormalHolder.OnItemChannelNormalListener {
    }
}

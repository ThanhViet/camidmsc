package com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.di.MvpPresenter;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;

public interface VideoDetailPresenter extends MvpPresenter {
    /**
     * thông tin video
     *
     * @param video thông tin video
     */
    void setVideo(Video video);

    void getVideoData(Video video);

    /**
     * lấy dữ liệu đầu tiên của tab
     *
     * @param limit giới hạn dữ liệu
     */
    void getData(int limit);

    /**
     * lấy dữ liệu load more
     *
     * @param limit giới hạn dữ liệu
     */
    void getDataLoadMore(int limit);

    /**
     * yêu thích video
     *
     * @param video thông tin video
     */
    void likeVideo(Video video);

//    /**
//     * gửi thông báo xem video
//     *
//     * @param video thông tin video đang xem
//     */
//    void seenVideo(Video video);

    /**
     * chia sẻ video
     *
     * @param activity activity
     * @param video    thông tin video
     */
    void shareVideo(BaseSlidingFragmentActivity activity, Video video);

    /**
     * save video
     *
     * @param video thông tin video
     */
    void saveVideo(Video video);

    /**
     * mở màn hình comment
     *
     * @param activity activity
     * @param video    thông tin video
     */
    void openComment(BaseSlidingFragmentActivity activity, Video video);

    /**
     * đăng ký video
     *
     * @param channel thông tin channel
     */
    void subscription(Channel channel);

    /**
     * mở màn hình channel
     *
     * @param activity activity
     * @param channel  thông tin channel
     */
    void openChannel(BaseSlidingFragmentActivity activity, Channel channel);

//    void callApiLog(Video currentVideo);

    void setupData(ArrayList<Video> list);
    void addVideoToListSeen(String id);

}

package com.metfone.selfcare.ui.tabvideo;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapterV3 extends BaseAdapterV2<Object, RecyclerView.LayoutManager, BaseAdapterV3.ViewHolder> {

    public BaseAdapterV3(Activity act) {
        super(act);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        holder.bindData(items, position, payloads);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(items, position);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.onViewAttachedToWindow(this);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        holder.onViewDetachedFromWindow(this);
        super.onViewDetachedFromWindow(holder);
    }

    public static class ViewHolder extends BaseAdapterV2.ViewHolder {

        @Nullable
        protected BaseAdapterV3 baseAdapter;

        public ViewHolder(View view) {
            super(view);
        }

        public void onViewAttachedToWindow(@Nullable BaseAdapterV3 baseAdapter) {
            this.baseAdapter = baseAdapter;
        }

        public void onViewDetachedFromWindow(@Nullable BaseAdapterV3 baseAdapter) {
            this.baseAdapter = baseAdapter;
        }
    }

    public static class LoadMoreHolder extends ViewHolder {

        public LoadMoreHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            this(layoutInflater.inflate(R.layout.sample_common_list_footer, parent, false));
        }

        public LoadMoreHolder(View view) {
            super(view);
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            if (layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
                StaggeredGridLayoutManager.LayoutParams staggeredGridParams = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
                staggeredGridParams.setFullSpan(true);
                itemView.setLayoutParams(staggeredGridParams);
            }
        }
    }

    public static class EndHolder extends ViewHolder {

        public EndHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            this(layoutInflater.inflate(R.layout.item_end, parent, false));
        }

        EndHolder(View view) {
            super(view);
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            if (layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
                StaggeredGridLayoutManager.LayoutParams staggeredGridParams = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
                staggeredGridParams.setFullSpan(true);
                itemView.setLayoutParams(staggeredGridParams);
            }
        }
    }

    public static class SpaceHolder extends ViewHolder {

        public SpaceHolder(int margin) {
            super(provideView(margin));
        }

        private static View provideView(int margin) {
            Context context = ApplicationController.self();
            View view = new View(context);
            view.setLayoutParams(new ViewGroup.LayoutParams(margin, margin));
            return view;
        }
    }
}

package com.metfone.selfcare.ui.tabvideo.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.VideoRevenue;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;

public class RevenueAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {

    public static final int HEADER = 0;
    public static final int NORMAL = 1;
    public static final int LOAD_MORE = 2;

    public RevenueAdapter(Activity act) {
        super(act);
    }

    @Override
    public int getItemViewType(int position) {
        Object object = items.get(position);
        if (position == 0) return HEADER;
        if (object instanceof VideoRevenue) return NORMAL;
        return LOAD_MORE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER:
                return new HeaderHolder(activity, layoutInflater, parent);
            case NORMAL:
                return new NormalHolder(activity, layoutInflater, parent);
            default:
                return new LoadMoreHolder(activity, layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder)
            ((ViewHolder) holder).bindData(items, position);
    }

    public static class RevenueHolder extends ViewHolder {

        @BindView(R.id.tv_link)
        TextView tvLink;
        @BindView(R.id.tv_number_view)
        TextView tvNumberView;
        @BindView(R.id.tv_revenue)
        TextView tvRevenue;

        RevenueHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_channel_detail_revenue, parent, false));
        }
    }

    public static class HeaderHolder extends RevenueHolder {

        HeaderHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
            super(activity, layoutInflater, parent);
            tvLink.setTypeface(null, Typeface.BOLD);
            tvLink.setText(R.string.channel_detail_title_link_video);
            tvLink.setTextColor(ContextCompat.getColor(activity, R.color.videoColorNormal));

            tvNumberView.setTypeface(null, Typeface.BOLD);
            tvNumberView.setText(R.string.channel_detail_title_view_30s);
            tvNumberView.setTextColor(ContextCompat.getColor(activity, R.color.videoColorNormal));

            tvRevenue.setTypeface(null, Typeface.BOLD);
            tvRevenue.setText(R.string.channel_detail_title_revenue);
            tvRevenue.setTextColor(ContextCompat.getColor(activity, R.color.videoColorNormal));
        }
    }

    public static class NormalHolder extends RevenueHolder {

        private VideoRevenue mVideoRevenue;

        NormalHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
            super(activity, layoutInflater, parent);
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            super.bindData(items, position);
            Object item = items.get(position);
            if (item instanceof VideoRevenue) {
                mVideoRevenue = (VideoRevenue) item;
                tvLink.setText(mVideoRevenue.getName());

                ExecutorService executor = Executors.newFixedThreadPool(5);
                asyncSetText(tvNumberView, new Handler(), mVideoRevenue.getViewGr30s(), executor);
                asyncSetTextPointRevenue(tvRevenue, new Handler(), mVideoRevenue.getPointRevenue(), executor);
                executor.shutdown();
            }
        }

        private void asyncSetText(TextView textView, final Handler handler, final long numberView, Executor executor) {
            final WeakReference<TextView> textViewRef = new WeakReference<>(textView);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    TextView textView = textViewRef.get();
                    if (textView == null) return;
                    final String numberViewStr = Utilities.shortenLongNumber(numberView);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            TextView textView = textViewRef.get();
                            if (textView == null) return;
                            textView.setText(numberViewStr);
                        }
                    });
                }
            });
        }

        private void asyncSetTextPointRevenue(TextView textView, final Handler handler, final long numberView, Executor executor) {
            final WeakReference<TextView> textViewRef = new WeakReference<>(textView);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    TextView textView = textViewRef.get();
                    if (textView == null) return;
                    final String numberViewStr = Utilities.shortenLongNumber(numberView);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            TextView textView = textViewRef.get();
                            if (textView == null) return;
                            textView.setText(numberViewStr);
                        }
                    });
                }
            });
        }
    }
}

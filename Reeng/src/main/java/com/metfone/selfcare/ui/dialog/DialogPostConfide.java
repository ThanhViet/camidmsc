package com.metfone.selfcare.ui.dialog;

import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.ashokvarma.bottomnavigation.utils.Utils;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.StrangerTopicConfideAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.TopicConfide;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.ui.DetailGridItemDecoration;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by toanvk2 on 7/21/2017.
 */

public class DialogPostConfide extends DialogFragment implements View.OnClickListener {
    //    private BaseSlidingFragmentActivity activity;
//    private Resources mRes;
//    private String negativeLabel;
//    private String positiveLabel;
    private NegativeListener<String> negativeListener;
    private PositiveListener<String> positiveListener;
    private DismissListener dismissListener;

    @BindView(R.id.tvCancel)
    AppCompatTextView tvCancel;
    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView tvTitleToolbar;
    @BindView(R.id.btnDone)
    RoundTextView btnDone;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    //    private Button mBtnNegative, mBtnPositive;
//    private ImageView mBtnHelp;
//    private TextView mTvwNoteChooseTopic;
    private int currentPositionSelected = -1;

    private ArrayList<TopicConfide> listTopic = new ArrayList<>();
    private StrangerTopicConfideAdapter mAdapter;
    private Unbinder unbinder;

//    public DialogPostConfide(BaseSlidingFragmentActivity activity, boolean isCancelable) {
//        super(activity, R.style.DialogFullscreen);
//        this.activity = activity;
//        this.mRes = activity.getResources();
//        setCancelable(isCancelable);
//    }

    public static DialogPostConfide newInstance() {
        Bundle args = new Bundle();
        DialogPostConfide fragment = new DialogPostConfide();
        fragment.setArguments(args);
        return fragment;
    }

//    public DialogPostConfide setNegativeLabel(String label) {
//        this.negativeLabel = label;
//        return this;
//    }
//
//    public DialogPostConfide setPositiveLabel(String label) {
//        this.positiveLabel = label;
//        return this;
//    }

    public DialogPostConfide setNegativeListener(NegativeListener listener) {
        this.negativeListener = listener;
        return this;
    }

    public DialogPostConfide setPositiveListener(PositiveListener<String> listener) {
        this.positiveListener = listener;
        return this;
    }

    public DialogPostConfide setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFullscreenV5);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_post_confide_v5, container, false);
        unbinder = ButterKnife.bind(this, view);
        tvTitleToolbar.setText(R.string.create_invite_confide);
        tvCancel.setPaintFlags(tvCancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvCancel.setText(R.string.btn_cancel);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogV5Animation;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getDialog().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getDialog().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.white));
        }
//        findComponentViews();
        drawDetail();
        setListener();
    }

    @Override
    public void dismiss() {
        Log.d("DialogEditText", "dismiss");
        //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_button_negative:
                if (negativeListener != null) {
                    negativeListener.onNegative(null);
                }
                dismiss();
                break;
            case R.id.dialog_button_positive:
                if (positiveListener != null) {
                    TopicConfide topic = getTopicSelected();
                    if (topic == null) {
//                        mTvwNoteChooseTopic.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
                    } else {
                        positiveListener.onPositive(topic.getMessage());
                        dismiss();
                    }
                } else {
                    dismiss();
                }
                break;
            case R.id.dialog_button_help:
                UrlConfigHelper.getInstance(getContext()).gotoWebViewOnMedia((ApplicationController) getActivity().getApplicationContext(),
                        (BaseSlidingFragmentActivity) getActivity(), getString(R.string.stranger_confide_help_url));
                dismiss();
                break;
        }
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private void findComponentViews() {
        mRecyclerView = getView().findViewById(R.id.recyclerView);
//        mBtnNegative = (Button) findViewById(R.id.dialog_button_negative);
//        mBtnPositive = (Button) findViewById(R.id.dialog_button_positive);
//        mBtnHelp = (ImageView) findViewById(R.id.dialog_button_help);
//        mTvwNoteChooseTopic = (TextView) findViewById(R.id.dialog_note_choose_topic);
    }

    private void setListener() {
        tvCancel.setOnClickListener(view -> dismiss());
        btnDone.setOnClickListener(view -> {
            if (positiveListener != null) {
                positiveListener.onPositive(getConfideSelected());
                dismissAllowingStateLoss();
            } else {
                dismissAllowingStateLoss();
            }
        });
//        mBtnPositive.setOnClickListener(this);
//        mBtnNegative.setOnClickListener(this);
//        mBtnHelp.setOnClickListener(this);
    }

    private String getConfideSelected() {
        switch (currentPositionSelected) {
            case 0:
                return getString(R.string.stranger_confide_suggest_happy_full);
            case 1:
                return getString(R.string.stranger_confide_suggest_excited_full);
            case 2:
                return getString(R.string.stranger_confide_suggest_wonderful_full);
            case 3:
                return getString(R.string.stranger_confide_suggest_cool_full);
            case 4:
                return getString(R.string.stranger_confide_suggest_crazy_full);
            case 5:
                return getString(R.string.stranger_confide_suggest_sad_full);
            case 6:
                return getString(R.string.stranger_confide_suggest_disappointed_full);
            case 7:
                return getString(R.string.stranger_confide_suggest_angry_full);
            case 8:
                return getString(R.string.stranger_confide_suggest_alone_full);
            case 9:
                return getString(R.string.stranger_confide_suggest_missing_full);
            default:
                return getString(R.string.stranger_confide_suggest_happy_full);
        }
    }

    private void drawDetail() {
//        mTvwNoteChooseTopic.setTextColor(ContextCompat.getColor(activity, R.color.text_gray));
//        if (TextUtils.isEmpty(negativeLabel)) {
//            mBtnNegative.setVisibility(View.GONE);
//        } else {
//            mBtnNegative.setVisibility(View.VISIBLE);
//            mBtnNegative.setText(negativeLabel);
//        }
//        if (TextUtils.isEmpty(positiveLabel)) {
//            mBtnPositive.setVisibility(View.GONE);
//        } else {
//            mBtnPositive.setVisibility(View.VISIBLE);
//            mBtnPositive.setText(positiveLabel);
//        }
        initListTopic();
        setAdapter();
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new StrangerTopicConfideAdapter((BaseSlidingFragmentActivity) getActivity(), listTopic, entry -> changeTopicSelected(entry));
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
            mRecyclerView.addItemDecoration(new DetailGridItemDecoration(2, Utils.dp2px(getContext(), 15), false));
        } else {
            mAdapter.setListTopic(listTopic);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void initListTopic() {
        TopicConfide topicHappy, topicConfusion, topicWonderful, topicCool, topicCrazy, topicSad, topicDisappointed, topicAngry, topicAlone, topicMiss;
        topicHappy = new TopicConfide(getString(R.string.stranger_confide_suggest_happy));
        topicConfusion = new TopicConfide(getString(R.string.stranger_confide_suggest_excited));
        topicWonderful = new TopicConfide(getString(R.string.stranger_confide_suggest_wonderful));
        topicCool = new TopicConfide(getString(R.string.stranger_confide_suggest_cool));
        topicCrazy = new TopicConfide(getString(R.string.stranger_confide_suggest_crazy));
        topicSad = new TopicConfide(getString(R.string.stranger_confide_suggest_sad));
        topicDisappointed = new TopicConfide(getString(R.string.stranger_confide_suggest_disappointed));
        topicAngry = new TopicConfide(getString(R.string.stranger_confide_suggest_angry));
        topicAlone = new TopicConfide(getString(R.string.stranger_confide_suggest_alone));
        topicMiss = new TopicConfide(getString(R.string.stranger_confide_suggest_missing));
        listTopic = new ArrayList<>();
        listTopic.add(topicHappy);
        listTopic.add(topicConfusion);
        listTopic.add(topicWonderful);
        listTopic.add(topicCool);
        listTopic.add(topicCrazy);
        listTopic.add(topicSad);
        listTopic.add(topicDisappointed);
        listTopic.add(topicAngry);
        listTopic.add(topicAlone);
        listTopic.add(topicMiss);
    }

    private void changeTopicSelected(int positionSelect) {
        if (listTopic != null && positionSelect >= 0 && positionSelect < listTopic.size()) {
            TopicConfide topic = listTopic.get(positionSelect);
            currentPositionSelected = positionSelect;
            if (topic.isSelected()) return;// click lai vao item da chon thi ko lam gi
            for (TopicConfide item : listTopic) {
                item.setSelected(false);
            }
            topic.setSelected(true);
        }
        mAdapter.notifyDataSetChanged();
        updateButtonDone();
//        mTvwNoteChooseTopic.setTextColor(ContextCompat.getColor(getContext(), R.color.text_gray));
    }

    private TopicConfide getTopicSelected() {
        for (TopicConfide item : listTopic) {
            if (item.isSelected()) {
                return item;
            }
        }
        return null;
    }

    private void updateButtonDone() {
        btnDone.setEnabled(true);
        btnDone.setTextColor(ContextCompat.getColor(getContext(), R.color.v5_text_7));
        btnDone.setBackgroundColorRound(ContextCompat.getColor(getContext(), R.color.v5_main_color));
        btnDone.setBackgroundColorAndPress(ContextCompat.getColor(getContext(), R.color.v5_main_color),
                ContextCompat.getColor(getContext(), R.color.v5_main_color_press));
    }
}
package com.metfone.selfcare.ui.tabvideo.channelDetail.statisticalDetail;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.VideoRevenue;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.adapter.RevenueAdapter;
import com.metfone.selfcare.ui.tabvideo.channelDetail.ChannelDetailActivity;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class StatisticalDetailFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        ApiCallbackV2<ArrayList<VideoRevenue>>,
        OnInternetChangedListener {

    private static final String CHANNEL = "channel";
    private static final int LIMIT = 20;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    Unbinder unbinder;

    public static StatisticalDetailFragment newInstance(Channel channel) {

        Bundle args = new Bundle();
        args.putSerializable(CHANNEL, channel);
        StatisticalDetailFragment fragment = new StatisticalDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private @Nullable
    Channel mChannel;
    private Object loadMore;
    private Object header;

    private ListenerUtils listenerUtils;
    private VideoApi mVideoApi;

    private RevenueAdapter mRevenueAdapter;

    private int offset = 0;
    private String lastId = "";
    private ArrayList<Object> feeds;
    private boolean loading = false;
    private boolean isLoadMore = false;

    private boolean isGetInfoSuccess = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadMore = new Object();
        header = new Object();
        Bundle bundle = getArguments();
        mChannel = (Channel) (bundle != null ? bundle.getSerializable(CHANNEL) : null);
        listenerUtils = application.getApplicationComponent().providerListenerUtils();
        mVideoApi = application.getApplicationComponent().providerVideoApi();

        listenerUtils.addListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistical_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        initView();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listenerUtils.removerListener(this);
        unbinder.unbind();
    }

    @OnClick(R.id.iv_search)
    public void onIvSearchClicked() {
        ((ChannelDetailActivity) activity).addStatisticalSearch();
    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        initView();
    }

    @OnClick(R.id.iv_back)
    public void onIvBackClicked() {
        activity.onBackPressed();
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    @Override
    public void onSuccess(String lastIdStr, ArrayList<VideoRevenue> results) {
        if (recyclerView == null || mRevenueAdapter == null) return;
        isGetInfoSuccess = true;
        lastId = lastIdStr;

        if (feeds == null) feeds = new ArrayList<>();
        if (offset == 0) feeds.clear();
        if (feeds.isEmpty()) feeds.add(header);

        isLoadMore = results.size() >= LIMIT;
        feeds.remove(loadMore);

        if (Utilities.notEmpty(results)) {
            for (VideoRevenue video : results) {
                if (feeds.contains(video)) continue;
                feeds.add(video);
            }
        }
        if (isLoadMore) feeds.add(loadMore);
        recyclerView.stopScroll();
        mRevenueAdapter.bindData(feeds);

        if (Utilities.notEmpty(feeds))
            hideError();
        else
            onError("");
    }

    @Override
    public void onError(String s) {
        isGetInfoSuccess = false;
        if (Utilities.notEmpty(feeds) || recyclerView == null) return;
        showError();
        if (!NetworkHelper.isConnectInternet(activity)) {
            showErrorNetwork();
            hideErrorDataEmpty();
        } else {
            showErrorDataEmpty();
            hideErrorNetwork();
        }
    }

    @Override
    public void onComplete() {
        loading = false;
        if (refreshLayout == null) return;
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(activity)
                || isGetInfoSuccess
                || Utilities.notEmpty(feeds)
                || recyclerView == null)
            return;
        isGetInfoSuccess = true;
        initView();
    }

    private void initView() {
        hideError();

        mRevenueAdapter = new RevenueAdapter(activity);
        mRevenueAdapter.setOnLoadMoreListener(onLoadMoreListener);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mRevenueAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setPadding(Utilities.dpToPixels(4, getResources()), 0, Utilities.dpToPixels(4, getResources()), 0);

        refreshLayout.removeCallbacks(refreshRunnable);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(refreshRunnable);
    }

    private Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            if (refreshLayout == null) return;
            refreshLayout.setRefreshing(true);
            loadData();
        }
    };

    private BaseAdapterV2.OnLoadMoreListener onLoadMoreListener = new BaseAdapterV2.OnLoadMoreListener() {
        @Override
        public void onLoadMore() {
            if (loading) return;
            if (!isLoadMore) return;
            loadMoreData();
        }
    };

    private void loadData() {
        if (loading) return;
        if (mChannel == null) return;
        loading = true;
        mVideoApi.getVideoRevenuesByChannelIdV2(mChannel.getId(), "", offset, LIMIT, lastId, this);
    }

    private void refreshData() {
        offset = 0;
        lastId = "";
        loadData();
    }

    private void loadMoreData() {
        offset = offset + LIMIT;
        loadData();
    }

    private void showErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.VISIBLE);
    }

    private void hideErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.GONE);
    }

    private void showErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.VISIBLE);
        emptyRetryText2.setVisibility(View.VISIBLE);
    }

    private void hideErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.GONE);
        emptyRetryText2.setVisibility(View.GONE);
    }

    private void showError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.GONE);
    }
}

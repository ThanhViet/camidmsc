package com.metfone.selfcare.ui.guestbook;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.metfone.selfcare.adapter.guestbook.TemplateAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Background;
import com.metfone.selfcare.listeners.OnSelectSticker;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.StrangerGridLayoutManager;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/14/2017.
 */
public class DialogSelectBackground extends DialogFragment {
    private static final String TAG = DialogSelectBackground.class.getSimpleName();
    private Context mContext;
    private ApplicationController mApplication;
    private OnSelectSticker callback;
    private RecyclerView mRecyclerView;
    private StrangerGridLayoutManager mGridLayoutManager;
    private TemplateAdapter mAdapter;
    protected ArrayList<Background> backgrounds;

    public static DialogSelectBackground newInstance(ArrayList<Background> backgrounds) {
        DialogSelectBackground fragment = new DialogSelectBackground();
        fragment.backgrounds = backgrounds;
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSelectSticker) {
            this.callback = (OnSelectSticker) context;
            this.mContext = context;
            this.mApplication = (ApplicationController) mContext.getApplicationContext();
        } else {
            throw new IllegalStateException(context.getClass().getName()
                    + " must implement " + OnSelectSticker.class.getName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycle_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        drawDetail();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // clearing memory on exit, cos manipulating with text uses bitmaps extensively
        // this does not frees memory immediately, but still can help
       /* System.gc();
        Runtime.getRuntime().gc();*/
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                int height = ((ApplicationController) getContext().getApplicationContext()).getHeightPixels();
                height = (height * 2) / 3;
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, height);
            }
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onDetach() {
        // release links
        this.callback = null;
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }

    private void drawDetail() {
        initAdapter();
    }

    private void initAdapter() {
        //emoCollections.add(new EmoCollection());
        mGridLayoutManager = new StrangerGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mGridLayoutManager.setAutoMeasureEnabled(false);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mAdapter = new TemplateAdapter(mContext);
        mAdapter.setListBackgrounds(backgrounds);
        mRecyclerView.setAdapter(mAdapter);
        setListener();
    }

    private void setListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                callback.onBackgroundItemSelected((Background) object);
                dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        // long click
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }
}
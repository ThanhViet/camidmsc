package com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.constant.VideoConstant;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChangedDataListener;

import java.util.ArrayList;

public class VideoLibraryPresenterImpl implements VideoLibraryPresenter,
        OnVideoCallback,
        OnVideoChangedDataListener,
        OnChannelChangedDataListener {

    private ApplicationController application;
    private VideoLibraryView videoLibraryView;
    private ListenerUtils listenerUtils;

    private VideoApi videoApi;
    private ArrayList<Video> videos;

    private String type = "";

    public VideoLibraryPresenterImpl(ApplicationController application, BaseView baseView) {
        this.application = application;
        this.videoLibraryView = (VideoLibraryView) baseView;

        this.videoApi = application.getApplicationComponent().providerVideoApi();
        this.listenerUtils = application.getApplicationComponent().providerListenerUtils();
        this.videos = new ArrayList<>();

        this.listenerUtils.addListener(this);
    }

    @Override
    public void getVideoByType(String type) {
        this.type = type;
        videoApi.getVideosByType(type, this);
    }

    @Override
    public void removerVideo(Video video) {
        if (type == null || listenerUtils == null || videoApi == null) return;
        if (type.equals(VideoConstant.Type.SAVED.VALUE)) {
            video.setSave(false);
            listenerUtils.notifyVideoSaveChangedData(video);
        } else {
            video.setWatchLater(false);
            listenerUtils.notifyVideoWatchLaterChangedData(video);
        }
        videoApi.removerVideo(type, video);
        videoApi.getVideosByType(type, this);
    }

    @Override
    public void removerListener() {
        if (listenerUtils != null) listenerUtils.removerListener(this);
    }

    @Override
    public void onGetVideosSuccess(ArrayList<Video> list) {
        if (videos == null || videoLibraryView == null) return;
        videos.clear();
        videos.addAll(list);
        videoLibraryView.showVideo(videos);
    }

    @Override
    public void onGetVideosError(String s) {

    }

    @Override
    public void onGetVideosComplete() {

    }

    @Override
    public void onVideoLikeChanged(Video video) {
        if (videos == null) return;
        for (Video v : videos) {
            if (v.equals(video)) {
                v.setTotalLike(video.getTotalLike());
                v.setLike(video.isLike());
            }
        }
    }

    @Override
    public void onVideoShareChanged(Video video) {
        if (videos == null) return;
        for (Video v : videos) {
            if (v.equals(video)) {
                v.setTotalShare(video.getTotalShare());
                v.setShare(video.isShare());
            }
        }
    }

    @Override
    public void onVideoCommentChanged(Video video) {
        if (videos == null) return;
        for (Video v : videos) {
            if (v.equals(video)) {
                v.setTotalComment(video.getTotalComment());
            }
        }
    }

    @Override
    public void onChannelCreate(Channel channel) {

    }

    @Override
    public void onChannelUpdate(Channel channel) {

    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        if (videos == null) return;
        for (Video video : videos) {
            if (video.getChannel().equals(channel)) {
                video.setChannel(channel);
            }
        }
    }

    @Override
    public void onVideoSaveChanged(Video video) {
        if (type == null || videoLibraryView == null || videos == null) return;
        if (type.equals(VideoConstant.Type.LATER.VALUE)) return;
        if (!video.isSave()) {
            videos.remove(video);
        } else {
            videos.add(0, video);
        }
        videoLibraryView.showVideo(videos);
    }

    @Override
    public void onVideoWatchLaterChanged(Video video) {
        if (type == null || videoLibraryView == null || videos == null) return;
        if (type.equals(VideoConstant.Type.SAVED.VALUE)) return;
        if (!video.isWatchLater()) {
            videos.remove(video);
        } else {
            videos.add(0, video);
        }
        videoLibraryView.showVideo(videos);
    }
}

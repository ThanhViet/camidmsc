package com.metfone.selfcare.ui.dialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerLocation;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.ui.RangeSeekBar;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 8/14/2017.
 */

public class BottomFilterStranger extends BottomSheetDialog implements View.OnClickListener {
    private static final String TAG = BottomSheetMenu.class.getSimpleName();
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private StrangerFilterHelper mFilterHelper;
    private PositiveListener<Boolean> mCallBack;
    private View mViewFilterLocation, mViewSexMale, mViewSexFemale, mViewSexAll;
    private RangeSeekBar<Integer> mRangeSeekBar;
    private TextView mTvwFilterLocation;
    private RoundTextView mBtnDone;
    //
    private StrangerLocation currentLocation;

    public BottomFilterStranger(@NonNull BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity);
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplicationContext();
        this.mFilterHelper = StrangerFilterHelper.getInstance(mApplication);
        setCancelable(isCancelable);
    }

    public BottomFilterStranger setListener(PositiveListener<Boolean> listener) {
        this.mCallBack = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.bottom_sheet_filter_stranger);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        findComponentViews();
        drawDetail();
        setViewListener();
        /*drawDetail();
        setListener();*/
    }

    @Override
    public void dismiss() {
        Log.d("BottomSheetMenu", "dismiss");
        super.dismiss();
        mFilterHelper.initLocationSelected();
        /*if (dismissListener != null) {
            dismissListener.onDismiss();
        }*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_stranger_location_layout:
                if (mApplication.getReengAccountBusiness().isVietnam()) {
                    new DialogSelectStrangerLocation(activity).setNegativeListener(new NegativeListener<StrangerLocation>
                            () {
                        @Override
                        public void onNegative(StrangerLocation result) {
                            currentLocation = result;
                            mTvwFilterLocation.setText(currentLocation.getName());
                        }
                    }).show();
                }
                break;
            case R.id.filter_stranger_sex_male:
                selectedSexMale();
                break;
            case R.id.filter_stranger_sex_female:
                selectedSexFeMale();
                break;
            case R.id.filter_stranger_sex_all:
                selectedSexAll();
                break;
            case R.id.filter_stranger_done:
                handleDoneFilter();
                break;
        }
    }

    private void findComponentViews() {
        mViewFilterLocation = findViewById(R.id.filter_stranger_location_layout);
        mTvwFilterLocation = (TextView) findViewById(R.id.filter_stranger_location_text);
        mViewSexMale = findViewById(R.id.filter_stranger_sex_male);
        mViewSexFemale = findViewById(R.id.filter_stranger_sex_female);
        mViewSexAll = findViewById(R.id.filter_stranger_sex_all);
        mRangeSeekBar = (RangeSeekBar<Integer>) findViewById(R.id.filter_stranger_age_seekbar);
        mBtnDone = (RoundTextView) findViewById(R.id.filter_stranger_done);
    }

    private void setViewListener() {
        mViewFilterLocation.setOnClickListener(this);
        mViewSexMale.setOnClickListener(this);
        mViewSexFemale.setOnClickListener(this);
        mViewSexAll.setOnClickListener(this);
        mBtnDone.setOnClickListener(this);
    }

    private void drawDetail() {
        currentLocation = mFilterHelper.getFilterLocation();
        mTvwFilterLocation.setText(currentLocation.getName());
        mRangeSeekBar.setRangeValues(StrangerFilterHelper.AGE_MIN, StrangerFilterHelper.AGE_MAX);
        int currentSex = mFilterHelper.getFilterSex();
        if (currentSex == Constants.CONTACT.GENDER_MALE) {
            selectedSexMale();
        } else if (currentSex == Constants.CONTACT.GENDER_FEMALE) {
            selectedSexFeMale();
        } else {
            selectedSexAll();
        }
        int ageMin = mFilterHelper.getRealFilterAgeMin();
        if (ageMin < StrangerFilterHelper.AGE_MIN) {
            ageMin = StrangerFilterHelper.AGE_MIN;
        }
        mRangeSeekBar.setSelectedMinValue(ageMin);
        // draw
        int ageMax = mFilterHelper.getRealFilterAgeMax();
        if (ageMax > StrangerFilterHelper.AGE_MAX) {
            ageMax = StrangerFilterHelper.AGE_MAX;
        }
        mRangeSeekBar.setSelectedMaxValue(ageMax);
    }

    private void selectedSexMale() {
        mViewSexMale.setSelected(true);
        mViewSexFemale.setSelected(false);
        mViewSexAll.setSelected(false);
    }

    private void selectedSexFeMale() {
        mViewSexMale.setSelected(false);
        mViewSexFemale.setSelected(true);
        mViewSexAll.setSelected(false);
    }

    private void selectedSexAll() {
        mViewSexMale.setSelected(false);
        mViewSexFemale.setSelected(false);
        mViewSexAll.setSelected(true);
    }

    private void handleDoneFilter() {
        int sex = -1;
        if (mViewSexMale.isSelected()) {
            sex = Constants.CONTACT.GENDER_MALE;
        } else if (mViewSexFemale.isSelected()) {
            sex = Constants.CONTACT.GENDER_FEMALE;
        }
        int minAge = mRangeSeekBar.getSelectedMinValue();
        int maxAge = mRangeSeekBar.getSelectedMaxValue();
        mFilterHelper.updateFilter(currentLocation, sex, minAge, maxAge, false);
        if (mCallBack != null) {
            mCallBack.onPositive(true);
        }
        dismiss();
    }
}
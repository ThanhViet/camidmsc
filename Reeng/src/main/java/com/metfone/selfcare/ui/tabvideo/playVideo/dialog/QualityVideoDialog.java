package com.metfone.selfcare.ui.tabvideo.playVideo.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.model.tab_video.Resolution;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QualityVideoDialog extends BottomSheetDialog {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.radio_group)
    LinearLayout radioGroup;

    private OnQualityVideoListener mOnQualityVideoListener;
    private ArrayList<Resolution> resolutions;
    private Video currentVideo;
    private Activity context;

    private View bottomSheet;

    public QualityVideoDialog(@NonNull Activity context) {
        super(context);
        this.context = context;
    }

    public QualityVideoDialog setCurrentVideo(Video currentVideo) {
        this.currentVideo = currentVideo;
        return this;
    }

    public QualityVideoDialog setOnQualityVideoListener(OnQualityVideoListener mOnQualityVideoListener) {
        this.mOnQualityVideoListener = mOnQualityVideoListener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_quality_video);
        if (getWindow() != null)
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);
        if (currentVideo != null)
            resolutions = currentVideo.getListResolution();
        View view;
        TextView tvCheckBox;
        ImageView ivCheckBox;
        if (Utilities.notEmpty(resolutions)) {
            String configResolution = ApplicationController.self().getConfigResolutionVideo();
            boolean check = false;
            int size = resolutions.size() - 1;
            for (int i = size; i >= 0; i--) {
                Resolution resolution = resolutions.get(i);
                view = LayoutInflater.from(context).inflate(R.layout.layout_radio_button, null, false);
                view.setOnClickListener(mOnClickListener);
                view.setId(i);
                ivCheckBox = view.findViewById(R.id.iv_check_box);
                tvCheckBox = view.findViewById(R.id.tv_check_box);
                tvCheckBox.setText(resolution.getTitle());
                //if (i == currentVideo.getIndexQuality())
                if (!check && (configResolution.equalsIgnoreCase(resolution.getKey()) || i == 0)) {
                    check = true;
                    checkView(ivCheckBox, tvCheckBox);
                }
                RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                radioGroup.addView(view, 0, layoutParams);
            }
        }

        bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Resources resources = context.getResources();
                if (bottomSheet != null && resources != null && bottomSheet.getWidth() > bottomSheet.getHeight()) {
                    BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                    int height = ScreenManager.getWidth(context) / 2;

                    if (bottomSheetBehavior != null)
                        if (height > bottomSheet.getHeight())
                            bottomSheetBehavior.setPeekHeight(bottomSheet.getHeight());
                        else
                            bottomSheetBehavior.setPeekHeight(height);
                }
            }
        });
    }

    private void checkView(ImageView ivCheckBox, TextView tvCheckBox) {
        ivCheckBox.setImageResource(R.drawable.ic_checkbox_video);
        tvCheckBox.setTextColor(ContextCompat.getColor(context, R.color.videoColorSelect));
    }

    @Override
    public void show() {
        super.show();
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mOnQualityVideoListener != null && radioGroup != null) {

                int position = radioGroup.indexOfChild(view);

                int idx = Math.min(Math.max(position, 0), resolutions.size() - 1);

                mOnQualityVideoListener.onQualityVideo(idx, currentVideo, resolutions.get(idx));
            }
            dismiss();
        }
    };

    public interface OnQualityVideoListener {
        void onQualityVideo(int idx, Video currentVideo, Resolution resolution);
    }
}

//package com.metfone.selfcare.ui;
//
//import android.widget.ImageView;
//
//
///**
// * Created by toanvk2 on 1/26/15.
// */
//public class ImageViewAwareTargetSize extends ImageViewAware {
//    private int targetWidthSize;
//    private int targetHeightSize;
//
//    public ImageViewAwareTargetSize(ImageView imageView) {
//        super(imageView);
//    }
//
//    public ImageViewAwareTargetSize(ImageView imageView, boolean checkActualViewSize) {
//        super(imageView, checkActualViewSize);
//    }
//
//    public ImageViewAwareTargetSize(ImageView imageView, int targetWidth, int targetHeight) {
//        super(imageView);
//        this.targetWidthSize = targetWidth;
//        this.targetHeightSize = targetHeight;
//    }
//
//    @Override
//    public int getWidth() {
//        if (targetWidthSize > 0) {
//            return targetWidthSize;
//        }
//        return super.getWidth();
//    }
//
//    @Override
//    public int getHeight() {
//        if (targetHeightSize > 0) {
//            return targetHeightSize;
//        }
//        return super.getHeight();
//    }
//}

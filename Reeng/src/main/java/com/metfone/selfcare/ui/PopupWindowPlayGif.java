package com.metfone.selfcare.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifDrawableBuilder;

/**
 * Created by toanvk on 1/6/2016.
 */
public class PopupWindowPlayGif extends PopupWindow implements AnimationListener {
    private static final String TAG = PopupWindowPlayGif.class.getSimpleName();
    private View popupView;
    private View parentView;
    private Drawable bg;
    private String filePath;
    private ImageView mImgGif;
    private GifDrawable gifDrawable;
    private ProgressLoading mProgressLoading;

    public PopupWindowPlayGif(View contentView) {
        super(contentView);
    }

    public static PopupWindowPlayGif newInstance(Context context, View parentView) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.popup_play_full_gif, null, false);
        PopupWindowPlayGif popup = new PopupWindowPlayGif(view);
        popup.parentView = parentView;
        popup.setWidth(ActionBar.LayoutParams.MATCH_PARENT);
        popup.setHeight(ActionBar.LayoutParams.MATCH_PARENT);
        popup.setFocusable(true);
        popup.setOutsideTouchable(true);
        popup.bg = ContextCompat.getDrawable(context, android.R.color.transparent);
        popup.setBackgroundDrawable(popup.bg);
        return popup;
    }

    @Override
    public void dismiss() {
        this.bg = null;
        if (gifDrawable != null) {
            gifDrawable.recycle();
            gifDrawable = null;
        }
        super.dismiss();
    }

    public void showPopup(String filePath) {
        this.filePath = filePath;
        popupView = initControl();
        setContentView(popupView);
        // setAnimationStyle(R.style.AnimationOverFlow);
        parentView.postDelayed(new Runnable() {
            @Override
            public void run() {
                showAtLocation(parentView, Gravity.TOP | Gravity.RIGHT, 0, 0);
            }
        }, 10);
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private View initControl() {
        long t = System.currentTimeMillis();
        View view = getContentView();
        mImgGif = (ImageView) view.findViewById(R.id.img_sticker_preview);
        mProgressLoading = (ProgressLoading) view.findViewById(R.id.progress_loading_sticker);
        mProgressLoading.setVisibility(View.GONE);
        Log.d(TAG, "filePath: " + filePath);
        try {
            gifDrawable = new GifDrawableBuilder().from(filePath).build();
            mImgGif.setImageDrawable(gifDrawable);
            gifDrawable.addAnimationListener(this);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
        Log.d(TAG, "initControl take : " + (System.currentTimeMillis() - t));
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dismiss();
                return false;
            }
        });
        return view;
    }

    @Override
    public void onAnimationCompleted(int loopNumber) {
        Log.d(TAG, "onAnimationCompleted : ");
        dismiss();
    }
}
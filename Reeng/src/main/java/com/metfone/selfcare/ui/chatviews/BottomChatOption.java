package com.metfone.selfcare.ui.chatviews;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.message.BottomChatOptionAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.message.TypingManager;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/28/2017.
 */

public class BottomChatOption extends PopupWindow implements View.OnClickListener,
        TextWatcher,
        RecyclerClickListener {
    private static final String TAG = BottomChatOption.class.getSimpleName();
    private View anchor;
    private BaseSlidingFragmentActivity mActivity;
    private Resources mRes;
    private int threadType = -1, threadId = -1;
    private boolean isStranger = false;
    private ArrayList<ItemContextMenu> listItems;
    private PositiveListener<ItemContextMenu> listener = null;
    private DismissListener dismissListener = null;
    private Drawable bg;
    private RecyclerView mRecyclerView;
    private LinearLayout.LayoutParams bottomParams;
    private View mViewTopDivider, mViewBottomPadding;
    private EditText mEdt;
    private ImageView mImgLocation, mImgVoiceMail, mImgShareContact, mImgShareFile;
    private BottomChatOptionAdapter mAdapter;
    private ApplicationController mApplication;
    private int optionHeight;

    public BottomChatOption(View contentView) {
        super(contentView);
    }

    public static BottomChatOption newInstance(BaseSlidingFragmentActivity activity, View anchor,
                                               PositiveListener<ItemContextMenu> callBack,
                                               DismissListener dismissListener) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.dialog_bottom_chat_option, null);
        BottomChatOption popup = new BottomChatOption(view);
        popup.mActivity = activity;
        popup.mRes = activity.getResources();
        popup.anchor = anchor;
        popup.dismissListener = dismissListener;
        popup.listener = callBack;
        popup.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setOutsideTouchable(true);
        popup.bg = ContextCompat.getDrawable(activity, R.drawable.transparent);
        //popup.bg = ContextCompat.getDrawable(activity, R.drawable.rec_gray_no_stroke);
        popup.setBackgroundDrawable(popup.bg);
        return popup;
    }

    public void setThreadType(int threadType) {
        this.threadType = threadType;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public void setStranger(boolean isStranger) {
        this.isStranger = isStranger;
    }

    public void setOptionHeight(int height) {
        this.optionHeight = height;
    }

    @Override

    public void dismiss() {
        Log.d(TAG, "dismiss");
        mEdt.clearFocus();
        dismissListener.onDismiss();
        bg = null;
        super.dismiss();
    }

    public void showPopup(boolean isViettel) {
        mApplication = (ApplicationController) mActivity.getApplicationContext();
        setContentView(initControl());
        listItems = getListItemsMenu(threadType, isViettel);
        setAdapter();
        //setAnimationStyle(R.style.AnimationOverFlow);
        //int offsetY = (int) ConvertHelper.dp2px(mActivity.getResources(), 0.5f);

        bottomParams.height = anchor.getHeight() + optionHeight;// + offsetY;
        /*int[] location = new int[2];
        anchor.getLocationOnScreen(location);
        Log.d(TAG, "showPopup: " + location[0] + " -- " + location[1] + " -optionHeight: " + optionHeight);*/

        showAtLocation(anchor, Gravity.BOTTOM, 0, 0);
        //showAsDropDown(anchor, 0, offsetY);
        /*if (Version.hasN()) {
            int[] location = new int[2];
            anchor.getLocationOnScreen(location);
            int Y = location[1] + anchor.getHeight();
            if (Version.hasNMR1()) { //Android 7.1 height match_parent
                int screenHeight = ((ApplicationController) mActivity.getApplicationContext()).getHeightPixels();
                setHeight(screenHeight - offsetY);
            }
            Log.d(TAG, "showPopup: " + Y + " -- " + offsetY);
            showAtLocation(anchor, Gravity.NO_GRAVITY, 0, Y + offsetY);
        } else {
            showAsDropDown(anchor, 0, offsetY);
        }*/
        setViewListener();
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */

    private View initControl() {
        View view = getContentView();
        mViewTopDivider = view.findViewById(R.id.bottom_chat_option_top_divider);
        mViewBottomPadding = view.findViewById(R.id.bottom_chat_option_bottom_padding);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.bottom_chat_option_recycler_view);
        mImgLocation = (ImageView) view.findViewById(R.id.bottom_chat_option_location);
        mImgVoiceMail = (ImageView) view.findViewById(R.id.bottom_chat_option_voice_mail);
        mImgShareContact = (ImageView) view.findViewById(R.id.bottom_chat_option_share_contact);
        mImgShareFile = (ImageView) view.findViewById(R.id.bottom_chat_option_share_file);
        mEdt = (EditText) view.findViewById(R.id.bottom_chat_option_edt);
        bottomParams = (LinearLayout.LayoutParams) mViewBottomPadding.getLayoutParams();
        /*if (mApplication.getReengAccountBusiness().isCambodia()) {
            mImgVoiceMail.setImageResource(R.drawable.selector_image_icon);
        }*/

        return view;
    }

    private void setAdapter() {
        if (listItems == null || listItems.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            mViewTopDivider.setVisibility(View.GONE);
        } else {
            mViewTopDivider.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
            if (mAdapter == null) {
                mAdapter = new BottomChatOptionAdapter(mApplication, this);
                mAdapter.setListItems(listItems);
                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
                mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL,
                        false));
            } else {
                mAdapter.setListItems(listItems);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View v, int pos, Object object) {
        dismiss();
        listener.onPositive((ItemContextMenu) object);
    }

    @Override
    public void onLongClick(View v, int pos, Object object) {

    }

    @Override
    public void onClick(View v) {
        dismiss();
        switch (v.getId()) {
            case R.id.bottom_chat_option_location:
                listener.onPositive(new ItemContextMenu(Constants.OPTION_LOCATION));
                break;
            case R.id.bottom_chat_option_voice_mail:
                /*if (mApplication.getReengAccountBusiness().isCambodia()) {
                    listener.onPositive(new ItemContextMenu(Constants.OPTION_SEND_IMAGE_CAMBODIA));
                } else*/
                listener.onPositive(new ItemContextMenu(Constants.OPTION_VOICE_MAIL));
                break;
            case R.id.bottom_chat_option_share_contact:
                listener.onPositive(new ItemContextMenu(Constants.OPTION_CONTACT));
                break;
            case R.id.bottom_chat_option_share_file:
                listener.onPositive(new ItemContextMenu(Constants.OPTION_FILE));
                break;
            case R.id.bottom_chat_option_bottom_padding:
                dismiss();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        dismiss();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void setViewListener() {
        mImgLocation.setOnClickListener(this);
        mImgShareContact.setOnClickListener(this);
        mImgVoiceMail.setOnClickListener(this);
        mImgShareFile.setOnClickListener(this);
        mViewBottomPadding.setOnClickListener(this);
        mEdt.requestFocus();
        mEdt.addTextChangedListener(this);
    }

    private ArrayList<ItemContextMenu> getListItemsMenu(int threadType, boolean isViettel) {
        ArrayList<ItemContextMenu> list = new ArrayList<>();
        ItemContextMenu music = new ItemContextMenu(mRes.getString(R.string.bottom_chat_listener),
                R.drawable.ic_bottom_chat_listener, TypingManager.KEY_TYPE_MUSIC + " ", Constants.OPTION_MUSIC);
        ItemContextMenu video = new ItemContextMenu(mRes.getString(R.string.bottom_chat_video),
                R.drawable.ic_bottom_chat_video, TypingManager.KEY_TYPE_VIDEO + " ", Constants.OPTION_VIDEO);
        ItemContextMenu zodiac = new ItemContextMenu(mRes.getString(R.string.bottom_chat_zodiac),
                R.drawable.ic_bottom_chat_zodiac, null, Constants.OPTION_ZODIAC);
        ItemContextMenu bPlus = new ItemContextMenu(mRes.getString(R.string.bottom_chat_plus),
                R.drawable.ic_bottom_chat_bplus, null, Constants.OPTION_BPLUS);
        ItemContextMenu giftLixi = new ItemContextMenu(mRes.getString(R.string.bottom_chat_gift_lixi),
                R.drawable.ic_bottom_chat_lixi, null, Constants.OPTION_GIFT_LIXI);
        ItemContextMenu location = new ItemContextMenu(mRes.getString(R.string.location),
                R.drawable.chat_ic_location, null, Constants.OPTION_LOCATION);
        ItemContextMenu contact = new ItemContextMenu(mRes.getString(R.string.tab_m_contact),
                R.drawable.chat_ic_contact, null, Constants.OPTION_CONTACT);
        ItemContextMenu eSport = new ItemContextMenu(mRes.getString(R.string.tab_m_esport),
                R.drawable.chat_ic_esport, null, Constants.OPTION_ESPORT);
        ItemContextMenu movie = new ItemContextMenu(mRes.getString(R.string.tab_movie),
                R.drawable.chat_ic_movie, null, Constants.OPTION_MOVIE);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            //list.add(music);
            if (mApplication.getConfigBusiness().isEnableWatchVideoTogether())
                list.add(video);
//            if (mApplication.getReengAccountBusiness().isVietnam())
            //list.add(zodiac);
            list.add(location);
            list.add(contact);
            list.add(eSport);
            if (!isStranger && mApplication.getConfigBusiness().isEnableBankplus()) {
                ThreadMessage thread = mApplication.getMessageBusiness().findThreadByThreadId(threadId);
                if (thread != null && isViettel) {
                    list.add(bPlus);
                }
                if (mApplication.getConfigBusiness().isEnableLixi()) {
                    list.add(0, giftLixi);
                }
            }
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT || threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            list.add(location);
//            list.add(music);
            if (mApplication.getConfigBusiness().isEnableWatchVideoTogether())
                list.add(video);
            if (mApplication.getConfigBusiness().isEnableLixi()) {
                list.add(0, giftLixi);
            }
            list.add(contact);
            list.add(eSport);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            ThreadMessage thread = mApplication.getMessageBusiness().findThreadByThreadId(threadId);
            if (thread != null && thread.getStateOnlineStar() == 1) {
                list.add(music);
            }
        }
        return list;
    }

    public static final class Constants {
        public static final int OPTION_LOCATION = 100;
        public static final int OPTION_VOICE_MAIL = 101;
        public static final int OPTION_CONTACT = 102;
        public static final int OPTION_MUSIC = 103;
        public static final int OPTION_VIDEO = 104;
        public static final int OPTION_ZODIAC = 105;
        public static final int OPTION_BPLUS = 106;
        public static final int OPTION_FILE = 107;
        public static final int OPTION_GIFT_LIXI = 108;
        public static final int OPTION_SEND_IMAGE_CAMBODIA = 109;
        public static final int OPTION_ESPORT = 110;
        public static final int OPTION_MOVIE = 111;
    }
}
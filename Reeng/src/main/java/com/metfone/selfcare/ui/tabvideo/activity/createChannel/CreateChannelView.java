package com.metfone.selfcare.ui.tabvideo.activity.createChannel;

import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.model.tab_video.Channel;

public interface CreateChannelView extends BaseView {
    void showMessage(int i);

    void showLoading();

    void hideLoading();

    void openChannel(Channel channel);

    void updateImage(String urlImage);

    void updateCoverImage(String urlImage);

    void updateTitleChannel(String name);

    void updateButtonSubmit();

    void updateDescriptionChannel(String description);

    void changeTitle();

    void back();
}

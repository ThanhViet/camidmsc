package com.metfone.selfcare.ui.dialog;

/**
 * Created by toanvk2 on 7/13/2017.
 */

public interface NegativeListener<T> {
    void onNegative(T result);
}

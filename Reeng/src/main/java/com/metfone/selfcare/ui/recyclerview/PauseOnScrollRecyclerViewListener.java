package com.metfone.selfcare.ui.recyclerview;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

/**
 * Created by thanhnt72 on 9/28/2016.
 */

public class PauseOnScrollRecyclerViewListener extends RecyclerView.OnScrollListener {

    private RequestManager imageLoader;

    private final boolean pauseOnScroll;
    private final boolean pauseOnSettling;
    private final RecyclerView.OnScrollListener externalListener;

    public PauseOnScrollRecyclerViewListener(Context activity, boolean pauseOnScroll, boolean pauseOnFling, RecyclerView.OnScrollListener customListener) {
        this(Glide.with(activity), pauseOnScroll, pauseOnFling, customListener);
    }

    public PauseOnScrollRecyclerViewListener(RequestManager requestManager, boolean pauseOnScroll, boolean pauseOnSettling) {
        this(requestManager, pauseOnScroll, pauseOnSettling, null);
    }

    public PauseOnScrollRecyclerViewListener(RequestManager requestManager, boolean pauseOnScroll, boolean pauseOnSettling,
                                             RecyclerView.OnScrollListener customListener) {
        this.imageLoader = requestManager;
        this.pauseOnScroll = pauseOnScroll;
        this.pauseOnSettling = pauseOnSettling;
        externalListener = customListener;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        switch (newState) {
            case RecyclerView.SCROLL_STATE_IDLE:
                imageLoader.resumeRequests();
                break;
            case RecyclerView.SCROLL_STATE_DRAGGING:
                if (pauseOnScroll) {
                    imageLoader.pauseRequests();
                }
                break;
            case RecyclerView.SCROLL_STATE_SETTLING:
                if (pauseOnSettling) {
                    imageLoader.pauseRequests();
                }
                break;
        }
        if (externalListener != null) {
            externalListener.onScrollStateChanged(recyclerView, newState);
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (externalListener != null) {
            externalListener.onScrolled(recyclerView, dx, dy);
        }
    }
}

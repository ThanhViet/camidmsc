package com.metfone.selfcare.ui;

public interface PopupListener {
    void dismissPopup();
}

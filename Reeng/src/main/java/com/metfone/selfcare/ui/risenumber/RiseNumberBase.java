package com.metfone.selfcare.ui.risenumber;

/**
 * Created by lee on 2014/7/29.
 */
public interface RiseNumberBase {
    void start();
    RiseNumberTextView withNumber(float number);
    RiseNumberTextView withNumber(int number);
    RiseNumberTextView setDuration(long duration);
    void setOnEnd(RiseNumberTextView.EndListener callback);
}

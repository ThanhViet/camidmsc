package com.metfone.selfcare.ui.tabvideo.channelDetail.main;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.AppCompatImageView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.callback.OnChannelInfoCallback;
import com.metfone.selfcare.common.api.video.channel.ChannelApi;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.activity.createChannel.CreateChannelActivity;
import com.metfone.selfcare.ui.tabvideo.channelDetail.ChannelPagerAdapter;
import com.metfone.selfcare.ui.tabvideo.channelDetail.info.InfoChannelFragment;
import com.metfone.selfcare.ui.tabvideo.channelDetail.movie.MovieChannelFragment;
import com.metfone.selfcare.ui.tabvideo.channelDetail.statistical.StatisticalChannelFragment;
import com.metfone.selfcare.ui.tabvideo.channelDetail.video.EventUploadVideo;
import com.metfone.selfcare.ui.tabvideo.channelDetail.video.VideoChannelFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.view.tab_video.SubscribeChannelLayout;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChannelDetailFragment extends BaseFragment implements OnChannelInfoCallback,
        SubscribeChannelLayout.SubscribeChannelListener,
        OnChannelChangedDataListener,
        OnInternetChangedListener {

    private static final String CHANNEL = "channel";

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.content)
    CoordinatorLayout content;
    @BindView(R.id.loading_view)
    ProgressLoading loadingView;
    @BindView(R.id.frame_loading)
    LinearLayout frameLoading;
    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    @BindView(R.id.iv_channel_cover)
    ImageView ivChannelCover;
    @BindView(R.id.iv_channel_avatar)
    RoundedImageView ivChannelAvatar;
    @BindView(R.id.tv_channel_name)
    TextView tvChannelName;
    @BindView(R.id.iv_channel_editor)
    TextView ivChannelEditor;
    @BindView(R.id.button_channel_subscription)
    SubscribeChannelLayout btnChannelSubscription;
    @BindView(R.id.tv_number_subscriptions)
    TextView tvNumberSubscriptions;
    @BindView(R.id.tv_number_video)
    TextView tvNumberVideo;
    @BindView(R.id.iv_upload_video)
    AppCompatImageView ivUploadVideo;
    @BindView(R.id.iv_channel_name)
    AppCompatImageView ivChannelName;
    Unbinder unbinder;
    private ListenerUtils listenerUtils;
    private ChannelApi channelApi;
    private Channel mChannel;
    private ArrayList<Fragment> fragments;
    private ArrayList<String> chars;
    private boolean isGetInfoSuccess = true;
    private boolean isShowUpload = false;

    public static ChannelDetailFragment newInstance(Channel channel) {
        Bundle args = new Bundle();
        args.putSerializable(CHANNEL, channel);
        ChannelDetailFragment fragment = new ChannelDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mChannel = (Channel) (bundle != null ? bundle.getSerializable(CHANNEL) : null);
        listenerUtils = application.getApplicationComponent().providerListenerUtils();
        channelApi = application.getApplicationComponent().providerChannelApi();
        listenerUtils.addListener(this);
        fragments = new ArrayList<>();
        chars = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channel_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        view.setOnClickListener(v -> {

        });
        initView();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listenerUtils.removerListener(this);
        unbinder.unbind();
    }

    private void initView() {
        hideError();
        showLoading();
        if (channelApi != null && mChannel != null)
            channelApi.getChannelInfo(mChannel.getId(), this);
    }

    @OnClick(R.id.iv_back)
    public void onIvBackClicked() {
        activity.onBackPressed();
    }

    @OnClick(R.id.iv_upload_video)
    public void onIvUploadClicked() {
        DialogUtils.onClickUpload(activity);
    }

    public void onIvShareClicked() {
        if (mChannel != null && Utilities.notEmpty(mChannel.getUrl())) {
            Utilities.shareLink(activity, mChannel.getUrl());
        }
    }

    @OnClick(R.id.iv_channel_editor)
    public void onIvChannelEditorClicked() {
        CreateChannelActivity.start(activity, mChannel);
    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        initView();
    }

    @Override
    public void onChannelCreate(Channel channel) {
    }

    @Override
    public void onChannelUpdate(Channel channel) {
        if (channel == null || !channel.equals(mChannel) || viewPager == null) return;
        mChannel = channel;
        bindChannelInfo(channel);
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        if (channel == null || !channel.equals(mChannel) || viewPager == null) return;
        mChannel.setNumFollow(channel.getNumfollow());
        mChannel.setFollow(channel.isFollow());
        bindSub(mChannel);
    }

    @Override
    public void onInternetChanged() {
        if (isGetInfoSuccess || !NetworkHelper.isConnectInternet(activity) || viewPager == null)
            return;
        isGetInfoSuccess = true;
        initView();
    }

    @Override
    public void onOpenApp(Channel channel, boolean isInstall) {
        Utilities.openApp(activity, channel.getPackageAndroid());
    }

    @Override
    public void onSub(Channel channel) {
        if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
            activity.showDialogLogin();
        else {
            if (listenerUtils == null || channelApi == null || viewPager == null) return;
            listenerUtils.notifyChannelSubscriptionsData(channel);
            channelApi.callApiSubOrUnsubChannel(channel.getId(), channel.isFollow());
        }
    }

    @Override
    public void onGetChannelInfoSuccess(Channel channel) {
        if (viewPager == null) return;
        isGetInfoSuccess = true;
        if (channel == null || Utilities.isEmpty(channel.getId())) {
            onGetChannelInfoError("");
            return;
        }
        mChannel = channel;
        hideError();
        bindTab(channel);
        bindChannelInfo(channel);
        content.setVisibility(View.VISIBLE);
        showBtnUpload();
    }

    @Override
    public void onGetChannelInfoError(String s) {
        if (viewPager == null) return;
        isGetInfoSuccess = false;
        showError();
        if (!NetworkHelper.isConnectInternet(activity)) {
            showErrorNetwork();
            hideErrorDataEmpty();
        } else {
            showErrorDataEmpty();
            hideErrorNetwork();
        }
    }

    @Override
    public void onGetChannelInfoComplete() {
        if (viewPager == null) return;
        hideLoading();
    }

    @SuppressWarnings("deprecation")
    private void bindChannelInfo(Channel channel) {
        if (channel == null) return;
        bindSub(channel);
//        SpannableString spannableString = new SpannableString(channel.getName().trim() + "  ");
        if (channel.getIsOfficial() == 1) {
//            Drawable myIcon = getResources().getDrawable(R.drawable.ic_checkbox_video);
//            int textSize = (int) tvChannelName.getTextSize() - 11;
//            myIcon.setBounds(0, 0, textSize, textSize);
//            spannableString.setSpan(new ImageSpan(myIcon, ImageSpan.ALIGN_BASELINE), spannableString.length() - 1, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            ivChannelName.setVisibility(View.VISIBLE);
        }else{
            ivChannelName.setVisibility(View.GONE);
        }

        tvTitle.setText(channel.getName());
        tvChannelName.setText(channel.getName().trim());
//        ImageManager.showImage(channel.getUrlImage(), ivChannelAvatar);
        ImageManager.showImageChannel(channel.getUrlImageCover(), ivChannelCover);
        ImageBusiness.setAvatarChannel(ivChannelAvatar, channel.getUrlImage());
    }

    private void bindSub(Channel channel) {
        if (channel == null) return;
        if (channel.isMyChannel()) {
            ivChannelEditor.setVisibility(View.VISIBLE);
        } else {
            btnChannelSubscription.setSubscribeChannelListener(this);
            btnChannelSubscription.setChannel(channel);
            ivChannelEditor.setVisibility(View.GONE);
        }
        tvNumberSubscriptions.setText(String.format(getString(R.string.people_subscription), channel.getTextFollow()));
        tvNumberVideo.setVisibility(View.VISIBLE);
        if (channel.getNumVideo() <= 1)
            tvNumberVideo.setText(getString(R.string.music_total_video, channel.getNumVideo()));
        else
            tvNumberVideo.setText(getString(R.string.music_total_videos, channel.getNumVideo()));
    }

    private void bindTab(Channel channel) {
        PagerAdapter channelPagerAdapter = providePagerAdapter(channel);
        viewPager.setAdapter(channelPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        if (channelPagerAdapter.getCount() > 3) {
            tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        } else {
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        }
        tabLayout.setupWithViewPager(viewPager);
    }

    private PagerAdapter providePagerAdapter(@Nullable Channel channel) {
        if (channel != null) {
            addFragment(getString(R.string.channel_detail_tab_video), VideoChannelFragment.newInstance(channel, true));
            if (channel.isHasFilmGroup())
                addFragment(getString(R.string.channel_detail_tab_movie), MovieChannelFragment.newInstance(channel));
            addFragment(getString(R.string.channel_detail_tab_info), InfoChannelFragment.newInstance(channel));
            if (channel.isMyChannel())
                addFragment(getString(R.string.channel_detail_tab_statistical), StatisticalChannelFragment.newInstance(channel));
        }
        ChannelPagerAdapter channelPagerAdapter = new ChannelPagerAdapter(getChildFragmentManager());
        channelPagerAdapter.bindData(fragments, chars);
        return channelPagerAdapter;
    }

    private void addFragment(String name, Fragment fragment) {
        fragments.add(fragment);
        chars.add(name);
    }

    private void showBtnUpload() {
        if (mChannel != null && mChannel.isMyChannel() && ivUploadVideo != null)
            ivUploadVideo.setVisibility(View.VISIBLE);
    }

    private void hideBtnUpload() {
        if (ivUploadVideo != null) ivUploadVideo.setVisibility(View.GONE);
    }

    private void showLoading() {
        if (frameLoading == null) return;
        frameLoading.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        if (frameLoading == null) return;
        frameLoading.setVisibility(View.GONE);
    }

    private void showErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.VISIBLE);
    }

    private void hideErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.GONE);
    }

    private void showErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.VISIBLE);
        emptyRetryText2.setVisibility(View.VISIBLE);
    }

    private void hideErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.GONE);
        emptyRetryText2.setVisibility(View.GONE);
    }

    private void showError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0 && mChannel != null && mChannel.isMyChannel() && !isShowUpload) {
                    showBtnUpload();
                } else {
                    hideBtnUpload();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(EventUploadVideo event) {
        if (event != null && event.isShowUpload()) {
            hideBtnUpload();
            isShowUpload = event.isShowUpload();
        }
    }
}

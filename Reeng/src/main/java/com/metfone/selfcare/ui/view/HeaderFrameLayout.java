package com.metfone.selfcare.ui.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Path;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.hoanganhtuan95ptit.shapeofview.ShapeOfView;
import com.hoanganhtuan95ptit.shapeofview.manager.ClipPathManager;
import com.metfone.selfcare.R;

public class HeaderFrameLayout extends ShapeOfView {

    private int heightRadius = 0;
    private int radius = 0;

    public HeaderFrameLayout(@NonNull Context context) {
        super(context);
        init();
    }

    public HeaderFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HeaderFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Resources resources = getResources();
        if (resources != null) {
            heightRadius = resources.getDimensionPixelSize(R.dimen.ab_home_padding_bottom);
            radius = resources.getDimensionPixelSize(R.dimen.ab_home_conner);
        }

        super.setClipPathCreator(new ClipPathManager.ClipPathCreator() {
            @Override
            public Path createClipPath(int width, int height) {
                final Path path = new Path();
                path.moveTo(0, 0);
                path.lineTo(0, height);
                path.quadTo(0, height - heightRadius, radius, height - heightRadius);
                path.lineTo(width - radius, height - heightRadius);
                path.quadTo(width, height - heightRadius, width, height);
                path.lineTo(width, 0);
                path.close();
                return path;
            }

            @Override
            public boolean requiresBitmap() {
                return false;
            }
        });
    }
}

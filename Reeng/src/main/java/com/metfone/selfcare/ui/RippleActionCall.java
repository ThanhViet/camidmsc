package com.metfone.selfcare.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/29/2016.
 */

public class RippleActionCall extends RelativeLayout implements View.OnTouchListener {
    private static final String TAG = RippleActionCall.class.getSimpleName();
    private static final int DEFAULT_DURATION_TIME = 300;
    private static final int DEFAULT_RADIUS = 40;
    private OnListener mListener;
    private int rippleColor;
    private int pressColor;
    private int rippleDuration;
    private int defaultRadius;
    private int maxRadius;

    private Paint paint;
    private AnimatorSet animatorSet;
    private ArrayList<Animator> animatorList;
    private Ripple mRipple, mRippleHold;
    private RelativeLayout.LayoutParams layoutParams;
    private boolean isPress = false;
    private boolean animationRunning = false;

    public RippleActionCall(Context context) {
        super(context);
        setOnTouchListener(this);
    }

    public RippleActionCall(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        setOnTouchListener(this);
    }

    public RippleActionCall(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
        setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                Log.i(TAG, "action down");
                v.setPressed(true);
                isPress = true;
                startRipple();
                if (mListener != null) mListener.onPress();
                break;
            case MotionEvent.ACTION_UP:
                Log.i(TAG, "action up");
                resetRipple(false);
                break;
            case MotionEvent.ACTION_MOVE:
                changeRippleHold(event);
                Log.i(TAG, "action move");
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.i(TAG, "action cancel");
                resetRipple(false);
                break;
            default:
                break;
        }
        return  super.onTouchEvent(event);
        //return true;// TODO disable ripple
    }

    private void init(Context context, AttributeSet attrs) {
        if (isInEditMode())
            return;
        if (attrs == null) {
            throw new IllegalArgumentException("Attributes should be provided to this view,");
        }
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RippleActionCall);
        // get attrs value
        pressColor = typedArray.getColor(R.styleable.RippleActionCall_ripple_holdColor, ContextCompat.getColor(context, R.color.ripple_answer_press));
        rippleColor = typedArray.getColor(R.styleable.RippleActionCall_ripple_color, ContextCompat.getColor(context, R.color.ripple_answer_background));
        rippleDuration = typedArray.getInt(R.styleable.RippleActionCall_ripple_duration, DEFAULT_DURATION_TIME);
        defaultRadius = typedArray.getDimensionPixelOffset(R.styleable.RippleActionCall_ripple_radius, DEFAULT_RADIUS);
        typedArray.recycle();
        // init pain
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        // init layout params
        layoutParams = new RelativeLayout.LayoutParams(100, 100);// default
        // init ripple view
        mRipple = new Ripple(getContext(), rippleColor, defaultRadius);
        addView(mRipple);
        // init ripple hold view
        mRippleHold = new Ripple(getContext(), pressColor, defaultRadius);
        addView(mRippleHold);
        changeParams(layoutParams.width, layoutParams.height);
    }

    public void setListener(OnListener listener) {
        this.mListener = listener;
    }

    public void changeParams(int width, int height) {
        layoutParams.width = width;
        layoutParams.height = height;
        maxRadius = width / 2;
        float scale = (float) maxRadius / (float) defaultRadius;
        mRipple.setLayoutParams(layoutParams);
        mRippleHold.setLayoutParams(layoutParams);
        animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorList = new ArrayList<>();
        // scale x
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(mRipple, "ScaleX", 1.0f, scale);
        scaleXAnimator.setRepeatCount(0);
        scaleXAnimator.setRepeatMode(ObjectAnimator.RESTART);
        scaleXAnimator.setDuration(rippleDuration);
        animatorList.add(scaleXAnimator);
        // scale y
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(mRipple, "ScaleY", 1.0f, scale);
        scaleYAnimator.setRepeatCount(0);
        scaleYAnimator.setRepeatMode(ObjectAnimator.RESTART);
        scaleYAnimator.setDuration(rippleDuration);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
    }

    private void startRipple() {
        if (!animationRunning) {
            mRipple.setVisibility(VISIBLE);
            animatorSet.start();
            mRippleHold.setVisibility(VISIBLE);
            animationRunning = true;
        }
    }

    private void changeRippleHold(MotionEvent event) {
        if (!isPress) return;
        int radius = getRadius(event);
        if (radius < defaultRadius) {
            radius = defaultRadius;
        } else if (radius > maxRadius) {
            radius = maxRadius;
        }
        mRippleHold.changeRadius(radius);
        if (radius >= maxRadius) {
            if (mListener != null)
                mListener.onClick();
            resetRipple(true);
        }
    }

    private void resetRipple(boolean isClick) {
        if (mRippleHold.getVisibility() == VISIBLE) {
            mRippleHold.setVisibility(GONE);
            mRippleHold.changeRadius(defaultRadius);
        }
        if (mRipple.getVisibility() == VISIBLE) {
            mRipple.setVisibility(GONE);
        }
        if (animationRunning) {
            animatorSet.end();
            animationRunning = false;
        }
        isPress = false;
        if (!isClick && mListener != null) {
            mListener.onCancel();
        }
    }

    private int getRadius(MotionEvent event) {
        int x = Math.abs((int) event.getX() - maxRadius);
        int y = Math.abs((int) event.getY() - maxRadius);
        return (int) Math.sqrt(x * x + y * y);
    }

    /**
     * Ripple view
     */
    private class Ripple extends View {
        private int radius;
        private int color;

        public Ripple(Context context) {
            super(context);
            this.setVisibility(View.INVISIBLE);
        }

        public Ripple(Context context, int color, int radius) {
            super(context);
            this.setVisibility(View.INVISIBLE);
            this.color = color;
            this.radius = radius;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            paint.setColor(color);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, paint);
        }

        public void changeRadius(int radius) {
            this.radius = radius;
            this.invalidate();
        }
    }

    public interface OnListener {
        void onPress();

        void onClick();

        void onCancel();
    }
}
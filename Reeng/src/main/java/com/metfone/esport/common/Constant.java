package com.metfone.esport.common;

import java.util.Locale;

/**
 * Created by Nguyễn Thành Chung on 8/20/20.
 */
public class Constant {
    public static final String KEY_DATA = "_DATA";
    public static final String KEY_TYPE = "_TYPE";
    public static final String KEY_LINK = "_LINK";
    public static final String KEY_FILE_PATH = "_FILE_PATH";
    public static final String KEY_TAB_ID = "_TAB_ID";
    public static final String KEY_ID = "_ID";
    public static final String KEY_ITEMS = "_ITEMS";
    public static final String KEY_TITLE = "_TITLE";
    public static final String KEY_DESC = "_DESC";
    public static final String KEY_THUMB = "_THUMB";
    public static final String CLIENT_TYPE = "Android";
    public static final String REVISION = "1";
    public static final Locale DEFAULT_LOCALE = Locale.US;
    public static final long TIME_OFF_SWIPE_REFRESH = 500L;
    public static final String DEFAULT_ENCODING = "UTF-8";

    public static final String PREF_USERNAME_ACCOUNT_ESPORT = "PREF_USERNAME_ACCOUNT_ESPORT";
    public static final String PREF_PASSWORD_ACCOUNT_ESPORT = "PREF_PASSWORD_ACCOUNT_ESPORT";
    public static final String PREF_OTP_ACCOUNT_ESPORT = "PREF_OTP_ACCOUNT_ESPORT";
    public static final String PREF_CACHE_AUTHEN_ESPORT = "PREF_CACHE_AUTHEN_ESPORT";
    public static final String PREF_CACHE_ACCOUNT_ESPORT = "PREF_CACHE_ACCOUNT_ESPORT";
    public static final String PREF_CACHE_ACCOUNT_RANK_ESPORT = "PREF_CACHE_ACCOUNT_RANK_ESPORT";
    public static final String PREF_CACHE_LIST_MY_CHANNEL_ESPORT = "PREF_CACHE_LIST_MY_CHANNEL_ESPORT";
    public static final String PREF_CACHE_CURRENT_CHANNEL_ESPORT = "PREF_CACHE_CURRENT_CHANNEL_ESPORT";

    public static final String PUBLIC_KEY_LIVE_COMMENT = "-----BEGIN PUBLIC KEY-----\n" +
            "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgF3g82nB1ImzAwSN7JXeOC\n" +
            "7wChDA\n" +
            "4Nbzun/2B60sB04LCxBt88yRQTK734ugqAJ9cnYYNjwYfzcoTmubiMygsdtoNf1H\n" +
            "TmezAL+ppsJxZ/TlfomXz6zUS2HxNUdNcgX0NdHpq5OR9713p6tiq5Z4TdYjja9P\n" +
            "7FEG8p4xf8snDEjhAgMBAAE=\n" +
            "-----END PUBLIC KEY-----";
    public static final String PUBLIC_KEY_API = "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgF3g82nB1ImzAwSN7JXeOC7wChDA" +
            "4Nbzun/2B60sB04LCxBt88yRQTK734ugqAJ9cnYYNjwYfzcoTmubiMygsdtoNf1H" +
            "TmezAL+ppsJxZ/TlfomXz6zUS2HxNUdNcgX0NdHpq5OR9713p6tiq5Z4TdYjja9P" +
            "7FEG8p4xf8snDEjhAgMBAAE=";

    public static final String  METFONE_NUMBER_REGEX = "^0?31[0-9]{7}$|^[+]?85531[0-9]{7}$|^0?60[0-9]{6}$|^[+]?85560[0-9]{6}$|^0?66[0-9]{6}$|" +
            "^[+]?85566[0-9]{6}$|^0?67[0-9]{6}$|^[+]?85567[0-9]{6}$|^0?68[0-9]{6}$|^[+]?85568[0-9]{6}$|" +
            "^0?71[0-9]{7}$|^[+]?85571[0-9]{7}$|^0?88[0-9]{7}$|^[+]?85588[0-9]{7}$|^0?90[0-9]{6}$|^[+]?85590[0-9]{6}$|^0?97[0-9]{7}$|^[+]?85597[0-9]{7}$";

    //Domain: api1camid.metfone.com.kh
    //Domain livecomment: ws://api2camid.metfone.com.kh/chatWS
    //Domain upload: uploadcamid.metfone.com.kh
    public static final String WS_ESPORT = "ws://api2camid.metfone.com.kh/chatWS/websocket";
    public static final String BASE_URL_ESPORT = "http://api1camid.metfone.com.kh/";
    public static final String BASE_URL_UPLOAD_ESPORT = "http://uploadcamid.metfone.com.kh/";
    //    public static final String BASE_URL_CAMID_ESPORT = "http://api1camid.metfone.com.kh/";
//    public static final String WS_ESPORT = "ws://apicamid2.keeng.net:8082/chatWS/websocket";
//    public static final String BASE_URL_ESPORT = "http://apicamid2.keeng.net:8081/";
//    public static final String BASE_URL_CAMID_ESPORT = "http://123.31.45.218:8856/";
    public static final String BASE_URL_CAMID_ESPORT = "https://openid.metfone.com.kh/";
    public static final String BASE_URL_VTC_API = "https://apigw.camid.app";

    public static final String BASE_URL_ESPORT_DONATE = "http://36.37.242.104:8123/";

    public interface FILTER {
        //(order 1:mới nhât, 2: cũ nhất, 3: nhiều view nhất, 4: ít view nhất)
        int NEWEST = 1;
        int OLDEST = 2;
        int MOST_VIEW = 3;
        int LEAST_VIEW = 4;
    }

    public interface TAG {
        int _HOME = 1;
        int _PLAYER_LIVE = 2;
        int _PLAYER_VOD = 3;
        int _PLAYER_BOARD_CAST = 4;
        int _UPLOAD_VIDEO = 5;
        int _SEARCH = 6;
        int _CHANNEL_DETAIL = 7;
        int _GAME_DETAIL = 8;
    }

    public interface REQUEST_CODE {
        int OPEN_CAMERA_FOR_UPLOAD_VIDEO = 100;
        int TAKE_PHOTO_CHANGE_AVATAR = 101;
        int TAKE_PHOTO_CHANGE_COVER = 102;
        int TAKE_PHOTO_CHANGE_ABOUT = 103;
        int OPEN_GALLERY_CHANGE_AVATAR = 104;
        int OPEN_GALLERY_CHANGE_COVER = 105;
        int OPEN_GALLERY_CHANGE_ABOUT = 106;
        int OPEN_GALLERY_CHOOSE_THUMB_UPLOAD_VIDEO = 107;
        int CHANGE_BRIGHTNESS = 108;
    }

    public interface MENU {
        int EXIT = 0;
        int TAKE_PHOTO = 1;
        int OPEN_GALLERY = 2;
        int SHARE_VIDEO = 3;
    }

    public static class DateFormat {
        public static final String ISO = "MM/dd/yyyy hh:mm:ss a";
        public static final String ISO_AM = "dd/MM/yyyy hh:mm a";
        public static final String ISO_V1 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        public static final String YEAR = "yyyy";
        public static final String DAY = "dd";
        public static final String MONTH = "MM";
        public static final String MONTH_YEAR = "LLLL, yyyy";
        public static final String YEAR_MONTH = "yyyy/MM";
        public static final String LONGMONTH_YEAR_VN = "MMMM/yyyy";
        public static final String LONGMONTH_YEAR_EN = "MMMM yyyy";
        public static final String YEAR_MONTH_DAY = "yyy-MM-dd";
        public static final String YEAR_MONTH_DAY_V2 = "yyyy/MM/dd";
        public static final String DAY_MONTH_YEAR = "dd/MM/yyyy";
        public static final String MONTH_DAY_YEAR = "MM/dd/yyyy";
        public static final String DATE_YEAR_MONTH_DAY = "EEEE, yyy-MM-dd";
        public static final String DATE_DAY_MONTH_YEAR = "EEEE, dd/MM/yyyy";
        public static final String DATE_MONTH_DAY_YEAR = "EEEE, MM/dd/yyyy";
        public static final String DAY_MONTH = "dd/MM";
        public static final String MONTH_DAY = "MM/dd";
        public static final String DAY_MONTH_YEAR_MINUS = "dd-MM-yyyy-HH-mm-ss";
        public static final String SQL = "yyyy-MM-dd HH:mm:ss";
        public static final String SQL_DATE = "yyyy-MM-dd";
        public static final String DDMMYYYY_HHMMSS = "dd/MM/yyyy HH:mm:ss";
        public static final String MMDDYYYY_HHMMSS = "MM/dd/yyyy HH:mm:ss";
        public static final String HHMM = "HH:mm";
        public static final String HHMMSS = "HH:mm:ss";
        public static final String DAY_MONTH_YEAR_HHMM = "dd/MM/yyyy HH:mm";
        public static final String HHMM_DAY_MONTH_YEAR = "HH:mm - dd/MM/yyyy";
        public static final String WEEKDAY = "EEEE";
        public static final String CONTACT_LIST_BIRTHDATE_FORMAT = "yyyyMMdd";
        public static final String DAY_MONTH_YEAR_V2 = "dd-MM-yyyy";
        public static final String MONTH_DAY_YEAR_H_M_S_A = "MMM dd, yyyy hh:mm:ss a";
        public static final String LONG_MONTH_DAY_YEAR = "MMMM dd, yyyy";
        public static final String DAY_MMM_YEAR = "dd-MMM-yyyy";

    }

    public static final boolean FLAG_AUTO_PLAY_ESPORT = true;
}

package com.metfone.esport.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupWindow;

import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.metfone.esport.entity.camid.CamIdUserResponse;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.request.UserInfoRequest;
import com.metfone.selfcare.R;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by Nguyễn Thành Chung on 8/20/20.
 */
public class Common {
    public static void handleException(Exception e) {
        try {
            e.printStackTrace();
            Log.f("Error", "handleException", e);
        } catch (Exception ex) {
            Log.e("Error Exception", Objects.requireNonNull(ex.getMessage()));
        }
    }

    public static Gson createGsonISODate() {
        return createGsonFormatDate(Constant.DateFormat.ISO);
    }

    public static Gson createGsonFormatDate(String formatDate) {
        GsonBuilder gsonb = new GsonBuilder();
        gsonb.setDateFormat(formatDate).setDateFormat(formatDate);
        return gsonb.create();
    }

    public static String getIpAddress(Context context){
        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        assert wm != null;
        return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
    }

    public static String convertDateToString(Date date, String format) {
        return convertDateToString(date, format, null);

    }

    public static String convertDateToString(Date date, String format, Locale locale) {
        if (date == null) {
            return "";
        }

        SimpleDateFormat dateFormat = getDateFormat(format, locale);

        return dateFormat.format(date);

    }

    public static Date convertStringToDate(String source, String format) {
        return convertStringToDate(source, format, null);
    }

    public static void setNavigationBarBlack(Activity activity) {
        if (activity == null || activity.isFinishing()) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setNavigationBarColor(ContextCompat.getColor(activity, R.color.view_live));
        }
    }

    public static Date convertStringToDate(String source, String format, Locale locale) {
        Date dt = null;

        try {
            if (!StringUtils.isTrimEmpty(source)) {
                SimpleDateFormat fm = getDateFormat(format, locale);

                dt = fm.parse(source);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dt;
    }

    public static SimpleDateFormat getDateFormat(String format, Locale locale) {
        SimpleDateFormat fm;

        if (locale != null) fm = new SimpleDateFormat(format, locale);

        else fm = new SimpleDateFormat(format, Locale.getDefault());

        return fm;
    }

    public static <T> T convertJsonToObject(String json, Class<T> classT) {
        return new Gson().fromJson(json, classT);
    }

    public static boolean checkNetworkWithToast(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            showToastError(context, context.getString(R.string.need_network));
            return false;
        }
    }

    public static void showToastError(Context context, String message) {
        if (StringUtils.isTrimEmpty(message)) return;
        ToastUtils.showShort(message);
        //new CustomToast(context, message, Toast.LENGTH_SHORT, CustomToast.ToastType.ERROR).show();
    }

    public static int getRandomColor(Context context) {
        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
        return androidColors[new Random().nextInt(androidColors.length)];
    }

    public static void dimBehind(PopupWindow popupWindow) {
        try {
            View container;
            if (popupWindow.getBackground() == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = (View) popupWindow.getContentView().getParent();
                } else {
                    container = popupWindow.getContentView();
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = (View) popupWindow.getContentView().getParent().getParent();
                } else {
                    container = (View) popupWindow.getContentView().getParent();
                }
            }
            Context context = popupWindow.getContentView().getContext();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            p.dimAmount = 0.5f;
            wm.updateViewLayout(container, p);
        } catch (Exception e) {
            handleException(e);
        }

    }

    public static void hideKeyBoard(Context context) {
        if (context instanceof Activity) {
            View v = ((Activity) context).getCurrentFocus();
            if (v != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    }

    public static int getStatusBarHeight(Context context) {
        if (context == null) return 0;
        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    public static void setFullStatusBar(Activity activity) {
        if (activity == null || activity.isFinishing()) return;
        activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= 21) {
            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public static String getMd5() {
        return AccountBusiness.getInstance().getUserId() + AccountBusiness.getInstance().getToken() + System.currentTimeMillis();
    }

    public static void setColorStatusBar(Activity activity, int color) {
        if (activity == null || activity.isFinishing()) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(color);
        }
    }

    public static String getViewLive(int view) {
        if (view > 1000000000) {
            return new DecimalFormat("#,#").format((double) (view / 1000000)) + "B";
        } else if (view > 1000000) {
            return new DecimalFormat("#,#").format((double) (view / 1000000)) + "M";
        } else if (view > 1000) {
            return new DecimalFormat("#,#").format(view / 1000) + "K";
        } else {
            return String.valueOf(view);
        }
    }

    public static String getViewLive(long view) {
        if (view > 1000000) {
            return new DecimalFormat("#,#").format((double) (view / 1000000)) + "M";
        } else if (view > 100000) {
            return new DecimalFormat("#,#").format(view / 100000) + "N";
        } else if (view > 1000) {
            return new DecimalFormat("#,#").format(view / 1000) + "K";
        } else {
            return String.valueOf(view);
        }
    }


    // slide the view from below itself to the current position
    public static void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public static void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public static void hideKeyBoard(Context context, EditText editText) {
        try {
            if (editText != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                editText.clearFocus();
            }
        } catch (Exception e) {
            handleException(e);
        }

    }

    public static String getNameAccount() {
        ChannelInfoResponse currentChannel = AccountBusiness.getInstance().getChannelInfo();
        CamIdUserResponse camIdUserResponse = AccountBusiness.getInstance().getUserInfo();
        if (currentChannel == null) {
            return camIdUserResponse.full_name == null ? "" : camIdUserResponse.full_name;
        } else {
            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                return camIdUserResponse.full_name == null ? "" : camIdUserResponse.full_name;
            } else {
                return currentChannel.getName();
            }
        }
    }

    public static String getNameCamId(){
        CamIdUserResponse camIdUserResponse = AccountBusiness.getInstance().getUserInfo();
        return camIdUserResponse.full_name == null ? "Profile Channel" : camIdUserResponse.full_name;
    }

    public static String getAvatarAccount() {
        ChannelInfoResponse currentChannel = AccountBusiness.getInstance().getChannelInfo();
        CamIdUserResponse camIdUserResponse = AccountBusiness.getInstance().getUserInfo();
        if (currentChannel == null) {
            return camIdUserResponse.avatar == null ? "test" : camIdUserResponse.avatar;
        } else {
            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                return camIdUserResponse.avatar == null ? "test" : camIdUserResponse.avatar;
            } else {
                return StringUtils.isEmpty(currentChannel.getUrlAvatar()) ? "test" : currentChannel.getUrlAvatar();
            }
        }
    }


}

package com.metfone.esport.util;

import com.blankj.utilcode.util.LogUtils;

public class Log {

    public static void setEnableLog(boolean enable) {
        LogUtils.getConfig().setLogSwitch(enable);
    }

    public static void i(String tag, String string) {
        LogUtils.log(LogUtils.I, tag, string);
    }

    public static void i(String tag, String string, Throwable throwable) {
        LogUtils.log(LogUtils.I, tag, string, throwable);
    }

    public static void e(String tag, String string) {
        LogUtils.log(LogUtils.E, tag, string);
    }

    public static void e(String tag, String string, Throwable throwable) {
        LogUtils.log(LogUtils.E, tag, string, throwable);
    }

    public static void e(String tag, Throwable throwable) {
        if (throwable != null)
            LogUtils.log(LogUtils.E, tag, throwable.getMessage(), throwable);
    }

    public static void d(String tag, String string) {
        LogUtils.log(LogUtils.D, tag, string);
    }

    public static void d(String tag, String string, Throwable throwable) {
        LogUtils.log(LogUtils.D, tag, string, throwable);
    }

    public static void v(String tag, String string) {
        LogUtils.log(LogUtils.V, tag, string);
    }

    public static void v(String tag, String string, Throwable throwable) {
        LogUtils.log(LogUtils.V, tag, string, throwable);
    }

    public static void w(String tag, String string) {
        LogUtils.log(LogUtils.W, tag, string);
    }

    public static void w(String tag, String string, Throwable throwable) {
        LogUtils.log(LogUtils.W, tag, string, throwable);
    }

    public static void f(String tag, String string) {
        LogUtils.log(LogUtils.FILE, tag, string);
    }

    public static void f(String tag, String string, Throwable throwable) {
        LogUtils.log(LogUtils.FILE, tag, string, throwable);
    }

    public static void sys(String tag, String string) {
        System.out.println(tag + "," + string);
    }

    public static void sys(String tag, String string, boolean isFile) {
        if (isFile)
            LogUtils.log(LogUtils.FILE, tag, string);
        System.out.println(tag + "," + string);
    }
}
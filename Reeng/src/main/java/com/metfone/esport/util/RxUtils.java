package com.metfone.esport.util;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.Utils;
import com.metfone.esport.base.BaseResponse;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.camid.CamIdBaseResponse;
import com.metfone.esport.entity.repsonse.CreateChannelResponse;
import com.metfone.esport.entity.repsonse.UploadImageResponse;
import com.metfone.selfcare.R;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class RxUtils {

    public static <T> Disposable async(CompositeDisposable compositeDisposable, Observable<BaseResponse<T>> observable, ICallBackResponse<T> iCallBackResponse) {
        try {
            if (!NetworkUtils.isConnected()) {
                if (iCallBackResponse != null) {
                    iCallBackResponse.onFail(null);

                    iCallBackResponse.onFinish();
                }

                return null;
            }

            if (iCallBackResponse != null)
                iCallBackResponse.onStart();

            Disposable disposable = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<BaseResponse<T>>() {
                        @Override
                        public void onNext(BaseResponse<T> response) {
                            if (iCallBackResponse != null) {
                                if (response.code == 200) {
                                    iCallBackResponse.onSuccess(response.data);
                                } else {
                                    iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (iCallBackResponse != null) {
                                iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));

                                iCallBackResponse.onFinish();
                            }
                        }

                        @Override
                        public void onComplete() {
                            if (iCallBackResponse != null)
                                iCallBackResponse.onFinish();
                        }
                    });

            compositeDisposable.add(disposable);

            return disposable;

        } catch (Exception e) {
            Common.handleException(e);
        }

        return null;
    }

    public static <T> Disposable asyncCamId(CompositeDisposable compositeDisposable, Observable<CamIdBaseResponse<T>> observable, ICallBackResponse<T> iCallBackResponse) {
        try {
            if (!NetworkUtils.isConnected()) {
                if (iCallBackResponse != null) {
                    iCallBackResponse.onFail(null);

                    iCallBackResponse.onFinish();
                }

                return null;
            }

            if (iCallBackResponse != null)
                iCallBackResponse.onStart();

            Disposable disposable = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<CamIdBaseResponse<T>>() {
                        @Override
                        public void onNext(CamIdBaseResponse<T> response) {
                            if (iCallBackResponse != null) {
                                if (response.code.equals("00")) {
                                    iCallBackResponse.onSuccess(response.data);
                                } else {
                                    iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (iCallBackResponse != null) {
                                iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));

                                iCallBackResponse.onError(e);

                                iCallBackResponse.onFinish();
                            }
                        }

                        @Override
                        public void onComplete() {
                            if (iCallBackResponse != null)
                                iCallBackResponse.onFinish();
                        }
                    });

            compositeDisposable.add(disposable);

            return disposable;

        } catch (Exception e) {
            Common.handleException(e);
        }

        return null;
    }

    public static void asyncCreateChannel(CompositeDisposable compositeDisposable, Observable<CreateChannelResponse> observable, ICallBackResponse<CreateChannelResponse> iCallBackResponse) {
        try {
            if (!NetworkUtils.isConnected()) {
                if (iCallBackResponse != null) {
                    iCallBackResponse.onFail(Utils.getApp().getString(R.string.error_internet_disconnect));

                    iCallBackResponse.onFinish();
                }

                return;
            }

            if (iCallBackResponse != null)
                iCallBackResponse.onStart();

            Disposable disposable = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<CreateChannelResponse>() {
                        @Override
                        public void onNext(CreateChannelResponse response) {
                            if (iCallBackResponse != null) {
                                if (response.code == 200) {
                                    iCallBackResponse.onSuccess(response);
                                } else {
                                    iCallBackResponse.onFail(response.desc);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (iCallBackResponse != null) {
                                iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));

                                iCallBackResponse.onFinish();
                            }
                        }

                        @Override
                        public void onComplete() {
                            if (iCallBackResponse != null)
                                iCallBackResponse.onFinish();
                        }
                    });

            compositeDisposable.add(disposable);

        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    public static void asyncUploadImage(CompositeDisposable compositeDisposable, Observable<UploadImageResponse> observable, ICallBackResponse<String> iCallBackResponse) {
        try {
            if (!NetworkUtils.isConnected()) {
                if (iCallBackResponse != null) {
                    iCallBackResponse.onFail(null);

                    iCallBackResponse.onFinish();
                }

                return;
            }

            if (iCallBackResponse != null)
                iCallBackResponse.onStart();

            Disposable disposable = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<UploadImageResponse>() {
                        @Override
                        public void onNext(UploadImageResponse response) {
                            if (iCallBackResponse != null) {
                                if (response.code == 200) {
                                    iCallBackResponse.onSuccess(response.path);
                                } else {
                                    iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (iCallBackResponse != null) {
                                iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));

                                iCallBackResponse.onFinish();
                            }
                        }

                        @Override
                        public void onComplete() {
                            if (iCallBackResponse != null)
                                iCallBackResponse.onFinish();
                        }
                    });

            compositeDisposable.add(disposable);

        } catch (Exception e) {
            Common.handleException(e);
        }

    }
    public static <T> Disposable asyncSimple(CompositeDisposable compositeDisposable, Observable<T> observable, ICallBackResponse<T> iCallBackResponse) {
        try {
            if (!NetworkUtils.isConnected()) {
                if (iCallBackResponse != null) {
                    iCallBackResponse.onFail(null);

                    iCallBackResponse.onFinish();
                }

                return null;
            }

            if (iCallBackResponse != null)
                iCallBackResponse.onStart();

            Disposable disposable = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<T>() {
                        @Override
                        public void onNext(T response) {
                            if (iCallBackResponse != null) {
                                iCallBackResponse.onSuccess(response);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (iCallBackResponse != null) {
                                iCallBackResponse.onFail(Utils.getApp().getString(R.string.service_error));

                                iCallBackResponse.onFinish();
                            }
                        }

                        @Override
                        public void onComplete() {
                            if (iCallBackResponse != null)
                                iCallBackResponse.onFinish();
                        }
                    });

            compositeDisposable.add(disposable);

            return disposable;

        } catch (Exception e) {
            Common.handleException(e);
        }

        return null;
    }

}

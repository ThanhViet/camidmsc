package com.metfone.esport.util;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.player.Jzvd;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;

public class AutoPlayHorizontalUtils {
    public static int positionInList = -1;

    private AutoPlayHorizontalUtils() {
    }

    public static void onScrollPlayVideo(RecyclerView recyclerView, int newPosition, boolean shouldPlaying) {
        Log.i("AutoPlayHorizontalUtils", "onScrollPlayVideo newPosition: " + newPosition + ", positionInList: " + positionInList);
        if (newPosition == positionInList || !Constant.FLAG_AUTO_PLAY_ESPORT) return;
        boolean hasPlaying = false;
        if (positionInList >= 0) { // Stop old autoplay
            try {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                View child = layoutManager.findViewByPosition(positionInList);
                if (child != null) {
                    View view = child.findViewById(R.id.video_player);
                    if (view instanceof Jzvd) {
                        Jzvd player = (Jzvd) view;
                        Log.d("AutoPlayHorizontalUtils", "stopAutoPlay mediaUrl: " + player.getCurrentUrl() + "\nplayerState: " + player.state);
                        View cover = child.findViewById(R.id.iv_cover);
                        View loading = child.findViewById(R.id.pb_loading);
                        View btnMute = child.findViewById(R.id.btn_mute);
                        if (cover != null) cover.setVisibility(View.VISIBLE);
                        if (loading != null) loading.setVisibility(View.GONE);
                        if (btnMute != null) {
                            btnMute.setSelected(ApplicationController.getApp().isEnableVolumeAutoPlay);
                            btnMute.setVisibility(View.GONE);
                        }
                        player.setVisibility(View.GONE);
                        player.reset();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!ApplicationController.getApp().isPlayingMini && positionInList >= 0 && shouldPlaying) {
            try {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                View child = layoutManager.findViewByPosition(newPosition);
                if (child != null) {
                    View view = child.findViewById(R.id.video_player);
                    if (view instanceof Jzvd) {
                        Jzvd player = (Jzvd) view;
                        Log.d("AutoPlayHorizontalUtils", "autoPlay mediaUrl: " + player.getCurrentUrl());
                        View cover = child.findViewById(R.id.iv_cover);
                        View loading = child.findViewById(R.id.pb_loading);
                        View btnMute = child.findViewById(R.id.btn_mute);
                        if (cover != null) cover.setVisibility(View.VISIBLE);
                        if (btnMute != null) {
                            btnMute.setSelected(ApplicationController.getApp().isEnableVolumeAutoPlay);
                            btnMute.setVisibility(View.GONE);
                        }
                        if (player.isCurrentUrlEmpty()) {
                            if (loading != null) loading.setVisibility(View.GONE);
                            player.setVisibility(View.GONE);
                            player.reset();
                        } else {
                            if (loading != null) loading.setVisibility(View.VISIBLE);
                            player.setVisibility(View.VISIBLE);
                            player.hideAllControl();
                            player.startVideo();
                            if (ApplicationController.getApp().isEnableVolumeAutoPlay) {
                                player.unmute();
                            } else player.mute();
                            hasPlaying = true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        positionInList = newPosition;

//        if (hasPlaying) {
//            positionInList = newPosition;
//        } else {
//            positionInList = -1;
//        }
    }

    public static void resumePlaying(RecyclerView recyclerView) {
        if (positionInList < 0 || recyclerView == null || recyclerView.getLayoutManager() == null || !Constant.FLAG_AUTO_PLAY_ESPORT || ApplicationController.getApp().isPlayingMini)
            return;
//        if (NetworkUtils.isWifiConnected()) {
        try {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            View child = layoutManager.findViewByPosition(positionInList);
            if (child != null) {
                View view = child.findViewById(R.id.video_player);
                if (view instanceof Jzvd) {
                    Jzvd player = (Jzvd) view;
                    Log.d("AutoPlayHorizontalUtils", "resumePlaying mediaUrl: " + player.getCurrentUrl() + "\nplayer state: " + player.state);
                    View cover = child.findViewById(R.id.iv_cover);
                    View loading = child.findViewById(R.id.pb_loading);
                    View btnMute = child.findViewById(R.id.btn_mute);
                    if (player.isCurrentUrlEmpty()) {
                        if (cover != null) cover.setVisibility(View.VISIBLE);
                        if (btnMute != null) btnMute.setVisibility(View.GONE);
                        if (loading != null) loading.setVisibility(View.GONE);
                        player.setVisibility(View.GONE);
                        player.reset();
                    } else {
                        player.setVisibility(View.VISIBLE);
                        player.hideAllControl();
                        if (btnMute != null)
                            btnMute.setSelected(ApplicationController.getApp().isEnableVolumeAutoPlay);
                        if (player.state == Jzvd.STATE_PAUSE || player.state == Jzvd.STATE_PLAYING) {
                            if (cover != null) cover.setVisibility(View.GONE);
                            if (btnMute != null) btnMute.setVisibility(View.VISIBLE);
                            if (loading != null) loading.setVisibility(View.GONE);
                            player.start();
                        } else {
                            if (cover != null) cover.setVisibility(View.VISIBLE);
                            if (btnMute != null) btnMute.setVisibility(View.GONE);
                            if (loading != null) loading.setVisibility(View.VISIBLE);
                            player.startVideo();
                        }
                        if (ApplicationController.getApp().isEnableVolumeAutoPlay)
                            player.unmute();
                        else
                            player.mute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stopPlaying(RecyclerView recyclerView) {
        if (positionInList < 0 || recyclerView == null || recyclerView.getLayoutManager() == null || !Constant.FLAG_AUTO_PLAY_ESPORT)
            return;
        if (positionInList >= 0) {
            try {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                View child = layoutManager.findViewByPosition(positionInList);
                if (child != null) {
                    View view = child.findViewById(R.id.video_player);
                    if (view instanceof Jzvd) {
                        Jzvd player = (Jzvd) view;
                        Log.d("AutoPlayHorizontalUtils", "stopPlaying mediaUrl: " + player.getCurrentUrl() + "\nplayer state: " + player.state);
                        View cover = child.findViewById(R.id.iv_cover);
                        View loading = child.findViewById(R.id.pb_loading);
                        View btnMute = child.findViewById(R.id.btn_mute);
                        if (cover != null) cover.setVisibility(View.VISIBLE);
                        if (loading != null) loading.setVisibility(View.GONE);
                        if (btnMute != null) {
                            btnMute.setSelected(ApplicationController.getApp().isEnableVolumeAutoPlay);
                            btnMute.setVisibility(View.GONE);
                        }
                        player.setVisibility(View.GONE);
                        player.reset();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void pausePlaying(RecyclerView recyclerView) {
        if (positionInList < 0 || recyclerView == null || recyclerView.getLayoutManager() == null || !Constant.FLAG_AUTO_PLAY_ESPORT)
            return;

        if (positionInList >= 0) {
            try {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                View child = layoutManager.findViewByPosition(positionInList);
                if (child != null) {
                    View view = child.findViewById(R.id.video_player);
                    if (view instanceof Jzvd) {
                        Jzvd player = (Jzvd) view;
                        Log.d("AutoPlayHorizontalUtils", "pausePlaying mediaUrl: " + player.getCurrentUrl() + "\nplayer state: " + player.state);
                        View cover = child.findViewById(R.id.iv_cover);
                        View loading = child.findViewById(R.id.pb_loading);
                        View btnMute = child.findViewById(R.id.btn_mute);
                        if (cover != null) cover.setVisibility(View.VISIBLE);
                        if (btnMute != null) btnMute.setVisibility(View.GONE);
                        if (loading != null) loading.setVisibility(View.GONE);
                        player.setVisibility(View.GONE);
                        player.pause();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

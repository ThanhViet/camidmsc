/*
 * Created by admin on 2020/9/3
 *
 */

package com.metfone.esport.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.IntentUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.esport.widgets.BottomSheetDialog;
import com.esport.widgets.recycler.CustomLinearLayoutManager;
import com.esport.widgets.recycler.DividerItemDecoration;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.player.Jzvd;
import com.metfone.esport.entity.ItemMenuEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.dialogs.BottomSheetAdapter;
import com.metfone.esport.ui.live.LiveVideoFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;

import org.json.JSONArray;

import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint("DefaultLocale")
public class Utilities {
    public static final long SECOND = 1000L;
    public static final long MINUTE = 60 * SECOND;
    public static final long HOUR = 60 * MINUTE;
    public static final long DAY = 24 * HOUR;
    public static final long WEEK = 7 * DAY;
    private static final String TAG = Utilities.class.getSimpleName();
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    private static final String RESULT_REGEX = "[-|+|:|.|_|+|;|'|$|@||/]";
    private static final String PARAM_APPEND_UUID = "uuid=";
    private static final String PARAM_APPEND_MCUID = "mcuid=";
    private static final String PARAM_APPEND_MCAPP = "mcapp=";

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static int dpToPixels(int dp, Resources res) {
        return (int) (res.getDisplayMetrics().density * dp + 0.5f);
    }

    /*
     * Convert string
     */
    public static String convert(String org) {
        // convert to VNese no sign. @haidh 2008
        char[] arrChar = org.toCharArray();
        char[] result = new char[arrChar.length];
        for (int i = 0; i < arrChar.length; i++) {
            switch (arrChar[i]) {
                case '\u00E1':
                case '\u00E0':
                case '\u1EA3':
                case '\u00E3':
                case '\u1EA1':
                case '\u0103':
                case '\u1EAF':
                case '\u1EB1':
                case '\u1EB3':
                case '\u1EB5':
                case '\u1EB7':
                case '\u00E2':
                case '\u1EA5':
                case '\u1EA7':
                case '\u1EA9':
                case '\u1EAB':
                case '\u1EAD':
                case '\u0203':
                case '\u01CE': {
                    result[i] = 'a';
                    break;
                }
                case '\u00E9':
                case '\u00E8':
                case '\u1EBB':
                case '\u1EBD':
                case '\u1EB9':
                case '\u00EA':
                case '\u1EBF':
                case '\u1EC1':
                case '\u1EC3':
                case '\u1EC5':
                case '\u1EC7':
                case '\u0207': {
                    result[i] = 'e';
                    break;
                }
                case '\u00ED':
                case '\u00EC':
                case '\u1EC9':
                case '\u0129':
                case '\u1ECB': {
                    result[i] = 'i';
                    break;
                }
                case '\u00F3':
                case '\u00F2':
                case '\u1ECF':
                case '\u00F5':
                case '\u1ECD':
                case '\u00F4':
                case '\u1ED1':
                case '\u1ED3':
                case '\u1ED5':
                case '\u1ED7':
                case '\u1ED9':
                case '\u01A1':
                case '\u1EDB':
                case '\u1EDD':
                case '\u1EDF':
                case '\u1EE1':
                case '\u1EE3':
                case '\u020F': {
                    result[i] = 'o';
                    break;
                }
                case '\u00FA':
                case '\u00F9':
                case '\u1EE7':
                case '\u0169':
                case '\u1EE5':
                case '\u01B0':
                case '\u1EE9':
                case '\u1EEB':
                case '\u1EED':
                case '\u1EEF':
                case '\u1EF1': {
                    result[i] = 'u';
                    break;
                }
                case '\u00FD':
                case '\u1EF3':
                case '\u1EF7':
                case '\u1EF9':
                case '\u1EF5': {
                    result[i] = 'y';
                    break;
                }
                case '\u0111': {
                    result[i] = 'd';
                    break;
                }
                case '\u00C1':
                case '\u00C0':
                case '\u1EA2':
                case '\u00C3':
                case '\u1EA0':
                case '\u0102':
                case '\u1EAE':
                case '\u1EB0':
                case '\u1EB2':
                case '\u1EB4':
                case '\u1EB6':
                case '\u00C2':
                case '\u1EA4':
                case '\u1EA6':
                case '\u1EA8':
                case '\u1EAA':
                case '\u1EAC':
                case '\u0202':
                case '\u01CD': {
                    result[i] = 'A';
                    break;
                }
                case '\u00C9':
                case '\u00C8':
                case '\u1EBA':
                case '\u1EBC':
                case '\u1EB8':
                case '\u00CA':
                case '\u1EBE':
                case '\u1EC0':
                case '\u1EC2':
                case '\u1EC4':
                case '\u1EC6':
                case '\u0206': {
                    result[i] = 'E';
                    break;
                }
                case '\u00CD':
                case '\u00CC':
                case '\u1EC8':
                case '\u0128':
                case '\u1ECA': {
                    result[i] = 'I';
                    break;
                }
                case '\u00D3':
                case '\u00D2':
                case '\u1ECE':
                case '\u00D5':
                case '\u1ECC':
                case '\u00D4':
                case '\u1ED0':
                case '\u1ED2':
                case '\u1ED4':
                case '\u1ED6':
                case '\u1ED8':
                case '\u01A0':
                case '\u1EDA':
                case '\u1EDC':
                case '\u1EDE':
                case '\u1EE0':
                case '\u1EE2':
                case '\u020E': {
                    result[i] = 'O';
                    break;
                }
                case '\u00DA':
                case '\u00D9':
                case '\u1EE6':
                case '\u0168':
                case '\u1EE4':
                case '\u01AF':
                case '\u1EE8':
                case '\u1EEA':
                case '\u1EEC':
                case '\u1EEE':
                case '\u1EF0': {
                    result[i] = 'U';
                    break;
                }

                case '\u00DD':
                case '\u1EF2':
                case '\u1EF6':
                case '\u1EF8':
                case '\u1EF4': {
                    result[i] = 'Y';
                    break;
                }
                case '\u0110':
                case '\u00D0':
                case '\u0089': {
                    result[i] = 'D';
                    break;
                }
                default:
                    result[i] = arrChar[i];
            }
        }
        return new String(result);
    }

    public static String fixPhoneNumbTo84(String str) {
        if (str == null || "".equals(str) || str.length() < 3)
            return "";
        /*String x = "0123456789";
        for (int i = 0; i < str.length(); i++) {
            if (x.indexOf("" + str.charAt(i)) < 0) {
                str = str.replace("" + str.charAt(i), "");
                i--;
            }
        }*/
        int i = 0;
        while (i < str.length())
            if (str.startsWith("084")) {
                str = str.substring(1);
            } else if (str.startsWith("0")) {
                str = "84" + str.substring(1);
            } else if (!str.startsWith("84")) {
                str = "84" + str;
            }

        return str.trim();
    }

    public static String fixPhoneNumb(String str) {
        String fixPhoneNumbTo84 = fixPhoneNumbTo84(str);
        if (fixPhoneNumbTo84.length() < 3) {
            return "";
        }
        return fixPhoneNumbTo84.substring(2);
    }

    public static String secondsToTimer(int allSeconds) {
        // Convert total duration into time
        int hours = (allSeconds / (60 * 60));
        int minutes = (allSeconds % (60 * 60)) / (60);
        int seconds = allSeconds - (hours * 3600 + minutes * 60);
        StringBuilder sb = new StringBuilder();
        if (hours > 0) {
            sb.append(twoDigit(hours)).append(':');
        }
        sb.append(twoDigit(minutes)).append(':');
        sb.append(twoDigit(seconds));
        return sb.toString();
    }

    static public String twoDigit(int d) {
        NumberFormat formatter = new DecimalFormat("#00");
        return formatter.format(d);
    }

    private static boolean isPhoneNumberStartWith0(String phoneNumber) {
        String startString = phoneNumber.substring(0, 1);
        return "0".equals(startString);
    }

    private static boolean isPhoneNumberStartWith84(String phoneNumber) {
        String startString = phoneNumber.substring(0, 2);
        Log.i(TAG, "startString: " + startString);
        return "84".equals(startString);
    }

    public static String convertPhoneNumberFromOnMedia(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return "";
        }
        if (isPhoneNumberStartWith0(phoneNumber)) {
            return phoneNumber;
        }
        if (isPhoneNumberStartWith84(phoneNumber)) {
            phoneNumber = phoneNumber.substring(2);
            return "0" + phoneNumber;
        } else {
            return "+" + phoneNumber;
        }
    }

    public static String hidenPhoneNumber(String sdt) {
        if (TextUtils.isEmpty(sdt)) {
            return "";
        }
        int leng = sdt.length();
        StringBuilder mPhone = new StringBuilder(sdt);
        if (leng >= 6) {
//            mPhone.replace(leng - 6, leng - 4, "***");  //ma hoa thanh dang 0984***398
            mPhone.replace(leng - 6, leng, "******");  //ma hoa thanh dang 0984******
        } else if (leng < 6 && leng >= 3) {
            mPhone.replace(leng - 3, leng, "***");
        } else {
            mPhone = new StringBuilder("***");
        }
        return mPhone.toString();
    }

    public static String shortenLongNumber(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here  //cai nay ko can
        if (value == Long.MIN_VALUE) return shortenLongNumber(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + shortenLongNumber(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        if (TextUtils.isEmpty(packageName)) return false;
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String convertQuery(String keySearch) {
        if (TextUtils.isEmpty(keySearch))
            keySearch = "";
        else {
            try {
                Pattern pattern = Pattern.compile(RESULT_REGEX);
                Matcher matcher = pattern.matcher(keySearch);
                if (matcher.find()) {
                    keySearch = matcher.replaceAll(" ");
                }
            } catch (Exception e) {
                Log.e(TAG, "convertQuery", e);
            }
        }
        return keySearch.trim();
    }

    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    public static <T> T getFistItem(List<T> list) {
        return list.get(0);
    }

    public static <T> T getLastItem(List<T> list) {
        return list.get(list.size() - 1);
    }

    public static <T> boolean notEmpty(List<T> list) {
        return !isEmpty(list);
    }

    public static <T> boolean isEmpty(List<T> list) {
        return list == null || list.isEmpty();
    }

    public static boolean isEmpty(JSONArray jsonArray) {
        return jsonArray == null || jsonArray.length() <= 0;
    }

    public static boolean notEmpty(JSONArray jsonArray) {
        return !isEmpty(jsonArray);
    }

    public static boolean notEmpty(String text) {
        return !isEmpty(text);
    }

    public static boolean isEmpty(String text) {
        return TextUtils.isEmpty(text);
    }

    public static boolean notEmpty(Editable editable) {
        return !isEmpty(editable);
    }

    public static boolean isEmpty(Editable editable) {
        return TextUtils.isEmpty(editable);
    }

    public static <T> boolean notNull(WeakReference<T> reference) {
        return reference != null && reference.get() != null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<String> getAppSignatures(Context context) {
        ArrayList<String> appCodes = new ArrayList<>();

        try {
            // Get all package signatures for the current package
            String packageName = context.getPackageName();
            PackageManager packageManager = context.getPackageManager();
            Signature[] signatures = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;

            // For each signature create a compatible hash
            for (Signature signature : signatures) {
                String hash = getKeyHash(context, packageName, signature.toCharsString());
                if (hash != null) appCodes.add(String.format("%s", hash));

            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Unable to find package to obtain hash.", e);
        }
        return appCodes;
    }

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static String getKeyHash(Context context, String packageName, String signature) {
        String appInfo = packageName + " " + signature;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(appInfo.getBytes(StandardCharsets.UTF_8));
            byte[] hashSignature = messageDigest.digest();

            // truncated into NUM_HASHED_BYTES
            hashSignature = Arrays.copyOfRange(hashSignature, 0, 9);

            // encode into Base64
            String base64Hash = Base64.encodeToString(hashSignature, Base64.NO_PADDING | Base64.NO_WRAP);
            base64Hash = base64Hash.substring(0, 11);

            Log.e(TAG, String.format("pkg: %s -- hash: %s", packageName, base64Hash));
            return base64Hash;

        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "hash:NoSuchAlgorithm", e);
        }
        return null;
    }

    public static String getTotalView(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here  //cai nay ko can
        if (value == Long.MIN_VALUE) return shortenLongNumber(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + shortenLongNumber(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();
        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static void setMargins(View view, int marginLeft, int marginTop, int marginRight, int marginBottom) {
        if (view != null && view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(marginLeft, marginTop, marginRight, marginBottom);
            view.requestLayout();
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
//        bm.recycle();
        return resizedBitmap;
    }

    public static boolean isNotShowActivityCall(Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                && !Settings.canDrawOverlays(context);
    }

    public static void insertPhoneContact(String displayName, String number, Context context) {
        /*context is App or Activity*/
        ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
        int contactIndex = operations.size();
        //Newly Inserted contact
        // A raw contact will be inserted ContactsContract.RawContacts table in contacts database.
        operations.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)//Step1
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        //Display name will be inserted in ContactsContract.Data table
        operations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)//Step2
                .withValueBackReference(ContactsContract.RawContacts.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.RawContacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, displayName)
                .build());

        //Mobile number will be inserted in ContactsContract.Data table
        operations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)//Step 3
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.RawContacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());
        boolean isSuccess = false;
        try {
            // We will do batch operation to insert all above data
            //Contains the output of the app of a ContentProviderOperation.
            //It is sure to have exactly one of uri or count set
            ContentProviderResult[] contentProvider = context.getContentResolver()
                    .applyBatch(ContactsContract.AUTHORITY, operations); //apply above data insertion into contacts list
            Log.e("insertPhoneContact", "result: " + contentProvider);
            isSuccess = contentProvider != null && contentProvider.length > 0;
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("insertPhoneContact displayName: " + displayName + ", number:  " + number + ", isSuccess: " + isSuccess);
    }

    public static Bitmap getImageCaptcha(String str) {
        Bitmap bitmap = null;
        byte[] data = Base64.decode(str, Base64.DEFAULT);
        bitmap = BitmapFactory.decodeByteArray(data,
                0, data.length);
        return bitmap;
    }

    public static void openPlayer(Activity activity, VideoInfoResponse model) {
        openPlayer(activity, model, false);
        //if (activity instanceof LiveVideoActivity) activity.finish();
    }

    public static void openPlayer(Activity activity, VideoInfoResponse model, boolean isRedStatusBarOnTiny) {
        if (activity == null || activity.isFinishing() || model == null) return;
        Jzvd.releaseAllVideos();
        if (activity instanceof BaseSlidingFragmentActivity) {
            SharedPrefs.getInstance().put(Constants.PREFERENCE.BACK_DEVICE,true);
            ((BaseSlidingFragmentActivity) activity).popFragment();
            ((BaseSlidingFragmentActivity) activity).addFragment(LiveVideoFragment.newInstance(model, isRedStatusBarOnTiny), true, LiveVideoFragment.class.getSimpleName());
        }
        //if (activity instanceof LiveVideoActivity) activity.finish();
        ApplicationController.self().getFirebaseEventBusiness().logPlayEsport(model.title, String.valueOf(model.id));
    }

    public static void showKeyBoard(Context context, EditText editText){
        editText.clearFocus();
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void showPopupChooseCameraOrGallery(FragmentActivity activity, int requestCodeTakePhoto, int requestCodeOpenGallery) {
        if (activity == null || activity.isFinishing()) return;
        ArrayList<ItemMenuEntity> items = new ArrayList<>();
        ItemMenuEntity item;
        item = new ItemMenuEntity(activity.getString(R.string.take_photo), R.drawable.ic_camera_option_es, null, Constant.MENU.TAKE_PHOTO);
        items.add(item);
        item = new ItemMenuEntity(activity.getString(R.string.open_gallery), R.drawable.ic_gallery_option_es, null, Constant.MENU.OPEN_GALLERY);
        items.add(item);
        item = new ItemMenuEntity(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option_es, null, Constant.MENU.EXIT);
        items.add(item);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_bottom_sheet, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        BottomSheetAdapter adapter = new BottomSheetAdapter(activity, items);
        adapter.setListener((actionTag, item1) -> {
            if (actionTag == Constant.MENU.TAKE_PHOTO) {
                takePhoto(activity, requestCodeTakePhoto);
            } else if (actionTag == Constant.MENU.OPEN_GALLERY) {
                openGallery(activity, requestCodeOpenGallery);
            }
            dialog.dismiss();
        });
        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(activity, LinearLayout.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        recyclerView.setLayoutManager(layoutManager);
        if (recyclerView.getItemDecorationCount() <= 0)
            recyclerView.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void openGallery(FragmentActivity activity, int requestCode) {
        PermissionUtils.permission(PermissionConstants.STORAGE)
                .rationale((activity1, shouldRequest) -> {
                    shouldRequest.again(true);
                })
                .callback(new PermissionUtils.SimpleCallback() {
                    @Override
                    public void onGranted() {
                        IntentUtils.openGallery(activity, requestCode);
                    }

                    @Override
                    public void onDenied() {
                        ToastUtils.showShort(R.string.msg_storage_permission);
                    }
                }).request();
    }

    public static void takePhoto(FragmentActivity activity, int requestCode) {
        PermissionUtils.permission(PermissionConstants.CAMERA)
                .rationale((activity1, shouldRequest) -> {
                    shouldRequest.again(true);
                })
                .callback(new PermissionUtils.SimpleCallback() {
                    @Override
                    public void onGranted() {
                        IntentUtils.takePhoto(activity, requestCode);
                    }

                    @Override
                    public void onDenied() {
                        ToastUtils.showShort(R.string.msg_camera_permission);
                    }
                }).request();
    }

    public static String calculateTime(Context context, long time) {
        if (time <= 0) return "";
        long delta = System.currentTimeMillis() - time;
        if (delta < 0) {
            return context.getString(R.string.last_minute, 1);
        } else if (delta < WEEK) {

            if (delta > DAY) {
                int num = (int) (delta / DAY);
                if (num == 1) return context.getString(R.string.last_day, num);
                else return context.getString(R.string.last_days, num);
            }
            if (delta > HOUR) {
                int num = (int) (delta / HOUR);
                if (num == 1) return context.getString(R.string.last_hour, num);
                else return context.getString(R.string.last_hours, num);
            }
            if (delta > MINUTE) {
                int num = (int) (delta / MINUTE);
                if (num == 1) return context.getString(R.string.last_minute, num);
                else return context.getString(R.string.last_minutes, num);
            }
            return context.getString(R.string.last_minute, 1);

        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        return sdfDate.format(time);
    }
}
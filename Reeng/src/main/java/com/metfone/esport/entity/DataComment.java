package com.metfone.esport.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyễn Thành Chung on 10/12/20.
 */
public class DataComment {

    @SerializedName("item_name")
    @Expose
    public String itemName;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("img_url")
    @Expose
    public String imgUrl;
    @SerializedName("site")
    @Expose
    public String site;
    @SerializedName("img_status")
    @Expose
    public String imgStatus;
    @SerializedName("stampId")
    @Expose
    public Long stampId;
    @SerializedName("action_type")
    @Expose
    public String actionType;
    @SerializedName("share")
    @Expose
    public Integer share;
    @SerializedName("numfollow")
    @Expose
    public Integer numfollow;
    @SerializedName("channel_name")
    @Expose
    public String channelName;
    @SerializedName("ispublic")
    @Expose
    public Integer ispublic;
    @SerializedName("post_action_from")
    @Expose
    public String postActionFrom;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("media_url")
    @Expose
    public String mediaUrl;
    @SerializedName("channel_avatar")
    @Expose
    public String channelAvatar;
    @SerializedName("content_url")
    @Expose
    public String contentUrl;
    @SerializedName("user_type")
    @Expose
    public Integer userType;
    @SerializedName("channel_id")
    @Expose
    public Integer channelId;
    @SerializedName("content_action")
    @Expose
    public String contentAction;
    @SerializedName("comment")
    @Expose
    public Integer comment;
    @SerializedName("like")
    @Expose
    public Integer like;
    @SerializedName("msisdn")
    @Expose
    public String msisdn;
    @SerializedName("description")
    @Expose
    public String description;
}

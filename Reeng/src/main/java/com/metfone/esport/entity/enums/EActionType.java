package com.metfone.esport.entity.enums;

/**
 * Created by Nguyễn Thành Chung on 10/4/20.
 */
public enum EActionType {
    COMMENT,
    SHARE,
    LIKE,
    UNLIKE
}

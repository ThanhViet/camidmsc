package com.metfone.esport.entity.event;

/**
 * Created by Nguyễn Thành Chung on 12/21/20.
 */
public class OnSendCommentLiveEvent {
    public String comment;

    public OnSendCommentLiveEvent(String comment) {
        this.comment = comment;
    }
}

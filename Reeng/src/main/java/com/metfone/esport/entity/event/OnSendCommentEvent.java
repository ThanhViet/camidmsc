package com.metfone.esport.entity.event;

/**
 * Created by Nguyễn Thành Chung on 10/22/20.
 */
public class OnSendCommentEvent {
    public String comment;

    public OnSendCommentEvent(String comment) {
        this.comment = comment;
    }
}

package com.metfone.esport.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyễn Thành Chung on 10/4/20.
 */
public class ContentParam {
    @SerializedName("site")
    @Expose
    public String site = "";
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("tags")
    @Expose
    public String tags = "";
    @SerializedName("item_type")
    @Expose
    public String itemType = "item_type";
    @SerializedName("item_sub_type")
    @Expose
    public String itemSubType = "sub type";
    @SerializedName("item_name")
    @Expose
    public String itemName = "";
    @SerializedName("item_cat")
    @Expose
    public String itemCat = "";
    @SerializedName("item_id")
    @Expose
    public String itemId = "";
    @SerializedName("ref_src")
    @Expose
    public String refSrc = "";
    @SerializedName("singer")
    @Expose
    public String singer = "";
    @SerializedName("author")
    @Expose
    public String author = "";
    @SerializedName("img_url")
    @Expose
    public String imgUrl = "";
    @SerializedName("media_url")
    @Expose
    public String mediaUrl = "";
    @SerializedName("description")
    @Expose
    public String description = "";
    @SerializedName("ispublic")
    @Expose
    public Integer ispublic = 0;
    @SerializedName("like")
    @Expose
    public Integer like;
    @SerializedName("share")
    @Expose
    public Integer share;
    @SerializedName("comment")
    @Expose
    public Integer comment;
    @SerializedName("total_action")
    @Expose
    public Integer totalAction;
    @SerializedName("img_status")
    @Expose
    public String imgStatus = "";
    @SerializedName("left_label")
    @Expose
    public String leftLabel = "";
    @SerializedName("left_deeplink")
    @Expose
    public String leftDeeplink = "";
    @SerializedName("right_label")
    @Expose
    public String rightLabel = "";
    @SerializedName("right_deeplink")
    @Expose
    public String rightDeeplink="";
    @SerializedName("desc_deeplink")
    @Expose
    public String descDeeplink = "";
    @SerializedName("stamp")
    @Expose
    public Integer stamp;
    @SerializedName("is_vip")
    @Expose
    public String isVip = "";
    @SerializedName("sub_desc_deeplink")
    @Expose
    public String subDescDeeplink = "";
    @SerializedName("numfollow")
    @Expose
    public Integer numfollow;
    @SerializedName("isMyChannel")
    @Expose
    public Integer isMyChannel;
}

package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyễn Thành Chung on 9/17/20.
 */
public class DataChannelInfo {
    @SerializedName("channelInfo")
    @Expose
    private ChannelInfoEntity channelInfo;
}

package com.metfone.esport.entity.enums;

import java.text.DecimalFormat;

public enum ViewsUnitEnum {
    THOUSAND(1000.0, "K"),
    MILLION(1000000.0, "M"),
    BILLION(1000000000.0, "B");

    public Double ratio;
    public String unitString;

    ViewsUnitEnum(Double ratio, String unitString) {
        this.ratio = ratio;
        this.unitString = unitString;
    }

    public static String getTextDisplay(Number number) {
        if (number == null || number.intValue() <= 0 || number.longValue() <= 0) return "0";
        try {
            Double doubleValue = number.doubleValue();

            if (doubleValue > BILLION.ratio) {
                return formatNumber(doubleValue / BILLION.ratio) + BILLION.unitString;
            } else if (doubleValue > MILLION.ratio) {
                return formatNumber(doubleValue / MILLION.ratio) + MILLION.unitString;
            } else if (doubleValue > THOUSAND.ratio) {
                return formatNumber(doubleValue / THOUSAND.ratio) + THOUSAND.unitString;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.valueOf(number);
    }

    public static String formatNumber(Number number) {
        DecimalFormat df = new DecimalFormat("#,###.#");
        df.setMinimumFractionDigits(0);
        df.setMaximumFractionDigits(1);

        return df.format(number);
    }
}

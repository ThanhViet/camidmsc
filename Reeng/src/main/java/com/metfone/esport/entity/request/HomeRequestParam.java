package com.metfone.esport.entity.request;

import com.google.gson.annotations.SerializedName;

public class HomeRequestParam {
    @SerializedName("userId")
    public Long userId = 100L;
    @SerializedName("domain")
    public String domain = "apicamid2.keeng.net";
    @SerializedName("clientType")
    public String clientType = "Android";
    @SerializedName("revision")
    public Integer revision = 1;
    @SerializedName("languageCode")
    public String languageCode = "vi-Vn";
}

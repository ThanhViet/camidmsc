package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.esport.entity.StreamerModel;
import com.metfone.esport.entity.request.ChannelInfoRequest;

import java.util.List;

/**
 * Created by Nguyễn Thành Chung on 9/20/20.
 */
public class ListCommentEntity {
    @SerializedName("base64RowID")
    @Expose
    public String base64RowID;
    @SerializedName("stamp")
    @Expose
    public Long stamp;
    @SerializedName("current_time")
    @Expose
    public Long currentTime;
    @SerializedName("userInfo")
    @Expose
    public UserInfoEntity userInfo;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("tags")
    @Expose
    public List<Object> tags = null;
    @SerializedName("number_comment")
    @Expose
    public Integer numberComment;
    @SerializedName("number_like")
    @Expose
    public Integer numberLike;
    @SerializedName("is_like")
    @Expose
    public Integer isLike;

    @SerializedName("idcmt")
    private String id;
    @SerializedName("rowId")
    private String rowId;
    @SerializedName("idRep")
    private String idRep;
    @SerializedName("type")
    private int type;
    @SerializedName("timestamp")
    private long timestamp;
    @SerializedName("timeServer")
    private long timeServer;
    @SerializedName("countLike")
    private long countLike;
    @SerializedName("levelMessage")
    private int levelMessage;
    @SerializedName("userId")
    private String userId;
    @SerializedName("from")
    private String nameSender;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("message")
    private String content = "";
    @SerializedName("roomId")
    private String roomId;
    @SerializedName("streamer")
    private StreamerModel user;
    @SerializedName("data")
    private String data;
    @SerializedName("msisdn")
    private String msisdn;
    @SerializedName("channelInfo")
    private ChannelInfoRequest channelInfo;

    public ListCommentEntity() {
    }



    public ListCommentEntity(UserInfoEntity userInfo, String status) {
        this.userInfo = userInfo;
        this.status = status;
    }

    public ChannelInfoRequest getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(ChannelInfoRequest channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getBase64RowID() {
        return base64RowID;
    }

    public void setBase64RowID(String base64RowID) {
        this.base64RowID = base64RowID;
    }

    public Long getStamp() {
        return stamp;
    }

    public void setStamp(Long stamp) {
        this.stamp = stamp;
    }

    public Long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Long currentTime) {
        this.currentTime = currentTime;
    }

    public UserInfoEntity getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoEntity userInfo) {
        this.userInfo = userInfo;
    }

    public String getStatus() {
        if (status == null) status = "";
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public Integer getNumberComment() {
        return numberComment;
    }

    public void setNumberComment(Integer numberComment) {
        this.numberComment = numberComment;
    }

    public Integer getNumberLike() {
        return numberLike;
    }

    public void setNumberLike(Integer numberLike) {
        this.numberLike = numberLike;
    }

    public Integer getIsLike() {
        return isLike;
    }

    public void setIsLike(Integer isLike) {
        this.isLike = isLike;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getIdRep() {
        return idRep;
    }

    public void setIdRep(String idRep) {
        this.idRep = idRep;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimeServer() {
        return timeServer;
    }

    public void setTimeServer(long timeServer) {
        this.timeServer = timeServer;
    }

    public long getCountLike() {
        return countLike;
    }

    public void setCountLike(long countLike) {
        this.countLike = countLike;
    }

    public int getLevelMessage() {
        return levelMessage;
    }

    public void setLevelMessage(int levelMessage) {
        this.levelMessage = levelMessage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNameSender() {
        return nameSender;
    }

    public void setNameSender(String nameSender) {
        this.nameSender = nameSender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public StreamerModel getUser() {
        return user;
    }

    public void setUser(StreamerModel user) {
        this.user = user;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}

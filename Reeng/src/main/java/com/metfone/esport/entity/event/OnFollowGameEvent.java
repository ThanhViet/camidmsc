package com.metfone.esport.entity.event;

import com.metfone.esport.entity.repsonse.GameInfoResponse;

/**
 * Created by Nguyễn Thành Chung on 10/12/20.
 */
public class OnFollowGameEvent {
    public GameInfoResponse game;

    public OnFollowGameEvent(GameInfoResponse game) {
        this.game = game;
    }
}

package com.metfone.esport.entity.enums;

import com.metfone.selfcare.R;

public enum SortEnum {
    MANUAL(R.string.manual),
    MOST_POPULAR(R.string.most_popular),
    DATE_ADDED_NEWEST(R.string.date_added_newest),
    DATE_ADDED_OLDEST(R.string.date_added_oldest),
    VIEWERS_HIGHEST(R.string.viewrs_highest),
    VIEWERS_LOWEST(R.string.viewrs_lowest);

    public int textId;

    SortEnum(int textId) {
        this.textId = textId;
    }
}

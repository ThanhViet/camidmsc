package com.metfone.esport.entity.vtcapi;

import com.google.gson.annotations.SerializedName;

public class VTCAccountRankDTOResponse {
    @SerializedName("accountRankId")
    public Long accountRankId;
    @SerializedName("vtAccId")
    public Long vtAccId;
    @SerializedName("rankId")
    public Integer rankId;
    @SerializedName("rankName")
    public String rankName;
    @SerializedName("startDate")
    public String startDate;
    @SerializedName("endDate")
    public String endDate;
    @SerializedName("pointValue")
    public Object pointValue;
    @SerializedName("pointExpireDate")
    public Object pointExpireDate;
}

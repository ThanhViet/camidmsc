package com.metfone.esport.entity.repsonse;

/**
 * Created by Nguyễn Thành Chung on 9/29/20.
 */
public class UploadImageResponse {
    public int code;
    public String message;
    public String path;
}

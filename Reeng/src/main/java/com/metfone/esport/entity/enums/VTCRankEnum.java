package com.metfone.esport.entity.enums;

import com.metfone.selfcare.R;

public enum VTCRankEnum {
    Member(1, R.drawable.ic_av_member, R.drawable.ic_rank_member, R.string.kh_user_rank_member),
    Silver(2, R.drawable.ic_av_silver, R.drawable.ic_rank_silver, R.string.kh_user_rank_silver),
    //    Gold(R.drawable.ic_av_gold, R.drawable.ic_rank_gold, R.string.kh_user_rank_gold),
    Gold(3, R.drawable.ic_av_gold, R.drawable.ic_cinema_grade_gold, R.string.kh_user_rank_gold),
    Diamond(4, R.drawable.ic_av_diamon, R.drawable.ic_rank_diamon, R.string.kh_user_rank_diamon),
    Platinum(5, R.drawable.ic_av_diamon, R.drawable.ic_rank_diamon, R.string.kh_user_rank_platinum),
    ;
    public int _id;
    public int resAvatar;
    public int resRank;
    public int resRankName;

    VTCRankEnum(int id, int resAvatar, int resRank, int resRankName) {
        this._id = id;
        this.resAvatar = resAvatar;
        this.resRank = resRank;
        this.resRankName = resRankName;
    }

    public static VTCRankEnum getById(int id) {
        for (VTCRankEnum e : values()) {
            if (e._id == id) return e;
        }
        return Member;
    }

//    public enum RewardRank {
//        MEMBER(1, R.drawable.ic_rank_member_2),
//        SILVER(2, R.drawable.ic_rank_silver_2),
//        GOLD(3, R.drawable.ic_rank_gold_2),
//        DIAMOND(4, R.drawable.ic_rank_diamond),
//        PLATINUM(5, R.drawable.ic_rank_platinum);
//        public final int id;
//        public final Drawable drawable;
//
//        RewardRank(int id, @DrawableRes int color) {
//            this.id = id;
//            this.drawable = ResourceUtils.getDrawable(color);
//        }
//
//        public static RewardRank getById(int id) {
//            for (RewardRank e : values()) {
//                if (e.id == id) return e;
//            }
//            return null;
//        }
//    }
}

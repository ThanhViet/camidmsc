/*
 * Created by admin on 2020/5/25
 *
 */

package com.metfone.esport.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public final class StreamerModel implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("username")
    private String username;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("cover")
    private String cover;
    @SerializedName("isFollow")
    private int isFollow;
    @SerializedName("totalFollow")
    private int totalFollow;
    @SerializedName("total_view")
    private int totalViews;
    @SerializedName("des")
    private String description;
    @SerializedName("isLive")
    private int isLive;
    @SerializedName("gamePlay")
    private String gamePlay;
    @SerializedName("totalVideo")
    private int totalVideo;
    @SerializedName("totalEarn")
    private Long totalEarn;
    @SerializedName("totalDonate")
    private Long totalDonate;
    @SerializedName("isStreamer")
    private int isStreamer;

    public StreamerModel() {

    }

    public StreamerModel(int id, String username, String avatar, String cover, int isFollow, int totalFollow, int totalViews, String description, int isLive, String gamePlay, int totalVideo, Long totalEarn, Long totalDonate, int isStreamer) {
        this.id = id;
        this.username = username;
        this.avatar = avatar;
        this.cover = cover;
        this.isFollow = isFollow;
        this.totalFollow = totalFollow;
        this.totalViews = totalViews;
        this.description = description;
        this.isLive = isLive;
        this.gamePlay = gamePlay;
        this.totalVideo = totalVideo;
        this.totalEarn = totalEarn;
        this.totalDonate = totalDonate;
        this.isStreamer = isStreamer;
    }

    public final int getId() {
        return this.id;
    }

    public final void setId(int var1) {
        this.id = var1;
    }

    public final String getUsername() {
        return this.username;
    }

    public final void setUsername(String var1) {
        this.username = var1;
    }

    public final String getAvatar() {
        return this.avatar;
    }

    public final void setAvatar(String var1) {
        this.avatar = var1;
    }

    public final String getCover() {
        return this.cover;
    }

    public final void setCover(String var1) {
        this.cover = var1;
    }

    public final int isFollow() {
        return this.isFollow;
    }

    public final void setFollow(int var1) {
        this.isFollow = var1;
    }

    public final int getTotalFollow() {
        return this.totalFollow;
    }

    public final void setTotalFollow(int var1) {
        this.totalFollow = var1;
    }

    public final int getTotalViews() {
        return this.totalViews;
    }

    public final void setTotalViews(int var1) {
        this.totalViews = var1;
    }

    public final String getDescription() {
        return this.description;
    }

    public final void setDescription(String var1) {
        this.description = var1;
    }

    public final int isLive() {
        return this.isLive;
    }

    public final void setLive(int var1) {
        this.isLive = var1;
    }

    public final String getGamePlay() {
        return this.gamePlay;
    }

    public final void setGamePlay(String var1) {
        this.gamePlay = var1;
    }

    public final int getTotalVideo() {
        return this.totalVideo;
    }

    public final void setTotalVideo(int var1) {
        this.totalVideo = var1;
    }

    public final Long getTotalEarn() {
        return this.totalEarn;
    }

    public final void setTotalEarn(Long var1) {
        this.totalEarn = var1;
    }

    public final Long getTotalDonate() {
        return this.totalDonate;
    }

    public final void setTotalDonate(Long var1) {
        this.totalDonate = var1;
    }

    public final int isStreamer() {
        return this.isStreamer;
    }

    public final void setStreamer(int var1) {
        this.isStreamer = var1;
    }

}

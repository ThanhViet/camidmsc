package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GameCalendarResponse implements Serializable {
    @SerializedName("title")
    public String title;

    @SerializedName("type")
    public String type;

    @SerializedName("lstCalenders")
    public ArrayList<GameMatchResponse> lstCalenders;

    @SerializedName("lstTournamentDto")
    public ArrayList<TournamentInfoResponse> lstTournamentDto;

    public GameCalendarResponse() {
    }
}

package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TournamentInfoResponse {
    @SerializedName("id")
    public Long id;

    @SerializedName("name")
    public String name;

    @SerializedName("prize")
    public long price;

    @SerializedName("startDate")
    public String startDate;

    @SerializedName("endDate")
    public String endDate;

    @SerializedName("totalParticipants")
    public Integer totalParticipants;

    @SerializedName("location")
    public String location;

    @SerializedName("isHome")
    public Integer isHome;

    @SerializedName("active")
    public Integer active;

    @SerializedName("avatar")
    public String avatar;

    @SerializedName("listParticipant")
    public ArrayList<ParticipantResponse> listParticipant;

}

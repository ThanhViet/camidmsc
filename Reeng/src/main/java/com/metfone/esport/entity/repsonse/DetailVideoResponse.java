package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.esport.entity.ResolutionVideo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nguyễn Thành Chung on 9/10/20.
 */
public class DetailVideoResponse implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("duration")
    @Expose
    public String duration;
    @SerializedName("durationS")
    @Expose
    public Integer durationS;
    @SerializedName("original_path")
    @Expose
    public String originalPath;
    @SerializedName("image_path")
    @Expose
    public String imagePath;
    @SerializedName("image_path_thumb")
    @Expose
    public String imagePathThumb;
    @SerializedName("image_path_small")
    @Expose
    public String imagePathSmall;
    @SerializedName("publish_time")
    @Expose
    public Long publishTime;
    @SerializedName("is_like")
    @Expose
    public Integer isLike;
    @SerializedName("is_share")
    @Expose
    public Integer isShare;
    @SerializedName("is_follow")
    @Expose
    public Integer isFollow;
    @SerializedName("link")
    @Expose
    public String link;
    @SerializedName("aspecRatio")
    @Expose
    public String aspecRatio;
    @SerializedName("videoType")
    @Expose
    public String videoType;
    @SerializedName("total_like")
    @Expose
    public Integer totalLike;
    @SerializedName("total_view")
    @Expose
    public Integer totalView;
    @SerializedName("total_unlike")
    @Expose
    public Integer totalUnlike;
    @SerializedName("total_share")
    @Expose
    public Integer totalShare;
    @SerializedName("total_comment")
    @Expose
    public Integer totalComment;
    @SerializedName("numfollow")
    @Expose
    public Integer numfollow;
    @SerializedName("exclusive")
    @Expose
    public Integer exclusive;
    @SerializedName("active")
    @Expose
    public Integer active;
    @SerializedName("gameId")
    @Expose
    public Integer gameId;
    @SerializedName("gameInfo")
    @Expose
    public GameInfoEntity gameInfoEntity;
    @SerializedName("channelInfo")
    @Expose
    public ChannelInfoEntity channelInfo;
    @SerializedName("ccu")
    @Expose
    public Integer ccu;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("list_resolution")
    @Expose
    public List<ResolutionVideo> listResolution = null;
    @SerializedName("isLive")
    @Expose
    public Integer isLive;
    @SerializedName("timeToPlay")
    @Expose
    public Integer timeToPlay;
    @SerializedName("isSaved")
    @Expose
    public Integer isSaved;
    @SerializedName("channel_id")
    @Expose
    public Integer channel_id;

    @SerializedName("type_video")
    @Expose
    public int type_video;

    public boolean isLiveVideo() {
        return isLive == 1;
    }
}

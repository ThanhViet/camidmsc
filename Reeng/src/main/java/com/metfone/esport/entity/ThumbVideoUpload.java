/*
 * Copyright (c) admin created on 2020-09-12 11:25:37 PM
 */

package com.metfone.esport.entity;

import android.graphics.Bitmap;

import java.io.Serializable;

public class ThumbVideoUpload implements Serializable {
    public static final int TYPE_CHOOSE_PHOTO = 1;
    public static final int TYPE_BITMAP = 2;
    public static final int TYPE_FILE_PATH = 3;
    public static final int TYPE_FILE_URL = 4;

    int type;
    Bitmap bitmap;
    String filePath;
    public boolean isSelect = false;

    public ThumbVideoUpload(int type) {
        this.type = type;
    }

    public ThumbVideoUpload(int type, String filePath) {
        this.type = type;
        this.filePath = filePath;
    }

    public ThumbVideoUpload(int type, Bitmap bitmap) {
        this.type = type;
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

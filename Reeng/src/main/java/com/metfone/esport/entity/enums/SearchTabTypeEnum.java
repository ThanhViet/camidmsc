package com.metfone.esport.entity.enums;

public enum SearchTabTypeEnum {
    ALL(0),
    LIVE_STREAMING(1),
    PAST_BROADCAST(2),
    UPLOADED(3),
    GAME(4),
    TOURNAMENT(5),
    CHANNEL(6);

    public int position;

    SearchTabTypeEnum(int position) {
        this.position = position;
    }

    public static SearchTabTypeEnum enumOf(int postion) {
        for (SearchTabTypeEnum value : values()) {
            if (value.position == postion) return value;
        }

        return ALL;
    }
}

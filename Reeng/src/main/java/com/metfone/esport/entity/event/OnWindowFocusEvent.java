package com.metfone.esport.entity.event;

/**
 * Created by Nguyễn Thành Chung on 12/9/20.
 */
public class OnWindowFocusEvent {
    private boolean hasFocus;

    public OnWindowFocusEvent(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    public boolean isHasFocus() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }
}

package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UploadVideoResponse implements Serializable {
    @SerializedName("errorCode")
    public int errorCode;
    @SerializedName("desc")
    public String desc;
    @SerializedName("lavatar")
    public String avatar;
    @SerializedName("itemvideo")
    public String itemVideo;

    public UploadVideoResponse() {
    }
}

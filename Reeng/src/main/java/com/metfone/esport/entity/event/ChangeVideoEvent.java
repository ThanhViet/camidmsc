package com.metfone.esport.entity.event;

import com.metfone.esport.entity.repsonse.VideoInfoResponse;

/**
 * Created by Nguyễn Thành Chung on 10/12/20.
 */
public class ChangeVideoEvent {
    public VideoInfoResponse response;

    public ChangeVideoEvent(VideoInfoResponse response) {
        this.response = response;
    }
}

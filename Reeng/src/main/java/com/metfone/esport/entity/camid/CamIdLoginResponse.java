package com.metfone.esport.entity.camid;

import com.google.gson.annotations.SerializedName;

public class CamIdLoginResponse {
    @SerializedName("access_token")
    public String access_token;
    @SerializedName("expires_in")
    public Long expires_in;
    @SerializedName("refresh_token")
    public String refresh_token;
}

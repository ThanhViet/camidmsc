package com.metfone.esport.entity.event;

import android.net.Uri;

import java.io.File;

/**
 * Created by Nguyễn Thành Chung on 10/22/20.
 */
public class CreateChannelEvent {
    public Uri fileAvatar;

    public CreateChannelEvent(Uri fileAvatar) {
        this.fileAvatar = fileAvatar;
    }
}

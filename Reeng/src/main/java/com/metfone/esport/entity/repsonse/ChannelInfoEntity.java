package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Nguyễn Thành Chung on 9/10/20.
 */
public class ChannelInfoEntity implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("avatar")
    @Expose
    public String avatar;
    @SerializedName("cover")
    @Expose
    public String cover;
    @SerializedName("active")
    @Expose
    public Integer active;
    @SerializedName("receiveAlert")
    @Expose
    public Integer receiveAlert;
    @SerializedName("isFollow")
    @Expose
    public Integer isFollow;
    @SerializedName("totalFollow")
    @Expose
    public Integer totalFollow;
    @SerializedName("isLive")
    @Expose
    public Integer isLive;
    @SerializedName("totalVideo")
    @Expose
    public Integer totalVideo;
    @SerializedName("isStreamer")
    @Expose
    public Integer isStreamer;
    @SerializedName("totalEarn")
    @Expose
    public Integer totalEarn;
    @SerializedName("totalDonate")
    @Expose
    public Integer totalDonate;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("categoryId")
    @Expose
    public Integer categoryId;
    @SerializedName("urlAvatar")
    @Expose
    public String urlAvatar;
    @SerializedName("urlImagesCover")
    @Expose
    public String urlImagesCover;
    @SerializedName("urlImages")
    @Expose
    public String urlImages;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getReceiveAlert() {
        return receiveAlert;
    }

    public void setReceiveAlert(Integer receiveAlert) {
        this.receiveAlert = receiveAlert;
    }

    public Integer getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(Integer isFollow) {
        this.isFollow = isFollow;
    }

    public Integer getTotalFollow() {
        return totalFollow;
    }

    public void setTotalFollow(Integer totalFollow) {
        this.totalFollow = totalFollow;
    }

    public Integer getIsLive() {
        return isLive;
    }

    public void setIsLive(Integer isLive) {
        this.isLive = isLive;
    }

    public Integer getTotalVideo() {
        return totalVideo;
    }

    public void setTotalVideo(Integer totalVideo) {
        this.totalVideo = totalVideo;
    }

    public Integer getIsStreamer() {
        return isStreamer;
    }

    public void setIsStreamer(Integer isStreamer) {
        this.isStreamer = isStreamer;
    }

    public Integer getTotalEarn() {
        return totalEarn;
    }

    public void setTotalEarn(Integer totalEarn) {
        this.totalEarn = totalEarn;
    }

    public Integer getTotalDonate() {
        return totalDonate;
    }

    public void setTotalDonate(Integer totalDonate) {
        this.totalDonate = totalDonate;
    }

    public String getName() {
        if (name == null) name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getUrlAvatar() {
        if (urlAvatar == null) urlAvatar = "";
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public String getUrlImagesCover() {
        return urlImagesCover;
    }

    public void setUrlImagesCover(String urlImagesCover) {
        this.urlImagesCover = urlImagesCover;
    }

    public String getUrlImages() {
        return urlImages;
    }

    public void setUrlImages(String urlImages) {
        this.urlImages = urlImages;
    }
}

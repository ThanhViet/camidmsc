package com.metfone.esport.entity.event;

import com.metfone.esport.entity.repsonse.ChannelInfoResponse;

/**
 * Created by Nguyễn Thành Chung on 10/12/20.
 */
public class OnFollowChannelEvent {
    public ChannelInfoResponse channel;

    public OnFollowChannelEvent(ChannelInfoResponse channel) {
        this.channel = channel;
    }
}

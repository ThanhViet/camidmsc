package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SearchAllResponse implements Serializable {
    @SerializedName("title")
    public String title;

    @SerializedName("type")
    public String type;

    @SerializedName("dataChannel")
    public ArrayList<ChannelInfoResponse> dataChannel;

    @SerializedName("listVideo")
    public ArrayList<VideoInfoResponse> listVideo;

    @SerializedName("listGame")
    public ArrayList<GameInfoResponse> listGame;

    @SerializedName("lstTournamentDto")
    public ArrayList<TournamentInfoResponse> lstTournamentDto;

    public SearchAllResponse() {
    }
}

package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GameInfoResponse implements Serializable {
    @SerializedName("id")
    public Long id;

    @SerializedName("gameName")
    public String gameName;

    @SerializedName("gameDesc")
    public String gameDesc;

    @SerializedName("gameSlug")
    public String gameSlug;

    @SerializedName("platform")
    public String platform;

    @SerializedName("gameImage")
    public String gameImage;

    @SerializedName("getGameTagIds")
    public String getGameTagIds;

    @SerializedName("isFollow")
    public int isFollow;

    @SerializedName("totalView")
    public int totalView;

    @SerializedName("totalFollow")
    public int totalFollow;

    @SerializedName("isLive")
    public int isLive;

    @SerializedName("active")
    public int active;

    @SerializedName("gameStatus")
    public String gameStatus;

    @SerializedName("ccu")
    public int ccu;

    public GameInfoResponse() {
    }

    public GameInfoResponse(Long id, String gameName, String gameDesc, String gameSlug, String platform, String gameImage, String getGameTagIds, Integer isFollow, Integer totalView, Integer totalFollow, Integer isLive, Integer active, String gameStatus, Integer ccu) {
        this.id = id;
        this.gameName = gameName;
        this.gameDesc = gameDesc;
        this.gameSlug = gameSlug;
        this.platform = platform;
        this.gameImage = gameImage;
        this.getGameTagIds = getGameTagIds;
        this.isFollow = isFollow;
        this.totalView = totalView;
        this.totalFollow = totalFollow;
        this.isLive = isLive;
        this.active = active;
        this.gameStatus = gameStatus;
        this.ccu = ccu;
    }
}

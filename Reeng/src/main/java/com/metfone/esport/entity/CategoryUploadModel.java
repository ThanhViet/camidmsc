/*
 * Created by admin on 2020/9/7
 */

package com.metfone.esport.entity;

import java.io.Serializable;

public class CategoryUploadModel implements Serializable {
    int id;
    String title;
    boolean isSelected;

    public CategoryUploadModel() {
    }

    public CategoryUploadModel(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public CategoryUploadModel(int id, String title, boolean isSelected) {
        this.id = id;
        this.title = title;
        this.isSelected = isSelected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

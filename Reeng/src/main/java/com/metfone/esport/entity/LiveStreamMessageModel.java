package com.metfone.esport.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public final class LiveStreamMessageModel implements Serializable {
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_SAY_HI = 1;
    public static final int TYPE_FRIEND_WATCH = 2;
    public static final int TYPE_LIKE_COMMENT = 3;
    public static final int TYPE_LIKE_VIDEO = 3;
    public static final int TYPE_FOLLOW_CHANNEL = 4;
    public static final int TYPE_COUNT_LIVE = 5;
    public static final int TYPE_UNLIKE_COMMENT = 6;
    public static final int TYPE_UNSUBSCRIBE = 7;
    public static final int LEVEL_MESSAGE_1 = 1;
    public static final int LEVEL_MESSAGE_2 = 2;
    public static final int TYPE_DONATE = 10;
    public static final int TYPE_LIKE_HEART = 11;
    @SerializedName("idcmt")
    private String id;
    @SerializedName("rowId")
    private String rowId;
    @SerializedName("idRep")
    private String idRep;
    @SerializedName("type")
    private int type;
    @SerializedName("timestamp")
    private long timestamp;
    @SerializedName("timeServer")
    private long timeServer;
    @SerializedName("isLike")
    private int isLike;
    @SerializedName("countLike")
    private long countLike;
    @SerializedName("levelMessage")
    private int levelMessage;
    @SerializedName("userId")
    private String userId;
    @SerializedName("from")
    private String nameSender;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("message")
    private String content;
    @SerializedName("roomId")
    private String roomId;
    @SerializedName("tags")
    private String tags;
    @SerializedName("streamer")
    private StreamerModel user;
    @SerializedName("data")
    private String data;
    @SerializedName("msisdn")
    private String msisdn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getIdRep() {
        return idRep;
    }

    public void setIdRep(String idRep) {
        this.idRep = idRep;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimeServer() {
        return timeServer;
    }

    public void setTimeServer(long timeServer) {
        this.timeServer = timeServer;
    }

    public int getIsLike() {
        return isLike;
    }

    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public long getCountLike() {
        return countLike;
    }

    public void setCountLike(long countLike) {
        this.countLike = countLike;
    }

    public int getLevelMessage() {
        return levelMessage;
    }

    public void setLevelMessage(int levelMessage) {
        this.levelMessage = levelMessage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNameSender() {
        return nameSender;
    }

    public void setNameSender(String nameSender) {
        this.nameSender = nameSender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public StreamerModel getUser() {
        return user;
    }

    public void setUser(StreamerModel user) {
        this.user = user;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
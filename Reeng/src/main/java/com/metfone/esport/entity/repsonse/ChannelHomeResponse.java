package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ChannelHomeResponse implements Serializable {
    public static final String TYPE_CHANNEL_INFO = "channelInfo";
    public static final String TYPE_PAST_BROADCAST = "pastBroadcast";
    public static final String TYPE_UPLOADED = "uploaded";
    public static final String TYPE_LIST_GAME_OF_CHANNEL = "listGameOfChannel";

    @SerializedName("title")
    public String title;
    @SerializedName("type")
    public String type;
    @SerializedName("channelInfo")
    public ChannelInfoResponse channelInfo;
    @SerializedName("pastBroadcast")
    public ArrayList<VideoInfoResponse> pastBroadcast;
    @SerializedName("uploaded")
    public ArrayList<VideoInfoResponse> uploaded;
    @SerializedName("listGame")
    public ArrayList<GameInfoResponse> listGame;

    public ChannelHomeResponse() {
    }
}

package com.metfone.esport.entity.camid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CamIdBaseResponse<T> implements Serializable {

    //    @SerializedName("code") public boolean Success;
    @SerializedName("code")
    public String code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public T data;
}

package com.metfone.esport.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyễn Thành Chung on 10/13/20.
 */
public class ResolutionVideo {
    @SerializedName("key")
    @Expose
    public String key;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("video_path")
    @Expose
    public String videoPath;
    @SerializedName("code")
    @Expose
    public Integer code;

    public ResolutionVideo(String key, String title, String videoPath, Integer code) {
        this.key = key;
        this.title = title;
        this.videoPath = videoPath;
        this.code = code;
    }
}

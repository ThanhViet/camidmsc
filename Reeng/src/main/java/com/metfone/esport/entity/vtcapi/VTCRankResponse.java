package com.metfone.esport.entity.vtcapi;

import com.google.gson.annotations.SerializedName;

public class VTCRankResponse {
    @SerializedName("code")
    public String code;
    @SerializedName("errorMessage")
    public String errorMessage;
    @SerializedName("accountRankDTO")
    public VTCAccountRankDTOResponse accountRankDTO;
}

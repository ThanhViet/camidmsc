package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GameRecentResultResponse {
    @SerializedName("time")
    public String time;

    @SerializedName("listGame")
    public ArrayList<GameMatchResponse> listGame;
}

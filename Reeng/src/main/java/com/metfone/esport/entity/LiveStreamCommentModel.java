/*
 * Copyright (c) admin created on 2020-09-28 11:47:15 PM
 */

package com.metfone.esport.entity;

import com.google.gson.annotations.SerializedName;

public class LiveStreamCommentModel {
    @SerializedName("site")
    public String site;
    @SerializedName("item_name")
    public String itemName;
    @SerializedName("item_type")
    public String itemType;
    @SerializedName("content_url")
    public String contentUrl;
    @SerializedName("userId")
    public String userId;
    @SerializedName("action_type")
    public String actionType;
    @SerializedName("url")
    public String url;
    @SerializedName("img_url")
    public String imgUrl;
    @SerializedName("status")
    public String status;
    @SerializedName("user_type")
    public int userType;
    @SerializedName("post_action_from")
    public String postActionFrom;
    @SerializedName("stampId_of_url")
    public int stampIdOfUrl;
    @SerializedName("stampId")
    public long stampId;
    @SerializedName("content_action")
    public String contentAction;
    @SerializedName("channel_id")
    public String channelId;
    @SerializedName("channel_name")
    public String channelName;
    @SerializedName("channel_avatar")
    public String channelAvatar;
    @SerializedName("url_temp")
    public String urlTemp;
    @SerializedName("numfollow")
    public int numfollow;
}

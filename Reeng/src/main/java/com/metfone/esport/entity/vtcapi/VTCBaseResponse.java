package com.metfone.esport.entity.vtcapi;

import com.google.gson.annotations.SerializedName;

public class VTCBaseResponse<T> {
    @SerializedName("errorCode")
    public String errorCode;
    @SerializedName("errorMessage")
    public String errorMessage;
    @SerializedName("result")
    public T result;
}

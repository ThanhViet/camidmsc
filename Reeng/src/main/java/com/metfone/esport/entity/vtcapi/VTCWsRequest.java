package com.metfone.esport.entity.vtcapi;

import com.google.gson.annotations.SerializedName;

public class VTCWsRequest {
    @SerializedName("isdn")
    public String isdn;
    @SerializedName("language")
    public String language = "en";
}

package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyễn Thành Chung on 9/20/20.
 */
public class UserInfoEntity {
    @SerializedName("avatar")
    @Expose
    public String avatar;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("isMochaUser")
    @Expose
    public Integer isMochaUser;
    @SerializedName("user_type")
    @Expose
    public Integer userType;
    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("martk_id")
    @Expose
    public Integer martkId;
    @SerializedName("stamp")
    @Expose
    public Integer stamp;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        if (name == null) name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsMochaUser() {
        return isMochaUser;
    }

    public void setIsMochaUser(Integer isMochaUser) {
        this.isMochaUser = isMochaUser;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMartkId() {
        return martkId;
    }

    public void setMartkId(Integer martkId) {
        this.martkId = martkId;
    }

    public Integer getStamp() {
        return stamp;
    }

    public void setStamp(Integer stamp) {
        this.stamp = stamp;
    }
}

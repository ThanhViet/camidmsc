package com.metfone.esport.entity.camid;

import com.google.gson.annotations.SerializedName;

public class CamIdLoginParam {
    @SerializedName("apiKey")
    public String apiKey = "string";
    @SerializedName("sessionId")
    public String sessionId = "string";
    @SerializedName("username")
    public String username = "string";
    @SerializedName("wsCode")
    public String wsCode = "string";
    @SerializedName("wsRequest")
    public WsRequest wsRequest = new WsRequest();

    public static CamIdLoginParam initWithPassword(String phone_number, String password) {
        CamIdLoginParam param = new CamIdLoginParam();
        param.wsRequest = WsRequest.initWithPassword(phone_number, password);
        return param;
    }

    public static CamIdLoginParam initWithOtp(String phone_number, String otp) {
        CamIdLoginParam param = new CamIdLoginParam();
        param.wsRequest = WsRequest.initWithOtp(phone_number, otp);
        return param;
    }
}

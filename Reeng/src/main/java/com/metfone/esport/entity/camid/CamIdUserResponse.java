package com.metfone.esport.entity.camid;

import com.google.gson.annotations.SerializedName;

public class CamIdUserResponse {
    @SerializedName("username")
    public String username;
    @SerializedName("email")
    public String email;
    @SerializedName("address")
    public String address;
    @SerializedName("gender")
    public Integer gender;
    @SerializedName("avatar")
    public String avatar ;
    @SerializedName("verified")
    public String verified;
    @SerializedName("province")
    public String province;
    @SerializedName("district")
    public String district;
    @SerializedName("user_id")
    public Long user_id;
    @SerializedName("phone_number")
    public String phone_number;
    @SerializedName("full_name")
    public String full_name;
    @SerializedName("date_of_birth")
    public String date_of_birth;
    @SerializedName("identity_number")
    public String identity_number;
}

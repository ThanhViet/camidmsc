package com.metfone.esport.entity.camid;

import com.google.gson.annotations.SerializedName;

public class CamIdRefreshTokenParam {
    @SerializedName("apiKey")
    public String apiKey = "string";
    @SerializedName("sessionId")
    public String sessionId = "string";
    @SerializedName("username")
    public String username = "string";
    @SerializedName("wsCode")
    public String wsCode = "string";
    @SerializedName("wsRequest")
    public WsRequestRefreshToken wsRequest = new WsRequestRefreshToken();

    public class WsRequestRefreshToken {
        @SerializedName("refresh_token")
        public String refresh_token;
    }
}

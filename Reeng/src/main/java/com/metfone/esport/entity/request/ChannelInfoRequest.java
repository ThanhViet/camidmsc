package com.metfone.esport.entity.request;

/**
 * Created by Nguyễn Thành Chung on 11/19/20.
 */
public class ChannelInfoRequest {
    public String channelId;
    public String channel_name;
    public String channel_avatar;

    public ChannelInfoRequest(String channelId, String channel_name, String channel_avatar) {
        this.channelId = channelId;
        this.channel_name = channel_name;
        this.channel_avatar = channel_avatar;
    }
}

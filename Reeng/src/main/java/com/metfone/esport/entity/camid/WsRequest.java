package com.metfone.esport.entity.camid;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.keeng.utils.Utilities;

public class WsRequest {
    @SerializedName("otp")
    public String otp = "";
    @SerializedName("password")
    public String password = "";
    @SerializedName("phone_number")
    public String phone_number = "";
    @SerializedName("social_token")
    public String social_token = "";
    @SerializedName("type")
    public String type = "password";

    public WsRequest() {
    }

    public WsRequest(String phone_number, String type, String otp, String password, String social_token) {
        this.phone_number = phone_number;
        this.type = type;
        this.otp = otp;
        this.password = password;
        this.social_token = social_token;
    }

    public static WsRequest initWithPassword(String phone_number, String password) {
        WsRequest request = new WsRequest();
        request.phone_number = Utilities.fixPhoneNumbTo0(phone_number);
        request.password = password;
        request.type = "password";
        return request;
    }

    public static WsRequest initWithOtp(String phone_number, String otp) {
        WsRequest request = new WsRequest();
        request.phone_number = Utilities.fixPhoneNumbTo0(phone_number);
        request.otp = otp;
        request.type = "otp";
        return request;
    }

}

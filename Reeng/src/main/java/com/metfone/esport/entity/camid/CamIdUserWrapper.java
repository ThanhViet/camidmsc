package com.metfone.esport.entity.camid;

import com.google.gson.annotations.SerializedName;

public class CamIdUserWrapper {
    @SerializedName("user")
    public CamIdUserResponse user;
}

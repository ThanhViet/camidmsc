package com.metfone.esport.entity.request;

/**
 * Created by Nguyễn Thành Chung on 10/20/20.
 */
public class UserInfoRequest {
    public String userId;
    public Integer user_type;
    public String username;
    public String name;
    public String avatar;

    public UserInfoRequest(String userId, Integer user_type, String username, String name, String avatar) {
        this.userId = userId;
        this.user_type = user_type;
        this.username = username;
        this.name = name;
        this.avatar = avatar;
    }
}

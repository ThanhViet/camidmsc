package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyễn Thành Chung on 9/21/20.
 */
public class ResultEntity {
    @SerializedName("id")
    private Integer id;
    @SerializedName("createdDate")
    private String createdDate;
    @SerializedName("isOfficial")
    private Integer isOfficial;
    @SerializedName("description")
    private String description;
    @SerializedName("isMyChannel")
    private Integer isMyChannel;
    @SerializedName("type")
    private Integer type;
    @SerializedName("isRegistered")
    private Integer isRegistered;
    @SerializedName("numFollow")
    private Integer numFollow;
    @SerializedName("urlImages")
    private String urlImages;
    @SerializedName("urlImagesCover")
    private String urlImagesCover;
    @SerializedName("isFollow")
    private Integer isFollow;
    @SerializedName("urlAvatar")
    private String urlAvatar;
    @SerializedName("name")
    private String name;

//    @SerializedName("id")
//    @Expose
//    public Integer id;
//    @SerializedName("name")
//    @Expose
//    public String name;
//    @SerializedName("urlAvatar")
//    @Expose
//    public String urlAvatar;
//    @SerializedName("isFollow")
//    @Expose
//    public Integer isFollow;
//    @SerializedName("urlImagesCover")
//    @Expose
//    public String urlImagesCover;
//    @SerializedName("urlImages")
//    @Expose
//    public String urlImages;
//    @SerializedName("numFollow")
//    @Expose
//    public Integer numFollow;
//    @SerializedName("isRegistered")
//    @Expose
//    public Integer isRegistered;
//    @SerializedName("type")
//    @Expose
//    public Integer type;
//    @SerializedName("isMyChannel")
//    @Expose
//    public Integer isMyChannel;
//    @SerializedName("description")
//    @Expose
//    public String description;
//    @SerializedName("isOfficial")
//    @Expose
//    public Integer isOfficial;
//    @SerializedName("createdDate")
//    @Expose
//    public String createdDate;
//    @SerializedName("createdDateTime")
//    @Expose
//    public Integer createdDateTime;
//    @SerializedName("numVideo")
//    @Expose
//    public Integer numVideo;
//    @SerializedName("categoryId")
//    @Expose
//    public Integer categoryId;
//    @SerializedName("categoryName")
//    @Expose
//    public String categoryName;
//    @SerializedName("status")
//    @Expose
//    public Integer status;
//    @SerializedName("isLive")
//    @Expose
//    public Integer isLive;
}

//{
//      "id": 168538,
//      "name": "Gái xinh quá ak",
//      "urlAvatar": "http://mcvideomd1fr.keeng.net/playnow/images/channel/avatar/20201005/2AxotSaMreC0bv7m.jpeg",
//      "isFollow": 0,
//      "urlImagesCover": "http://media1.xgaming.vn/playnow/images/channel/banner/banner_default/3.jpg",
//      "urlImages": "http://mcvideomd1fr.keeng.net/playnow/images/channel/avatar/20201005/2AxotSaMreC0bv7m.jpeg",
//      "numFollow": 0,
//      "isRegistered": 0,
//      "type": 1,
//      "isMyChannel": 1,
//      "description": "Gái ngon nhé",
//      "isOfficial": 0,
//      "createdDate": "2020-10-05 00:46:34.0",
//      "createdDateTime": 1601833594000,
//      "numVideo": 0,
//      "categoryId": 0,
//      "categoryName": "Gái xinh quá ak",
//      "itemVideo": [
//
//      ],
//      "status": 2,
//      "isLive": 0,
//      "about": "Okie cô em",
//      "imageAbout": "http://mcvideomd1fr.keeng.net/playnow/images/channel/avatar/20201005/LDiNYs4gzysitHkW.jpeg"
//    }
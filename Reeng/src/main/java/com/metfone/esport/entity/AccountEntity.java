package com.metfone.esport.entity;

import java.io.Serializable;

/**
 * Created by Nguyễn Thành Chung on 9/15/20.
 */
public class AccountEntity implements Serializable {
    public String name;
    public int drawable;

    public AccountEntity(String name, int drawable) {
        this.name = name;
        this.drawable = drawable;
    }
}

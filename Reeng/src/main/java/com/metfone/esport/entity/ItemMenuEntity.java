package com.metfone.esport.entity;

import java.io.Serializable;

public class ItemMenuEntity implements Serializable {
    private CharSequence title;
    private int resIcon;
    private int actionTag;
    private Object item;
    private boolean isChecked = false;

    public ItemMenuEntity(CharSequence title, int resIcon, Object object, int actionTag) {
        this.title = title;
        this.resIcon = resIcon;
        this.actionTag = actionTag;
        this.item = object;
    }

    public ItemMenuEntity() {
    }

    public CharSequence getTitle() {
        return title;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public int getResIcon() {
        return resIcon;
    }

    public void setResIcon(int resIcon) {
        this.resIcon = resIcon;
    }

    public int getActionTag() {
        return actionTag;
    }

    public void setActionTag(int actionTag) {
        this.actionTag = actionTag;
    }

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}

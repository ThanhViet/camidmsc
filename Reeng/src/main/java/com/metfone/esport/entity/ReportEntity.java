package com.metfone.esport.entity;

/**
 * Created by Nguyễn Thành Chung on 9/22/20.
 */
public class ReportEntity {
    public String text;
    public boolean isSelect;

    public ReportEntity(String text, boolean isSelect) {
        this.text = text;
        this.isSelect = isSelect;
    }
}

package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

public class GameMatchResponse {
    @SerializedName("id")
    public Long id;

    @SerializedName("startDate")
    public String startDate;

    @SerializedName("startTime")
    public String startTime;

    @SerializedName("tourName")
    public String tourName;

    @SerializedName("logoT1")
    public String avatarT1;

    @SerializedName("nameT1")
    public String nameT1;

    @SerializedName("logoT2")
    public String avatarT2;

    @SerializedName("nameT2")
    public String nameT2;

    @SerializedName("result")
    public String result;
}

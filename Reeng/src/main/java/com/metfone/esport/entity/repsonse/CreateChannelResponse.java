package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nguyễn Thành Chung on 9/21/20.
 */
public class CreateChannelResponse {
    @SerializedName("result")
    @Expose
    public ArrayList<ChannelInfoResponse> result;
    @SerializedName("desc")
    @Expose
    public String desc;
    @SerializedName("code")
    @Expose
    public Integer code;
}

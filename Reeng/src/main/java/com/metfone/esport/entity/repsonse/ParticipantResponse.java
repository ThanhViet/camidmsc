package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

public class ParticipantResponse {

    @SerializedName("id")
    public Long id;

    @SerializedName("name")
    public String name;

    @SerializedName("avatar")
    public String avatar;

    @SerializedName("description")
    public String description;

    @SerializedName("location")
    public String location;

}

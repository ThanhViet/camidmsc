package com.metfone.esport.entity.repsonse;

import android.annotation.SuppressLint;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;

import java.io.Serializable;
import java.util.ArrayList;

public class VideoInfoResponse implements Serializable, IHomeModelType {
    @SerializedName("id")
    public Long id;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("duration")
    public String duration;

    @SerializedName("durationS")
    public int durationS;

    @SerializedName("original_path")
    public String original_path;

    @SerializedName("image_path")
    public String image_path;

    @SerializedName("image_path_thumb")
    public String image_path_thumb;

    @SerializedName("image_path_small")
    public String image_path_small;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("publish_time")
    public Long publish_time;

    @SerializedName("is_like")
    public int is_like;

    @SerializedName("is_share")
    public int is_share;

    @SerializedName("is_follow")
    public int is_follow;

    @SerializedName("link")
    public String link;

    @SerializedName("username")
    public String username;

    @SerializedName("aspecRatio")
    public String aspecRatio;

    @SerializedName("videoType")
    public String videoType;

    @SerializedName("total_like")
    public int total_like;

    @SerializedName("total_view")
    public int total_view;

    @SerializedName("total_unlike")
    public int total_unlike;

    @SerializedName("total_share")
    public int total_share;

    @SerializedName("total_comment")
    public int total_comment;

    @SerializedName("numfollow")
    public int numfollow;

    @SerializedName("exclusive")
    public int exclusive;

    @SerializedName("active")
    public int active;

    @SerializedName("slug")
    public String slug;

    @SerializedName("gameId")
    public int gameId;

    @SerializedName("gameInfo")
    public GameInfoResponse gameInfo;

    @SerializedName("channelInfo")
    public ChannelInfoResponse channelInfo;

    @SerializedName("ccu")
    public int ccu;

    @SerializedName("channel_id")
    public int channel_id;

    @SerializedName("list_resolution")
    public ArrayList<Object> list_resolution;

    @SerializedName("itemStatus")
    public int itemStatus;

    @SerializedName("showAds")
    public int showAds;

    @SerializedName("useLogo")
    public int useLogo;

    @SerializedName("hasLive")
    public int hasLive;

    @SerializedName("isLive")
    public int isLive;

    @SerializedName("startLive")
    public String startLive;

    @SerializedName("endLive")
    public String endLive;

    @SerializedName("urlLive")
    public String urlLive;

    @SerializedName("startLiveTime")
    public long startLiveTime;

    @SerializedName("endLiveTime")
    public long endLiveTime;

    @SerializedName("timeToPlay")
    public int timeToPlay;

    @SerializedName("adaptive_resolution")
    public String adaptive_resolution;

    @SerializedName("isSaved")
    public int isSaved;

    @SerializedName("isHot")
    public int isHot;

    @SerializedName("isHighlight")
    public int isHighlight;

    @SerializedName("type_video")
    public int type_video; // 1: upload, 2: pastbroadcast

    @Override
    public int getItemType() {
        return IHomeModelType.POPULAR_LIVE_CHANNEL;
    }

    public VideoInfoResponse() {
    }

    public VideoInfoResponse(Long id, String title, String description, String duration, Integer durationS, String original_path, String image_path, String image_path_thumb, String image_path_small, String created_at, Long publish_time, Integer is_like, Integer is_share, Integer is_follow, String link, String username, String aspecRatio, String videoType, Integer total_like, Integer total_view, Integer total_unlike, Integer total_share, Integer total_comment, Integer numfollow, Integer exclusive, Integer active, String slug, Integer gameId, GameInfoResponse gameInfo, ChannelInfoResponse channelInfo, Integer ccu, Integer channel_id, ArrayList<Object> list_resolution, Integer itemStatus, Integer showAds, Integer useLogo, Integer hasLive, int isLive, String startLive, String endLive, String urlLive, Long startLiveTime, Long endLiveTime, Integer timeToPlay, String adaptive_resolution, Integer isSaved, Integer isHot, Integer isHighlight) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.durationS = durationS;
        this.original_path = original_path;
        this.image_path = image_path;
        this.image_path_thumb = image_path_thumb;
        this.image_path_small = image_path_small;
        this.created_at = created_at;
        this.publish_time = publish_time;
        this.is_like = is_like;
        this.is_share = is_share;
        this.is_follow = is_follow;
        this.link = link;
        this.username = username;
        this.aspecRatio = aspecRatio;
        this.videoType = videoType;
        this.total_like = total_like;
        this.total_view = total_view;
        this.total_unlike = total_unlike;
        this.total_share = total_share;
        this.total_comment = total_comment;
        this.numfollow = numfollow;
        this.exclusive = exclusive;
        this.active = active;
        this.slug = slug;
        this.gameId = gameId;
        this.gameInfo = gameInfo;
        this.channelInfo = channelInfo;
        this.ccu = ccu;
        this.channel_id = channel_id;
        this.list_resolution = list_resolution;
        this.itemStatus = itemStatus;
        this.showAds = showAds;
        this.useLogo = useLogo;
        this.hasLive = hasLive;
        this.isLive = isLive;
        this.startLive = startLive;
        this.endLive = endLive;
        this.urlLive = urlLive;
        this.startLiveTime = startLiveTime;
        this.endLiveTime = endLiveTime;
        this.timeToPlay = timeToPlay;
        this.adaptive_resolution = adaptive_resolution;
        this.isSaved = isSaved;
        this.isHot = isHot;
        this.isHighlight = isHighlight;
    }

    public boolean isLive() {
        return isLive == 1;
    }

    public boolean isLike() {
        return is_like == 1;
    }

    @SuppressLint("DefaultLocale")
    public String getDurationDisplay() {
        int hours = durationS / 3600;
        int minutes = (durationS - hours * 3600) / 60;
        int seconds = durationS - hours * 3600 - minutes * 60;
        return hours > 0 ? String.format("%02d:%02d:%02d", hours, minutes, seconds) : String.format("%02d:%02d", minutes, seconds);
    }
}

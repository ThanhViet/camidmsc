package com.metfone.esport.entity.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CamIdRefreshTokenResponse {
    @SerializedName("error")
    @Expose
    public Object error;
    @SerializedName("error_description")
    @Expose
    public Object errorDescription;
    @SerializedName("access_token")
    @Expose
    public String accessToken;
    @SerializedName("expires_in")
    @Expose
    public Integer expiresIn;
    @SerializedName("refresh_expires_in")
    @Expose
    public Integer refreshExpiresIn;
    @SerializedName("refresh_token")
    @Expose
    public String refreshToken;
    @SerializedName("token_type")
    @Expose
    public String tokenType;
}

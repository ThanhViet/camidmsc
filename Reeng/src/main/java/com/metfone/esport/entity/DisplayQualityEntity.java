package com.metfone.esport.entity;

import com.metfone.esport.ui.live.EDisplayQuality;

import java.io.Serializable;

/**
 * Created by Nguyễn Thành Chung on 9/2/20.
 */
public class DisplayQualityEntity implements Serializable {
    public EDisplayQuality eDisplayQuality;
    public int status;

    public DisplayQualityEntity(EDisplayQuality eDisplayQuality, int status) {
        this.eDisplayQuality = eDisplayQuality;
        this.status = status;
    }
}

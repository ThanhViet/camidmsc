package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

public class ChannelInfoWrapperResponse {
    @SerializedName("channelInfo")
    public ChannelInfoResponse channelInfo;
}

package com.metfone.esport.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public final class GiftBalanceModel implements Serializable {
    public static final int TYPE_GEM = 0;
    public static final int TYPE_GIFT = 1;
    @SerializedName("id")
    private String id;
    @SerializedName("amount")
    private String amount;
    @SerializedName("price")
    private String price;
    @SerializedName("icon")
    private String icon;
    @SerializedName("active")
    private Integer active;
    @SerializedName("giftType")
    private Integer giftType;
    @SerializedName("accType")
    private Integer accType;
    @SerializedName("sms_content")
    private String smsContent;
    @SerializedName("sms_short_code")
    private String smsShortCode;
    private boolean isSelected;

    public GiftBalanceModel() {

    }

    public GiftBalanceModel(String id, String amount, String price, String icon, Integer active, Integer giftType, Integer accType, String smsContent, String smsShortCode, boolean isSelected) {
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.icon = icon;
        this.active = active;
        this.giftType = giftType;
        this.accType = accType;
        this.smsContent = smsContent;
        this.smsShortCode = smsShortCode;
        this.isSelected = isSelected;
    }

    public final long getAmountLong() {
        try {
            String var10000 = this.amount;
            long var4;
            if (var10000 != null) {
                String var1 = var10000;
                boolean var2 = false;
                var4 = Long.parseLong(var1);
            } else {
                var4 = 0L;
            }

            return var4;
        } catch (Exception var3) {
            var3.printStackTrace();
            return 0L;
        }
    }

    public final long getPriceLong() {
        try {
            if (this.price != null) {
                return Long.parseLong(this.price);
            }
            return 0L;
        } catch (Exception e) {
            return 0L;
        }
    }

    public final String getId() {
        return this.id;
    }

    public final void setId(String var1) {
        this.id = var1;
    }

    public final String getAmount() {
        return this.amount;
    }

    public final void setAmount(String var1) {
        this.amount = var1;
    }

    public final String getPrice() {
        return this.price;
    }

    public final void setPrice(String var1) {
        this.price = var1;
    }

    public final String getIcon() {
        return this.icon;
    }

    public final void setIcon(String var1) {
        this.icon = var1;
    }

    public final Integer getActive() {
        return this.active;
    }

    public final void setActive(Integer var1) {
        this.active = var1;
    }

    public final Integer getGiftType() {
        return this.giftType;
    }

    public final void setGiftType(Integer var1) {
        this.giftType = var1;
    }

    public final Integer getAccType() {
        return this.accType;
    }

    public final void setAccType(Integer var1) {
        this.accType = var1;
    }

    public final String getSmsContent() {
        return this.smsContent;
    }

    public final void setSmsContent(String var1) {
        this.smsContent = var1;
    }

    public final String getSmsShortCode() {
        return this.smsShortCode;
    }

    public final void setSmsShortCode(String var1) {
        this.smsShortCode = var1;
    }

    public final boolean isSelected() {
        return this.isSelected;
    }

    public final void setSelected(boolean var1) {
        this.isSelected = var1;
    }

}
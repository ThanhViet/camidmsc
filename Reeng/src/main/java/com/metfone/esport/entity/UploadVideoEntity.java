/*
 * Copyright (c) admin created on 2020-10-05 02:19:25 AM
 */

package com.metfone.esport.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class UploadVideoEntity implements Serializable {
    public String pathVideo;
    public String title;
    public String description;
    public String itemVideo="";
    public ArrayList<String> listPathThumb;
    public long channelId;
    public long videoId;
    public long categoryId;
    public long publishTime = 0;
}

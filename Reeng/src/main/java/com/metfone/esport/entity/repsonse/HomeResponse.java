package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class HomeResponse implements Serializable {
    @SerializedName("title")
    public String title;

    @SerializedName("type")
    public String type;

    @SerializedName("listVideo")
    public ArrayList<VideoInfoResponse> listVideo;

    @SerializedName("video")
    public VideoInfoResponse video;

    @SerializedName("listGame")
    public ArrayList<GameInfoResponse> listGame;

    public HomeResponse() {
    }

    public HomeResponse(String title, String type, ArrayList<VideoInfoResponse> listVideo, VideoInfoResponse video, ArrayList<GameInfoResponse> listGame) {
        this.title = title;
        this.type = type;
        this.listVideo = listVideo;
        this.video = video;
        this.listGame = listGame;
    }
}

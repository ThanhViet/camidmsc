package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChannelInfoResponse implements Serializable {
    @SerializedName("id")
    public Long id = 0L;

    @SerializedName("name")
    public String name;

    @SerializedName("isFollow")
    public int isFollow;

    @SerializedName("urlImagesCover")
    public String urlImagesCover;

    @SerializedName("urlImages")
    public String urlImages;

    @SerializedName("urlAvatar")
    public String urlAvatar;

    @SerializedName("numFollow")
    public int numFollow;

    @SerializedName("isRegistered")
    public int isRegistered;

    @SerializedName("isNotify")
    public int isNotify;

    @SerializedName("type")
    public int type;

    @SerializedName("isMyChannel")
    public int isMyChannel;

    @SerializedName("description")
    public String description;

    @SerializedName("isOfficial")
    public int isOfficial;

    @SerializedName("createdDate")
    public String createdDate;

    @SerializedName("createdDateTime")
    public Long createdDateTime;

    @SerializedName("numVideo")
    public Integer numVideo;

    @SerializedName("categoryId")
    public int categoryId;

    @SerializedName("categoryName")
    public String categoryName;

    @SerializedName("createdFrom")
    public String createdFrom;

    @SerializedName("status")
    public int status;

    @SerializedName("isLive")
    public int isLive;

    @SerializedName("video")
    public VideoInfoResponse video;

    @SerializedName("videoLive")
    public VideoInfoResponse videoLive;

    @SerializedName("about")
    public String about;

    @SerializedName("totalView")
    public long totalView;

    @SerializedName("imageAbout")
    public String imageAbout;

    @SerializedName("aboutTitle")
    public String aboutTitle;


    public boolean isSelect;

    public ChannelInfoResponse() {
    }

    public ChannelInfoResponse(Long id, String name, Integer isFollow, String urlImagesCover, String urlImages, String urlAvatar, Integer numFollow, Integer isRegistered, Integer isNotify, Integer type, Integer isMyChannel, String description, Integer isOfficial, String createdDate, Long createdDateTime, Integer numVideo, Integer categoryId, String categoryName, String createdFrom, Integer status, Integer isLive, String about, Long totalView) {
        this.id = id;
        this.name = name;
        this.isFollow = isFollow;
        this.urlImagesCover = urlImagesCover;
        this.urlImages = urlImages;
        this.urlAvatar = urlAvatar;
        this.numFollow = numFollow;
        this.isRegistered = isRegistered;
        this.isNotify = isNotify;
        this.type = type;
        this.isMyChannel = isMyChannel;
        this.description = description;
        this.isOfficial = isOfficial;
        this.createdDate = createdDate;
        this.createdDateTime = createdDateTime;
        this.numVideo = numVideo;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.createdFrom = createdFrom;
        this.status = status;
        this.isLive = isLive;
        this.about = about;
        this.totalView = totalView;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        if (name == null) name = "";
        return name;
    }

    public Integer getIsFollow() {
        return isFollow;
    }

    public String getUrlImagesCover() {
        return urlImagesCover;
    }

    public String getUrlImages() {
        return urlImages;
    }

    public String getUrlAvatar() {
        return urlAvatar == null ? "" : urlAvatar ;
    }

    public Integer getNumFollow() {
        return numFollow;
    }

    public Integer getIsRegistered() {
        return isRegistered;
    }

    public Integer getType() {
        return type;
    }

    public Integer getIsMyChannel() {
        return isMyChannel;
    }

    public String getDescription() {
        if (description == null) description = "";
        return description;
    }

    public Integer getIsOfficial() {
        return isOfficial;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public Integer getNumVideo() {
        return numVideo;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        if (categoryName == null) categoryName = "";
        return categoryName;
    }

    public String getCreatedFrom() {
        return createdFrom;
    }

    public Integer getStatus() {
        return status;
    }

    public Integer getIsLive() {
        return isLive;
    }

    public String getAbout() {
        if (about == null) about = "";
        return about;
    }

    public Long getTotalView() {
        return totalView;
    }

    public String getImageAbout() {
        return imageAbout;
    }

    public String getAboutTitle() {
        if (aboutTitle == null) aboutTitle = "";
        return aboutTitle;
    }

    public boolean isSelect() {
        return isSelect;
    }
}

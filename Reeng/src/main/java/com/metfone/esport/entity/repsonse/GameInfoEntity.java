package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Nguyễn Thành Chung on 9/10/20.
 */
public class GameInfoEntity implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("gameName")
    @Expose
    public String gameName;
    @SerializedName("gameDesc")
    @Expose
    public String gameDesc;
    @SerializedName("gameSlug")
    @Expose
    public String gameSlug;
    @SerializedName("platform")
    @Expose
    public String platform;
    @SerializedName("gameImage")
    @Expose
    public String gameImage;
    @SerializedName("getGameTagIds")
    @Expose
    public String getGameTagIds;
    @SerializedName("isFollow")
    @Expose
    public Integer isFollow;
    @SerializedName("totalView")
    @Expose
    public Integer totalView;
    @SerializedName("isLive")
    @Expose
    public Integer isLive;
    @SerializedName("ccu")
    @Expose
    public Integer ccu;
}

package com.metfone.esport.entity.vtcapi;

import com.google.gson.annotations.SerializedName;

public class VTCApiUseRoutingParam {
    @SerializedName("wsCode")
    public String wsCode = "";
    @SerializedName("wsRequest")
    public VTCWsRequest wsRequest;
}

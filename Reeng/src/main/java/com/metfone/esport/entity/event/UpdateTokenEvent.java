package com.metfone.esport.entity.event;

public class UpdateTokenEvent {
    String token = "";

    public UpdateTokenEvent(String token) {
        this.token = token;
    }

    public UpdateTokenEvent() {
    }

    public String getToken() {
        return token;
    }
}

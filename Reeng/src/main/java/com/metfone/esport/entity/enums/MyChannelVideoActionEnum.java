package com.metfone.esport.entity.enums;

import com.metfone.selfcare.R;

public enum MyChannelVideoActionEnum {
    EDIT(R.string.edit, R.drawable.ic_edit_action_es),
    DELETE(R.string.delete, R.drawable.ic_remove_my_list_es),
    SHARE(R.string.share, R.drawable.ic_share_action_es);

    public int textId;
    public int iconId;

    MyChannelVideoActionEnum(int textId, int iconId) {
        this.textId = textId;
        this.iconId = iconId;
    }
}

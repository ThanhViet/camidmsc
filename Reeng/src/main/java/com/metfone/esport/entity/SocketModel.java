package com.metfone.esport.entity;

import com.google.gson.annotations.SerializedName;
import com.metfone.esport.entity.repsonse.ListCommentEntity;

import java.io.Serializable;
import java.util.List;

public class SocketModel implements Serializable {
    public static final int TYPE_MESSAGES = 0;
    public static final int TYPE_LIST_COMMENT = 1;
    public static final int TYPE_DONATE = 2;
    public static final int TYPE_NUMBER_ROOM = 3;
    public static final int TYPE_HEART = 4;

    @SerializedName("type")
    public int type;
    @SerializedName("timestamp")
    public long timestamp;
    @SerializedName("numberLive")
    public long numberLive;
    @SerializedName("user_id")
    public String userId;
    @SerializedName("roomId")
    public String roomId;
    @SerializedName("security")
    public String security;
    @SerializedName("chatMessage")
    public ListCommentEntity chatMessage;
    @SerializedName("messageList")
    public List<ListCommentEntity> messageList;
    @SerializedName("typeGift")
    public String typeGift;
    @SerializedName("data")
    public String data;

}
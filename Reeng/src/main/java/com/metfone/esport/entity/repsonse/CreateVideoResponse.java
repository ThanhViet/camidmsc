package com.metfone.esport.entity.repsonse;

import com.google.gson.annotations.SerializedName;

public class CreateVideoResponse {
    @SerializedName("code")
    private int code;
    @SerializedName("ExecuteTime")
    private int executeTime;
    @SerializedName("desc")
    private String desc;
    @SerializedName("lastIdStr")
    private String lastIdStr;
    @SerializedName("lastId")
    private int lastId;

    public boolean isSuccess() {
        return code == 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(int executeTime) {
        this.executeTime = executeTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLastIdStr() {
        return lastIdStr;
    }

    public void setLastIdStr(String lastIdStr) {
        this.lastIdStr = lastIdStr;
    }

    public int getLastId() {
        return lastId;
    }

    public void setLastId(int lastId) {
        this.lastId = lastId;
    }
}

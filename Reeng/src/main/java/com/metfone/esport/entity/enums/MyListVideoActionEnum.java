package com.metfone.esport.entity.enums;

import com.metfone.selfcare.R;

public enum MyListVideoActionEnum {
    SHARE(R.string.share, R.drawable.ic_share_action_es),
    REMOVE_FROM_LIST(R.string.remove_from_list, R.drawable.ic_remove_my_list_es);

    public int textId;
    public int iconId;

    MyListVideoActionEnum(int textId, int iconId) {
        this.textId = textId;
        this.iconId = iconId;
    }
}

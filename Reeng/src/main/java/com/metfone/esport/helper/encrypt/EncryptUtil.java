/*
 * Created by admin on 2020/5/31
 *
 */

package com.metfone.esport.helper.encrypt;


import com.metfone.esport.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptUtil {
    private static final String TAG = EncryptUtil.class.getSimpleName();

    public static String encryptMD5(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            byte byteData[] = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
//            for (byte aByteData : byteData) {
//                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
//            }
            return sb.toString();
        } catch (EnumConstantNotPresentException e) {
            Log.e(TAG, "EnumConstantNotPresentException", e);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "NoSuchAlgorithmException", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return "";
    }

    public static String encryptSHA256(String input) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("SHA256");
            byte[] byteData = mDigest.digest(input.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
//            for (byte tmp : byteData) {
//                sb.append(Integer.toString((tmp & 0xff) + 0x100, 16).substring(1));
//            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
    }

    public static String md5(String input) {
        if (input != null) {
            try {
                MessageDigest msgDigest = MessageDigest.getInstance("MD5");
                char[] hexArray = "0123456789abcdef".toCharArray();
                byte[] bytes = msgDigest.digest(input.getBytes());
                char[] hexChars = new char[bytes.length * 2];
                int v;
                for (int j = 0; j < bytes.length; j++) {
                    v = bytes[j] & 0xFF;
                    hexChars[j * 2] = hexArray[v >>> 4];
                    hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                }
                return new String(hexChars);
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return null;
    }
}

package com.metfone.esport.helper;

import com.blankj.utilcode.util.DeviceUtils;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.camid.CamIdLoginParam;
import com.metfone.esport.entity.camid.CamIdLoginResponse;
import com.metfone.esport.entity.camid.CamIdUserWrapper;
import com.metfone.esport.entity.event.OnLoginChannel;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.service.APIService;
import com.metfone.esport.service.CamIdServiceRetrofit;
import com.metfone.esport.util.RxUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.service.ServiceRetrofit.getDefaultParam;

/**
 * Created by Nguyễn Thành Chung on 10/14/20.
 */
public class ServiceBusiness {
    public static void login(CompositeDisposable compositeDisposable, APIService apiService) {
        RxUtils.asyncCamId(
                compositeDisposable,
                CamIdServiceRetrofit.getInstance().login(DeviceUtils.getUniqueDeviceId() ,new CamIdLoginParam()),
                new ICallBackResponse<CamIdLoginResponse>() {
                    @Override
                    public void onSuccess(CamIdLoginResponse response) {
                        AccountBusiness.getInstance().saveAuthenInfo(response);
                        getUserId(compositeDisposable,apiService);
                    }

                    @Override
                    public void onFail(String error) {

                    }
                }
        );
    }

    private static void getUserId(CompositeDisposable compositeDisposable, APIService apiService) {
        RxUtils.asyncCamId(
                compositeDisposable,
                CamIdServiceRetrofit.getInstance().getUserId(),
                new ICallBackResponse<CamIdUserWrapper>() {
                    @Override
                    public void onSuccess(CamIdUserWrapper response) {
                        if (response != null && response.user != null)
                            AccountBusiness.getInstance().saveUserId(response.user);

                        getMyChannel(compositeDisposable,apiService);
                    }

                    @Override
                    public void onFail(String error) {

                    }
                }
        );
    }

    public static void getMyChannel(CompositeDisposable compositeDisposable, APIService apiService) {
        //"84358427382"
        RxUtils.async(
                compositeDisposable,
                apiService.getMyChannels(AccountBusiness.getInstance().getPhoneNumberOrUserId(), getDefaultParam()),
                new ICallBackResponse<ArrayList<ChannelInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<ChannelInfoResponse> response) {
                        AccountBusiness.getInstance().saveMyChannel(response);
                        EventBus.getDefault().post(new OnLoginChannel());
//                        if (view != null) view.setupHeaderView();
                    }

                    @Override
                    public void onFail(String error) {
//                        if (view != null) view.setupHeaderView();
                    }
                });
    }
}

/*
 * Created by admin on 2020/5/31
 *
 */

package com.metfone.esport.helper;

import android.text.TextUtils;

import com.metfone.selfcare.BuildConfig;
import com.metfone.esport.base.BaseResponse;
import com.metfone.esport.common.Constant;
import com.metfone.esport.helper.encrypt.EncryptUtil;
import com.metfone.esport.helper.encrypt.RSAEncrypt;
import com.metfone.esport.helper.encrypt.XXTEACrypt;
import com.metfone.esport.util.Log;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class HttpHelper {
    private static final String TAG = HttpHelper.class.getSimpleName();

    public static String encoderUrl(String content) {
        if (TextUtils.isEmpty(content)) return content;
        try {
            return URLEncoder.encode(content, Constant.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException", e);
        }
        return content;
    }

    public static String decoderUrl(String content) {
        if (TextUtils.isEmpty(content)) return content;
        try {
            return URLDecoder.decode(content, Constant.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException", e);
        }
        return content;
    }

    /**
     * Sử dụng cho mã hóa tạo ra chuỗi security để làm param
     *
     * @param textToEncrypt tong params truyen vao
     * @param token
     * @return dataEncrypt
     */
    public static String encryptDataApi(String textToEncrypt, String token) {
        return encryptData(textToEncrypt, token, Constant.PUBLIC_KEY_API);
    }

    public static String encryptData(String textToEncrypt, String token, String publishKey) {
        try {
            String md5Encrypt = EncryptUtil.encryptMD5(textToEncrypt);
            JSONObject data = new JSONObject();
            data.put("token", token);
            data.put("md5", md5Encrypt);
            return RSAEncrypt.encrypt(data.toString(), RSAEncrypt.getPublicKeyFromString(publishKey));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return "";
        }
    }

    public static String decryptResponse(String input, String key) {
        //Log.d(TAG, "decryptResponse key: " + key);
        if (TextUtils.isEmpty(input)) {
            return null;
        }
        try {
            JSONObject jsonObject = new JSONObject(input);
            if (jsonObject.optInt("code", -1) == 200) {
                return XXTEACrypt.decryptBase64StringToString(jsonObject.optString("data"), key);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public static String getDomainName(String url) {
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            if (domain.startsWith("www.")) {
                return domain.substring(4);
            }
            return domain;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String decryptResponse(BaseResponse<String> response, String key) {
        //Log.d(TAG, "decryptResponse key: " + key);
        String result = "";
        if (response != null && response.isSuccess()) {
            try {
                result = XXTEACrypt.decryptBase64StringToString(response.data, key);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "response: " + response);
            Log.d(TAG, "decrypt: " + result);
        }
        return result;
    }

    public static String encryptDataLiveComment(String userId, String token, long timestamp) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(userId);
            sb.append(token);
            sb.append(timestamp);
            String md5Encrypt = EncryptUtil.encryptMD5(sb.toString());
            Log.d("LiveComment", "md5Encrypt: " + md5Encrypt);
            JSONObject data = new JSONObject();
            try {
                data.put("token", token);
                data.put("md5", md5Encrypt);
            } catch (Exception e) {
                return null;
            }
            return RSAEncrypt.encrypt(data.toString(), RSAEncrypt.getPublicKeyFromString(Constant.PUBLIC_KEY_LIVE_COMMENT));
        } catch (Exception e) {
        }
        return "";
    }
}
package com.metfone.esport.helper;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.UploadVideoEntity;
import com.metfone.esport.entity.repsonse.CreateVideoResponse;
import com.metfone.esport.entity.repsonse.UploadVideoResponse;
import com.metfone.esport.network.file.upload.UploadListener;
import com.metfone.esport.network.file.upload.UploadManager;
import com.metfone.esport.network.file.upload.UploadRequest;
import com.metfone.esport.service.FileServiceRetrofit;
import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.RxUtils;

import java.io.File;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TransferFileBusiness {
    private static final String TAG = TransferFileBusiness.class.getSimpleName();
    private AccountBusiness mAccountBusiness;
    private UploadManager mUploadManager;
    private UploadListener uploadListener = new UploadListener() {
        @Override
        public void onUploadStarted(UploadRequest request) {
            Log.i(TAG, "onUploadStarted");
            ListenerHelper.getInstance().onUploadStated(request);
        }

        @Override
        public void onUploadComplete(UploadRequest request, String response) {
            Log.i(TAG, "onUploadComplete response: " + response);
            ListenerHelper.getInstance().onUploadComplete(request, response);
            try {
                UploadVideoResponse uploadVideoResponse = GsonUtils.fromJson(response, UploadVideoResponse.class);
                if (uploadVideoResponse != null && request != null) {
                    if (request.getData() instanceof UploadVideoEntity) {
                        UploadVideoEntity data = (UploadVideoEntity) request.getData();
                        if (uploadVideoResponse.errorCode == 0) {
                            data.itemVideo = uploadVideoResponse.itemVideo;
                            createUploadVideo(request, data);
                        } else {
                            ToastUtils.showShort(uploadVideoResponse.desc);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUploadFailed(UploadRequest request, int errorCode, String errorMessage) {
            Log.e(TAG, "onUploadFailed code: " + errorMessage + " msg: " + errorMessage);

        }

        @Override
        public void onProgress(UploadRequest request, long totalBytes, long uploadedBytes, int progress, long speed) {
            Log.d(TAG, "upload onProgress totalBytes: " + totalBytes + ", uploadedBytes: " + uploadedBytes + ", progress: " + progress + ", speed: " + speed);
            ListenerHelper.getInstance().onUploadProgress(request, totalBytes, uploadedBytes, progress, speed);
        }

        @Override
        public void onCreateVideoSuccess(UploadRequest uploadRequest, CreateVideoResponse response) {
            ListenerHelper.getInstance().onCreateVideoSuccess(uploadRequest, response);
        }

        @Override
        public void onCreateVideoFailed(UploadRequest uploadRequest, String error) {
            ListenerHelper.getInstance().onCreateVideoFailed(uploadRequest, error);
        }

    };

    public TransferFileBusiness() {
        mUploadManager = new UploadManager();
    }

    public UploadManager getUploadManager() {
        return mUploadManager;
    }

    public void cancelUpload(UploadRequest request) {
        if (request == null) return;
        mUploadManager.cancel(request.getUploadId());
    }

    public UploadRequest uploadVideo(UploadVideoEntity entity) {
        if (mAccountBusiness == null) mAccountBusiness = AccountBusiness.getInstance();
        String uploadUrl = Constant.BASE_URL_UPLOAD_ESPORT + "/camidApiService/app/uploadVideo";
        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams("msisdn", mAccountBusiness.getPhoneNumber())
                    .setFormData("uploadfile")
                    .setContentType("multipart/form-data")
                    .setFilePath(entity.pathVideo)
                    .setContainer(entity.pathVideo)
                    .setUploadListener(uploadListener);
            request.setData(entity);
            mUploadManager.add(request);
            return request;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }



    private void createUploadVideo(UploadRequest request, UploadVideoEntity entity) {
        String thumbPath = CollectionUtils.isNotEmpty(entity.listPathThumb) ? entity.listPathThumb.get(0) : "";
        File fileThumb = null;
        if (StringUtils.isNotEmpty(thumbPath)) {
            fileThumb = new File(thumbPath);
        }
        MultipartBody.Part bodyThumb = null;
        MediaType imageType = MediaType.parse("image/jpeg");
        RequestBody requestFile;
        if (fileThumb != null) {
            requestFile = RequestBody.Companion.create(fileThumb, imageType);
            bodyThumb = MultipartBody.Part.createFormData("imageUpload", fileThumb.getName(), requestFile);
        }
        RequestBody imageLead = RequestBody.create("", MultipartBody.FORM);
        RxUtils.asyncSimple(new CompositeDisposable(), FileServiceRetrofit.getInstance().createVideoUploadByUser(
                entity.channelId, entity.videoId, entity.categoryId, entity.title, entity.description,
                entity.itemVideo, entity.publishTime, "", imageLead, bodyThumb, ServiceRetrofit.getDefaultParam()), new ICallBackResponse<CreateVideoResponse>() {
            @Override
            public void onSuccess(CreateVideoResponse response) {
                if (uploadListener != null) uploadListener.onCreateVideoSuccess(request, response);
            }

            @Override
            public void onFail(String error) {
                if (uploadListener != null)
                    uploadListener.onCreateVideoFailed(request, error);
            }
        });
    }
}
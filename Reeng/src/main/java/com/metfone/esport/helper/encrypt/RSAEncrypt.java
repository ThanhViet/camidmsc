/*
 * Created by admin on 2020/5/31
 *
 */

package com.metfone.esport.helper.encrypt;

import android.text.TextUtils;
import android.util.Base64;

import com.metfone.esport.util.Log;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class RSAEncrypt {
    protected static final String ALGORITHM = "RSA";
    private static final String TAG = RSAEncrypt.class.getSimpleName();

    private static String base64Encode(byte[] input) {
        return Base64.encodeToString(input, Base64.NO_WRAP);
    }

    private static String base64EncodeRFC2045(byte[] input) {
        return Base64.encodeToString(input, Base64.DEFAULT);
    }

    private static byte[] base64Decode(String input, String coding) throws UnsupportedEncodingException {
        return Base64.decode(input.getBytes(coding), Base64.NO_WRAP);
    }

    private static byte[] base64Decode(String input) {
        return Base64.decode(input.getBytes(), Base64.NO_WRAP);
    }

    private static synchronized byte[] encrypt(byte[] text, PublicKey publicKey) throws Exception {
        byte[] cipherText = new byte[0];
        if (publicKey != null) {
            // get an RSA cipher object and print the provider
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            // encrypt the plaintext using the public key
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            cipherText = cipher.doFinal(text);
        }
        return cipherText;
    }

    public static PublicKey getPublicKeyFromString(String key) throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(base64Decode(key));
        return keyFactory.generatePublic(publicKeySpec);
    }

    /**
     * Encrypt a text using public key. The result is encrypted BASE64 encoded
     * text
     *
     * @param text The original unencrypted text
     * @return Encrypted text encoded as BASE64
     * @throws Exception
     */
    public static String encrypt(String text, PublicKey publicKey) {
        if (TextUtils.isEmpty(text)) return null;
        try {
            String encryptedText;
            byte[] cipherText = encrypt(text.getBytes(StandardCharsets.UTF_8), publicKey);
            encryptedText = base64Encode(cipherText);
            return encryptedText;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
    }

    public static String encryptRFC2045(String text, PublicKey publicKey) {
        try {
            String encryptedText;
            byte[] cipherText = encrypt(text.getBytes(StandardCharsets.UTF_8), publicKey);
            encryptedText = base64EncodeRFC2045(cipherText);
            return encryptedText;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
    }

}
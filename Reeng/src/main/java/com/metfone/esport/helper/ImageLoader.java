package com.metfone.esport.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.SizeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.R;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

public class ImageLoader {

    protected static final String TAG = "ImageLoader";

    protected static void setResourceTransform(ImageView image, int resource, Transformation transformation) {
        if (image == null)
            return;
        try {
            int width = image.getWidth();
            int height = image.getHeight();
            Context context = image.getContext();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = RequestOptions.bitmapTransform(transformation)
                        .override(width, height)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .clone();
                Glide.with(context)
                        .load(resource)
                        .apply(requestOptions)
                        .into(image);
            } else {
                RequestOptions requestOptions = RequestOptions.bitmapTransform(transformation)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .clone();
                Glide.with(context)
                        .load(resource)
                        .apply(requestOptions)
                        .into(image);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setImage(ImageView image, String url, int placeholder, int error, Transformation transformation) {
        setImage(image, url, placeholder, error, transformation, null);
    }

    public static void setImage(ImageView image, String url, int placeholder, int error, Transformation transformation, @Nullable RequestListener<Drawable> requestListener) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            if (transformation == null) {
                if (width > 0 && height > 0) {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .error(error)
                            .override(width, height)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .transition(DrawableTransitionOptions.withCrossFade(500))
                            .listener(requestListener)
                            .into(image);
                } else {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .error(error)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .transition(DrawableTransitionOptions.withCrossFade(500))
                            .listener(requestListener)
                            .into(image);
                }
            } else {
                if (width > 0 && height > 0) {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .override(width, height)
                            .error(error)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .centerCrop()
                            .dontAnimate()
                            .transform(transformation);
                    DrawableTransitionOptions transitionOptions = new DrawableTransitionOptions();
                    transitionOptions.crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)).withCrossFade(500);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
//                            .transition(transitionOptions)
                            .listener(requestListener)
                            .into(image);
                } else {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .error(error)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .centerCrop()
                            .dontAnimate()
                            .transform(transformation);
                    DrawableTransitionOptions transitionOptions = new DrawableTransitionOptions();
                    transitionOptions.crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)).withCrossFade(500);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .transition(transitionOptions)
                            .listener(requestListener)
                            .into(image);
                }
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    protected static void setImageTransform(ImageView image, String url, int placeholder, int error, Transformation transformation) {
        if (image == null)
            return;
        setImage(image, url, placeholder, error, transformation);
    }

    public static void setAvatarProfile(String url, final ImageView image) {
        if (image == null)
            return;
        if (TextUtils.isEmpty(url)) {
//            setResource(image, R.drawable.df_avatar_profile);
        } else {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = new RequestOptions()
//                        .placeholder(R.drawable.df_avatar_profile)
//                        .error(R.drawable.df_avatar_profile)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .override(width, height)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            } else {
                int size = SizeUtils.dp2px(48f);
                RequestOptions requestOptions = new RequestOptions()
//                        .placeholder(R.drawable.df_avatar_profile)
//                        .error(R.drawable.df_avatar_profile)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .override(size, size)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            }
        }
    }

    public static void setNotification(Context context, String url, int error, Target target) {
        if (context == null || target == null)
            return;
        try {
            if (TextUtils.isEmpty(url)) {
                RequestOptions requestOptions = new RequestOptions()
                        .priority(Priority.IMMEDIATE)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                Glide.with(context)
                        .asBitmap()
                        .load(error)
                        .apply(requestOptions)
                        .into(target);
            } else {
                RequestOptions requestOptions = new RequestOptions()
                        .priority(Priority.IMMEDIATE)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .placeholder(error)
                        .fitCenter()
                        .clone()
                        .dontAnimate()
                        .dontTransform();
                Glide.with(context)
                        .asBitmap()
                        .load(url)
                        .apply(requestOptions)
                        .into(target);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @SuppressLint("CheckResult")
    public static void setResource(ImageView image, int resource) {
        if (image == null) return;
        try {
            int width = image.getWidth();
            int height = image.getHeight();
            Context context = image.getContext();
            RequestOptions requestOptions = new RequestOptions()
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            if (width > 0 && height > 0) {
                requestOptions.override(width, height);
            }
            Glide.with(context)
                    .load(resource)
                    .apply(requestOptions.clone())
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setResourceCaptcha(ImageView image, int resource) {
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            RequestOptions requestOptions = new RequestOptions()
                    .priority(Priority.HIGH)
                    .dontTransform()
                    .dontAnimate()
                    .optionalCenterInside()
                    .clone();
            if (width > 0 && height > 0) {
                requestOptions = requestOptions.override(width, height);
            }
            Glide.with(context)
                    .load(resource)
                    .apply(requestOptions)
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setImageCaptcha(ImageView image, String url, RequestListener<Drawable> listener) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.color.white)
                    .error(R.color.white)
                    .priority(Priority.HIGH)
                    .dontTransform()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.NONE);
            if (width > 0 && height > 0) {
                requestOptions = requestOptions.override(width, height);
            }
            Glide.with(context)
                    .load(url)
                    .apply(requestOptions)
                    .listener(listener)
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @SuppressLint("CheckResult")
    public static void setImageFile(ImageView image, String filePath, int placeholder, int error, int width, int height, Transformation transformation) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            File file = new File(filePath);
            Uri imageUri = Uri.fromFile(file);
            if (width == 0 && height == 0) {
                width = image.getMeasuredWidth();
                height = image.getMeasuredHeight();
            }
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(placeholder)
                    .error(error)
                    .priority(Priority.HIGH)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            if (width > 0 && height > 0)
                requestOptions.override(width, height);

            if (transformation == null) {
                Glide.with(context)
                        .load(imageUri)
                        .apply(requestOptions)
                        .transition(DrawableTransitionOptions.withCrossFade(500))
                        .into(image);
            } else {
                requestOptions.centerCrop()
                        .dontAnimate()
                        .transform(transformation);
                DrawableTransitionOptions transitionOptions = new DrawableTransitionOptions();
                transitionOptions.crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)).withCrossFade(500);
                Glide.with(context)
                        .load(imageUri)
                        .apply(requestOptions)
                        .transition(transitionOptions)
                        .into(image);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static Bitmap downloadImageBitmap(String linkImage) {
        Log.d(TAG, "downloadImageBitmap: " + linkImage);
        Bitmap bitmap = null;
        try {
            InputStream inputStream = new URL(linkImage).openStream();   // Download Image from URL
            bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
            inputStream.close();
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return bitmap;
    }

    public static Bitmap getImageBitmapFromFile(String filePath) {
        Log.d(TAG, "getImageBitmapFromFile: " + filePath);
        Bitmap bitmap = null;
        try {
            File file = new File(filePath);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return bitmap;
    }

    public static void setImageFile(ImageView image, String filePath, int placeholder, int error, int width, int height) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            File file = new File(filePath);
            Uri imageUri = Uri.fromFile(file);
            if (width == 0 && height == 0) {
                width = image.getMeasuredWidth();
                height = image.getMeasuredHeight();
            }
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(placeholder)
                    .error(error)
                    .priority(Priority.HIGH)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            if (width > 0 && height > 0)
                requestOptions.override(width, height);

            Glide.with(context)
                    .load(imageUri)
                    .apply(requestOptions)
                    .transition(DrawableTransitionOptions.withCrossFade(500))
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setImageFile(ImageView image, String filePath) {
        if (image == null)
            return;
        setImageFile(image, filePath, R.drawable.bg_image_placeholder_es, 0, 0, 0);
    }

    public static void setAvatarChannel(final ImageView image, String url) {
        if (image == null)
            return;
        if (TextUtils.isEmpty(url)) {
//            setResource(image, R.drawable.df_avatar_profile);
        } else {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = new RequestOptions()
//                        .placeholder(R.drawable.df_avatar_profile)
//                        .error(R.drawable.df_avatar_profile)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .override(width, height)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            } else {
                int size = SizeUtils.dp2px(48f);
                RequestOptions requestOptions = new RequestOptions()
//                        .placeholder(R.drawable.df_avatar_profile)
//                        .error(R.drawable.df_avatar_profile)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .override(size, size)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            }
        }
    }

    public static void setCoverChannel(final ImageView image, String url) {
        if (image == null)
            return;
        if (TextUtils.isEmpty(url)) {
            setResource(image, R.drawable.ic_cover_channel_es);
        } else {
            try {
                RequestOptions requestOptions = new RequestOptions()
                        .error(R.drawable.ic_cover_channel_es)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(image)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static void setAboutChannel(final ImageView image, String url) {
        if (image == null)
            return;
        if (TextUtils.isEmpty(url)) {
            setResource(image, R.drawable.bg_image_placeholder_es);
        } else {
            try {
                RequestOptions requestOptions = new RequestOptions()
                        .error(R.drawable.bg_image_placeholder_es)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(image)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static void setImageUri(final ImageView image, Uri uri, int placeholderId, int errorId, int width, int height) {
        if (image == null) return;
        try {
            RequestBuilder requestBuilder = Glide.with(image).load(uri);
            if (placeholderId > 0) requestBuilder.placeholder(placeholderId);
            if (errorId > 0) requestBuilder.error(errorId);
            if (width > 0 && height > 0) requestBuilder.override(width, height);
            requestBuilder.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            requestBuilder.into(image);
        } catch (OutOfMemoryError e) {
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    public static void setImageUri(final ImageView image, Uri uri, int placeholderId, int errorId, Transformation transformation) {
        if (image == null) return;
        try {
            RequestBuilder requestBuilder = Glide.with(image).load(uri);
            if (placeholderId > 0) requestBuilder.placeholder(placeholderId);
            if (errorId > 0) requestBuilder.error(errorId);
            if (transformation != null) requestBuilder.transform(transformation);
            requestBuilder.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            requestBuilder.into(image);
        } catch (OutOfMemoryError e) {
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    public static void setImageUri(final ImageView image, Uri uri) {
        setImageUri(image, uri, 0, 0, 0, 0);
    }

    public static void setCoverVideo(final ImageView image, VideoInfoResponse entity) {
        if (image == null) return;
        try {
            int radius = image.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall);
            setImage(image, entity.image_path_thumb, R.drawable.bg_image_placeholder_es, R.drawable.bg_image_error_es, new MultiTransformation<>(new CenterCrop(), new GranularRoundedCorners(radius, 0, 0, radius)));
        } catch (OutOfMemoryError e) {
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }


    public static void setAvatarUrl(ImageView imageView, String url) {
        if (imageView == null) return;
        try {
            ImageLoader.setImage(
                    imageView,
                    url,
                    R.drawable.bg_avatar_default_es,
                    R.drawable.ic_avatar_member,
                    new CircleCrop());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setAvatarUri(ImageView imageView, Uri uri) {
        if (imageView == null) return;
        try {
            ImageLoader.setImageUri(
                    imageView,
                    uri,
                    R.drawable.bg_avatar_default_es,
                    R.drawable.ic_avatar_member,
                    new CircleCrop());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
/*
 * Copyright (c) admin created on 2020-09-16 10:27:12 PM
 */

package com.metfone.esport.helper;

import android.text.TextUtils;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.camid.CamIdLoginResponse;
import com.metfone.esport.entity.camid.CamIdUserResponse;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.util.EnumUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;

public class AccountBusiness {

    private static AccountBusiness mInstance;

//    private CamIdLoginResponse loginResponse;

    private CamIdUserResponse userResponse;

//    private VTCAccountRankDTOResponse accountRankDTO;

    private ArrayList<ChannelInfoResponse> myChannels;

    private ChannelInfoResponse currentChannelInfo;

    public static AccountBusiness getInstance() {
        if (mInstance == null) {
            synchronized (AccountBusiness.class) {
                mInstance = new AccountBusiness();

                //init Data from cache
//                mInstance.getAuthenInfo();
                mInstance.getUserInfo();
//                mInstance.getAccountRank();
                mInstance.getMyChannels();
            }
        }
        return mInstance;
    }

    public boolean isLogin() {
        //todo viết lại hàm này để lấy đúng thông tin trạng thái login
        //return StringUtils.isNotEmpty(getToken());
        return UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT;
    }

    public long getUserId() {
        if (userResponse != null)
            return userResponse.user_id;

        return 0;
    }

    public String getToken() {
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        return token;
    }

    public void saveToken(String token) {
        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
    }

    public void saveRefreshToken(String token) {
        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, token);
    }

    public String getRefreshToken() {
        String token = SharedPrefs.getInstance().get(PREF_REFRESH_TOKEN, String.class);
        return token;
    }

    public String getLanguageCode() {
        return MetfonePlusClient.LANGUAGE;
//        return ApplicationController.getApp().getReengAccountBusiness().getCurrentLanguage();
    }

    public void saveAuthenInfo(CamIdLoginResponse response) {
//        if (response != null) {
//            loginResponse = response;
//
//            SPUtils.getInstance().put(Constant.PREF_CACHE_AUTHEN_ESPORT, new Gson().toJson(response));
//        }
    }

//    public void saveAccountRank(VTCAccountRankDTOResponse response) {
//        if (response != null) {
//            accountRankDTO = response;
//
//            SPUtils.getInstance().put(Constant.PREF_CACHE_ACCOUNT_RANK_ESPORT, new Gson().toJson(response));
//        }
//    }

    public void saveUserId(CamIdUserResponse response) {
        userResponse = response;

        if (response != null) {
            SPUtils.getInstance().put(Constant.PREF_CACHE_ACCOUNT_ESPORT, new Gson().toJson(response));
        } else {
            SPUtils.getInstance().remove(Constant.PREF_CACHE_ACCOUNT_ESPORT);
        }
    }

//    public CamIdLoginResponse getAuthenInfo() {
//        if (loginResponse == null) {
//            try {
//                String s = SPUtils.getInstance().getString(Constant.PREF_CACHE_AUTHEN_ESPORT, null);
//
//                if (StringUtils.isNotEmpty(s)) {
//                    loginResponse = new Gson().fromJson(s, CamIdLoginResponse.class);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        return loginResponse;
//    }

    public CamIdUserResponse getUserInfo() {
        if (userResponse == null) {
            try {
                String s = SPUtils.getInstance().getString(Constant.PREF_CACHE_ACCOUNT_ESPORT, null);

                if (StringUtils.isNotEmpty(s)) {
                    userResponse = new Gson().fromJson(s, CamIdUserResponse.class);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return userResponse;
    }

//    public VTCAccountRankDTOResponse getAccountRank() {
//        if (accountRankDTO == null) {
//            try {
//                String s = SPUtils.getInstance().getString(Constant.PREF_CACHE_ACCOUNT_RANK_ESPORT, null);
//
//                if (StringUtils.isNotEmpty(s)) {
//                    accountRankDTO = new Gson().fromJson(s, VTCAccountRankDTOResponse.class);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        return accountRankDTO;
//    }

    public String getPhoneNumber() {
        if (userResponse != null)
            return userResponse.phone_number == null ? "" : userResponse.phone_number;
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(ApplicationController.getApp());
        if (userInfoBusiness.getUser() != null) {
            return String.valueOf(userInfoBusiness.getUser().getPhone_number());
        }
        return "";
    }

    public ArrayList<ChannelInfoResponse> getMyChannels() {
        if (myChannels == null) {
            try {
                String s = SPUtils.getInstance().getString(Constant.PREF_CACHE_LIST_MY_CHANNEL_ESPORT, null);
                if (StringUtils.isNotEmpty(s)) {
                    Type type = new TypeToken<ArrayList<ChannelInfoResponse>>() {
                    }.getType();
                    myChannels = GsonUtils.fromJson(s, type);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myChannels;
    }

    public String getAvatarUrl() {
        if (userResponse != null) {
            if (userResponse.avatar == null) {
                return "";
            }
            return userResponse.avatar;
        }
        return "";
    }

    public String getUsername() {
        if (userResponse != null)
            return userResponse.full_name;
        return "";
    }

    public ChannelInfoResponse getChannelInfo() {
        if (SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
            ChannelInfoResponse channel = null;

            if (currentChannelInfo == null) {
                try {
                    String cache = SPUtils.getInstance().getString(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT, null);
                    if (StringUtils.isNotEmpty(cache)) {
                        channel = new Gson().fromJson(cache, ChannelInfoResponse.class);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (channel == null && CollectionUtils.isNotEmpty(myChannels)) {
                    channel = Collections.max(myChannels, (channel1, channel2) -> {
                        if ((channel1 == null || channel1.numVideo == null) && (channel2 == null || channel2.numVideo == null))
                            return 0;
                        if (channel1 == null || channel1.numVideo == null) return -1;
                        if (channel2 == null || channel2.numVideo == null) return 1;

                        return channel1.numVideo.compareTo(channel2.numVideo);
                    });
                    saveCurrentChannel(channel);
                }
            }
        }

        return currentChannelInfo;
    }

    public long getChannelId() {
        ChannelInfoResponse channel = getChannelInfo();
        return channel == null ? 0 : channel.id;
    }

    public void addChannel(ArrayList<ChannelInfoResponse> listChannel) {
        if (myChannels == null) {
            myChannels = new ArrayList<>();
        }
        myChannels.addAll(listChannel);
        SPUtils.getInstance().put(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT, new Gson().toJson(myChannels));
    }

    public void saveMyChannel(ArrayList<ChannelInfoResponse> listChannel) {
        myChannels = listChannel;

        ChannelInfoResponse channel = null;
        try {
            String cache = SPUtils.getInstance().getString(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT, null);
            if (StringUtils.isNotEmpty(cache)) {
                channel = new Gson().fromJson(cache, ChannelInfoResponse.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        SPUtils.getInstance().remove(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT);

        if (CollectionUtils.isNotEmpty(myChannels)) {
            if (channel == null) {
                channel = Collections.max(myChannels, (channel1, channel2) -> {
                    if ((channel1 == null || channel1.numVideo == null) && (channel2 == null || channel2.numVideo == null))
                        return 0;
                    if (channel1 == null || channel1.numVideo == null) return -1;
                    if (channel2 == null || channel2.numVideo == null) return 1;

                    return channel1.numVideo.compareTo(channel2.numVideo);
                });
            } else {
                ChannelInfoResponse finalChannel = channel;
                channel = CollectionUtils.find(myChannels, item -> item != null && item.id.equals(finalChannel.id));
            }

            saveCurrentChannel(channel);
        } else {
            saveCurrentChannel(null);

            SPUtils.getInstance().remove(Constant.PREF_CACHE_LIST_MY_CHANNEL_ESPORT);
        }
    }

    public void saveCurrentChannel(ChannelInfoResponse channel) {
        if (SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
            currentChannelInfo = channel;

            if (currentChannelInfo == null)
                SPUtils.getInstance().remove(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT);
            else
                SPUtils.getInstance().put(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT, new Gson().toJson(currentChannelInfo));
        } else {
            currentChannelInfo = null;

            SPUtils.getInstance().remove(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT);
        }
    }

    public boolean isOfficial() {
        return currentChannelInfo != null && currentChannelInfo.isOfficial == 1;
    }

    public boolean isVerified() {
        return userResponse != null && "1".equals(userResponse.verified);
    }

    public void clearMyChannel() {
        if (CollectionUtils.isNotEmpty(myChannels)) {
            myChannels.clear();
        }
    }
//
//    public void clearCurrentChannel() {
//        currentChannelInfo = null;
//    }

    /**
     * clear du lieu login khi logout
     * Created_by @dchieu on 12/8/20
     */
    public void clearAuthenInfo() {
        saveUserId(null);
        saveMyChannel(null);
    }

    public String getPhoneNumberOrUserId() {
        String result = getPhoneNumber();
        if (TextUtils.isEmpty(result)) {
            if (getUserId() <= 0 && currentChannelInfo != null) {
                result = String.valueOf(currentChannelInfo.id);
            } else if (getUserId() > 0) {
                result = String.valueOf(getUserId());
            } else result = "";
        }
        return result;
    }
}

package com.metfone.esport.helper;

import com.metfone.esport.entity.repsonse.CreateVideoResponse;
import com.metfone.esport.network.file.download.DownloadListener;
import com.metfone.esport.network.file.download.DownloadRequest;
import com.metfone.esport.network.file.upload.UploadListener;
import com.metfone.esport.network.file.upload.UploadRequest;

import java.util.concurrent.CopyOnWriteArrayList;

public class ListenerHelper {
    private static final String TAG = ListenerHelper.class.getSimpleName();
    private static ListenerHelper mInstance;
    private CopyOnWriteArrayList<DownloadListener> mDownloadListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<UploadListener> mUploadListeners = new CopyOnWriteArrayList<>();

    private ListenerHelper() {

    }

    public static synchronized ListenerHelper getInstance() {
        if (mInstance == null)
            mInstance = new ListenerHelper();
        return mInstance;
    }

    /**
     * download listener
     *
     * @param listener
     */
    public void addDownloadListener(DownloadListener listener) {
        if (!mDownloadListeners.contains(listener))
            mDownloadListeners.add(listener);
    }

    public void removeDownloadListener(DownloadListener listener) {
        if (mDownloadListeners.contains(listener))
            mDownloadListeners.remove(listener);
    }

    public void onDownloadStated(DownloadRequest request) {
        if (mDownloadListeners != null && !mDownloadListeners.isEmpty()) {
            for (DownloadListener listener : mDownloadListeners) {
                listener.onDownloadStarted(request);
            }
        }
    }

    public void onDownloadComplete(DownloadRequest request) {
        if (mDownloadListeners != null && !mDownloadListeners.isEmpty()) {
            for (DownloadListener listener : mDownloadListeners) {
                listener.onDownloadComplete(request);
            }
        }
    }

    public void onDownloadProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
        if (mDownloadListeners != null && !mDownloadListeners.isEmpty()) {
            for (DownloadListener listener : mDownloadListeners) {
                listener.onProgress(request, totalBytes, downloadedBytes, progress);
            }
        }
    }

    /**
     * Upload listener
     *
     * @param listener
     */
    public void addUploadListener(UploadListener listener) {
        if (!mUploadListeners.contains(listener))
            mUploadListeners.add(listener);
    }

    public void removeUploadListener(UploadListener listener) {
        if (mUploadListeners.contains(listener))
            mUploadListeners.remove(listener);
    }

    public void onUploadStated(UploadRequest request) {
        if (mUploadListeners != null && !mUploadListeners.isEmpty()) {
            for (UploadListener listener : mUploadListeners) {
                listener.onUploadStarted(request);
            }
        }
    }

    public void onUploadComplete(UploadRequest request, String response) {
        if (mUploadListeners != null && !mUploadListeners.isEmpty()) {
            for (UploadListener listener : mUploadListeners) {
                listener.onUploadComplete(request, response);
            }
        }
    }

    public void onUploadProgress(UploadRequest request, long totalBytes, long uploadedBytes, int progress, long speed) {
        if (mUploadListeners != null && !mUploadListeners.isEmpty()) {
            for (UploadListener listener : mUploadListeners) {
                listener.onProgress(request, totalBytes, uploadedBytes, progress, speed);
            }
        }
    }

    public void onCreateVideoSuccess(UploadRequest request, CreateVideoResponse response) {
        if (mUploadListeners != null && !mUploadListeners.isEmpty()) {
            for (UploadListener listener : mUploadListeners) {
                listener.onCreateVideoSuccess(request, response);
            }
        }
    }

    public void onCreateVideoFailed(UploadRequest request, String error) {
        if (mUploadListeners != null && !mUploadListeners.isEmpty()) {
            for (UploadListener listener : mUploadListeners) {
                listener.onCreateVideoFailed(request, error);
            }
        }
    }
}
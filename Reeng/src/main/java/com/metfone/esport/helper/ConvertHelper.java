/*
 * Created by admin on 2020/9/3
 *
 */

package com.metfone.esport.helper;

import com.metfone.esport.entity.repsonse.ListCommentEntity;

import org.json.JSONObject;

public class ConvertHelper {

    public static ListCommentEntity convertToLiveMessage(JSONObject jsonObject) {
        ListCommentEntity result = null;
        if (jsonObject != null) {
            result = new ListCommentEntity();
            result.setId(jsonObject.optString("idcmt"));
            result.setContent(jsonObject.optString("message"));
            result.setRoomId(jsonObject.optString("roomId"));
            result.setAvatar(jsonObject.optString("avatar"));
            result.setType(jsonObject.optInt("type"));
            result.setIsLike(jsonObject.optInt("isLike"));
            result.setNameSender(jsonObject.optString("from"));
            result.setLevelMessage(jsonObject.optInt("levelMessage"));
            result.setCountLike(jsonObject.optLong("countLike"));
            result.setTimestamp(jsonObject.optLong("timestamp"));
            result.setRowId(jsonObject.optString("rowId"));
            result.setData(jsonObject.optString("data"));
        }
        return result;
    }
}

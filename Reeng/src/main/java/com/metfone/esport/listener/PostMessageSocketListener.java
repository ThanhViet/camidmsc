/*
 * Created by admin on 2020/9/3
 *
 */

package com.metfone.esport.listener;

public interface PostMessageSocketListener {
    void onPostMessageSuccess();
    void onPostMessageFailure();
}

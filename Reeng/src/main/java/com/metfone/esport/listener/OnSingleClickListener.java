/*
 * Created by admin on 2020/9/6
 */

package com.metfone.esport.listener;

import android.annotation.SuppressLint;
import android.os.SystemClock;
import android.view.View;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.Utilities;

public abstract class OnSingleClickListener extends com.library.listener.OnSingleClickListener {

    private long lastTimeClick = 0L;

    public boolean isCheckLogin() {
        return false;
    }

    public boolean isCheckNetwork() {
        return false;
    }

    public boolean isLockTime() {
        return true;
    }

    public abstract void onSingleClick(View view);

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View view) {
        if (isCheckNetwork()) {
            //check network
            if (!NetworkUtils.isConnected()) {
                if (view != null && view.getContext() instanceof BaseActivity) {
                    //todo call toast from activity
                    BaseActivity activity = (BaseActivity) view.getContext();
                    activity.showToast(activity.getString(R.string.error_internet_disconnect));
                } else {
                    ToastUtils.showShort(R.string.error_internet_disconnect);
                }
                return;
            }
        }
        if (isCheckLogin()) {
            //check login
            if (!AccountBusiness.getInstance().isLogin()) {
                if (view != null && view.getContext() instanceof BaseActivity) {
                    //todo show dialog request login from activity
                    BaseActivity activity = (BaseActivity) view.getContext();
                    activity.showDialogLogin();
                } else {
                    ToastUtils.showShort(R.string.content_popup_login);
                }
                return;
            }
        }
        if (isLockTime()) {
            long currentTime = SystemClock.uptimeMillis();
            long deltaTime = Math.abs(currentTime - lastTimeClick);
            lastTimeClick = currentTime;
            if (deltaTime < 800) return;
        }
        onSingleClick(view);
    }

}

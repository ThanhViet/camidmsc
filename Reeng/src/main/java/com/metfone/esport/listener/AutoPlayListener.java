package com.metfone.esport.listener;

public interface AutoPlayListener {
    void onPrepared();
    void onError();
    void onComplete();
    void onPlayerStateChanged(boolean isPlaying);
}

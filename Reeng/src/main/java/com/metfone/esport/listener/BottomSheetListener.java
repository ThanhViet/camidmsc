package com.metfone.esport.listener;

public interface BottomSheetListener {
    void onItemClick(int actionTag, Object item);
}

package com.metfone.esport.donate.fragment;

import static com.metfone.esport.common.Common.handleException;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.blankj.utilcode.util.JsonUtils;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.metfone.esport.network.donate.response.DonatePackageModel;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.module.metfoneplus.dialog.MPSupportStoreDialog;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class DonateDialogOtpVerifyFragment extends DialogFragment implements View.OnClickListener/*, ClickListener.IconListener */{
    public static final String TAG = MPSupportStoreDialog.class.getSimpleName();
    private static final int TIME_DEFAULT = 60;
    @BindView(R.id.etOtp)
    PinEntryEditText etOtp;
    @BindView(R.id.ivWrongOTP)
    CamIdTextView ivWrongOTP;
    @BindView(R.id.txt_resend_opt)
    AppCompatTextView tvResendOtp;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    @BindView(R.id.btnDonate)
    Button btnDonate;
    @BindView(R.id.txt_donate_number)
    AppCompatTextView tvDonateNumber;

    private Context mContext;
    Unbinder unbinder;
    private Handler mHandler;
    int remain = 0;
    private String phone;
    private String otp = "";
    private ClickListener.IconListener mClickHandler;
    private BaseSlidingFragmentActivity baseSlidingFragmentActivity;
    private DonateDialogOtpVerifyFragment.ButtonOnClickListener mButtonOnClickListener;
    private static boolean handlerflag = false;
    private boolean isSaveInstanceState = false;
    private boolean checkResend = false;

    DonatePackageModel mDonatePackage;
    private String donateComment = "";

    public void setDonatePackage(DonatePackageModel donatePackage, String number, String cmt) {
        this.mDonatePackage = donatePackage;
        this.donateComment = cmt;
        this.phone = number;
    }

    public static DonateDialogOtpVerifyFragment newInstance() {
        DonateDialogOtpVerifyFragment dialog = new DonateDialogOtpVerifyFragment();
        return dialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        handlerflag = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dialog_donate_verify_otp, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        setCancelable(false);
        setViewListeners();
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        getDialog().setCanceledOnTouchOutside(false);
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.WHITE);
        etOtp.setTextColor(Color.WHITE);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (getDialog() != null && getDialog().getWindow() != null) {
                getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        } catch (Exception e) {
            handleException(e);
        }
        setCancelable(true);
    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);

        etOtp.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 6) {
                otp = etOtp.getText().toString();
                btnDonate.setEnabled(true);
            }
        });

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() < 6) {
                    if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                        ivWrongOTP.setVisibility(View.GONE);
                    }
                    btnDonate.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etOtp.setOnKeyListener((v, keyCode, event) -> {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                //this is for backspace
                if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                    ivWrongOTP.setVisibility(View.GONE);
                }
            }
            return false;
        });
    }

    private void startCountDown(int time) {
        etOtp.setEnabled(true);
        remain = time;
        if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                remain--;
                if (remain > 30) {
                    tvResendOtp.setEnabled(false);
                    tvResendOtp.setTypeface(null, Typeface.ITALIC);
                    tvResendOtp.setTextColor(Color.parseColor("#80ffffff"));
                } else {
                    tvResendOtp.setEnabled(true);
                    tvResendOtp.setTextColor(Color.parseColor("#266B9E"));
                    tvResendOtp.setTypeface(null, Typeface.NORMAL);
                }

                if (remain == 0) {
                    btnDonate.setEnabled(false);
                    etOtp.setEnabled(false);
                    if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                        ivWrongOTP.setVisibility(View.GONE);
                    }
                    if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
                }
                if (remain > 0) {
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                } else {
                    remain = 0;
                }
                if (remain > 1)
                    tvTimeResendOtp.setText(getString(R.string.donate_seconds, remain));
                else
                    tvTimeResendOtp.setText(getString(R.string.donate_second, remain));
            }
        }, 300);
    }

    public void generateOtpByServer() {
        new MetfonePlusClient().wsGetOTPDonate(phone, new MPApiCallback<WsGetOtpResponse>() {
            @Override
            public void onResponse(Response<WsGetOtpResponse> response) {
                if (response.body().getResult().getErrorCode().equals("0")) {
                    if (checkResend) {
                        checkResend = false;
                        ivWrongOTP.setVisibility(View.VISIBLE);
                        if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                            ivWrongOTP.setText(getString(R.string.mc_resend_success));
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ivWrongOTP.setVisibility(View.GONE);
                                }
                            }, 3000);
                        }
                    }
                    startSmsUserConsent();
                }
                android.util.Log.d("TAG", "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(), etOtp);
                    }
                }, 300);
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());
        Task task = client.startSmsUserConsent(null);
        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etOtp.requestFocus();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                UIUtil.showKeyboard(getContext(), etOtp);
            }
        }, 300);
        view.findViewById(R.id.btnCancle)
                .setOnClickListener(this);
        view.findViewById(R.id.txt_resend_opt)
                .setOnClickListener(this);
        view.findViewById(R.id.btnDonate)
                .setOnClickListener(this);
        String number = Long.parseLong(phone)+"";
        ((AppCompatTextView) view.findViewById(R.id.txt_donate_number)).setText(getString(R.string.donate_otp_has_been_sent_to, number));
    }

    public void addButtonOnClickListener(DonateDialogOtpVerifyFragment.ButtonOnClickListener buttonOnClickListener) {
        this.mButtonOnClickListener = buttonOnClickListener;
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(etOtp, mContext);
        dismissAllowingStateLoss();
        mClickHandler = null;
        handlerflag = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancle:
                if (mButtonOnClickListener != null) {
                    mButtonOnClickListener.onClickBackFromOTP(mDonatePackage, donateComment, phone);
                }
                dismissAllowingStateLoss();
                break;
            case R.id.txt_resend_opt:
                checkResend = true;
                etOtp.setText("");
                etOtp.setSingleCharHint("-");
                etOtp.requestFocus();
                ivWrongOTP.setVisibility(View.GONE);
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            case R.id.btnDonate:
                if (mButtonOnClickListener != null) {
                    mButtonOnClickListener.onClickDonate(mDonatePackage, donateComment, phone, otp);
                }
                UIUtil.hideKeyboard(mContext, etOtp);
                break;
        }
    }

    public void invalidateOTP() {
        etOtp.setText("");
        etOtp.setSingleCharHint("-");
        ivWrongOTP.setVisibility(View.VISIBLE);
        if (ivWrongOTP.getVisibility() == View.VISIBLE) {
            ivWrongOTP.setText(getString(R.string.wrong_otp));
        }
        btnDonate.setEnabled(false);
    }

    public void dismissOTP() {
        dismissAllowingStateLoss();
    }
    public interface ButtonOnClickListener {
        default void onClickDonate(DonatePackageModel packageModel, String cmt, String number, String otp) {
        }
        default void onClickBackFromOTP(DonatePackageModel packageModel, String cmt, String number) {
        }
    }

    @Override
    public void onResume() {
        etOtp.requestFocus();
        UIUtil.showKeyboard(getContext(), etOtp);
        if (!handlerflag && remain > 0) {
            startCountDown(remain);
        }
        isSaveInstanceState = false;
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }

    }
}

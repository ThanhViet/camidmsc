package com.metfone.esport.donate.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.esport.donate.fragment.DonateFragment;
import com.metfone.esport.donate.fragment.DonateHomeDialog;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;

import java.util.List;

public class DonateTypeAdapter extends FragmentStatePagerAdapter {
    private final static int SIZE_VIEW_PAGER_DONATE = 2;

    private String[] mTitleBalance;
    private List<DonateFragment> mFragmentList;

    public DonateTypeAdapter(@NonNull FragmentManager fm, int behavior, String[] titleBalance, List<DonateFragment> fragmentList) {
        super(fm, behavior);
        this.mTitleBalance = titleBalance;
        this.mFragmentList = fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return SIZE_VIEW_PAGER_DONATE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleBalance[position];
    }

}

package com.metfone.esport.donate.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.esport.network.donate.response.DonatePackageModel;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

import java.util.ArrayList;

public class GiftAdapter extends RecyclerView.Adapter<GiftAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<DonatePackageModel> donatePackages = new ArrayList<>();
    private int type;

    public GiftAdapter(Context context, int type) {
        this.mContext = context;
        this.type = type;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateData(ArrayList<DonatePackageModel> list) {
        this.donatePackages = new ArrayList<>();
        this.donatePackages = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_donate_gift, parent, false);
        return new ViewHolder(view);
    }

    String Points = ResourceUtils.getString(R.string.donate_points, "");
    int selected_position = 0;
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DonatePackageModel itemData = donatePackages.get(position);
        Glide.with(mContext)
                .asBitmap().error(R.drawable.star)
                .load(itemData.getIconUrl())
                .fitCenter()
                .into(holder.ivDonatePackage);
        holder.tvStarDonate.setText(itemData.getCoin()+"");
        if (type == 1) {
            holder.tvValueDonate.setText((itemData.getPrice() % 1) == 0 ? "$"+(int)itemData.getPrice() : String.format("$%.2f", itemData.getPrice()));
        }
        if (type == 2) {
            holder.tvValueDonate.setText(ResourceUtils.getString(R.string.donate_points, (int)itemData.getPrice()));
        }
        holder.itemView.setBackgroundResource(selected_position == position ? R.drawable.bg_boder_donate_gift_selected : R.drawable.bg_boder);
        holder.lnMoneyDonate.setBackgroundResource(selected_position == position ? R.drawable.bg_boder_donate_gift_red: R.drawable.bg_boder_donate_gift_violet);
    }

    @Override
    public int getItemCount() {
        return donatePackages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout lnMoneyDonate;
        TextView tvStarDonate, tvValueDonate;
        ImageView ivDonatePackage;

        ViewHolder(View itemView) {
            super(itemView);
            lnMoneyDonate = itemView.findViewById(R.id.lnValueDonate);
            tvStarDonate = itemView.findViewById(R.id.tvValueStarDonate);
            tvValueDonate = itemView.findViewById(R.id.tvValueDonate);
            ivDonatePackage = itemView.findViewById(R.id.img_donate_package);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            pickItem(getAdapterPosition());
        }
    }

    public void pickItem(int position) {
        if (position == RecyclerView.NO_POSITION) return;
        notifyItemChanged(selected_position);
        selected_position = position;
        if (mClickListener != null) mClickListener.onItemClick(donatePackages.get(selected_position));
        notifyItemChanged(selected_position);
    }
    private ItemClickListener mClickListener;

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(DonatePackageModel packageModel);
    }
}

package com.metfone.esport.donate.fragment;

import static com.metfone.esport.common.Common.convertDateToString;
import static com.metfone.esport.common.Common.handleException;
import static com.metfone.esport.common.Common.hideKeyBoard;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.donate.adapter.GiftAdapter;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.network.donate.response.DonatePackageModel;
import com.metfone.esport.ui.live.BottomFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Le Viet Thanh on 11/4/21.
 */
public class DonateFragment extends Fragment {
    @Nullable
    @BindView(R.id.rcDonatePackage)
    RecyclerView rcDonatePackage;
    @Nullable
    @BindView(R.id.btnDonate)
    Button btnDonate;
    @Nullable
    @BindView(R.id.edtInputDonatePhone)
    EditText edtInputDonatePhone;
    @Nullable
    @BindView(R.id.edtInputDonateComment)
    EditText edtInputDonateComment;
    @Nullable
    @BindView(R.id.ivAvatarComment)
    AvatarView ivAvatarComment;

    private final String TAG = "DonateFragment";
    private Unbinder unbinder;
    public static final int DONATE_TYPE_METFONE = 1;
    public static final int DONATE_TYPE_POINT = 2;

    private OnClickDonateFragmentListener mListener;
    private GiftAdapter adapter;

    private int typeDonate;

    private int getLayout() {
        return this.typeDonate == DONATE_TYPE_METFONE ? R.layout.fragment_dialog_donate_tab_money : R.layout.fragment_dialog_donate_tab_point;
    }

    DonatePackageModel selectedDonatePackage;
    GiftAdapter.ItemClickListener listener = new GiftAdapter.ItemClickListener() {
        @Override
        public void onItemClick(DonatePackageModel item) {
            selectedDonatePackage = item;
            String ss = getString(R.string.donate_donate_for) + " $0";
            if (typeDonate == DONATE_TYPE_METFONE) {
                ss = getString(R.string.donate_donate_for, String.format("$%.2f", item.getPrice()));
                ss = getString(R.string.donate_donate_for, ((item.getPrice() % 1) == 0 ? "$"+(int)item.getPrice() : String.format("$%.2f", item.getPrice())));
            } else if (typeDonate == DONATE_TYPE_POINT) {
                ss = getString(R.string.donate_donate_for, getString(R.string.donate_points, (int)item.getPrice()+""));
            }
            if (btnDonate != null) {
                btnDonate.setText(ss);
            }
        }
    };

    @Override
    @Nullable
    public View onCreateView(@NotNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        try {
            unbinder = ButterKnife.bind(this, view);
            initView();
        } catch (Exception e) {
            handleException(e);
        }
        return view;
    }

    private void initView() {
        if (btnDonate != null) {
            btnDonate.setEnabled(false);
        }
        adapter = new GiftAdapter(getContext(), this.typeDonate);
        adapter.setClickListener(listener);
        assert rcDonatePackage != null;
        rcDonatePackage.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rcDonatePackage.setAdapter(adapter);
    }


    public void reloadData(int posPackage, String cmt, String number, boolean focusNumber) {
        if (rcDonatePackage != null) {
            if (posPackage > 0 && posPackage < Objects.requireNonNull(rcDonatePackage.getAdapter()).getItemCount()-1){
                rcDonatePackage.scrollToPosition(posPackage-1);
            } else rcDonatePackage.scrollToPosition(posPackage);
        }
        adapter.pickItem(posPackage);
        if (edtInputDonateComment != null) {
            edtInputDonateComment.setText(cmt);
        }
        if (edtInputDonatePhone != null) {
            edtInputDonatePhone.setText(number);
        }
        if (!number.equals("")) {
            edtInputDonatePhone.setSelection(0, number.length());
        }
        if (focusNumber) {
            if (edtInputDonatePhone != null) {
                edtInputDonatePhone.requestFocus();
                KeyboardUtils.showSoftInput(edtInputDonatePhone);
            }
        } else hideKeyBoard(getContext());

    }

    public void go2Point(int posPackage, String cmt){
        if (rcDonatePackage != null) {
            if (posPackage > 0 && posPackage < Objects.requireNonNull(rcDonatePackage.getAdapter()).getItemCount()-1){
                rcDonatePackage.scrollToPosition(posPackage-1);
            } else rcDonatePackage.scrollToPosition(posPackage);
        }
        adapter.pickItem(posPackage);
        if (edtInputDonateComment != null) {
            edtInputDonateComment.setText(cmt);
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void updateData(ArrayList<DonatePackageModel> list) {

        if (btnDonate != null) {
            btnDonate.setEnabled(true);
        }
        if (list != null && list.size() > 0) {
            selectedDonatePackage = list.get(0);
            String ss = getString(R.string.donate_donate_for) + "$0";
            if (typeDonate == DONATE_TYPE_METFONE) {
                ss = getString(R.string.donate_donate_for, ((list.get(0).getPrice() % 1) == 0 ? "$"+(int)list.get(0).getPrice() : String.format("$%.2f", list.get(0).getPrice())));
            } else if (typeDonate == DONATE_TYPE_POINT) {
                ss = getString(R.string.donate_donate_for, getString(R.string.donate_points, (int)list.get(0).getPrice()+""));
            }
            if (btnDonate != null) {
                btnDonate.setText(ss);
            }
        }

        if (adapter != null) adapter.updateData(list);

    }

    private void initData() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
        UserInfo currentUser = userInfoBusiness.getUser();
        String name = "";
        if (AccountBusiness.getInstance().isLogin()) {
            name = !SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false) ? currentUser.getFull_name() : AccountBusiness.getInstance().getChannelInfo().name;
            if (edtInputDonateComment != null) {
                edtInputDonateComment.setHint(getString(R.string.send_a_message, name));
            }
        } else if (edtInputDonateComment != null) {
            edtInputDonateComment.setHint(getString(R.string.send_a_message_hint));
        }
        setUpAvatar();
        String ss = "";
        if (typeDonate == DONATE_TYPE_METFONE) {
            ss = getString(R.string.donate_donate_for) + "$0";
        } else if (typeDonate == DONATE_TYPE_POINT) {
            ss = getString(R.string.donate_donate_for) + getString(R.string.donate_points, "0");
        }
        if (btnDonate != null) {
            btnDonate.setText(ss);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public DonateFragment(int type, OnClickDonateFragmentListener listener) {
        this.typeDonate = type;
        this.mListener = listener;
    }

    public static DonateFragment newInstance(int type, OnClickDonateFragmentListener listener) {
        return new DonateFragment(type, listener);
    }

    @OnClick({R.id.btnDonate})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.btnDonate) {
            if (!checkPhoneNumber()){
                return;
            }
            assert edtInputDonateComment != null;
            donateComment = edtInputDonateComment.getText().toString();
            if (mListener != null)
                mListener.onClickDonateFragment(selectedDonatePackage, donatePhoneNumber, donateComment);
        }
    }

    private String donatePhoneNumber = "";
    private String donateComment = "";

    public void focusValidate() {
        assert edtInputDonatePhone != null;
        edtInputDonatePhone.requestFocus();
    }

    public static boolean isDefaultNumberAcc = true;
    public static boolean isRequireChangeNumber = false;
    private boolean checkPhoneNumber() {

        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
        if (AccountBusiness.getInstance().isLogin()) {
            donatePhoneNumber = ApplicationController.self().getCamIdUserBusiness().getMetfoneUsernameIsdn();
            if (StringUtils.isEmpty(donatePhoneNumber)) {
                donatePhoneNumber = userInfoBusiness.getUser().getPhone_number();
            }
        }

        if (this.typeDonate == DONATE_TYPE_POINT) {
            if (AccountBusiness.getInstance().isLogin()) {
                if (StringUtils.isEmpty(donatePhoneNumber)) {
                    if (mListener != null)
                        mListener.onClickDonateFragmentRequireAddPhone(selectedDonatePackage, donateComment);
                    return false;
                }
            }
            return true;
        }
        if (this.typeDonate == DONATE_TYPE_METFONE) {
            if (AccountBusiness.getInstance().isLogin()) {
                if (StringUtils.isEmpty(donatePhoneNumber)) {
                    isRequireChangeNumber = true;
                } else isRequireChangeNumber = false;
            }
            assert edtInputDonatePhone != null;
            if (StringUtils.isNotEmpty(edtInputDonatePhone.getText())) {
                isRequireChangeNumber = true;
                if (donatePhoneNumber.equals(edtInputDonatePhone.getText().toString())) {
                    isDefaultNumberAcc = true;
                } else {
                    donatePhoneNumber = edtInputDonatePhone.getText().toString();
                    isDefaultNumberAcc = false;
                }
            } else {
                isRequireChangeNumber = false;
                isDefaultNumberAcc = true;
            }
            if (StringUtils.isEmpty(donatePhoneNumber)) { // luc chua nhap sdt/tk chua co sdt

                if (edtInputDonateComment != null) {
                    donateComment = edtInputDonateComment.getText().toString();
                }
                if (mListener != null)
                    mListener.onClickDonateFragmentRequireNumber(selectedDonatePackage, donateComment);
                return false;
            }
        }
        return true;
    }

    public interface OnClickDonateFragmentListener {
        void onClickDonateFragment(DonatePackageModel selectedDonatePackage, String number, String comment);
        void onClickDonateFragmentRequireNumber(DonatePackageModel selectedDonatePackage, String comment);
        void onClickDonateFragmentRequireAddPhone(DonatePackageModel selectedDonatePackage, String comment);
    }

    private void setUpAvatar() {
        try {
            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                UserInfo currentUser = userInfoBusiness.getUser();
                if (TextUtils.isEmpty(currentUser.getAvatar())) {
                    showAvatarCamid();
                } else {
                    Glide.with(this)
                            .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                            .transform(new MultiTransformation<Bitmap>(new CenterCrop(), new CircleCrop()))
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(ivAvatarComment);
                }
            } else {
                if (AccountBusiness.getInstance().getChannelInfo() != null) {
                    if (StringUtils.isNotEmpty(AccountBusiness.getInstance().getChannelInfo().urlAvatar)) {
                        ivAvatarComment.setAvatarUrl(AccountBusiness.getInstance().getChannelInfo().urlAvatar);
                    } else {
                        ivAvatarComment.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_avatar_member));
                    }
                } else {
                    showAvatarCamid();
                }
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void showAvatarCamid() {
        try {
            AccountRankDTO account = ApplicationController.self().getAccountRankDTO();
            KhUserRank myRank = KhUserRank.getById(account.rankId);
            Glide.with(this)
                    .load(myRank.resAvatar)
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_member)
                    .error(R.drawable.ic_avatar_member)
                    .into(ivAvatarComment);
        } catch (Exception e) {
            handleException(e);
        }
    }
}

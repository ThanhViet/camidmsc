package com.metfone.esport.donate.fragment;

import static com.metfone.esport.common.Common.getAvatarAccount;
import static com.metfone.esport.common.Common.handleException;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.metfone.esport.network.donate.response.DonatePackageModel;
import com.metfone.selfcare.R;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.annotation.Resource;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Le Viet Thanh on 11/4/21.
 */
public class DonateNotifyDialog extends DialogFragment implements View.OnClickListener {

    private final String TAG = "DonateNotifyDialog";
    public static final int NOTIFY_REQUIRE_NUMBER = 1;
    public static final int NOTIFY_NOT_ENOUGH_BALANCE = 2;
    public static final int NOTIFY_NOT_ENOUGH_POINT = 3;
    public static final int NOTIFY_NOT_METFONE_NUMBER_DEFAULT = 4;
    public static final int NOTIFY_NOT_METFONE_NUMBER_INPUT = 5;
    public static final int NOTIFY_ADD_PHONE = 6;
    public static final int NOTIFY_CHANGE_NUMBER = 7;
    public static final int NOTIFY_SYSTEM_ERROR = 8;
    public static final int NOTIFY_OVER_OTP = 9;
    public static final int NOTIFY_CONFIRM_DONATE_POINT = 10;
    public static final int NOTIFY_WAIT_GET_OTP = 11;
    private Unbinder unbinder;
    private int type;
    private boolean isCanceledOnTouchOutside = false;
    private String unit = "";
    private int remainTime = 0;

    private int getLayout() {
        if (type == NOTIFY_NOT_METFONE_NUMBER_DEFAULT
                || type == NOTIFY_NOT_METFONE_NUMBER_INPUT
                || type == NOTIFY_REQUIRE_NUMBER
                || type == NOTIFY_CHANGE_NUMBER
                || type == NOTIFY_SYSTEM_ERROR
                || type == NOTIFY_OVER_OTP
                || type == NOTIFY_WAIT_GET_OTP)
            return R.layout.fragment_dialog_donate_notify_notmetfone;
        return R.layout.fragment_dialog_donate_notify_notenough;
    }

    @Override
    @Nullable
    public View onCreateView(@NotNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(isCanceledOnTouchOutside);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        View rootView = requireActivity().getLayoutInflater().inflate(getLayout(), container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (type == NOTIFY_NOT_ENOUGH_BALANCE || type == NOTIFY_NOT_ENOUGH_POINT
                || type == NOTIFY_ADD_PHONE || type == NOTIFY_CONFIRM_DONATE_POINT) {
            ((AppCompatTextView) view.findViewById(R.id.tvCancelButton)).setOnClickListener(this);
            ((Button) view.findViewById(R.id.btnGo2TopUp)).setOnClickListener(this);
        } else
            ((Button) view.findViewById(R.id.btnContinue)).setOnClickListener(this);

        if (type == NOTIFY_NOT_ENOUGH_BALANCE) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleSuggest)).setText(getString(R.string.donate_your_account_balance_is_not_enough));
            ((AppCompatTextView) view.findViewById(R.id.tvSuggest)).setText(getString(R.string.donate_please_add_more_money_to_your_account));
        }

        if (type == NOTIFY_NOT_ENOUGH_POINT) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleSuggest)).setText(getString(R.string.donate_your_point_is_not_enough));
            ((AppCompatTextView) view.findViewById(R.id.tvSuggest)).setText(getString(R.string.donate_please_top_up_now_to_get_more_point));
        }

        if (type == NOTIFY_ADD_PHONE) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleSuggest)).setText(getString(R.string.donate_your_account_need_to_onnect_the_phone_number));
            ((AppCompatTextView) view.findViewById(R.id.tvSuggest)).setText(getString(R.string.donate_please_add_metfone_number_to_your_account));
            ((Button) view.findViewById(R.id.btnGo2TopUp)).setText(getString(R.string.donate_connect_now));
        }

        if (type == NOTIFY_CONFIRM_DONATE_POINT) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleSuggest)).setText(getString(R.string.donate_donation_confirmation));
            ((AppCompatTextView) view.findViewById(R.id.tvSuggest)).setText(getString(R.string.donate_are_you_sure_that_you_want_to_continue));
            ((Button) view.findViewById(R.id.btnGo2TopUp)).setText(getString(R.string.donate_continue));
        }

        if (type == NOTIFY_NOT_METFONE_NUMBER_DEFAULT) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleVerify)).setText(getString(R.string.donate_this_function_is_for_Metfone_subscriber_only));
            ((AppCompatTextView) view.findViewById(R.id.tvVerify)).setText(getString(R.string.donate_please_add_metfone_number_to_your_account));
        }

        if (type == NOTIFY_NOT_METFONE_NUMBER_INPUT) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleVerify)).setText(getString(R.string.donate_the_phone_number_you_use_is_not_metfone));
            ((AppCompatTextView) view.findViewById(R.id.tvVerify)).setText(getString(R.string.donate_please_enter_your_metfone_mobile_number));
        }

        if (type == NOTIFY_REQUIRE_NUMBER) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleVerify)).setText(getString(R.string.donate_the_phone_number_is_required));
            ((AppCompatTextView) view.findViewById(R.id.tvVerify)).setText(getString(R.string.donate_please_enter_your_metfone_mobile_number));
        }

        if (type == NOTIFY_CHANGE_NUMBER) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleVerify)).setText(getString(R.string.donate_the_phone_number_you_use_does_not_have_enough_balance));
            ((AppCompatTextView) view.findViewById(R.id.tvVerify)).setText(getString(R.string.donate_please_recharge_or_use_another_mobile_number));
        }

        if (type == NOTIFY_SYSTEM_ERROR) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleVerify)).setText(getString(R.string.donate_the_system_is_in_error));
            ((AppCompatTextView) view.findViewById(R.id.tvVerify)).setText(getString(R.string.donate_please_try_again_later));
        }

        if (type == NOTIFY_OVER_OTP) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleVerify)).setText(getString(R.string.donate_you_have_failed_5_times_to_enter_otp));
            ((AppCompatTextView) view.findViewById(R.id.tvVerify)).setText(getString(R.string.donate_please_try_again_in));
            ((AppCompatTextView) view.findViewById(R.id.tvRemainTime)).setVisibility(View.VISIBLE);
            int minutes = (remainTime-1)/60 + 1;
            ((AppCompatTextView) view.findViewById(R.id.tvRemainTime)).setText(getString(R.string.donate_minutes, minutes));
        }

        if (type == NOTIFY_WAIT_GET_OTP) {
            ((AppCompatTextView) view.findViewById(R.id.tvTitleVerify)).setText(getString(R.string.donate_please_try_again_later));
            ((AppCompatTextView) view.findViewById(R.id.tvVerify)).setText(getString(R.string.donate_please_try_again_after));
            ((AppCompatTextView) view.findViewById(R.id.tvRemainTime)).setVisibility(View.VISIBLE);
            if (remainTime > 1)
                ((AppCompatTextView) view.findViewById(R.id.tvRemainTime)).setText(getString(R.string.donate_seconds, remainTime));
            else
                ((AppCompatTextView) view.findViewById(R.id.tvRemainTime)).setText(getString(R.string.donate_second, remainTime));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (getDialog() != null && getDialog().getWindow() != null) {
                getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        } catch (Exception e) {
            handleException(e);
        }
        setCancelable(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();

    }

    private OnClickNotify onClickNotify;

    public DonatePackageModel mPackageInput = null;
    public String mCommentInput = "";
    public String mNumberInput = "";

    public void setInput(DonatePackageModel donatePackageModel, String number, String cmt) {
        this.mPackageInput = donatePackageModel;
        this.mNumberInput = number;
        this.mCommentInput = cmt;
    }

    public void setRemainTime(int s) {
        this.remainTime = s;
    }

    public static DonateNotifyDialog newInstance(int type, OnClickNotify onClickNotify) {

        DonateNotifyDialog dialog = new DonateNotifyDialog();
        dialog.onClickNotify = onClickNotify;
        dialog.type = type;
        dialog.isCanceledOnTouchOutside = !(type == NOTIFY_NOT_ENOUGH_BALANCE || type == NOTIFY_NOT_ENOUGH_POINT || type == NOTIFY_ADD_PHONE);
        return dialog;
    }

    private void initView(View rootView) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCancelButton:
                if (onClickNotify != null) {
                    if (type == NOTIFY_CONFIRM_DONATE_POINT) {
                        onClickNotify.onClickCancelConfirmDonatePoint(mPackageInput, mCommentInput);
                    } else
                        onClickNotify.onClickCancelNotify();
                }
                dismissAllowingStateLoss();
                break;
            case R.id.btnGo2TopUp:
                if (onClickNotify != null) {
                    if (type == NOTIFY_NOT_ENOUGH_BALANCE || type == NOTIFY_NOT_ENOUGH_POINT)
                        onClickNotify.onClickGo2TopUp();
                    if (type == NOTIFY_ADD_PHONE)
                        onClickNotify.onClickGo2AddPhone();
                    if (type == NOTIFY_CONFIRM_DONATE_POINT)
                        onClickNotify.onClickGo2DonatePoint(mPackageInput, mCommentInput);
                }
                dismissAllowingStateLoss();
                break;
            case R.id.btnContinue:
                if (onClickNotify != null) {
                    if (type == NOTIFY_NOT_METFONE_NUMBER_DEFAULT)
                        onClickNotify.onClickGo2AddPhone();
                    else
                        onClickNotify.onClickContinue(mPackageInput, mNumberInput, mCommentInput);
                }
                dismissAllowingStateLoss();
                break;
        }
    }

    public interface OnClickNotify {
        void onClickCancelNotify();
        void onClickCancelConfirmDonatePoint(DonatePackageModel donatePackageModel, String cmt);

        void onClickGo2TopUp();

        void onClickGo2AddPhone();
        void onClickGo2DonatePoint(DonatePackageModel donatePackageModel, String cmt);

        void onClickContinue(DonatePackageModel donatePackageModel, String number, String cmt);
    }
}

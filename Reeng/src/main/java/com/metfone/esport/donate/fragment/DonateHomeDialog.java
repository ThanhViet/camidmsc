package com.metfone.esport.donate.fragment;

import static com.metfone.esport.common.Common.handleException;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.metfone.esport.donate.adapter.DonateTypeAdapter;
import com.metfone.esport.donate.common.DonateViewPager;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.network.donate.DonateESportApiCallback;
import com.metfone.esport.network.donate.DonateESportClient;
import com.metfone.esport.network.donate.response.DonatePackageModel;
import com.metfone.esport.network.donate.response.WsGetAllDonatePackageAppResponse;
import com.metfone.selfcare.R;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.CheckCarrierResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by Le Viet Thanh on 11/4/21.
 */
public class DonateHomeDialog extends DialogFragment implements DonateFragment.OnClickDonateFragmentListener {
    @Nullable
    @BindView(R.id.lnCloseDialog)
    LinearLayout closeBtn;
    @Nullable
    @BindView(R.id.tabLayoutDonate)
    TabLayout tabLayoutDonate;
    @Nullable
    @BindView(R.id.view_pager_donate)
    DonateViewPager viewpagerDonate;

    private final String TAG = "DonateHomeDialog";
    private Unbinder unbinder;
    private String[] titleDonateTabs;
    private DonateFragment donateMoneyFragment, donatePointFragment;
    private List<DonateFragment> donateFragments;
    private DonateTypeAdapter donateTypeAdapter;


    private int getLayout() {
        return R.layout.fragment_dialog_donate_home;
    }

    @Override
    @Nullable
    public View onCreateView(@NotNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        View rootView = requireActivity().getLayoutInflater().inflate(getLayout(), container, false);
        initData();
        try {
            unbinder = ButterKnife.bind(this, rootView);
            initView(rootView);
        } catch (Exception e) {
            handleException(e);
        }
        return rootView;
    }

    private void initData() {
        getAllDonatePackage();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (getDialog() != null && getDialog().getWindow() != null) {
                getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            }
        } catch (Exception e) {
            handleException(e);
        }
        setCancelable(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private OnClickDonateHomeListener mListener;

    public DonatePackageModel mPackageInput = null;
    public String mCommentInput = "";
    public String mNumberInput = "";
    public boolean isReloadFragment = false;
    public boolean isFocusNumber = false;
    public boolean isGo2Point = false;

    public void setInput(DonatePackageModel donatePackageModel, String number, String cmt, boolean isReload, boolean isFocusNumber) {
        this.mPackageInput = donatePackageModel;
        this.mNumberInput = number;
        this.mCommentInput = cmt;
        this.isReloadFragment = isReload;
        this.isFocusNumber = isFocusNumber;
    }

    public void go2Point(DonatePackageModel donatePackageModel, String cmt) {
        this.mPackageInput = donatePackageModel;
        this.mCommentInput = cmt;
        this.isGo2Point = true;
    }

    public static DonateHomeDialog newInstance(OnClickDonateHomeListener listener) {
        DonateHomeDialog dialog = new DonateHomeDialog();
        dialog.mListener = listener;
        return dialog;
    }

    private void initView(View rootView) {
        initTabLayout();
    }

    private void initTabLayout() {
        titleDonateTabs = getActivity().getResources().getStringArray(R.array.donate_type);
        donateMoneyFragment = DonateFragment.newInstance(DonateFragment.DONATE_TYPE_METFONE, this);
        donatePointFragment = DonateFragment.newInstance(DonateFragment.DONATE_TYPE_POINT, this);
        donateFragments = new ArrayList<>();
        donateFragments.add(donateMoneyFragment);
        donateFragments.add(donatePointFragment);
        donateTypeAdapter = new DonateTypeAdapter(getChildFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                titleDonateTabs,
                donateFragments);
        viewpagerDonate.setAdapter(donateTypeAdapter);
        viewpagerDonate.setOffscreenPageLimit(2);
        tabLayoutDonate.setupWithViewPager(viewpagerDonate);
        if (isGo2Point) {
            viewpagerDonate.setCurrentItem(1);
        } else
            viewpagerDonate.setCurrentItem(0);
        tabLayoutDonate.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabUnselected: ");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabReselected: ");
            }
        });
    }

    @OnClick({R.id.lnCloseDialog})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lnCloseDialog:
                if (mListener != null) mListener.onClickDonateGo2Close();
                dismissAllowingStateLoss();
                break;
        }
    }

    public void isMetfoneNumber(DonatePackageModel donatePackage, String number, String comment) {
        generateOtpByServer(donatePackage, number, comment);
        dismissAllowingStateLoss();
    }

    public void generateOtpByServer(DonatePackageModel donatePackage, String number, String comment) {
        new MetfonePlusClient().wsGetOTPDonate(number, new MPApiCallback<WsGetOtpResponse>() {
            @Override
            public void onResponse(Response<WsGetOtpResponse> response) {
                if (response.body().getResult().getErrorCode().equals("0")) {
                    if (mListener != null) {
                        mListener.onClickDonateGo2ReceiveOTP();
                    }
                    if (mListener != null) {
                        mListener.onClickDonateGo2VerifyOTP(donatePackage, number, comment);
                    }
                } else {
                    if (response.body().getResult().getErrorCode().equals("1")
                            && (response.body().getResult().getWsResponse() != null)) {
                        String jsonInString = new Gson().toJson(response.body().getResult().getWsResponse());
                        String ss = "0";
                        String des = "";
                        try {
                            JSONObject mJSONObject = new JSONObject(jsonInString);
                            ss =  mJSONObject.getString("remainTime");
                            des =  mJSONObject.getString("description");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (des.equals("request.resend.otp.fail") && mListener != null) {
                            mListener.onClickDonateGo2WaitOTP(donatePackage, number, comment, Integer.parseInt(ss));
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    @Override
    public void onClickDonateFragment(DonatePackageModel donatePackage, String number, String comment) {
        if (mListener != null) {
            if (mListener.onClickDonateGo2CheckOverOTP(number)) return;
        }
        if (donatePackage.getUnit().equals("USD")) {
            checkPhoneNumber(donatePackage, number, comment);
        } else {
            if (!AccountBusiness.getInstance().isLogin()) {
                if (mListener != null) {
                    mListener.onClickDonateGo2Login(donatePackage, number);
                }
            } else {
                if (mListener != null) {
                    mListener.onClickDonateGo2DonateByPoints(donatePackage, comment);
                }
            }
            dismissAllowingStateLoss();
        }
    }

    @Override
    public void onClickDonateFragmentRequireNumber(DonatePackageModel selectedDonatePackage, String comment) {
        if (mListener != null) {
            mListener.onClickDonateGo2RequireNumber(selectedDonatePackage, comment);
        }
        dismissAllowingStateLoss();
    }

    @Override
    public void onClickDonateFragmentRequireAddPhone(DonatePackageModel selectedDonatePackage, String comment) {
        if (mListener != null) {
            mListener.onClickDonateGo2RequireAddPhone(selectedDonatePackage, comment);
        }
        dismissAllowingStateLoss();
    }

    // thanhlv - check metfone number
    private void checkPhoneNumber(DonatePackageModel donatePackage, String phoneNumber, String comment) {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.checkCarrier(phoneNumber, new ApiCallback<CheckCarrierResponse>() {
            @Override
            public void onResponse(Response<CheckCarrierResponse> response) {
                if (response.body() != null) {
                    String code = response.body().getCode();
                    Log.d(TAG, "onResponse: " + response.body().getMessage());

                    if ("00".equals(code)) {
                        if (response.body().getData().getMetfone()) {
                            // thanhlv - is metfone number
                            isMetfoneNumber(donatePackage, phoneNumber, comment);
                        } else {
                            isNotMetfoneNumber(donatePackage, phoneNumber, comment);
                        }
                    } else {
                        ToastUtils.showToast(getActivity(), response.body().getMessage());
                        for (Fragment f : getChildFragmentManager().getFragments()) {
                            if (f instanceof DonateFragment) {
                                ((DonateFragment) f).focusValidate();
                                break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "onError: ", error);
            }
        });
    }

    private void isNotMetfoneNumber(DonatePackageModel donatePackage, String number, String cmt) {
        if (mListener != null) {
            mListener.onClickDonateGo2NotMetfone(donatePackage, number, cmt);
        }
        dismissAllowingStateLoss();
    }

    public interface OnClickDonateHomeListener {
        void onClickDonateGo2NotMetfone(DonatePackageModel donatePackage, String number, String cmt);
        void onClickDonateGo2RequireAddPhone(DonatePackageModel donatePackage, String cmt);
        void onClickDonateGo2RequireNumber(DonatePackageModel donatePackage, String cmt);
        void onClickDonateGo2DonateByPoints(DonatePackageModel donatePackage, String cmt);
        void onClickDonateGo2VerifyOTP(DonatePackageModel donatePackage, String number, String cmt);
        void onClickDonateGo2WaitOTP(DonatePackageModel donatePackage, String number, String cmt, int remainTime);
        void onClickDonateGo2ReceiveOTP();
        void onClickDonateGo2Login(DonatePackageModel donatePackage, String number);
        void onClickDonateGo2Close();
        boolean onClickDonateGo2CheckOverOTP(String number);
    }

    public void onSuccessGetAllDonatePackage(List<DonatePackageModel> list) {
        ArrayList<DonatePackageModel> donatePackages = new ArrayList<DonatePackageModel>(list);
        Collections.sort(donatePackages, new Comparator<DonatePackageModel>() {
            @Override
            public int compare(DonatePackageModel lhs, DonatePackageModel rhs) {
                return Double.compare(lhs.getPrice(), rhs.getPrice());
            }
        });
        ArrayList<DonatePackageModel> donatePackagesMoney = new ArrayList<DonatePackageModel>();
        ArrayList<DonatePackageModel> donatePackagesPoint = new ArrayList<DonatePackageModel>();
        for (DonatePackageModel item : donatePackages) {
            if (item.getPaymentMethod() != null && item.getPaymentMethod().equals("UNIT_METFONE"))
                donatePackagesMoney.add(item);
            if (item.getPaymentMethod() != null && item.getPaymentMethod().equals("UNIT_POINT"))
                donatePackagesPoint.add(item);
        }
        donateMoneyFragment.updateData(donatePackagesMoney);
        donatePointFragment.updateData(donatePackagesPoint);
        if (isGo2Point) {
            int pos = 0;
            for (int i = 0; i < donatePackagesPoint.size(); i++)
                if (mPackageInput.getId() == donatePackagesPoint.get(i).getId()) {
                    pos = i;
                    break;
                }
            donatePointFragment.go2Point(pos, mCommentInput);
        }
        if (isReloadFragment) {
            isReloadFragment = false;
            int pos = 0;
            for (int i = 0; i < donatePackagesMoney.size(); i++)
                if (mPackageInput.getId() == donatePackagesMoney.get(i).getId()) {
                    pos = i;
                    break;
                }
            donateMoneyFragment.reloadData(pos, mCommentInput, mNumberInput, isFocusNumber);
        }
    }

    //thanhlv - get all donate package
    public void getAllDonatePackage() {
        new DonateESportClient().wsGetAllDonatePackageApp(new DonateESportApiCallback<WsGetAllDonatePackageAppResponse>() {
            @Override
            public void onResponse(Response<WsGetAllDonatePackageAppResponse> response) {
                if (response.body() != null && response.body().isSuccessful())
                    onSuccessGetAllDonatePackage(response.body().getResult().getWsResponse());
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }
}

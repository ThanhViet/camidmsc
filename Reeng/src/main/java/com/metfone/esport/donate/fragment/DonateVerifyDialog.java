package com.metfone.esport.donate.fragment;

import static com.metfone.esport.common.Common.handleException;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.android.material.tabs.TabLayout;
import com.metfone.esport.donate.adapter.DonateTypeAdapter;
import com.metfone.esport.donate.common.DonateViewPager;
import com.metfone.selfcare.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Le Viet Thanh on 11/4/21.
 */
public class DonateVerifyDialog extends DialogFragment {


    private final String TAG = "DonateHomeDialog";
    private Unbinder unbinder;



    private int getLayout() {
        return R.layout.fragment_dialog_donate_verify_otp;
    }
    @Override
    @Nullable
    public View onCreateView(@NotNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        View rootView = requireActivity().getLayoutInflater().inflate(getLayout(), container, false);
        try {
            unbinder = ButterKnife.bind(this,rootView);
        } catch (Exception e) {
            handleException(e);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (getDialog() != null && getDialog().getWindow() != null) {
                getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        } catch (Exception e) {
            handleException(e);
        }
        setCancelable(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    private OnClickAccept onClickAccept;

    public static DonateVerifyDialog newInstance(String message, OnClickAccept onClickAccept) {
        DonateVerifyDialog dialog = new DonateVerifyDialog();
        dialog.onClickAccept = onClickAccept;
        return dialog;
    }

    private void initView(View rootView) {

    }


//    @OnClick({R.id.lnCloseDialog})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.lnCloseDialog:
//                dismissAllowingStateLoss();
//                System.out.println("thanhlv on click lnCloseDialog2222222222");
//                break;
//        }
//
//    }

    public interface OnClickAccept {
        void onClickAccept();
    }
}

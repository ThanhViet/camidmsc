package com.metfone.esport.donate;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.manager.SupportRequestManagerFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.module.home_kh.fragment.game.GameFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewards.RewardsKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.RewardsShopKHFragment;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddFeedbackFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPBuyServiceDetailFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPServiceFragment;
import com.metfone.selfcare.module.metfoneplus.ishare.fragment.IShareFragment;
import com.metfone.selfcare.module.metfoneplus.search.fragment.BuyPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.topup.TopupMetfoneFragment;
import com.metfone.selfcare.module.movienew.fragment.ActionCategoryFragment;
import com.metfone.selfcare.module.movienew.fragment.MyListFragment;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContainerActivity extends BaseSlidingFragmentActivity {


    @BindView(R.id.root_frame)
    FrameLayout root_frame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);

        setContentView(R.layout.activity_deep_link);
        ButterKnife.bind(this);

        Fragment fragment = TopupMetfoneFragment.newInstance();
        addFragment(R.id.root_frame, fragment, true);
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
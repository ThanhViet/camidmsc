package com.metfone.esport;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.common.Common;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.common.Common.handleException;

/**
 * Created by Nguyễn Thành Chung  on 9/4/20
 */
public class AloneFragmentActivity extends BaseActivity<Object> {

    private static String ARGS = "ARGS";
    private static String FRAGMENT_NAME = "FRAGMENT_NAME";
    private static String TRANSLUCENT = "TRANSLUCENT";
    @BindView(R.id.fragment_container)
    FrameLayout frmContainer;

    private Fragment currentFragment;

    public static Builder with(Context context) {
        return new Builder(context);
    }

    @Override
    public Object getPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_none;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            //ButterKnife.bind(this);
            getFragmentForOpen(getIntent().getExtras());
        } catch (Exception e) {
            handleException(e);
        }

    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return null;
    }

    private Bundle getArgsForFragment(Bundle bundle) {
        return bundle.getBundle(ARGS);
    }

    void getFragmentForOpen(Bundle bundle) {

        String fragmentName = bundle.getString(FRAGMENT_NAME);

        currentFragment = Fragment.instantiate(getApplicationContext(), fragmentName, getArgsForFragment(bundle));
        try {
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, currentFragment, currentFragment.getClass().getSimpleName());

            transaction.commitAllowingStateLoss();

        } catch (Exception e) {
            handleException(e);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (currentFragment != null)
            currentFragment.onActivityResult(requestCode, resultCode, data);
    }

    public static class Builder {

        private Context contextForOpen;

        private Bundle params = null;

        Builder(Context context) {

            this.contextForOpen = context;

        }

        public Builder parameters(Bundle params) {

            this.params = params;

            return this;

        }

        public void start(Class<? extends Fragment> fragmentClass) {

            try {

                Intent intent = createIntentForStart(fragmentClass);
                contextForOpen.startActivity(intent);

            } catch (Exception e) {

                handleException(e);

            }

        }

        private Intent getIntent(Context contextForOpen, @Nullable Bundle params) {

            Intent intent = new Intent(contextForOpen, AloneFragmentActivity.class);

            if (params != null) {
                intent.putExtra(ARGS, params);
            } else {
                intent.putExtra(ARGS, new Bundle());
            }

            return intent;

        }

        private Intent createIntentForStart(Class<? extends Fragment> fragmentClass) {

            Intent intent = getIntent(contextForOpen, params);

            intent.putExtra(FRAGMENT_NAME, fragmentClass.getName());

            return intent;

        }

    }
}
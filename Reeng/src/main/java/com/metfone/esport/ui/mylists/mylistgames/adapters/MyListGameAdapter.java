package com.metfone.esport.ui.mylists.mylistgames.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.ui.viewholders.HomeGameViewHolder;

import java.util.ArrayList;

public class MyListGameAdapter extends BaseListAdapter<GameInfoResponse, HomeGameViewHolder> {

    private Context context;

    private int width = 0;

    private HomeGameViewHolder.IOnClickItem callBack;

    public MyListGameAdapter(Context context, ArrayList<GameInfoResponse> items, HomeGameViewHolder.IOnClickItem callBack) {
        super(context);

        this.context = context;

        this.callBack = callBack;

        width = (ScreenUtils.getScreenWidth() - context.getResources().getDimensionPixelSize(R.dimen.pading_layout) * 4) / 3;

        setNewData(items);
    }

    @NonNull
    @Override
    public HomeGameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new HomeGameViewHolder(LayoutInflater.from(context), parent, width);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeGameViewHolder holder, int position) {
        holder.binData(mData.get(position), false, callBack);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

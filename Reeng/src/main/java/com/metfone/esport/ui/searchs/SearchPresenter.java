package com.metfone.esport.ui.searchs;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.BaseResponse;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.enums.SearchTabTypeEnum;
import com.metfone.esport.entity.repsonse.SearchAllResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

class SearchPresenter extends BasePresenter<SearchFragment, BaseModel> {
    public SearchPresenter(SearchFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void search(final SearchTabTypeEnum currentTab, String text, final int currentPage) {
        Observable<?> observable = apiService.searchAll(text, getDefaultParam());

        switch (currentTab) {
            case LIVE_STREAMING:
                observable = apiService.searchVideoEsport(text, 3, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam());
                break;
            case PAST_BROADCAST:
                observable = apiService.searchVideoEsport(text, 2, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam());
                break;
            case UPLOADED:
                observable = apiService.searchVideoEsport(text, 1, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam());
                break;
            case CHANNEL:
                observable = apiService.searchChannel(text, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam());
                break;
            case GAME:
                observable = apiService.searchGame(text, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam());
                break;
            case TOURNAMENT:
                observable = apiService.searchTournament(text, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam());
                break;
            default:
                apiService.searchAll(text, getDefaultParam());
        }

        RxUtils.async(
                compositeDisposable,
                (Observable<BaseResponse<ArrayList<Object>>>) observable,
                new ICallBackResponse<ArrayList<Object>>() {
                    @Override
                    public void onSuccess(ArrayList<Object> response) {
                        if (view != null)
                            view.searchSuccess(currentTab, response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null)
                            view.searchFail(currentTab);
                    }
                });
    }

//    public void getVideoSuggest() {
//        RxUtils.async(compositeDisposable,
//                apiService.searchGetVideoSuggest(0, 3, getDefaultParam()),
//                new ICallBackResponse<ArrayList<VideoInfoResponse>>() {
//                    @Override
//                    public void onSuccess(ArrayList<VideoInfoResponse> response) {
//                        if (view != null)
//                            view.getVideoSuggestSuccess(response);
//                    }
//
//                    @Override
//                    public void onFail(String error) {
//                        if (view != null)
//                            view.getVideoSuggestSuccess(null);
//                    }
//                });
//    }

    public void getDataSuggest() {
        RxUtils.async(compositeDisposable,
                apiService.searchGetSuggest(0, 3, AccountBusiness.getInstance().getChannelId(), getDefaultParam()),
                new ICallBackResponse<ArrayList<SearchAllResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<SearchAllResponse> response) {
                        if (view != null)
                            view.getSuggestSuccess(response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null)
                            view.getSuggestSuccess(null);
                    }
                });
    }

//    public void getChannelSuggest() {
//        RxUtils.async(compositeDisposable,
//                apiService.searchGetChannelSuggest(0, 3, getDefaultParam()),
//                new ICallBackResponse<ArrayList<ChannelInfoResponse>>() {
//                    @Override
//                    public void onSuccess(ArrayList<ChannelInfoResponse> response) {
//                        if (view != null)
//                            view.getChannelSuggestSuccess(response);
//                    }
//
//                    @Override
//                    public void onFail(String error) {
//                        if (view != null)
//                            view.getChannelSuggestSuccess(null);
//                    }
//                });
//    }
}

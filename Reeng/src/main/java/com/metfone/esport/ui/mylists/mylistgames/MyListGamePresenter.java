package com.metfone.esport.ui.mylists.mylistgames;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class MyListGamePresenter extends BasePresenter<MyListGameFragment, BaseModel> {
    private Disposable apiDispose = null;
    private boolean canLoadMore = false;

    public boolean isLoading() {
        return apiDispose != null && !apiDispose.isDisposed();
    }

    public boolean isCanLoadMore() {
        return canLoadMore;
    }

    public MyListGamePresenter(MyListGameFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(final int currentPage) {
        if (currentPage > 0 && isLoading()) return;

        if (currentPage == 0 && apiDispose != null)
            apiDispose.dispose();

        canLoadMore = false;

        apiDispose = RxUtils.async(
                compositeDisposable,
                apiService.myListGame(currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam()),
                new ICallBackResponse<ArrayList<GameInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<GameInfoResponse> response) {
                        if (view != null) view.onGetDataSuccess(response);

                        if (CollectionUtils.size(response) >= DEFAULT_LIMIT) canLoadMore = true;
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }

                    @Override
                    public void onFinish() {
                        if (apiDispose != null)
                            apiDispose.dispose();
                    }
                });
    }

    public void unFollowGame(Long id) {
        RxUtils.async(
                compositeDisposable,
                apiService.followGame(id, 0, getDefaultParam()), //0 la unfollow
                new ICallBackResponse<String>() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFail(String error) {

                    }
                });
    }
}

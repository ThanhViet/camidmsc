package com.metfone.esport.ui.games.luckywheel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.facebook.CallbackManager;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseFragment;

import butterknife.BindView;

/**
 * Created by duyth
 */
@SuppressLint("SetJavaScriptEnabled")
public class FrgmtWebView extends BaseFragment {
    private String url;
    private WebView webView;
    private Activity mActivity;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    private CallbackManager callbackManager;

    @Override
    public int getResIdView() {
        return 0;
    }

    public FrgmtWebView setUrl(String url) {
        this.url = url;
        return this;
    }

    public static FrgmtWebView newInstance(String url, String title) {
        FrgmtWebView frgmtWebView = new FrgmtWebView();
        frgmtWebView.setUrl(url);
        frgmtWebView.setTitle(title);
        return frgmtWebView;
    }

    @SuppressLint("AddJavascriptInterface")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frgmt_webview, container, false);
        pbLoading = v.findViewById(R.id.pbLoading);
        webView = v.findViewById(R.id.webview);
        webView.clearCache(true);
        webView.setWebViewClient(new WebClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webView.requestFocusFromTouch();
        callbackManager = CallbackManager.Factory.create();
        webView.addJavascriptInterface(new WebAppInterface(getActivity()), "MyMetfone");
        pbLoading.setVisibility(View.VISIBLE);
        pbLoading.bringToFront();
        webView.loadUrl(url);
        return v;
    }

    @Override
    public void onDestroy() {
        if(webView!=null)
            webView.destroy();
        super.onDestroy();
    }

    class WebClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            pbLoading.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            String message = "SSL Certificate error.";
            switch (error.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    message = "The certificate authority is not trusted.";
                    break;
                case SslError.SSL_EXPIRED:
                    message = "The certificate has expired.";
                    break;
                case SslError.SSL_IDMISMATCH:
                    message = "The certificate Hostname mismatch.";
                    break;
                case SslError.SSL_NOTYETVALID:
                    message = "The certificate is not yet valid.";
                    break;
            }
            builder.setTitle("SSL Certificate Error");
            builder.setMessage(message);
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public class WebAppInterface {


        public WebAppInterface(Activity activity) {
            mActivity = activity;
        }

        @JavascriptInterface
        public void inviteFriends() {
            mActivity.runOnUiThread(() -> ((GameActivity) mActivity).inviteFriends());
        }

        @JavascriptInterface
        public void inviteFb() {
            mActivity.runOnUiThread(() -> ((GameActivity) mActivity).inviteFb());
        }
    }

    public void ShareFb(String name, String userId) {
//        HashMap<String, Object> param = new HashMap<>();
//        param.put("isdn", client.getIsdn());
//        param.put("gameCode", Const.GAME_CODE_LUCKY_LOYALTY);
//        param.put("fbId", userId);
//        param.put("fbName", name);
//        param.put("language", client.lang);
//        requestAction(Client.WS_SHARE_FB, param, new RequestFinishListener() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            public void onRequestFinish(boolean success, Object data) {
//                ModelDoAction model = (ModelDoAction) data;
//                if (success) {
//                    webView.evaluateJavascript("FbNotice(1)", null);
//                } else {
//                    webView.evaluateJavascript("FbNotice(0)", null);
//                }
//
//            }
//        });
    }

    @Override
    public String getName() {
        return FrgmtWebView.class.getName();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}

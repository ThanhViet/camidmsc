package com.metfone.esport.ui.games.livestreams.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;

import java.util.ArrayList;

public class GameLiveStreamAdapter extends BaseListAdapter<VideoInfoResponse, HalfVideoNoOptionViewHolder> {
    private Context context;

    private HalfVideoNoOptionViewHolder.IOnClickItem onClickItem;

    public GameLiveStreamAdapter(Context context, ArrayList<VideoInfoResponse> items, HalfVideoNoOptionViewHolder.IOnClickItem onClickItem) {
        super(context);

        this.context = context;

        this.onClickItem = onClickItem;

        setNewData(items);
    }

    @NonNull
    @Override
    public HalfVideoNoOptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new HalfVideoNoOptionViewHolder(LayoutInflater.from(context), parent, 0);
    }

    @Override
    public void onBindViewHolder(@NonNull HalfVideoNoOptionViewHolder holder, int position) {
        holder.binData(mData.get(position), onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

package com.metfone.esport.ui.searchs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.drakeet.multitype.MultiTypeAdapter;
import com.esport.widgets.MultipleStatusView;
import com.google.gson.reflect.TypeToken;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ViewPagerBaseAdapter;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.enums.SearchTabTypeEnum;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.entity.repsonse.SearchAllResponse;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.homes.adapters.ExtSaveStateMultiTypeAdapter;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.games.tournamentdetails.TournamentDetailFragment;
import com.metfone.esport.ui.searchs.adapters.ObjectRecentSearch;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchAllGame;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchChannelLike;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchLive;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchPastBroadCast;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchUploaded;
import com.metfone.esport.ui.searchs.adapters.ObjectSuggestSearch;
import com.metfone.esport.ui.searchs.adapters.ObjectTitle;
import com.metfone.esport.ui.searchs.adapters.SearchChannelLiveItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchDefaultDecoration;
import com.metfone.esport.ui.searchs.adapters.SearchLiveStreamItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchPastBroadcastItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchRecentItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchSuggestItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchTextTitleItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchTournamentItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchUploadedItemViewBinder;
import com.metfone.esport.ui.searchs.contents.SearchContentFragment;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.entity.enums.SearchTabTypeEnum.ALL;
import static com.metfone.esport.entity.enums.SearchTabTypeEnum.CHANNEL;
import static com.metfone.esport.entity.enums.SearchTabTypeEnum.GAME;
import static com.metfone.esport.entity.enums.SearchTabTypeEnum.LIVE_STREAMING;
import static com.metfone.esport.entity.enums.SearchTabTypeEnum.PAST_BROADCAST;
import static com.metfone.esport.entity.enums.SearchTabTypeEnum.TOURNAMENT;
import static com.metfone.esport.entity.enums.SearchTabTypeEnum.UPLOADED;

public class SearchFragment extends BaseFragment<SearchPresenter> implements SearchContentFragment.IActionListener {

    public static String KEY_SP_SEARCH_HISTORY = "KEY_SP_SEARCH_HISTORY";

    List<SearchContentFragment> fragments = new ArrayList<>();

    private MultiTypeAdapter adapter = new ExtSaveStateMultiTypeAdapter();

    private ArrayList<Object> itemsSuggest = new ArrayList<>();

    private ArrayList<Object> itemsChannelMaybeLike = new ArrayList<>();

    private ArrayList<Object> itemsVideoMaybeLike = new ArrayList<>();

    private ArrayList<Object> itemsHistory = new ArrayList<>();

    private ArrayList<String> searchHistoryStringList = null;

    private SearchTabTypeEnum currentTab = ALL;

    private Map<SearchTabTypeEnum, ArrayList<Object>> mapResultItems = new HashMap<>();

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.rvSearch)
    RecyclerView rvSearch;

    @BindView(R.id.edtSearchContent)
    AppCompatEditText edtSearchContent;

    @BindView(R.id.tvSearchAction)
    AppCompatTextView tvSearchAction;

    @BindView(R.id.ivIconSearch)
    AppCompatImageView ivIconSearch;
//
//    @BindView(R.id.tlSearchType)
//    TabLayout tlSearchType;

    @BindView(R.id.vpSearch)
    ViewPager vpSearch;

    @BindView(R.id.rootView)
    LinearLayout rootView;

    @BindView(R.id.lnToolBar)
    LinearLayout lnToolBar;

    @Override
    public SearchPresenter getPresenter() {
        return new SearchPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
        com.metfone.selfcare.util.Utilities.adaptViewForInsertBottom(rootView);
        searchHistoryStringList = GsonUtils.getGson().fromJson(
                SPUtils.getInstance().getString(KEY_SP_SEARCH_HISTORY, ""),
                new TypeToken<ArrayList<String>>() {
                }.getType());

        if (searchHistoryStringList == null) searchHistoryStringList = new ArrayList<>();

        initRecyclerView();

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) lnToolBar.getLayoutParams();
        params.topMargin = Common.getStatusBarHeight(getContext()) + getResources().getDimensionPixelOffset(R.dimen._10sdp);
        lnToolBar.setLayoutParams(params);

        initViewPager();

//        tlSearchType.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                SearchTabTypeEnum newTab = SearchTabTypeEnum.enumOf(tab.getPosition());
//
//                if (newTab != currentTab) {
//                    currentTab = newTab;
//
//                    refreshData();
//                }
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        vpSearch.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                SearchTabTypeEnum newTab = SearchTabTypeEnum.enumOf(position);

                if (newTab != currentTab) {
                    currentTab = newTab;

                    if (!mapResultItems.containsKey(currentTab) || CollectionUtils.isEmpty(mapResultItems.get(currentTab)))
                        getChildData(null, null);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        edtSearchContent.setOnFocusChangeListener((view1, isFocused) -> {
            if (isFocused) {
                tvSearchAction.setText(getString(R.string.search_action));

                tvSearchAction.setTextColor(ContextCompat.getColor(getContext(), R.color.indicator_color));

                ivIconSearch.setImageResource(R.drawable.ic_navigate_back);

                ivIconSearch.setEnabled(true);

//                int padding = getResources().getDimensionPixelSize(R.dimen._6sdp);
//
//                ivIconSearch.setPadding(padding, padding, padding, padding);

                setViewRecentOrResult();
            } else {
                tvSearchAction.setText(getString(R.string.cancel));

                tvSearchAction.setTextColor(ContextCompat.getColor(getContext(), R.color.white));

                ivIconSearch.setEnabled(false);
//
//                int padding = getResources().getDimensionPixelSize(R.dimen._8sdp);
//
//                ivIconSearch.setPadding(padding, padding, padding, padding);

                ivIconSearch.setImageResource(R.drawable.ic_icon_search_fragment_es);
            }
        });

        edtSearchContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (StringUtils.isTrimEmpty(editable.toString())) {
                    setViewRecentOrResult();
                } else {

                }
            }
        });

        edtSearchContent.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                onSearch();

                KeyboardUtils.hideSoftInput(getActivity());
            }

            return false;
        });

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        ivIconSearch.setOnClickListener(v -> {
            KeyboardUtils.hideSoftInput(v);

            edtSearchContent.clearFocus();

            setViewRecentOrResult();
        });

        refreshData();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViewPager() {
        List<String> titles = new ArrayList<>();

        fragments.add(SearchContentFragment.newInstance(ALL));
        titles.add(getString(R.string.all));

        fragments.add(SearchContentFragment.newInstance(LIVE_STREAMING));
        titles.add(getString(R.string.live_streaming));

        fragments.add(SearchContentFragment.newInstance(PAST_BROADCAST));
        titles.add(getString(R.string.past_broad_cast));

        fragments.add(SearchContentFragment.newInstance(UPLOADED));
        titles.add(getString(R.string.uploaded));

        fragments.add(SearchContentFragment.newInstance(GAME));
        titles.add(getString(R.string.game));

        fragments.add(SearchContentFragment.newInstance(TOURNAMENT));
        titles.add(getString(R.string.tournament));

        fragments.add(SearchContentFragment.newInstance(CHANNEL));
        titles.add(getString(R.string.channel));

        vpSearch.setAdapter(new ViewPagerBaseAdapter(getChildFragmentManager(), fragments, titles));
        vpSearch.setOffscreenPageLimit(fragments.size());
    }

    /**
     * Goi api lay channel channel, video có thể thích, các recent search
     * Created_by @dchieu on 9/21/20
     */
    private void getDataRecent() {
        multipleStatusView.showLoading();

        presenter.getDataSuggest();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initRecyclerView() {
        initItemHistory();

        adapter.register(ObjectTitle.class, new SearchTextTitleItemViewBinder());
        adapter.register(ObjectSearchLive.class, new SearchLiveStreamItemViewBinder(new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(ObjectSearchChannelLike.class, new SearchChannelLiveItemViewBinder(new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }

            //            @Override
//            public void onClickOption() {
//
//            }
        }));
        adapter.register(ObjectSearchPastBroadCast.class, new SearchPastBroadcastItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
                ApplicationController.self().getFirebaseEventBusiness().logPastBroadcastEsport(video.title, String.valueOf(video.id));
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(ObjectSearchUploaded.class, new SearchUploadedItemViewBinder(new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
                ApplicationController.self().getFirebaseEventBusiness().logUploadedEsport(video.title, String.valueOf(video.id));
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
//        adapter.register(ChannelInfoResponse.class, new SearchChannelItemViewBinder());
//        adapter.register(GameInfoResponse.class, new SearchGamesItemViewBinder());
        adapter.register(TournamentInfoResponse.class, new SearchTournamentItemViewBinder((TournamentInfoResponse entity) -> {
            AloneFragmentActivity.with(getContext()).parameters(TournamentDetailFragment.newBundle(entity.id))
                    .start(TournamentDetailFragment.class);
        }));
        adapter.register(ObjectSuggestSearch.class, new SearchSuggestItemViewBinder((entity) -> {
            edtSearchContent.setText(entity.text);

            onSearch();
        }));

        adapter.register(ObjectRecentSearch.class, new SearchRecentItemViewBinder(() -> {
            searchHistoryStringList.clear();

            initItemHistory();

            reInitItemSuggest();

            SPUtils.getInstance().put(KEY_SP_SEARCH_HISTORY, GsonUtils.getGson().toJson(searchHistoryStringList));
        }));

        rvSearch.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvSearch.addItemDecoration(new SearchDefaultDecoration(getContext()));
        rvSearch.setAdapter(adapter);

        rvSearch.setOnTouchListener((view, motionEvent) -> {
            if (edtSearchContent.isFocused()) {
                KeyboardUtils.hideSoftInput(getActivity());

                edtSearchContent.clearFocus();

                setViewRecentOrResult();
            }

            return false;
        });

        setViewRecentOrResult();
    }

    private void navigateToChannel(ChannelInfoResponse channel) {
        if (channel != null)
            AloneFragmentActivity.with(getActivity())
                    .parameters(ChannelContainerFragment.newBundle(channel.id))
                    .start(ChannelContainerFragment.class);
    }

    /**
     * tạo các item lịch sử tìm kiếm từ cache
     * Created_by @dchieu on 10/6/20
     */
    private void initItemHistory() {
        if (itemsHistory == null) itemsHistory = new ArrayList<>();
        else itemsHistory.clear();

        if (CollectionUtils.isNotEmpty(searchHistoryStringList))
            for (String s : searchHistoryStringList) {
                itemsHistory.add(new ObjectSuggestSearch(s));
            }
    }

    @OnClick({R.id.tvSearchAction})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSearchAction:
                try {
                    if (edtSearchContent.isFocused()) {
                        onSearch();
                    } else /*if (!StringUtils.isTrimEmpty(edtSearchContent.getText().toString())) {
                        currentTab = ALL;

//                        tlSearchType.getTabAt(0).select();

                        vpSearch.setCurrentItem(currentTab.position);

                        edtSearchContent.setText("");

                        setViewRecentOrResult();
                    } else*/ {
                        onClickBack();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    /**
     * refresh man hinh de lay du lieu channel sugget vs video suggest
     * Created_by @dchieu on 9/11/20
     */
    private void refreshData() {
        try {
            if (edtSearchContent.isFocused()) {
                itemsHistory.clear();
            } else if (StringUtils.isTrimEmpty(edtSearchContent.getText().toString())) {
                itemsSuggest.clear();

                getDataRecent();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lay du lieu cho cac man hinh con
     * Created_by @dchieu on 10/2/20
     */
    private void getChildData(SearchTabTypeEnum typeEnum, Integer currentPage) {
        try {
            if (typeEnum == null)
                typeEnum = currentTab;

            if (currentPage == null)
                currentPage = 0;

            fragments.get(typeEnum.position).showLoading();

            presenter.search(typeEnum, edtSearchContent.getText().toString(), currentPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Thuc hien tim kiem
     * Created_by @dchieu on 17:04
     */
    private void onSearch() {
        try {
            KeyboardUtils.hideSoftInput(getActivity());

            edtSearchContent.clearFocus();

            mapResultItems.clear();

            setViewRecentOrResult();

            if (!StringUtils.isTrimEmpty(edtSearchContent.getText().toString())) {
                mapResultItems.clear();

                for (SearchContentFragment fragment : fragments) {
                    fragment.resetState();
                }

                getChildData(currentTab, 0);

                //Luu lịch sử tìm kiếm
                saveHistorySearch();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Luu lịch sử tìm kiếm
     * Created_by @dchieu on 10/7/20
     */
    private void saveHistorySearch() {
        try {
            String newSearch = edtSearchContent.getText().toString();

            if (StringUtils.isTrimEmpty(newSearch)) return;

            Iterator<String> iterator = searchHistoryStringList.iterator();

            while (iterator.hasNext()) {
                String text = iterator.next();

                if (newSearch.contains(text)) {
                    iterator.remove();
                }
            }

            searchHistoryStringList.add(0, edtSearchContent.getText().toString());

            if (searchHistoryStringList.size() > 3) searchHistoryStringList.remove(3);

            SPUtils.getInstance().put(KEY_SP_SEARCH_HISTORY, GsonUtils.getGson().toJson(searchHistoryStringList));

            //init lai lich su tim kiem
            initItemHistory();

            reInitItemSuggest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set view ve trang thai recent or result
     * Created_by @dchieu on 16:56
     */
    @SuppressWarnings("ConstantConditions")
    private void setViewRecentOrResult() {
        if (StringUtils.isTrimEmpty(edtSearchContent.getText().toString())) {
            vpSearch.setVisibility(View.INVISIBLE);

            if (edtSearchContent.isFocused() && CollectionUtils.isNotEmpty(itemsHistory)) {
                adapter.setItems(itemsHistory);
            } else {
                adapter.setItems(itemsSuggest);
            }

            multipleStatusView.showContent();

            swipeRefresh.setVisibility(View.VISIBLE);

            adapter.notifyDataSetChanged();
        } else {
            vpSearch.setVisibility(View.VISIBLE);

            swipeRefresh.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Hien thi ket qua tim kiem
     * Created_by @dchieu on 10/1/20
     */
    private void showResultData(SearchTabTypeEnum typeEnum) {
        if (mapResultItems.containsKey(typeEnum)) {
            fragments.get(typeEnum.position).addAndShowData(mapResultItems.get(typeEnum));
        } else {
            fragments.get(typeEnum.position).addAndShowData(null);
        }
    }

    @SuppressWarnings("DuplicateBranchesInSwitch")
    public void searchSuccess(SearchTabTypeEnum typeEnum, ArrayList<Object> response) {
        ArrayList<Object> items = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(response))
            switch (typeEnum) {
                case LIVE_STREAMING:
                    for (Object res : response) {
                        items.add(new ObjectSearchLive(res));
                    }
                    break;
                case PAST_BROADCAST:
                    for (Object res : response) {
                        items.add(new ObjectSearchPastBroadCast(res));
                    }
                    break;
                case UPLOADED:
                    for (Object res : response) {
                        items.add(new ObjectSearchUploaded(res));
                    }
                    break;
                case CHANNEL:
                    if (CollectionUtils.isNotEmpty(response))
                        items.addAll(response);
                    break;
                case GAME:
                    if (CollectionUtils.isNotEmpty(response))
                        items.addAll(response);
                    break;
                case TOURNAMENT:
                    if (CollectionUtils.isNotEmpty(response))
                        items.addAll(response);
                    break;
                case ALL:
                    ArrayList<ObjectSearchLive> lives = new ArrayList<>();
                    ArrayList<ObjectSearchPastBroadCast> pastBroadCasts = new ArrayList<>();
                    ArrayList<ObjectSearchUploaded> uploadeds = new ArrayList<>();
                    ArrayList<ChannelInfoResponse> channels = new ArrayList<>();
                    ArrayList<GameInfoResponse> games = new ArrayList<>();
                    ArrayList<TournamentInfoResponse> tournaments = new ArrayList<>();

                    for (Object res : response) {
                        if (res instanceof SearchAllResponse) {
                            SearchAllResponse castData = (SearchAllResponse) res;
                            switch (castData.type) {
                                case "listChannel":
                                    if (CollectionUtils.isNotEmpty(castData.dataChannel))
                                        for (int i = 0; i < castData.dataChannel.size() && i < 3; i++) {
                                            channels.add(castData.dataChannel.get(i));
                                        }
                                    break;
                                case "listVideo":
                                    if (CollectionUtils.isNotEmpty(castData.listVideo))
                                        for (int i = 0; i < castData.listVideo.size() && i < 3; i++) {
                                            lives.add(new ObjectSearchLive(castData.listVideo.get(i)));
                                        }
                                    break;
                                case "listGame":
                                    if (CollectionUtils.isNotEmpty(castData.listGame))
                                        for (int i = 0; i < castData.listGame.size() && i < 3; i++) {
                                            games.add(castData.listGame.get(i));
                                        }
                                    break;
                                case "pastBroadcast":
                                    if (CollectionUtils.isNotEmpty(castData.listVideo))
                                        for (int i = 0; i < castData.listVideo.size() && i < 3; i++) {
                                            pastBroadCasts.add(new ObjectSearchPastBroadCast(castData.listVideo.get(i)));
                                        }
                                    break;
                                case "listTour":
                                    if (CollectionUtils.isNotEmpty(castData.lstTournamentDto))
                                        for (int i = 0; i < castData.lstTournamentDto.size() && i < 3; i++) {
                                            tournaments.add(castData.lstTournamentDto.get(i));
                                        }
                                    break;
                                case "videoUploaded":
                                    if (CollectionUtils.isNotEmpty(castData.listVideo))
                                        for (int i = 0; i < castData.listVideo.size() && i < 3; i++) {
                                            uploadeds.add(new ObjectSearchUploaded(castData.listVideo.get(i)));
                                        }
                                    break;
                            }
                        }
                    }

                    if (CollectionUtils.isNotEmpty(lives)) {
                        items.add(new ObjectTitle(getString(R.string.live_streaming)));
                        items.addAll(lives);
                    }

                    if (CollectionUtils.isNotEmpty(pastBroadCasts)) {
                        items.add(new ObjectTitle(getString(R.string.past_broad_cast)));
                        items.addAll(pastBroadCasts);
                    }

                    if (CollectionUtils.isNotEmpty(uploadeds)) {
                        items.add(new ObjectTitle(getString(R.string.uploaded)));
                        items.addAll(uploadeds);
                    }

                    if (CollectionUtils.isNotEmpty(channels)) {
                        items.add(new ObjectTitle(getString(R.string.channel)));
                        items.addAll(channels);
                    }

                    if (CollectionUtils.isNotEmpty(games)) {
                        items.add(new ObjectTitle(getString(R.string.game)));
                        items.add(new ObjectSearchAllGame(games));
                    }

                    if (CollectionUtils.isNotEmpty(tournaments)) {
                        items.add(new ObjectTitle(getString(R.string.tournament)));
                        items.addAll(tournaments);
                    }

                    break;
                default:
            }

        mapResultItems.put(typeEnum, items);

        showResultData(typeEnum);
    }

    public void searchFail(SearchTabTypeEnum typeEnum) {
        showResultData(typeEnum);
    }

    public void getVideoSuggestSuccess(ArrayList<VideoInfoResponse> response) {
        itemsVideoMaybeLike.clear();

        if (CollectionUtils.isNotEmpty(response)) {
            for (VideoInfoResponse video : response) {
                if (video.isLive == 1) {
                    itemsVideoMaybeLike.add(new ObjectSearchLive(video));
                } else if (video.type_video == 1) {
                    itemsVideoMaybeLike.add(new ObjectSearchUploaded(video));
                } else if (video.type_video == 2) {
                    itemsVideoMaybeLike.add(new ObjectSearchPastBroadCast(video));
                }
            }
//            itemsVideoMaybeLike.addAll(response);

//            if (CollectionUtils.isNotEmpty(itemsVideoMaybeLike))
//                reInitItemSuggest();
        }

    }

    public void getSuggestSuccess(ArrayList<SearchAllResponse> response) {
        if (CollectionUtils.isNotEmpty(response)) {
            try {
                for (SearchAllResponse searchAllResponse : response) {
                    if (searchAllResponse.type.equalsIgnoreCase("listChannel")) {
                        getChannelSuggestSuccess(searchAllResponse.dataChannel);

                    } else if (searchAllResponse.type.equalsIgnoreCase("listVideo")) {
                        getVideoSuggestSuccess(searchAllResponse.listVideo);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            reInitItemSuggest();
        }

    }

    /**
     * Created_by @dchieu on 10/7/20
     */
    private void reInitItemSuggest() {
        itemsSuggest.clear();

        if (CollectionUtils.isNotEmpty(itemsChannelMaybeLike)) {
            itemsSuggest.add(new ObjectTitle(getString(R.string.search_title_channel_maybe_you_like)));

            for (int i = 0; i < itemsChannelMaybeLike.size() && i < 3; i++) {
                itemsSuggest.add(itemsChannelMaybeLike.get(i));
            }
        }

        if (CollectionUtils.isNotEmpty(itemsVideoMaybeLike)) {
            itemsSuggest.add(new ObjectTitle(getString(R.string.search_title_video_maybe_you_like)));

            for (int i = 0; i < itemsVideoMaybeLike.size() && i < 3; i++) {
                itemsSuggest.add(itemsVideoMaybeLike.get(i));
            }
        }

        if (CollectionUtils.isNotEmpty(itemsHistory)) {
            itemsSuggest.add(new ObjectRecentSearch(getString(R.string.search_title_recent_search)));

            for (String s : searchHistoryStringList) {
                itemsSuggest.add(new ObjectSuggestSearch(0, s));
            }
        }

        if (!edtSearchContent.isFocused()) {
            adapter.notifyDataSetChanged();

            multipleStatusView.showContent();
        }
    }

    public void getChannelSuggestSuccess(ArrayList<ChannelInfoResponse> response) {
        itemsChannelMaybeLike.clear();

        if (CollectionUtils.isNotEmpty(response)) {
            for (ChannelInfoResponse channel : response) {
                if (channel == null) continue;

                if (channel.videoLive != null && channel.video == null) {
                    channel.video = channel.videoLive;
                }

                if (channel.video != null) {
                    if (channel.video.isLive == 1) {
                        itemsChannelMaybeLike.add(new ObjectSearchChannelLike(channel));
                    } else if (channel.video.type_video == 1) {
                        VideoInfoResponse video = channel.video;

                        video.channelInfo = channel;

                        itemsChannelMaybeLike.add(new ObjectSearchUploaded(video));
                    } else if (channel.video.type_video == 2) {
                        VideoInfoResponse video = channel.video;

                        video.channelInfo = channel;
                        itemsChannelMaybeLike.add(new ObjectSearchPastBroadCast(video));
                    }
                }
            }
//            itemsChannelMaybeLike.addAll(response);
//
//            if (CollectionUtils.isNotEmpty(itemsChannelMaybeLike))
//                reInitItemSuggest();
        }
    }

    @Override
    public void onChildFragmentLoadData(SearchTabTypeEnum typeEnum, int currentPage) {
        mapResultItems.remove(typeEnum);

        getChildData(typeEnum, currentPage);
    }
}

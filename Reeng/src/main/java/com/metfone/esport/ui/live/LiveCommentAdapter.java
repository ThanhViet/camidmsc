package com.metfone.esport.ui.live;

import static com.metfone.esport.ui.live.LiveVideoFragment.PRE_CODE_THANKMESSAGE;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Nguyễn Thành Chung on 8/28/20.
 */
public class LiveCommentAdapter extends BaseListAdapter<ListCommentEntity, LiveCommentAdapter.LiveCommentViewHolder> {
    public static int PORTRAIT = 0;
    public static int LANDSCAPE = 1;
    private int type = PORTRAIT;

    public LiveCommentAdapter(Context context, int type) {
        super(context);
        this.type = type;
    }

    public void setData(List<ListCommentEntity> entities){
        int position = 0;
        if (!mData.isEmpty()){
            position = mData.size() - 1;
        }
        mData.addAll(entities);
        notifyDataSetChanged();
    }

    public void setData(ListCommentEntity entities){
        int position = 0;
        if (!mData.isEmpty()){
            position = mData.size()-1;
        }
        mData.add(entities);
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public LiveCommentViewHolder onCreateViewHolder(@NotNull ViewGroup viewGroup, int i) {
        if (type == PORTRAIT) {
            return new LiveCommentViewHolder(LayoutInflater.from(context).inflate(R.layout.item_comment_live, viewGroup, false));
        } else {
            return new LiveCommentViewHolder(LayoutInflater.from(context).inflate(R.layout.item_comment_live_landscape, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NotNull LiveCommentViewHolder liveCommentViewHolder, int position) {
        ListCommentEntity entity = mData.get(position);
        liveCommentViewHolder.binData(entity, position);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    static class LiveCommentViewHolder extends BaseViewHolder<ListCommentEntity> {
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvContent)
        TextView tvContent;


        @BindView(R.id.ln_live_comment)
        LinearLayout ln_live_comment;

        @BindView(R.id.ln_donate_comment)
        LinearLayout ln_donate_comment;

        @BindView(R.id.tvDesDonate)
        TextView tvDesDonate;

        public LiveCommentViewHolder(View itemView) {
            super(itemView);
        }
        String ss = "";
        String preCode = PRE_CODE_THANKMESSAGE;
        @Override
        public void binData(ListCommentEntity entity, int position) {
//            DataComment dataComment = Common.convertJsonToObject(entity.getData(), DataComment.class);

            if (entity.getChannelInfo() == null){
                tvUserName.setText(entity.userInfo == null ? entity.getNameSender() : entity.userInfo.name);
            }else {
                tvUserName.setText(entity.getChannelInfo().channel_name);
            }

            tvContent.setText(entity.status == null ? entity.getContent() : entity.status);

            tvUserName.setTextColor(Common.getRandomColor(context));

            String pre = "";
            if (entity.getContent().length() > preCode.length()-1)
                pre = entity.getContent().substring(0, preCode.length());
            if (pre.equals(preCode)) {

                ln_live_comment.setVisibility(View.GONE);
                ln_donate_comment.setVisibility(View.VISIBLE);

                ss = entity.getContent();
                if (ss.split("_").length > 1)
                    tvDesDonate.setText(ss.split("_")[1]);
            } else {
                ln_live_comment.setVisibility(View.VISIBLE);
                ln_donate_comment.setVisibility(View.GONE);
            }

//            itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.white));
        }
    }
}

/*
 * Created by admin on 2020/9/6
 */

package com.metfone.esport.ui.uploadVideo.uploading;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.FileUtils;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.UploadVideoEntity;
import com.metfone.esport.entity.repsonse.CreateVideoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.helper.ListenerHelper;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.network.file.upload.UploadListener;
import com.metfone.esport.network.file.upload.UploadRequest;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.R;

import java.io.File;

import butterknife.BindView;
import iknow.android.utils.DateUtil;
import io.reactivex.disposables.CompositeDisposable;

public class UploadingVideoActivity extends BaseActivity<UploadingVideoPresenter> implements UploadListener {
    @BindView(R.id.btn_back)
    View btnBack;

    @BindView(R.id.tv_title_video)
    AppCompatTextView tvTitleVideo;

    @BindView(R.id.tv_desc)
    AppCompatTextView tvDesc;

    @BindView(R.id.tv_duration)
    AppCompatTextView tvDuration;

    @BindView(R.id.tv_progress)
    AppCompatTextView tvProgress;

    @BindView(R.id.tv_video_ready)
    AppCompatTextView tv_video_ready;

    @BindView(R.id.iv_cover)
    AppCompatImageView ivCover;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private UploadVideoEntity data;

    @Override
    public UploadingVideoPresenter getPresenter() {
        return new UploadingVideoPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_uploading_video;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            data = (UploadVideoEntity) getIntent().getSerializableExtra(Constant.KEY_DATA);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (data == null) {
            finish();
            return;
        }
        tvTitleVideo.setText(data.title);
        tvDesc.setText(data.description);
        final long duration = FileUtils.getDuration(data.pathVideo);
        if (duration <= 0) {
            tvDuration.setVisibility(View.GONE);
        } else {
            tvDuration.setVisibility(View.VISIBLE);
            tvDuration.setText(DateUtil.convertSecondsToTime(duration / 1000));
        }
        if (CollectionUtils.isNotEmpty(data.listPathThumb)) {
            try {
                String thumb = data.listPathThumb.get(0);
                File file = new File(thumb);
                ImageLoader.setImageUri(ivCover, Uri.fromFile(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (btnBack != null) btnBack.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                finish();
            }
        });
        ListenerHelper.getInstance().addUploadListener(this);
    }

    @Override
    protected void onDestroy() {
        ListenerHelper.getInstance().removeUploadListener(this);
        super.onDestroy();
    }

    @Override
    public void onUploadStarted(UploadRequest uploadRequest) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onUploadComplete(UploadRequest uploadRequest, String response) {
        Log.e(TAG, "upload success: " + response);
        if (tvProgress != null) tvProgress.setText(R.string.msg_upload_video_finished);
    }

    @Override
    public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
        Log.e(TAG, "upload failed: " + errorMessage);
        showToast(getString(R.string.msg_upload_video_failure));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress, long speed) {
        if (progressBar != null) {
            progressBar.setMax(100);
            progressBar.setProgress(progress);
        }
        if (tvProgress != null) tvProgress.setText(progress + getString(R.string.per_uploaded));
    }

    @Override
    public void onCreateVideoSuccess(UploadRequest uploadRequest, CreateVideoResponse response) {
        showToast(response.getDesc());
//        if (response.isSuccess()) finish();
        if (tvProgress != null) tvProgress.setVisibility(View.INVISIBLE);
        if (progressBar != null) progressBar.setVisibility(View.INVISIBLE);
        if (tv_video_ready != null) tv_video_ready.setVisibility(View.VISIBLE);

        new Handler().postDelayed(() -> {
            try {
                if (tv_video_ready != null) {
                    tv_video_ready.setVisibility(View.INVISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 3000);
    }

    @Override
    public void onCreateVideoFailed(UploadRequest uploadRequest, String error) {
        showToast(getString(R.string.service_error));
    }
}

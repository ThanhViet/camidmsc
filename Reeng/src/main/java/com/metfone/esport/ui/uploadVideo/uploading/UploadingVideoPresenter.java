package com.metfone.esport.ui.uploadVideo.uploading;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.Utils;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.UploadVideoEntity;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;

import java.io.File;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

class UploadingVideoPresenter extends BasePresenter<UploadingVideoActivity, BaseModel> {

    public UploadingVideoPresenter(UploadingVideoActivity view, CompositeDisposable disposable) {
        super(view, disposable);

    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }
}

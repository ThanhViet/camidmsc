package com.metfone.esport.ui.games.videoss.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drakeet.multitype.MultiTypeAdapter;
import com.metfone.selfcare.R;

public class GameVideoDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public GameVideoDecoration(Context context) {
        space = context != null ? context.getResources().getDimensionPixelSize(R.dimen.pading_content) : 0;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position = parent.getChildAdapterPosition(view);

        if (position != 0) {
            outRect.top = space;
        }

        if (parent.getAdapter() != null && parent.getAdapter() instanceof MultiTypeAdapter
                && !(((MultiTypeAdapter) parent.getAdapter()).getItems().get(position) instanceof ObjectGameVideoPastBroadcast)) {
            outRect.left = 2 * space;
            outRect.right = 2 * space;
        }

    }
}

package com.metfone.esport.ui.games.tournamentdetails;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.NumberUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.esport.ui.games.tournamentdetails.adapters.TournamentParticipantsAdapter;

import java.util.Date;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.common.Constant.DateFormat.DAY_MMM_YEAR;
import static com.metfone.esport.common.Constant.DateFormat.MONTH_DAY_YEAR_H_M_S_A;

public class TournamentDetailFragment extends BaseFragment<TournamentDetailPresenter> {

    private static String KEY_ID = "KEY_ID";

    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;

    @BindView(R.id.rvTournamentParticipants)
    RecyclerView rvTournamentParticipants;

    @BindView(R.id.lnContent)
    LinearLayout lnContent;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.avTournamentAvatar)
    AvatarView avTournamentAvatar;

    @BindView(R.id.tvTournamentGame)
    AppCompatTextView tvTournamentGame;

    @BindView(R.id.tvTournamentBudget)
    AppCompatTextView tvTournamentBudget;

    @BindView(R.id.tvTournamentDate)
    AppCompatTextView tvTournamentDate;

    @BindView(R.id.tvTournamentParticipants)
    AppCompatTextView tvTournamentParticipants;

    @BindView(R.id.tvTournamentLocation)
    AppCompatTextView tvTournamentLocation;

    private TournamentParticipantsAdapter adapter;

    private TournamentInfoResponse tournament = null;
    private Long tournamentId;

    public static TournamentDetailFragment newInstance(Long tournamentId) {
        TournamentDetailFragment fragment = new TournamentDetailFragment();
        fragment.setArguments(newBundle(tournamentId));
        return fragment;
    }

    public static Bundle newBundle(Long tournamentId) {

        Bundle args = new Bundle();

        args.putLong(KEY_ID, tournamentId);

        return args;
    }

    @Override
    public TournamentDetailPresenter getPresenter() {
        return new TournamentDetailPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tournament_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();

        ivBack.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    private void getDataBundle() {
        if (getArguments() != null)
            tournamentId = getArguments().getLong(KEY_ID, 1);
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        getData();
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        multipleStatusView.showLoading();

        presenter.getData(tournamentId);
    }

    private void initRecyclerView() {
        adapter = new TournamentParticipantsAdapter(getContext(), null);

        rvTournamentParticipants.setHasFixedSize(true);
        rvTournamentParticipants.setAdapter(adapter);
    }

    public void onGetDataSuccess(TournamentInfoResponse response) {
        tournament = response;

        if (tournament == null) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (lnContent != null) lnContent.setVisibility(View.INVISIBLE);

        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (lnContent != null) lnContent.setVisibility(View.VISIBLE);

            adapter.setNewData(tournament.listParticipant);

            setViewTournamentHeader();
        }
    }

    /**
     * Bind data cho thong tin chung giai dau
     * Created_by @dchieu on 9/23/20
     */
    @SuppressLint("SetTextI18n")
    private void setViewTournamentHeader() {
        if (tournament == null) return;

        avTournamentAvatar.setAvatarUrl(tournament.avatar);
        tvTournamentGame.setText(tournament.name);
        tvTournamentBudget.setText("$" + NumberUtils.format((float) tournament.price, true, 0));
        tvTournamentParticipants.setText(getString(R.string.template_number_participants, NumberUtils.format((float) tournament.totalParticipants, true, 0)));
        tvTournamentLocation.setText(tournament.location);

        Date startTime = Common.convertStringToDate(tournament.startDate, MONTH_DAY_YEAR_H_M_S_A);
        Date endTime = Common.convertStringToDate(tournament.endDate, MONTH_DAY_YEAR_H_M_S_A);

        tvTournamentDate.setText(getString(R.string.template_from_date_to_date, Common.convertDateToString(startTime, DAY_MMM_YEAR), Common.convertDateToString(endTime, DAY_MMM_YEAR)));
    }

    public void onGetDataFailure(String message) {
        if (tournament == null) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (lnContent != null) lnContent.setVisibility(View.INVISIBLE);
        }
    }
}

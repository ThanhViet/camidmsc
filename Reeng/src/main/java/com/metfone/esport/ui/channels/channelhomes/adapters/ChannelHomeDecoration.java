package com.metfone.esport.ui.channels.channelhomes.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drakeet.multitype.MultiTypeAdapter;
import com.metfone.esport.ui.games.videoss.adapters.ObjectGameVideoPastBroadcast;
import com.metfone.esport.ui.searchs.adapters.ObjectTitle;
import com.metfone.selfcare.R;

public class ChannelHomeDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public ChannelHomeDecoration(Context context) {
        space = context != null ? context.getResources().getDimensionPixelSize(R.dimen.pading_content) : 0;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position = parent.getChildAdapterPosition(view);

        if (parent.getAdapter() != null && parent.getAdapter() instanceof MultiTypeAdapter) {
            Object item = ((MultiTypeAdapter) parent.getAdapter()).getItems().get(position);

            if (!(item instanceof ObjectGameVideoPastBroadcast) && !(item instanceof ObjectChannelHomeGame)) {
                outRect.left = 2 * space;
                outRect.right = 2 * space;
            }
            if (position != 0) {
                if (item instanceof ObjectTitle) {
                    outRect.top = 2 * space;
                } else {
                    outRect.top = space;
                }
            }
        }
    }
}

package com.metfone.esport.ui.feedback;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ReengAccount;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 9/15/20.
 */
public class FeedbackPresenter extends BasePresenter<FeedbackFragment, BaseModel> {
    public FeedbackPresenter(FeedbackFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void feedback(long videoId, long cateId, long channelId, String desc) {
        ReengAccount mAccount = ApplicationController.self().getReengAccountBusiness().getCurrentAccount();
        RxUtils.async(compositeDisposable, apiService.feedback(mAccount.getNumberJid(), videoId, cateId, channelId, desc, getDefaultParam()),
                new ICallBackResponse<Object>() {

                    @Override
                    public void onSuccess(Object response) {
                        view.onSuccessFeedback();
                    }

                    @Override
                    public void onFail(String error) {
                        view.onFailFeedback(error);
                    }
                });
    }

}

package com.metfone.esport.ui.games.tournamentdetails;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.esport.util.RxUtils;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class TournamentDetailPresenter extends BasePresenter<TournamentDetailFragment, BaseModel> {
    private Disposable apiDispose = null;

    public TournamentDetailPresenter(TournamentDetailFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(final Long tournamentId) {
        if (apiDispose != null)
            apiDispose.dispose();

        apiDispose = RxUtils.async(
                compositeDisposable,
                apiService.getTournamentInfo(tournamentId,
                        getDefaultParam()), new ICallBackResponse<TournamentInfoResponse>() {
                    @Override
                    public void onSuccess(TournamentInfoResponse response) {
                        if (view != null) view.onGetDataSuccess(response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }
                });
    }
}

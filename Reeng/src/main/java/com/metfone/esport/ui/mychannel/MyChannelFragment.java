/*
 * Copyright (c) admin created on 2020-09-15 10:18:59 PM
 */

package com.metfone.esport.ui.mychannel;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.EncodeUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ViewPagerBaseAdapter;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.event.OnEditMyChannelEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.mychannel.editchannel.EditMyChannelFragment;
import com.metfone.esport.ui.mychannel.tababout.TabAboutMyChannelFragment;
import com.metfone.esport.ui.mychannel.tabpastbroadcast.TabPastBroadcastMyChannelFragment;
import com.metfone.esport.ui.mychannel.tabvideo.TabVideoMyChannelFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class MyChannelFragment extends BaseFragment<MyChannelPresenter> {

    @BindView(R.id.btn_back)
    AppCompatImageView btnBack;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.ivAvatar)
    AvatarView ivAvatar;

    @BindView(R.id.tvChannelName)
    AppCompatTextView tvChannelName;

    @BindView(R.id.tvViewers)
    AppCompatTextView tvViewers;

    @BindView(R.id.tvFollowers)
    AppCompatTextView tvFollowers;

    @BindView(R.id.tvDescription)
    AppCompatTextView tvDescription;

    @BindView(R.id.btn_edit)
    AppCompatTextView btnEdit;
    @BindView(R.id.rootView)
    ConstraintLayout rootView;

    private Long channelId = null;
    private ChannelInfoResponse channelInfo = null;
    private boolean isInitViewPager = false;

    @Override
    public MyChannelPresenter getPresenter() {
        return new MyChannelPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_channel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
        Utilities.adaptViewForInsertBottom(rootView);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickBack();
            }
        });
        btnEdit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.KEY_DATA, channelInfo);
                AloneFragmentActivity.with(getActivity()).parameters(bundle).start(EditMyChannelFragment.class);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Serializable serializable = bundle.getSerializable(Constant.KEY_DATA);
            if (serializable instanceof ChannelInfoResponse) {
                channelInfo = (ChannelInfoResponse) serializable;
            }
            channelId = bundle.getLong(Constant.KEY_ID);
        }
        if (channelId > 0) {
            if (multipleStatusView != null) multipleStatusView.showLoading();
            getPresenter().loadChannelInfo(channelId);
        } else if (channelInfo == null) {
            loadData();
        } else {
            multipleStatusView.showContent();
            setupHeaderMyChannel();
            initViewPager(null, null);
        }
    }

    private void initViewPager(ArrayList<VideoInfoResponse> pastBroadcast, ArrayList<VideoInfoResponse> uploaded) {
        isInitViewPager = true;
        List<BaseFragment<?>> fragments = new ArrayList<>();
        List<String> titles = new ArrayList<>();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constant.KEY_DATA, channelInfo);
        if (CollectionUtils.isNotEmpty(pastBroadcast)) {
            bundle.putSerializable(Constant.KEY_ITEMS, pastBroadcast);
        }
        fragments.add(TabPastBroadcastMyChannelFragment.newInstance(bundle));
        titles.add(getString(R.string.tab_past_broadcast));

        bundle = new Bundle();
        bundle.putSerializable(Constant.KEY_DATA, channelInfo);
        if (CollectionUtils.isNotEmpty(uploaded)) {
            bundle.putSerializable(Constant.KEY_ITEMS, uploaded);
        }
        fragments.add(TabVideoMyChannelFragment.newInstance(bundle));
        titles.add(getString(R.string.tab_videos));

        bundle = new Bundle();
        bundle.putSerializable(Constant.KEY_DATA, channelInfo);
        fragments.add(TabAboutMyChannelFragment.newInstance(bundle));
        titles.add(getString(R.string.tab_about));

        if (viewPager != null) {
            viewPager.setAdapter(new ViewPagerBaseAdapter(getChildFragmentManager(), fragments, titles));
            viewPager.setOffscreenPageLimit(fragments.size());
        }
    }

    @SuppressLint("SetTextI18n")
    private void setupHeaderMyChannel() {
        if (channelInfo != null) {
            if (ivAvatar != null) ivAvatar.setAvatarUrl(channelInfo.getUrlAvatar());
            if (tvChannelName != null) tvChannelName.setText(channelInfo.getName());

            String textView;
            long totalViews = channelInfo.getTotalView();
            if (totalViews > 1) {
                textView = getString(R.string.total_views, ViewsUnitEnum.getTextDisplay(totalViews));
            } else {
                textView = getString(R.string.total_view, totalViews + "");
            }
            if (tvViewers != null) {
                tvViewers.setText(EncodeUtils.htmlDecode(textView));
                tvViewers.setVisibility(TextUtils.isEmpty(textView) ? View.GONE : View.VISIBLE);
            }

            String textFollow;
            long totalFollows = channelInfo.getNumFollow();
            if (totalFollows > 1) {
                textFollow = getString(R.string.total_followers, ViewsUnitEnum.getTextDisplay(totalFollows));
            } else {
                textFollow = getString(R.string.total_follower, totalFollows + "");
            }
            if (tvFollowers != null) {
                tvFollowers.setText(EncodeUtils.htmlDecode(textFollow));
                tvFollowers.setVisibility(TextUtils.isEmpty(textFollow) ? View.GONE : View.VISIBLE);
            }
            if (tvDescription != null) tvDescription.setText(channelInfo.getDescription());
        }
    }

    private void loadData() {
        if (multipleStatusView != null) multipleStatusView.showLoading();
        getPresenter().getData();
    }

    public void onGetDataSuccess(ChannelInfoResponse response) {
        if (channelInfo == null || response != null) channelInfo = response;
        if (multipleStatusView != null) multipleStatusView.showContent();
        if (!isInitViewPager) initViewPager(null, null);
        setupHeaderMyChannel();
    }

    public void onGetDataFailure(String message) {
        if (!isInitViewPager) {
            if (multipleStatusView != null) multipleStatusView.showError();
        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();
        }
    }

    public void onGetDataSuccess(ChannelInfoResponse response, ArrayList<VideoInfoResponse> pastBroadcast, ArrayList<VideoInfoResponse> uploaded) {
        if (response != null) channelInfo = response;
        if (multipleStatusView != null) multipleStatusView.showContent();
        if (!isInitViewPager) initViewPager(pastBroadcast, uploaded);
        setupHeaderMyChannel();
    }

    @Subscribe
    public void onEvent(OnEditMyChannelEvent event) {
        if (multipleStatusView != null) multipleStatusView.showLoading();
        getPresenter().loadChannelInfo(channelId);
    }
}

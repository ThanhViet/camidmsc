package com.metfone.esport.ui.mylists;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class MyListContainerPresenter extends BasePresenter<MyListContainerFragment, BaseModel> {
    private Disposable homeDispose = null;

    public MyListContainerPresenter(MyListContainerFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

}

package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.TextRecentClearViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchRecentItemViewBinder extends ItemViewBinder<ObjectRecentSearch, TextRecentClearViewHolder> {
    private TextRecentClearViewHolder.ClickListener clickListener;

    public SearchRecentItemViewBinder(TextRecentClearViewHolder.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NotNull
    @Override
    public TextRecentClearViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new TextRecentClearViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull TextRecentClearViewHolder viewBinderHolder, ObjectRecentSearch s) {
        viewBinderHolder.binData(s.title, clickListener);
    }
}

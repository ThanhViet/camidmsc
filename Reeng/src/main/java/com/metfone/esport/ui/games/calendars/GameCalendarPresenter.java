package com.metfone.esport.ui.games.calendars;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.GameCalendarResponse;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class GameCalendarPresenter extends BasePresenter<GameCalendarFragment, BaseModel> {
    private Disposable apiDispose = null;

    public GameCalendarPresenter(GameCalendarFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(Long gameId) {
        if (apiDispose != null)
            apiDispose.dispose();

        apiDispose = RxUtils.async(
                compositeDisposable,
                apiService.getGameCalendar(gameId, getDefaultParam()),
                new ICallBackResponse<ArrayList<GameCalendarResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<GameCalendarResponse> response) {
                        if (view != null) view.onGetDataSuccess(response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }
                });
    }
}

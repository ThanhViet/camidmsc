/*
 * Copyright (c) admin created on 2020-09-20 04:34:13 PM
 */

package com.metfone.esport.ui.mychannel.editchannel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.EncodeUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.UriUtils;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.event.OnEditMyChannelEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.Serializable;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class EditMyChannelFragment extends BaseFragment<EditMyChannelPresenter> {
    @BindView(R.id.btn_back)
    AppCompatImageView btnBack;

    @BindView(R.id.edit_title)
    AppCompatEditText editTitle;

    @BindView(R.id.edit_desc)
    AppCompatEditText editDesc;

    @BindView(R.id.edit_about)
    AppCompatEditText editAbout;

    @BindView(R.id.edit_title_about)
    AppCompatEditText editTitleAbout;

    @BindView(R.id.btn_update)
    AppCompatTextView btnUpdate;

    @BindView(R.id.iv_avatar)
    AvatarView ivAvatar;

    @BindView(R.id.iv_cover)
    AppCompatImageView ivCover;

    @BindView(R.id.iv_change_avatar)
    AppCompatImageView btnChangeAvatar;

    @BindView(R.id.iv_change_cover)
    AppCompatImageView btnChangeCover;

    @BindView(R.id.tv_count_char_title)
    AppCompatTextView tvCountCharTitle;

    @BindView(R.id.tv_count_char_desc)
    AppCompatTextView tvCountCharDesc;

    @BindView(R.id.iv_about)
    AppCompatImageView ivAbout;

    @BindView(R.id.layout_choose_image_about)
    View btnChooseImageAbout;

    @BindView(R.id.layout_root)
    View layoutRoot;

    @BindView(R.id.ivCancelImageAbout)
    AppCompatImageView ivCancelImageAbout;

    private ChannelInfoResponse channelInfo;
    private File fileAvatar;
    private File fileBanner;
    private File fileAbout;

    @Override
    public EditMyChannelPresenter getPresenter() {
        return new EditMyChannelPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_edit_my_channel;
    }

    private boolean validateEditChannel() {
        try {
            if (editTitle.getText().toString().trim().isEmpty()) {
                showToast("You have not entered the title yet");
                return false;
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
        return true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (getActivity() != null) getActivity().onBackPressed();
            }
        });
        btnUpdate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (validateEditChannel()) {
                    presenter.editChannel(String.valueOf(channelInfo.id),
                            channelInfo.urlImages,
                            channelInfo.urlImagesCover,
                            channelInfo.imageAbout,
                            channelInfo.createdDate,
                            editTitle.getText().toString(),
                            editDesc.getText().toString(),
                            editTitleAbout.getText().toString(),
                            editAbout.getText().toString(),
                            fileAvatar,
                            fileBanner,
                            fileAbout
                    );
                }
            }
        });
        btnChangeAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {

                //Utilities.showPopupChooseCameraOrGallery(getActivity(), Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_AVATAR, Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_AVATAR);
                Utilities.openGallery(getActivity(), Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_AVATAR);
            }
        });
        ivCancelImageAbout.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {

                //Utilities.showPopupChooseCameraOrGallery(getActivity(), Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_AVATAR, Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_AVATAR);
                fileAbout = null;
                ivAbout.setImageResource(0);
                ImageLoader.setImage(ivAbout, "", 0, 0, null);
                ivCancelImageAbout.setVisibility(View.GONE);
            }
        });
        btnChangeCover.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                //Utilities.showPopupChooseCameraOrGallery(getActivity(), Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_COVER, Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_COVER);
                Utilities.openGallery(getActivity(), Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_COVER);
            }
        });
        btnChooseImageAbout.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                //Utilities.showPopupChooseCameraOrGallery(getActivity(), Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_ABOUT, Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_ABOUT);
                Utilities.openGallery(getActivity(), Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_ABOUT);
            }
        });
        if (editTitle != null) {
            editTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharTitle != null)
                        tvCountCharTitle.setText(charSequence.length() + "/100");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (!StringUtils.isEmpty(editable)) {
                        //if (tvWarningTitle != null) tvWarningTitle.setVisibility(View.GONE);
                    }
                }
            });
        }
        if (editDesc != null) {
            editDesc.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharDesc != null)
                        tvCountCharDesc.setText(charSequence.length() + "/200");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (!StringUtils.isEmpty(editable)) {
                        //if (tvWarningDesc != null) tvWarningDesc.setVisibility(View.GONE);
                    }
                }
            });
        }
        KeyboardUtils.hideKeyboardWhenTouch(layoutRoot, getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Serializable serializable = bundle.getSerializable(Constant.KEY_DATA);
            if (serializable instanceof ChannelInfoResponse) {
                channelInfo = (ChannelInfoResponse) serializable;
            }
            if (channelInfo != null) {
                editTitle.setText(channelInfo.name);
                editTitle.setSelection(editTitle.getText().length());
                editDesc.setText(channelInfo.description);
                editTitleAbout.setText(EncodeUtils.htmlDecode(channelInfo.getAboutTitle()));
                editAbout.setText(EncodeUtils.htmlDecode(channelInfo.getAbout()));
                if (ivAvatar != null) ivAvatar.setAvatarUrl(channelInfo.urlAvatar);
                ImageLoader.setCoverChannel(ivCover, channelInfo.urlImagesCover);
                ImageLoader.setAboutChannel(ivAbout, channelInfo.imageAbout);
                if (StringUtils.isNotEmpty(channelInfo.imageAbout)){
                    ivCancelImageAbout.setVisibility(View.VISIBLE);
                }else {
                    ivCancelImageAbout.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_AVATAR: {
                    try {
                        if (data != null && data.getExtras() != null) {
                            Object object = data.getExtras().get("data");
                            if (object instanceof Bitmap) {
                                File file = FileUtils.saveBitmapToFile((Bitmap) object);
                                if (file != null) {
                                    if (ivAvatar != null) ivAvatar.setAvatarUri(Uri.fromFile(file));
                                    fileAvatar = file;
                                    channelInfo.urlImages = "";
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                return;
                case Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_COVER: {
                    try {
                        if (data != null && data.getExtras() != null) {
                            Object object = data.getExtras().get("data");
                            if (object instanceof Bitmap) {
                                File file = FileUtils.saveBitmapToFile((Bitmap) object);
                                if (file != null) {
                                    ImageLoader.setImageUri(ivCover, Uri.fromFile(file));
                                    fileBanner = file;
                                    channelInfo.urlImagesCover = "";
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                return;
                case Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_ABOUT: {
                    try {
                        if (data != null && data.getExtras() != null) {
                            Object object = data.getExtras().get("data");
                            if (object instanceof Bitmap) {
                                File file = FileUtils.saveBitmapToFile((Bitmap) object);
                                if (file != null) {
                                    ImageLoader.setImageUri(ivAbout, Uri.fromFile(file));
                                    fileAbout = file;
                                    channelInfo.imageAbout = "";
                                    ivCancelImageAbout.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                return;
                case Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_AVATAR: {
                    try {
                        if (data != null && data.getData() != null) {
                            if (ivAvatar != null) ivAvatar.setAvatarUri(data.getData());
                            fileAvatar = UriUtils.uri2File(data.getData());
                            channelInfo.urlImages = "";
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                return;
                case Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_COVER: {
                    try {
                        if (data != null && data.getData() != null) {
                            ImageLoader.setImageUri(ivCover, data.getData());
                            fileBanner = UriUtils.uri2File(data.getData());
                            channelInfo.urlImagesCover = "";
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                return;
                case Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_ABOUT: {
                    try {
                        if (data != null && data.getData() != null) {
                            ImageLoader.setImageUri(ivAbout, data.getData());
                            fileAbout = UriUtils.uri2File(data.getData());
                            ivCancelImageAbout.setVisibility(View.VISIBLE);
                            channelInfo.imageAbout = "";
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onSuccessEditChannel() {
        EventBus.getDefault().post(new OnEditMyChannelEvent());
        getActivity().onBackPressed();
    }
}

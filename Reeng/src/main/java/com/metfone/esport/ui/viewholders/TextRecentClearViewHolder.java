package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TextRecentClearViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTextRecent)
    AppCompatTextView tvTextRecent;

    @BindView(R.id.tvClear)
    AppCompatTextView tvClear;

    public TextRecentClearViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_text_recent_search, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

//    public TextTitleViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//        ButterKnife.bind(this, itemView);
//    }

    public void binData(String entity, ClickListener clickListener) {
        tvTextRecent.setText(entity);

        tvClear.setOnClickListener(v -> {
            if (clickListener != null)
                clickListener.onClickClear();
        });
    }

    public interface ClickListener {
        void onClickClear();
    }
}

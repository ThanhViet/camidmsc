package com.metfone.esport.ui.checkRequest;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.InputType;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.metfone.esport.base.BaseActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.home_kh.api.response.CheckRequest;
import com.metfone.selfcare.module.home_kh.api.response.CheckRequestHistory;
import com.metfone.selfcare.module.home_kh.api.response.CheckRequestResponse;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.ui.view.CamIdTextView;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckRequestActivity extends BaseActivity {

    ApiService apiService;
    @BindView(R.id.ivBack)
    AppCompatImageView btnBack;
    @BindView(R.id.ln_checkbox)
    LinearLayout lnCheckBox;
    @BindView(R.id.ln_forget)
    LinearLayout lnForget;
    @BindView(R.id.edtRequestId)
    EditText edtRequestId;
    @BindView(R.id.btn_expand)
    ImageView btnExpand;
    @BindView(R.id.ln_expand_infor)
    LinearLayout lnExpandInfor;
    @BindView(R.id.sv_container)
    ScrollView svContainer;
    @BindView(R.id.ln_check_request_result)
    LinearLayout lnCheckRequestInfor;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_phone_number)
    TextView tvPhoneNumber;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_request_id)
    TextView tvRequestId;
    @BindView(R.id.tv_request_date)
    TextView tvRequestDate;
    @BindView(R.id.tv_customer_name)
    TextView tvCustomerName;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.tv_request_speed)
    TextView tvRequestSpeed;
    @BindView(R.id.tv_monthly_fee)
    TextView tvMonthlyFee;
    @BindView(R.id.tv_modern_wifi)
    TextView tvModern;
    @BindView(R.id.tv_installation)
    TextView tvInstallation;
    @BindView(R.id.tv_pay_advance)
    TextView tvPayAdvance;
    @BindView(R.id.tv_deposit)
    TextView tvDeposit;
    @BindView(R.id.tv_reason)
    TextView tvReason;
    @BindView(R.id.btn_search)
    TextView btnSearch;
    @BindView(R.id.edt_phone)
    EditText edtPhone;
    @BindView(R.id.view_loading)
    FrameLayout loading;
    @BindView(R.id.v_step1)
    View vStep1;
    @BindView(R.id.v_step2)
    View vStep2;
    @BindView(R.id.v_step3)
    View vStep3;
    @BindView(R.id.iv_step1)
    View ivStep1;
    @BindView(R.id.iv_step2)
    View ivStep2;
    @BindView(R.id.iv_step3)
    View ivStep3;
    @BindView(R.id.iv_step4)
    View ivStep4;
    @BindView(R.id.ckb_content1)
    CheckBox checkbox;
    @BindView(R.id.ln_step2)
    LinearLayout lnStep2;
    @BindView(R.id.ln_step3)
    LinearLayout lnStep3;
    @BindView(R.id.tv_step2)
    CamIdTextView tvStep2;
    @BindView(R.id.tv_step3)
    CamIdTextView tvStep3;


    @Override
    public Object getPresenter() {
        return null;
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_request;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeStatusBar(Color.parseColor("#C41627"));
        btnBack.setOnClickListener(v -> finish());
        lnCheckBox.setOnClickListener(v -> {
            if (lnForget.getVisibility() == View.VISIBLE) {
                lnCheckBox.setBackground(getResources().getDrawable(R.drawable.bg_forgot_request_id));
                lnForget.setVisibility(View.GONE);
                edtRequestId.setBackground(getResources().getDrawable(R.drawable.bg_edittext_enter_id));
                edtRequestId.setInputType(InputType.TYPE_CLASS_TEXT);
                edtPhone.setText("");
                checkbox.setChecked(false);
            } else {
                edtRequestId.setText("");
                lnCheckBox.setBackground(getResources().getDrawable(R.drawable.bg_forgot_request_id_selected));
                lnForget.setVisibility(View.VISIBLE);
                edtRequestId.setBackground(getResources().getDrawable(R.drawable.bg_edittext_enter_id_disable));
                edtRequestId.setInputType(InputType.TYPE_NULL);
                checkbox.setChecked(true);
            }
            showResult(false);
        });
        btnExpand.setOnClickListener(v -> {
            if (lnExpandInfor.getVisibility() == View.GONE) {
                lnExpandInfor.setVisibility(View.VISIBLE);
                svContainer.post(() -> svContainer.fullScroll(ScrollView.FOCUS_DOWN));
                btnExpand.setRotation(180);
            } else {
                lnExpandInfor.setVisibility(View.GONE);
                btnExpand.setRotation(0);
            }
        });
        btnSearch.setOnClickListener(v -> {
            if (!NetworkHelper.isConnectInternet(this)) {
                showError(getString(R.string.error_internet_disconnect), null);
            } else {
                hideKeyboard();
                showResult(false);
                if (lnForget.getVisibility() == View.VISIBLE) {
                    if (edtPhone.getText().toString().trim().length() == 0) {
                        showError(getString(R.string.check_request_please_enter_your_phone_number), null);
                    } else {
                        loading.setVisibility(View.VISIBLE);
                        forgotRequest();
                    }
                } else {
                    if (edtRequestId.getText().toString().trim().length() == 0) {
                        showError(getString(R.string.check_request_please_enter_your_request_id), null);
                    } else {
                        loading.setVisibility(View.VISIBLE);
                        searchFTTHRequest(edtRequestId.getText().toString());
                    }
                }
            }
        });
    }

    public void showResult(boolean isShow) {
        if (isShow) {
            lnCheckRequestInfor.setVisibility(View.VISIBLE);
            lnStep2.post(() -> {
                if (!isFinishing()) {
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) tvStep2.getLayoutParams();
                    params.leftMargin = Math.round(lnStep2.getX() - (tvStep2.getWidth() / 2) + (ivStep2.getWidth() / 2));
                    tvStep2.setLayoutParams(params);
                }
            });
            lnStep3.post(() -> {
                if (!isFinishing()) {
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) tvStep3.getLayoutParams();
                    params.leftMargin = Math.round(lnStep3.getX() - (tvStep3.getWidth() / 2) + (ivStep3.getWidth() / 2));
                    tvStep3.setLayoutParams(params);
                }
            });
        } else {
            lnCheckRequestInfor.setVisibility(View.GONE);
        }
    }

    private void searchFTTHRequest(String requestId) {
        new MetfonePlusClient().searchFTTHRequest(true, requestId, new MPApiCallback<CheckRequestResponse>() {
            @Override
            public void onResponse(Response<CheckRequestResponse> response) {
                loading.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getRequest().getRequest() != null) {
                        updateData(response.body().getRequest().getRequest());
                    } else {
                        showError(response.body().getRequest().getMessage(), null);
                    }
                } else {
                    showError(getResources().getString(R.string.e601_error_but_undefined), null);
                }
            }

            @Override
            public void onError(Throwable error) {
                loading.setVisibility(View.GONE);
                showError(getResources().getString(R.string.e601_error_but_undefined), null);
            }
        });
    }

    private void forgotRequest() {
        hideKeyboard();
        new MetfonePlusClient().searchFTTHRequest(false, edtPhone.getText().toString(), new MPApiCallback<CheckRequestResponse>() {
            @Override
            public void onResponse(Response<CheckRequestResponse> response) {
                loading.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getRequest().getRequest() != null) {
                        updateData(response.body().getRequest().getRequest());
                    } else {
                        showError(response.body().getRequest().getMessage(), null);
                    }
                } else {
                    showError(getResources().getString(R.string.e601_error_but_undefined), null);
                    loading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable error) {
                loading.setVisibility(View.GONE);
                showError(getResources().getString(R.string.e601_error_but_undefined), null);
            }
        });
    }

    public void updateData(CheckRequest request) {
        showResult(true);
        tvCustomerName.setText(request.getCustomerName());
        tvEmail.setText(request.getCustomerMail());
        tvPhoneNumber.setText(request.getCustomerPhone());
        tvAddress.setText(request.getAddress());
        tvRequestId.setText(request.getId());
        tvRequestDate.setText(request.getCreateDateStr());
        setHtmlText(tvAccount, "<b>" + getResources().getString(R.string.check_request_account)
                + "</b>   <font color=\"#4165F8\">" + request.getAccount() + "</font>");
        setHtmlText(tvRequestSpeed, "<b>" + getResources().getString(R.string.check_request_request)
                + "</b>  " + request.getSpeed());
        setHtmlText(tvMonthlyFee, "<b>" + getResources().getString(R.string.check_request_monthly_fee)
                + "</b>  " + request.getMonthFee());
        setHtmlText(tvModern, "<b>" + getResources().getString(R.string.check_request_modern)
                + "</b>  " + request.getModemWifi());
        setHtmlText(tvInstallation, "<b>" + getResources().getString(R.string.check_request_install)
                + "</b>  " + request.getInstalation());
        setHtmlText(tvPayAdvance, "<b>" + getResources().getString(R.string.check_request_pay_advance)
                + "</b>  " + request.getPayAdvance());
        setHtmlText(tvDeposit, "<b>" + getResources().getString(R.string.check_request_deposit)
                + "</b>  " + request.getDeposit());
        setHtmlText(tvReason, "<b>" + getResources().getString(R.string.check_request_reason)
                + "</b>  " + request.getReason());

        int step = request.getStatus();
        if (step == 0 || step == 3) {
            ivStep1.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep2.setBackgroundResource(R.drawable.ic_white_dot);
            ivStep3.setBackgroundResource(R.drawable.ic_white_dot);
            ivStep4.setBackgroundResource(R.drawable.ic_white_dot);
            vStep1.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.white));
            vStep2.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.white));
            vStep3.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.white));
        } else if (step == 1) {
            ivStep1.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep2.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep3.setBackgroundResource(R.drawable.ic_white_dot);
            ivStep4.setBackgroundResource(R.drawable.ic_white_dot);
            vStep1.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.color_topic6));
            vStep2.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.white));
            vStep3.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.white));
        } else if (step == 2) {
            ivStep1.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep2.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep3.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep4.setBackgroundResource(R.drawable.ic_white_dot);
            vStep1.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.color_topic6));
            vStep2.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.color_topic6));
            vStep3.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.white));
        }  else {
            ivStep1.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep2.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep3.setBackgroundResource(R.drawable.ic_green_dot);
            ivStep4.setBackgroundResource(R.drawable.ic_green_dot);
            vStep1.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.color_topic6));
            vStep2.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.color_topic6));
            vStep3.setBackgroundColor(ContextCompat.getColor(CheckRequestActivity.this, R.color.color_topic6));
        }
    }

    public int getCurrentStep(ArrayList<CheckRequestHistory> histories) {
        int step = 0;
        for (int i = 0; i < histories.size(); i++) {
            if (step < Integer.parseInt(histories.get(i).getNewValue())) {
                step = Integer.parseInt(histories.get(i).getNewValue());
            }
        }
        return step;
    }

    public void hideKeyboard() {
        Handler handler = new Handler();
        handler.postDelayed(() -> InputMethodUtils.hideSoftKeyboard(CheckRequestActivity.this), 200);
    }

    private void setHtmlText(TextView tv, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(text));
        }
    }
}

package com.metfone.esport.ui.searchs.adapters;

import com.metfone.esport.entity.repsonse.ChannelInfoResponse;

public class ObjectSearchChannelLike {
    public ChannelInfoResponse channelInfo;

    public ObjectSearchChannelLike(Object channelInfo) {
        if (channelInfo instanceof ChannelInfoResponse)
            this.channelInfo = (ChannelInfoResponse) channelInfo;
    }
}

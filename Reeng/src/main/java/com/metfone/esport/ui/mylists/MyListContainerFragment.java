package com.metfone.esport.ui.mylists;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.ViewPager;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ViewPagerBaseAdapter;
import com.metfone.esport.ui.mylists.mylistchanels.MyListChanelFragment;
import com.metfone.esport.ui.mylists.mylistgames.MyListGameFragment;
import com.metfone.esport.ui.mylists.mylistvideos.MyListVideoFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class MyListContainerFragment extends BaseFragment<MyListContainerPresenter> {

    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;

    @BindView(R.id.vpMyList)
    ViewPager vpMyList;

    public static MyListContainerFragment newInstance() {
        Bundle args = new Bundle();
        MyListContainerFragment fragment = new MyListContainerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public MyListContainerPresenter getPresenter() {
        return new MyListContainerPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivBack.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        initViewPager();
    }

    private void initViewPager() {
        List<BaseFragment<?>> fragments = new ArrayList<>();
        List<String> titles = new ArrayList<>();

        fragments.add(MyListChanelFragment.newInstance());
        titles.add(getString(R.string.menu_my_list_chanel));

        fragments.add(MyListGameFragment.newInstance());
        titles.add(getString(R.string.menu_my_list_games));

        fragments.add(MyListVideoFragment.newInstance());
        titles.add(getString(R.string.menu_my_list_videos));

        vpMyList.setAdapter(new ViewPagerBaseAdapter(getChildFragmentManager(), fragments, titles));
        vpMyList.setOffscreenPageLimit(fragments.size());
    }
}

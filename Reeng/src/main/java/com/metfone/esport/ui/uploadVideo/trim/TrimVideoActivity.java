/*
 * Created by admin on 2020/9/6
 */

package com.metfone.esport.ui.uploadVideo.trim;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.DialogUtils;
import com.iknow.trimvideo.listener.VideoTrimListener;
import com.iknow.trimvideo.widget.VideoTrimmerView;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.common.Constant;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.uploadVideo.upload.UploadVideoActivity;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class TrimVideoActivity extends BaseActivity implements VideoTrimListener {

    @BindView(R.id.btn_back)
    @Nullable
    View btnBack;
    @BindView(R.id.trimmer_view)
    @Nullable
    VideoTrimmerView trimmerView;
    @BindView(R.id.btn_next)
    @Nullable
    View btnNext;
    private Dialog dialogLoading;

    @Override
    public Object getPresenter() {
        return null;
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_trim_video;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String filePath = getIntent().getStringExtra(Constant.KEY_FILE_PATH);
        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.getWindow().setStatusBarColor(Color.parseColor("#161819"));
        }
        if (btnBack != null) btnBack.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                finish();
            }
        });

        if (btnNext != null) btnNext.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                if (trimmerView != null) {
                    trimmerView.onSaveClicked();
                }
            }
        });
        if (trimmerView != null) {
            trimmerView.setTextDescription(getString(R.string.trim_video_description));
            trimmerView.setOnTrimVideoListener(this);
            if (Utilities.notEmpty(filePath)) {
                try {
                    trimmerView.initVideoByURI(Uri.parse(filePath));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onStartTrim() {
        if (dialogLoading != null) dialogLoading.dismiss();
        dialogLoading = DialogUtils.create(this, R.layout.dialog_loading_es);
        dialogLoading.show();
    }

    @Override
    public void onCancelTrim() {
        if (dialogLoading != null) dialogLoading.dismiss();
        showToast(getString(R.string.msg_cancel_trim_video));
    }

    @Override
    public void onTrimSuccess(String filePath, String output) {
        Log.d(TAG, "onTrimSuccess filePath: " + filePath);
        if (dialogLoading != null) dialogLoading.dismiss();
        if (ActivityUtils.isActivityAlive(this)) {
            Intent intent = new Intent(this, UploadVideoActivity.class);
            intent.putExtra(Constant.KEY_FILE_PATH, filePath);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onTrimFailure(String message) {
        if (dialogLoading != null) dialogLoading.dismiss();
        showToast(R.string.msg_trim_failure);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (trimmerView != null) {
            trimmerView.onVideoPause();
            trimmerView.setRestoreState(true);
        }
    }

    @Override
    protected void onDestroy() {
        if (trimmerView != null) trimmerView.onDestroy();
        super.onDestroy();
    }

}

package com.metfone.esport.ui.popups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.ExtKeyPair;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;

class SingleTextSelectAdapter extends BaseListAdapter<ExtKeyPair<?, String>, SingleTextSelectAdapter.ViewHolder> {
    private IOnClickItem onClickItem;

    public SingleTextSelectAdapter(Context context, IOnClickItem onClickItem) {
        super(context);

        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_single_text_select, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        holder.bindData(context, mData.get(position), onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @BindView(R.id.tvSingleText)
        AppCompatTextView tvSingleText;

        public void bindData(Context context, ExtKeyPair<?, String> data, IOnClickItem onClickItem) {
            if (data == null) return;

            tvSingleText.setText(data.value);

            if (data.isSelected) {
                tvSingleText.setTextColor(ContextCompat.getColor(context, R.color.white_opacity_800));
            } else {
                tvSingleText.setTextColor(ContextCompat.getColor(context, R.color.white_opacity_500));
            }

            itemView.setOnClickListener(v -> {
                if (onClickItem != null)
                    onClickItem.onClickItem(data);
            });
        }
    }

    public interface IOnClickItem {
        void onClickItem(ExtKeyPair<?, String> e);
    }
}

/*
 * Copyright (c) admin created on 2020-09-15 10:54:18 PM
 */

package com.metfone.esport.ui.mychannel.tababout;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.EncodeUtils;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.ImageLoader;

import java.io.Serializable;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class TabAboutMyChannelFragment extends BaseFragment<TabAboutMyChannelPresenter> {

    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;

    @BindView(R.id.tv_desc)
    AppCompatTextView tvDesc;

    @BindView(R.id.iv_cover)
    AppCompatImageView ivCover;
    private ChannelInfoResponse channelInfo;

    public static TabAboutMyChannelFragment newInstance(Bundle bundle) {
        TabAboutMyChannelFragment fragment = new TabAboutMyChannelFragment();
        if (bundle != null) fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public TabAboutMyChannelPresenter getPresenter() {
        return new TabAboutMyChannelPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_about_my_channel;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Serializable serializable = bundle.getSerializable(Constant.KEY_DATA);
            if (serializable instanceof ChannelInfoResponse) {
                channelInfo = (ChannelInfoResponse) serializable;
                setupChannelInfo();
            }
        }
    }

    public void setupChannelInfo() {
        if (channelInfo != null) {
            if (tvTitle != null) tvTitle.setText(EncodeUtils.htmlDecode(channelInfo.getAboutTitle()));
            if (tvDesc != null) tvDesc.setText(EncodeUtils.htmlDecode(channelInfo.getAbout()));
            ImageLoader.setAboutChannel(ivCover, channelInfo.getImageAbout());
        }
    }
}

package com.metfone.esport.ui.viewholders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeMetfoneViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivThumbVideo)
    AppCompatImageView ivThumbVideo;

    @BindView(R.id.tvEyeViewCount)
    AppCompatTextView tvEyeViewCount;

    @BindView(R.id.tvViews)
    AppCompatTextView tvViews;

    @BindView(R.id.tvVideoTitle)
    AppCompatTextView tvVideoTitle;

    @BindView(R.id.tvVideoSubTitle)
    AppCompatTextView tvVideoSubTitle;

    @BindView(R.id.tvVideoDescription)
    AppCompatTextView tvVideoDescription;

    @BindView(R.id.tvTimeUploadAgo)
    AppCompatTextView tvTimeUploadAgo;

    @BindView(R.id.tvLive)
    AppCompatTextView tvLive;

    @BindView(R.id.avAvatar)
    AvatarView avAvatar;

    @BindView(R.id.videoLive)
    View videoLive;

    @BindView(R.id.channelInfo)
    View channelInfo;

    @BindView(R.id.layout_eye_viewer)
    View layout_eye_viewer;

    private Context context;

    public HomeMetfoneViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_home_metfone, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        context = itemView.getContext();

        ButterKnife.bind(this, itemView);
    }

    public void binData(VideoInfoResponse entity, IOnClickItem onClickItem, boolean isShowBackground) {
        if (isShowBackground)
            itemView.setBackgroundResource(R.drawable.bg_video_with_info_radius_6_es);

        binData(entity, onClickItem);

    }

    public void binData(VideoInfoResponse entity, IOnClickItem onClickItem) {
        try {
            ImageLoader.setImage(
                    ivThumbVideo,
                    entity.image_path,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    new MultiTransformation<>(new CenterCrop(), new RoundedCorners(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall))));

            if (entity.isLive()) {
                tvTimeUploadAgo.setVisibility(View.INVISIBLE);

                tvViews.setVisibility(View.INVISIBLE);

                tvLive.setVisibility(View.VISIBLE);

                if (entity.ccu <= 0) {
                    layout_eye_viewer.setVisibility(View.INVISIBLE);
                } else {
                    layout_eye_viewer.setVisibility(View.VISIBLE);
                    tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(entity.ccu));
                }

            } else {
                tvLive.setVisibility(View.INVISIBLE);

                if (entity.type_video == 1) { // uploaded
                    layout_eye_viewer.setVisibility(View.INVISIBLE);

                    if (entity.total_view <= 0) {
                        tvViews.setVisibility(View.INVISIBLE);
                    } else {
                        tvViews.setVisibility(View.VISIBLE);
                        tvViews.setText(itemView.getContext().getString(entity.total_view > 1 ? R.string.template_views_count : R.string.template_view_count, ViewsUnitEnum.getTextDisplay(entity.total_view)));
                    }
                } else { // past broad cast
                    tvViews.setVisibility(View.INVISIBLE);

                    if (entity.total_view <= 0) {
                        layout_eye_viewer.setVisibility(View.INVISIBLE);
                    } else {
                        layout_eye_viewer.setVisibility(View.VISIBLE);
                        tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(entity.total_view));
                    }
                }

                if (entity.publish_time <= 0)
                    tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                else {
                    tvTimeUploadAgo.setVisibility(View.VISIBLE);
                    tvTimeUploadAgo.setText(Utilities.calculateTime(context, entity.publish_time));
                }
            }

            tvVideoSubTitle.setText(entity.title);

            if (entity.channelInfo != null) {
                tvVideoTitle.setText(entity.channelInfo.name);

                avAvatar.setAvatarUrl(entity.channelInfo.urlAvatar);
            } else {
                tvVideoTitle.setText("");

                avAvatar.setImageResource(R.drawable.bg_avatar_default_es);
            }

            if (entity.gameInfo != null) {
                tvVideoDescription.setText(entity.gameInfo.gameName);
            } else {
                tvVideoDescription.setText("");
            }

            avAvatar.setOnClickListener(v -> {
                if (onClickItem != null && entity.channelInfo != null) {
                    onClickItem.onClickChannel(entity.channelInfo);
                }
            });

            tvVideoTitle.setOnClickListener(v -> {
                if (onClickItem != null && entity.channelInfo != null) {
                    onClickItem.onClickChannel(entity.channelInfo);
                }
            });

            videoLive.setOnClickListener(new OnSingleClickListener() {
                @Override
                public boolean isCheckNetwork() {
                    return true;
                }

                @Override
                public void onSingleClick(View view) {
                    if (onClickItem != null) {
                        onClickItem.onClickVideo(entity);
                    }
                }
            });

            tvVideoSubTitle.setOnClickListener(v -> {
                if (onClickItem != null) {
                    onClickItem.onClickVideo(entity);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void binDataByChannel(ChannelInfoResponse entity, IOnClickItem onClickItem) {
        itemView.setBackgroundResource(R.drawable.bg_video_with_info_radius_6_es);

        if (entity == null) return;
        try {

            if (entity.video != null) {
                ImageLoader.setImage(
                        ivThumbVideo,
                        entity.video.image_path_thumb,
                        R.drawable.bg_image_placeholder_es,
                        R.drawable.bg_image_error_es,
                        new MultiTransformation<>(new CenterCrop(), new RoundedCorners(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall))));

                if (entity.video.isLive()) {
                    tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                    tvLive.setVisibility(View.VISIBLE);

                    if (entity.video.ccu <= 0) {
                        layout_eye_viewer.setVisibility(View.INVISIBLE);
                    } else {
                        layout_eye_viewer.setVisibility(View.VISIBLE);
                        tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(entity.video.ccu));
                    }

                } else {
                    tvLive.setVisibility(View.INVISIBLE);

                    if (entity.video.type_video == 1) { // uploaded
                        layout_eye_viewer.setVisibility(View.INVISIBLE);

                        if (entity.video.total_view <= 0) {
                            tvViews.setVisibility(View.INVISIBLE);
                        } else {
                            tvViews.setVisibility(View.VISIBLE);
                            tvViews.setText(itemView.getContext().getString(entity.video.total_view > 1 ? R.string.template_views_count : R.string.template_view_count, ViewsUnitEnum.getTextDisplay(entity.video.total_view)));
                        }
                    } else { // past broad cast
                        tvViews.setVisibility(View.INVISIBLE);

                        if (entity.video.total_view <= 0) {
                            layout_eye_viewer.setVisibility(View.INVISIBLE);
                        } else {
                            layout_eye_viewer.setVisibility(View.VISIBLE);
                            tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(entity.video.total_view));
                        }
                    }

                    if (entity.video.publish_time <= 0)
                        tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                    else {
                        tvTimeUploadAgo.setVisibility(View.VISIBLE);
                        tvTimeUploadAgo.setText(Utilities.calculateTime(context, entity.video.publish_time));
                    }
                }

                tvVideoSubTitle.setText(entity.video.title);

                if (entity.video.gameInfo != null)
                    tvVideoDescription.setText(entity.video.gameInfo.gameName);

                videoLive.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public boolean isCheckNetwork() {
                        return true;
                    }

                    @Override
                    public void onSingleClick(View view) {
                        if (onClickItem != null) {
                            onClickItem.onClickVideo(entity.video);
                        }
                    }
                });

                tvVideoSubTitle.setOnClickListener(v -> {
                    if (onClickItem != null) {
                        onClickItem.onClickVideo(entity.video);
                    }
                });
            }

            tvVideoTitle.setText(entity.name);

            avAvatar.setAvatarUrl(entity.urlAvatar);

            avAvatar.setOnClickListener(v -> {
                if (onClickItem != null) {
                    onClickItem.onClickChannel(entity);
                }
            });

            tvVideoTitle.setOnClickListener(v -> {
                if (onClickItem != null) {
                    onClickItem.onClickChannel(entity);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface IOnClickItem {
        void onClickVideo(VideoInfoResponse video);

        void onClickChannel(ChannelInfoResponse channel);
    }

    public interface IScrollData{
        void onScrollData();
    }
}

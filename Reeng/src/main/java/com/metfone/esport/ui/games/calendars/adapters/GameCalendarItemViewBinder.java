package com.metfone.esport.ui.games.calendars.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.entity.repsonse.GameMatchResponse;
import com.metfone.esport.ui.viewholders.GameCalendarViewHolder;

import org.jetbrains.annotations.NotNull;

public class GameCalendarItemViewBinder extends ItemViewBinder<GameMatchResponse, GameCalendarViewHolder> {

    private GameCalendarViewHolder.IOnClickItem onClickItem;

    public GameCalendarItemViewBinder(GameCalendarViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public GameCalendarViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new GameCalendarViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull GameCalendarViewHolder viewHolder, GameMatchResponse o) {
        viewHolder.binData(o, onClickItem);
    }
}

package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.ui.viewholders.MyListChanelViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchChannelItemViewBinder extends ItemViewBinder<ChannelInfoResponse, MyListChanelViewHolder> {
    private MyListChanelViewHolder.IOnClickItem onClickItem;

    public SearchChannelItemViewBinder(MyListChanelViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public MyListChanelViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new MyListChanelViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull MyListChanelViewHolder viewBinderHolder, ChannelInfoResponse s) {
        viewBinderHolder.binDataByFollowers(s, onClickItem);
    }
}

package com.metfone.esport.ui.editvideo;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.UploadVideoEntity;
import com.metfone.esport.entity.repsonse.CreateVideoResponse;
import com.metfone.esport.entity.repsonse.GameInfoEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.service.FileServiceRetrofit;
import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

class EditVideoPresenter extends BasePresenter<EditVideoFragment, BaseModel> {

    public EditVideoPresenter(EditVideoFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void loadCategory(long channelId) {
        //channelId = 369;
        RxUtils.async(compositeDisposable, apiService.getGameOfChannel(channelId, AccountBusiness.getInstance().getPhoneNumber(), getDefaultParam()), new ICallBackResponse<ArrayList<GameInfoEntity>>() {
            @Override
            public void onSuccess(ArrayList<GameInfoEntity> response) {
                if (view != null) view.updateCategories(response);
            }

            @Override
            public void onFail(String error) {

            }
        });
    }

    public void editUploadVideo(UploadVideoEntity entity, VideoInfoResponse currentVideo) {
        RequestBody imageLead = null;
        Observable<CreateVideoResponse> observable;
        String thumbPath = CollectionUtils.isNotEmpty(entity.listPathThumb) ? entity.listPathThumb.get(0) : "";
        File fileThumb = null;
        if (StringUtils.isNotEmpty(thumbPath)) {
            fileThumb = new File(thumbPath);
        }
        MultipartBody.Part bodyThumb = null;
        if (fileThumb != null) {
            MediaType imageType = MediaType.parse("image/jpeg");
            RequestBody requestFile = RequestBody.Companion.create(fileThumb, imageType);
            bodyThumb = MultipartBody.Part.createFormData("imageUpload", fileThumb.getName(), requestFile);
        }
        RequestBody msisdn = RequestBody.create(ApplicationController.getApp().getReengAccountBusiness().getJidNumber(), MultipartBody.FORM);
        RequestBody channelId = RequestBody.create("" + entity.channelId, MultipartBody.FORM);
        RequestBody videoId = RequestBody.create("" + entity.videoId, MultipartBody.FORM);
        RequestBody categoryId = RequestBody.create("" + entity.categoryId, MultipartBody.FORM);
        RequestBody videoTitle = RequestBody.create("" + entity.title, MultipartBody.FORM);
        RequestBody videoDesc = RequestBody.create("" + entity.description, MultipartBody.FORM);
        RequestBody itemVideo = RequestBody.create("" + entity.itemVideo, MultipartBody.FORM);
        RequestBody schedule = RequestBody.create("" + entity.publishTime, MultipartBody.FORM);
        if (fileThumb == null) {
            imageLead = RequestBody.create("" + currentVideo.image_path, MultipartBody.FORM);
        }
        RequestBody createDate = RequestBody.create("" + currentVideo.created_at, MultipartBody.FORM);


        observable = FileServiceRetrofit.getInstance().createVideoUploadByUser(
                channelId, videoId, categoryId, videoTitle, videoDesc,
                itemVideo, schedule, imageLead,createDate, bodyThumb, ServiceRetrofit.getDefaultParam());
        RxUtils.asyncSimple(new CompositeDisposable(), observable, new ICallBackResponse<CreateVideoResponse>() {
            @Override
            public void onSuccess(CreateVideoResponse response) {
                if (response.isSuccess()) {
                    if (view != null) view.onEditVideoSuccess();
                } else {
                    if (view != null) view.onEditVideoFailed();
                }
            }

            @Override
            public void onFail(String error) {
                if (view != null) view.onEditVideoFailed();
            }
        });
    }
}

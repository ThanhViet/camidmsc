package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeGameViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivGameThumb)
    AppCompatImageView ivGameThumb;

    @BindView(R.id.tvComingSoon)
    AppCompatTextView tvComingSoon;

    @BindView(R.id.ivDotLive)
    AppCompatImageView ivDotLive;

    @BindView(R.id.tvViewCount)
    AppCompatTextView tvViewCount;

//    public HomeGameViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//    }

    public HomeGameViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_home_game, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

    public void binData(GameInfoResponse entity) {
        binData(entity, true, null);
    }

    public void binData(GameInfoResponse entity, IOnClickItem onClickItem) {
        binData(entity, true, onClickItem);
    }

    public void binData(GameInfoResponse entity, boolean isShowViews, IOnClickItem onClickItem) {
        if (entity == null) return;

        try {
            tvViewCount.setText(tvViewCount.getContext().getString(entity.totalView > 1 ? R.string.template_viewers_count : R.string.template_viewer_count, ViewsUnitEnum.getTextDisplay(entity.totalView)));

            ImageLoader.setImage(
                    ivGameThumb,
                    entity.gameImage,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    new MultiTransformation<>(new CenterCrop(), new RoundedCorners(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall))));

            if (isShowViews) {
                if (entity.active == 1) {
                    tvComingSoon.setVisibility(View.INVISIBLE);

                    if (entity.isLive == 1) {
                        tvViewCount.setVisibility(View.VISIBLE);
                        ivDotLive.setVisibility(View.VISIBLE);
                    } else {
                        tvViewCount.setVisibility(View.INVISIBLE);
                        ivDotLive.setVisibility(View.INVISIBLE);
                    }

                    if (onClickItem != null) {
                        itemView.setOnClickListener(v -> onClickItem.onClickItem(entity));

                        itemView.setOnLongClickListener(v -> {
                            onClickItem.onLongClickItem(entity);

                            return false;
                        });
                    }
                } else {
                    tvComingSoon.setText(entity.gameStatus);
                    tvComingSoon.setVisibility(View.VISIBLE);
                    tvViewCount.setVisibility(View.INVISIBLE);
                    ivDotLive.setVisibility(View.INVISIBLE);

                    itemView.setOnClickListener(null);

                    itemView.setOnLongClickListener(v -> false);
                }

            } else {
                tvViewCount.setVisibility(View.GONE);
                ivDotLive.setVisibility(View.GONE);

                if (onClickItem != null) {
                    itemView.setOnClickListener(v -> onClickItem.onClickItem(entity));

                    itemView.setOnLongClickListener(v -> {
                        onClickItem.onLongClickItem(entity);

                        return false;
                    });
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface IOnClickItem {
        void onClickItem(GameInfoResponse game);

        default void onLongClickItem(GameInfoResponse game) {

        }
    }
}

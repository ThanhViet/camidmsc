package com.metfone.esport.ui.searchs.adapters;

import com.metfone.selfcare.R;

public class ObjectSuggestSearch {
    public int iconId;
    public String text;

    public ObjectSuggestSearch(int iconId, String text) {
        this.iconId = iconId;
        this.text = text;
    }

    public ObjectSuggestSearch(String text) {
        this(R.drawable.ic_search_gray_es, text);
    }
}

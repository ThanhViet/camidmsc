package com.metfone.esport.ui.createchannel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.UriUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.event.CreateChannelEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 9/16/20.
 */
public class CreateChannelFragment extends BaseFragment<CreateChannelPresenter> {
    @BindView(R.id.btn_back)
    AppCompatImageView btnBack;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.layout_action_bar)
    RelativeLayout layoutActionBar;
    @BindView(R.id.ivBanner)
    AppCompatImageView ivCover;
    @BindView(R.id.avatarView)
    AvatarView ivAvatar;
    @BindView(R.id.ivCameraBanner)
    AppCompatImageView ivCameraBanner;
    @BindView(R.id.ivCameraAvatar)
    AppCompatImageView ivCameraAvatar;
    @BindView(R.id.tvCountCharTitle)
    TextView tvCountCharTitle;
    @BindView(R.id.tvCountCharDescription)
    TextView tvCountCharDescription;
    @BindView(R.id.edContentAbout)
    EditText edContentAbout;
    @BindView(R.id.ivAbout)
    AppCompatImageView ivAbout;
    @BindView(R.id.lnContent)
    LinearLayout lnContent;
    @BindView(R.id.tvCreate)
    TextView tvCreate;
    @BindView(R.id.edTitle)
    EditText edTitle;
    @BindView(R.id.edDescription)
    EditText edDescription;
    @BindView(R.id.rlUploadImageAbout)
    RelativeLayout rlUploadImageAbout;
    @BindView(R.id.tvTitleChannel)
    TextView tvTitleChannel;
    @BindView(R.id.edAboutHeader)
    EditText edAboutHeader;
    @BindView(R.id.ivCancelImageAbout)
    AppCompatImageView ivCancelImageAbout;
    @BindView(R.id.rootView)
    RelativeLayout rootView;
    @BindView(R.id.tvCountAbout)
    TextView tvCountAbout;
    private Uri uriAvatar;
    private File fileAvatar;
    private File fileBanner;
    private File fileAbout;

    public static CreateChannelFragment newInstance() {
        CreateChannelFragment fragment = new CreateChannelFragment();
        return fragment;
    }

    @Override
    public CreateChannelPresenter getPresenter() {
        return new CreateChannelPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_create_channel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(getActivity(), R.color.black);
            com.metfone.selfcare.util.Utilities.adaptViewForInsertBottom(rootView);
            initListener();
            ivAvatar.setImageResource(R.drawable.circle_empty_avatar_es);
            ivCover.setBackgroundColor(getResources().getColor(R.color.color_banner_create_channel));
            tvTitleChannel.setText(Html.fromHtml(getString(R.string.channel_name)));
            ivCancelImageAbout.setVisibility(View.GONE);
            edContentAbout.setOnTouchListener((v, event) -> {
                if (edContentAbout.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            });
        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    private void initListener() {
        try {
            edTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharTitle != null)
                        tvCountCharTitle.setText(charSequence.length() + "/23");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            edAboutHeader.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (tvCountAbout != null)
                        tvCountAbout.setText(s.length() + "/100");
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            edDescription.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharDescription != null)
                        tvCountCharDescription.setText(charSequence.length() + "/200");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void onSuccessCreateChannel(ArrayList<ChannelInfoResponse> list) {
        try {
            showToast(R.string.msg_create_channel_success);
            AccountBusiness.getInstance().addChannel(list);
            AccountBusiness.getInstance().saveCurrentChannel(list.get(0));
            SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL,true);
            hideDialog();
            EventBus.getDefault().post(new CreateChannelEvent(uriAvatar));
            if (getActivity() != null) getActivity().finish();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void onFailCreateChannel(String error) {
        try {
            showToast(error);
            hideDialog();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }



    @OnClick({R.id.tvCreate, R.id.avatarView, R.id.ivBanner, R.id.rlUploadImageAbout, R.id.btn_back, R.id.ivCancelImageAbout})
    public void onViewClicked(View view) {
        try {
            switch (view.getId()) {
                case R.id.tvCreate:
                    if (validateCreateChannel()) {
                        presenter.createChannel(edTitle.getText().toString(),
                                edDescription.getText().toString(),
                                edAboutHeader.getText().toString(),
                                edContentAbout.getText().toString(),
                                fileAvatar,
                                fileBanner,
                                fileAbout
                        );
                    }
                    break;
                case R.id.avatarView:
                    Utilities.openGallery(getActivity(), Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_AVATAR);
                    break;
                case R.id.ivBanner:
                    Utilities.openGallery(getActivity(), Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_COVER);
                    break;
                case R.id.rlUploadImageAbout:
                    Utilities.openGallery(getActivity(), Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_ABOUT);
                    break;
                case R.id.btn_back:
                    if (getActivity() != null) getActivity().finish();
                    break;
                case R.id.ivCancelImageAbout:
                    fileAbout = null;
                    ivAbout.setImageResource(0);
                    ivCancelImageAbout.setVisibility(View.GONE);
                    break;

            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private boolean validateCreateChannel() {
        try {
            if (edTitle.getText().toString().trim().isEmpty()) {
                showToast(getString(R.string.you_have_not_entered_the_title_yet));
                return false;
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {

                switch (requestCode) {
                    case Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_AVATAR: {
                        try {
                            if (data != null && data.getExtras() != null) {
                                Object object = data.getExtras().get("data");
                                uriAvatar = data.getData();
                                if (object instanceof Bitmap) {
                                    File file = FileUtils.saveBitmapToFile((Bitmap) object);
                                    if (file != null) {
                                        if (ivAvatar != null)
                                            ivAvatar.setAvatarUri(Uri.fromFile(file));
                                        fileAvatar = file;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                    return;
                    case Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_COVER: {
                        try {
                            if (data != null && data.getExtras() != null) {
                                Object object = data.getExtras().get("data");
                                if (object instanceof Bitmap) {
                                    File file = FileUtils.saveBitmapToFile((Bitmap) object);
                                    if (file != null) {
                                        ImageLoader.setImageUri(ivCover, Uri.fromFile(file));
                                        fileBanner = file;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                    return;
                    case Constant.REQUEST_CODE.TAKE_PHOTO_CHANGE_ABOUT: {
                        try {
                            if (data != null && data.getExtras() != null) {
                                Object object = data.getExtras().get("data");
                                if (object instanceof Bitmap) {
                                    File file = FileUtils.saveBitmapToFile((Bitmap) object);
                                    if (file != null) {
                                        ivCancelImageAbout.setVisibility(View.VISIBLE);
                                        Glide.with(this).load(data.getData()).apply(new RequestOptions().transform(new RoundedCorners(getResources().getDimensionPixelOffset(R.dimen._5sdp)))).into(ivAbout);
                                        fileAbout = file;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                    return;
                    case Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_AVATAR: {
                        try {
                            if (data != null && data.getData() != null) {
                                uriAvatar = data.getData();
                                if (ivAvatar != null) ivAvatar.setAvatarUri(data.getData());
                                fileAvatar = UriUtils.uri2File(data.getData());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                    return;
                    case Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_COVER: {
                        try {
                            if (data != null && data.getData() != null) {
                                ImageLoader.setImageUri(ivCover, data.getData());
                                fileBanner = UriUtils.uri2File(data.getData());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                    return;
                    case Constant.REQUEST_CODE.OPEN_GALLERY_CHANGE_ABOUT: {
                        try {
                            if (data != null && data.getData() != null) {
                                ivCancelImageAbout.setVisibility(View.VISIBLE);
                                Glide.with(this).load(data.getData()).apply(new RequestOptions().transform(new RoundedCorners(getResources().getDimensionPixelOffset(R.dimen._5sdp)))).into(ivAbout);
//                            ImageLoader.setImageUri(ivAbout, data.getData());
                                fileAbout = UriUtils.uri2File(data.getData());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                    return;
                }
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

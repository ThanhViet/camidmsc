package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchChannelLiveItemViewBinder extends ItemViewBinder<ObjectSearchChannelLike, HalfVideoNoOptionViewHolder> {
    private HalfVideoNoOptionViewHolder.IOnClickItem clickListener;

    public SearchChannelLiveItemViewBinder(HalfVideoNoOptionViewHolder.IOnClickItem clickListener) {
        this.clickListener = clickListener;
    }

    @NotNull
    @Override
    public HalfVideoNoOptionViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HalfVideoNoOptionViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull HalfVideoNoOptionViewHolder viewBinderHolder, ObjectSearchChannelLike s) {
        viewBinderHolder.binDataByChannel(s.channelInfo, clickListener);
    }
}

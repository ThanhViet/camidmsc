package com.metfone.esport.ui.games.videoss.adapters;

import com.metfone.esport.entity.repsonse.VideoInfoResponse;

public class ObjectGameUploaded {
    public VideoInfoResponse videoInfoResponse;

    public ObjectGameUploaded(VideoInfoResponse videoInfoResponse) {
        this.videoInfoResponse = videoInfoResponse;
    }

    public ObjectGameUploaded() {
    }
}

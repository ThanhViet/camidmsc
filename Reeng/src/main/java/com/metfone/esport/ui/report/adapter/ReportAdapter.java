package com.metfone.esport.ui.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.entity.ReportEntity;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * Created by Nguyễn Thành Chung on 9/22/20.
 */
public class ReportAdapter extends BaseListAdapter<ReportEntity, ReportAdapter.ReportViewHolder> {

    private OnClickItem onClickItem;

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public ReportAdapter(Context context) {
        super(context);
    }

    @NotNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new ReportViewHolder(LayoutInflater.from(context).inflate(R.layout.item_report_video, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull ReportViewHolder holder, int position) {
        ReportEntity entity = mData.get(position);
        holder.binData(entity,position);
        holder.itemView.setOnClickListener(view -> {
            for (ReportEntity mDatum : mData) {
                mDatum.isSelect = false;
            }
            entity.isSelect = true;
            notifyDataSetChanged();
            if (onClickItem!=null){
                onClickItem.onClickItem(entity);
            }
        });
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    static class ReportViewHolder extends BaseViewHolder<ReportEntity> {
        @BindView(R.id.tvText)
        AppCompatTextView tvText;
        @BindView(R.id.ivCheck)
        AppCompatImageView ivCheck;
        public ReportViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void binData(ReportEntity entity, int position) {
            tvText.setText(entity.text);
            ivCheck.setVisibility(entity.isSelect ? View.VISIBLE : View.GONE);
        }
    }

    public interface OnClickItem{
        void onClickItem(ReportEntity entity);
    }
}

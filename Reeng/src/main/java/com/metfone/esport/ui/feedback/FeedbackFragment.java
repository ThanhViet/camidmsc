package com.metfone.esport.ui.feedback;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 9/3/20.
 */
public class FeedbackFragment extends BaseFragment<FeedbackPresenter> {
    static String VIDEO_ID = "VIDEO_ID";
    static String CATEGORY_ID = "CATEGORY_ID";
    static String CHANNEL_ID = "CHANNEL_ID";
    @BindView(R.id.edContent)
    EditText edContent;
    @BindView(R.id.rlSuccessFully)
    RelativeLayout rlSuccessFully;
    private long videoId;
    private long cateId;
    private long channelId;

    public static Bundle newBundle(long videoId, long categoryId, long channelId) {
        Bundle bundle = new Bundle();
        bundle.putLong(VIDEO_ID, videoId);
        bundle.putLong(CATEGORY_ID, categoryId);
        bundle.putLong(CATEGORY_ID, channelId);
        return bundle;
    }

    @Override
    public FeedbackPresenter getPresenter() {
        return new FeedbackPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_feedback;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDataBundle();
    }

    private void getDataBundle() {
        assert getArguments() != null;
        videoId = getArguments().getLong(VIDEO_ID);
        cateId = getArguments().getLong(CATEGORY_ID);
        channelId = getArguments().getLong(CHANNEL_ID);
    }

    public void onSuccessFeedback() {

        rlSuccessFully.setVisibility(View.VISIBLE);
        new Handler().postDelayed(() -> getActivity().onBackPressed(), 3000);

    }

    public void onFailFeedback(String error) {

    }

    @OnClick({R.id.btn_back, R.id.tvSendFeedback})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onClickBack();
                break;
            case R.id.tvSendFeedback:
                if (edContent.getText().toString().isEmpty()) {
                    Common.showToastError(getContext(), getString(R.string.msg_enter_feedback));
                } else {
                    Common.hideKeyBoard(getContext());
                    presenter.feedback(videoId, cateId, channelId, edContent.getText().toString());
                }
                break;
        }
    }
}

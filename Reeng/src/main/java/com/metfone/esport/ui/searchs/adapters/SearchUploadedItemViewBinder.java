package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchUploadedItemViewBinder extends ItemViewBinder<ObjectSearchUploaded, HalfVideoNoOptionViewHolder> {
    private HalfVideoNoOptionViewHolder.IOnClickItem IOnClickItem;

    public SearchUploadedItemViewBinder(HalfVideoNoOptionViewHolder.IOnClickItem IOnClickItem) {
        this.IOnClickItem = IOnClickItem;
    }

    @NotNull
    @Override
    public HalfVideoNoOptionViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HalfVideoNoOptionViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull HalfVideoNoOptionViewHolder viewBinderHolder, ObjectSearchUploaded s) {
        viewBinderHolder.binData(s.video, IOnClickItem, true);
    }
}

package com.metfone.esport.ui.live;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.esport.entity.ResolutionVideo;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Nguyễn Thành Chung on 9/2/20.
 */
public class PopupDisplayQuality extends BottomSheetDialogFragment {

    @BindView(R.id.rcvData)
    RecyclerView rcvData;
    private OnClickItem onClickItem;
    private DisplayQualityAdapter adapter;
    private List<ResolutionVideo> listResolution;
    private Unbinder unbinder;
    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.popup_display_quality_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        Log.e("PopupDisplayQuality","oncreate");
        Log.e("listResolution",listResolution.size() + "");
        rcvData.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvData.setHasFixedSize(true);
        adapter = new DisplayQualityAdapter(getContext());
        adapter.setNewData(listResolution);
        adapter.setOnClickItem(entity -> {
            if (onClickItem!=null) {
                onClickItem.onClickItem(entity);
            }
            dismiss();
        });
        rcvData.setAdapter(adapter);
        return view;
    }

    public void setData(List<ResolutionVideo> listResolution){
        Log.e("PopupDisplayQuality","setdata");
        if (listResolution.isEmpty()){
            this.listResolution = getDummyData();
        }else {
            this.listResolution = listResolution;
        }


    }

    private List<ResolutionVideo> getDummyData(){
        List<ResolutionVideo> resolutionVideos = new ArrayList<>();
        resolutionVideos.add(new ResolutionVideo("1080","1080P","http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4",200));
        resolutionVideos.add(new ResolutionVideo("720","720P","http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4",200));
        resolutionVideos.add(new ResolutionVideo("480","480P","http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4",200));
        resolutionVideos.add(new ResolutionVideo("360","360P","http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4",200));
        resolutionVideos.add(new ResolutionVideo("240","240P","http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4",200));
        return resolutionVideos;
    }



    public interface OnClickItem{
        void onClickItem(ResolutionVideo entity);
    }

}

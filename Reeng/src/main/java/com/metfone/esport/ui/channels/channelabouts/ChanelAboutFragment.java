package com.metfone.esport.ui.channels.channelabouts;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.EncodeUtils;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.esport.widgets.MultipleStatusView;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.ImageLoader;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class ChanelAboutFragment extends BaseFragment<ChanelAboutPresenter> {
    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.lnContent)
    LinearLayout lnContent;

    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;

    @BindView(R.id.tvDescription)
    AppCompatTextView tvDescription;

    @BindView(R.id.ivDescription)
    AppCompatImageView ivDescription;

    private ChannelInfoResponse currentChannel;

    public static ChanelAboutFragment newInstance() {
        Bundle args = new Bundle();
        ChanelAboutFragment fragment = new ChanelAboutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public ChanelAboutPresenter getPresenter() {
        return new ChanelAboutPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_chanel_about;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        multipleStatusView.showLoading();

        lnContent.setVisibility(View.INVISIBLE);

        setViewAbout();
    }

    public void onGetDataSuccess(ChannelInfoResponse response) {
        currentChannel = response;

        setViewAbout();
    }

    /**
     * Hiển thị about channel
     * Created_by @dchieu on 10/5/20
     */
    private void setViewAbout() {
        if (currentChannel == null || !isAdded()) return;

        lnContent.setVisibility(View.VISIBLE);

        multipleStatusView.showContent();

        tvTitle.setText(EncodeUtils.htmlDecode(currentChannel.getAboutTitle()));
        tvDescription.setText(EncodeUtils.htmlDecode(currentChannel.getAbout()));

        ImageLoader.setImage(
                ivDescription,
                currentChannel.imageAbout,
                R.drawable.bg_image_placeholder_es,
                R.drawable.bg_image_error_es,
                new RoundedCorners(getResources().getDimensionPixelSize(R.dimen._6sdp))
        );
    }

    public void onGetDataFailure(String message) {

    }
}

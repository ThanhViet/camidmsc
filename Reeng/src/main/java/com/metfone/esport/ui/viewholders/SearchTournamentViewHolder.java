package com.metfone.esport.ui.viewholders;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.NumberUtils;
import com.metfone.esport.common.Common;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.selfcare.R;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.metfone.esport.common.Constant.DateFormat.LONG_MONTH_DAY_YEAR;
import static com.metfone.esport.common.Constant.DateFormat.MONTH_DAY_YEAR_H_M_S_A;

public class SearchTournamentViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTournamentGame)
    AppCompatTextView tvTournamentGame;

    @BindView(R.id.tvTournamentBudget)
    AppCompatTextView tvTournamentBudget;

    @BindView(R.id.tvTournamentStartTime)
    AppCompatTextView tvTournamentStartTime;

    @BindView(R.id.avTournamentAvatar)
    AvatarView avTournamentAvatar;

    public SearchTournamentViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_game_tournament, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

//    private VideoLiveWithInfoHorizontalViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//        ButterKnife.bind(this, itemView);
//    }

    @SuppressLint("SetTextI18n")
    public void binData(TournamentInfoResponse entity, IOnClickItem onClickItem) {
        avTournamentAvatar.setAvatarUrl(entity.avatar);

        tvTournamentGame.setText(entity.name);
        tvTournamentBudget.setText("$" + NumberUtils.format((float) entity.price, true, 0));

        Date startTime = Common.convertStringToDate(entity.startDate, MONTH_DAY_YEAR_H_M_S_A);

        tvTournamentStartTime.setText(startTime != null ? Common.convertDateToString(startTime, LONG_MONTH_DAY_YEAR) : "");

        itemView.setOnClickListener(v -> {
            if (onClickItem != null)
                onClickItem.onClickItem(entity);
        });
    }

    public interface IOnClickItem {
        void onClickItem(TournamentInfoResponse entity);
    }
}

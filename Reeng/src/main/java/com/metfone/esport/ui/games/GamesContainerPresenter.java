package com.metfone.esport.ui.games;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.util.RxUtils;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class GamesContainerPresenter extends BasePresenter<GamesContainerFragment, BaseModel> {
    private Disposable disposable = null;

    public GamesContainerPresenter(GamesContainerFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(Long gameId) {
        if (disposable != null)
            disposable.dispose();

        disposable = RxUtils.async(
                compositeDisposable,
                apiService.getGameDetail(getDefaultParam(), gameId),
                new ICallBackResponse<GameInfoResponse>() {
                    @Override
                    public void onSuccess(GameInfoResponse response) {
                        if (view != null)
                            view.getDataSuccess(response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null)
                            view.getDataFail();
                    }
                }
        );
    }

    public void followGame(Long gameId, Integer isFollow) {
        RxUtils.async(
                compositeDisposable,
                apiService.followGame(gameId, isFollow, getDefaultParam()), //0 la unfollow
                new ICallBackResponse<String>() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFail(String error) {

                    }
                });
    }
}

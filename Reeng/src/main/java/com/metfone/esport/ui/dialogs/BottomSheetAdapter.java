package com.metfone.esport.ui.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.entity.ItemMenuEntity;
import com.metfone.esport.listener.BottomSheetListener;
import com.metfone.esport.listener.OnSingleClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class BottomSheetAdapter extends BaseListAdapter<ItemMenuEntity, BottomSheetAdapter.ItemMenuHolder> {
    private BottomSheetListener listener;

    public BottomSheetAdapter(Context context, ArrayList<ItemMenuEntity> listItem) {
        super(context);
        this.setData(listItem);
    }

    public void setListener(BottomSheetListener listener) {
        this.listener = listener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NotNull
    @Override
    public BottomSheetAdapter.ItemMenuHolder onCreateViewHolder(@NotNull ViewGroup parent, int type) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bottom_sheet, parent, false);
        ItemMenuHolder holder;
        holder = new ItemMenuHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ItemMenuHolder holder, int position) {
        ItemMenuEntity item = getItem(position);
        holder.binData(item, position);
        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) listener.onItemClick(item.getActionTag(), item.getItem());
            }
        });
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public static class ItemMenuHolder extends BaseViewHolder<ItemMenuEntity> {
        private TextView tvTitle;
        private AppCompatImageView ivIcon;

        public ItemMenuHolder(View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }

        @Override
        public void binData(ItemMenuEntity entity, int position) {
            tvTitle.setText(entity.getTitle());
            if (entity.getResIcon() != -1) {
                ivIcon.setVisibility(View.VISIBLE);
                ivIcon.setImageResource(entity.getResIcon());
            } else {
                ivIcon.setVisibility(View.GONE);
            }
        }
    }
}
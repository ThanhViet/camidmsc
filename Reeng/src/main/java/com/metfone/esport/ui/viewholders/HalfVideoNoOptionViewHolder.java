package com.metfone.esport.ui.viewholders;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HalfVideoNoOptionViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.ivThumbVideo)
    AppCompatImageView ivThumbVideo;

    @BindView(R.id.tvLive)
    AppCompatTextView tvLive;

    @BindView(R.id.tvViews)
    AppCompatTextView tvViews;

    @BindView(R.id.tvEyeViewCount)
    AppCompatTextView tvEyeViewCount;

    @BindView(R.id.tvTimeUploadAgo)
    AppCompatTextView tvTimeUploadAgo;

    @BindView(R.id.tvVideoTitle)
    AppCompatTextView tvVideoTitle;

    @BindView(R.id.tvVideoSubTitle)
    AppCompatTextView tvVideoSubTitle;

    @BindView(R.id.tvAccountName)
    AppCompatTextView tvAccountName;

    @BindView(R.id.avAvatar)
    AvatarView avAvatar;

    @BindView(R.id.videoThumb)
    View videoThumb;

    @BindView(R.id.layout_eye_viewer)
    View layout_eye_viewer;

    private Context context;

    public HalfVideoNoOptionViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_half_video_no_option, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        context = itemView.getContext();

        ButterKnife.bind(this, itemView);
    }

    public void binDataByChannel(ChannelInfoResponse channelInfo, IOnClickItem onClickItem) {
        binDataByChannel(channelInfo, onClickItem, false);
    }

    public void binDataByChannel(ChannelInfoResponse channelInfo, IOnClickItem onClickItem, boolean isShowDuration) {
        if (channelInfo == null) return;

        try {
            avAvatar.setAvatarUrl(channelInfo.urlAvatar);

            tvAccountName.setText(channelInfo.name);

            avAvatar.setOnClickListener(v -> {
                if (onClickItem != null)
                    onClickItem.onClickChannel(channelInfo);
            });

            tvAccountName.setOnClickListener(v -> {
                if (onClickItem != null)
                    onClickItem.onClickChannel(channelInfo);
            });
            int radius = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall);

            if (channelInfo.video != null) {
                VideoInfoResponse video = channelInfo.video;

                ImageLoader.setImage(
                        ivThumbVideo,
                        video.image_path_thumb,
                        R.drawable.bg_image_placeholder_es,
                        R.drawable.bg_image_error_es,
                        new MultiTransformation<Bitmap>(new CenterCrop(), new GranularRoundedCorners(radius, 0, 0, radius)));

                if (video.isLive()) {
                    tvTimeUploadAgo.setVisibility(View.INVISIBLE);

                    tvViews.setVisibility(View.INVISIBLE);

                    tvLive.setVisibility(View.VISIBLE);

                    if (video.ccu <= 0) {
                        layout_eye_viewer.setVisibility(View.INVISIBLE);
                    } else {
                        layout_eye_viewer.setVisibility(View.VISIBLE);
                        tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(video.ccu));
                    }
                } else {
                    tvLive.setVisibility(View.INVISIBLE);

                    if (video.type_video == 1) { // Video uploaded
                        layout_eye_viewer.setVisibility(View.INVISIBLE);

                        if (video.total_view <= 0) {
                            tvViews.setVisibility(View.INVISIBLE);
                        } else {
                            tvViews.setVisibility(View.VISIBLE);
                            tvViews.setText(itemView.getContext().getString(video.total_view > 1 ? R.string.template_views_count : R.string.template_view_count, ViewsUnitEnum.getTextDisplay(video.total_view)));
                        }

                        if (isShowDuration) {
                            if (video.durationS <= 0) {
                                tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                            } else {
                                tvTimeUploadAgo.setVisibility(View.VISIBLE);
                                tvTimeUploadAgo.setText(video.getDurationDisplay());
                            }
                        } else {
                            if (video.publish_time <= 0)
                                tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                            else {
                                tvTimeUploadAgo.setVisibility(View.VISIBLE);
                                tvTimeUploadAgo.setText(Utilities.calculateTime(context, video.publish_time));
                            }
                        }
                    } else { //Video pastbroadcast
                        tvViews.setVisibility(View.INVISIBLE);

                        if (video.total_view <= 0) {
                            layout_eye_viewer.setVisibility(View.INVISIBLE);
                        } else {
                            layout_eye_viewer.setVisibility(View.VISIBLE);
                            tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(video.total_view));
                        }

                        if (video.publish_time <= 0)
                            tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                        else {
                            tvTimeUploadAgo.setVisibility(View.VISIBLE);
                            tvTimeUploadAgo.setText(Utilities.calculateTime(context, video.publish_time));
                        }
                    }
                }

                tvVideoTitle.setText(channelInfo.video.title);

                if (channelInfo.video.gameInfo != null)
                    tvVideoSubTitle.setText(channelInfo.video.gameInfo.gameName);
                else
                    tvVideoSubTitle.setText("");

            } else {
                ImageLoader.setImage(
                        ivThumbVideo,
                        "",
                        R.drawable.bg_image_placeholder_es,
                        R.drawable.bg_image_error_es,
                        new MultiTransformation<Bitmap>(new CenterCrop(), new GranularRoundedCorners(radius, 0, 0, radius)));

                tvVideoTitle.setText("");
                tvEyeViewCount.setText("0");
            }

            videoThumb.setOnClickListener(v -> {
                if (onClickItem != null && channelInfo.video != null)
                    onClickItem.onClickVideo(channelInfo.video);
            });

            tvVideoTitle.setOnClickListener(v -> {
                if (onClickItem != null && channelInfo.video != null)
                    onClickItem.onClickVideo(channelInfo.video);
            });

//            ivOptionMore.setOnClickListener(view -> {
//                if (clickListener != null)
//                    clickListener.onClickOption();
//            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void binData(VideoInfoResponse video, IOnClickItem onClickItem) {
        binData(video, onClickItem, false);
    }

    public void binData(VideoInfoResponse video, IOnClickItem onClickItem, boolean isShowDuration) {
        if (video == null) return;

        try {
            int radius = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall);

            ImageLoader.setImage(
                    ivThumbVideo,
                    video.image_path_thumb,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    new MultiTransformation<Bitmap>(new CenterCrop(), new GranularRoundedCorners(radius, 0, 0, radius)));

            if (video.channelInfo != null) {
                avAvatar.setAvatarUrl(video.channelInfo.urlAvatar);

                tvAccountName.setText(video.channelInfo.name);
            } else {
                avAvatar.setImageResource(R.drawable.bg_avatar_default_es);

                tvAccountName.setText("");
            }

            if (video.isLive()) {
                tvTimeUploadAgo.setVisibility(View.INVISIBLE);

                tvViews.setVisibility(View.INVISIBLE);

                tvLive.setVisibility(View.VISIBLE);

                if (video.ccu <= 0) {
                    layout_eye_viewer.setVisibility(View.INVISIBLE);
                } else {
                    layout_eye_viewer.setVisibility(View.VISIBLE);
                    tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(video.ccu));
                }
            } else {
                tvLive.setVisibility(View.INVISIBLE);

                if (video.type_video == 1) { // Video uploaded
                    layout_eye_viewer.setVisibility(View.INVISIBLE);

                    if (video.total_view <= 0) {
                        tvViews.setVisibility(View.INVISIBLE);
                    } else {
                        tvViews.setVisibility(View.VISIBLE);
                        tvViews.setText(itemView.getContext().getString(video.total_view > 1 ? R.string.template_views_count : R.string.template_view_count, ViewsUnitEnum.getTextDisplay(video.total_view)));
                    }

                    if (isShowDuration) {
                        if (video.durationS <= 0) {
                            tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                        } else {
                            tvTimeUploadAgo.setVisibility(View.VISIBLE);
                            tvTimeUploadAgo.setText(video.getDurationDisplay());
                        }
                    } else {
                        if (video.publish_time <= 0)
                            tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                        else {
                            tvTimeUploadAgo.setVisibility(View.VISIBLE);
                            tvTimeUploadAgo.setText(Utilities.calculateTime(context, video.publish_time));
                        }
                    }
                } else { //Video pastbroadcast
                    tvViews.setVisibility(View.INVISIBLE);

                    if (video.total_view <= 0) {
                        layout_eye_viewer.setVisibility(View.INVISIBLE);
                    } else {
                        layout_eye_viewer.setVisibility(View.VISIBLE);
                        tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(video.total_view));
                    }

                    if (video.publish_time <= 0)
                        tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                    else {
                        tvTimeUploadAgo.setVisibility(View.VISIBLE);
                        tvTimeUploadAgo.setText(Utilities.calculateTime(context, video.publish_time));
                    }
                }
            }

            tvVideoTitle.setText(video.title);

            if (video.gameInfo != null) {
                tvVideoSubTitle.setText(video.gameInfo.gameName);
            } else {
                tvVideoSubTitle.setText("");
            }

            avAvatar.setOnClickListener(v -> {
                if (onClickItem != null && video.channelInfo != null)
                    onClickItem.onClickChannel(video.channelInfo);
            });

            tvAccountName.setOnClickListener(v -> {
                if (onClickItem != null && video.channelInfo != null)
                    onClickItem.onClickChannel(video.channelInfo);
            });

            videoThumb.setOnClickListener(view -> {
                if (onClickItem != null)
                    onClickItem.onClickVideo(video);
            });

            tvVideoTitle.setOnClickListener(view -> {
                if (onClickItem != null)
                    onClickItem.onClickVideo(video);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

//        ivOptionMore.setOnClickListener(view -> {
//            if (clickListener != null)
//                clickListener.onClickOption();
//        });
    }

    public interface IOnClickItem {
        void onClickVideo(VideoInfoResponse video);

        void onClickChannel(ChannelInfoResponse channel);
    }
}

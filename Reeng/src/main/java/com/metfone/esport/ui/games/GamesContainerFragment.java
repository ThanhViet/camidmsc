package com.metfone.esport.ui.games;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.EncodeUtils;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.google.android.material.tabs.TabLayout;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.base.ViewPagerBaseAdapter;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.event.OnFollowGameEvent;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.games.calendars.GameCalendarFragment;
import com.metfone.esport.ui.games.channels.GameChanelFragment;
import com.metfone.esport.ui.games.livestreams.GameLiveStreamFragment;
import com.metfone.esport.ui.games.videoss.GameVideoFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class GamesContainerFragment extends BaseFragment<GamesContainerPresenter> {

    private static String KEY_GAME_ID = "KEY_GAME_ID";
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.lnViewFollow)
    LinearLayout lnViewFollow;
    @BindView(R.id.tlGamesTab)
    TabLayout tlGamesTab;
    @BindView(R.id.ivGameThumb)
    AppCompatImageView ivGameThumb;
    @BindView(R.id.tvGameName)
    AppCompatTextView tvGameName;
    @BindView(R.id.tvViewers)
    AppCompatTextView tvViewers;
    @BindView(R.id.tvFollowers)
    AppCompatTextView tvFollowers;
    @BindView(R.id.tvGameDescription)
    AppCompatTextView tvGameDescription;
    @BindView(R.id.ivFavorite)
    AppCompatImageView ivFavorite;
    @BindView(R.id.vpGamePager)
    ViewPager vpGamePager;
    @BindView(R.id.layout_root)
    LinearLayout layout_root;
    @BindView(R.id.ivOnline)
    View ivOnline;
    private Long gameId;
    private GameInfoResponse currentGame;

    public static GamesContainerFragment newInstance(Long gameId) {
        GamesContainerFragment fragment = new GamesContainerFragment();

        fragment.setArguments(newBundle(gameId));

        return fragment;
    }

    public static Bundle newBundle(Long gameId) {

        Bundle args = new Bundle();

        args.putLong(KEY_GAME_ID, gameId);

        return args;
    }

    @Override
    public GamesContainerPresenter getPresenter() {
        return new GamesContainerPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_games;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        initViewPager();

        ivBack.setOnClickListener(v -> getActivity().onBackPressed());
        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
        Utilities.adaptViewForInsertBottom(layout_root);
        ivFavorite.setOnClickListener(v -> {
            if (currentGame == null) return;

            new SingleSelectBottomSheet()
                    .setOptions(CollectionUtils.newArrayList(new ExtKeyPair("",
                            getString(currentGame.isFollow == 1 ? R.string.unfollow : R.string.follow),
                            currentGame.isFollow == 1 ? R.drawable.ic_favorite_white_es : R.drawable.ic_favorite_red_es)))
                    .setOnClickItem(e -> {
                        if (!isLogin()) {
                            showDialogLogin();

                            return;
                        }

                        boolean isFollow = currentGame.isFollow == 1;
                        if (isFollow) {
                            currentGame.isFollow = 0;
                            currentGame.totalFollow -= 1;
                        } else {
                            currentGame.isFollow = 1;
                            currentGame.totalFollow += 1;
                        }
                        presenter.followGame(gameId, currentGame.isFollow);
                        setViewFavorite();

                        EventBus.getDefault().post(new OnFollowGameEvent(currentGame));
                    })
                    .show(getChildFragmentManager(), null);

        });

        presenter.getData(gameId);
    }

    /**
     * Lay du lieu gameId
     * Created_by @dchieu on 9/15/20
     */
    private void getDataBundle() {
        if (getArguments() != null) {
            gameId = getArguments().getLong(KEY_GAME_ID, 0);
        }
    }

    private void initViewPager() {
        List<BaseFragment<?>> fragments = new ArrayList<>();
        List<String> titles = new ArrayList<>();

        fragments.add(GameLiveStreamFragment.newInstance(gameId));
        titles.add(getString(R.string.menu_game_live_stream));

        fragments.add(GameVideoFragment.newInstance(gameId));
        titles.add(getString(R.string.menu_game_video));

        fragments.add(GameChanelFragment.newInstance(gameId));
        titles.add(getString(R.string.menu_game_channel));

        fragments.add(GameCalendarFragment.newInstance(gameId));
        titles.add(getString(R.string.menu_game_calendar));

        vpGamePager.setAdapter(new ViewPagerBaseAdapter(getChildFragmentManager(), fragments, titles));
        vpGamePager.setOffscreenPageLimit(fragments.size());
    }

    public void getDataFail() {

    }

    public void getDataSuccess(GameInfoResponse response) {
        currentGame = response;

        lnViewFollow.setVisibility(View.VISIBLE);
        ivFavorite.setVisibility(View.VISIBLE);

        try {
            ImageLoader.setImage(
                    ivGameThumb,
                    currentGame.gameImage,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    new MultiTransformation<>(new CenterCrop(), new RoundedCorners(getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall))));

            tvGameName.setText(currentGame.gameName);

            tvViewers.setText(getContext().getString(currentGame.totalView > 1 ? R.string.template_viewers_count : R.string.template_viewer_count, ViewsUnitEnum.getTextDisplay(response.totalView)));

            tvGameDescription.setText(currentGame.gameDesc);

            ivOnline.setVisibility(currentGame.isLive == 1 ? View.VISIBLE : View.GONE);

            setViewFavorite();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setViewFavorite() {
        if (currentGame == null)
            return;

        String textFollow;
        long totalFollows = currentGame.totalFollow;
        if (totalFollows > 1) {
            textFollow = getString(R.string.total_followers, ViewsUnitEnum.getTextDisplay(totalFollows));
        } else {
            textFollow = getString(R.string.total_follower, totalFollows + "");
        }
        if (tvFollowers != null) {
            tvFollowers.setText(EncodeUtils.htmlDecode(textFollow));

            tvFollowers.setVisibility(TextUtils.isEmpty(textFollow) ? View.GONE : View.VISIBLE);
        }

        ivFavorite.setImageResource(currentGame.isFollow == 0 ? R.drawable.ic_favorite_gray_es : R.drawable.ic_favorite_red_es);
    }
}

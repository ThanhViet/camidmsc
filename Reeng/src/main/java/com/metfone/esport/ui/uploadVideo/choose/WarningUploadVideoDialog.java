package com.metfone.esport.ui.uploadVideo.choose;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class WarningUploadVideoDialog extends BaseDialogFragment {
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.tv_msg)
    AppCompatTextView tvMessage;
    @BindView(R.id.btn_submit)
    AppCompatTextView btnSubmit;
    private String title, message, labelSubmit;

    public static WarningUploadVideoDialog newInstance(String title, String message, String labelSubmit) {
        WarningUploadVideoDialog dialog = new WarningUploadVideoDialog();
        dialog.title = title;
        dialog.message = message;
        dialog.labelSubmit = labelSubmit;
        return dialog;
    }

    public static WarningUploadVideoDialog newInstance(String message) {
        WarningUploadVideoDialog dialog = new WarningUploadVideoDialog();
        dialog.message = message;
        return dialog;
    }

    @Override
    protected void initView(View rootView) {
        tvTitle.setVisibility(title == null ? View.GONE : View.VISIBLE);
        tvTitle.setText(title == null ? "" : title);
        tvMessage.setText(message == null ? "" : message);
        btnSubmit.setText(labelSubmit == null ? "" : labelSubmit);
        btnSubmit.setOnClickListener(view -> dismissAllowingStateLoss());
    }

    @Override
    protected int getDialogWidth() {
        return ScreenUtils.getScreenWidth();
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_warning_upload_video;
    }

    @Override
    public String getTAG() {
        return DialogFragment.class.getSimpleName();
    }

}

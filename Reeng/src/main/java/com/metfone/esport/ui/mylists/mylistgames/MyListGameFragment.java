package com.metfone.esport.ui.mylists.mylistgames;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.entity.event.OnFollowGameEvent;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.games.GamesContainerFragment;
import com.metfone.esport.ui.mylists.mylistgames.adapters.MyListGameAdapter;
import com.metfone.esport.ui.mylists.mylistgames.adapters.MyListGameDecoration;
import com.metfone.esport.ui.viewholders.HomeGameViewHolder;
import com.metfone.selfcare.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class MyListGameFragment extends BaseFragment<MyListGamePresenter> {
    public static int SPAN_COUNT = 3;

    @BindView(R.id.rvMyListGame)
    RecyclerView rvMyListGame;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private MyListGameAdapter adapter;
    private int currentPage = 0;

    public static MyListGameFragment newInstance() {
        Bundle args = new Bundle();
        MyListGameFragment fragment = new MyListGameFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public MyListGamePresenter getPresenter() {
        return new MyListGamePresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_list_game;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        try {
            currentPage = 0;

            adapter.clear();

            getData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(adapter.getData())) multipleStatusView.showLoading();

        presenter.getData(currentPage++);
    }

    private void initRecyclerView() {
        adapter = new MyListGameAdapter(getContext(), null, new HomeGameViewHolder.IOnClickItem() {
            @Override
            public void onClickItem(GameInfoResponse game) {
                if (game == null) return;

                AloneFragmentActivity
                        .with(getActivity())
                        .parameters(GamesContainerFragment.newBundle(game.id))
                        .start(GamesContainerFragment.class);
            }

            @Override
            public void onLongClickItem(GameInfoResponse game) {
                new SingleSelectBottomSheet()
                        .setOptions(CollectionUtils.newArrayList(new ExtKeyPair("", getString(R.string.unfollow), R.drawable.ic_favorite_white_es)))
                        .setOnClickItem(e -> {
                            if (!isLogin()) {
                                showDialogLogin();

                                return;
                            }

                            presenter.unFollowGame(game.id);

                            adapter.delete(game);

                            checkEmptyView();

                        })
                        .show(getChildFragmentManager(), null);
            }
        });

        rvMyListGame.setLayoutManager(new GridLayoutManager(getContext(), SPAN_COUNT, RecyclerView.VERTICAL, false));
        rvMyListGame.setHasFixedSize(true);
        rvMyListGame.addItemDecoration(new MyListGameDecoration(getContext()));
        rvMyListGame.setAdapter(adapter);
        rvMyListGame.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(adapter.getData()) - 1) {
                        //bottom of list!
                        getData();
                    }
                }
            }
        });
    }

    public void onGetDataSuccess(ArrayList<GameInfoResponse> response) {
        if (CollectionUtils.isNotEmpty(response)) {
            adapter.addAll(response);
        }

        checkEmptyView();
    }

    private void checkEmptyView() {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvMyListGame != null) rvMyListGame.setVisibility(View.INVISIBLE);
        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvMyListGame != null) rvMyListGame.setVisibility(View.VISIBLE);
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvMyListGame != null) rvMyListGame.setVisibility(View.INVISIBLE);
        }
    }

    @Subscribe
    public void onEvent(OnFollowGameEvent event) {
        if (event == null || event.game == null) return;

        if (event.game.isFollow == 0) {
            GameInfoResponse removeGame = CollectionUtils.find(adapter.getData(), item ->
                    item != null && item.id.equals(event.game.id)
            );

            if (removeGame != null) {
                adapter.delete(removeGame);

                checkEmptyView();
            }
        } else {
            new Handler().postDelayed(this::refreshData, 1000);
        }
    }
}

package com.metfone.esport.ui.mylists.mylistchanels.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

public class MyListChanelDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public MyListChanelDecoration(Context context) {
        space = context != null ? context.getResources().getDimensionPixelSize(R.dimen.pading_content) : 0;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (parent.getChildAdapterPosition(view) != 0) {
            outRect.top = space;
        }
    }
}

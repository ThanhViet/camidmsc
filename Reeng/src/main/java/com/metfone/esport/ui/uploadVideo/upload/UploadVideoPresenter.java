package com.metfone.esport.ui.uploadVideo.upload;

import android.net.Uri;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.StringUtils;
import com.iknow.trimvideo.view.trim.VideoTrimmerUtil;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.CategoryUploadModel;
import com.metfone.esport.entity.ThumbVideoUpload;
import com.metfone.esport.entity.UploadVideoEntity;
import com.metfone.esport.entity.repsonse.GameInfoEntity;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.TransferFileBusiness;
import com.metfone.esport.util.RxUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import iknow.android.utils.thread.UiThreadExecutor;
import io.reactivex.disposables.CompositeDisposable;

class UploadVideoPresenter extends BasePresenter<UploadVideoActivity, BaseModel> {
    private TransferFileBusiness transferFileBusiness;

    public UploadVideoPresenter(UploadVideoActivity view, CompositeDisposable disposable) {
        super(view, disposable);
        transferFileBusiness = new TransferFileBusiness();
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void loadCategory(long channelId) {
//        channelId = 369;
        RxUtils.async(compositeDisposable, apiService.getGameOfChannel(channelId, AccountBusiness.getInstance().getPhoneNumber(), getDefaultParam()), new ICallBackResponse<ArrayList<GameInfoEntity>>() {
            @Override
            public void onSuccess(ArrayList<GameInfoEntity> response) {
                if (view != null) view.updateCategories(response);
            }

            @Override
            public void onFail(String error) {

            }
        });
    }

    public void loadThumbnail(final Uri videoUri, final int totalThumbsCount, final long startPosition,
                              final long endPosition) {
        VideoTrimmerUtil.shootVideoThumbInBackground(view, videoUri, totalThumbsCount, startPosition, endPosition, (bitmap, integer) -> {
            if (bitmap != null) {
                UiThreadExecutor.runTask("", () -> {
                    if (view != null) {
                        view.loadThumbSuccess(new ThumbVideoUpload(ThumbVideoUpload.TYPE_BITMAP, bitmap));
                    }
                }, 0L);
            }
        });
    }

    public void uploadVideo(String filePath, String title, String description, CategoryUploadModel category, Date datePublished, ThumbVideoUpload thumb) {
        UploadVideoEntity data = new UploadVideoEntity();
        data.pathVideo = filePath;
        data.title = title;
        data.description = description;
        if (AccountBusiness.getInstance().getChannelInfo() != null)
            data.channelId = AccountBusiness.getInstance().getChannelInfo().id;
        if (category != null) data.categoryId = category.getId();
        if (datePublished != null) data.publishTime = datePublished.getTime();
        data.listPathThumb = new ArrayList<>();
        if (thumb != null) {
            if (thumb.getBitmap() != null) {
                File file = FileUtils.saveBitmapToFile(thumb.getBitmap());
                if (file != null) data.listPathThumb.add(file.getPath());
            } else if (StringUtils.isNotEmpty(thumb.getFilePath())) {
                data.listPathThumb.add(thumb.getFilePath());
            }
        }
        transferFileBusiness.uploadVideo(data);

        if (view != null) {
            view.uploadVideo(data);
        }
    }
}

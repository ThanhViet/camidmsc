package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.TextWithIconViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchSuggestItemViewBinder extends ItemViewBinder<ObjectSuggestSearch, TextWithIconViewHolder> {
    TextWithIconViewHolder.ClickListener clickListener;

    public SearchSuggestItemViewBinder(TextWithIconViewHolder.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NotNull
    @Override
    public TextWithIconViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new TextWithIconViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull TextWithIconViewHolder viewBinderHolder, ObjectSuggestSearch s) {
        viewBinderHolder.binData(s, clickListener);
    }
}

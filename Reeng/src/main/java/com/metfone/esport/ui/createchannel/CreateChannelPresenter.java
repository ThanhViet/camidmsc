package com.metfone.esport.ui.createchannel;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.CreateChannelResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.network.donate.DonateESportApiCallback;
import com.metfone.esport.network.donate.DonateESportClient;
import com.metfone.esport.network.donate.request.SubUpdateChannelInformationRequest;
import com.metfone.esport.network.donate.response.WsUpdateChannelInformationResponse;
import com.metfone.esport.service.FileServiceRetrofit;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.model.account.UserInfo;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * Created by Nguyễn Thành Chung on 9/21/20.
 */
public class CreateChannelPresenter extends BasePresenter<CreateChannelFragment, BaseModel> {
    private MultipartBody.Part firstBody = null;
    private MultipartBody.Part secondBody = null;

    public CreateChannelPresenter(CreateChannelFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void createChannel(String channelName, String desc, String aboutTitle, String about, File avatar, File banner, File aboutImage) {
        MultipartBody.Part bodyAvatar = null;
        MultipartBody.Part bodyBanner = null;
        MultipartBody.Part bodyAbout = null;
        MediaType imageType = MediaType.parse("image/jpeg");
        RequestBody requestFile;
        ReengAccount mAccount = ApplicationController.self().getReengAccountBusiness().getCurrentAccount();
        if (avatar != null) {
            requestFile = RequestBody.Companion.create(avatar, imageType);
            bodyAvatar = MultipartBody.Part.createFormData("avatar", avatar.getName(), requestFile);
        }
        if (banner != null) {
            requestFile = RequestBody.Companion.create(banner, imageType);
            bodyBanner = MultipartBody.Part.createFormData("banner", banner.getName(), requestFile);
        }
        if (aboutImage != null) {
            requestFile = RequestBody.Companion.create(aboutImage, imageType);
            bodyAbout = MultipartBody.Part.createFormData("aboutImage", aboutImage.getName(), requestFile);
        }
        RxUtils.asyncCreateChannel(compositeDisposable, getApiService(mAccount.getNumberJid(), channelName, desc, aboutTitle,
                about,
                bodyAvatar,
                bodyBanner,
                bodyAbout)
        , new ICallBackResponse<CreateChannelResponse>() {
            @Override
            public void onStart() {
                if (view != null) {
                    view.showDialog();
                }
            }

            @Override
            public void onSuccess(CreateChannelResponse response) {
                if (view != null) view.onSuccessCreateChannel(response.result);
                updateChannel(response.result.get(0));
            }

            @Override
            public void onFail(String error) {
                if (view != null) view.onFailCreateChannel(error);
            }
        });
    }

    private String getMsisdn() {
        if (!StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getJidNumber())) {
            return ApplicationController.self().getReengAccountBusiness().getJidNumber();
        }
        return "";
    }
    public void updateChannel(ChannelInfoResponse channel) {
        String camId = "";
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(view.getParentActivity());
        UserInfo currentUser = userInfoBusiness.getUser();
        camId = currentUser.getUser_id() + "";

        SubUpdateChannelInformationRequest param = new SubUpdateChannelInformationRequest();
        param.setId(channel.id+"");
        param.setChannelName(channel.name);
        param.setCamId(camId);
        param.setMsisdn(getMsisdn());
        param.setComment("");
        param.setCommentKh("");
        param.setEmoneyAccount("");
        param.setStatus("1");
        param.setImageUrl(channel.urlAvatar);
        param.setBannerUrl(channel.urlImages);
        param.setCreatedDate(channel.createdDate);

        new DonateESportClient().wsUpdateChannelInformation(param, new DonateESportApiCallback<WsUpdateChannelInformationResponse>() {
            @Override
            public void onResponse(Response<WsUpdateChannelInformationResponse> response) {
                if (response.body() != null && response.body().isSuccessful()) {
                    SPUtils.getInstance().put("update_channel_"+channel.id, true);
                } else
                    SPUtils.getInstance().put("update_channel_"+channel.id, false);
            }

            @Override
            public void onError(Throwable error) {
                SPUtils.getInstance().put("update_channel_"+channel.id, false);
            }
        });
    }

    private Observable<CreateChannelResponse> getApiService(String phone, String channelName, String desc, String aboutTitle, String about, MultipartBody.Part bodyAvatar, MultipartBody.Part bodyBanner, MultipartBody.Part bodyAbout) {
        if (bodyAvatar == null && bodyBanner == null && bodyAbout == null) {
            return FileServiceRetrofit.getInstance().createChannelEmptyImage(
                    phone,
                    channelName,
                    desc,
                    aboutTitle,
                    about,
                    getDefaultParam()
            );
        } else if (bodyAvatar != null && bodyBanner != null && bodyAbout != null) {
            return FileServiceRetrofit.getInstance().createChannelFull(
                    phone,
                    channelName,
                    desc,
                    aboutTitle,
                    about,
                    bodyAvatar,
                    bodyBanner,
                    bodyAbout,
                    getDefaultParam()
            );
        } else {
            if (getCount(bodyAvatar, bodyBanner, bodyAbout) > 1) {
                conditionSend2Image(bodyAvatar, bodyBanner, bodyAbout);
                return FileServiceRetrofit.getInstance().createChannel2Image(
                        phone,
                        channelName,
                        desc,
                        aboutTitle,
                        about,
                        firstBody,
                        secondBody,
                        getDefaultParam()
                );
            } else {
                conditionSendAloneImage(bodyAvatar, bodyBanner, bodyAbout);
                return FileServiceRetrofit.getInstance().createChannelAlone(
                        phone,
                        channelName,
                        desc,
                        aboutTitle,
                        about,
                        firstBody,
                        getDefaultParam()
                );
            }
        }
    }

    private void conditionSend2Image(MultipartBody.Part bodyAvatar, MultipartBody.Part bodyBanner, MultipartBody.Part bodyAbout) {
        try {
            if (bodyAvatar != null && bodyBanner != null) {
                firstBody = bodyAvatar;
                secondBody = bodyBanner;
            } else if (bodyAvatar != null && bodyAbout != null) {
                firstBody = bodyAvatar;
                secondBody = bodyAbout;
            } else {
                firstBody = bodyBanner;
                secondBody = bodyAbout;
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private void conditionSendAloneImage(MultipartBody.Part bodyAvatar, MultipartBody.Part bodyBanner, MultipartBody.Part bodyAbout) {
        try {
            if (bodyAvatar != null) {
                firstBody = bodyAvatar;
            } else if (bodyBanner != null) {
                firstBody = bodyBanner;
            } else {
                firstBody = bodyAbout;
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private int getCount(MultipartBody.Part avatar, MultipartBody.Part banner, MultipartBody.Part about) {
        int count = 0;
        if (avatar != null) {
            count++;
        }
        if (banner != null) {
            count++;
        }
        if (about != null) {
            count++;
        }
        return count;
    }

}

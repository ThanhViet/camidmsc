package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyListChanelViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.avAvatar)
    AvatarView avAvatar;

    @BindView(R.id.ivOnline)
    AppCompatImageView ivOnline;

    @BindView(R.id.tvChanelName)
    AppCompatTextView tvChanelName;

    @BindView(R.id.tvChanelViewers)
    AppCompatTextView tvChanelViewers;

    @BindView(R.id.ivOptionMore)
    AppCompatImageView ivOptionMore;

//        public HomeMetfoneViewHolder(@NonNull View itemView) {
//
//            super(itemView);
//        }

    public MyListChanelViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_my_list_chanel, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

    public void binDataByFollowers(ChannelInfoResponse entity, IOnClickItem callBack) {
        if (entity == null) return;

        bindData(entity, callBack);

        tvChanelViewers.setText(itemView.getContext().getString(entity.numFollow > 1 ? R.string.template_followers_count : R.string.template_follower_count, ViewsUnitEnum.getTextDisplay(entity.numFollow)));
    }

    public void binDataByViewers(ChannelInfoResponse entity, IOnClickItem callBack) {
        if (entity == null) return;

        bindData(entity, callBack);

        tvChanelViewers.setText(itemView.getContext().getString(entity.totalView > 1 ? R.string.template_viewers_count : R.string.template_viewer_count, ViewsUnitEnum.getTextDisplay(entity.totalView)));
    }

    /**
     * bindata
     * Created_by @dchieu on 9/22/20
     */
    private void bindData(ChannelInfoResponse entity, IOnClickItem callBack) {
        if (entity == null) return;

        try {
            avAvatar.setAvatarUrl(entity.urlAvatar);

            tvChanelName.setText(entity.name);

            ivOnline.setVisibility(entity.isLive == 1 ? View.VISIBLE : View.INVISIBLE);

            ivOptionMore.setOnClickListener(v -> {
                if (callBack != null)
                    callBack.onClickOption(entity);
            });

            itemView.setOnClickListener(v -> {
                if (callBack != null)
                    callBack.onClickItem(entity);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface IOnClickItem {
        void onClickItem(ChannelInfoResponse entity);

        void onClickOption(ChannelInfoResponse entity);
    }
}

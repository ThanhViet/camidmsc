package com.metfone.esport.ui.live;

import static android.view.View.GONE;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.SimpleOrientationListener;
import com.metfone.esport.customview.player.JZDataSource;
import com.metfone.esport.customview.player.JZUtils;
import com.metfone.esport.customview.player.Jzvd;
import com.metfone.esport.customview.player.JzvdStd;
import com.metfone.esport.customview.player.JzvdStdTinyWindow;
import com.metfone.esport.entity.ResolutionVideo;
import com.metfone.esport.entity.camid.CamIdUserResponse;
import com.metfone.esport.entity.event.ChangeVideoEvent;
import com.metfone.esport.entity.event.OnBackDeviceEvent;
import com.metfone.esport.entity.event.OnClickCommentUploadEvent;
import com.metfone.esport.entity.event.OnLikeVideoEvent;
import com.metfone.esport.entity.event.OnSendCommentEvent;
import com.metfone.esport.entity.event.OnSendCommentLiveEvent;
import com.metfone.esport.entity.event.OnWindowFocusEvent;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.entity.repsonse.UserInfoEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.ui.feedback.FeedbackFragment;
import com.metfone.esport.ui.report.ReportVideoFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import butterknife.BindView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import iknow.android.utils.KeyboardUtil;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.common.Common.handleException;
import static com.metfone.esport.customview.player.Jzvd.STATE_AUTO_COMPLETE;
import static com.metfone.esport.customview.player.Jzvd.STATE_ERROR;
import static com.metfone.esport.customview.player.Jzvd.STATE_PAUSE;
import static com.metfone.esport.ui.live.LiveVideoFragment.PRE_CODE_THANKMESSAGE;
import static com.metfone.esport.ui.live.LiveVideoFragment.TIME_REMAIN_SHOW;
import static com.metfone.esport.ui.live.LiveVideoFragment.TIME_SHOW_THANKMESSAGE;
import static com.metfone.esport.ui.live.LiveVideoFragment.isClickDonateHorizontal;

/**
 * Created by Nguyễn Thành Chung on 10/11/20.
 */
public class TopFragment extends BaseFragment<TopPresenter> {
    @BindView(R.id.video)
    JzvdStdTinyWindow mJzvdStd;
    private List<ResolutionVideo> resolutionVideos = new ArrayList<>();
    private VideoInfoResponse modelData;
    private InfoPlayerBottomSheet dialogInfoPlayer;
    private CallBackFragment callBackFragment;
    private boolean isMini = false;
    private String rowStart = "";
    private String idUser = "";
    private long currentPosition = 31000;
    private boolean isHorizontal = false;
    private boolean isClickFullScreen = false;
    private SimpleOrientationListener mOrientationListener;
    private int str = 0;
    private LinearLayout donateOverlay;
    private TextView tvDonateComment, tvDesDonate;

    public static TopFragment newInstance(VideoInfoResponse modelData, CallBackFragment callBackFragment) {
        TopFragment fragment = new TopFragment();
        fragment.modelData = modelData;
        fragment.callBackFragment = callBackFragment;
        return fragment;
    }

    @Override
    public TopPresenter getPresenter() {
        return new TopPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_top;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        try {
            mJzvdStd.setActivity(getParentActivity());
            getParentActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_USER);

        } catch (Exception e) {
            Common.handleException(e);
        }

        donateOverlay = view.findViewById(R.id.ln_donate_overlay_not_login);
        tvDesDonate = view.findViewById(R.id.tvDesDonateNotLogin);
        tvDonateComment = view.findViewById(R.id.tvDonateCommentNotLogin);
        donateOverlay.setVisibility(GONE);

        try {
            mJzvdStd.setControllerListener(new Jzvd.VideoPlayerControllerListener() {
                @Override
                public void onClickBack() {
                    try {
                        if (callBackFragment != null)
                            callBackFragment.callBackTinyScreen();
                        if (mJzvdStd != null) mJzvdStd.setScreenTiny();
                        isMini = true;
                    } catch (Exception e) {
                        handleException(e);
                    }

                }

                @Override
                public void onClickFullScreen() {
                    try {
                        isClickFullScreen = true;
                        isHorizontal = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isClickFullScreen = false;
                                isHorizontal = false;

                            }
                        }, 2000);
                    } catch (Exception e) {
                        handleException(e);
                    }
//                    mOrientationListener.disable();

//                    Utilities.setNavigationBarTransparent(getParentActivity());

                }

                @Override
                public void onClickExitFullScreen() {
                    try {
                        new Handler().postDelayed(() -> {
                            Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
                            Common.setNavigationBarBlack(getParentActivity());
                        }, 100);
                        if (isClickDonateHorizontal) {
                            KeyboardUtils.hideSoftInput(getParentActivity());
                        }

                        if (callBackFragment != null) {
                            callBackFragment.onClickExitFullScreen();
                        }
                    } catch (Exception e) {
                        handleException(e);
                    }
//                    getParentActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_USER);
                }

                @Override
                public void onClickOption() {
                    try {
                        if (getParentActivity() == null || getParentActivity().isFinishing() || !isAdded())
                            return;
                        if (isLogin()) {
                            SettingPlayerBottomSheet settingPlayerBottomSheet = new SettingPlayerBottomSheet();
                            settingPlayerBottomSheet.setData(resolutionVideos);
                            settingPlayerBottomSheet.setOnClickSetting(new SettingPlayerBottomSheet.OnClickSetting() {
                                @Override
                                public void onClickFeedback() {
                                    long videoId = 0;
                                    long channelId = 0;
                                    long categoryId = 0;
                                    if (modelData != null) {
                                        videoId = modelData.id;
                                        if (modelData.channelInfo != null) {
                                            channelId = modelData.channelInfo.id;
                                            categoryId = modelData.channelInfo.categoryId;
                                        }
                                    }
                                    mJzvdStd.gotoNormalScreen();
                                    AloneFragmentActivity.with(getParentActivity()).parameters(FeedbackFragment.newBundle(videoId, categoryId, channelId)).start(FeedbackFragment.class);
                                }

                                @Override
                                public void onClickReport() {
                                    mJzvdStd.gotoNormalScreen();
                                    AloneFragmentActivity.with(getParentActivity()).parameters(ReportVideoFragment.newBundle(GsonUtils.toJson(modelData))).start(ReportVideoFragment.class);
                                }

                                @Override
                                public void onClickQuality(ResolutionVideo resolutionVideo) {
                                    if (mJzvdStd != null) {
                                        long seekToInAdvance = mJzvdStd.seekToInAdvance;
                                        mJzvdStd.changeUrl(new JZDataSource(resolutionVideo.videoPath), seekToInAdvance);
                                    }
                                }

                                @Override
                                public void onCancel() {
                                    JZUtils.showSystemUI(getContext());
                                    Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
                                }
                            });
                            settingPlayerBottomSheet.show(getChildFragmentManager(), settingPlayerBottomSheet.getTag());
                        } else {
                            showDialogLogin();
                        }
                    } catch (Exception e) {
                        handleException(e);
                    }

                }

                @Override
                public void showDialogInfoPlayer() {
                    try {
                        if (getParentActivity() == null || getParentActivity().isFinishing() || !isAdded())
                            return;
                        if (dialogInfoPlayer != null)
                            dialogInfoPlayer.dismissAllowingStateLoss();
                        if (modelData != null) {
                            dialogInfoPlayer = InfoPlayerBottomSheet.newInstance(modelData, modelData -> {
                                processLikeVideo(modelData);
                            });
                            dialogInfoPlayer.show(getChildFragmentManager(), dialogInfoPlayer.getTag());
                            new Handler().postDelayed(() -> {
                                if (dialogInfoPlayer != null)
                                    dialogInfoPlayer.dismissAllowingStateLoss();
                            }, 500);
                        }
                    } catch (Exception e) {
                        handleException(e);
                    }

                }

                @Override
                public void onExpandScreen() {
                    if (callBackFragment != null) callBackFragment.callBackScreenExpand();
                }

                @Override
                public void onCloseScreenTiny() {
                    try {
                        if (mJzvdStd != null) {
                            mJzvdStd.pause();
                            mJzvdStd.reset();
                        }
                        logTime();
                        if (callBackFragment != null) callBackFragment.closeTinyScreen();
                        ApplicationController.getApp().isPlayingMini = false;
                    } catch (Exception e) {
                        handleException(e);
                    }

                }

                @Override
                public void onSendComment(EmojiconEditText editText) {
                    try {
                        if (editText == null || StringUtils.isEmpty(editText.getText())) {
                            ToastUtils.showShort(getString(R.string.please_enter_your_comment));
                        } else {
//                        KeyboardUtil.hideKeyboard(getParentActivity(), editText);
                            if (isLogin()) {
                                processSendComment(editText.getText().toString());
                                editText.setText("");
                            } else {
                                showDialogLogin();
                            }
                        }
                    } catch (Exception e) {
                        handleException(e);
                    }

                }

                @Override
                public void onClickShare() {
                    try {
                        if (mJzvdStd != null) {
                            new Handler().postDelayed(() -> {
                                if (modelData != null)
                                    TopFragment.this.onClickShare(modelData.link);
                            }, 500);
                        }
                    } catch (Exception e) {
                        handleException(e);
                    }
                }

                @Override
                public void onGetComment() {
                    getData();
                }

                @Override
                public void onClickCommentUpload() {
                    Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
                    EventBus.getDefault().post(new OnClickCommentUploadEvent());
                }

                @Override
                public void onLogin() {
                    showDialogLoginOutSite();
                }

            });
            listenerRotateScreen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialogLoginOutSite() {
        if (getParentActivity() == null) return;
        DialogMessage dialogMessage = new DialogMessage(getParentActivity(), true);
        dialogMessage.setMessage(getResources().getString(R.string.content_popup_login));
        dialogMessage.setDismissListener(new DismissListener() {
            @Override
            public void onDismiss() {
                JZUtils.hideSystemUI(getContext());
            }
        });
        dialogMessage.setNegativeListener(result -> NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true));
        dialogMessage.show();
    }


    @Subscribe
    public void OnWindowFocusEvent(OnWindowFocusEvent event) {
        try {
//            listenerRotateScreen();

            Log.e("focuswindow", event.isHasFocus() + "");
            if (str == 1) {
                if (!isClickFullScreen) {
                    mOrientationListener.enable();
                }
                // rotation is Unlocked
            } else {
                // rotation is Locked
                mOrientationListener.disable();
            }

        } catch (Exception e) {
            handleException(e);
        }
    }

    public void lockRotation(){
        if (mOrientationListener != null) {
            mOrientationListener.disable();
            mOrientationListener = null;
        }
    }
    public void listenerRotateScreen() {
        try {
            mOrientationListener = new SimpleOrientationListener(
                    getContext()) {

                @Override
                public void onSimpleOrientationChanged(int orientation) {
                    if (mJzvdStd != null && !isMini) {
                        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            isHorizontal = true;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (isHorizontal && !isMini && !isClickFullScreen) {
                                        mJzvdStd.clickFullscreen();
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                isHorizontal = false;
                                            }
                                        }, 1000);

                                    }
                                }
                            }, 1000);

                        } else if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                            new Handler().postDelayed(() -> {
                                if (!isHorizontal) {
                                    if (mJzvdStd != null) mJzvdStd.gotoNormalScreen();
                                    if (callBackFragment != null)
                                        callBackFragment.onClickExitFullScreen();
                                    Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
                                    if (modelData.isLive()) {
                                        KeyboardUtils.hideSoftInput(getParentActivity());
                                    }
                                }

                            }, 1000);

                        }
                    }
                }
            };
            str = Settings.System.getInt(getParentActivity().getContentResolver(),
                    Settings.System.ACCELEROMETER_ROTATION);

            if (str == 1) {
                // rotation is Unlocked
                mOrientationListener.enable();
            } else {
                // rotation is Locked
                mOrientationListener.disable();
            }

        } catch (Exception e) {
            handleException(e);
        }
    }

    public void initVideo(DetailVideoResponse response) {
        try {
            if (response != null) {
                if (!response.isLiveVideo()) {
                    getData();
                }
            }
            modelData.original_path = response.originalPath;
            initVideoPlayer(modelData);

        } catch (Exception e) {
            Common.handleException(e);
        }
    }


    @Override
    public void onDestroy() {
        logTime();
        super.onDestroy();

        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mJzvdStd != null) {
            if (mJzvdStd.isHorizontal) {
                JZUtils.hideSystemUI(getContext());
            } else {
                new Handler().postDelayed(() -> {
                    Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
                    Common.setNavigationBarBlack(getParentActivity());
                }, 100);
            }
        }
    }

    @Override
    public void onDestroyView() {
        logTime();
        //Jzvd.releaseAllVideos();
        if (mJzvdStd != null) {
            mJzvdStd.stopHandlerMess = true;
            mJzvdStd.reset();
        }
        super.onDestroyView();

    }

    public void setExpandVideo() {
        try {
            isMini = false;
            mJzvdStd.setType(modelData.isLive() ? JzvdStd.STYLE_LIVE : JzvdStd.STYLE_NORMAL);
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void setQualityList(List<ResolutionVideo> qualityList) {
        try {
            this.resolutionVideos = qualityList;
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void setCollapseVideo() {
        try {
            mJzvdStd.setScreenTiny();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private void initVideoPlayer(VideoInfoResponse response) {
        try {
            if (mJzvdStd == null) return;
            mJzvdStd.pause();
            mJzvdStd.reset();
            if (response == null) return;
            mJzvdStd.setUp(modelData.original_path, response.title, JzvdStd.SCREEN_NORMAL);
            mJzvdStd.setType(response.isLive() ? JzvdStd.STYLE_LIVE : JzvdStd.STYLE_NORMAL);
            modelData.type_video = response.type_video;
            mJzvdStd.setData(response);
            mJzvdStd.setViewLive(Common.getViewLive(Math.max(response.ccu, 1)));
            mJzvdStd.startVideo();

        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    private void getData() {
        try {
            presenter.getComments(String.valueOf(modelData.link), rowStart);
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void processLikeVideo(VideoInfoResponse modelData) {
        try {
            if (isLogin()) {
                presenter.likeVideo(modelData.is_like, modelData);

                EventBus.getDefault().post(new OnLikeVideoEvent(modelData));
            } else {
                showDialogLogin();
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private void processSendComment(String comment) {
        try {
            if (modelData.isLive()) {
                presenter.sendComment(comment.trim(), modelData);
                EventBus.getDefault().post(new OnSendCommentLiveEvent(comment.trim()));
            } else {
                presenter.sendCommentPastBroadcast(comment.trim(), modelData);
                CamIdUserResponse camIdUserResponse = AccountBusiness.getInstance().getUserInfo();
                UserInfoEntity userInfoEntity = new UserInfoEntity();
                if (camIdUserResponse == null) {
                    userInfoEntity.avatar = "";
                    userInfoEntity.name = "";
                } else {
                    if (AccountBusiness.getInstance().getChannelInfo() == null) {
                        userInfoEntity.avatar = camIdUserResponse.avatar == null ? "" : camIdUserResponse.avatar;
                        userInfoEntity.name = camIdUserResponse.full_name == null ? "" : camIdUserResponse.full_name;
                    } else {
                        if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                            userInfoEntity.avatar = camIdUserResponse.avatar == null ? "" : camIdUserResponse.avatar;
                            userInfoEntity.name = camIdUserResponse.full_name == null ? "" : camIdUserResponse.full_name;
                        } else {
                            userInfoEntity.avatar = AccountBusiness.getInstance().getChannelInfo().urlAvatar;
                            userInfoEntity.name = AccountBusiness.getInstance().getChannelInfo().name;
                        }

                    }
                }
                ListCommentEntity entity = new ListCommentEntity(userInfoEntity, comment.trim());
                mJzvdStd.setMessage(entity);
                EventBus.getDefault().post(new OnSendCommentEvent(comment.trim()));
            }

        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void onUpdateLiveCount(long countLive) {
        try {
            if (modelData == null || mJzvdStd == null) return;
            if (modelData.isLive()) {
                mJzvdStd.setViewLive(Common.getViewLive(Math.max(countLive, 1)));
            }
        } catch (Exception e) {
            Common.handleException(e);
        }

    }


    public void onSuccessCommentList(List<ListCommentEntity> listComment) {
        try {
            if (CollectionUtils.isNotEmpty(listComment)) {
                mJzvdStd.isLoading = listComment.size() == 20;
                rowStart = listComment.get(listComment.size() - 1).base64RowID;

                mJzvdStd.setDataComment(listComment);
            } else {
                mJzvdStd.isLoading = false;
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void setListMessageFullScreen(List<ListCommentEntity> entityList) {
        try {
            mJzvdStd.setDataComment(entityList);
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void setMessageFullScreen(ListCommentEntity entity) {
        try {
            mJzvdStd.setMessage(entity);
        } catch (Exception e) {
            Common.handleException(e);
        }
    }


    @Subscribe
    public void onChangeVideoEvent(ChangeVideoEvent event) {
        try {
            logTime();
            modelData = event.response;
            initVideoPlayer(event.response);
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mJzvdStd.onStatePause();
            mJzvdStd.pause();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private void logTime() {
        try {
            if (isLogin()) {
                idUser = String.valueOf(AccountBusiness.getInstance().getUserId());
            } else {
                idUser = DeviceUtils.getUniqueDeviceId();
            }
            if (mJzvdStd != null) {
                currentPosition = mJzvdStd.mCurrentPosition;
            }
            presenter.getLogView(idUser, String.valueOf(modelData.id), modelData.link, currentPosition / 1000, String.valueOf(modelData.gameId));
        } catch (Exception e) {
            handleException(e);
        }
    }

    public boolean isHorizontal(){
        return mJzvdStd.isHorizontal;
    }

    @Subscribe
    public void OnBackDeviceEvent(OnBackDeviceEvent commentEvent) {
        try {
            mJzvdStd.gotoNormalScreen();
            Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);

        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public interface CallBackFragment {
        void callBackTinyScreen();

        void callBackScreenExpand();

        void closeTinyScreen();

        void onClickExitFullScreen();
    }

    public void showThankMessNotLogin(int coin, String cmt){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                donateOverlay.setVisibility(View.VISIBLE);
                tvDesDonate.setText(getString(R.string.donate_thank_for_donated_coin, coin));
                tvDonateComment.setText(cmt);
                donateOverlay.animate().translationY(90);
                donateOverlay.animate().setDuration(400);
            }
        }, 200);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                donateOverlay.animate().setDuration(400);
                donateOverlay.animate().translationY(-25);
            }
        }, 4000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                donateOverlay.setVisibility(GONE);
            }
        }, 5400);
    }
}

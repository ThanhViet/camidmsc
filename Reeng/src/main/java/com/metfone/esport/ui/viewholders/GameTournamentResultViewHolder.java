package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.repsonse.GameMatchResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.selfcare.R;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.metfone.esport.common.Constant.DateFormat.LONG_MONTH_DAY_YEAR;
import static com.metfone.esport.common.Constant.DateFormat.MONTH_DAY_YEAR_H_M_S_A;

public class GameTournamentResultViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.ivLogoTeam1)
    AppCompatImageView ivLogoTeam1;

    @BindView(R.id.ivLogoTeam2)
    AppCompatImageView ivLogoTeam2;

    @BindView(R.id.tvGameDate)
    AppCompatTextView tvGameDate;

    @BindView(R.id.tvGameTournament)
    AppCompatTextView tvGameTournament;

    @BindView(R.id.tvGameScore)
    AppCompatTextView tvGameScore;

    @BindView(R.id.tvTeam1)
    AppCompatTextView tvTeam1;

    @BindView(R.id.tvTeam2)
    AppCompatTextView tvTeam2;

    public GameTournamentResultViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_game_result, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

//    private VideoLiveWithInfoHorizontalViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//        ButterKnife.bind(this, itemView);
//    }

    public void binData(GameMatchResponse entity, IOnClickItem clickListener) {
        if (entity == null)
            return;

        try {
            ImageLoader.setImage(
                    ivLogoTeam1,
                    entity.avatarT1,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    new MultiTransformation<>(new CenterCrop(), new RoundedCorners(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall))));

            ImageLoader.setImage(
                    ivLogoTeam2,
                    entity.avatarT2,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    new MultiTransformation<>(new CenterCrop(), new RoundedCorners(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall))));

            tvTeam1.setText(entity.nameT1);
            tvTeam2.setText(entity.nameT2);

            Date startTime = Common.convertStringToDate(entity.startTime, MONTH_DAY_YEAR_H_M_S_A);

            tvGameDate.setText(startTime != null ? Common.convertDateToString(startTime, LONG_MONTH_DAY_YEAR) : "");

            tvGameTournament.setText(entity.tourName);
            tvGameScore.setText(entity.result);

            itemView.setOnClickListener(v -> {
                if (clickListener != null)
                    clickListener.onClickItem(entity);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface IOnClickItem {
        void onClickItem(GameMatchResponse entity);
    }
}

package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.Utils;
import com.drakeet.multitype.ItemViewBinder;
import com.metfone.selfcare.R;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.ui.viewholders.HomeGameViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchGamesItemViewBinder extends ItemViewBinder<GameInfoResponse, HomeGameViewHolder> {
    private int width;

    private HomeGameViewHolder.IOnClickItem onClickItem;

    public SearchGamesItemViewBinder(HomeGameViewHolder.IOnClickItem onClickItem) {
        super();

        this.onClickItem = onClickItem;

        width = (ScreenUtils.getScreenWidth() - Utils.getApp().getResources().getDimensionPixelSize(R.dimen.pading_layout) * 4) / 3;
    }

    @NotNull
    @Override
    public HomeGameViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HomeGameViewHolder(layoutInflater, viewGroup, width);
    }

    @Override
    public void onBindViewHolder(@NotNull HomeGameViewHolder viewBinderHolder, GameInfoResponse s) {
        viewBinderHolder.binData(s, false, onClickItem);
    }
}

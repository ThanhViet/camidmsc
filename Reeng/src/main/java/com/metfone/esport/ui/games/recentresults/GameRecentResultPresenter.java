package com.metfone.esport.ui.games.recentresults;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.GameRecentResultResponse;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class GameRecentResultPresenter extends BasePresenter<GameRecentResultFragment, BaseModel> {
    private Disposable apiDispose = null;

    public GameRecentResultPresenter(GameRecentResultFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(final Long gameId) {
        if (apiDispose != null)
            apiDispose.dispose();

        apiDispose = RxUtils.async(
                compositeDisposable,
                apiService.getGameRecentResult(gameId, getDefaultParam()),
                new ICallBackResponse<ArrayList<GameRecentResultResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<GameRecentResultResponse> response) {
                        if (view != null) view.onGetDataSuccess(response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }
                });
    }
}

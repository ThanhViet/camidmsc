package com.metfone.esport.ui.mylists.mylistvideos.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HalfVideoWithOptionViewHolder;

import java.util.ArrayList;

public class MyListVideoAdapter extends BaseListAdapter<VideoInfoResponse, HalfVideoWithOptionViewHolder> {

    private Context context;
    private HalfVideoWithOptionViewHolder.IOnClickItem onClickItem;
    private boolean isShowOption = true;

    public void setShowOption() {
        isShowOption = false;
    }

    public MyListVideoAdapter(Context context, ArrayList<VideoInfoResponse> items) {
        super(context);

        this.context = context;

        setNewData(items);
    }

    public MyListVideoAdapter(Context context, ArrayList<VideoInfoResponse> items, HalfVideoWithOptionViewHolder.IOnClickItem onClickItem) {
        this(context, items);

        this.onClickItem = onClickItem;
    }

    @NonNull
    @Override
    public HalfVideoWithOptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new HalfVideoWithOptionViewHolder(LayoutInflater.from(context), parent, 0);
    }

    @Override
    public void onBindViewHolder(@NonNull HalfVideoWithOptionViewHolder holder, int position) {
        holder.binData(mData.get(position), onClickItem, true);
        holder.ivOptionMore.setVisibility(isShowOption ? View.VISIBLE : View.GONE);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

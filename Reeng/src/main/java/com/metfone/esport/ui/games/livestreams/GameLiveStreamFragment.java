package com.metfone.esport.ui.games.livestreams;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.homes.adapters.decorations.VerticalTopContentDecoration;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.games.livestreams.adapters.GameLiveStreamAdapter;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class GameLiveStreamFragment extends BaseFragment<GameLiveStreamPresenter> {

    private static String KEY_GAME_ID = "KEY_GAME_ID";

    @BindView(R.id.rvGameLiveStream)
    RecyclerView rvGameLiveStream;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private GameLiveStreamAdapter adapter;
    private Long gameId;
    private int currentPage = 0;

    public static GameLiveStreamFragment newInstance(Long gameId) {
        GameLiveStreamFragment fragment = new GameLiveStreamFragment();
        fragment.setArguments(newBundle(gameId));
        return fragment;
    }

    public static Bundle newBundle(Long gameId) {

        Bundle args = new Bundle();

        args.putLong(KEY_GAME_ID, gameId);

        return args;
    }

    @Override
    public GameLiveStreamPresenter getPresenter() {
        return new GameLiveStreamPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game_live_stream;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();
    }

    private void getDataBundle() {
        if (getArguments() != null)
            gameId = getArguments().getLong(KEY_GAME_ID, 0);
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        currentPage = 0;

        adapter.clear();

        getData();
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(adapter.getData())) multipleStatusView.showLoading();

        presenter.getData(gameId, currentPage++);
    }

    private void initRecyclerView() {
        adapter = new GameLiveStreamAdapter(getContext(), null, new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                if (channel != null)
                    AloneFragmentActivity.with(getActivity())
                            .parameters(ChannelContainerFragment.newBundle(channel.id))
                            .start(ChannelContainerFragment.class);
            }
        });

        rvGameLiveStream.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvGameLiveStream.setHasFixedSize(true);
        rvGameLiveStream.addItemDecoration(new VerticalTopContentDecoration(getContext()));
        rvGameLiveStream.setAdapter(adapter);
        rvGameLiveStream.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(adapter.getData()) - 1) {
                        //bottom of list!
                        getData();
                    }
                }
            }
        });
    }

    public void onGetDataSuccess(ArrayList<VideoInfoResponse> response) {
        if (CollectionUtils.isNotEmpty(response)) {
            adapter.addAll(response);
        }

        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvGameLiveStream != null) rvGameLiveStream.setVisibility(View.INVISIBLE);

        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvGameLiveStream != null) rvGameLiveStream.setVisibility(View.VISIBLE);
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvGameLiveStream != null) rvGameLiveStream.setVisibility(View.INVISIBLE);
        }
    }
}

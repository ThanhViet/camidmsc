package com.metfone.esport.ui.games.channels;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.homes.adapters.decorations.VerticalTopContentDecoration;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.games.channels.adapters.GameChanelAdapter;
import com.metfone.esport.ui.viewholders.MyListChanelViewHolder;
import com.metfone.selfcare.R;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class GameChanelFragment extends BaseFragment<GameChanelPresenter> {

    private static String KEY_GAME_ID = "KEY_GAME_ID";

    @BindView(R.id.rvChannel)
    RecyclerView rvChannel;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private GameChanelAdapter adapter;
    private Long gameId;
    private int currentPage = 0;

    public static GameChanelFragment newInstance(Long gameId) {
        GameChanelFragment fragment = new GameChanelFragment();
        fragment.setArguments(newBundle(gameId));
        return fragment;
    }

    public static Bundle newBundle(Long gameId) {

        Bundle args = new Bundle();

        args.putLong(KEY_GAME_ID, gameId);

        return args;
    }

    @Override
    public GameChanelPresenter getPresenter() {
        return new GameChanelPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game_channel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();
    }

    private void getDataBundle() {
        if (getArguments() != null)
            gameId = getArguments().getLong(KEY_GAME_ID, 0);
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        currentPage = 0;

        adapter.clear();

        getData();
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(adapter.getData())) multipleStatusView.showLoading();

        presenter.getData(gameId, currentPage++);

    }

    private void initRecyclerView() {
        adapter = new GameChanelAdapter(getContext(), null, new MyListChanelViewHolder.IOnClickItem() {
            @Override
            public void onClickItem(ChannelInfoResponse channelInfo) {
                if (channelInfo != null)
                    AloneFragmentActivity.with(getActivity())
                            .parameters(ChannelContainerFragment.newBundle(channelInfo.id))
                            .start(ChannelContainerFragment.class);
            }

            @Override
            public void onClickOption(ChannelInfoResponse channel) {
                if (channel != null) {
                    new SingleSelectBottomSheet()
                            .setOptions(CollectionUtils.newArrayList(new ExtKeyPair("", getString(channel.isFollow == 1 ? R.string.unfollow : R.string.follow), channel.isFollow == 1 ? R.drawable.ic_favorite_white_es : R.drawable.ic_favorite_red_es)))
                            .setOnClickItem(e -> {
                                if (!isLogin()) {
                                    showDialogLogin();

                                    return;
                                }
                                if (channel.isFollow == 1) {
                                    channel.isFollow = 0;
                                    channel.numFollow--;
                                } else {
                                    channel.isFollow = 1;
                                    channel.numFollow++;
                                }

                                presenter.followChannel(channel.id, channel.isFollow);

                                int index = adapter.getData().indexOf(channel);

                                if (index > -1)
                                    adapter.notifyItemChanged(index);

                            })
                            .show(getChildFragmentManager(), null);
                }
            }
        });

        rvChannel.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvChannel.setHasFixedSize(true);
        rvChannel.addItemDecoration(new VerticalTopContentDecoration(getContext()));
        rvChannel.setAdapter(adapter);
        rvChannel.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(adapter.getData()) - 1) {
                        //bottom of list!
                        getData();
                    }
                }
            }
        });
    }

    public void onGetDataSuccess(ArrayList<ChannelInfoResponse> response) {
        if (CollectionUtils.isNotEmpty(response)) {
            adapter.addAll(response);
        }

        if (CollectionUtils.isEmpty(response)) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvChannel != null) rvChannel.setVisibility(View.INVISIBLE);

        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvChannel != null) rvChannel.setVisibility(View.VISIBLE);
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvChannel != null) rvChannel.setVisibility(View.INVISIBLE);
        }
    }
}

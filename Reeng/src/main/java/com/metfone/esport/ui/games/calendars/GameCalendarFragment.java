package com.metfone.esport.ui.games.calendars;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.drakeet.multitype.MultiTypeAdapter;
import com.esport.widgets.MultipleStatusView;
import com.esport.widgets.recycler.CustomLinearLayoutManager;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.repsonse.GameCalendarResponse;
import com.metfone.esport.entity.repsonse.GameMatchResponse;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.esport.homes.adapters.decorations.VerticalTopContentDecoration;
import com.metfone.esport.ui.games.calendars.adapters.GameButtonItemViewBinder;
import com.metfone.esport.ui.games.calendars.adapters.GameCalendarItemViewBinder;
import com.metfone.esport.ui.games.calendars.adapters.GameDateTitleItemViewBinder;
import com.metfone.esport.ui.games.calendars.adapters.ObjectGameButton;
import com.metfone.esport.ui.games.calendars.adapters.ObjectGameDateTitle;
import com.metfone.esport.ui.games.recentresults.GameRecentResultFragment;
import com.metfone.esport.ui.games.tournamentdetails.TournamentDetailFragment;
import com.metfone.esport.ui.searchs.adapters.ObjectTitle;
import com.metfone.esport.ui.searchs.adapters.SearchTextTitleItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchTournamentItemViewBinder;
import com.metfone.selfcare.R;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.common.Constant.DateFormat.LONG_MONTH_DAY_YEAR;
import static com.metfone.esport.common.Constant.DateFormat.MONTH_DAY_YEAR_H_M_S_A;

public class GameCalendarFragment extends BaseFragment<GameCalendarPresenter> {

    private static String KEY_GAME_ID = "KEY_GAME_ID";

    @BindView(R.id.rvGameCalendar)
    RecyclerView rvGameCalendar;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private MultiTypeAdapter adapter = new MultiTypeAdapter();

    private ArrayList<Object> items = new ArrayList<>();
    private Long gameId;

    public static GameCalendarFragment newInstance(Long gameId) {
        GameCalendarFragment fragment = new GameCalendarFragment();
        fragment.setArguments(newBundle(gameId));
        return fragment;
    }

    public static Bundle newBundle(Long gameId) {

        Bundle args = new Bundle();

        args.putLong(KEY_GAME_ID, gameId);

        return args;
    }

    @Override
    public GameCalendarPresenter getPresenter() {
        return new GameCalendarPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game_calendar;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();
    }

    private void getDataBundle() {
        if (getArguments() != null)
            gameId = getArguments().getLong(KEY_GAME_ID, 0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        refreshData();
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        items.clear();

        adapter.notifyDataSetChanged();

        getData();
    }

    /**
     * Call service lay du lieu man home
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(items)) multipleStatusView.showLoading();

        presenter.getData(gameId);
//
//        //fake data
//        ArrayList<Object> list = new ArrayList<>();
//        list.add(new ObjectGameDateTitle("September 20, 2020"));
////        list.add(new ObjectGameCalendar());
//        list.add(new ObjectGameDateTitle("September 21, 2020"));
////        list.add(new ObjectGameCalendar());
////        list.add(new ObjectGameCalendar());
//        list.add(new ObjectGameButton("See recent results"));
//        list.add(new ObjectTitle("Tournaments"));
//        list.add(new ObjectSearchTournament());
//        list.add(new ObjectSearchTournament());
//
////        onGetDataSuccess(list);
    }

    private void initRecyclerView() {
        adapter.register(ObjectGameDateTitle.class, new GameDateTitleItemViewBinder());
        adapter.register(GameMatchResponse.class, new GameCalendarItemViewBinder(entity -> {
//            AloneFragmentActivity.with(getContext()).start(TournamentDetailFragment.class);
        }));
        adapter.register(ObjectTitle.class, new SearchTextTitleItemViewBinder());
        adapter.register(ObjectGameButton.class, new GameButtonItemViewBinder(() -> {
            AloneFragmentActivity
                    .with(getContext())
                    .parameters(GameRecentResultFragment.newBundle(gameId))
                    .start(GameRecentResultFragment.class);
        }));
        adapter.register(TournamentInfoResponse.class, new SearchTournamentItemViewBinder((TournamentInfoResponse entity) -> {
            AloneFragmentActivity.with(getContext()).parameters(TournamentDetailFragment.newBundle(entity.id))
                    .start(TournamentDetailFragment.class);
        }));

        adapter.setItems(items);

        rvGameCalendar.setLayoutManager(new CustomLinearLayoutManager(getParentActivity()));

        rvGameCalendar.setAdapter(adapter);

        rvGameCalendar.setHasFixedSize(true);

        rvGameCalendar.setItemAnimator(null);

        rvGameCalendar.addItemDecoration(new VerticalTopContentDecoration(getContext()));
    }

    public void onGetDataSuccess(ArrayList<GameCalendarResponse> response) {
        try {
            ArrayList<GameMatchResponse> listMatch = new ArrayList<>();
            ArrayList<TournamentInfoResponse> listTournament = new ArrayList<>();

            for (GameCalendarResponse gameCalendarResponse : response) {
                if (gameCalendarResponse.type.equals("listCalender")) {
                    if (CollectionUtils.isNotEmpty(gameCalendarResponse.lstCalenders))
                        listMatch.addAll(gameCalendarResponse.lstCalenders);
                } else if (gameCalendarResponse.type.equals("listTour")) {
                    if (CollectionUtils.isNotEmpty(gameCalendarResponse.lstTournamentDto))
                        listTournament.addAll(gameCalendarResponse.lstTournamentDto);
                }
            }

            String oldDate = "";

            if (CollectionUtils.isNotEmpty(listMatch)) {
                for (GameMatchResponse match : listMatch) {
                    if (match != null && match.startTime != null && match.startTime.length() > 6) {
                        String date = match.startTime.substring(0, 6);

                        if (!date.equals(oldDate)) {
                            try {
                                String startDate = Common.convertDateToString(Common.convertStringToDate(match.startTime, MONTH_DAY_YEAR_H_M_S_A), LONG_MONTH_DAY_YEAR);

                                items.add(new ObjectGameDateTitle(startDate));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            oldDate = date;
                        }
                    }

                    items.add(match);
                }

                items.add(new ObjectGameButton(getString(R.string.see_recent_result)));
            }

            if (CollectionUtils.isNotEmpty(listTournament)) {
                items.add(new ObjectTitle(getString(R.string.tournaments)));

                items.addAll(listTournament);
            }

            if (CollectionUtils.isEmpty(items)) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();

                if (rvGameCalendar != null) rvGameCalendar.setVisibility(View.INVISIBLE);

            } else {
                if (multipleStatusView != null) multipleStatusView.showContent();

                if (rvGameCalendar != null) rvGameCalendar.setVisibility(View.VISIBLE);

//                adapter.setItems(items);

                if (adapter != null)
                    adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(items)) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvGameCalendar != null) rvGameCalendar.setVisibility(View.INVISIBLE);
        }
    }
}

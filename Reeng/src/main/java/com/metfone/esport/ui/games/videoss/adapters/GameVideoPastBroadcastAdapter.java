package com.metfone.esport.ui.games.videoss.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;

import java.util.ArrayList;

public class GameVideoPastBroadcastAdapter extends BaseListAdapter<VideoInfoResponse, HomeMetfoneViewHolder> {
    private Context context;

    private int width;

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;

    public GameVideoPastBroadcastAdapter(Context context, ArrayList<VideoInfoResponse> items, HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        super(context);

        this.context = context;

        this.onClickItem = onClickItem;

        width = ScreenUtils.getScreenWidth() * 2 / 3;

        setNewData(items);
    }

    @NonNull
    @Override
    public HomeMetfoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new HomeMetfoneViewHolder(LayoutInflater.from(context), parent, width);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeMetfoneViewHolder holder, int position) {
        holder.binData(mData.get(position), onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

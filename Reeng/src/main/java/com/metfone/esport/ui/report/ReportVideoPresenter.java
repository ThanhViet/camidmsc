package com.metfone.esport.ui.report;

import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.ReportEntity;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.database.model.ReengAccount;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 9/22/20.
 */
public class ReportVideoPresenter extends BasePresenter<ReportVideoFragment, BaseModel> {

    public ReportVideoPresenter(ReportVideoFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public List<ReportEntity> getDataDummy(){
        List<ReportEntity> reportEntities =  new ArrayList<>();
        reportEntities.add(new ReportEntity(context.getString(R.string.harassment),true));
        reportEntities.add(new ReportEntity(context.getString(R.string.violence),false));
        reportEntities.add(new ReportEntity(context.getString(R.string.impersonation),false));
        reportEntities.add(new ReportEntity(context.getString(R.string.spam),false));
        reportEntities.add(new ReportEntity(context.getString(R.string.sexually_content),false));
        reportEntities.add(new ReportEntity(context.getString(R.string.cheating),false));
        reportEntities.add(new ReportEntity(context.getString(R.string.other_term),false));
        return reportEntities;
    }

    public void reportVideo(String videoId,String videoName,String link,String path,String reason){
        ReengAccount mAccount = ApplicationController.self().getReengAccountBusiness().getCurrentAccount();
        RxUtils.async(compositeDisposable, apiService.reportVideo(
                mAccount.getNumberJid(),
                mAccount.getName(),
                ServiceRetrofit.getDomain(),
                "networkType",
                videoId,
                videoName, link,
                path,
                UUID.randomUUID().toString(),
                Constant.CLIENT_TYPE,
                String.valueOf(BuildConfig.VERSION_CODE),
                reason

        ), new ICallBackResponse<Object>() {
            @Override
            public void onSuccess(Object response) {
                view.onSuccessReport();
            }

            @Override
            public void onFail(String error) {
                view.onFailReport(error);
            }
        });
    }
}

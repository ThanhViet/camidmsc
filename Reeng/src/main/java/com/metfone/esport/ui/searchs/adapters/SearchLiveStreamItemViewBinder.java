package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchLiveStreamItemViewBinder extends ItemViewBinder<ObjectSearchLive, HalfVideoNoOptionViewHolder> {
    private HalfVideoNoOptionViewHolder.IOnClickItem onClick;

    public SearchLiveStreamItemViewBinder(HalfVideoNoOptionViewHolder.IOnClickItem onClick) {
        this.onClick = onClick;
    }

    @NotNull
    @Override
    public HalfVideoNoOptionViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HalfVideoNoOptionViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull HalfVideoNoOptionViewHolder viewBinderHolder, ObjectSearchLive s) {
        viewBinderHolder.binData(s.video, onClick);
    }
}

package com.metfone.esport.ui.popups;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BasePopupWindow;
import com.metfone.esport.base.ExtKeyPair;

import java.util.ArrayList;

import butterknife.BindView;

public class SingleTextSelectPopup extends BasePopupWindow {
    SingleTextSelectAdapter.IOnClickItem onClickItem;

    ArrayList<ExtKeyPair<?, String>> options;

    ExtKeyPair<?, String> currentOption;

    private SingleTextSelectAdapter adapter;

    @BindView(R.id.rvSingleTextSelect)
    RecyclerView rvSingleTextSelect;

    public SingleTextSelectPopup(Context context, SingleTextSelectAdapter.IOnClickItem onClickItem) {
        super(context);

        this.onClickItem = onClickItem;
    }

    @Override
    protected void onCreate() {
        adapter = new SingleTextSelectAdapter(context, (e) -> {
            if (onClickItem != null) {
                onClickItem.onClickItem(e);

                dismiss();
            }
        });
        rvSingleTextSelect.setHasFixedSize(true);
        rvSingleTextSelect.setAdapter(adapter);
    }

    @Override
    protected int getLayout() {
        return R.layout.popup_single_text_select;
    }

    @Override
    protected void onViewCreated() {

        if (currentOption != null && options != null) {
            for (ExtKeyPair<?, String> option : options) {
                if (option != null) {
                    option.isSelected = option.key == currentOption.key;
                }
            }
        }

        adapter.setNewData(options);
    }

    public SingleTextSelectPopup setOptions(ArrayList<ExtKeyPair<?, String>> options) {
        this.options = options;

        return this;
    }

    public SingleTextSelectPopup setCurrentSelected(ExtKeyPair<?, String> option) {
        this.currentOption = option;

        return this;
    }
}

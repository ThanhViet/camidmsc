package com.metfone.esport.ui.games.luckywheel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.metfone.esport.common.Constant;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.util.Utilities;
import com.mytelsupportsdk.dialog.CustomDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;

public class GameActivity extends BaseSlidingFragmentActivity {

    public static final String AUTHENTICATION_KEY = "AUTHENTICATION_KEY";
    public static final String LINKGAME_KEY = "LINKGAME_KEY";
    public static final String FBSHARE_KEY = "FBSHARE_KEY";

    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String GAME_LINK = "GAME_LINK";
    private String url;

    private static final String TAG = GameActivity.class.getSimpleName();
    private static final String ORIGIN_URL = "https://luckyspin.ugame.vn/api/index.php";
    //    private static final String URL = "file:///android_asset/game.html";
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    String name;
    String userId;
    String fbShare = "";
    private Fragment currentFragment;
    @BindView(R.id.ic_close)
    ImageView closeImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.fragment_activity);
        Intent intent = getIntent();
        String gameLink = intent.getStringExtra(LINKGAME_KEY);
        String authenticationKey = intent.getStringExtra(AUTHENTICATION_KEY);
        Double latitude = intent.getDoubleExtra(LATITUDE, 0);
        Double longitude = intent.getDoubleExtra(LONGITUDE, 0);
        fbShare = intent.getStringExtra(FBSHARE_KEY);
        closeImage = this.findViewById(R.id.ic_close);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        StringBuilder stringBuilder = new StringBuilder();
        try {
            url = stringBuilder.append(gameLink)
                    .append("?authenkey=").append(authenticationKey)
                    .append("&token=").append(AccountBusiness.getInstance().getToken())
                    .append("&sessionId=").append(AccountBusiness.getInstance().getUserId())
                    .append("&latitude=").append(latitude)
                    .append("&longitude=").append(longitude)
                    .toString();
        } catch (Exception ex) {
            url = stringBuilder.append(gameLink)
                    .append("?authenkey=").append(authenticationKey)
                    .append("&token=").append("")
                    .append("&sessionId=").append("")
                    .append("&latitude=").append(latitude)
                    .append("&longitude=").append(longitude)
                    .toString();
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = FrgmtWebView.newInstance(url, "Lucky Wheel");
        currentFragment = fragment;
        ft.add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());
        ft.commit();

    }

    public void inviteFriends() {
        Context context = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            PermissionHelper.requestPermission(this, Manifest.permission.READ_CONTACTS, Constants.PERMISSION.PERMISSION_CONTACT);
        } else {

        }
    }

    public void inviteFb() {
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                if (result != null) {
                    ((FrgmtWebView) currentFragment).ShareFb(name, userId);
                }
                Log.d("aaa", "aaa");
            }

            @Override
            public void onCancel() {
                Log.d("aaa", "aaa");

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("aaa", "aaa");

            }
        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (ShareDialog.canShow(ShareLinkContent.class)) {
                            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                    .setQuote(fbShare)
                                    .setContentUrl(Uri.parse("https://www.facebook.com/210301035798660/posts/1495296497299101/"))
                                    .build();
                            shareDialog.show(linkContent);
                        }
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {

                                        // Application code
                                        try {
                                            name = object.getString("name");
                                            userId = object.getString("id");
                                            SharedPrefs.getInstance().put("fackbookName", name);
                                            SharedPrefs.getInstance().put("fackbookId", userId);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "name, id");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Toast.makeText(GameActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });


        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Resources res = getResources();
        new DialogConfirm(this, true).setMessage(res.getString(R
                .string.msg_confirm_logout_game)).
                setNegativeLabel(res.getString(R.string.cancel)).setPositiveLabel(res.getString(R.string.label_ok)).
                setPositiveListener(result -> {
                    GameActivity.super.onBackPressed();
                }).show();
    }

}

package com.metfone.esport.ui.searchs.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

public class SearchDefaultDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SearchDefaultDecoration(Context context) {
        space = context != null ? context.getResources().getDimensionPixelSize(R.dimen.pading_layout) : 0;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        outRect.top = space / 2;
        outRect.left = space;
        outRect.right = space;
    }
}

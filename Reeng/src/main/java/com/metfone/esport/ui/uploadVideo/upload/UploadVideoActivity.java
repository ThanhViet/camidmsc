/*
 * Created by admin on 2020/9/6
 */

package com.metfone.esport.ui.uploadVideo.upload;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.UriUtils;
import com.blankj.utilcode.util.Utils;
import com.esport.widgets.recycler.GridDividerItemDecoration;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.CategoryUploadModel;
import com.metfone.esport.entity.ThumbVideoUpload;
import com.metfone.esport.entity.UploadVideoEntity;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.GameInfoEntity;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.uploadVideo.upload.adapter.SpinnerCategoryAdapter;
import com.metfone.esport.ui.uploadVideo.upload.adapter.ThumbVideoUploadAdapter;
import com.metfone.esport.ui.uploadVideo.uploading.UploadingVideoActivity;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class UploadVideoActivity extends BaseActivity<UploadVideoPresenter> {
    @BindView(R.id.btn_back)
    View btnBack;
    @BindView(R.id.btn_upload)
    View btnUpload;
    @BindView(R.id.tv_schedule_date)
    AppCompatTextView tvScheduleDate;
    @BindView(R.id.tv_schedule_time)
    AppCompatTextView tvScheduleTime;
    @BindView(R.id.iv_schedule_date)
    AppCompatImageView ivScheduleDate;
    @BindView(R.id.iv_schedule_time)
    AppCompatImageView ivScheduleTime;
    @BindView(R.id.sp_category)
    AppCompatSpinner spCategory;
    @BindView(R.id.edit_title)
    AppCompatEditText editTitle;
    @BindView(R.id.edit_desc)
    AppCompatEditText editDesc;
    @BindView(R.id.iv_schedule)
    AppCompatImageView ivSchedule;
    @BindView(R.id.layout_schedule_time)
    View layoutScheduleTime;
    @BindView(R.id.tv_warning_title)
    AppCompatTextView tvWarningTitle;
    @BindView(R.id.tv_warning_desc)
    AppCompatTextView tvWarningDesc;
    @BindView(R.id.tv_warning_category)
    AppCompatTextView tvWarningCategory;
    @BindView(R.id.tv_warning_schedule)
    AppCompatTextView tvWarningSchedule;
    @BindView(R.id.tv_count_char_title)
    AppCompatTextView tvCountCharTitle;
    @BindView(R.id.tv_count_char_desc)
    AppCompatTextView tvCountCharDesc;
    @BindView(R.id.recycler_thumb)
    RecyclerView recyclerThumb;
    private SpinnerCategoryAdapter adapterCategory;
    private ArrayList<CategoryUploadModel> listCategories = new ArrayList<>();
    private ThumbVideoUploadAdapter adapterThumb;
    //private ArrayList<ThumbVideoUpload> listThumbs = new ArrayList<>();
    private CategoryUploadModel currentCategory;
    private String filePath;
    private Date datePublished = null;
    private long channelId = 0;

    @Override
    public UploadVideoPresenter getPresenter() {
        return new UploadVideoPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_upload_video_es;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filePath = getIntent().getStringExtra(Constant.KEY_FILE_PATH);
        if (StringUtils.isEmpty(filePath)) {
            finish();
            return;
        }
        if (btnBack != null) btnBack.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                finish();
            }
        });
        if (btnUpload != null) btnUpload.setOnClickListener(new OnSingleClickListener() {
            @Override
            public boolean isCheckLogin() {
                return true;
            }

            @Override
            public boolean isCheckNetwork() {
                return true;
            }

            @Override
            public void onSingleClick(View view) {
                if (checkValidInput()) {
                    view.setEnabled(false);
                    getPresenter().uploadVideo(filePath, String.valueOf(editTitle.getText()), String.valueOf(editDesc.getText()), currentCategory, datePublished, adapterThumb.getThumbUpload());
                }
            }
        });
        if (editTitle != null) {
            editTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharTitle != null)
                        tvCountCharTitle.setText(charSequence.length() + "/100");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (!StringUtils.isEmpty(editable)) {
                        if (tvWarningTitle != null) tvWarningTitle.setVisibility(View.GONE);
                    }
                }
            });
        }
        if (editDesc != null) {
            editDesc.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharDesc != null)
                        tvCountCharDesc.setText(charSequence.length() + "/200");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (!StringUtils.isEmpty(editable)) {
                        if (tvWarningDesc != null) tvWarningDesc.setVisibility(View.GONE);
                    }
                }
            });
        }
        if (spCategory != null) {
            listCategories.add(new CategoryUploadModel(-1, getString(R.string.choose_game_category)));
            adapterCategory = new SpinnerCategoryAdapter(this, R.layout.item_category_video_upload, listCategories);
            adapterCategory.setDropDownViewResource(R.layout.item_category_video_upload);
            spCategory.setAdapter(adapterCategory);
            spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (adapterCategory != null && adapterCategory.getCount() > 0) {
                        currentCategory = adapterCategory.getItem(i);
                        if (currentCategory != null && currentCategory.getId() > 0) {
                            if (tvWarningCategory != null)
                                tvWarningCategory.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
        if (ivSchedule != null) {
            ivSchedule.setSelected(false);
            ivSchedule.setOnClickListener(view -> {
                boolean isSelected = view.isSelected();
                if (isSelected) {
                    if (layoutScheduleTime != null)
                        layoutScheduleTime.setVisibility(View.GONE);
                } else {
                    if (tvWarningSchedule != null) tvWarningSchedule.setVisibility(View.GONE);
                    if (layoutScheduleTime != null)
                        layoutScheduleTime.setVisibility(View.VISIBLE);
                    if (datePublished == null) {
                        datePublished = new Date((long) (Math.ceil((double) System.currentTimeMillis() / 300000) * 300000)); //Lam tron den 5p
                    }
                    if (tvScheduleDate != null) tvScheduleDate.setVisibility(View.VISIBLE);
                    if (tvScheduleTime != null) tvScheduleTime.setVisibility(View.VISIBLE);
                    if (ivScheduleDate != null) ivScheduleDate.setVisibility(View.VISIBLE);
                    if (ivScheduleTime != null) ivScheduleTime.setVisibility(View.VISIBLE);

                    setTextScheduleDate();
                }
                view.setSelected(!isSelected);
            });
        }
        if (layoutScheduleTime != null) {
            layoutScheduleTime.setOnClickListener(new OnSingleClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onSingleClick(View view) {
                    new SingleDateAndTimePickerDialog.Builder(UploadVideoActivity.this)
                            .bottomSheet()
                            .curved()
                            .mustBeOnFuture()
                            .defaultDate(datePublished)
                            .customLocale(Constant.DEFAULT_LOCALE)
                            .displayMinutes(true)
                            .displayHours(true)
                            .displayDays(false)
                            .displayMonth(true)
                            .displayYears(true)
                            .displayDaysOfMonth(true)
                            .listener(date -> {
                                datePublished = date;
                                setTextScheduleDate();
                            })
                            .display();
                }
            });
        }
        if (recyclerThumb != null) {
            GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
            adapterThumb = new ThumbVideoUploadAdapter(this, null);
            adapterThumb.add(new ThumbVideoUpload(ThumbVideoUpload.TYPE_CHOOSE_PHOTO));
            adapterThumb.setListener(size -> {
                if (size >= 6)
                    showToast(R.string.msg_warning_max_photo_upload_video);
                else
                    Utilities.openGallery(UploadVideoActivity.this, Constant.REQUEST_CODE.OPEN_GALLERY_CHOOSE_THUMB_UPLOAD_VIDEO);
            });
            if (recyclerThumb.getItemDecorationCount() <= 0)
                recyclerThumb.addItemDecoration(new GridDividerItemDecoration(this, R.drawable.divider_grid, 3));
            recyclerThumb.setLayoutManager(layoutManager);
            recyclerThumb.setAdapter(adapterThumb);
        }
        ChannelInfoResponse channel = AccountBusiness.getInstance().getChannelInfo();
        if (channel != null) channelId = AccountBusiness.getInstance().getChannelInfo().id;
        getPresenter().loadCategory(channelId);
        long duration = FileUtils.getDuration(filePath);
        int totalThumbsCount = Math.round(duration / 30000f);
        totalThumbsCount = Math.max(totalThumbsCount, 1);
        totalThumbsCount = Math.min(totalThumbsCount, 5);
        getPresenter().loadThumbnail(Uri.parse(filePath), totalThumbsCount, 0, duration);
    }

    /**
     * Set text schedule Date
     * Created_by @dchieu on 11/26/20
     */
    private void setTextScheduleDate() {
        if (datePublished == null) return;

        Calendar calendar = Calendar.getInstance(Constant.DEFAULT_LOCALE);
        calendar.setTime(datePublished);
        SimpleDateFormat df;
        if (tvScheduleDate != null) {
            tvScheduleDate.setVisibility(View.VISIBLE);
            df = new SimpleDateFormat("MMM dd, yyyy", Constant.DEFAULT_LOCALE);
            tvScheduleDate.setText(df.format(datePublished));
            if (ivScheduleDate != null)
                ivScheduleDate.setVisibility(View.VISIBLE);
        }
        if (tvScheduleTime != null) {
            tvScheduleTime.setVisibility(View.VISIBLE);
            df = new SimpleDateFormat("HH:mm", Constant.DEFAULT_LOCALE);
            tvScheduleTime.setText(df.format(datePublished));
            if (ivScheduleTime != null)
                ivScheduleTime.setVisibility(View.VISIBLE);
        }
    }

    private boolean checkValidInput() {
        if (editTitle != null && StringUtils.isEmpty(editTitle.getText())) {
            if (tvWarningTitle != null) tvWarningTitle.setVisibility(View.VISIBLE);
            editTitle.requestFocus();
            return false;
        }
        if (editDesc != null && StringUtils.isEmpty(editDesc.getText())) {
            if (tvWarningDesc != null) tvWarningDesc.setVisibility(View.VISIBLE);
            editDesc.requestFocus();
            return false;
        }
        if (spCategory != null) {
            long itemId = 0;
            try {
                itemId = currentCategory == null ? -1 : currentCategory.getId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (itemId <= 0) {
                if (tvWarningCategory != null) tvWarningCategory.setVisibility(View.VISIBLE);
                KeyboardUtils.hideSoftInput(this);
                spCategory.requestFocus();
                return false;
            }
        }
        if (ivSchedule != null) {
            if (ivSchedule.isSelected()) {
                if (datePublished == null || datePublished.getTime() < System.currentTimeMillis()) {
                    if (tvWarningSchedule != null) {
                        tvWarningSchedule.setText(R.string.warning_upload_schedule_2);
                        tvWarningSchedule.setVisibility(View.VISIBLE);
                    }
                    if (layoutScheduleTime != null) layoutScheduleTime.requestFocus();
                    KeyboardUtils.hideSoftInput(this);
                    return false;
                }
            } else {
//                if (tvWarningSchedule != null) {
//                    tvWarningSchedule.setText(R.string.warning_upload_schedule);
//                    tvWarningSchedule.setVisibility(View.VISIBLE);
//                }
                ivSchedule.requestFocus();
                KeyboardUtils.hideSoftInput(this);
//                return false;
            }
        }
        KeyboardUtils.hideSoftInput(this);
        return true;
    }

    public void loadThumbSuccess(ThumbVideoUpload item) {
//        if (listThumbs == null) listThumbs = new ArrayList<>();
//        listThumbs.add(item);
        if (adapterThumb != null) {
            item.isSelect = true;
            adapterThumb.add(item);
            adapterThumb.notifyDataSetChanged();
        }
    }

    public void updateCategories(ArrayList<GameInfoEntity> list) {
        if (CollectionUtils.isNotEmpty(list)) {
            if (listCategories == null) listCategories = new ArrayList<>();
            else listCategories.clear();
            listCategories.add(new CategoryUploadModel(-1, getString(R.string.choose_game_category)));
            for (GameInfoEntity item : list) {
                if (item != null) {
                    listCategories.add(new CategoryUploadModel(item.id, item.gameName));
                }
            }
            if (adapterCategory != null) adapterCategory.notifyDataSetChanged();
        }
    }

    public void uploadVideo(UploadVideoEntity data) {
        try {
            Intent intent = new Intent(this, UploadingVideoActivity.class);
            intent.putExtra(Constant.KEY_DATA, data);
            startActivity(intent);
            finish();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Constant.REQUEST_CODE.OPEN_GALLERY_CHOOSE_THUMB_UPLOAD_VIDEO) {
            try {
                if (data != null && data.getData() != null) {
                    File file = UriUtils.uri2File(data.getData());
                    ThumbVideoUpload item = new ThumbVideoUpload(ThumbVideoUpload.TYPE_FILE_PATH);
                    item.setFilePath(file.getPath());
                    if (adapterThumb != null) {
                        if (CollectionUtils.isNotEmpty(adapterThumb.getData()))
                            for (ThumbVideoUpload datum : adapterThumb.getData()) {
                                datum.isSelect = false;
                            }
                        item.isSelect = true;
                        adapterThumb.add(1, item);
                        adapterThumb.notifyDataSetChanged();
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }
}

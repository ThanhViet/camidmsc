package com.metfone.esport.ui.account;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.UriUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.event.CreateChannelEvent;
import com.metfone.esport.entity.event.OnEditMyChannelEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.createchannel.CreateChannelFragment;
import com.metfone.esport.ui.mychannel.MyChannelFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.common.Common.handleException;

/**
 * Created by Nguyễn Thành Chung on 9/15/20.
 */
public class AccountFragment extends BaseFragment<AccountPresenter> {
    public static final int ITEM_MY_CHANNEL = 0;
    public static final int ITEM_SWITCH_ACCOUNT = 1;
    @BindView(R.id.btn_back)
    AppCompatImageView btnBack;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.layout_action_bar)
    RelativeLayout layoutActionBar;
    @BindView(R.id.ivLogoTeam)
    AvatarView ivLogoTeam;
    @BindView(R.id.TeamName)
    AppCompatTextView TeamName;
    @BindView(R.id.tvTypeAccount)
    AppCompatTextView tvTypeAccount;
    @BindView(R.id.ivCheck)
    AppCompatImageView ivCheck;
    @BindView(R.id.lnItem)
    LinearLayout lnItem;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.viewRoot)
    LinearLayout viewRoot;

    @Override
    public AccountPresenter getPresenter() {
        return new AccountPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_account;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            EventBus.getDefault().register(this);
            lnItem.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.view_live));
            initRecyclerData();
            if (btnBack != null) btnBack.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    onClickBack();
                }
            });
            setupMyChannel();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private void initRecyclerData() {
        try {
            rvData.setLayoutManager(new LinearLayoutManager(getContext()));
            rvData.setHasFixedSize(true);
            AccountAdapter adapter = new AccountAdapter(getContext());
            adapter.setNewData(presenter.getDataDummy());
            adapter.setOnClickItem((entity, position) -> {
                switch (position) {
                    case ITEM_MY_CHANNEL:
                        if (CollectionUtils.isNotEmpty(AccountBusiness.getInstance().getMyChannels())) {
                            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                                showToast(ApplicationController.getApp().getString(R.string.msg_choose_account_channel));
                            }else {
                                Bundle bundle = new Bundle();
                                bundle.putLong(Constant.KEY_ID, AccountBusiness.getInstance().getChannelInfo().id);
                                AloneFragmentActivity.with(getContext()).parameters(bundle).start(MyChannelFragment.class);
                            }

                        } else {
                            AloneFragmentActivity.with(getContext()).start(CreateChannelFragment.class);
                        }
                        break;
                    case ITEM_SWITCH_ACCOUNT:
                        presenter.getMyChannel();
                        break;
                }

            });
            rvData.setAdapter(adapter);
        } catch (Exception e) {
            Common.handleException(e);
        }
    }



    public void onSuccessMyChannel(ArrayList<ChannelInfoResponse> response){
        try{
            SwitchAccountDialog dialog = new SwitchAccountDialog();
            dialog.setOnDismissListener(dialog1 -> {
                if (isAdded()) {
                    setupMyChannel();
                }
            });
            dialog.show(getChildFragmentManager());
        }catch (Exception e){
            handleException(e);
        }
    }

    private void setupMyChannel() {
        try {
            if (ivCheck != null) ivCheck.setVisibility(View.GONE);
            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getParentActivity());
                UserInfo currentUser = userInfoBusiness.getUser();
                String name = currentUser.getFull_name();
                if (TeamName != null) TeamName.setText(name);
                if (tvTypeAccount != null) {
                    tvTypeAccount.setVisibility(View.VISIBLE);
                    tvTypeAccount.setText(getString(R.string.camid_account_es));
                }
                //Neu khong co channel -> Hien thi rank va ten nguoi dung
                if (TextUtils.isEmpty(currentUser.getAvatar())) {
                    AccountRankDTO account = null;
                    try {
                        account = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_ACCOUNT_RANK_KH, AccountRankDTO.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (account!=null){
                        int rankID = account.rankId;
                        EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
                        EnumUtils.CrownRank crownRank = EnumUtils.CrownRank.getById(rankID);
                        if (myRank != null) {
                            Glide.with(this)
                                    .load(myRank.drawable)
                                    .centerCrop()
                                    .placeholder(R.drawable.ic_avatar_member)
                                    .error(R.drawable.ic_avatar_member)
                                    .into(ivLogoTeam);
                        } else {
                            showAvatarCamid();
                        }
                    }else {
                        showAvatarCamid();
                    }
                } else {
                    Glide.with(this)
                            .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                            .transform(new MultiTransformation<Bitmap>(new CenterCrop(), new CircleCrop()))
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(ivLogoTeam);
                }
            }else {
                ChannelInfoResponse channelInfo = AccountBusiness.getInstance().getChannelInfo();
                if (channelInfo != null) {
                    ivLogoTeam.setAvatarUrl(channelInfo.getUrlAvatar());
                    if (TeamName != null) TeamName.setText(channelInfo.getName());
                    if (tvTypeAccount != null) {
                        tvTypeAccount.setVisibility(View.VISIBLE);
                        tvTypeAccount.setText(getString(R.string.channel_account_es));
                    }
                }
            }

        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    @Subscribe
    public void onEvent(OnEditMyChannelEvent event) {
        try{
//            presenter.getMyChannel();
            setupMyChannel();
        }catch (Exception e){
            Common.handleException(e);
        }
    }
    @Subscribe
    public void onEvent(CreateChannelEvent event) {
        try{
            setupMyChannel();
            if (event.fileAvatar != null){
                ivLogoTeam.setAvatarUri(event.fileAvatar);
            }
        }catch (Exception e){
            Common.handleException(e);
        }
    }

    private void showAvatarCamid() {
        try {
            AccountRankDTO account = ApplicationController.self().getAccountRankDTO();
            KhUserRank myRank = KhUserRank.getById(account.rankId);
            Glide.with(getParentActivity())
                    .load(myRank.resAvatar)
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_member)
                    .error(R.drawable.ic_avatar_member)
                    .into(ivLogoTeam);
        } catch (Exception e) {
            handleException(e);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}

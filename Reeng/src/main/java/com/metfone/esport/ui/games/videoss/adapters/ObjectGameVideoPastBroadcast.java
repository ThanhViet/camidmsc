package com.metfone.esport.ui.games.videoss.adapters;

import com.metfone.esport.entity.repsonse.VideoInfoResponse;

import java.util.ArrayList;

public class ObjectGameVideoPastBroadcast {
    public ArrayList<VideoInfoResponse> listVideo;

    public ObjectGameVideoPastBroadcast(ArrayList<VideoInfoResponse> listVideo) {
        this.listVideo = listVideo;
    }

    public ObjectGameVideoPastBroadcast() {
    }
}

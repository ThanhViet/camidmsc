package com.metfone.esport.ui.channels.channelhomes;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.drakeet.multitype.MultiTypeAdapter;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.entity.repsonse.ChannelHomeResponse;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.homes.adapters.ExtSaveStateMultiTypeAdapter;
import com.metfone.esport.ui.channels.channelgamecategory.ChannelGameCategoryFragment;
import com.metfone.esport.ui.channels.channelhomes.adapters.ChannelHomeDecoration;
import com.metfone.esport.ui.channels.channelhomes.adapters.ChannelHomeGameItemViewBinder;
import com.metfone.esport.ui.channels.channelhomes.adapters.ChannelHomeLiveItemViewBinder;
import com.metfone.esport.ui.channels.channelhomes.adapters.ChannelVideoUploadedItemViewBinder;
import com.metfone.esport.ui.channels.channelhomes.adapters.ObjectChannelHomeGame;
import com.metfone.esport.ui.games.videoss.adapters.GameVideoPastBroadcastItemViewBinder;
import com.metfone.esport.ui.games.videoss.adapters.ObjectGameVideoPastBroadcast;
import com.metfone.esport.ui.searchs.adapters.ObjectTitle;
import com.metfone.esport.ui.searchs.adapters.SearchTextTitleItemViewBinder;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class ChannelHomeFragment extends BaseFragment<ChannelHomePresenter> {

    private static String KEY_ID = "KEY_ID";

    @BindView(R.id.rvChannelHome)
    RecyclerView rvChannelHome;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private MultiTypeAdapter adapter = new ExtSaveStateMultiTypeAdapter();

    private ArrayList<Object> items = new ArrayList<>();

    private Long channelId;

    private ChannelInfoResponse currentChannel;
    private int currentPage = 0;

    public static ChannelHomeFragment newInstance(Long channelId) {
        ChannelHomeFragment fragment = new ChannelHomeFragment();
        fragment.setArguments(newBundle(channelId));
        return fragment;
    }

    public static Bundle newBundle(Long channelId) {

        Bundle args = new Bundle();

        args.putLong(KEY_ID, channelId);

        return args;
    }

    @Override
    public ChannelHomePresenter getPresenter() {
        return new ChannelHomePresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_channel_home;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();
    }

    private void getDataBundle() {
        if (getArguments() != null)
            channelId = getArguments().getLong(KEY_ID, 0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        refreshData();
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        items.clear();

        currentChannel = null;

        currentPage = 0;

        getData();
    }

    /**
     * Call service lay du lieu man home
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (Utilities.isEmpty(items)) multipleStatusView.showLoading();

        presenter.getData(channelId, currentPage++);
    }

    private void initRecyclerView() {
        adapter.register(ObjectTitle.class, new SearchTextTitleItemViewBinder());
        adapter.register(ChannelInfoResponse.class, new ChannelHomeLiveItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(ObjectGameVideoPastBroadcast.class, new GameVideoPastBroadcastItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer(getActivity(), video);
                ApplicationController.self().getFirebaseEventBusiness().logPastBroadcastEsport(video.title, String.valueOf(video.id));
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(VideoInfoResponse.class, new ChannelVideoUploadedItemViewBinder(new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
                ApplicationController.self().getFirebaseEventBusiness().logUploadedEsport(video.title, String.valueOf(video.id));
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(ObjectChannelHomeGame.class, new ChannelHomeGameItemViewBinder(game -> {
                    if (game != null)
                        AloneFragmentActivity.with(getActivity())
                                .parameters(ChannelGameCategoryFragment.newBundle(currentChannel.id, game.gameName, game.id))
                                .start(ChannelGameCategoryFragment.class);
                })
        );
        adapter.setItems(items);
        rvChannelHome.setAdapter(adapter);

        rvChannelHome.setHasFixedSize(true);

        rvChannelHome.addItemDecoration(new ChannelHomeDecoration(getContext()));
        rvChannelHome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == (adapter.getItemCount() - 1)) {
                    //bottom of list!
                    if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                        getData();
                    }
                }
            }
        });
    }

    private void navigateToChannel(ChannelInfoResponse channel) {
//        if (channel != null)
//            AloneFragmentActivity.with(getActivity())
//                    .parameters(ChannelContainerFragment.newBundle(channel.id))
//                    .start(ChannelContainerFragment.class);
    }

    public void onGetDataSuccess(ArrayList<ChannelHomeResponse> response, int currentPage) {
        if (currentPage == 0) {
            if (CollectionUtils.isEmpty(response)) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();

                if (rvChannelHome != null) rvChannelHome.setVisibility(View.INVISIBLE);

                return;
            } else {
                if (multipleStatusView != null) multipleStatusView.showContent();

                if (rvChannelHome != null) rvChannelHome.setVisibility(View.VISIBLE);
            }

            ChannelInfoResponse channel = null;
            ArrayList<VideoInfoResponse> pastBroadCasts = new ArrayList<>();
            ArrayList<VideoInfoResponse> uploadeds = new ArrayList<>();
            ArrayList<GameInfoResponse> games = new ArrayList<>();

            for (ChannelHomeResponse res : response) {
                if (res.type != null)
                    switch (res.type) {
                        case ChannelHomeResponse.TYPE_CHANNEL_INFO:
                            channel = res.channelInfo;
                            break;
                        case ChannelHomeResponse.TYPE_PAST_BROADCAST:
                            if (CollectionUtils.isNotEmpty(res.pastBroadcast))
                                pastBroadCasts.addAll(res.pastBroadcast);
                            break;
                        case ChannelHomeResponse.TYPE_UPLOADED:
                            if (CollectionUtils.isNotEmpty(res.uploaded))
                                uploadeds.addAll(res.uploaded);
                            break;
                        case ChannelHomeResponse.TYPE_LIST_GAME_OF_CHANNEL:
                            if (CollectionUtils.isNotEmpty(res.listGame))
                                games.addAll(res.listGame);
                            break;
                    }
            }

            if (channel != null) {
                if (channel.videoLive != null && channel.videoLive.isLive() && channel.video == null) {
                    channel.video = channel.videoLive;
                }

                if (channel.video != null && channel.video.isLive())
                    items.add(channel);

                currentChannel = channel;
            }

            if (CollectionUtils.isNotEmpty(pastBroadCasts)) {
                items.add(new ObjectTitle(getString(R.string.past_broad_cast)));
                items.add(new ObjectGameVideoPastBroadcast(pastBroadCasts));
            }

            if (CollectionUtils.isNotEmpty(uploadeds)) {
                items.add(new ObjectTitle(getString(R.string.uploaded)));
                items.addAll(uploadeds);

                if (uploadeds.size() >= BasePresenter.DEFAULT_LIMIT) {
                    presenter.canLoadMore = true;
                }
            }
            if (CollectionUtils.isNotEmpty(games)) {
                items.add(new ObjectTitle(getString(R.string.template_channel_video_by_category, channel != null ? channel.name : "")));
                items.add(new ObjectChannelHomeGame(games));
            }

//            adapter.setItems(items);
//
//            if (adapter != null)
//                adapter.notifyDataSetChanged();
        } else {
            for (ChannelHomeResponse res : response) {
                if (res.type != null && res.type.equalsIgnoreCase(ChannelHomeResponse.TYPE_UPLOADED) && CollectionUtils.isNotEmpty(res.uploaded)) {
                    for (int i = items.size() - 1; i >= 0; i--) {
                        if (items.get(i) instanceof VideoInfoResponse) {
                            items.addAll(i + 1, res.uploaded);

                            break;
                        }
                    }

                    if (res.uploaded.size() >= BasePresenter.DEFAULT_LIMIT) {
                        presenter.canLoadMore = true;
                    }

                    break;
                }
            }
        }

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(items)) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvChannelHome != null) rvChannelHome.setVisibility(View.INVISIBLE);
        }
    }
}

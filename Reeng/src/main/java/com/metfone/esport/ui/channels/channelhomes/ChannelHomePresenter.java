package com.metfone.esport.ui.channels.channelhomes;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.ChannelHomeResponse;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class ChannelHomePresenter extends BasePresenter<ChannelHomeFragment, BaseModel> {
    private Disposable apiDispose = null;

    public boolean canLoadMore = false;

    public boolean isLoading() {
        return apiDispose != null && !apiDispose.isDisposed();
    }

    public boolean isCanLoadMore() {
        return canLoadMore;
    }

    public ChannelHomePresenter(ChannelHomeFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(final Long channelId, int currentPage) {
        if (currentPage > 0 && isLoading()) return;

        if (currentPage == 0 && apiDispose != null)
            apiDispose.dispose();

        canLoadMore = false;

        apiDispose = RxUtils.async(
                compositeDisposable,
                apiService.getChannelHome(channelId, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam()),
                new ICallBackResponse<ArrayList<ChannelHomeResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<ChannelHomeResponse> response) {
                        if (view != null) view.onGetDataSuccess(response, currentPage);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }

                    @Override
                    public void onFinish() {
                        if (apiDispose != null)
                            apiDispose.dispose();
                    }
                });
    }
}

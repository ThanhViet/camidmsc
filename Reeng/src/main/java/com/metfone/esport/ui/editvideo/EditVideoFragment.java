/*
 * Created by admin on 2020/9/6
 */

package com.metfone.esport.ui.editvideo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.UriUtils;
import com.esport.widgets.recycler.GridDividerItemDecoration;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.CategoryUploadModel;
import com.metfone.esport.entity.ThumbVideoUpload;
import com.metfone.esport.entity.UploadVideoEntity;
import com.metfone.esport.entity.repsonse.GameInfoEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.mychannel.WarningEditVideoDialog;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.entity.ThumbVideoUpload.TYPE_FILE_URL;

public class EditVideoFragment extends BaseFragment<EditVideoPresenter> {
    @BindView(R.id.btn_back)
    View btnBack;
    @BindView(R.id.btn_edit)
    View btnEdit;
    @BindView(R.id.tv_schedule_date)
    AppCompatTextView tvScheduleDate;
    @BindView(R.id.tv_schedule_time)
    AppCompatTextView tvScheduleTime;
    @BindView(R.id.iv_schedule_date)
    AppCompatImageView ivScheduleDate;
    @BindView(R.id.iv_schedule_time)
    AppCompatImageView ivScheduleTime;
    @BindView(R.id.sp_category)
    AppCompatSpinner spCategory;
    @BindView(R.id.edit_title)
    AppCompatEditText editTitle;
    @BindView(R.id.edit_desc)
    AppCompatEditText editDesc;
    @BindView(R.id.iv_schedule)
    AppCompatImageView ivSchedule;
    @BindView(R.id.layout_schedule_time)
    View layoutScheduleTime;
    @BindView(R.id.tv_warning_title)
    AppCompatTextView tvWarningTitle;
    @BindView(R.id.tv_warning_desc)
    AppCompatTextView tvWarningDesc;
    @BindView(R.id.tv_warning_category)
    AppCompatTextView tvWarningCategory;
    @BindView(R.id.tv_warning_schedule)
    AppCompatTextView tvWarningSchedule;
    @BindView(R.id.tv_count_char_title)
    AppCompatTextView tvCountCharTitle;
    @BindView(R.id.tv_count_char_desc)
    AppCompatTextView tvCountCharDesc;
    @BindView(R.id.recycler_thumb)
    RecyclerView recyclerThumb;
    @BindView(R.id.tv_current_position)
    TextView tvCurrentPosition;
    @BindView(R.id.tv_duration)
    TextView tvDuration;
    @BindView(R.id.seek_bar)
    SeekBar seekBar;
    @BindView(R.id.btn_play)
    AppCompatImageView btnPlay;
    @BindView(R.id.iv_cover)
    AppCompatImageView ivCover;
    @BindView(R.id.video_view)
    VideoView videoView;

    private SpinnerCategoryAdapter adapterCategory;
    private ArrayList<CategoryUploadModel> listCategories = new ArrayList<>();
    private ThumbVideoUploadAdapter adapterThumb;
    //    private ArrayList<ThumbVideoUpload> listThumbs = new ArrayList<>();
    private CategoryUploadModel currentCategory;
    private Date datePublished = null;
    private VideoInfoResponse currentVideo;
    private boolean isPrepared = false;
    private Runnable runnableSeekBar = new Runnable() {
        @Override
        public void run() {
            if (videoView == null || !videoView.isPlaying()) return;
            int totalDuration = videoView.getDuration();
            long currentDuration = videoView.getCurrentPosition();
            int progress = com.metfone.selfcare.util.Utilities.getProgressPercentage(currentDuration, totalDuration);
            if (tvCurrentPosition != null)
                tvCurrentPosition.setText(com.metfone.selfcare.util.Utilities.milliSecondsToTimer(currentDuration));
            if (seekBar != null) {
                seekBar.setProgress(progress);
            }
            updateSeekBar();
        }
    };

    public static Bundle newBundle(VideoInfoResponse video) {
        Bundle bundle = new Bundle();

        bundle.putSerializable(Constant.KEY_DATA, video);

        return bundle;
    }

    @Override
    public EditVideoPresenter getPresenter() {
        return new EditVideoPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_edit_video_es;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            try {
                currentVideo = (VideoInfoResponse) bundle.getSerializable(Constant.KEY_DATA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (currentVideo == null) {
            if (getParentActivity() != null) getParentActivity().finish();
            return;
        }
        if (btnBack != null) btnBack.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                if (getParentActivity() != null) getParentActivity().finish();
            }
        });

        if (btnEdit != null) btnEdit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public boolean isCheckLogin() {
                return true;
            }

            @Override
            public boolean isCheckNetwork() {
                return true;
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onSingleClick(View view) {
                if (checkValidInput()) {
                    WarningEditVideoDialog dialog = WarningEditVideoDialog.newInstance(getString(R.string.title_edit_video), getString(R.string.msg_edit_video));
                    dialog.setCancelable(true);
                    dialog.setPositiveListener(result -> {
                        view.setEnabled(false);
//                    if (listThumbs == null) listThumbs = new ArrayList<>();
//(ArrayList<ThumbVideoUpload>) CollectionUtils.select(listThumbs, item -> item.getType() != TYPE_FILE_URL)
                        //getPresenter().updateVideo(currentVideo.id, String.valueOf(editTitle.getText()), String.valueOf(editDesc.getText()), currentCategory, datePublished, adapterThumb.getThumbUpload());

                        UploadVideoEntity entity = new UploadVideoEntity();
                        entity.videoId = currentVideo.id;
                        entity.title = String.valueOf(editTitle.getText());
                        entity.description = String.valueOf(editDesc.getText());
                        if (AccountBusiness.getInstance().getChannelInfo() != null)
                            entity.channelId = AccountBusiness.getInstance().getChannelInfo().id;

                        if (currentCategory != null) entity.categoryId = currentCategory.getId();
                        if (datePublished != null) entity.publishTime = datePublished.getTime();
                        entity.listPathThumb = new ArrayList<>();
                        ThumbVideoUpload thumb = adapterThumb.getThumbUpload();
                        if (thumb != null && thumb.getType() != ThumbVideoUpload.TYPE_FILE_URL) {
                            if (thumb.getBitmap() != null) {
                                File file = FileUtils.saveBitmapToFile(thumb.getBitmap());
                                if (file != null) entity.listPathThumb.add(file.getPath());
                            } else if (StringUtils.isNotEmpty(thumb.getFilePath())) {
                                entity.listPathThumb.add(thumb.getFilePath());
                            }
                        }
                        //presenter.editUploadVideo(entity);
                        presenter.editUploadVideo(entity, currentVideo);
                        if (dialog != null) dialog.dismissAllowingStateLoss();
                    });
                    dialog.setNegativeListener(result -> {
                        if (dialog != null) dialog.dismissAllowingStateLoss();
                    });
                    dialog.show(getChildFragmentManager());
                }
            }
        });
        if (editTitle != null) {
            editTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharTitle != null)
                        tvCountCharTitle.setText(charSequence.length() + "/100");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (!StringUtils.isEmpty(editable)) {
                        if (tvWarningTitle != null) tvWarningTitle.setVisibility(View.GONE);
                    }
                }
            });
        }
        if (editDesc != null) {
            editDesc.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvCountCharDesc != null)
                        tvCountCharDesc.setText(charSequence.length() + "/200");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (!StringUtils.isEmpty(editable)) {
                        if (tvWarningDesc != null) tvWarningDesc.setVisibility(View.GONE);
                    }
                }
            });
        }

        if (ivSchedule != null) {
            ivSchedule.setOnClickListener(view -> {
                view.setSelected(!view.isSelected());

                setViewSchedule();
            });
        }
        if (layoutScheduleTime != null) {
            layoutScheduleTime.setOnClickListener(new OnSingleClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onSingleClick(View view) {
                    new SingleDateAndTimePickerDialog.Builder(getParentActivity())
                            .bottomSheet()
                            .curved()
                            .mustBeOnFuture()
                            .defaultDate(datePublished)
                            .customLocale(Constant.DEFAULT_LOCALE)
                            .displayMinutes(true)
                            .displayHours(true)
                            .displayDays(false)
                            .displayMonth(true)
                            .displayYears(true)
                            .displayDaysOfMonth(true)
                            .listener(date -> {
                                datePublished = date;

                                setTextScheduleDate();
                            })
                            .display();
                }
            });
        }
        if (recyclerThumb != null) {
            GridLayoutManager layoutManager = new GridLayoutManager(getParentActivity(), 3);
            adapterThumb = new ThumbVideoUploadAdapter(getParentActivity(), null);
            adapterThumb.add(new ThumbVideoUpload(ThumbVideoUpload.TYPE_CHOOSE_PHOTO));
            adapterThumb.setListener(size -> {
                if (size >= 6)
                    showToast(R.string.msg_warning_max_photo_upload_video);
                else
                    Utilities.openGallery(getParentActivity(), Constant.REQUEST_CODE.OPEN_GALLERY_CHOOSE_THUMB_UPLOAD_VIDEO);
            });
            if (recyclerThumb.getItemDecorationCount() <= 0)
                recyclerThumb.addItemDecoration(new GridDividerItemDecoration(getParentActivity(), R.drawable.divider_grid, 3));
            recyclerThumb.setLayoutManager(layoutManager);
            recyclerThumb.setAdapter(adapterThumb);
        }

        initDataFromCurrentVideo();
        if (spCategory != null) {
            listCategories.add(new CategoryUploadModel(-1, getString(R.string.choose_game_category)));
            if (currentCategory != null)
                listCategories.add(currentCategory);

            adapterCategory = new SpinnerCategoryAdapter(getParentActivity(), R.layout.item_category_video_upload, listCategories);
            adapterCategory.setDropDownViewResource(R.layout.item_category_video_upload);
            spCategory.setAdapter(adapterCategory);
            spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (adapterCategory != null && adapterCategory.getCount() > 0) {
                        currentCategory = adapterCategory.getItem(i);
                        if (currentCategory != null && currentCategory.getId() > 0) {
                            if (tvWarningCategory != null)
                                tvWarningCategory.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
//        ChannelInfoResponse channel = AccountBusiness.getInstance().getChannelInfo();
//        if (channel != null) channelId = AccountBusiness.getInstance().getChannelInfo().id;
//        getPresenter().loadCategory(channelId);
//        long duration = FileUtils.getDuration(filePath);
//        int totalThumbsCount = Math.round(duration / 30000f);
//        totalThumbsCount = Math.max(totalThumbsCount, 1);
//        totalThumbsCount = Math.min(totalThumbsCount, 5);
//        getPresenter().loadThumbnail(Uri.parse(filePath), totalThumbsCount, 0, duration);
        ImageLoader.setCoverVideo(ivCover, currentVideo);
        btnPlay.setOnClickListener(v -> {
            if (currentVideo != null && videoView != null) {
                try {
                    if (isPrepared) {
                        if (videoView.isPlaying()) {
                            videoView.pause();
                            stopUpdateSeekBar();
                            if (btnPlay != null) btnPlay.setImageResource(R.drawable.ic_play_es);
                        } else {
                            videoView.start();
                            updateSeekBar();
                            if (btnPlay != null) btnPlay.setImageResource(R.drawable.ic_pause_es);
                        }
                    } else {
                        videoView.setVideoPath(currentVideo.original_path);
                        videoView.start();
                        if (btnPlay != null) btnPlay.setImageResource(R.drawable.ic_pause_es);
                        if (seekBar != null) seekBar.setProgress(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        videoView.setOnPreparedListener(mp -> {
            isPrepared = true;
            if (ivCover != null) ivCover.setVisibility(View.GONE);
            int totalDuration = videoView.getDuration();
            long currentDuration = videoView.getCurrentPosition();
            int progress = com.metfone.selfcare.util.Utilities.getProgressPercentage(currentDuration, totalDuration);
            if (tvCurrentPosition != null)
                tvCurrentPosition.setText(com.metfone.selfcare.util.Utilities.milliSecondsToTimer(currentDuration));
            if (tvDuration != null)
                tvDuration.setText(com.metfone.selfcare.util.Utilities.milliSecondsToTimer(totalDuration));
            if (seekBar != null) {
                seekBar.setProgress(progress);
            }
            updateSeekBar();
        });
        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                return false;
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                isPrepared = false;
                return true;
            }
        });
        videoView.setOnCompletionListener(mp -> {
            if (btnPlay != null) btnPlay.setImageResource(R.drawable.ic_play_es);
            if (seekBar != null) seekBar.setProgress(100);
            stopUpdateSeekBar();
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (videoView != null && isPrepared) {
                    int totalDuration = videoView.getDuration();
                    int currentPosition = com.metfone.selfcare.util.Utilities.progressToTimer(seekBar.getProgress(), totalDuration);
                    videoView.seekTo(currentPosition);
                }
            }
        });
    }

    private void updateSeekBar() {
        if (videoView != null) {
            videoView.removeCallbacks(runnableSeekBar);
            videoView.postDelayed(runnableSeekBar, 1000L);
        }
    }

    private void stopUpdateSeekBar() {
        if (videoView != null) {
            videoView.removeCallbacks(runnableSeekBar);
        }
    }

    private void setViewSchedule() {
        boolean isSelected = ivSchedule.isSelected();

        if (!isSelected) {
            if (layoutScheduleTime != null)
                layoutScheduleTime.setVisibility(View.GONE);
        } else {
            if (tvWarningSchedule != null) tvWarningSchedule.setVisibility(View.GONE);
            if (layoutScheduleTime != null)
                layoutScheduleTime.setVisibility(View.VISIBLE);
            if (datePublished == null) {
                if (tvScheduleDate != null)
                    tvScheduleDate.setVisibility(View.INVISIBLE);
                if (tvScheduleTime != null)
                    tvScheduleTime.setVisibility(View.INVISIBLE);
                if (ivScheduleDate != null)
                    ivScheduleDate.setVisibility(View.INVISIBLE);
                if (ivScheduleTime != null)
                    ivScheduleTime.setVisibility(View.INVISIBLE);
            } else {
                if (tvScheduleDate != null) tvScheduleDate.setVisibility(View.VISIBLE);
                if (tvScheduleTime != null) tvScheduleTime.setVisibility(View.VISIBLE);
                if (ivScheduleDate != null) ivScheduleDate.setVisibility(View.VISIBLE);
                if (ivScheduleTime != null) ivScheduleTime.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Hien thi text date publish
     * Created_by @dchieu on 10/15/20
     */
    private void setTextScheduleDate() {
        if (datePublished == null) return;

        Calendar calendar = Calendar.getInstance(Constant.DEFAULT_LOCALE);
        calendar.setTime(datePublished);
        SimpleDateFormat df;
        if (tvScheduleDate != null) {
            tvScheduleDate.setVisibility(View.VISIBLE);
            df = new SimpleDateFormat("MMM dd, yyyy", Constant.DEFAULT_LOCALE);
            tvScheduleDate.setText(df.format(datePublished));
            if (ivScheduleDate != null)
                ivScheduleDate.setVisibility(View.VISIBLE);
        }
        if (tvScheduleTime != null) {
            tvScheduleTime.setVisibility(View.VISIBLE);
            df = new SimpleDateFormat("HH:mm", Constant.DEFAULT_LOCALE);
            tvScheduleTime.setText(df.format(datePublished));
            if (ivScheduleTime != null)
                ivScheduleTime.setVisibility(View.VISIBLE);
        }
    }

    /**
     * init các thông tin đã có của video
     * Created_by @dchieu on 10/14/20
     */
    private void initDataFromCurrentVideo() {
        editTitle.setText(currentVideo.title);
        editDesc.setText(currentVideo.description);

        currentCategory = new CategoryUploadModel(currentVideo.gameId, currentVideo.gameInfo == null ? "" : currentVideo.gameInfo.gameName);
        presenter.loadCategory(currentVideo.channel_id);

        if (currentVideo.publish_time != null) {
            ivSchedule.setSelected(true);

            setViewSchedule();

            if (currentVideo.publish_time > 0)
                datePublished = new Date(currentVideo.publish_time);

            setTextScheduleDate();
        }

        loadThumbSuccess(new ThumbVideoUpload(TYPE_FILE_URL, currentVideo.image_path_thumb));
    }

    private boolean checkValidInput() {
        if (editTitle != null && StringUtils.isEmpty(editTitle.getText())) {
            if (tvWarningTitle != null) tvWarningTitle.setVisibility(View.VISIBLE);
            editTitle.requestFocus();
            return false;
        }
        if (editDesc != null && StringUtils.isEmpty(editDesc.getText())) {
            if (tvWarningDesc != null) tvWarningDesc.setVisibility(View.VISIBLE);
            editDesc.requestFocus();
            return false;
        }
        if (spCategory != null) {
            long itemId = 0;
            try {
                itemId = currentCategory == null ? -1 : currentCategory.getId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (itemId <= 0) {
                if (tvWarningCategory != null) tvWarningCategory.setVisibility(View.VISIBLE);
                KeyboardUtils.hideSoftInput(getParentActivity());
                spCategory.requestFocus();
                return false;
            }
        }
        if (ivSchedule != null) {
            if (ivSchedule.isSelected()) {
                if (datePublished == null) {
                    if (tvWarningSchedule != null) {
                        tvWarningSchedule.setText(R.string.warning_upload_schedule_2);
                        tvWarningSchedule.setVisibility(View.VISIBLE);
                    }
                    if (layoutScheduleTime != null) layoutScheduleTime.requestFocus();
                    KeyboardUtils.hideSoftInput(getParentActivity());
                    return false;
                }
                if (tvWarningSchedule != null) {
                    tvWarningSchedule.setVisibility(View.GONE);
                }
            } else {
//                if (tvWarningSchedule != null) {
//                    tvWarningSchedule.setText(R.string.warning_upload_schedule);
//                    tvWarningSchedule.setVisibility(View.VISIBLE);
//                }
                ivSchedule.requestFocus();
                KeyboardUtils.hideSoftInput(getParentActivity());
//                return false;
            }
        }
        KeyboardUtils.hideSoftInput(getParentActivity());
        return true;
    }

    public void loadThumbSuccess(ThumbVideoUpload item) {
//        if (listThumbs == null) listThumbs = new ArrayList<>();
//        listThumbs.add(item);
        if (adapterThumb != null) {
            item.isSelect = true;
            adapterThumb.add(item);
            adapterThumb.notifyDataSetChanged();
        }
    }

    public void updateCategories(ArrayList<GameInfoEntity> list) {
        if (CollectionUtils.isNotEmpty(list)) {
            if (listCategories == null) listCategories = new ArrayList<>();
            else listCategories.clear();
            listCategories.add(new CategoryUploadModel(-1, getString(R.string.choose_game_category)));

            int currentPosition = 0;

            for (int i = 0; i < list.size(); i++) {
                GameInfoEntity item = list.get(i);

                if (item != null) {
                    listCategories.add(new CategoryUploadModel(item.id, item.gameName));

                    if (currentCategory != null && item.id == currentCategory.getId()) {
                        currentPosition = i + 1;
                    }
                }
            }

            spCategory.setSelection(currentPosition);

            if (adapterCategory != null) adapterCategory.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Constant.REQUEST_CODE.OPEN_GALLERY_CHOOSE_THUMB_UPLOAD_VIDEO) {
            try {
                if (data != null && data.getData() != null) {
                    File file = UriUtils.uri2File(data.getData());
                    ThumbVideoUpload item = new ThumbVideoUpload(ThumbVideoUpload.TYPE_FILE_PATH);
                    item.setFilePath(file.getPath());
//                    if (listThumbs == null) listThumbs = new ArrayList<>();
//                    listThumbs.add(item);
                    if (adapterThumb != null) {
                        if (CollectionUtils.isNotEmpty(adapterThumb.getData()))
                            for (ThumbVideoUpload datum : adapterThumb.getData()) {
                                datum.isSelect = false;
                            }
                        item.isSelect = true;
                        adapterThumb.add(1, item);
                        adapterThumb.notifyDataSetChanged();
//                        adapterThumb.notifyDataSetChanged();
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public void onEditVideoSuccess() {
        if (!isAdded()) return;
        if (btnEdit != null) btnEdit.setEnabled(true);
        showToast(R.string.msg_edit_video_success);
        onClickBack();
    }

    public void onEditVideoFailed() {
        if (!isAdded()) return;
        if (btnEdit != null) btnEdit.setEnabled(true);
        showToast(R.string.msg_edit_video_failed);
    }
}

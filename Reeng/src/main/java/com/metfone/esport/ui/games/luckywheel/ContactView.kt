package com.metfone.esport.ui.games.luckywheel

import android.app.Activity
import com.metfone.selfcare.di.BaseView

interface ContactView: BaseView {
    fun activity(): Activity
    fun getContactsSuccess(contacts: List<ContactUiModel>)
    fun sendInviteSuccess()
    fun showError(message: String?)
}
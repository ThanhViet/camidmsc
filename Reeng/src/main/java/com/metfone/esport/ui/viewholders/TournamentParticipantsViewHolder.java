package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.repsonse.ParticipantResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TournamentParticipantsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.ivLogoTeam)
    AvatarView ivLogoTeam;

    @BindView(R.id.tvTeamName)
    AppCompatTextView tvTeamName;

    public TournamentParticipantsViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_tournament_participant, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

//    public TextTitleViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//        ButterKnife.bind(this, itemView);
//    }

    public void binData(ParticipantResponse entity) {

        if (entity == null) return;

        try {
            ivLogoTeam.setAvatarUrl(entity.avatar);

            tvTeamName.setText(entity.name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

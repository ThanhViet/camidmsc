package com.metfone.esport.ui.live;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.donate.ContainerActivity;
import com.metfone.esport.donate.fragment.DonateDialogOtpVerifyFragment;
import com.metfone.esport.donate.fragment.DonateFragment;
import com.metfone.esport.donate.fragment.DonateHomeDialog;
import com.metfone.esport.customview.player.Jzvd;
import com.metfone.esport.donate.fragment.DonateNotifyDialog;
import com.metfone.esport.entity.camid.CamIdUserResponse;
import com.metfone.esport.entity.enums.MyListVideoActionEnum;
import com.metfone.esport.entity.event.ChangeAccountEvent;
import com.metfone.esport.entity.event.ChangeVideoEvent;
import com.metfone.esport.entity.event.CreateChannelEvent;
import com.metfone.esport.entity.event.OnClickCommentUploadEvent;
import com.metfone.esport.entity.event.OnLikeVideoEvent;
import com.metfone.esport.entity.event.OnLoginChannel;
import com.metfone.esport.entity.event.OnSendCommentEvent;
import com.metfone.esport.entity.event.OnSendCommentLiveEvent;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.entity.repsonse.UserInfoEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.network.donate.DonateESportApiCallback;
import com.metfone.esport.network.donate.DonateESportClient;
import com.metfone.esport.network.donate.request.SubDonateGiftPackageRequest;
import com.metfone.esport.network.donate.response.DonatePackageModel;
import com.metfone.esport.network.donate.response.WsDonateGiftPackageResponse;
import com.metfone.esport.network.socket.SocketManager;
import com.metfone.esport.ui.account.SwitchAccountDialog;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.mylists.mylistchanels.adapters.MyListChanelDecoration;
import com.metfone.esport.ui.mylists.mylistvideos.adapters.MyListVideoAdapter;
import com.metfone.esport.ui.viewholders.HalfVideoWithOptionViewHolder;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.metfoneplus.activity.MPAddPhoneActivity;
import com.metfone.selfcare.util.ImageUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import iknow.android.utils.KeyboardUtil;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

import static com.metfone.esport.common.Common.handleException;
import static com.metfone.esport.common.Common.slideDown;
import static com.metfone.esport.common.Common.slideUp;
import static com.metfone.esport.donate.fragment.DonateFragment.isRequireChangeNumber;
import static com.metfone.esport.entity.enums.MyListVideoActionEnum.SHARE;
import static com.metfone.esport.ui.live.LiveVideoFragment.PRE_CODE_THANKMESSAGE;

/**
 * Created by Nguyễn Thành Chung on 10/11/20.
 */
public class BottomFragment extends BaseFragment<BottomPresenter>
    implements DonateHomeDialog.OnClickDonateHomeListener, DonateDialogOtpVerifyFragment.ButtonOnClickListener, DonateNotifyDialog.OnClickNotify {
    @BindView(R.id.ivLogoChannel)
    AvatarView ivLogoChannel;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.ivFavorite)
    AppCompatImageView ivFavorite;
    @BindView(R.id.tvTitleVideo)
    TextView tvTitleVideo;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.layout_item_info_video_bottom)
    CardView layoutItemInfoVideoBottom;
    @BindView(R.id.tvSendCommentChannel)
    TextView tvSendCommentChannel;
    @BindView(R.id.iv_avatar)
    AvatarView ivAvatar;
    @BindView(R.id.lnSwitchAccountCommentNormal)
    LinearLayout lnSwitchAccountCommentNormal;
    @BindView(R.id.rlCommentNormal)
    RelativeLayout rlCommentNormal;
    @BindView(R.id.rvRecommendVideo)
    RecyclerView rvRecommendVideo;
    @BindView(R.id.lnUploaded)
    LinearLayout lnUploaded;
    @BindView(R.id.rcvComment)
    RecyclerView rcvComment;
    @BindView(R.id.tvCommentEmptyLive)
    TextView tvCommentEmptyLive;
    @BindView(R.id.rlComment)
    RelativeLayout rlComment;
    @BindView(R.id.tvCommentEmptyNormal)
    TextView tvCommentEmptyNormal;
    @BindView(R.id.ivHideComment)
    AppCompatImageView ivHideComment;
    @BindView(R.id.layout_switch_account)
    LinearLayout lnSwitchAccount;
    @BindView(R.id.iv_send_comment)
    AppCompatImageView ivSendComment;
    @BindView(R.id.iv_sticker)
    AppCompatImageView ivSticker;
    @BindView(R.id.iv_donate)
    AppCompatImageView ivDonate;
    @BindView(R.id.edComment)
    EditText edComment;
    @BindView(R.id.edit_comment)
    EmojiconEditText editComment;
    @BindView(R.id.rlBottomComment)
    CardView rlBottomComment;
    @BindView(R.id.ivAvatarComment)
    AvatarView ivAvatarComment;
    @BindView(R.id.tvRecommendVideo)
    TextView tvRecommendVideo;
    @BindView(R.id.layout_recommend_video)
    View layoutRecommendVideo;
    @BindView(R.id.rlBottom)
    RelativeLayout rlBottom;
    @BindView(R.id.freshLayout)
    SwipeRefreshLayout freshLayout;
    @BindView(R.id.viewLogin)
    View viewLogin;

    private VideoInfoResponse modelData;
    private ArrayList<ExtKeyPair<?, String>> actionList = new ArrayList<>();
    private ArrayList<VideoInfoResponse> items = new ArrayList<>();
    private LiveCommentAdapter adapter;
    private MyListVideoAdapter myListVideoAdapter;
    private boolean isLoading = true;
    private String rowStart = "";
    public static final int TYPE_UPLOAD = 1;
    public static final int TYPE_BROADCAST = 2;
    private String categoryName = "";

    public static BottomFragment newInstance(VideoInfoResponse modelData, DonateListener listener) {
        BottomFragment fragment = new BottomFragment();
        fragment.modelData = modelData;
        fragment.donateListener = listener;
        return fragment;
    }

    @Override
    public BottomPresenter getPresenter() {
        return new BottomPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_bottom;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    //ham kiem tra video o trang thai nao : live, upload,pastbroadcast
    private void processStatusPlayer() {
        try {
            if (modelData != null) {
                if (modelData.isLive()) {
                    lnUploaded.setVisibility(View.GONE);
                    showListComment();
                    setEmptyCommentLive();
                    rlBottomComment.setVisibility(View.VISIBLE);
                    ivDonate.setVisibility(View.VISIBLE);
                } else {
                    lnUploaded.setVisibility(modelData.type_video == TYPE_UPLOAD ? View.VISIBLE : View.GONE);
                    rlComment.setVisibility(modelData.type_video == TYPE_UPLOAD ? View.GONE : View.VISIBLE);
                    rcvComment.setVisibility(modelData.type_video == TYPE_UPLOAD ? View.GONE : View.VISIBLE);
                    rlBottomComment.setVisibility(modelData.type_video == TYPE_UPLOAD ? View.GONE : View.VISIBLE);
                    tvCommentEmptyNormal.setVisibility(View.GONE);
                    tvCommentEmptyLive.setVisibility(modelData.type_video == TYPE_UPLOAD ? View.GONE : View.VISIBLE);
                    ivDonate.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            handleException(e);
        }
    }


    private View view;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        initView();
    }

    public void initView() {
        if (isLogin())viewLogin.setVisibility(View.GONE);
        else viewLogin.setVisibility(View.VISIBLE);
        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);
            initRecyclerViewComment();
            initRecyclerRecommendVideo();
            if (modelData != null) {
                processStatusPlayer();
                if (!modelData.isLive()) {
                    getData();
                }
            }

            if (modelData != null) {
                if (modelData.channelInfo != null) {
                    if (modelData.channelInfo.getName() != null) {
                        categoryName = modelData.channelInfo.getName();
                    }
                }
            }

            freshLayout.setOnRefreshListener(() -> {
                if (modelData.isLive()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            freshLayout.setRefreshing(false);
                        }
                    }, 1500);
                } else {
                    adapter.clear();
                    rowStart = "";
                    getData();
                }
            });
            if (categoryName == null) categoryName = "";
            lnSwitchAccount.setVisibility(isLogin() ? View.VISIBLE : View.GONE);
//            editComment.setFocusable(isLogin());
            UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
            UserInfo currentUser = userInfoBusiness.getUser();
            String name = !SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false) ? currentUser.getFull_name() : AccountBusiness.getInstance().getChannelInfo().name;
            if (isLogin()) {
                editComment.setHint(getString(R.string.send_a_message, name));
            } else
                editComment.setHint(getString(R.string.send_a_message_hint));
            ivLogoChannel.setOnClickListener(v -> {
                AloneFragmentActivity.with(getActivity())
                    .parameters(ChannelContainerFragment.newBundle(modelData.channelInfo.id))
                    .start(ChannelContainerFragment.class);
            });
//            edComment.setEnabled(isLogin());
//            editComment.setEnabled(isLogin());
//            ivSendComment.setEnabled(isLogin());
//            ivSticker.setEnabled(isLogin());
            if (isLogin()) {
                EmojIconActions emojIcon = new EmojIconActions(getContext(), view, editComment, ivSticker);
                emojIcon.ShowEmojIcon();
            } else {
                ivSticker.setOnClickListener(v -> {
                    showDialogLogin();
                });
            }
            if (!isLogin()) {
                rlBottom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BottomFragment.this.showDialogLogin();
                    }
                });
            } else {
                rlBottom.setVisibility(View.INVISIBLE);
            }
            setUpAvatar();
            ivAvatarComment.setVisibility(isLogin() ? View.VISIBLE : View.INVISIBLE);
            editComment.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0 && !s.toString().trim().isEmpty()) {
                        ivSendComment.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
                        ivSendComment.setEnabled(true);
                    } else {
                        ivSendComment.setEnabled(false);
                        ivSendComment.setColorFilter(ContextCompat.getColor(getContext(), R.color.color_icon_send));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            KeyboardUtils.hideKeyboardWhenTouch(lnUploaded, getParentActivity());
            KeyboardUtils.hideKeyboardWhenTouch(layoutItemInfoVideoBottom, getParentActivity());
            KeyboardUtils.hideKeyboardWhenTouch(rcvComment, getParentActivity());
            KeyboardUtils.hideKeyboardWhenTouch(rlComment, getParentActivity());
//            KeyboardUtils.hideKeyboardWhenTouch(ivSendComment, getParentActivity());
            actionList.clear();
            actionList.add(new ExtKeyPair<>(SHARE, getString(SHARE.textId), SHARE.iconId));
//            view.setPadding(0,0,0,Utilities.getHeightNavigationDevice(getContext()));
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void initRecyclerRecommendVideo() {
        try {
            myListVideoAdapter = new MyListVideoAdapter(getParentActivity(), items, new HalfVideoWithOptionViewHolder.IOnClickItem() {
                @Override
                public void onClickItem(VideoInfoResponse entity) {
                    modelData = entity;
                    presenter.getDetailVideo(modelData.link);
                    EventBus.getDefault().post(new ChangeVideoEvent(entity));
                }

                @Override
                public void onClickOption(VideoInfoResponse entity) {
                    new SingleSelectBottomSheet()
                        .setOptions(actionList)
                        .setOnClickItem(e -> {
                            if (e.key instanceof MyListVideoActionEnum)
                                switch ((MyListVideoActionEnum) e.key) {
                                    case SHARE:
                                        onClickShare(entity.link);
                                        break;
                                    case REMOVE_FROM_LIST:
                                        //TODO call api
                                        break;
                                    default:
                                        break;
                                }
                        })
                        .show(getChildFragmentManager(), null);
                }
            });
            myListVideoAdapter.setShowOption();
            rvRecommendVideo.setLayoutManager(new LinearLayoutManager(getParentActivity(), RecyclerView.VERTICAL, false));
            rvRecommendVideo.setHasFixedSize(true);
            rvRecommendVideo.addItemDecoration(new MyListChanelDecoration(getContext()));
            rvRecommendVideo.setAdapter(myListVideoAdapter);
            layoutRecommendVideo.setVisibility(View.GONE);
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void initRecyclerViewComment() {
        try {
            rcvComment.setLayoutManager(new LinearLayoutManager(getContext()));
            rcvComment.setHasFixedSize(true);
            adapter = new LiveCommentAdapter(getContext(), LiveCommentAdapter.PORTRAIT);
            adapter.setData(new ArrayList<>());
            rcvComment.setAdapter(adapter);
            rcvComment.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rcvComment.getLayoutManager();
                    if (isLoading && !modelData.isLive()) {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == adapter.getData().size() - 3) {
                            getData();
                        }
                    }
                }
            });
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void getData() {
        try {
            presenter.getComments(String.valueOf(modelData.link), rowStart);
        } catch (Exception e) {
            handleException(e);
        }
    }

    @OnClick({R.id.layout_switch_account, R.id.lnUploaded, R.id.ivHideComment, R.id.iv_send_comment,
        R.id.ivFavorite, R.id.layoutRoot, R.id.edit_comment, R.id.iv_donate})
    public void onClickView(View view) {
        try {
            switch (view.getId()) {
                case R.id.layout_switch_account:
                    if (isLogin()) {
                        new SwitchAccountDialog().show(getChildFragmentManager());
                    }
                    break;
                case R.id.lnUploaded:
                    slideDown(lnUploaded);
                    lnUploaded.setVisibility(View.GONE);
                    rlCommentNormal.setVisibility(View.GONE);
                    new Handler().postDelayed(() -> {

                        if (rcvComment != null) rcvComment.setVisibility(View.VISIBLE);
                        if (rlBottomComment != null) rlBottomComment.setVisibility(View.VISIBLE);
                        if (ivHideComment != null) ivHideComment.setVisibility(View.VISIBLE);
                        if (layoutRecommendVideo != null)
                            layoutRecommendVideo.setVisibility(View.GONE);

                        showListComment();
                    }, 500);

                    new Handler().postDelayed(() -> {
                        if (adapter == null || CollectionUtils.isEmpty(adapter.getData())) {
                            if (tvCommentEmptyNormal != null)
                                tvCommentEmptyNormal.setVisibility(View.VISIBLE);
                        } else {
                            if (tvCommentEmptyNormal != null)
                                tvCommentEmptyNormal.setVisibility(View.GONE);
                        }
                    }, 700);
                    break;
                case R.id.ivHideComment:
                    slideUp(lnUploaded);
                    rlCommentNormal.setVisibility(View.VISIBLE);
                    tvCommentEmptyNormal.setVisibility(View.GONE);
                    lnUploaded.setVisibility(View.VISIBLE);
                    rcvComment.setVisibility(View.GONE);
                    rlBottomComment.setVisibility(View.GONE);
                    ivHideComment.setVisibility(View.GONE);
                    if (myListVideoAdapter == null || CollectionUtils.isEmpty(myListVideoAdapter.getData())) {
                        tvRecommendVideo.setVisibility(View.GONE);
                    } else {
                        tvRecommendVideo.setVisibility(View.VISIBLE);
                    }
                    KeyboardUtil.hideKeyboard(getParentActivity(), edComment);
                    if (layoutRecommendVideo != null)
                        layoutRecommendVideo.setVisibility(View.VISIBLE);
                    break;
                case R.id.iv_send_comment:
                    if (isLogin()) {
                        processSendComment();
                    } else {
                        showDialogLogin();
                    }
                    break;
                case R.id.ivFavorite:
                    processFavoriteVideo();
                    break;
                case R.id.layoutRoot:
                    break;
                case R.id.edit_comment:
                    if (!isLogin()) {
                        showDialogLogin();
                    } else {
//                        editComment.setFocusable(true);
//                        editComment.requestFocus();
                        Utilities.showKeyBoard(getContext(), editComment);
//                        editComment.requestFocus();
//                        KeyboardUtils.showSoftInput();
                    }
                    break;
                case R.id.iv_donate:
//                    sendCommentSuccess(PRE_CODE_THANKMESSAGE + "_Thank for donate_Hi guy");
                    showDonateHome();
                    break;
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void processFavoriteVideo() {
        try {
            if (isLogin()) {
                if (!modelData.isLike()) {
                    modelData.is_like = 1;
                    ivFavorite.setImageResource(R.drawable.ic_favorite_red_es);
                } else {
                    modelData.is_like = 0;
                    ivFavorite.setImageResource(R.drawable.ic_favorite_white_es);
                }
                presenter.likeVideo(modelData.is_like, modelData);
                EventBus.getDefault().post(new OnLikeVideoEvent(modelData));
            } else {
                showDialogLogin();
            }
        } catch (Exception e) {
            handleException(e);
        }
    }


    private void processSendComment() {
        try {
            String content = StringUtils.trimText(editComment);
            if (StringUtils.isNotEmpty(content)) {
                showListComment();
                if (modelData.isLive()) {
                    presenter.sendComment(content, modelData);
                    tvCommentEmptyLive.setVisibility(View.GONE);
                } else {
                    tvCommentEmptyNormal.setVisibility(View.GONE);
                    if (adapter != null && adapter.getData() != null) {
                        showListComment();
                        CamIdUserResponse camIdUserResponse = AccountBusiness.getInstance().getUserInfo();
                        UserInfoEntity userInfoEntity = new UserInfoEntity();
                        if (camIdUserResponse == null) {
                            userInfoEntity.avatar = "";
                            userInfoEntity.name = "";
                        } else {
                            if (AccountBusiness.getInstance().getChannelInfo() == null) {
                                userInfoEntity.avatar = camIdUserResponse.avatar == null ? "" : camIdUserResponse.avatar;
                                userInfoEntity.name = camIdUserResponse.full_name == null ? "" : camIdUserResponse.full_name;
                            } else {
                                if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                                    userInfoEntity.avatar = camIdUserResponse.avatar == null ? "" : camIdUserResponse.avatar;
                                    userInfoEntity.name = camIdUserResponse.full_name == null ? "" : camIdUserResponse.full_name;
                                } else {
                                    userInfoEntity.avatar = AccountBusiness.getInstance().getChannelInfo().urlAvatar;
                                    userInfoEntity.name = AccountBusiness.getInstance().getChannelInfo().name;
                                }

                            }
                        }

                        ListCommentEntity entity = new ListCommentEntity(userInfoEntity, content);
                        adapter.setData(entity);
                    }
                    presenter.sendCommentPastBroadcast(content, modelData);
                }
            }
            editComment.setText("");
            tvCommentEmptyLive.setVisibility(View.GONE);
            tvCommentEmptyNormal.setVisibility(View.GONE);
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();

            Common.hideKeyBoard(getContext(), editComment);
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void onSuccessCommentBroadcast() {

    }

    public void onErrorCommentBroadcast(String error) {

    }

    String channelID = "";
    String channelName = "";

    public void onSuccessGetVideoDetail(DetailVideoResponse response) {
        try {
            channelID = response.channelInfo.id + "";
            channelName = response.channelInfo.name;
            tvTitleVideo.setText(response.title);
            if (response.type_video == TYPE_UPLOAD) {
                presenter.getVideoRelated(modelData.id, modelData.title, String.valueOf(modelData.channel_id), String.valueOf(modelData.gameId));
            }
            modelData.type_video = response.type_video;
            processStatusPlayer();
            modelData.isLive = response.isLive;
            if (response.isLiveVideo()) {
                initSocket();
            }
            if (response.isLike == 0) {
                ivFavorite.setImageResource(R.drawable.ic_favorite_white_es);
            } else {
                ivFavorite.setImageResource(R.drawable.ic_favorite_red_es);
            }
            if (response.channelInfo != null) {
                tvChannel.setText(response.channelInfo.getName());
                tvSendCommentChannel.setText(getString(R.string.send_a_message, response.channelInfo.getName()));
                ivLogoChannel.setAvatarUrl(response.channelInfo.getUrlAvatar());

            }
            if (response.gameInfoEntity != null) {
                tvDescription.setText(response.gameInfoEntity.gameName == null ? "" : response.gameInfoEntity.gameName);
                tvCommentEmptyNormal.setText(getString(R.string.empty_comment, response.gameInfoEntity.gameName == null ? "" : response.gameInfoEntity.gameName));
                tvCommentEmptyLive.setText(getString(R.string.empty_comment, response.gameInfoEntity.gameName == null ? "" : response.gameInfoEntity.gameName));
            } else {
                tvCommentEmptyNormal.setText(getString(R.string.empty_comment, categoryName));
                tvCommentEmptyLive.setText(getString(R.string.empty_comment, categoryName));
            }
            if (adapter != null) {
                if (CollectionUtils.isNotEmpty(adapter.getData())) {
                    tvCommentEmptyNormal.setVisibility(View.GONE);
                    tvCommentEmptyLive.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void initSocket() {
        try {
            SocketManager.getInstance().disconnect();
            SocketManager.getInstance().connectWebSocket(Constant.WS_ESPORT, String.valueOf(modelData.id));
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void onFailGetVideoDetail(String error) {
        freshLayout.setRefreshing(false);
    }

    @Override
    public void onDestroyView() {
        Jzvd.releaseAllVideos();
        super.onDestroyView();
    }

    public void onSuccessGetVideoRelated(ArrayList<VideoInfoResponse> responses) {
        try {
            if (myListVideoAdapter != null) myListVideoAdapter.setNewData(responses);
            if (CollectionUtils.isNotEmpty(responses)) {
                if (rvRecommendVideo != null) rvRecommendVideo.scrollToPosition(0);
                if (layoutRecommendVideo != null) layoutRecommendVideo.setVisibility(View.VISIBLE);
            } else {
                if (layoutRecommendVideo != null) layoutRecommendVideo.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    public void onSuccessCommentList(List<ListCommentEntity> commentEntities) {
        try {
            freshLayout.setRefreshing(false);
            if (modelData.isLive()) {
                tvCommentEmptyLive.setVisibility(CollectionUtils.isEmpty(commentEntities) ? View.VISIBLE : View.GONE);
            }

            if (CollectionUtils.isNotEmpty(commentEntities)) {
                if (!modelData.isLive()) {
                    Collections.sort(commentEntities, new Comparator<ListCommentEntity>() {
                        @Override
                        public int compare(ListCommentEntity o1, ListCommentEntity o2) {
                            return o2.currentTime.compareTo(o1.currentTime);
                        }
                    });
                }
                isLoading = commentEntities.size() == 20;
                rowStart = commentEntities.get(commentEntities.size() - 1).base64RowID;
                adapter.setData(commentEntities);
                tvCommentEmptyLive.setVisibility(View.GONE);
            } else {
                isLoading = false;
                if (!modelData.isLive()) {
                    tvCommentEmptyLive.setVisibility(modelData.type_video == TYPE_UPLOAD ? View.GONE : View.VISIBLE);
                }
            }
            if (CollectionUtils.isNotEmpty(adapter.getData())) {
                tvCommentEmptyLive.setVisibility(View.GONE);
                tvCommentEmptyNormal.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            handleException(e);
        }

    }

    public void setEmptyCommentLive() {
        try {
            tvCommentEmptyLive.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void onReceiverMessageLive(ListCommentEntity model) {
        try {
            freshLayout.setRefreshing(false);
            rcvComment.setVisibility(View.VISIBLE);
            UserInfoEntity userInfoEntity = new UserInfoEntity();
            userInfoEntity.avatar = model.getAvatar();
            userInfoEntity.name = model.getNameSender();
            if (adapter == null) {
                initRecyclerViewComment();
            }
            adapter.add(new ListCommentEntity(userInfoEntity, model.getContent()));
            adapter.notifyDataSetChanged();
            tvCommentEmptyLive.setVisibility(View.GONE);
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void onSuccessComment(ListCommentEntity commentEntity) {
        try {
            freshLayout.setRefreshing(false);
            showListComment();
            if (modelData == null) return;
            if (adapter != null && commentEntity != null) {
                UserInfoEntity entity = new UserInfoEntity();
                entity.name = commentEntity.getNameSender();
                entity.avatar = commentEntity.getAvatar();
                adapter.setData(commentEntity);
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void showListComment() {
        try {
            rlComment.setVisibility(View.VISIBLE);
            rcvComment.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void setUpAvatar() {
        try {
            if (!isLogin()) return;
            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getParentActivity());
                UserInfo currentUser = userInfoBusiness.getUser();
                if (TextUtils.isEmpty(currentUser.getAvatar())) {
                    showAvatarCamid();
                } else {
                    Glide.with(this)
                        .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                        .transform(new MultiTransformation<Bitmap>(new CenterCrop(), new CircleCrop()))
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivAvatar);

                    Glide.with(this)
                        .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                        .transform(new MultiTransformation<Bitmap>(new CenterCrop(), new CircleCrop()))
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivAvatarComment);
                }
            } else {
                if (AccountBusiness.getInstance().getChannelInfo() != null) {
                    if (StringUtils.isNotEmpty(AccountBusiness.getInstance().getChannelInfo().urlAvatar)) {
                        ivAvatar.setAvatarUrl(AccountBusiness.getInstance().getChannelInfo().urlAvatar);
                        ivAvatarComment.setAvatarUrl(AccountBusiness.getInstance().getChannelInfo().urlAvatar);
                    } else {
                        ivAvatar.setImageDrawable(ContextCompat.getDrawable(getParentActivity(), R.drawable.ic_avatar_member));
                        ivAvatarComment.setImageDrawable(ContextCompat.getDrawable(getParentActivity(), R.drawable.ic_avatar_member));
                    }
                } else {
                    showAvatarCamid();
                }
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void showAvatarCamid() {
        try {
            AccountRankDTO account = ApplicationController.self().getAccountRankDTO();
            KhUserRank myRank = KhUserRank.getById(account.rankId);
            Glide.with(this)
                .load(myRank.resAvatar)
                .centerCrop()
                .placeholder(R.drawable.ic_avatar_member)
                .error(R.drawable.ic_avatar_member)
                .into(ivAvatar);
            Glide.with(this)
                .load(myRank.resAvatar)
                .centerCrop()
                .placeholder(R.drawable.ic_avatar_member)
                .error(R.drawable.ic_avatar_member)
                .into(ivAvatarComment);
        } catch (Exception e) {
            handleException(e);
        }
    }


    @Subscribe
    public void onEvent(ChangeAccountEvent event) {
        try {
            setUpAvatar();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    @Subscribe
    public void onSaveChannel(OnLoginChannel event) {
        try {
            lnSwitchAccount.setVisibility(View.VISIBLE);
            setUpAvatar();
            rlBottomComment.setEnabled(false);
            edComment.setEnabled(true);
        } catch (Exception e) {
            handleException(e);
        }
    }

    @Subscribe
    public void OnSendCommentEvent(OnSendCommentEvent event) {
        try {
            CamIdUserResponse camIdUserResponse = AccountBusiness.getInstance().getUserInfo();
            UserInfoEntity userInfoEntity = new UserInfoEntity();
            if (camIdUserResponse == null) {
                userInfoEntity.avatar = "";
                userInfoEntity.name = "";
            } else {
                userInfoEntity.avatar = camIdUserResponse.avatar == null ? "" : camIdUserResponse.avatar;
                userInfoEntity.name = camIdUserResponse.full_name == null ? "" : camIdUserResponse.full_name;
            }

            ListCommentEntity entity = new ListCommentEntity(userInfoEntity, event.comment);
            adapter.setData(entity);
            tvCommentEmptyLive.setVisibility(View.GONE);
            tvCommentEmptyNormal.setVisibility(View.GONE);
        } catch (Exception e) {
            handleException(e);
        }
    }

    @Subscribe
    public void onCreateChannelEvent(CreateChannelEvent event) {
        try {
            setUpAvatar();
        } catch (Exception e) {
            handleException(e);
        }
    }

    @Subscribe
    public void OnSendCommentLiveEvent(OnSendCommentLiveEvent event) {
        try {
            presenter.sendComment(event.comment, modelData);
            tvCommentEmptyLive.setVisibility(View.GONE);
        } catch (Exception e) {
            handleException(e);
        }
    }

    @Subscribe
    public void OnClickCommentUploadEvent(OnClickCommentUploadEvent event) {
        try {
            lnUploaded.setVisibility(View.GONE);
            showListComment();
            rlBottomComment.setVisibility(View.VISIBLE);
            editComment.requestFocus();
            KeyboardUtils.showSoftInput();
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void wsDonateGiftPackage(DonatePackageModel packageModel, String cmt, String number, String otp) {
        String camId = "";
        String cusName = "";
        if (isLogin()) {
            UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getParentActivity());
            UserInfo currentUser = userInfoBusiness.getUser();
            camId = currentUser.getUser_id() + "";
            cusName = currentUser.getFull_name();
        }
        SubDonateGiftPackageRequest param = new SubDonateGiftPackageRequest();
        param.setComment(cmt);
        param.setDonatePackageId(packageModel.getId() + "");
        param.setCamId(camId); //user_id
        param.setCustomerName(cusName);
        param.setPhoneNumber(number);
        param.setChannelId(channelID); //streamer
        param.setChannelName(channelName); //streamer
        param.setPaymentMethod(packageModel.getPaymentMethod());
        param.setOtp(otp);
        if (packageModel.getUnit().equals("USD")) {
        } else {
            param.setPhoneNumber("");
            param.setOtp("");
        }
        new DonateESportClient().wsDonateGiftPackage(param, new DonateESportApiCallback<WsDonateGiftPackageResponse>() {
            @Override
            public void onResponse(Response<WsDonateGiftPackageResponse> response) {
                if (response.body() != null && response.body().isSuccessful()) {
                    if (response.body().getResult().getErrorCode().equals("0") && response.body().getResult().getWsResponse() != null) {
                        for (Fragment f : getChildFragmentManager().getFragments()) {
                            if (f instanceof DonateDialogOtpVerifyFragment) {
                                ((DonateDialogOtpVerifyFragment) f).dismissOTP();
                                break;
                            }
                        }
                        if (!isLogin()) {
                            if (donateListener != null)
                                donateListener.onDonateSuccessNotLogin(packageModel.getCoin(), cmt);
                        } else
                            sendCommentSuccess(PRE_CODE_THANKMESSAGE + "_" + response.body().getResult().getWsResponse().getThankMessage() + "_" + cmt);
                        return;
                    }
                    if (response.body().getResult().getErrorCode().equals("1") && response.body().getResult().getWsResponse() != null) {
                        if (response.body().getResult().getWsResponse().getDescription().contains("over.otp")) {
                            remainTimeOverOTP = response.body().getResult().getWsResponse().getRemainTime();
                            timeStampOverOTP = System.currentTimeMillis();
                            SPUtils.getInstance().put("remaintime_" + numberFormat(number), timeStampOverOTP);

                        }
                        go2NotSuccess(response.body().getResult().getWsResponse().getDescription(), packageModel, cmt, number);
                    } else {
                        go2NotSuccess(response.body().getResult().getErrorCode(), packageModel, cmt, number);
                    }

                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    public long numberFormat(String number) {
        if (number.equals("")) return 0;
        return Long.parseLong(number);
    }

    String remainTimeOverOTP = "0";
    long timeStampOverOTP = 0;

    private void go2NotSuccess(String description, DonatePackageModel packageModel, String cmt, String number) {
        String desc = description.toLowerCase().trim();
        if (desc.contains("over.otp")) {
            for (Fragment f : getChildFragmentManager().getFragments()) {
                if (f instanceof DonateDialogOtpVerifyFragment) {
                    ((DonateDialogOtpVerifyFragment) f).dismissOTP();
                    break;
                }
            }
            DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_OVER_OTP, this);
            if (remainTimeOverOTP != null && !remainTimeOverOTP.equals(""))
                dialog.setRemainTime(Integer.parseInt(remainTimeOverOTP));
            dialog.show(getChildFragmentManager(), "");

        } else if (desc.contains("otp")) {
            for (Fragment f : getChildFragmentManager().getFragments()) {
                if (f instanceof DonateDialogOtpVerifyFragment) {
                    ((DonateDialogOtpVerifyFragment) f).invalidateOTP();
                    break;
                }
            }
        } else if (desc.contains("point")) {
            for (Fragment f : getChildFragmentManager().getFragments()) {
                if (f instanceof DonateDialogOtpVerifyFragment) {
                    ((DonateDialogOtpVerifyFragment) f).dismissOTP();
                    break;
                }
            }
            DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_NOT_ENOUGH_POINT, this);
            dialog.show(getChildFragmentManager(), "");
        } else if (desc.contains("balance")) {
            for (Fragment f : getChildFragmentManager().getFragments()) {
                if (f instanceof DonateDialogOtpVerifyFragment) {
                    ((DonateDialogOtpVerifyFragment) f).dismissOTP();
                    break;
                }
            }
            DonateNotifyDialog dialog;
            if (isRequireChangeNumber) {
                dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_CHANGE_NUMBER, this);
                dialog.setInput(packageModel, number, cmt);
            } else {
                dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_NOT_ENOUGH_BALANCE, this);
            }
            dialog.show(getChildFragmentManager(), "");

        } else if (!desc.equals("1")) {    // case: errorCode # {0, 1}
            for (Fragment f : getChildFragmentManager().getFragments()) {
                if (f instanceof DonateDialogOtpVerifyFragment) {
                    ((DonateDialogOtpVerifyFragment) f).dismissOTP();
                    break;
                }
            }
            DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_SYSTEM_ERROR, this);
            dialog.show(getChildFragmentManager(), "");
        }
    }

    @Override
    public void onClickDonate(DonatePackageModel donatePackage, String cmt, String number, String otp) {
        wsDonateGiftPackage(donatePackage, cmt, number, otp);
    }

    @Override
    public void onClickBackFromOTP(DonatePackageModel donatePackage, String cmt, String number) {
        if (donateListener != null) donateListener.onClickOpen();
        DonateHomeDialog dialog = DonateHomeDialog.newInstance(this);
        dialog.setInput(donatePackage, number, cmt, true, false);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickCancelNotify() {
        System.out.println(TAG + " thanhlv onClickCancelNotify");
    }

    @Override
    public void onClickCancelConfirmDonatePoint(DonatePackageModel donatePackageModel, String cmt) {
        if (donateListener != null) donateListener.onClickOpen();
        DonateHomeDialog dialog = DonateHomeDialog.newInstance(this);
        dialog.go2Point(donatePackageModel, cmt);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickGo2TopUp() {
        Intent intent = new Intent(getContext(), ContainerActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClickGo2AddPhone() {
        Intent intent = new Intent(getContext(), MPAddPhoneActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClickGo2DonatePoint(DonatePackageModel donatePackageModel, String cmt) {
        wsDonateGiftPackage(donatePackageModel, cmt, "", "");
    }

    @Override
    public void onClickContinue(DonatePackageModel donatePackageModel, String number, String cmt) {
        if (donatePackageModel == null) return;
        if (donateListener != null) donateListener.onClickOpen();
        DonateHomeDialog dialog = DonateHomeDialog.newInstance(this);
        dialog.setInput(donatePackageModel, number, cmt, true, true);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickDonateGo2NotMetfone(DonatePackageModel donatePackageModel, String number, String cmt) {
        DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(
            (DonateFragment.isDefaultNumberAcc ? DonateNotifyDialog.NOTIFY_NOT_METFONE_NUMBER_DEFAULT
                : DonateNotifyDialog.NOTIFY_NOT_METFONE_NUMBER_INPUT), this);
        if (!DonateFragment.isDefaultNumberAcc) {
            dialog.setInput(donatePackageModel, number, cmt);
        }
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickDonateGo2RequireAddPhone(DonatePackageModel donatePackage, String cmt) {
        DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_ADD_PHONE, this);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickDonateGo2RequireNumber(DonatePackageModel donatePackage, String cmt) {
        DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_REQUIRE_NUMBER, this);
        dialog.setInput(donatePackage, "", cmt);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickDonateGo2DonateByPoints(DonatePackageModel donatePackage, String cmt) {
        DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_CONFIRM_DONATE_POINT, this);
        dialog.setInput(donatePackage, "", cmt);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickDonateGo2VerifyOTP(DonatePackageModel donatePackage, String number, String cmt) {
        DonateDialogOtpVerifyFragment dialog = new DonateDialogOtpVerifyFragment();
        dialog.setDonatePackage(donatePackage, number, cmt);
        dialog.addButtonOnClickListener(this);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickDonateGo2WaitOTP(DonatePackageModel donatePackage, String number, String cmt, int remainTime) {
        DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_WAIT_GET_OTP, this);
        dialog.setRemainTime(remainTime);
        dialog.setInput(donatePackage, number, cmt);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onClickDonateGo2ReceiveOTP() {
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());
        Task task = client.startSmsUserConsent(null);
        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    @OnClick(R.id.viewLogin)
    public void onClickLogin(){
        showDialogLogin();
    }

    @Override
    public void onClickDonateGo2Login(DonatePackageModel donatePackage, String number) {
        showDialogLogin();
    }

    @Override
    public void onClickDonateGo2Close() {
        if (donateListener != null) donateListener.onClickClose();
    }

    @Override
    public boolean onClickDonateGo2CheckOverOTP(String number) {
        long curTime = System.currentTimeMillis();
        long markTime = SPUtils.getInstance().getLong("remaintime_" + numberFormat(number), 0);
        if (curTime - markTime < 10 * 60 * 1000) {
            DonateNotifyDialog dialog = DonateNotifyDialog.newInstance(DonateNotifyDialog.NOTIFY_OVER_OTP, this);
            dialog.setRemainTime(10 * 60 - (int) (curTime - markTime) / 1000);
            dialog.show(getChildFragmentManager(), "");
            return true;
        }
        return false;
    }

    public void sendCommentSuccess(String donateComment) {
        try {
            showListComment();
            presenter.sendComment(donateComment, modelData);
            tvCommentEmptyLive.setVisibility(View.GONE);
        } catch (Exception e) {
            handleException(e);
        }
    }

    public interface DonateListener {
        void onClickOpen();

        void onClickClose();

        void onDonateSuccessNotLogin(int coin, String cmt);
    }

    private DonateListener donateListener;

    public void showDonateHome() {
        if (donateListener != null) donateListener.onClickOpen();
        DonateHomeDialog dialog = DonateHomeDialog.newInstance(this);
        dialog.show(getChildFragmentManager(), "");
    }
}

package com.metfone.esport.ui.mylists.mylistvideos;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.enums.SortEnum;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class MyListVideoPresenter extends BasePresenter<MyListVideoFragment, BaseModel> {
    private Disposable apiDispose = null;

    private boolean canLoadMore = false;

    public boolean isLoading() {
        return apiDispose != null && !apiDispose.isDisposed();
    }

    public boolean isCanLoadMore() {
        return canLoadMore;
    }

    public MyListVideoPresenter(MyListVideoFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(final int currentPage, SortEnum currentSort) {
        if (currentPage > 0 && isLoading()) return;

        if (currentPage == 0 && apiDispose != null)
            apiDispose.dispose();

        canLoadMore = false;

        int sortType = 1;

        switch (currentSort) {
//            case MANUAL:
//                sortType = 1;
//                break;
//            case MOST_POPULAR:
//                sortType = 2;
//                break;
            case DATE_ADDED_NEWEST:
                sortType = 1;
                break;
            case DATE_ADDED_OLDEST:
                sortType = 2;
                break;
        }

        apiDispose = RxUtils.async(
                compositeDisposable,
                apiService.myListVideo(currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, sortType, getDefaultParam()),
                new ICallBackResponse<ArrayList<VideoInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<VideoInfoResponse> response) {
                        if (view != null) view.onGetDataSuccess(response);

                        if (CollectionUtils.size(response) >= DEFAULT_LIMIT) canLoadMore = true;
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }

                    @Override
                    public void onFinish() {
                        if (apiDispose != null)
                            apiDispose.dispose();
                    }
                });
    }

    public void likeVideo(String link) {
        RxUtils.async(
                compositeDisposable,
                apiService.likeVideo(link, "UNLIKE", AccountBusiness.getInstance().getUserId()),
                new ICallBackResponse<Object>() {
                    @Override
                    public void onSuccess(Object response) {

                    }

                    @Override
                    public void onFail(String error) {

                    }
                });
    }
}

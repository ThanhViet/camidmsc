package com.metfone.esport.ui.games.videoss.adapters;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.homes.adapters.decorations.InSpaceContentDecoration;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameVideoPastBroadcastItemViewBinder extends ExtSaveStateItemViewBinder<ObjectGameVideoPastBroadcast, GameVideoPastBroadcastItemViewBinder.GameVideoPastBroadcastBinderViewHolder> {

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;
    private OnScrollListener onScrollListener;

    public GameVideoPastBroadcastItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public GameVideoPastBroadcastItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem, OnScrollListener onScrollListener) {
        this.onClickItem = onClickItem;
        this.onScrollListener = onScrollListener;
    }

    @Override
    public int getItemLayoutId() {
        return R.layout.item_only_recycler_view;
    }

    @Override
    public GameVideoPastBroadcastBinderViewHolder onCreateBinderHolder(View view) {
        return new GameVideoPastBroadcastBinderViewHolder(view);
    }

    public class GameVideoPastBroadcastBinderViewHolder extends ExtSaveStateViewHolder<ObjectGameVideoPastBroadcast> {

        private GameVideoPastBroadcastAdapter adapter = null;

        @BindView(R.id.rvOnlyRecyclerView)
        RecyclerView rvOnlyRecyclerView;

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return rvOnlyRecyclerView.getLayoutManager();
        }

        public GameVideoPastBroadcastBinderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onHolderCreated() {
            adapter = new GameVideoPastBroadcastAdapter(itemView.getContext(), null, onClickItem);

            rvOnlyRecyclerView.setAdapter(adapter);
            rvOnlyRecyclerView.setHasFixedSize(true);
            rvOnlyRecyclerView.addItemDecoration(new InSpaceContentDecoration(itemView.getContext()));
            rvOnlyRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == (adapter.getItemCount() - 1)) {
                        //bottom of list!
                        if (onScrollListener != null)
                            onScrollListener.onEndLessScroll();
                    }
                }
            });
        }

        @Override
        public void onBindData(ObjectGameVideoPastBroadcast o) {
            if (adapter != null && CollectionUtils.isNotEmpty(o.listVideo)) {
                adapter.setNewData(o.listVideo);
            }
        }
    }

    public interface OnScrollListener {
        void onEndLessScroll();
    }
}

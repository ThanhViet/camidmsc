package com.metfone.esport.ui.mychannel;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.base.BaseDialogFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;

import butterknife.BindView;

public class WarningEditVideoDialog extends BaseDialogFragment {
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.tv_msg)
    AppCompatTextView tvMessage;
    @BindView(R.id.btn_submit)
    AppCompatTextView btnSubmit;
    @BindView(R.id.btn_cancel)
    AppCompatTextView btnCancel;
    private String title, message, labelSubmit, labelCancel;
    private NegativeListener<String> negativeListener;
    private PositiveListener<String> positiveListener;

    public static WarningEditVideoDialog newInstance(String title, String message) {
        WarningEditVideoDialog dialog = new WarningEditVideoDialog();
        dialog.title = title;
        dialog.message = message;
        return dialog;
    }

    public static WarningEditVideoDialog newInstance(String message) {
        WarningEditVideoDialog dialog = new WarningEditVideoDialog();
        dialog.message = message;
        return dialog;
    }

    public void setPositiveListener(PositiveListener<String> positiveListener) {
        this.positiveListener = positiveListener;
    }

    public void setNegativeListener(NegativeListener<String> negativeListener) {
        this.negativeListener = negativeListener;
    }

    @Override
    protected void initView(View rootView) {
        tvTitle.setVisibility(title == null ? View.GONE : View.VISIBLE);
        tvTitle.setText(title == null ? "" : title);
        tvMessage.setText(message == null ? "" : message);
        if (StringUtils.isNotEmpty(labelSubmit)) btnSubmit.setText(labelSubmit);
        if (StringUtils.isNotEmpty(labelCancel)) btnCancel.setText(labelCancel);
        btnSubmit.setOnClickListener(view -> {
            if (positiveListener != null) positiveListener.onPositive("");
        });
        btnCancel.setOnClickListener(view -> {
            if (negativeListener != null) negativeListener.onNegative("");
        });
    }

    @Override
    protected int getDialogWidth() {
        return ScreenUtils.getScreenWidth();
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_warning_edit_video_es;
    }

    @Override
    public String getTAG() {
        return DialogFragment.class.getSimpleName();
    }

}

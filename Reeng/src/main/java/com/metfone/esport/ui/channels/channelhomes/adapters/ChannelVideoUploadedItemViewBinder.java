package com.metfone.esport.ui.channels.channelhomes.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;

import org.jetbrains.annotations.NotNull;

public class ChannelVideoUploadedItemViewBinder extends ItemViewBinder<VideoInfoResponse, HalfVideoNoOptionViewHolder> {

    private HalfVideoNoOptionViewHolder.IOnClickItem onClickItem;

    public ChannelVideoUploadedItemViewBinder(HalfVideoNoOptionViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public HalfVideoNoOptionViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HalfVideoNoOptionViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull HalfVideoNoOptionViewHolder myListVideoViewHolder, VideoInfoResponse video) {
        myListVideoViewHolder.binData(video, onClickItem, true);
    }
}

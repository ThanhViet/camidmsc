package com.metfone.esport.ui.searchs.adapters;

import com.metfone.esport.entity.repsonse.VideoInfoResponse;

public class ObjectSearchUploaded {
    public VideoInfoResponse video;

    public ObjectSearchUploaded(Object video) {
        if (video instanceof VideoInfoResponse)
            this.video = (VideoInfoResponse) video;
    }
}

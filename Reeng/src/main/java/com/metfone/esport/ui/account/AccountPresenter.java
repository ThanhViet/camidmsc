package com.metfone.esport.ui.account;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.AccountEntity;
import com.metfone.esport.entity.event.OnLoginChannel;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 9/15/20.
 */
public class AccountPresenter extends BasePresenter<AccountFragment, BaseModel> {

    public AccountPresenter(AccountFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public List<AccountEntity> getDataDummy(){
        List<AccountEntity> accountEntities = new ArrayList<>();
        accountEntities.add(new AccountEntity(context.getString(R.string.my_channel),R.drawable.ic_mychannel_es));
        accountEntities.add(new AccountEntity(context.getString(R.string.switch_account),R.drawable.ic_switch_account_es));
        return accountEntities;
    }

    public void getMyChannel() {
        //"84358427382"
        RxUtils.async(
                compositeDisposable,
                apiService.getMyChannels(AccountBusiness.getInstance().getPhoneNumberOrUserId(), getDefaultParam()),
                new ICallBackResponse<ArrayList<ChannelInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<ChannelInfoResponse> response) {
                        if (CollectionUtils.isNotEmpty(response)) {
                            AccountBusiness.getInstance().saveMyChannel(response);
                            view.onSuccessMyChannel(response);
                            EventBus.getDefault().post(new OnLoginChannel());
                        }else {
                            view.showToast("Please try again");
                        }
//                        if (view != null) view.setupHeaderView();
                    }

                    @Override
                    public void onFail(String error) {
//                        if (view != null) view.setupHeaderView();
                    }
                });
    }
}

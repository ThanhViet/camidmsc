package com.metfone.esport.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.selfcare.R;
import com.metfone.esport.base.ExtKeyPair;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleSelectBottomSheet extends BottomSheetDialogFragment {
    SingleSelectAdapter.IOnClickItem onClickItem;

    ArrayList<ExtKeyPair<?, String>> options;

    ExtKeyPair<?, String> currentOption;

    private SingleSelectAdapter adapter;

    @BindView(R.id.rvSingleTextSelect)
    RecyclerView rvSingleTextSelect;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.single_select_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public SingleSelectBottomSheet setOnClickItem(SingleSelectAdapter.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;

        return this;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new SingleSelectAdapter(getActivity(), (e) -> {
            if (onClickItem != null) {
                onClickItem.onClickItem(e);

                dismiss();
            }
        });
        rvSingleTextSelect.setHasFixedSize(true);
        rvSingleTextSelect.setAdapter(adapter);

        if (currentOption != null && options != null) {
            for (ExtKeyPair<?, String> option : options) {
                if (option != null) {
                    option.isSelected = option.key == currentOption.key;
                }
            }
        }

        adapter.setNewData(options);
    }

    public SingleSelectBottomSheet setOptions(ArrayList<ExtKeyPair<?, String>> options) {
        this.options = options;

        return this;
    }

    public SingleSelectBottomSheet setCurrentSelected(ExtKeyPair<?, String> option) {
        this.currentOption = option;

        return this;
    }
}

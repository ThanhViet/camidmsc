package com.metfone.esport.ui.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.entity.AccountEntity;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * Created by Nguyễn Thành Chung on 9/15/20.
 */
public class AccountAdapter extends BaseListAdapter<AccountEntity, AccountAdapter.AccountViewHolder> {

    private OnClickItem onClickItem;

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public AccountAdapter(Context context) {
        super(context);
    }

    @NotNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new AccountViewHolder(LayoutInflater.from(context).inflate(R.layout.item_account, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull AccountViewHolder holder, int position) {
        AccountEntity entity = mData.get(position);
        holder.itemView.setOnClickListener(view -> {
            if (onClickItem!=null){
                onClickItem.onClickItem(entity,position);
            }
        });
        holder.binData(entity, position);
    }

    @Override
    public Filter getFilter() {
        return null;
    }


    public interface OnClickItem{
        void onClickItem(AccountEntity entity, int position);
    }

    static class AccountViewHolder extends BaseViewHolder<AccountEntity> {
        @BindView(R.id.ivRight)
        AppCompatImageView ivRight;
        @BindView(R.id.ivIcon)
        AppCompatImageView ivIcon;
        @BindView(R.id.tvName)
        TextView tvName;

        public AccountViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void binData(AccountEntity entity, int position) {
            tvName.setText(entity.name);
            ivIcon.setImageResource(entity.drawable);
        }
    }
}

package com.metfone.esport.ui.channels.channelhomes.adapters;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.selfcare.R;
import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.homes.adapters.decorations.InSpacelayoutDecoration;
import com.metfone.esport.ui.viewholders.HomeGameViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelHomeGameItemViewBinder extends ExtSaveStateItemViewBinder<ObjectChannelHomeGame, ChannelHomeGameItemViewBinder.ChannelHomeGameBinderViewHolder> {
    private HomeGameViewHolder.IOnClickItem onClickItem;

    public ChannelHomeGameItemViewBinder(HomeGameViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @Override
    public int getItemLayoutId() {
        return R.layout.item_only_recycler_view;
    }

    @Override
    public ChannelHomeGameBinderViewHolder onCreateBinderHolder(View view) {
        return new ChannelHomeGameBinderViewHolder(view);
    }

    public class ChannelHomeGameBinderViewHolder extends ExtSaveStateViewHolder<ObjectChannelHomeGame> {

        private ChannelHomeGameAdapter adapter = null;

        @BindView(R.id.rvOnlyRecyclerView)
        RecyclerView rvOnlyRecyclerView;

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return rvOnlyRecyclerView.getLayoutManager();
        }

        public ChannelHomeGameBinderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onHolderCreated() {
            adapter = new ChannelHomeGameAdapter(itemView.getContext(), null, onClickItem);

            rvOnlyRecyclerView.setAdapter(adapter);
            rvOnlyRecyclerView.setHasFixedSize(true);
            rvOnlyRecyclerView.addItemDecoration(new InSpacelayoutDecoration(itemView.getContext()));
        }

        @Override
        public void onBindData(ObjectChannelHomeGame o) {
            if (adapter != null && CollectionUtils.isNotEmpty(o.listGame)) {
                adapter.setNewData(o.listGame);
            }
        }
    }
}

package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchPastBroadcastItemViewBinder extends ItemViewBinder<ObjectSearchPastBroadCast, HomeMetfoneViewHolder> {

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;

    public SearchPastBroadcastItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public HomeMetfoneViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HomeMetfoneViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull HomeMetfoneViewHolder viewBinderHolder, ObjectSearchPastBroadCast s) {
        viewBinderHolder.binData(s.video, onClickItem, true);
    }
}

package com.metfone.esport.ui.searchs.contents;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.util.RxUtils;

import io.reactivex.disposables.CompositeDisposable;

class SearchContentPresenter extends BasePresenter<SearchContentFragment, BaseModel> {

    public SearchContentPresenter(SearchContentFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void followChannel(Long id, int actionType) {
        RxUtils.async(
                compositeDisposable,
                apiService.followChannel(id, actionType, getDefaultParam()), //0 la unfollow
                new ICallBackResponse<String>() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFail(String error) {

                    }
                });
    }
}

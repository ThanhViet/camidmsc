/*
 * Copyright (c) admin created on 2020-09-12 10:27:42 PM
 */

package com.metfone.esport.ui.viewholders;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;

import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.entity.ThumbVideoUpload;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.selfcare.R;

import butterknife.BindView;

public class PhotoOfVideoUploadHolder extends BaseViewHolder<ThumbVideoUpload> {
    @BindView(R.id.iv_remove)
    public AppCompatImageView ivRemove;
    @BindView(R.id.iv_cover)
    AppCompatImageView ivCover;
    @BindView(R.id.layout_choose_photo)
    View layoutChoosePhoto;

    public PhotoOfVideoUploadHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void binData(ThumbVideoUpload entity, int position) {
        if (entity.getType() == ThumbVideoUpload.TYPE_CHOOSE_PHOTO) {
            layoutChoosePhoto.setVisibility(View.VISIBLE);
            ivCover.setVisibility(View.GONE);
            ivRemove.setVisibility(View.GONE);
        } else if (entity.getType() == ThumbVideoUpload.TYPE_BITMAP) {
            layoutChoosePhoto.setVisibility(View.GONE);
            ivCover.setVisibility(View.VISIBLE);
            ivRemove.setVisibility(View.VISIBLE);
            ivCover.setImageBitmap(entity.getBitmap());
        } else if (entity.getType() == ThumbVideoUpload.TYPE_FILE_PATH) {
            layoutChoosePhoto.setVisibility(View.GONE);
            ivCover.setVisibility(View.VISIBLE);
            ivRemove.setVisibility(View.VISIBLE);
            ImageLoader.setImageFile(ivCover, entity.getFilePath());
        } else if (entity.getType() == ThumbVideoUpload.TYPE_FILE_URL) {
            layoutChoosePhoto.setVisibility(View.GONE);
            ivCover.setVisibility(View.VISIBLE);
            ivRemove.setVisibility(View.VISIBLE);
            ImageLoader.setImage(
                    ivCover,
                    entity.getFilePath(),
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    null
            );
        }
    }
}

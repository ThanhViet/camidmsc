package com.metfone.esport.ui.mylists.mylistgames.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

import static com.metfone.esport.ui.mylists.mylistgames.MyListGameFragment.SPAN_COUNT;

public class MyListGameDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public MyListGameDecoration(Context context) {
        space = context != null ? context.getResources().getDimensionPixelSize(R.dimen.pading_layout) : 0;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position = parent.getChildAdapterPosition(view);

        int column = position % SPAN_COUNT;

        if (position >= SPAN_COUNT) {
            outRect.top = space;
        }

        outRect.left = space - column * space / SPAN_COUNT;

        outRect.right = (column + 1) * space / SPAN_COUNT;

//        if (column == 0)
//            outRect.left = space;
//        else
//            outRect.left = space / 2;

    }
}

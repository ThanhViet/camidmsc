package com.metfone.esport.ui.searchs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.ui.viewholders.HomeGameViewHolder;
import com.metfone.selfcare.R;

import java.util.ArrayList;

public class SearchAllGameAdapter extends BaseListAdapter<GameInfoResponse, HomeGameViewHolder> {
    private int width;

    private HomeGameViewHolder.IOnClickItem onClickItem;

    public SearchAllGameAdapter(Context context, ArrayList<GameInfoResponse> items, HomeGameViewHolder.IOnClickItem onClickItem) {
        super(context);

        setNewData(items);

        width = (ScreenUtils.getScreenWidth() - context.getResources().getDimensionPixelSize(R.dimen.pading_layout) * 4) / 3;

        this.onClickItem = onClickItem;
    }

    @NonNull
    @Override
    public HomeGameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_game, parent, false);
//
//        ViewGroup.LayoutParams params = v.getLayoutParams();
//
//        params.width = width;
//
//        v.setLayoutParams(params);

        return new HomeGameViewHolder(LayoutInflater.from(context), parent, width);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeGameViewHolder holder, int position) {
        holder.binData(mData.get(position), false, onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

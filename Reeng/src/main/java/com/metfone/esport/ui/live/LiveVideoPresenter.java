package com.metfone.esport.ui.live;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.ContentParam;
import com.metfone.esport.entity.GiftBalanceModel;
import com.metfone.esport.entity.SocketModel;
import com.metfone.esport.entity.enums.EActionType;
import com.metfone.esport.entity.repsonse.DataCommentResponse;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.HttpHelper;
import com.metfone.esport.helper.encrypt.EncryptUtil;
import com.metfone.esport.listener.PostMessageSocketListener;
import com.metfone.esport.network.socket.SocketManager;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.RxUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 9/10/20.
 */
public class LiveVideoPresenter extends BasePresenter<LiveVideoFragment, BaseModel> {

    public LiveVideoPresenter(LiveVideoFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    public static SocketModel createLiveComment(int type, String content, String roomId, VideoInfoResponse media, GiftBalanceModel gift) {
        SocketModel model = new SocketModel();
        String userId = String.valueOf(AccountBusiness.getInstance().getUserId());
        String token = AccountBusiness.getInstance().getToken();
        long timestamp = System.currentTimeMillis();
        String security = HttpHelper.encryptDataLiveComment(userId, token, timestamp);
        model.type = type;
        model.roomId = roomId;
        model.security = security;
        ListCommentEntity chatMessage = new ListCommentEntity();
        chatMessage.setId(EncryptUtil.encryptMD5(userId + timestamp));
        chatMessage.setUserId(userId);
        String avatar = AccountBusiness.getInstance().getAvatarUrl().length() > 256 ? "" : AccountBusiness.getInstance().getAvatarUrl();
        chatMessage.setAvatar(avatar);
        chatMessage.setRoomId(roomId);
        chatMessage.setContent(content);
        chatMessage.setTimestamp(timestamp);
        chatMessage.setNameSender(AccountBusiness.getInstance().getUsername());
        if (media != null) {
            chatMessage.setData(GsonUtils.toJson(media));
        } else if (gift != null) {
            chatMessage.setData(GsonUtils.toJson(gift));
        }
        model.chatMessage = chatMessage;
        return model;
    }

    @Override
    public BaseModel getModel() {
        return null;
    }

    @Override
    public boolean isUseEventPost() {
        return true;
    }

    public void getDetailVideo(String url) {
        RxUtils.async(compositeDisposable, apiService.getVideoDetailV2(url, getDefaultParam())
                , new ICallBackResponse<DetailVideoResponse>() {
                    @Override
                    public void onSuccess(DetailVideoResponse response) {
                        view.onSuccessGetVideoDetail(response);
                    }

                    @Override
                    public void onFail(String error) {
                        view.onFailGetVideoDetail(error);
                    }
                });
    }





    public void sendComment(String comment, VideoInfoResponse media) {
        if (StringUtils.isEmpty(comment) || media == null) return;
        SocketManager.getInstance().sendMessage(GsonUtils.toJson(createLiveComment(SocketModel.TYPE_MESSAGES, comment, String.valueOf(media.id), media, null)), new PostMessageSocketListener() {

            @Override
            public void onPostMessageSuccess() {

            }

            @Override
            public void onPostMessageFailure() {

            }
        });
    }

    public void sendDonate(VideoInfoResponse media, GiftBalanceModel gift) {
        if (media == null) return;
        SocketManager.getInstance().sendMessage(GsonUtils.toJson(createLiveComment(SocketModel.TYPE_DONATE, "", String.valueOf(media.id), media, gift)), new PostMessageSocketListener() {

            @Override
            public void onPostMessageSuccess() {

            }

            @Override
            public void onPostMessageFailure() {

            }
        });
    }

    public void sendHeart(VideoInfoResponse media) {
        if (media == null) return;
        SocketManager.getInstance().sendMessage(GsonUtils.toJson(createLiveComment(SocketModel.TYPE_HEART, "", String.valueOf(media.id), null, null)), new PostMessageSocketListener() {

            @Override
            public void onPostMessageSuccess() {

            }

            @Override
            public void onPostMessageFailure() {

            }
        });
    }

    private boolean isLoadAllComments = true;
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEvent(SocketModel event) {
        switch (event.type) {
            case SocketModel.TYPE_MESSAGES: {
                //todo bản tin tin nhắn mới
//                if (view!=null) view.onReceiverMessageLive(event.chatMessage);
                break;
            }
            case SocketModel.TYPE_LIST_COMMENT: {
                //todo bản tin danh sách tin nhắn ban đầu
                if (isLoadAllComments) {
                  //  if (view != null) view.onSuccessCommentList(event.messageList);
                }
                break;
            }
            case SocketModel.TYPE_NUMBER_ROOM: {
                //todo bản tin cập nhật số người đang xem live
                //if (view != null) view.onUpdateLiveCount(event.numberLive);
                Log.e("LiveVidePresnter",event.numberLive + "");
                break;
            }
            case SocketModel.TYPE_DONATE: {
                //todo bản tin tin nhắn donate
                break;
            }
            case SocketModel.TYPE_HEART: {
                //todo bản tin tin nhắn thả tim
                break;
            }
        }
        EventBus.getDefault().removeStickyEvent(event);
    }




}

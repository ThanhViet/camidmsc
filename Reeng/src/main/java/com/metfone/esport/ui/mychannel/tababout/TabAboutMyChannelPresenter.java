package com.metfone.esport.ui.mychannel.tababout;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

class TabAboutMyChannelPresenter extends BasePresenter<TabAboutMyChannelFragment, BaseModel> {

    public TabAboutMyChannelPresenter(TabAboutMyChannelFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

}

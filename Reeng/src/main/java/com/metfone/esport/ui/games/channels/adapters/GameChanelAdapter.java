package com.metfone.esport.ui.games.channels.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.ui.viewholders.MyListChanelViewHolder;

import java.util.ArrayList;

public class GameChanelAdapter extends BaseListAdapter<ChannelInfoResponse, MyListChanelViewHolder> {

    private Context context;

    private MyListChanelViewHolder.IOnClickItem onClickItem;

    public GameChanelAdapter(Context context, ArrayList<ChannelInfoResponse> items, MyListChanelViewHolder.IOnClickItem onClickItem) {
        super(context);

        this.context = context;

        this.onClickItem = onClickItem;

        setNewData(items);
    }

    @NonNull
    @Override
    public MyListChanelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new MyListChanelViewHolder(LayoutInflater.from(context), parent, 0);
    }

    @Override
    public void onBindViewHolder(@NonNull MyListChanelViewHolder holder, int position) {
        holder.binDataByFollowers(mData.get(position), onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

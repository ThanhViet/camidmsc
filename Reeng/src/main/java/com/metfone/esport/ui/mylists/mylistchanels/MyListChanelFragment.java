package com.metfone.esport.ui.mylists.mylistchanels;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.entity.event.OnFollowChannelEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.mylists.mylistchanels.adapters.MyListChanelAdapter;
import com.metfone.esport.ui.mylists.mylistchanels.adapters.MyListChanelDecoration;
import com.metfone.esport.ui.viewholders.MyListChanelViewHolder;
import com.metfone.selfcare.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class MyListChanelFragment extends BaseFragment<MyListChanelPresenter> {

    @BindView(R.id.rvMyListChannel)
    RecyclerView rvMyListChannel;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private MyListChanelAdapter adapter;
    private int currentPage = 0;

    public static MyListChanelFragment newInstance() {
        Bundle args = new Bundle();
        MyListChanelFragment fragment = new MyListChanelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public MyListChanelPresenter getPresenter() {
        return new MyListChanelPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_list_chanel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        try {
            currentPage = 0;

            adapter.clear();

            getData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(adapter.getData())) multipleStatusView.showLoading();

        presenter.getData(currentPage++);
    }

    private void initRecyclerView() {
        adapter = new MyListChanelAdapter(getContext(), null, new MyListChanelViewHolder.IOnClickItem() {
            @Override
            public void onClickItem(ChannelInfoResponse channelInfo) {
                if (channelInfo != null)
                    AloneFragmentActivity.with(getActivity())
                            .parameters(ChannelContainerFragment.newBundle(channelInfo.id))
                            .start(ChannelContainerFragment.class);
            }

            @Override
            public void onClickOption(ChannelInfoResponse entity) {
                new SingleSelectBottomSheet()
                        .setOptions(CollectionUtils.newArrayList(new ExtKeyPair("", getString(R.string.unfollow), R.drawable.ic_favorite_white_es)))
                        .setOnClickItem(e -> {
                            if (!isLogin()) {
                                showDialogLogin();

                                return;
                            }

                            presenter.unFollowChannel(entity.id);

                            adapter.delete(entity);

                            checkEmptyView();

                        })
                        .show(getChildFragmentManager(), null);
            }
        });

        rvMyListChannel.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvMyListChannel.setHasFixedSize(true);
        rvMyListChannel.addItemDecoration(new MyListChanelDecoration(getContext()));
        rvMyListChannel.setAdapter(adapter);
        rvMyListChannel.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(adapter.getData()) - 1) {
                        //bottom of list!
                        getData();
                    }
                }
            }
        });
    }

    public void onGetDataSuccess(ArrayList<ChannelInfoResponse> response) {
        if (CollectionUtils.isNotEmpty(response)) {
            adapter.addAll(response);
        }

        checkEmptyView();
    }

    private void checkEmptyView() {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvMyListChannel != null) rvMyListChannel.setVisibility(View.INVISIBLE);
        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvMyListChannel != null) rvMyListChannel.setVisibility(View.VISIBLE);
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvMyListChannel != null) rvMyListChannel.setVisibility(View.INVISIBLE);
        }
    }

    @Subscribe
    public void onEvent(OnFollowChannelEvent event) {
        if (event == null || event.channel == null) return;

        if (event.channel.isFollow == 0) {
            ChannelInfoResponse removeChannel = CollectionUtils.find(adapter.getData(), item ->
                    item != null && item.id.equals(event.channel.id)
            );

            if (removeChannel != null) {
                adapter.delete(removeChannel);

                checkEmptyView();
            }
        } else {
            new Handler().postDelayed(this::refreshData, 1000);
        }
    }
}

package com.metfone.esport.ui.channels;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.EncodeUtils;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ViewPagerBaseAdapter;
import com.metfone.esport.common.Common;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.customview.ConfirmDialog;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.event.OnFollowChannelEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.ui.channels.channelabouts.ChanelAboutFragment;
import com.metfone.esport.ui.channels.channelhomes.ChannelHomeFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class ChannelContainerFragment extends BaseFragment<ChannelContainerPresenter> {

    private static String KEY_ID = "KEY_ID";

    public static ChannelContainerFragment newInstance(Long gameId) {
        ChannelContainerFragment fragment = new ChannelContainerFragment();

        fragment.setArguments(newBundle(gameId));

        return fragment;
    }

    public static Bundle newBundle(Long channelId) {

        Bundle args = new Bundle();

        args.putLong(KEY_ID, channelId);

        return args;
    }

    private Long channelId;

    private ChannelInfoResponse currentChannel;
    List<BaseFragment<?>> fragments = new ArrayList<>();
    List<String> titles = new ArrayList<>();

    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;

    @BindView(R.id.ivImageCover)
    AppCompatImageView ivImageCover;

    @BindView(R.id.lnViewFollow)
    LinearLayout lnViewFollow;

    @BindView(R.id.avChannelAvatar)
    AvatarView avChannelAvatar;

    @BindView(R.id.tvLive)
    AppCompatTextView tvLive;

    @BindView(R.id.tvFollowStatus)
    AppCompatTextView tvFollowStatus;

    @BindView(R.id.tvNotifyStatus)
    AppCompatTextView tvNotifyStatus;

    @BindView(R.id.ivNotifyStatus)
    AppCompatImageView ivNotifyStatus;

    @BindView(R.id.tvChannelName)
    AppCompatTextView tvChannelName;

    @BindView(R.id.tvViewers)
    AppCompatTextView tvViewers;

    @BindView(R.id.tvFollowers)
    AppCompatTextView tvFollowers;

    @BindView(R.id.tvChannelDescription)
    AppCompatTextView tvChannelDescription;

    @BindView(R.id.vpChannelPager)
    ViewPager vpChannelPager;

    @BindView(R.id.lnFollowNow)
    LinearLayout lnFollowNow;

    @BindView(R.id.lnNotifyNone)
    LinearLayout lnNotifyNone;

    @BindView(R.id.lnStatus)
    LinearLayout lnStatus;

    @BindView(R.id.ivOnline)
    View ivOnline;

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinator;

    @Override
    public ChannelContainerPresenter getPresenter() {
        return new ChannelContainerPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_channel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();
        Utilities.setSystemUiVisibilityHideNavigation(getActivity(), R.color.black);
        Common.setNavigationBarBlack(getParentActivity());
        Utilities.adaptViewForInsertBottom(coordinator);
        initViewPager();

        ivBack.setOnClickListener(v -> getActivity().onBackPressed());

        presenter.getData(channelId);
    }

    /**
     * Lay du lieu gameId
     * Created_by @dchieu on 9/15/20
     */
    private void getDataBundle() {
        if (getArguments() != null) {
            channelId = getArguments().getLong(KEY_ID, 0);
        }
    }

    private void initViewPager() {
        fragments.add(ChannelHomeFragment.newInstance(channelId));
        titles.add(getString(R.string.menu_channel_home));

        fragments.add(ChanelAboutFragment.newInstance());
        titles.add(getString(R.string.menu_channel_about));

        vpChannelPager.setAdapter(new ViewPagerBaseAdapter(getChildFragmentManager(), fragments, titles));
        vpChannelPager.setOffscreenPageLimit(fragments.size());
    }

    public void getDataFail() {

    }

    public void getDataSuccess(ChannelInfoResponse response) {
        currentChannel = response;

        if (currentChannel == null) return;

        try {
            lnViewFollow.setVisibility(View.VISIBLE);
            lnStatus.setVisibility(View.VISIBLE);

            avChannelAvatar.setAvatarUrl(currentChannel.urlAvatar);

            ImageLoader.setImage(
                    ivImageCover,
                    currentChannel.urlImagesCover,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    null);

            tvLive.setVisibility(currentChannel.isLive == 1 ? View.VISIBLE : View.INVISIBLE);

            tvChannelName.setText(currentChannel.name);

            tvViewers.setText(getString(currentChannel.totalView > 1 ? R.string.template_viewers_count : R.string.template_viewer_count, ViewsUnitEnum.getTextDisplay(currentChannel.totalView)));

            tvChannelDescription.setText(currentChannel.description);

            ivOnline.setVisibility(currentChannel.isLive == 1 ? View.VISIBLE : View.GONE);

            setViewFollowChannel();

            lnFollowNow.setOnClickListener(v -> {
                if (currentChannel == null) return;

                if (!isLogin()) {
                    showDialogLogin();

                    return;
                }

                if (currentChannel.isFollow == 1) {
                    ConfirmDialog.newInstance(getString(R.string.template_confirm_unfollow_channel, currentChannel.name), () -> {
                        currentChannel.isFollow = 0;

                        currentChannel.numFollow -= 1;

                        setViewFollowChannel();

                        presenter.followChannel(currentChannel.id, currentChannel.isFollow);

                        EventBus.getDefault().post(new OnFollowChannelEvent(currentChannel));
                    }).show(getChildFragmentManager());
                } else {
                    currentChannel.isFollow = 1;
                    currentChannel.numFollow += 1;

                    setViewFollowChannel();

                    presenter.followChannel(currentChannel.id, currentChannel.isFollow);

                    EventBus.getDefault().post(new OnFollowChannelEvent(currentChannel));
                }
            });

            lnNotifyNone.setOnClickListener(v -> {
                if (currentChannel == null) return;

                if (currentChannel.isNotify == 1) {
                    currentChannel.isNotify = 0;
                } else {
                    currentChannel.isNotify = 1;
                }

                presenter.notifyFromChannel(currentChannel.id, currentChannel.isNotify);

                setViewNotifyStatus();

            });

            ((ChanelAboutFragment) fragments.get(1)).onGetDataSuccess(currentChannel);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hien thi View Follow channel theo trang thai follow
     * Created_by @dchieu on 9/17/20
     */
    private void setViewFollowChannel() {
        if (currentChannel == null) return;

        String textFollow;
        long totalFollows = currentChannel.getNumFollow();
        if (totalFollows > 1) {
            textFollow = getString(R.string.total_followers, ViewsUnitEnum.getTextDisplay(totalFollows));
        } else {
            textFollow = getString(R.string.total_follower, totalFollows + "");
        }
        if (tvFollowers != null) {
            tvFollowers.setText(EncodeUtils.htmlDecode(textFollow));

            tvFollowers.setVisibility(TextUtils.isEmpty(textFollow) ? View.GONE : View.VISIBLE);
        }

        if (currentChannel.isFollow == 1) {
            lnNotifyNone.setVisibility(View.VISIBLE);

            tvFollowStatus.setText(R.string.unfollow);

            lnFollowNow.setBackgroundResource(R.drawable.bg_video_with_info_radius_4_es);

            setViewNotifyStatus();
        } else {
            lnNotifyNone.setVisibility(View.GONE);

            tvFollowStatus.setText(getString(R.string.follow_now));

            lnFollowNow.setBackgroundResource(R.drawable.bg_red_radius_4_es);
        }
    }

    /**
     * Hien thi Trang thai dang ky thoong bao
     * Created_by @dchieu on 9/17/20
     */
    private void setViewNotifyStatus() {
        if (currentChannel == null) return;

        if (currentChannel.isNotify == 1) {
            tvNotifyStatus.setText(getString(R.string.notifications));
            ivNotifyStatus.setImageResource(R.drawable.ic_notifications_active_es);
        } else {
            tvNotifyStatus.setText(getString(R.string.notification_none));
            ivNotifyStatus.setImageResource(R.drawable.ic_notifications_off_es);
        }
    }
}

package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.esport.ui.viewholders.SearchTournamentViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchTournamentItemViewBinder extends ItemViewBinder<TournamentInfoResponse, SearchTournamentViewHolder> {
    private SearchTournamentViewHolder.IOnClickItem onClickItem;

    public SearchTournamentItemViewBinder(SearchTournamentViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public SearchTournamentViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new SearchTournamentViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull SearchTournamentViewHolder viewBinderHolder, TournamentInfoResponse data) {
        viewBinderHolder.binData(data, onClickItem);
    }
}

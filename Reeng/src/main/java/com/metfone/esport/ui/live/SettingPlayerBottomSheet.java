package com.metfone.esport.ui.live;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.ResolutionVideo;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Nguyễn Thành Chung on 9/1/20.
 */
public class SettingPlayerBottomSheet extends BottomSheetDialogFragment {
    @BindView(R.id.tvHighSolution)
    TextView tvHighSolution;
    @BindView(R.id.lnReport)
    LinearLayout lnReport;
    @BindView(R.id.lnDisplayQuality)
    LinearLayout lnDisplayQuality;
    @BindView(R.id.lnHelpAndFeedback)
    LinearLayout lnHelpAndFeedback;
    private Unbinder unbinder;
    private OnClickSetting onClickSetting;
    private List<ResolutionVideo> listResolution = new ArrayList<>();

    public void setData(List<ResolutionVideo> listResolution) {
        this.listResolution = listResolution;
    }

    public void setOnClickSetting(OnClickSetting onClickSetting) {
        this.onClickSetting = onClickSetting;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.setting_player_bottom_sheet_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        Utilities.setSystemUiVisibilityHideNavigation(getActivity(), R.color.black);
        try{
            lnDisplayQuality.setVisibility(listResolution.isEmpty() ? View.GONE : View.VISIBLE);
            tvHighSolution.setText(getString(R.string.auto_480p,"480p"));
        }catch (Exception e){
            Common.handleException(e);
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.lnReport, R.id.lnDisplayQuality, R.id.lnHelpAndFeedback})
    public void onViewClicked(View view) {
        try {
            switch (view.getId()) {
                case R.id.lnReport:
                    if (onClickSetting != null) {
                        onClickSetting.onClickReport();
                    }
                    break;
                case R.id.lnDisplayQuality:
                    Log.e("lnDisplayQuality", "lnDisplayQuality");
                    PopupDisplayQuality quality = new PopupDisplayQuality();
                    quality.setData(listResolution);
                    quality.setOnClickItem(entity -> {
                        tvHighSolution.setText(getString(R.string.auto_480p,entity.title));
                        if (onClickSetting != null) {
                            onClickSetting.onClickQuality(entity);
                            Log.e("onClickSetting", "onClickSetting");
                        }
                    });
                    quality.show(getChildFragmentManager(), quality.getTag());

                    break;
                case R.id.lnHelpAndFeedback:
                    if (onClickSetting != null) {
                        onClickSetting.onClickFeedback();
                    }
                    break;
            }
        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        if(onClickSetting != null){
            onClickSetting.onCancel();
        }
    }

    public interface OnClickSetting {
        default void onClickFeedback() {
        }

        default void onClickReport() {
        }

        default void onClickQuality(ResolutionVideo resolutionVideo) {
        }

        default void onCancel() {
        }
    }
}

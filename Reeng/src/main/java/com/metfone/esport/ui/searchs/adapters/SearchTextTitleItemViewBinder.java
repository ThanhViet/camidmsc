package com.metfone.esport.ui.searchs.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.TextTitleViewHolder;

import org.jetbrains.annotations.NotNull;

public class SearchTextTitleItemViewBinder extends ItemViewBinder<ObjectTitle, TextTitleViewHolder> {
    @NotNull
    @Override
    public TextTitleViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new TextTitleViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull TextTitleViewHolder viewBinderHolder, ObjectTitle s) {
        viewBinderHolder.binData(s.title);
    }

}

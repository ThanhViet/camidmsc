package com.metfone.esport.ui.viewholders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HalfVideoWithOptionViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivThumbVideo)
    AppCompatImageView ivThumbVideo;

    @BindView(R.id.tvEyeViewCount)
    AppCompatTextView tvEyeViewCount;

    @BindView(R.id.tvLive)
    AppCompatTextView tvLive;

    @BindView(R.id.tvViews)
    AppCompatTextView tvViews;

    @BindView(R.id.tvTimeUploadAgo)
    public AppCompatTextView tvTimeUploadAgo;

    @BindView(R.id.tvVideoTitle)
    public AppCompatTextView tvVideoTitle;

    @BindView(R.id.tvVideoSubTitle)
    public AppCompatTextView tvVideoSubTitle;

    @BindView(R.id.tvAccountName)
    public AppCompatTextView tvAccountName;

    @BindView(R.id.avAvatar)
    public AvatarView avAvatar;

    @BindView(R.id.ivOptionMore)
    public AppCompatImageView ivOptionMore;

    @BindView(R.id.videoThumb)
    public View videoThumb;

    @BindView(R.id.layout_eye_viewer)
    View layout_eye_viewer;

    private Context context;

    public HalfVideoWithOptionViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_half_video_with_option, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        context = itemView.getContext();

        ButterKnife.bind(this, itemView);
    }

    public void binData(VideoInfoResponse entity, IOnClickItem callBack) {
        binData(entity, callBack, false);
    }

    public void binData(VideoInfoResponse entity, IOnClickItem callBack, boolean isShowDuration) {
        if (entity == null) return;

        try {
            if (entity.isLive()) {
                tvTimeUploadAgo.setVisibility(View.INVISIBLE);

                tvViews.setVisibility(View.INVISIBLE);

                tvLive.setVisibility(View.VISIBLE);

                if (entity.ccu <= 0) {
                    layout_eye_viewer.setVisibility(View.INVISIBLE);
                } else {
                    layout_eye_viewer.setVisibility(View.VISIBLE);
                    tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(entity.ccu));
                }
            } else {
                tvLive.setVisibility(View.INVISIBLE);

                if (entity.type_video == 1) { // Video uploaded
                    layout_eye_viewer.setVisibility(View.INVISIBLE);

                    if (entity.total_view <= 0) {
                        tvViews.setVisibility(View.INVISIBLE);
                    } else {
                        tvViews.setVisibility(View.VISIBLE);
                        tvViews.setText(itemView.getContext().getString(entity.total_view > 1 ? R.string.template_views_count : R.string.template_view_count, ViewsUnitEnum.getTextDisplay(entity.total_view)));
                    }

                    if (isShowDuration) {
                        if (entity.durationS <= 0) {
                            tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                        } else {
                            tvTimeUploadAgo.setVisibility(View.VISIBLE);
                            tvTimeUploadAgo.setText(entity.getDurationDisplay());
                        }
                    } else {
                        if (entity.publish_time <= 0)
                            tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                        else {
                            tvTimeUploadAgo.setVisibility(View.VISIBLE);
                            tvTimeUploadAgo.setText(Utilities.calculateTime(context, entity.publish_time));
                        }
                    }
                } else { //Video pastbroadcast
                    tvViews.setVisibility(View.INVISIBLE);

                    if (entity.total_view <= 0) {
                        layout_eye_viewer.setVisibility(View.INVISIBLE);
                    } else {
                        layout_eye_viewer.setVisibility(View.VISIBLE);
                        tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(entity.total_view));
                    }

                    if (entity.publish_time <= 0)
                        tvTimeUploadAgo.setVisibility(View.INVISIBLE);
                    else {
                        tvTimeUploadAgo.setVisibility(View.VISIBLE);
                        tvTimeUploadAgo.setText(Utilities.calculateTime(context, entity.publish_time));
                    }
                }
            }

            tvVideoTitle.setText(entity.title);

            if (entity.gameInfo != null) {
                tvVideoSubTitle.setText(entity.gameInfo.gameName);
            } else {
                tvVideoSubTitle.setText("");
            }

            if (entity.channelInfo != null) {
                tvAccountName.setText(entity.channelInfo.name);

                avAvatar.setAvatarUrl(entity.channelInfo.urlAvatar);
            } else {
                tvAccountName.setText("");

                avAvatar.setImageResource(R.drawable.bg_avatar_default_es);
            }

            avAvatar.setOnClickListener(v -> {
                if (callBack != null && entity.channelInfo != null)
                    callBack.onClickChannel(entity.channelInfo);
            });

            tvAccountName.setOnClickListener(v -> {
                if (callBack != null && entity.channelInfo != null)
                    callBack.onClickChannel(entity.channelInfo);
            });

            ivOptionMore.setOnClickListener(v -> {
                if (callBack != null)
                    callBack.onClickOption(entity);
            });

            videoThumb.setOnClickListener(v -> {
                if (callBack != null)
                    callBack.onClickItem(entity);
            });

            tvVideoTitle.setOnClickListener(v -> {
                if (callBack != null)
                    callBack.onClickItem(entity);
            });

            int radius = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.radius_xsmall);

            ImageLoader.setImage(
                    ivThumbVideo,
                    entity.image_path_thumb,
                    R.drawable.bg_image_placeholder_es,
                    R.drawable.bg_image_error_es,
                    new MultiTransformation<>(new CenterCrop(), new GranularRoundedCorners(radius, 0, 0, radius)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface IOnClickItem {
        void onClickItem(VideoInfoResponse entity);

        default void onClickChannel(ChannelInfoResponse channel) {
        }

        void onClickOption(VideoInfoResponse entity);
    }
}

package com.metfone.esport.ui.channels;

import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.ChannelInfoWrapperResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class ChannelContainerPresenter extends BasePresenter<ChannelContainerFragment, BaseModel> {
    private Disposable disposable = null;

    public ChannelContainerPresenter(ChannelContainerFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(Long channelId) {
        if (disposable != null)
            disposable.dispose();

        disposable = RxUtils.async(
                compositeDisposable,
                apiService.getChannelInfo(getDefaultParam(), channelId),
                new ICallBackResponse<ChannelInfoWrapperResponse>() {
                    @Override
                    public void onSuccess(ChannelInfoWrapperResponse response) {
                        if (view != null && response != null)
                            view.getDataSuccess(response.channelInfo);
                        else onFail("");
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null)
                            view.getDataFail();
                    }
                }
        );
    }

    public void followChannel(Long id, int actionType) {
        RxUtils.async(
                compositeDisposable,
                apiService.followChannel(id, actionType, getDefaultParam()), //0 la unfollow
                new ICallBackResponse<String>() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFail(String error) {

                    }
                });
    }

    public void notifyFromChannel(Long id, Integer actionType) {
        RxUtils.async(
                compositeDisposable,
                apiService.notifyFromChannel(id, actionType, AccountBusiness.getInstance().getUserId()), //1 la bat, 0 la tat
                new ICallBackResponse<Object>() {
                    @Override
                    public void onSuccess(Object response) {

                    }

                    @Override
                    public void onFail(String error) {

                    }
                });
    }
}

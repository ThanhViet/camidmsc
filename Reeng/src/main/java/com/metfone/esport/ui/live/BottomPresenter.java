package com.metfone.esport.ui.live;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.ContentParam;
import com.metfone.esport.entity.GiftBalanceModel;
import com.metfone.esport.entity.SocketModel;
import com.metfone.esport.entity.camid.CamIdUserResponse;
import com.metfone.esport.entity.enums.EActionType;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.DataCommentResponse;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.entity.repsonse.UserInfoEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.entity.request.ChannelInfoRequest;
import com.metfone.esport.entity.request.UserInfoRequest;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.HttpHelper;
import com.metfone.esport.helper.ServiceBusiness;
import com.metfone.esport.helper.encrypt.EncryptUtil;
import com.metfone.esport.listener.PostMessageSocketListener;
import com.metfone.esport.network.donate.DonateESportApiCallback;
import com.metfone.esport.network.donate.DonateESportClient;
import com.metfone.esport.network.donate.response.WsGetAllDonatePackageAppResponse;
import com.metfone.esport.network.socket.SocketManager;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

/**
 * Created by Nguyễn Thành Chung on 10/11/20.
 */
public class BottomPresenter extends BasePresenter<BottomFragment, BaseModel> {
    public BottomPresenter(BottomFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public boolean isUseEventPost() {
        return true;
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getDetailVideo(String url) {
        RxUtils.async(compositeDisposable, apiService.getVideoDetailV2(url, getDefaultParam())
                , new ICallBackResponse<DetailVideoResponse>() {
                    @Override
                    public void onSuccess(DetailVideoResponse response) {
                        view.onSuccessGetVideoDetail(response);
                    }

                    @Override
                    public void onFail(String error) {
                        view.onFailGetVideoDetail(error);
                    }
                });
    }

    public static SocketModel createLiveComment(int type, String content, String roomId, VideoInfoResponse media, GiftBalanceModel gift) {
        SocketModel model = new SocketModel();
        String userId = String.valueOf(AccountBusiness.getInstance().getUserId());
        String token = AccountBusiness.getInstance().getToken();
        long timestamp = System.currentTimeMillis();
        String security = HttpHelper.encryptDataLiveComment(userId, token, timestamp);
        model.type = type;
        model.roomId = roomId;
        model.security = security;
        ListCommentEntity chatMessage = new ListCommentEntity();
        chatMessage.setId(EncryptUtil.encryptMD5(userId + timestamp));
        chatMessage.setUserId(userId);
        String avatar = "test";
        chatMessage.setAvatar(avatar);
        chatMessage.setRoomId(roomId);
        chatMessage.setContent(content);
        chatMessage.setTimestamp(timestamp);
        chatMessage.setNameSender(Common.getNameAccount());
        if (media != null) {
            chatMessage.setData(GsonUtils.toJson(media));
        } else if (gift != null) {
            chatMessage.setData(GsonUtils.toJson(gift));
        }
        model.chatMessage = chatMessage;
        return model;
    }

    public void login() {
        ServiceBusiness.login(compositeDisposable, apiService);
    }

    public void getComments(String url, String rowStart) {
        RxUtils.async(compositeDisposable, apiService.getComments(rowStart, 20, url, getDefaultParam())
                , new ICallBackResponse<DataCommentResponse>() {
                    @Override
                    public void onSuccess(DataCommentResponse response) {
                        view.onSuccessCommentList(response.listComment);
                    }

                    @Override
                    public void onFail(String error) {
                        view.onFailGetVideoDetail(error);
                    }
                });

    }

    public void getVideoRelated(Long videoId,String videoName,String channelId,String gameId) {
        RxUtils.async(compositeDisposable,
                apiService.getVideoRelated(videoName,videoId,channelId,gameId, getDefaultParam()), new ICallBackResponse<ArrayList<VideoInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<VideoInfoResponse> response) {
                        if (view != null) view.onSuccessGetVideoRelated(response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onFailGetVideoDetail(error);
                    }
                });
    }

    public void sendComment(String comment, VideoInfoResponse media) {
        if (StringUtils.isEmpty(comment) || media == null) return;
        SocketManager.getInstance().sendMessage(GsonUtils.toJson(createLiveComment(SocketModel.TYPE_MESSAGES, comment, String.valueOf(media.id), media, null)), new PostMessageSocketListener() {

            @Override
            public void onPostMessageSuccess() {
            }

            @Override
            public void onPostMessageFailure() {

            }
        });
    }

    public void sendDonate(VideoInfoResponse media, GiftBalanceModel gift) {
        if (media == null) return;
        SocketManager.getInstance().sendMessage(GsonUtils.toJson(createLiveComment(SocketModel.TYPE_DONATE, "", String.valueOf(media.id), media, gift)), new PostMessageSocketListener() {

            @Override
            public void onPostMessageSuccess() {

            }

            @Override
            public void onPostMessageFailure() {

            }
        });
    }

    public void sendHeart(VideoInfoResponse media) {
        if (media == null) return;
        SocketManager.getInstance().sendMessage(GsonUtils.toJson(createLiveComment(SocketModel.TYPE_HEART, "", String.valueOf(media.id), null, null)), new PostMessageSocketListener() {

            @Override
            public void onPostMessageSuccess() {

            }

            @Override
            public void onPostMessageFailure() {

            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEvent(SocketModel event) {
        switch (event.type) {
            case SocketModel.TYPE_MESSAGES: {
                //todo bản tin tin nhắn mới
//                if (view!=null) view.onReceiverMessageLive(event.chatMessage);
//                Log.e("comment", event.chatMessage.getContent());
                break;
            }
            case SocketModel.TYPE_LIST_COMMENT: {
                //todo bản tin danh sách tin nhắn ban đầu
//                Log.e("socket==listcomment", event.messageList.size() + "");
                break;
            }
            case SocketModel.TYPE_NUMBER_ROOM: {
                //todo bản tin cập nhật số người đang xem live
//                Log.e("LiveVidePresnter", event.numberLive + "");
                break;
            }
            case SocketModel.TYPE_DONATE: {
                //todo bản tin tin nhắn donate
                break;
            }
            case SocketModel.TYPE_HEART: {
                //todo bản tin tin nhắn thả tim
                break;
            }
        }
        EventBus.getDefault().removeStickyEvent(event);
    }

    public void sendCommentPastBroadcast(String message, VideoInfoResponse media) {
        ContentParam contentParam = new ContentParam();
        contentParam.url = media.link;
        String channelId = "";
        String jsonChannelInfo = "";
        if (SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
            channelId = String.valueOf(AccountBusiness.getInstance().getChannelInfo().id);
            jsonChannelInfo =  GsonUtils.toJson(getChannelInfoRequest());
        }
        RxUtils.async(compositeDisposable, apiService.actionApp(
                String.valueOf(AccountBusiness.getInstance().getUserId()),
                GsonUtils.toJson(contentParam),
                media.link,
                "postActionFrom",
                message,
                1,
                "tag",
                "",
                0,
                GsonUtils.toJson(getUserInfoRequest()),
                channelId,
                jsonChannelInfo
        ), new ICallBackResponse<Object>() {

            @Override
            public void onSuccess(Object response) {

            }

            @Override
            public void onFail(String error) {

            }
        });
    }


    private UserInfoRequest getUserInfoRequest() {
        return new UserInfoRequest(String.valueOf(AccountBusiness.getInstance().getUserId()),
                0,
                Common.getNameCamId(),
                Common.getNameCamId().isEmpty() ? "Profile channel" : Common.getNameCamId(),
                Common.getAvatarAccount());
    }


    private ChannelInfoRequest getChannelInfoRequest(){
        return new ChannelInfoRequest(
                String.valueOf(AccountBusiness.getInstance().getChannelInfo().id),
                AccountBusiness.getInstance().getChannelInfo().name,
                StringUtils.isNotEmpty(AccountBusiness.getInstance().getChannelInfo().getUrlImages()) ? AccountBusiness.getInstance().getChannelInfo().getUrlImages() : "test"
        );
    }

    public void likeVideo(Integer like, VideoInfoResponse media) {
        RxUtils.async(compositeDisposable, apiService.likeVideo(
                media.link,
                like == 1 ? EActionType.LIKE.name() : EActionType.UNLIKE.name(),
                AccountBusiness.getInstance().getUserId()
        ), new ICallBackResponse<Object>() {

            @Override
            public void onSuccess(Object response) {
                view.onSuccessCommentBroadcast();
            }

            @Override
            public void onFail(String error) {
                view.onErrorCommentBroadcast(error);
            }
        });
    }
}

package com.metfone.esport.ui.channels.channelhomes.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;

import org.jetbrains.annotations.NotNull;

public class ChannelHomeLiveItemViewBinder extends ItemViewBinder<ChannelInfoResponse, HomeMetfoneViewHolder> {

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;

    public ChannelHomeLiveItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public HomeMetfoneViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HomeMetfoneViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull HomeMetfoneViewHolder viewHolder, ChannelInfoResponse channelInfo) {
        if (channelInfo != null)
            viewHolder.binDataByChannel(channelInfo, onClickItem);
    }
}

package com.metfone.esport.ui.live;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.chungnguyen95.draggablepane.DraggablePanel;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.SocketModel;
import com.metfone.esport.entity.event.ChangeVideoEvent;
import com.metfone.esport.entity.event.OnBackDeviceEvent;
import com.metfone.esport.entity.event.OnChangeStateVideoTinyEvent;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.network.socket.SocketManager;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.entity.SocketModel.TYPE_MESSAGES;

/**
 * Created by Nguyễn Thành Chung on 8/28/20.
 */
public class LiveVideoFragment extends BaseFragment<LiveVideoPresenter> implements BottomFragment.DonateListener {
    @BindView(R.id.layout_root)
    RelativeLayout layout_root;
    @BindView(R.id.draggablePanel)
    DraggablePanel draggablePanel;
    @BindView(R.id.viewStatus)
    View viewStatus;
    private VideoInfoResponse modelData;
    private DetailVideoResponse response;
    private TopFragment topFragment;
    private BottomFragment bottomFragment;
    private boolean showAllCommentLive = true;
    private boolean isRedStatusBarOnTiny = false;
    private boolean fromNotification = false;
    private boolean isLogin = isLogin();

    public static LiveVideoFragment newInstance(VideoInfoResponse modelData, boolean isRedStatusBarOnTiny) {
        LiveVideoFragment fragment = new LiveVideoFragment();
        fragment.modelData = modelData;
        fragment.isRedStatusBarOnTiny = isRedStatusBarOnTiny;
        return fragment;
    }

    public static LiveVideoFragment newInstance(DetailVideoResponse response,boolean isRedStatusBarOnTiny) {
        LiveVideoFragment fragment = new LiveVideoFragment();
        fragment.response = response;
        fragment.modelData = new VideoInfoResponse();
        fragment.modelData.link = response.link;
        fragment.isRedStatusBarOnTiny = isRedStatusBarOnTiny;
        fragment.fromNotification = true;
        return fragment;
    }

    @Override
    public LiveVideoPresenter getPresenter() {
        return new LiveVideoPresenter(this, compositeDisposable);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        try {
//            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            SocketManager.getInstance().disconnect();
        } catch (Exception e) {
            Common.handleException(e);
        }
        Log.e("xxxxxxxxxx", "onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void showStatus(boolean isShow) {
        try {
            viewStatus.setVisibility(isShow ? View.VISIBLE : View.GONE);
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isLogin != isLogin()){
            bottomFragment.initView();
        }
    }

    @Override
    public void onDestroy() {
        try {
            SocketManager.getInstance().disconnect();
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        } catch (Exception e) {
            Common.handleException(e);
        }
        ApplicationController.getApp().isPlayingMini = false;

        EventBus.getDefault().post(new OnChangeStateVideoTinyEvent());

        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();

    }


    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.live_video_activity;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewStatus.getLayoutParams();
            params.height = Common.getStatusBarHeight(getContext());
            viewStatus.setLayoutParams(params);
            showStatus(true);
            Utilities.setSystemUiVisibilityHideNavigation(getActivity(), R.color.black);
            Utilities.adaptViewForInsertBottom(layout_root);
            topFragment = TopFragment.newInstance(modelData, new TopFragment.CallBackFragment() {
                @Override
                public void callBackTinyScreen() {
                    getParentActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    if (draggablePanel!=null)  draggablePanel.minimize();
                    ApplicationController.getApp().isPlayingMini = true;
                }

                @Override
                public void callBackScreenExpand() {
                    draggablePanel.maximize();
                    topFragment.setExpandVideo();

                }

                @Override
                public void closeTinyScreen() {
                    getParentActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    draggablePanel.close();
                    popFragment();
                    try {
                        SocketManager.getInstance().disconnect();
                    } catch (Exception e) {
                        Common.handleException(e);
                    }
                    ApplicationController.getApp().isPlayingMini = false;
//                    Utilities.setSystemUiVisibilityHideNavigation();
                }

                @Override
                public void onClickExitFullScreen() {
                    try {
                        showStatus(false);
                        if (isClickDonateHorizontal) {
                            if (bottomFragment != null)
                                bottomFragment.showDonateHome();
                            isClickDonateHorizontal = false;
                        }

                    } catch (Exception e) {
                        Common.handleException(e);
                    }
                }
            });
            bottomFragment = BottomFragment.newInstance(modelData, this);
//            if (isRedStatusBarOnTiny) {
//                draggablePanel.setMMarginBottomWhenMin(getResources().getDimensionPixelOffset(R.dimen._70sdp));
//            } else {
//                draggablePanel.setMMarginBottomWhenMin(getResources().getDimensionPixelOffset(R.dimen._16sdp));
//            }
            draggablePanel.setMMarginBottomWhenMin(0);
            draggablePanel.setDraggableListener(new DraggablePanel.DraggableListener() {
                @Override
                public void onSwipeBack() {
                    popFragment();
                }

                @Override
                public void onExpanded() {

                }

                @Override
                public void onChangeState(@NotNull DraggablePanel.State state) {
                    Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
                    if (state == DraggablePanel.State.CLOSE) {
                        ApplicationController.getApp().isPlayingMini = false;
                    } else {
                        ApplicationController.getApp().isPlayingMini = true;
                    }
                }

                @Override
                public void onChangePercent(float percent) {
//                layout_root.setAlpha(1 - percent);
                    if (1 - percent == 0) {
                        topFragment.setCollapseVideo();
                        Utilities.setSystemUiVisibilityHideNavigation(getParentActivity(), R.color.black);
                        KeyboardUtils.hideSoftInput(getParentActivity());
                    } else {
                        topFragment.setExpandVideo();
                        Common.setNavigationBarBlack(getParentActivity());
                    }
                    showStatus(1 - percent != 0);
                }
            });
//            setKeyDown();
            getChildFragmentManager().beginTransaction().add(R.id.frameFirst, topFragment).commit();
            getChildFragmentManager().beginTransaction().add(R.id.frameSecond, bottomFragment).commit();
            if(response != null){
                new Handler().postDelayed(() -> onSuccessGetVideoDetail(response),1000);
            }else{
                presenter.getDetailVideo(modelData.link);
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void initSocket() {
        try {
            SocketManager.getInstance().disconnect();
            SocketManager.getInstance().connectWebSocket(Constant.WS_ESPORT, String.valueOf(modelData.id));
        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    public void onSuccessGetVideoDetail(DetailVideoResponse response) {
        try {
            if (response == null) return;
            topFragment.initVideo(response);
            modelData.isLive = response.isLive;
            topFragment.setQualityList(response.listResolution);
            bottomFragment.onSuccessGetVideoDetail(response);
            if (response.isLiveVideo()) {
                initSocket();
            }
        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    public void onFailGetVideoDetail(String error) {

    }

    @Subscribe
    public void onEvent(SocketModel event) {
        try {
            switch (event.type) {
                case TYPE_MESSAGES: {
                    //todo bản tin tin nhắn mới
//                if (view!=null) view.onReceiverMessageLive(event.chatMessage);
                    Log.e("TYPE_MESSAGES", event.chatMessage.getContent());
                    bottomFragment.onSuccessComment(event.chatMessage);
                    topFragment.setMessageFullScreen(event.chatMessage);
                    break;
                }
                case SocketModel.TYPE_LIST_COMMENT: {
                    //todo bản tin danh sách tin nhắn ban đầu
//                if (isLoadAllComments) {
//                      if (view != null) view.onSuccessCommentList(event.messageList);
                    if (CollectionUtils.isNotEmpty(event.messageList)) {
//                        Log.e("TYPE_LIST_COMMENT", event.messageList.get(0).getContent());
                        if (showAllCommentLive) {
                            bottomFragment.onSuccessCommentList(event.messageList);
                            topFragment.setListMessageFullScreen(event.messageList);
                            showAllCommentLive = false;
                        } else {
                            bottomFragment.setEmptyCommentLive();
                        }
                    }

//                }
                    break;
                }
                case SocketModel.TYPE_NUMBER_ROOM: {
                    //todo bản tin cập nhật số người đang xem live
                    topFragment.onUpdateLiveCount(event.numberLive);
                    Log.e("LiveVidePresnter", event.numberLive + "");
                    break;
                }
                case SocketModel.TYPE_DONATE: {
                    //todo bản tin tin nhắn donate
                    break;
                }
                case SocketModel.TYPE_HEART: {
                    //todo bản tin tin nhắn thả tim
                    break;
                }
            }
            EventBus.getDefault().removeStickyEvent(event);
        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    @Subscribe
    public void onChangeVideoEvent(ChangeVideoEvent event) {
        try {
            modelData = event.response;
            presenter.getDetailVideo(modelData.link);
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private void setKeyDown(){
        try{
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener((v, keyCode, event) -> {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    popFragment();
                    getView().setFocusableInTouchMode(false);
                    getView().clearFocus();
                    return true;
                }
                return false;
            });
        }catch (Exception e){
            Common.handleException(e);
        }
    }



    @Subscribe
    public void OnBackDeviceEvent(OnBackDeviceEvent commentEvent) {
        try {
            if (!topFragment.isHorizontal()) {
                popFragment();
            }else {
                SharedPrefs.getInstance().put(Constants.PREFERENCE.BACK_DEVICE,true);
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public static boolean isClickDonateHorizontal = false;
    public final static int TIME_SHOW_THANKMESSAGE = 2000;
    public final static int TIME_REMAIN_SHOW = 5000;
    public final static String PRE_CODE_THANKMESSAGE = "msc#";
    @Override
    public void onClickOpen() {
        if (topFragment != null) topFragment.lockRotation();
    }

    @Override
    public void onClickClose() {
        if (topFragment != null) topFragment.listenerRotateScreen();
    }

    @Override
    public void onDonateSuccessNotLogin(int coin, String cmt) {
        if (topFragment != null) topFragment.showThankMessNotLogin(coin, cmt);
    }
}

package com.metfone.esport.ui.games.videoss;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.drakeet.multitype.MultiTypeAdapter;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.homes.adapters.ExtSaveStateMultiTypeAdapter;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.games.videoss.adapters.GameVideoDecoration;
import com.metfone.esport.ui.games.videoss.adapters.GameVideoPastBroadcastItemViewBinder;
import com.metfone.esport.ui.games.videoss.adapters.GameVideoUploadedItemViewBinder;
import com.metfone.esport.ui.games.videoss.adapters.ObjectGameUploaded;
import com.metfone.esport.ui.games.videoss.adapters.ObjectGameVideoPastBroadcast;
import com.metfone.esport.ui.searchs.adapters.ObjectTitle;
import com.metfone.esport.ui.searchs.adapters.SearchTextTitleItemViewBinder;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class GameVideoFragment extends BaseFragment<GameVideoPresenter> {

    private static String KEY_GAME_ID = "KEY_GAME_ID";

    @BindView(R.id.rvGameVideo)
    RecyclerView rvGameVideo;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private MultiTypeAdapter adapter = new ExtSaveStateMultiTypeAdapter();

    private ArrayList<Object> items = new ArrayList<>();
    private ArrayList<Object> uploadedItems = new ArrayList<>();
    private ArrayList<Object> pastBroadcastItems = new ArrayList<>();
    private Long gameId;
    private int currentPage = 0;
    private int currentPagePastBroadCast = 0;

    public static GameVideoFragment newInstance(Long gameId) {
        GameVideoFragment fragment = new GameVideoFragment();
        fragment.setArguments(newBundle(gameId));
        return fragment;
    }

    public static Bundle newBundle(Long gameId) {

        Bundle args = new Bundle();

        args.putLong(KEY_GAME_ID, gameId);

        return args;
    }

    @Override
    public GameVideoPresenter getPresenter() {
        return new GameVideoPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game_video;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();
    }

    private void getDataBundle() {
        if (getArguments() != null)
            gameId = getArguments().getLong(KEY_GAME_ID, 0);
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        currentPage = 0;

        currentPagePastBroadCast = 0;

        items.clear();

        uploadedItems.clear();

        pastBroadcastItems.clear();

        getData();
    }

    /**
     * Call service lay du lieu video
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(items)) multipleStatusView.showLoading();

        getDataUploaded();

        getDataPastBroadcast();
    }

    /**
     * Call service lay du lieu video uploaded
     * Created_by @dchieu on 9/7/20
     */
    private void getDataUploaded() {
        presenter.getData(gameId, currentPage++);
    }

    /**
     * Call service lay du lieu video pastbroadcast
     * Created_by @dchieu on 9/7/20
     */
    private void getDataPastBroadcast() {
        presenter.getDataPastBroadcast(gameId, currentPagePastBroadCast++);
    }

    private void initRecyclerView() {
        adapter.register(ObjectGameVideoPastBroadcast.class, new GameVideoPastBroadcastItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
                ApplicationController.self().getFirebaseEventBusiness().logPastBroadcastEsport(video.title, String.valueOf(video.id));
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }, new GameVideoPastBroadcastItemViewBinder.OnScrollListener() {
            @Override
            public void onEndLessScroll() {
                if (!presenter.isLoadingPastBroadCast() && presenter.isCanLoadMorePastBroadCast()) {
                    getDataPastBroadcast();
                }
            }
        }));
        adapter.register(ObjectGameUploaded.class, new GameVideoUploadedItemViewBinder(new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
                ApplicationController.self().getFirebaseEventBusiness().logUploadedEsport(video.title, String.valueOf(video.id));
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(ObjectTitle.class, new SearchTextTitleItemViewBinder());

        rvGameVideo.setAdapter(adapter);

        rvGameVideo.setHasFixedSize(true);

        rvGameVideo.addItemDecoration(new GameVideoDecoration(getContext()));

        rvGameVideo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(items) - 1) {
                        //bottom of list!
                        getDataUploaded();
                    }
                }
            }
        });
    }

    private void navigateToChannel(ChannelInfoResponse channel) {
        if (channel != null)
            AloneFragmentActivity.with(getActivity())
                    .parameters(ChannelContainerFragment.newBundle(channel.id))
                    .start(ChannelContainerFragment.class);
    }

    public void onGetVideoUploadedSuccess(ArrayList<VideoInfoResponse> response, int currentPage) {
        if (uploadedItems == null) uploadedItems = new ArrayList<>();

        uploadedItems.clear();

        if (CollectionUtils.isNotEmpty(response)) {
            if (currentPage == 0) uploadedItems.add(new ObjectTitle(getString(R.string.uploaded)));

            for (VideoInfoResponse videoInfoResponse : response) {
                uploadedItems.add(new ObjectGameUploaded(videoInfoResponse));
            }

            if (CollectionUtils.isNotEmpty(uploadedItems)) {
                items.addAll(uploadedItems);
            }

            setEmptyViewRecyclerView();
        }
    }

    private void setEmptyViewRecyclerView() {
//        if (isAddPastBroadcast) {
//            if (CollectionUtils.isNotEmpty(pastBroadcastItems) && CollectionUtils.isEmpty(items)) {
//                items.addAll(pastBroadcastItems);
//            }
//        } else {
//            if (CollectionUtils.isNotEmpty(uploadedItems)) {
//                items.addAll(uploadedItems);
//            }
//        }

        if (CollectionUtils.isEmpty(items)) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvGameVideo != null) rvGameVideo.setVisibility(View.INVISIBLE);

        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvGameVideo != null) rvGameVideo.setVisibility(View.VISIBLE);

            if (adapter != null) {
                adapter.setItems(items);

                adapter.notifyDataSetChanged();
            }
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(items)) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvGameVideo != null) rvGameVideo.setVisibility(View.INVISIBLE);
        }
    }

    public void onGetVideoPastBroadCastSuccess(ArrayList<VideoInfoResponse> response, int currentPage) {
        try {
            if (pastBroadcastItems == null) pastBroadcastItems = new ArrayList<>();

            if (CollectionUtils.isNotEmpty(response)) {

                if (currentPage == 0) {
                    items.clear();

                    pastBroadcastItems.clear();

                    pastBroadcastItems.add(new ObjectTitle(getString(R.string.past_broad_cast)));

                    pastBroadcastItems.add(new ObjectGameVideoPastBroadcast());
                }

                ObjectGameVideoPastBroadcast pastBroadcast = (ObjectGameVideoPastBroadcast) pastBroadcastItems.get(1);

                if (pastBroadcast.listVideo == null)
                    pastBroadcast.listVideo = new ArrayList<>();

                pastBroadcast.listVideo.addAll(response);

                if (CollectionUtils.isNotEmpty(pastBroadcastItems) && CollectionUtils.isEmpty(items)) {
                    items.addAll(pastBroadcastItems);

                    if (CollectionUtils.isNotEmpty(uploadedItems)) {
                        items.addAll(uploadedItems);
                    }
                }

                setEmptyViewRecyclerView();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

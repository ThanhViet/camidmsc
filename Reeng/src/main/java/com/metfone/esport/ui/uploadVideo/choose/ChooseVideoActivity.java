/*
 * Created by admin on 2020/9/6
 */

package com.metfone.esport.ui.uploadVideo.choose;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.UriUtils;
import com.iknow.trimvideo.listener.OnChooseVideoListener;
import com.iknow.trimvideo.view.select.VideoCursorLoader;
import com.iknow.trimvideo.view.select.VideoLoadManager;
import com.iknow.trimvideo.view.select.VideoSelectAdapter;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.golive.GoLiveActivity;
import com.metfone.esport.ui.uploadVideo.trim.TrimVideoActivity;
import com.metfone.selfcare.R;

import java.io.File;

import butterknife.BindView;
import iknow.android.utils.callback.SimpleCallback;
import io.reactivex.disposables.CompositeDisposable;

public class ChooseVideoActivity extends BaseActivity implements OnChooseVideoListener {

    @BindView(R.id.btn_back)
    @Nullable
    View btnBack;
    @BindView(R.id.btn_record)
    @Nullable
    View btnRecord;
    @BindView(R.id.btn_go_live)
    @Nullable
    View btnGoLive;
    @BindView(R.id.grid_view)
    @Nullable
    GridView gridVideo;
    @BindView(R.id.rootView)
    @Nullable
    ConstraintLayout rootView;
    @BindView(R.id.layout_action_bar)
    @Nullable
    RelativeLayout layout_action_bar;

    private VideoSelectAdapter adapter;
    private VideoLoadManager loadManager;

    @Override
    public Object getPresenter() {
        return null;
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_choose_video;
    }

    @SuppressLint("CheckResult")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        if (rootView != null) {
            com.metfone.selfcare.util.Utilities.adaptViewForInsertBottom(rootView);
        }

        if (btnBack != null) btnBack.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                finish();
            }
        });
        if (btnRecord != null) btnRecord.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                PermissionUtils.permission(PermissionConstants.UPLOAD_VIDEO)
                        .rationale((activity, shouldRequest) -> {
                            shouldRequest.again(true);
                        })
                        .callback(new PermissionUtils.SimpleCallback() {
                            @Override
                            public void onGranted() {
                                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                startActivityForResult(intent, Constant.REQUEST_CODE.OPEN_CAMERA_FOR_UPLOAD_VIDEO);
                            }

                            @Override
                            public void onDenied() {
                                ToastUtils.showShort(R.string.msg_grant_camera_storage_permission);
                            }
                        }).request();
            }
        });
        if (btnGoLive != null) btnGoLive.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View view) {
                startActivity(new Intent(ChooseVideoActivity.this, GoLiveActivity.class));
            }
        });
        loadManager = new VideoLoadManager();
        loadManager.setLoader(new VideoCursorLoader());
        PermissionUtils.permission(PermissionConstants.STORAGE)
                .rationale((activity, shouldRequest) -> {
                    shouldRequest.again(true);
                })
                .callback(new PermissionUtils.SimpleCallback() {
                    @Override
                    public void onGranted() {
                        loadManager.load(ChooseVideoActivity.this, new SimpleCallback() {
                            @Override
                            public void success(Object obj) {
                                if (adapter == null) {
                                    adapter = new VideoSelectAdapter(ChooseVideoActivity.this, (Cursor) obj);
                                    adapter.setListener(ChooseVideoActivity.this);
                                } else {
                                    adapter.swapCursor((Cursor) obj);
                                }
                                if (gridVideo != null && gridVideo.getAdapter() == null) {
                                    gridVideo.setAdapter(adapter);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void onDenied() {
                        ToastUtils.showShort(R.string.msg_grant_camera_storage_permission);
                        finish();
                    }
                }).request();

        if (rootView != null && layout_action_bar != null) {
            ConstraintSet newSet = new ConstraintSet();
            newSet.clone(rootView);
            newSet.connect(layout_action_bar.getId(), ConstraintSet.TOP, rootView.getId(), ConstraintSet.TOP, Common.getStatusBarHeight(this));
//            rootView.setPadding(0, Common.getStatusBarHeight(this) + getResources().getDimensionPixelOffset(R.dimen._10sdp), 0, 0);

            newSet.applyTo(rootView);
        }

    }

    private void showDialogWarningUploadVideo(int resTitle, int resMessage, int resBtnSubmit) {
        //file length > 1GB: show popup alert with message max length of File.
        WarningUploadVideoDialog dialog = WarningUploadVideoDialog.newInstance(getString(resTitle), getString(resMessage), getString(resBtnSubmit));
        dialog.setCancelable(true);
        dialog.show(getSupportFragmentManager());

    }

    @Override
    public void onChooseVideo(View view, String filePath) {
        long length = FileUtils.getFileLengthMB(filePath);
        if (length > 1024) {
            //file length > 1GB: show popup alert with message max length of File.
            showDialogWarningUploadVideo(R.string.title_too_heavy_upload_video, R.string.msg_too_heavy_upload_video, R.string.btn_try_again);
            return;
        }
        long duration = FileUtils.getDuration(filePath);
        duration /= 1000;
        if (duration < 30) {
            //file duration < 30 ms: show popup alert with message too short duration.
            showDialogWarningUploadVideo(R.string.title_too_short_upload_video, R.string.msg_too_short_upload_video, R.string.btn_try_again);
            return;
        } else if (duration > 14400) {
            //file duration < 240 minutes: show popup alert with message too long duration.
            showDialogWarningUploadVideo(R.string.title_too_long_upload_video, R.string.msg_too_long_upload_video, R.string.btn_try_again);
            return;
        }
        Intent intent = new Intent(this, TrimVideoActivity.class);
        intent.putExtra(Constant.KEY_FILE_PATH, filePath);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constant.REQUEST_CODE.OPEN_CAMERA_FOR_UPLOAD_VIDEO && data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    File file = UriUtils.uri2File(uri);
                    if (file != null) {
                        Intent intent = new Intent(this, TrimVideoActivity.class);
                        intent.putExtra(Constant.KEY_FILE_PATH, file.getPath());
                        startActivity(intent);
                    }
                }
            }
        }
    }
}

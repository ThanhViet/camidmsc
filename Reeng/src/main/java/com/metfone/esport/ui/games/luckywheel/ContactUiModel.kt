package com.metfone.esport.ui.games.luckywheel

data class ContactUiModel(var id: Int,
                          var name: String,
                          var phoneNumber: String,
                          var isSelected: Boolean = false)
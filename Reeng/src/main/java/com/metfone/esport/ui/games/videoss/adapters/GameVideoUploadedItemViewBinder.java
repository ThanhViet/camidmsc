package com.metfone.esport.ui.games.videoss.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;

import org.jetbrains.annotations.NotNull;

public class GameVideoUploadedItemViewBinder extends ItemViewBinder<ObjectGameUploaded, HalfVideoNoOptionViewHolder> {
    private HalfVideoNoOptionViewHolder.IOnClickItem onClickItem;

    public GameVideoUploadedItemViewBinder(HalfVideoNoOptionViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public HalfVideoNoOptionViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new HalfVideoNoOptionViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull HalfVideoNoOptionViewHolder myListVideoViewHolder, ObjectGameUploaded o) {
        myListVideoViewHolder.binData(o.videoInfoResponse, onClickItem, true);
    }
}

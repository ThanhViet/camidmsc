/*
 * Copyright (c) admin created on 2020-09-12 10:26:38 PM
 */

package com.metfone.esport.ui.uploadVideo.upload.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import com.library.listener.OnSingleClickListener;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.ThumbVideoUpload;
import com.metfone.esport.listener.ThumbVideoUploadListener;
import com.metfone.esport.ui.viewholders.PhotoOfVideoUploadHolder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ThumbVideoUploadAdapter extends BaseListAdapter<ThumbVideoUpload, PhotoOfVideoUploadHolder> {

    private ThumbVideoUploadListener listener;

    public ThumbVideoUploadAdapter(Context context, ArrayList<ThumbVideoUpload> items) {
        super(context);
        setNewData(items);
    }

    public void setListener(ThumbVideoUploadListener listener) {
        this.listener = listener;
    }

    @NotNull
    @Override
    public PhotoOfVideoUploadHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new PhotoOfVideoUploadHolder(mInflater.inflate(R.layout.item_photo_of_video_upload, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull PhotoOfVideoUploadHolder holder, int position) {
        ThumbVideoUpload item = getItem(position);
        holder.binData(item, position);
        if (item.getType() == ThumbVideoUpload.TYPE_CHOOSE_PHOTO) {
            holder.itemView.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickChoosePhoto(getItemCount());
                }
            });
        } else {
            holder.ivRemove.setVisibility(item.isSelect ? View.VISIBLE : View.GONE);
            holder.itemView.setAlpha(item.isSelect ? 1f : 0.5f);
            holder.itemView.setOnClickListener(view -> {
                for (ThumbVideoUpload mDatum : mData) {
                    mDatum.isSelect = false;
                }
                item.isSelect = true;
                notifyDataSetChanged();
            });
            holder.ivRemove.setOnClickListener(view -> {
                getData().remove(item);
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public ThumbVideoUpload getThumbUpload() {
        if (getItemCount() > 1)
            return getItem(1);
        return null;
    }
}

package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ButtonViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvButtonText)
    AppCompatTextView tvButtonText;

    public ButtonViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_button, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

//    public TextTitleViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//        ButterKnife.bind(this, itemView);
//    }

    public void binData(String entity, IOnClickItem onClickItem) {
        tvButtonText.setText(entity);

        if (onClickItem != null)
            itemView.setOnClickListener(v -> onClickItem.onClickItem());
    }

    public interface IOnClickItem {
        void onClickItem();
    }
}

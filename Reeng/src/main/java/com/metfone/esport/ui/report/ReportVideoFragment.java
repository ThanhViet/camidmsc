package com.metfone.esport.ui.report;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.report.adapter.ReportAdapter;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 9/22/20.
 */
public class ReportVideoFragment extends BaseFragment<ReportVideoPresenter> {
    @BindView(R.id.btn_back)
    AppCompatImageView btnBack;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.layout_action_bar)
    RelativeLayout layoutActionBar;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.tvReport)
    TextView tvReport;

    public static String VIDEO_ENTITY = "VIDEO_ENTITY";
    @BindView(R.id.rlSuccessFully)
    RelativeLayout rlSuccessFully;
    private String textSelect;
    private DetailVideoResponse videoResponse;

    public static Bundle newBundle(String videoResponse) {
        Bundle bundle = new Bundle();
        bundle.putString(VIDEO_ENTITY, videoResponse);
        return bundle;
    }

    @Override
    public ReportVideoPresenter getPresenter() {
        return new ReportVideoPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_report_video;
    }

    private void getDataBundle() {
        assert getArguments() != null;
        videoResponse = Common.convertJsonToObject(getArguments().getString(VIDEO_ENTITY), DetailVideoResponse.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDataBundle();
        textSelect = presenter.getDataDummy().get(0).text;
        initRecycler();
        btnBack.setOnClickListener(new OnSingleClickListener(){

            @Override
            public void onSingleClick(View view) {
                if (getActivity() != null) getActivity().finish();
            }
        });
    }

    private void initRecycler() {
        rvData.setLayoutManager(new LinearLayoutManager(getContext()));
        rvData.setHasFixedSize(true);
        ReportAdapter adapter = new ReportAdapter(getContext());
        adapter.setNewData(presenter.getDataDummy());
        adapter.setOnClickItem(entity -> {
            textSelect = entity.text;
        });
        rvData.setAdapter(adapter);
    }

    @OnClick(R.id.tvReport)
    public void onViewClicked() {
        if (isLogin()) {
            showDialog();
            presenter.reportVideo(String.valueOf(videoResponse.id), videoResponse.title, videoResponse.link, "", textSelect);
        } else {
            showDialogLogin();
        }
    }

    public void onSuccessReport() {
        hideDialog();
        rlSuccessFully.setVisibility(View.VISIBLE);
        new Handler().postDelayed(() -> getActivity().onBackPressed(), 3000);
    }

    public void onFailReport(String error) {
        hideDialog();
    }
}

package com.metfone.esport.ui.games.videoss;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class GameVideoPresenter extends BasePresenter<GameVideoFragment, BaseModel> {
    private Disposable apiDispose = null;
    private Disposable apiDisposePastBroadCast = null;
    private boolean canLoadMore = false;
    private boolean canLoadMorePastBroadCast = false;

    public boolean isLoading() {
        return apiDispose != null && !apiDispose.isDisposed();
    }

    public boolean isCanLoadMore() {
        return canLoadMore;
    }

    public boolean isLoadingPastBroadCast() {
        return apiDisposePastBroadCast != null && !apiDisposePastBroadCast.isDisposed();
    }

    public boolean isCanLoadMorePastBroadCast() {
        return canLoadMorePastBroadCast;
    }

    public GameVideoPresenter(GameVideoFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData(final Long gameId, final int currentPage) {
        if (currentPage > 0 && isLoading()) return;

        if (currentPage == 0 && apiDispose != null)
            apiDispose.dispose();

        canLoadMore = false;

        apiDispose = RxUtils.async(
                compositeDisposable,
                apiService.getVideoOfGame(gameId, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam()),
                new ICallBackResponse<ArrayList<VideoInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<VideoInfoResponse> response) {
                        if (view != null) view.onGetVideoUploadedSuccess(response, currentPage);

                        if (CollectionUtils.size(response) >= DEFAULT_LIMIT) canLoadMore = true;
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }

                    @Override
                    public void onFinish() {
                        if (apiDispose != null)
                            apiDispose.dispose();
                    }
                });
    }

    public void getDataPastBroadcast(final Long gameId, final int currentPage) {
        if (currentPage > 0 && isLoading()) return;

        if (currentPage == 0 && apiDisposePastBroadCast != null)
            apiDisposePastBroadCast.dispose();

        canLoadMorePastBroadCast = false;

        apiDisposePastBroadCast = RxUtils.async(
                compositeDisposable,
                apiService.getPastBroadcastOfGame(gameId, currentPage * DEFAULT_LIMIT, DEFAULT_LIMIT, getDefaultParam()),
                new ICallBackResponse<ArrayList<VideoInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<VideoInfoResponse> response) {
                        if (view != null) view.onGetVideoPastBroadCastSuccess(response, currentPage);

                        if (CollectionUtils.size(response) >= DEFAULT_LIMIT) canLoadMorePastBroadCast = true;
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }

                    @Override
                    public void onFinish() {
                        if (apiDisposePastBroadCast != null)
                            apiDisposePastBroadCast.dispose();
                    }
                });
    }
}

package com.metfone.esport.ui.live;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.blankj.utilcode.util.StringUtils;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.common.Common;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nguyễn Thành Chung on 9/7/20.
 */
public class InfoPlayerBottomSheet extends BottomSheetDialogFragment {
    @BindView(R.id.ivLogoChannel)
    AvatarView ivLogoChannel;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.ivFavorite)
    AppCompatImageView ivFavorite;
    @BindView(R.id.tvTitleVideo)
    TextView tvTitleVideo;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    private VideoInfoResponse modelData;
    private OnClickFavorite onClickFavorite;

    private InfoPlayerBottomSheet() {
    }

    public static InfoPlayerBottomSheet newInstance(VideoInfoResponse modelData, OnClickFavorite onClickFavorite) {
        InfoPlayerBottomSheet bottomSheet = new InfoPlayerBottomSheet();
        bottomSheet.modelData = modelData;
        bottomSheet.onClickFavorite = onClickFavorite;
        return bottomSheet;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_info_video_bottom, container, false);
        ButterKnife.bind(this, view);
        try {
            if (modelData != null) {
                tvTitleVideo.setText(modelData.title);
                tvDescription.setText(modelData.description);
                showFavoriteVideo();
                if (modelData.channelInfo == null) {
                    tvChannel.setText("");
                    ivLogoChannel.setAvatarUrl("");
                } else {
                    ChannelInfoResponse channel = modelData.channelInfo;
                    if (StringUtils.isEmpty(channel.name))
                        tvChannel.setText("");
                    else
                        tvChannel.setText(channel.name);
                    ivLogoChannel.setAvatarUrl(channel.getUrlAvatar());
                }
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
        return view;
    }

    private void showFavoriteVideo() {
        try{
            if (modelData.isLike()) {
                ivFavorite.setImageResource(R.drawable.ic_favorite_red_es);
            } else {
                ivFavorite.setImageResource(R.drawable.ic_favorite_white_es);
            }
        }catch (Exception e){
            Common.handleException(e);
        }
    }

    @OnClick(R.id.ivFavorite)
    public void onViewClicked() {
        if (AccountBusiness.getInstance().isLogin()) {
            try {
                if (modelData.isLike()) {
                    modelData.is_like = 0;
                } else {
                    modelData.is_like = 1;
                }
                showFavoriteVideo();
                if (onClickFavorite != null) {
                    onClickFavorite.onClickFavorite(modelData);
                }
            }catch (Exception e){
                Common.handleException(e);
            }
        } else if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showDialogLogin();
        }
    }

    public interface OnClickFavorite {
        void onClickFavorite(VideoInfoResponse modelData);
    }
}

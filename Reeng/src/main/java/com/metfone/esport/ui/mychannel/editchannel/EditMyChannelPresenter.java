package com.metfone.esport.ui.mychannel.editchannel;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.google.gson.Gson;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.network.donate.DonateESportApiCallback;
import com.metfone.esport.network.donate.DonateESportClient;
import com.metfone.esport.network.donate.request.SubUpdateChannelInformationRequest;
import com.metfone.esport.network.donate.response.WsUpdateChannelInformationResponse;
import com.metfone.esport.service.FileServiceRetrofit;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.CreateChannelResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.model.account.UserInfo;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

class EditMyChannelPresenter extends BasePresenter<EditMyChannelFragment, BaseModel> {

    public EditMyChannelPresenter(EditMyChannelFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData() {

    }

    public void editChannel(String channelId,String avatarChannel,String bannerChannel,String aboutImg,String createdDate, String channelName, String desc, String aboutTitle, String about, File avatar, File banner, File aboutImage) {
        MultipartBody.Part bodyAvatar = null;
        MultipartBody.Part bodyBanner = null;
        MultipartBody.Part bodyAbout = null;
        MediaType imageType = MediaType.parse("image/jpeg");
        RequestBody requestFile;
        if (avatar != null) {
            requestFile = RequestBody.Companion.create(avatar, imageType);
            bodyAvatar = MultipartBody.Part.createFormData("avatar", avatar.getName(), requestFile);
        }
        if (banner != null) {
            requestFile = RequestBody.Companion.create(banner, imageType);
            bodyBanner = MultipartBody.Part.createFormData("banner", banner.getName(), requestFile);
        }
        if (aboutImage != null) {
            requestFile = RequestBody.Companion.create(aboutImage, imageType);
            bodyAbout = MultipartBody.Part.createFormData("aboutImage", aboutImage.getName(), requestFile);
        }
        RequestBody imageLead = RequestBody.create("", MultipartBody.FORM);
        RxUtils.asyncCreateChannel(compositeDisposable, FileServiceRetrofit.getInstance().editChannel(
                AccountBusiness.getInstance().getPhoneNumber(),
                channelId,
                channelName,
                desc,
                aboutTitle,
                avatarChannel,
                bannerChannel,
                aboutImg,
                about,
                createdDate,
                imageLead,
                bodyAvatar,
                bodyBanner,
                bodyAbout,
                getDefaultParam()
        ), new ICallBackResponse<CreateChannelResponse>() {
            @Override
            public void onSuccess(CreateChannelResponse response) {
                if (view != null) {
                    view.showToast(R.string.msg_update_channel_success);
                    if (CollectionUtils.isNotEmpty(response.result)) {
                        ArrayList<ChannelInfoResponse> myChannels = AccountBusiness.getInstance().getMyChannels();
                        ArrayList<ChannelInfoResponse> saveChannels = new ArrayList<>();
                        AccountBusiness.getInstance().saveCurrentChannel(response.result.get(0));
                        updateChannel(response.result.get(0));
                        if (CollectionUtils.isNotEmpty(myChannels)) {
                            for (ChannelInfoResponse myChannel : myChannels) {
                                if (myChannel.id.equals(response.result.get(0).id)) {
                                    myChannel.imageAbout = myChannels.get(0).imageAbout;
                                    myChannel.urlAvatar = myChannels.get(0).urlAvatar;
                                    myChannel.urlImages = myChannels.get(0).urlImages;
                                    myChannel.urlImagesCover = myChannels.get(0).urlImagesCover;
                                    myChannel.aboutTitle = myChannels.get(0).aboutTitle;
                                    myChannel.about = myChannels.get(0).about;
                                    myChannel.description = myChannels.get(0).description;
                                    myChannel.name = myChannels.get(0).name;

                                }
                                saveChannels.add(myChannel);
                            }
                            SPUtils.getInstance().getString(Constant.PREF_CACHE_LIST_MY_CHANNEL_ESPORT, new Gson().toJson(saveChannels));
                        }

                    }


                    view.onSuccessEditChannel();
                }

            }

            @Override
            public void onFail(String error) {
                if (view != null) view.showToast("Edit channel fail");
            }
        });
    }

    private String getMsisdn() {
        if (!StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getJidNumber())) {
            return ApplicationController.self().getReengAccountBusiness().getJidNumber();
        }
        return "";
    }
    public void updateChannel(ChannelInfoResponse channel) {
        String camId = "";
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(view.getParentActivity());
        UserInfo currentUser = userInfoBusiness.getUser();
        camId = currentUser.getUser_id() + "";

        SubUpdateChannelInformationRequest param = new SubUpdateChannelInformationRequest();
        param.setId(channel.id+"");
        param.setChannelName(channel.name);
        param.setCamId(camId);
        param.setMsisdn(getMsisdn());
        param.setComment("");
        param.setCommentKh("");
        param.setEmoneyAccount("");
        param.setStatus("1");
        param.setImageUrl(channel.urlAvatar);
        param.setBannerUrl(channel.urlImages);
        param.setCreatedDate(channel.createdDate);

        new DonateESportClient().wsUpdateChannelInformation(param, new DonateESportApiCallback<WsUpdateChannelInformationResponse>() {
            @Override
            public void onResponse(Response<WsUpdateChannelInformationResponse> response) {
                if (response.body() != null && response.body().isSuccessful()) {
                    SPUtils.getInstance().put("update_channel_"+channel.id, true);
                } else
                    SPUtils.getInstance().put("update_channel_"+channel.id, false);
            }

            @Override
            public void onError(Throwable error) {
                SPUtils.getInstance().put("update_channel_"+channel.id, false);
            }
        });
    }

}

package com.metfone.esport.ui.searchs.adapters;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.homes.adapters.decorations.InSpacelayoutDecoration;
import com.metfone.esport.ui.viewholders.HomeGameViewHolder;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAllGameItemViewBinder extends ExtSaveStateItemViewBinder<ObjectSearchAllGame, SearchAllGameItemViewBinder.HomeGameBinderViewHolder> {

    private HomeGameViewHolder.IOnClickItem onClickItem;

    public SearchAllGameItemViewBinder(HomeGameViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @Override
    public int getItemLayoutId() {
        return R.layout.item_search_all_game_recyclerview;
    }

    @Override
    public HomeGameBinderViewHolder onCreateBinderHolder(View view) {
        return new HomeGameBinderViewHolder(view);
    }

    public class HomeGameBinderViewHolder extends ExtSaveStateViewHolder<ObjectSearchAllGame> {
        SearchAllGameAdapter adapter = null;

        @BindView(R.id.rvOnlyRecyclerView)
        RecyclerView rvOnlyRecyclerView;

        public HomeGameBinderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return rvOnlyRecyclerView.getLayoutManager();
        }

        @Override
        public void onHolderCreated() {
            adapter = new SearchAllGameAdapter(itemView.getContext(), null, onClickItem);

            rvOnlyRecyclerView.setAdapter(adapter);
            rvOnlyRecyclerView.setHasFixedSize(true);
            rvOnlyRecyclerView.addItemDecoration(new InSpacelayoutDecoration(itemView.getContext()));
        }

        @Override
        public void onBindData(ObjectSearchAllGame o) {

            if (adapter != null) {
                adapter.setNewData(o.listGame);
            }
        }
    }
}

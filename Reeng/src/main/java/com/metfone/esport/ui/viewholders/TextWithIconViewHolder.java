package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.esport.ui.searchs.adapters.ObjectSuggestSearch;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TextWithIconViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.ivIconImage)
    AppCompatImageView ivIconImage;

    @BindView(R.id.tvContent)
    AppCompatTextView tvContent;

    public TextWithIconViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_text_with_icon, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

//    public TextTitleViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//        ButterKnife.bind(this, itemView);
//    }

    public void binData(ObjectSuggestSearch entity, ClickListener clickListener) {
        tvContent.setText(entity.text);

        if (entity.iconId > 0) {
            ivIconImage.setVisibility(View.VISIBLE);

            ivIconImage.setImageResource(entity.iconId);
        } else {
            ivIconImage.setVisibility(View.GONE);
        }

        itemView.setOnClickListener(v -> {
            if (clickListener != null)
                clickListener.onClickItem(entity);
        });
    }

    public interface ClickListener {
        public void onClickItem(ObjectSuggestSearch entity);
    }
}

package com.metfone.esport.ui.account;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.customview.AvatarView;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.util.ImageUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

import static com.metfone.esport.common.Common.handleException;

/**
 * Created by Nguyễn Thành Chung on 9/6/20.
 */
class SwitchAccountAdapter extends BaseListAdapter<ChannelInfoResponse, SwitchAccountAdapter.SwitchAccountViewHolder> {

    private OnClickItem onClickItem;

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public SwitchAccountAdapter(Context context) {
        super(context);
    }

    @NotNull
    @Override
    public SwitchAccountViewHolder onCreateViewHolder(@NotNull ViewGroup viewGroup, int i) {
        return new SwitchAccountViewHolder(LayoutInflater.from(context).inflate(R.layout.item_switch_account, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NotNull SwitchAccountViewHolder switchAccountViewHolder, int i) {
        ChannelInfoResponse entity = mData.get(i);
        switchAccountViewHolder.binData(entity, i);
        switchAccountViewHolder.itemView.setOnClickListener((View.OnClickListener) view -> {
            if (onClickItem!=null){
                for (ChannelInfoResponse mDatum : mData) {
                    mDatum.isSelect = false;
                }
                entity.isSelect = true;
                notifyDataSetChanged();
                onClickItem.onClickItem(entity,i);
            }
        });
    }

    @Override
    public Filter getFilter() {
        return null;
    }


    static class SwitchAccountViewHolder extends BaseViewHolder<ChannelInfoResponse> {
        @BindView(R.id.ivLogoTeam)
        AvatarView ivLogoTeam;
        @BindView(R.id.TeamName)
        AppCompatTextView TeamName;
        @BindView(R.id.tvTypeAccount)
        AppCompatTextView tvTypeAccount;
        @BindView(R.id.ivCheck)
        AppCompatImageView ivCheck;

        public SwitchAccountViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void binData(ChannelInfoResponse entity, int position) {
            itemView.setBackgroundColor(Color.parseColor("#161819"));
            tvTypeAccount.setVisibility(View.VISIBLE);
            tvTypeAccount.setText(position == 0 ? context.getString(R.string.camid_account_es) : context.getString(R.string.channel_account_es));
            TeamName.setText(entity.name);
            ivCheck.setVisibility(entity.isSelect ? View.VISIBLE : View.GONE);
//            ivLogoTeam.setAvatarUrl(entity.urlAvatar);
            if (StringUtils.isNotEmpty(entity.getUrlAvatar())){
                if (entity.getUrlAvatar().length() > 256){
                    Glide.with(context)
                            .load(ImageUtils.convertBase64ToBitmap(entity.getUrlAvatar()))
                            .transform(new MultiTransformation<Bitmap>(new CenterCrop(), new CircleCrop()))
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(ivLogoTeam);
                }else {
                    if (position == 0){
                        showAvatarCamid();
                    }else {
                        ivLogoTeam.setAvatarUrl(entity.getUrlAvatar());
                    }
                }
            }else {
                showAvatarCamid();
            }


        }

        private void showAvatarCamid() {
            try {
                AccountRankDTO account = ApplicationController.self().getAccountRankDTO();
                KhUserRank myRank = KhUserRank.getById(account.rankId);
                Glide.with(context)
                        .load(myRank.resAvatar)
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivLogoTeam);
            } catch (Exception e) {
                handleException(e);
            }
        }
    }

    public interface OnClickItem{
        void onClickItem(ChannelInfoResponse entity,int position);
    }

}

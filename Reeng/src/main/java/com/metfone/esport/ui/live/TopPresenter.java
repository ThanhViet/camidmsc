package com.metfone.esport.ui.live;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.player.JZUtils;
import com.metfone.esport.entity.ContentParam;
import com.metfone.esport.entity.GiftBalanceModel;
import com.metfone.esport.entity.SocketModel;
import com.metfone.esport.entity.camid.CamIdUserResponse;
import com.metfone.esport.entity.enums.EActionType;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.DataCommentResponse;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.entity.request.ChannelInfoRequest;
import com.metfone.esport.entity.request.UserInfoRequest;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.HttpHelper;
import com.metfone.esport.helper.encrypt.EncryptUtil;
import com.metfone.esport.listener.PostMessageSocketListener;
import com.metfone.esport.network.socket.SocketManager;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 10/11/20.
 */
public class TopPresenter extends BasePresenter<TopFragment, BaseModel> {
    public TopPresenter(TopFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public static SocketModel createLiveComment(int type, String content, String roomId, VideoInfoResponse media, GiftBalanceModel gift) {
        SocketModel model = new SocketModel();
        String userId = String.valueOf(AccountBusiness.getInstance().getUserId());
        String token = AccountBusiness.getInstance().getToken();
        long timestamp = System.currentTimeMillis();
        String security = HttpHelper.encryptDataLiveComment(userId, token, timestamp);
        model.type = type;
        model.roomId = roomId;
        model.security = security;
        ListCommentEntity chatMessage = new ListCommentEntity();
        chatMessage.setId(EncryptUtil.encryptMD5(userId + timestamp));
        chatMessage.setUserId(userId);
        String avatar = Common.getAvatarAccount().length() > 256 ? "" : Common.getAvatarAccount();
        chatMessage.setAvatar(avatar);
        chatMessage.setRoomId(roomId);
        chatMessage.setContent(content);
        chatMessage.setTimestamp(timestamp);
        chatMessage.setNameSender(Common.getAvatarAccount());
        if (media != null) {
            chatMessage.setData(GsonUtils.toJson(media));
        } else if (gift != null) {
            chatMessage.setData(GsonUtils.toJson(gift));
        }
        model.chatMessage = chatMessage;
        return model;
    }

    public void sendCommentPastBroadcast(String message, VideoInfoResponse media) {
        ContentParam contentParam = new ContentParam();
        contentParam.url = media.link;
        String channelId = "";
        String jsonChannelInfo = "";
        if (SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
            channelId = String.valueOf(AccountBusiness.getInstance().getChannelInfo().id);
            jsonChannelInfo =  GsonUtils.toJson(getChannelInfoRequest());
        }
        RxUtils.async(compositeDisposable, apiService.actionApp(
                String.valueOf(AccountBusiness.getInstance().getUserId()),
                GsonUtils.toJson(contentParam),
                media.link,
                "postActionFrom",
                message,
                1,
                "tag",
                "",
                0,
                GsonUtils.toJson(getUserInfoRequest()),
                channelId,
                jsonChannelInfo
        ), new ICallBackResponse<Object>() {

            @Override
            public void onSuccess(Object response) {

            }

            @Override
            public void onFail(String error) {

            }
        });
    }


    private UserInfoRequest getUserInfoRequest() {
        return new UserInfoRequest(String.valueOf(AccountBusiness.getInstance().getUserId()),
                0,
                Common.getNameCamId(),
                Common.getNameCamId().isEmpty() ? "Profile channel" : Common.getNameCamId(),
                Common.getAvatarAccount());
    }



    private ChannelInfoRequest getChannelInfoRequest(){
        return new ChannelInfoRequest(
                String.valueOf(AccountBusiness.getInstance().getChannelInfo().id),
                AccountBusiness.getInstance().getChannelInfo().name,
                StringUtils.isNotEmpty(AccountBusiness.getInstance().getChannelInfo().getUrlImages()) ? AccountBusiness.getInstance().getChannelInfo().getUrlImages() : "test"
        );
    }

    public void sendComment(String comment, VideoInfoResponse media) {
        if (StringUtils.isEmpty(comment) || media == null) return;
        SocketManager.getInstance().sendMessage(GsonUtils.toJson(createLiveComment(SocketModel.TYPE_MESSAGES, comment, String.valueOf(media.id), media, null)), new PostMessageSocketListener() {

            @Override
            public void onPostMessageSuccess() {

            }

            @Override
            public void onPostMessageFailure() {

            }
        });
    }

    public void likeVideo(Integer like, VideoInfoResponse media) {
        RxUtils.async(compositeDisposable, apiService.likeVideo(
                media.link,
                like == 1 ? EActionType.LIKE.name() : EActionType.UNLIKE.name(),
                AccountBusiness.getInstance().getUserId()
        ), new ICallBackResponse<Object>() {

            @Override
            public void onSuccess(Object response) {

            }

            @Override
            public void onFail(String error) {

            }
        });
    }

    public void getComments(String url,String rowStart) {
        RxUtils.async(compositeDisposable, apiService.getComments(rowStart, 20, url, getDefaultParam())
                , new ICallBackResponse<DataCommentResponse>() {
                    @Override
                    public void onSuccess(DataCommentResponse response) {
                        view.onSuccessCommentList(response.listComment);
                    }

                    @Override
                    public void onFail(String error) {

                    }
                });

    }


    public void getLogView(String uid,String videoId,String mediaLink,long timePlay,String gameId){
        try{

            RxUtils.async(compositeDisposable, apiService.logView(
                    uid,
                    getDomain(),
                    System.currentTimeMillis(),
                    "CDR",
                    Common.getIpAddress(context),
                    videoId,
                    mediaLink,
                    timePlay,
                    String.valueOf(timePlay),
                    Constant.REVISION,
                    Constant.CLIENT_TYPE,
                    "0",
                    gameId,
                    JZUtils.isWifiConnected(context) ? "wifi" : "cellular"

            ), new ICallBackResponse<Object>() {
                @Override
                public void onSuccess(Object response) {

                }

                @Override
                public void onFail(String error) {

                }
            });
        }catch (Exception e){
            Common.handleException(e);
        }
    }

}

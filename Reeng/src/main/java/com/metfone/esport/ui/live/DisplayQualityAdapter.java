package com.metfone.esport.ui.live;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.entity.ResolutionVideo;
import com.metfone.selfcare.R;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * Created by Nguyễn Thành Chung on 9/2/20.
 */
public class DisplayQualityAdapter extends BaseListAdapter<ResolutionVideo, DisplayQualityAdapter.DisplayQualityViewHolder> {

    public OnClickItem onClickItem;

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public DisplayQualityAdapter(Context context) {
        super(context);
    }

    @NotNull
    @Override
    public DisplayQualityViewHolder onCreateViewHolder(@NotNull ViewGroup viewGroup, int i) {
        return new DisplayQualityViewHolder(LayoutInflater.from(context).inflate(R.layout.item_display_quality, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NotNull DisplayQualityViewHolder displayQualityViewHolder, int i) {
        ResolutionVideo entity = mData.get(i);
        displayQualityViewHolder.itemView.setOnClickListener(view -> {
            if (onClickItem!=null){
                onClickItem.onClickItem(entity);
            }
        });
        displayQualityViewHolder.binData(entity, i);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    static class DisplayQualityViewHolder extends BaseViewHolder<ResolutionVideo>  {
        @BindView(R.id.tvDisplay)
        TextView tvDisplay;
        @BindView(R.id.tvHighSolution)
        TextView tvHighSolution;
        public DisplayQualityViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void binData(ResolutionVideo entity, int position) {
            tvDisplay.setText(entity.title);
            if (entity.key.equalsIgnoreCase(EDisplayQuality.DISPLAY_QUALITY_1080.status)){
                tvHighSolution.setText("HD");
                tvHighSolution.setTextColor(ContextCompat.getColor(context,R.color.red_live));
            }else {
                tvHighSolution.setText("");
                tvHighSolution.setTextColor(ContextCompat.getColor(context,R.color.white));
            }
        }



    }
    public interface OnClickItem{
        void onClickItem(ResolutionVideo entity);
    }
}

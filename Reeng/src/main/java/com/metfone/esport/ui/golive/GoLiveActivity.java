package com.metfone.esport.ui.golive;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.metfone.selfcare.util.Utilities;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.metfone.esport.base.BaseActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.customview.player.Jzvd;
import com.metfone.esport.customview.player.JzvdStd;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung on 10/15/20.
 */
public class GoLiveActivity extends BaseActivity {
    @BindView(R.id.btn_back)
    AppCompatImageView btnBack;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.layout_action_bar)
    RelativeLayout layoutActionBar;
    @BindView(R.id.tvTitleGoLive)
    TextView tvTitleGoLive;
    @BindView(R.id.tvDescriptionFirst)
    TextView tvDescriptionFirst;
    @BindView(R.id.ivAppStore)
    AppCompatImageView ivAppStore;
    @BindView(R.id.ivAppGoogle)
    AppCompatImageView ivAppGoogle;
    @BindView(R.id.tvDescriptionSecond)
    TextView tvDescriptionSecond;
    @BindView(R.id.youtube_player_view)
    YouTubePlayerView youtube_player_view;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @Override
    public Object getPresenter() {
        return null;
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_go_live_es;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Common.setColorStatusBar(this, Color.parseColor("#C41627"));
        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        Utilities.adaptViewForInsertBottom(rootView);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layoutActionBar.getLayoutParams();
        params.topMargin = Common.getStatusBarHeight(this) + getResources().getDimensionPixelOffset(R.dimen._5sdp);
        layoutActionBar.setLayoutParams(params);
        getLifecycle().addObserver(youtube_player_view);
        tvTitleGoLive.setText(Html.fromHtml(getResources().getString(R.string.title_go_live)));
        tvDescriptionSecond.setText(Html.fromHtml(getResources().getString(R.string.description_second)));
//        youtube_player_view.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
//            @Override
//            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
//                String videoId = "7jQipab-Ddc";
//                youTubePlayer.loadVideo(videoId, 0);
//            }
//        });
//        youtube_player_view.setVisibility(View.GONE);

    }



    @OnClick({R.id.btn_back, R.id.ivAppStore, R.id.ivAppGoogle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.ivAppStore:
                break;
            case R.id.ivAppGoogle:
                String appPackage = getPackageName();
                String url = "market://details?id=" + appPackage;
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackage)));
                }
                break;
        }
    }
}

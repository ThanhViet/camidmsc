package com.metfone.esport.ui.searchs.contents;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.drakeet.multitype.MultiTypeAdapter;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.entity.enums.SearchTabTypeEnum;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.homes.adapters.ExtSaveStateMultiTypeAdapter;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.games.GamesContainerFragment;
import com.metfone.esport.ui.games.tournamentdetails.TournamentDetailFragment;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchAllGame;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchLive;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchPastBroadCast;
import com.metfone.esport.ui.searchs.adapters.ObjectSearchUploaded;
import com.metfone.esport.ui.searchs.adapters.ObjectTitle;
import com.metfone.esport.ui.searchs.adapters.SearchAllGameItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchChannelItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchDefaultDecoration;
import com.metfone.esport.ui.searchs.adapters.SearchGameDecoration;
import com.metfone.esport.ui.searchs.adapters.SearchGamesItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchLiveStreamItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchPastBroadcastItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchTextTitleItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchTournamentItemViewBinder;
import com.metfone.esport.ui.searchs.adapters.SearchUploadedItemViewBinder;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.esport.ui.viewholders.MyListChanelViewHolder;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class SearchContentFragment extends BaseFragment<SearchContentPresenter> {

    private static String KEY_TYPE = "KEY_TYPE";

    private static int GAME_SPAN_COUNT = 3;
    private static int DEFAULT_SPAN_COUNT = 1;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.rvSearchContent)
    RecyclerView rvSearchContent;

    private MultiTypeAdapter adapter = new ExtSaveStateMultiTypeAdapter();

    private SearchTabTypeEnum typeEnum = SearchTabTypeEnum.ALL;

    private IActionListener callBack = null;

    private Boolean isLoading = false;

    private Boolean isCanLoadMore = true;

    private int currentPage = 0;

    private final ArrayList<Object> items = new ArrayList<>();

    public static SearchContentFragment newInstance(SearchTabTypeEnum type) {
        SearchContentFragment fragment = new SearchContentFragment();
        fragment.setArguments(newBundle(type));
        return fragment;
    }

    public static Bundle newBundle(SearchTabTypeEnum type) {
        Bundle args = new Bundle();
        args.putString(KEY_TYPE, type.name());
        return args;
    }

    @Override
    public SearchContentPresenter getPresenter() {
        return new SearchContentPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_content_layout;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            try {
                typeEnum = SearchTabTypeEnum.valueOf(getArguments().getString(KEY_TYPE, SearchTabTypeEnum.ALL.name()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (getParentFragment() instanceof IActionListener) {
            callBack = (IActionListener) getParentFragment();
        }

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();
    }

    public void refreshData() {
        resetState();

        loadData();
    }

    public void resetState() {
        currentPage = 0;

        isCanLoadMore = true;

        items.clear();
    }

    public void loadData() {
        if (callBack != null) {
            callBack.onChildFragmentLoadData(typeEnum, currentPage);
        }
    }

    private void initRecyclerView() {
        adapter.register(ObjectTitle.class, new SearchTextTitleItemViewBinder());
        adapter.register(ObjectSearchLive.class, new SearchLiveStreamItemViewBinder(new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }

            //            @Override
//            public void onClickOption() {
//                ArrayList<ExtKeyPair<?, String>> actionList = new ArrayList<>();
//                actionList.add(new ExtKeyPair<>(SHARE, getString(SHARE.textId), SHARE.iconId));
////                actionList.add(new ExtKeyPair<>(REMOVE_FROM_LIST, getString(REMOVE_FROM_LIST.textId), REMOVE_FROM_LIST.iconId));
//
//                new SingleSelectBottomSheet()
//                        .setOptions(actionList)
//                        .setOnClickItem(e -> {
//                            if (e.key instanceof MyListVideoActionEnum)
//                                switch ((MyListVideoActionEnum) e.key) {
//                                    case SHARE:
//                                        break;
////                                    case REMOVE_FROM_LIST:
////                                        //TODO call api
////                                        adapter.delete(entity);
////                                        break;
//                                    default:
//                                        break;
//                                }
//                        })
//                        .show(getChildFragmentManager(), null);
//            }
        }));
//        adapter.register(ObjectSearchChannelLike.class, new SearchChannelLiveItemViewBinder());
        adapter.register(ObjectSearchPastBroadCast.class, new SearchPastBroadcastItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(ObjectSearchUploaded.class, new SearchUploadedItemViewBinder(new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }
        }));
        adapter.register(ChannelInfoResponse.class, new SearchChannelItemViewBinder(new MyListChanelViewHolder.IOnClickItem() {
            @Override
            public void onClickItem(ChannelInfoResponse channel) {
                navigateToChannel(channel);
            }

            @Override
            public void onClickOption(ChannelInfoResponse channel) {
                if (channel != null) {
                    new SingleSelectBottomSheet()
                            .setOptions(CollectionUtils.newArrayList(new ExtKeyPair("",
                                    getString(channel.isFollow == 1 ? R.string.unfollow : R.string.follow),
                                    channel.isFollow == 1 ? R.drawable.ic_favorite_white_es : R.drawable.ic_favorite_red_es)))
                            .setOnClickItem(e -> {
                                if (!isLogin()) {
                                    showDialogLogin();

                                    return;
                                }

                                if (channel.isFollow == 1) {
                                    channel.isFollow = 0;
                                    channel.numFollow--;
                                } else {
                                    channel.isFollow = 1;
                                    channel.numFollow++;
                                }

                                presenter.followChannel(channel.id, channel.isFollow);

                                int index = adapter.getItems().indexOf(channel);

                                if (index > -1)
                                    adapter.notifyItemChanged(index);

                            })
                            .show(getChildFragmentManager(), null);
                }
            }
        }));
        adapter.register(GameInfoResponse.class, new SearchGamesItemViewBinder(game -> {
            if (game != null)
                AloneFragmentActivity.with(getActivity())
                        .parameters(GamesContainerFragment.newBundle(game.id))
                        .start(GamesContainerFragment.class);
        }));
        adapter.register(ObjectSearchAllGame.class, new SearchAllGameItemViewBinder(game -> {
            if (game != null)
                AloneFragmentActivity.with(getActivity())
                        .parameters(GamesContainerFragment.newBundle(game.id))
                        .start(GamesContainerFragment.class);
        }));
        adapter.register(TournamentInfoResponse.class, new SearchTournamentItemViewBinder((TournamentInfoResponse entity) -> {
            AloneFragmentActivity.with(getContext()).parameters(TournamentDetailFragment.newBundle(entity.id))
                    .start(TournamentDetailFragment.class);
        }));

        adapter.setItems(items);

        if (typeEnum == SearchTabTypeEnum.GAME) {
            rvSearchContent.setLayoutManager(new GridLayoutManager(getContext(), GAME_SPAN_COUNT, RecyclerView.VERTICAL, false));
            rvSearchContent.addItemDecoration(new SearchGameDecoration(getContext(), GAME_SPAN_COUNT));
        } else {
            rvSearchContent.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            rvSearchContent.addItemDecoration(new SearchDefaultDecoration(getContext()));
        }

        rvSearchContent.setAdapter(adapter);

        if (typeEnum != SearchTabTypeEnum.ALL)
            rvSearchContent.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                    if (!isLoading && isCanLoadMore) {
                        if (linearLayoutManager != null && CollectionUtils.size(items) != 0 && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(items) - 1) {
                            //bottom of list!
                            currentPage++;

                            loadData();
                        }
                    }
                }
            });
    }

    private void navigateToChannel(ChannelInfoResponse channel) {
        if (channel != null)
            AloneFragmentActivity.with(getActivity())
                    .parameters(ChannelContainerFragment.newBundle(channel.id))
                    .start(ChannelContainerFragment.class);
    }

    public void showLoading() {
        isLoading = true;

        if (CollectionUtils.isEmpty(items))
            multipleStatusView.showLoading();
    }

    public void addAndShowData(ArrayList<Object> items) {
        isLoading = false;

        if (CollectionUtils.isNotEmpty(items)) {
            this.items.addAll(items);

            adapter.notifyDataSetChanged();

            multipleStatusView.showContent();

            isCanLoadMore = items.size() >= BasePresenter.DEFAULT_LIMIT;
        } else {
            multipleStatusView.showEmpty();

            isCanLoadMore = false;
        }
    }

    public interface IActionListener {
        void onChildFragmentLoadData(SearchTabTypeEnum typeEnum, int currentPage);
    }
}

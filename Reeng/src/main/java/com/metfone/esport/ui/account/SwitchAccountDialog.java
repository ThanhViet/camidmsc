package com.metfone.esport.ui.account;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseDialogFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.event.ChangeAccountEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.ui.createchannel.CreateChannelFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;

import static com.metfone.esport.common.Common.handleException;

/**
 * Created by Nguyễn Thành Chung on 9/6/20.
 */
public class SwitchAccountDialog extends BaseDialogFragment {
    @BindView(R.id.ivAddAccount)
    AppCompatImageView ivAddAccount;
    @BindView(R.id.rvAccount)
    RecyclerView rvAccount;
    private ArrayList<ChannelInfoResponse> myChannels;

    public static SwitchAccountDialog newInstance(ArrayList<ChannelInfoResponse> myChannels) {
        SwitchAccountDialog fragment = new SwitchAccountDialog();
        fragment.myChannels = myChannels;
        return fragment;
    }



    @Override
    protected void initView(View rootView) {
        try{
            initRecyclerAccount();
            initListener();
        }catch (Exception e){
            handleException(e);
        }
    }




    private void initListener() {
        try{
            ivAddAccount.setOnClickListener(view -> {
                AloneFragmentActivity.with(getContext()).start(CreateChannelFragment.class);
                dismiss();
            });
        }catch (Exception e){
            handleException(e);
        }
    }

    private void initRecyclerAccount() {
        try{
            if (AccountBusiness.getInstance().getMyChannels()== null){
                myChannels = new ArrayList<>();
            }else {
                myChannels = new ArrayList<>(AccountBusiness.getInstance().getMyChannels());
            }
            AccountRankDTO account = null;
            try {
                account = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_ACCOUNT_RANK_KH, AccountRankDTO.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getActivity());
            UserInfo currentUser = userInfoBusiness.getUser();
            String name = currentUser.getFull_name();
            ChannelInfoResponse channelInfoResponse = new ChannelInfoResponse();
            channelInfoResponse.name = name;
            channelInfoResponse.isSelect = true;
            channelInfoResponse.id = 0L;
            channelInfoResponse.urlAvatar = currentUser.getAvatar();
            if (account == null){
                channelInfoResponse.categoryName = getString(R.string.kh_user_rank_member);
            }else {
                channelInfoResponse.categoryName = account.rankName == null || account.rankName.equalsIgnoreCase("") ?  getString(R.string.kh_user_rank_member) : account.rankName;
                channelInfoResponse.id = account.vtAccId;
            }
            myChannels.add(0,channelInfoResponse);
            if (SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                for (ChannelInfoResponse myChannel : myChannels) {
                    if (myChannel.id.equals(AccountBusiness.getInstance().getChannelInfo().id)) {
                        myChannel.urlAvatar = AccountBusiness.getInstance().getChannelInfo().urlAvatar;
                        myChannel.name = AccountBusiness.getInstance().getChannelInfo().name;
                        myChannel.description = AccountBusiness.getInstance().getChannelInfo().description;
                        myChannel.urlImages = AccountBusiness.getInstance().getChannelInfo().urlImages;
                        myChannel.isSelect = true;
                        myChannels.get(0).isSelect = false;
                        break;
                    }else {
                        myChannel.isSelect = false;
                    }
                }
            }


            rvAccount.setLayoutManager(new LinearLayoutManager(getContext()));
            rvAccount.setHasFixedSize(true);
            SwitchAccountAdapter adapter = new SwitchAccountAdapter(getContext());
            adapter.setOnClickItem((entity, position) -> {
                if (position == 0) {
                    SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, false);

                    AccountBusiness.getInstance().saveCurrentChannel(null);
                }else {
                    SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL,true);
                    processSwitchAccount(entity);
                }
                EventBus.getDefault().post(new ChangeAccountEvent());
                dismiss();
            });
            adapter.setNewData(myChannels);
            rvAccount.setAdapter(adapter);
        }catch (Exception e){
            handleException(e);
        }

    }

    private void processSwitchAccount(ChannelInfoResponse entity) {
        try{
            AccountBusiness.getInstance().saveCurrentChannel(entity);
        }catch (Exception e){
            Common.handleException(e);
        }
    }

    @Override
    protected int getDialogWidth() {
        return ScreenUtils.getScreenWidth() - getResources().getDimensionPixelOffset(R.dimen._60sdp);
    }



    @Override
    protected int getLayout() {
        return R.layout.dialog_switch_account;
    }

    @Override
    public String getTAG() {
        return null;
    }

}

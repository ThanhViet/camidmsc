package com.metfone.esport.ui.mychannel.tabpastbroadcast;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.enums.MyChannelVideoActionEnum;
import com.metfone.esport.entity.enums.SortEnum;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.editvideo.EditVideoFragment;
import com.metfone.esport.ui.mychannel.WarningEditVideoDialog;
import com.metfone.esport.ui.mychannel.tabpastbroadcast.adapter.TabPastBroadcastMyChannelAdapter;
import com.metfone.esport.ui.mychannel.tabpastbroadcast.adapter.TabPastBroadcastMyChannelDecoration;
import com.metfone.esport.ui.popups.SingleTextSelectPopup;
import com.metfone.esport.ui.viewholders.HalfVideoWithOptionViewHolder;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.entity.enums.MyChannelVideoActionEnum.DELETE;
import static com.metfone.esport.entity.enums.MyChannelVideoActionEnum.EDIT;
import static com.metfone.esport.entity.enums.MyChannelVideoActionEnum.SHARE;

public class TabPastBroadcastMyChannelFragment extends BaseFragment<TabPastBroadcastMyChannelPresenter> {

    @BindView(R.id.tvVideoCount)
    AppCompatTextView tvVideoCount;

    @BindView(R.id.rvMyListVideo)
    RecyclerView rvMyListVideo;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.lnSort)
    LinearLayout lnSort;

    private TabPastBroadcastMyChannelAdapter adapter;
    private ArrayList<VideoInfoResponse> items = new ArrayList<>();
    private long channelId = 0;
    private int currentPage = 0;
    private int currentFilter = Constant.FILTER.NEWEST;
    private SortEnum currentSort = SortEnum.MANUAL;
    private ArrayList<ExtKeyPair<?, String>> listSort = new ArrayList<>();
    private ArrayList<ExtKeyPair<?, String>> actionList = new ArrayList<>();

    public static TabPastBroadcastMyChannelFragment newInstance(Bundle bundle) {
        TabPastBroadcastMyChannelFragment fragment = new TabPastBroadcastMyChannelFragment();
        if (bundle != null) fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public TabPastBroadcastMyChannelPresenter getPresenter() {
        return new TabPastBroadcastMyChannelPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_list_video;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, Constant.TIME_OFF_SWIPE_REFRESH);
            refreshData();
        });
        multipleStatusView.setOnRetryClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                refreshData();
            }
        });
        initRecyclerView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Serializable serializable = bundle.getSerializable(Constant.KEY_DATA);
            if (serializable instanceof ChannelInfoResponse) {
                ChannelInfoResponse channelInfo = (ChannelInfoResponse) serializable;
                channelId = channelInfo.id;
            }
        }
        refreshData();
        initSort();
        actionList.clear();
        actionList.add(new ExtKeyPair<>(EDIT, getString(EDIT.textId), EDIT.iconId));
        actionList.add(new ExtKeyPair<>(DELETE, getString(DELETE.textId), DELETE.iconId));
        actionList.add(new ExtKeyPair<>(SHARE, getString(SHARE.textId), SHARE.iconId));
    }

    private void refreshData() {
        Log.d(TAG, "refreshData");
        if (CollectionUtils.isEmpty(items)) multipleStatusView.showLoading();
        currentPage = 0;
        presenter.getData(channelId, currentPage, currentFilter);
    }

    private void loadMoreData() {
        if (!presenter.isLoading() && presenter.isCanLoadMore()) {
            currentPage++;
            Log.d(TAG, "loadMoreData: " + currentPage);
            presenter.getData(channelId, currentPage, currentFilter);
        }
    }

    private void initRecyclerView() {
        rvMyListVideo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(items) - 1) {
                        //bottom of list!
                        loadMoreData();
                    }
                }
            }
        });
        adapter = new TabPastBroadcastMyChannelAdapter(getContext(), items);
        adapter.setOnClickItem(new HalfVideoWithOptionViewHolder.IOnClickItem() {

            @Override
            public void onClickItem(VideoInfoResponse entity) {
                Utilities.openPlayer(getParentActivity(), entity);
            }

            @Override
            public void onClickOption(VideoInfoResponse entity) {
                new SingleSelectBottomSheet()
                        .setOptions(actionList)
                        .setOnClickItem(e -> {
                            if (e.key instanceof MyChannelVideoActionEnum)
                                switch ((MyChannelVideoActionEnum) e.key) {
                                    case SHARE:
                                        onClickShare(entity.link);
                                        break;
                                    case EDIT:
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(Constant.KEY_DATA, entity);
                                        AloneFragmentActivity.with(getParentActivity()).parameters(bundle).start(EditVideoFragment.class);
                                        break;
                                    case DELETE: {
                                        WarningEditVideoDialog dialog = WarningEditVideoDialog.newInstance(getString(R.string.title_delete_video), getString(R.string.msg_delete_video));
                                        dialog.setCancelable(true);
                                        dialog.setPositiveListener(result -> {
//                                            if (adapter != null) adapter.delete(entity);
                                            if (items != null) items.remove(entity);
                                            if (adapter != null) adapter.notifyDataSetChanged();
                                            if (presenter != null) presenter.delete(entity);
                                            if (dialog != null)
                                                dialog.dismissAllowingStateLoss();
                                        });
                                        dialog.setNegativeListener(result -> {
                                            if (dialog != null)
                                                dialog.dismissAllowingStateLoss();
                                        });
                                        dialog.show(getChildFragmentManager());
                                    }
                                    break;
                                    default:
                                        break;
                                }
                        })
                        .show(getChildFragmentManager(), null);
            }
        });
        rvMyListVideo.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvMyListVideo.setHasFixedSize(true);
        rvMyListVideo.addItemDecoration(new TabPastBroadcastMyChannelDecoration(getContext()));
        rvMyListVideo.setAdapter(adapter);
    }

    public void onGetDataSuccess(ArrayList<VideoInfoResponse> response) {
        if (currentPage == 0 && items != null) items.clear();
        if (CollectionUtils.isNotEmpty(response) && items != null) {
            items.addAll(response);
        }
        int size = CollectionUtils.size(items);
        if (tvVideoCount != null)
            tvVideoCount.setText(getString((size > 1) ? R.string.total_videos_es : R.string.total_video_es, size));
        if (CollectionUtils.isEmpty(items)) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();
            if (rvMyListVideo != null) rvMyListVideo.setVisibility(View.INVISIBLE);
            if (tvVideoCount != null) tvVideoCount.setVisibility(View.GONE);
            if (lnSort != null) lnSort.setVisibility(View.GONE);
            if (adapter != null) {
                adapter.clear();
                adapter.notifyDataSetChanged();
            }
        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();
            if (rvMyListVideo != null) rvMyListVideo.setVisibility(View.VISIBLE);
            if (tvVideoCount != null) tvVideoCount.setVisibility(View.VISIBLE);
            if (lnSort != null) lnSort.setVisibility(View.VISIBLE);
            if (adapter != null) adapter.setNewData(items);
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(items)) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showError();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }
            if (rvMyListVideo != null) rvMyListVideo.setVisibility(View.INVISIBLE);
        }
    }

    private void initSort() {
        listSort.clear();
        listSort.add(new ExtKeyPair<>(SortEnum.DATE_ADDED_NEWEST, getString(SortEnum.DATE_ADDED_NEWEST.textId)));
        listSort.add(new ExtKeyPair<>(SortEnum.DATE_ADDED_OLDEST, getString(SortEnum.DATE_ADDED_OLDEST.textId)));
        listSort.add(new ExtKeyPair<>(SortEnum.VIEWERS_HIGHEST, getString(SortEnum.VIEWERS_HIGHEST.textId)));
        listSort.add(new ExtKeyPair<>(SortEnum.VIEWERS_LOWEST, getString(SortEnum.VIEWERS_LOWEST.textId)));

        lnSort.setOnClickListener(v -> {
            new SingleTextSelectPopup(getContext(), e -> {
                SortEnum newSort = (SortEnum) e.key;
                if (newSort != currentSort) {
                    currentSort = newSort;
                    switch (currentSort) {
                        case DATE_ADDED_OLDEST:
                            currentFilter = Constant.FILTER.OLDEST;
                            break;
                        case VIEWERS_HIGHEST:
                            currentFilter = Constant.FILTER.MOST_VIEW;
                            break;
                        case VIEWERS_LOWEST:
                            currentFilter = Constant.FILTER.LEAST_VIEW;
                            break;
                        default:
                            currentFilter = Constant.FILTER.NEWEST;
                            break;
                    }
                    refreshData();
                }
            })
                    .setOptions(listSort)
                    .setCurrentSelected(new ExtKeyPair<>(currentSort))
                    .showAsDropDown(lnSort, 0, 16, Gravity.BOTTOM);
        });
    }
}

package com.metfone.esport.ui.searchs.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

public class SearchGameDecoration extends RecyclerView.ItemDecoration {
    private int space;

    private int spanCount = 1;

    public SearchGameDecoration(Context context, int spanCount) {
        space = context != null ? context.getResources().getDimensionPixelSize(R.dimen.pading_layout) : 0;

        this.spanCount = spanCount;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position = parent.getChildAdapterPosition(view);

        int column = position % spanCount;

        outRect.top = space / 2;

        outRect.left = space - column * space / spanCount;

        outRect.right = (column + 1) * space / spanCount;

//        if (column == 0)
//            outRect.left = space;
//        else
//            outRect.left = space / 2;

    }
}

package com.metfone.esport.ui.mychannel.tabpastbroadcast;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

class TabPastBroadcastMyChannelPresenter extends BasePresenter<TabPastBroadcastMyChannelFragment, BaseModel> {

    private Disposable disposable;
    private boolean isLoading = false;
    private boolean canLoadMore = false;

    public TabPastBroadcastMyChannelPresenter(TabPastBroadcastMyChannelFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public boolean isLoading() {
        return isLoading;
    }

    public boolean isCanLoadMore() {
        return canLoadMore;
    }

    public void getData(long channelId, final int currentPage, final int filterType) {
        if (currentPage == 0 && disposable != null) {
            disposable.dispose();
            isLoading = false;
        }
        if (currentPage > 0 && isLoading) return;
        canLoadMore = false;
        int limit = 20;
        int offset = currentPage * limit;
        isLoading = true;
        disposable = RxUtils.async(compositeDisposable, apiService.getPastBroadcastOfMyChannel(getDefaultParam(), channelId, filterType, offset, limit)
                , new ICallBackResponse<ArrayList<VideoInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<VideoInfoResponse> response) {
                        if (view != null) view.onGetDataSuccess(response);
                        isLoading = false;
                        if (CollectionUtils.size(response) >= limit) canLoadMore = true;
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                        isLoading = false;
                    }
                });
    }

    public void delete(VideoInfoResponse entity) {
        RxUtils.async(compositeDisposable, apiService.deleteVideo(String.valueOf(entity.channel_id),
                String.valueOf(entity.gameId),
                entity.id, System.currentTimeMillis(), ServiceRetrofit.getDefaultParam())
                , new ICallBackResponse<Object>() {

                    @Override
                    public void onSuccess(Object response) {

                    }

                    @Override
                    public void onFail(String error) {

                    }
                });
    }
}

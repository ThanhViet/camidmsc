package com.metfone.esport.ui.games.recentresults;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.entity.repsonse.GameMatchResponse;
import com.metfone.esport.entity.repsonse.GameRecentResultResponse;
import com.metfone.esport.homes.adapters.decorations.VerticalTopContentDecoration;
import com.metfone.esport.ui.games.recentresults.adapters.GameRecentResultAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class GameRecentResultFragment extends BaseFragment<GameRecentResultPresenter> {

    private static String KEY_GAME_ID = "KEY_GAME_ID";

    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;

    @BindView(R.id.rvGameResult)
    RecyclerView rvGameResult;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private GameRecentResultAdapter adapter;
    private Long gameId;

    public static GameRecentResultFragment newInstance(Long gameId) {
        GameRecentResultFragment fragment = new GameRecentResultFragment();
        fragment.setArguments(newBundle(gameId));
        return fragment;
    }

    public static Bundle newBundle(Long gameId) {

        Bundle args = new Bundle();

        args.putLong(KEY_GAME_ID, gameId);

        return args;
    }

    @Override
    public GameRecentResultPresenter getPresenter() {
        return new GameRecentResultPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game_recent_result;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();

        ivBack.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    private void getDataBundle() {
        if (getArguments() != null)
            gameId = getArguments().getLong(KEY_GAME_ID, 0);
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        getData();
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        multipleStatusView.showLoading();

        presenter.getData(gameId);
    }

    private void initRecyclerView() {
        adapter = new GameRecentResultAdapter(getContext(), null);

        rvGameResult.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvGameResult.setHasFixedSize(true);
        rvGameResult.addItemDecoration(new VerticalTopContentDecoration(getContext()));
        rvGameResult.setAdapter(adapter);
    }

    public void onGetDataSuccess(ArrayList<GameRecentResultResponse> response) {

        ArrayList<GameMatchResponse> list = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(response))
            for (GameRecentResultResponse gameRecentResultResponse : response) {
                if (CollectionUtils.isNotEmpty(gameRecentResultResponse.listGame))
                    list.addAll(gameRecentResultResponse.listGame);
            }

        if (CollectionUtils.isEmpty(list)) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvGameResult != null) rvGameResult.setVisibility(View.INVISIBLE);

        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvGameResult != null) rvGameResult.setVisibility(View.VISIBLE);

            adapter.setNewData(list);
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvGameResult != null) rvGameResult.setVisibility(View.INVISIBLE);
        }
    }
}

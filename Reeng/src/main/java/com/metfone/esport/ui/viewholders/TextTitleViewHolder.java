package com.metfone.esport.ui.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TextTitleViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvSingleTextTitle)
    AppCompatTextView tvOnlyTitle;

    public TextTitleViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int width) {
        super(layoutInflater.inflate(R.layout.item_single_text_title, viewGroup, false));

        if (width != 0) {
            ViewGroup.LayoutParams params = itemView.getLayoutParams();

            params.width = width;

            itemView.setLayoutParams(params);
        }

        ButterKnife.bind(this, itemView);
    }

//    public TextTitleViewHolder(@NonNull View itemView) {
//        super(itemView);
//
//        ButterKnife.bind(this, itemView);
//    }

    public void binData(String entity) {
        tvOnlyTitle.setText(entity);
    }
}

package com.metfone.esport.ui.channels.channelgamecategory;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.homes.adapters.decorations.VerticalTopContentDecoration;
import com.metfone.esport.ui.channels.channelgamecategory.adapters.ChannelGameByCategoryAdapter;
import com.metfone.esport.ui.viewholders.HalfVideoNoOptionViewHolder;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class ChannelGameCategoryFragment extends BaseFragment<ChannelGameCategoryPresenter> {

    private static String KEY_GAME_ID = "KEY_GAME_ID";
    private static String KEY_CHANNEL_ID = "KEY_CHANNEL_ID";
    private static String KEY_GAME_NAME = "KEY_GAME_NAME";

    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;

    @BindView(R.id.rvVideo)
    RecyclerView rvVideo;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.tvFragmentTitle)
    AppCompatTextView tvFragmentTitle;

    private ChannelGameByCategoryAdapter adapter;
    private Long gameId;
    private Long channelId;
    private String gameName;
    private int currentPage = 0;

    public static ChannelGameCategoryFragment newInstance(Long channelId, String gameName, Long gameId) {
        ChannelGameCategoryFragment fragment = new ChannelGameCategoryFragment();
        fragment.setArguments(newBundle(channelId, gameName, gameId));
        return fragment;
    }

    public static Bundle newBundle(Long channelId, String gameName, Long gameId) {

        Bundle args = new Bundle();

        args.putLong(KEY_CHANNEL_ID, channelId);
        args.putLong(KEY_GAME_ID, gameId);
        args.putString(KEY_GAME_NAME, gameName);

        return args;
    }

    @Override
    public ChannelGameCategoryPresenter getPresenter() {
        return new ChannelGameCategoryPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_channel_game_category;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDataBundle();

        tvFragmentTitle.setText(gameName);

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        refreshData();

        ivBack.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    private void getDataBundle() {
        if (getArguments() != null) {
            channelId = getArguments().getLong(KEY_CHANNEL_ID, 0);
            gameId = getArguments().getLong(KEY_GAME_ID, 0);
            gameName = getArguments().getString(KEY_GAME_NAME, "");
        }
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        adapter.clear();

        currentPage = 0;

        getData();
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(adapter.getData())) multipleStatusView.showLoading();

        presenter.getData(currentPage++, channelId, gameId);
    }

    private void initRecyclerView() {
        adapter = new ChannelGameByCategoryAdapter(getContext(), null, new HalfVideoNoOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), video);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channelInfoResponse) {

            }
        });

        rvVideo.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvVideo.setHasFixedSize(true);
        rvVideo.addItemDecoration(new VerticalTopContentDecoration(getContext()));
        rvVideo.setAdapter(adapter);
        rvVideo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(adapter.getData()) - 1) {
                        //bottom of list!
                        getData();
                    }
                }
            }
        });
    }

    public void onGetDataSuccess(ArrayList<VideoInfoResponse> response) {
        if (CollectionUtils.isNotEmpty(response)) {
            adapter.addAll(response);
        }

        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvVideo != null) rvVideo.setVisibility(View.INVISIBLE);
        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvVideo != null) rvVideo.setVisibility(View.VISIBLE);
        }
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvVideo != null) rvVideo.setVisibility(View.INVISIBLE);
        }
    }
}

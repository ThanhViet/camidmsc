package com.metfone.esport.ui.games.calendars.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.ButtonViewHolder;

import org.jetbrains.annotations.NotNull;

public class GameButtonItemViewBinder extends ItemViewBinder<ObjectGameButton, ButtonViewHolder> {

    private ButtonViewHolder.IOnClickItem onClickItem;

    public GameButtonItemViewBinder(ButtonViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @NotNull
    @Override
    public ButtonViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new ButtonViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull ButtonViewHolder viewHolder, ObjectGameButton o) {
        viewHolder.binData(o.buttonText, onClickItem);
    }
}

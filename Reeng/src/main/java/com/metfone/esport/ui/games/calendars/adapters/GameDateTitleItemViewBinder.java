package com.metfone.esport.ui.games.calendars.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;
import com.metfone.esport.ui.viewholders.GameCalendarDateTitleViewHolder;

import org.jetbrains.annotations.NotNull;

public class GameDateTitleItemViewBinder extends ItemViewBinder<ObjectGameDateTitle, GameCalendarDateTitleViewHolder> {

    @NotNull
    @Override
    public GameCalendarDateTitleViewHolder onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        return new GameCalendarDateTitleViewHolder(layoutInflater, viewGroup, 0);
    }

    @Override
    public void onBindViewHolder(@NotNull GameCalendarDateTitleViewHolder viewHolder, ObjectGameDateTitle o) {
        viewHolder.binData(o.title);
    }
}

/*
 * Copyright (c) admin created on 2020-09-12 10:26:44 PM
 */

/*
 * Created by admin on 2020/9/7
 */

package com.metfone.esport.ui.uploadVideo.upload.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.R;
import com.metfone.esport.entity.CategoryUploadModel;

import java.util.List;

public class SpinnerCategoryAdapter extends ArrayAdapter<CategoryUploadModel> {

    public SpinnerCategoryAdapter(@NonNull Context context, int resource, @NonNull List<CategoryUploadModel> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View itemView = super.getView(position, convertView, parent);
        AppCompatTextView tvTitle = itemView.findViewById(R.id.tv_title);
        CategoryUploadModel item = getItem(position);
        if (tvTitle != null && item != null) {
            tvTitle.setText(item.getTitle());
        }
        return itemView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View itemView = super.getDropDownView(position, convertView, parent);
        AppCompatTextView tvTitle = itemView.findViewById(R.id.tv_title);
        CategoryUploadModel item = getItem(position);
        if (tvTitle != null && item != null) {
            tvTitle.setText(item.getTitle());
        }
        return itemView;
    }
}

package com.metfone.esport.ui.mylists.mylistvideos;

import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.base.ExtKeyPair;
import com.metfone.esport.entity.enums.MyListVideoActionEnum;
import com.metfone.esport.entity.enums.SortEnum;
import com.metfone.esport.entity.event.OnLikeVideoEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.dialogs.SingleSelectBottomSheet;
import com.metfone.esport.ui.mylists.mylistvideos.adapters.MyListVideoAdapter;
import com.metfone.esport.ui.mylists.mylistvideos.adapters.MyListVideoDecoration;
import com.metfone.esport.ui.popups.SingleTextSelectPopup;
import com.metfone.esport.ui.viewholders.HalfVideoWithOptionViewHolder;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.esport.entity.enums.MyListVideoActionEnum.REMOVE_FROM_LIST;
import static com.metfone.esport.entity.enums.MyListVideoActionEnum.SHARE;

public class MyListVideoFragment extends BaseFragment<MyListVideoPresenter> {

    @BindView(R.id.tvVideoCount)
    AppCompatTextView tvVideoCount;

    @BindView(R.id.rvMyListVideo)
    RecyclerView rvMyListVideo;

    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.lnSort)
    LinearLayout lnSort;

    private SortEnum currentSort = SortEnum.DATE_ADDED_NEWEST;

    private ArrayList<ExtKeyPair<?, String>> listSort = new ArrayList<>();

    private MyListVideoAdapter adapter;

    private ArrayList<ExtKeyPair<?, String>> actionList = new ArrayList<>();

    private int currentPage = 0;

    public static MyListVideoFragment newInstance() {
        Bundle args = new Bundle();
        MyListVideoFragment fragment = new MyListVideoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public MyListVideoPresenter getPresenter() {
        return new MyListVideoPresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_list_video;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);

            refreshData();
        });

        initRecyclerView();

        initData();

        refreshData();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    /**
     * Khoi tao cac gia tri sort
     * Created_by @dchieu on 9/17/20
     */
    private void initData() {
        listSort.clear();
//        listSort.add(new ExtKeyPair<>(SortEnum.MANUAL, getString(SortEnum.MANUAL.textId)));
//        listSort.add(new ExtKeyPair<>(SortEnum.MOST_POPULAR, getString(SortEnum.MOST_POPULAR.textId)));
        listSort.add(new ExtKeyPair<>(SortEnum.DATE_ADDED_NEWEST, getString(SortEnum.DATE_ADDED_NEWEST.textId)));
        listSort.add(new ExtKeyPair<>(SortEnum.DATE_ADDED_OLDEST, getString(SortEnum.DATE_ADDED_OLDEST.textId)));

        lnSort.setOnClickListener(v -> {
            new SingleTextSelectPopup(getContext(), e -> {
                SortEnum newSort = (SortEnum) e.key;

                if (newSort != currentSort) {
                    currentSort = newSort;

                    refreshData();
                }
            })
                    .setOptions(listSort)
                    .setCurrentSelected(new ExtKeyPair<>(currentSort))
                    .showAsDropDown(lnSort, 0, 16, Gravity.BOTTOM);
        });

        actionList.clear();
        actionList.add(new ExtKeyPair<>(SHARE, getString(SHARE.textId), SHARE.iconId));
        actionList.add(new ExtKeyPair<>(REMOVE_FROM_LIST, getString(REMOVE_FROM_LIST.textId), REMOVE_FROM_LIST.iconId));
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        try {
            currentPage = 0;

            adapter.clear();

            getData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Call service lay du lieu
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(adapter.getData())) multipleStatusView.showLoading();

        presenter.getData(currentPage++, currentSort);
    }

    private void initRecyclerView() {
        adapter = new MyListVideoAdapter(getContext(), null, new HalfVideoWithOptionViewHolder.IOnClickItem() {
            @Override
            public void onClickItem(VideoInfoResponse entity) {
                if (getActivity() instanceof AppCompatActivity)
                    Utilities.openPlayer((AppCompatActivity) getActivity(), entity);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                if (channel != null)
                    AloneFragmentActivity.with(getActivity())
                            .parameters(ChannelContainerFragment.newBundle(channel.id))
                            .start(ChannelContainerFragment.class);
            }

            @Override
            public void onClickOption(VideoInfoResponse entity) {
                new SingleSelectBottomSheet()
                        .setOptions(actionList)
                        .setOnClickItem(e -> {
                            if (e.key instanceof MyListVideoActionEnum)
                                switch ((MyListVideoActionEnum) e.key) {
                                    case SHARE:
                                        onClickShare(entity.link);
                                        break;
                                    case REMOVE_FROM_LIST:
                                        presenter.likeVideo(entity.link);

                                        adapter.delete(entity);

                                        setViewTotalVideos();

                                        checkEmptyView();

                                        break;
                                    default:
                                        break;
                                }
                        })
                        .show(getChildFragmentManager(), null);
            }
        });

        rvMyListVideo.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvMyListVideo.setHasFixedSize(true);
        rvMyListVideo.addItemDecoration(new MyListVideoDecoration(getContext()));
        rvMyListVideo.setAdapter(adapter);
        rvMyListVideo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == CollectionUtils.size(adapter.getData()) - 1) {
                        //bottom of list!
                        getData();
                    }
                }
            }
        });
    }

    public void onGetDataSuccess(ArrayList<VideoInfoResponse> response) {
        if (CollectionUtils.isNotEmpty(response)) {
            adapter.addAll(response);
        }

        checkEmptyView();

        setViewTotalVideos();
    }

    private void checkEmptyView() {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (multipleStatusView != null) multipleStatusView.showEmpty();

            if (rvMyListVideo != null) rvMyListVideo.setVisibility(View.INVISIBLE);

            if (tvVideoCount != null) tvVideoCount.setVisibility(View.GONE);

            if (lnSort != null) lnSort.setVisibility(View.GONE);

        } else {
            if (multipleStatusView != null) multipleStatusView.showContent();

            if (rvMyListVideo != null) rvMyListVideo.setVisibility(View.VISIBLE);

            if (tvVideoCount != null) tvVideoCount.setVisibility(View.VISIBLE);

            if (lnSort != null) lnSort.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Hien thi so tong so luongw video
     * Created_by @dchieu on 10/9/20
     */
    private void setViewTotalVideos() {
        int size = CollectionUtils.size(adapter.getData());

        if (tvVideoCount != null)
            tvVideoCount.setText(getString((size > 1) ? R.string.total_videos_es : R.string.total_video_es, size));
    }

    public void onGetDataFailure(String message) {
        if (CollectionUtils.isEmpty(adapter.getData())) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }

            if (rvMyListVideo != null) rvMyListVideo.setVisibility(View.INVISIBLE);
        }
    }

    @Subscribe
    public void onEvent(OnLikeVideoEvent event) {
        if (event == null || event.video == null) return;

        if (event.video.isLike()) {
            VideoInfoResponse removeVideo = CollectionUtils.find(adapter.getData(), item ->
                    item != null && item.id.equals(event.video.id)
            );

            if (removeVideo != null) {
                adapter.delete(removeVideo);

                checkEmptyView();
            }
        } else {
            new Handler().postDelayed(this::refreshData, 1000);
        }
    }
}

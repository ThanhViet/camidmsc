package com.metfone.esport.ui.games.tournamentdetails.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.ParticipantResponse;
import com.metfone.esport.ui.viewholders.TournamentParticipantsViewHolder;

import java.util.ArrayList;

public class TournamentParticipantsAdapter extends BaseListAdapter<ParticipantResponse, TournamentParticipantsViewHolder> {

    private Context context;

    public TournamentParticipantsAdapter(Context context, ArrayList<ParticipantResponse> items) {
        super(context);

        this.context = context;

        setNewData(items);
    }

    @NonNull
    @Override
    public TournamentParticipantsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new TournamentParticipantsViewHolder(LayoutInflater.from(context), parent, 0);
    }

    @Override
    public void onBindViewHolder(@NonNull TournamentParticipantsViewHolder holder, int position) {
        holder.binData(mData.get(position));
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

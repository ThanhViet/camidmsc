package com.metfone.esport.ui.live;

import android.content.Context;

import com.metfone.selfcare.R;

/**
 * Created by Nguyễn Thành Chung on 9/2/20.
 */
public enum EDisplayQuality {
    DISPLAY_QUALITY_1080("1080"),
    DISPLAY_QUALITY_720("720"),
    DISPLAY_QUALITY_480("480"),
    DISPLAY_QUALITY_360("360");

    public String status;

    EDisplayQuality(String status) {
        this.status = status;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTextDisplay(Context context,int status){
        switch (status){
            case 0:
                return context.getString(R.string.display_1080);
            case 1:
                return context.getString(R.string.display_720);
            case 2:
                return context.getString(R.string.display_480);
            case 3:
                return context.getString(R.string.display_360);
            default:
                return context.getString(R.string.display_720);
        }
    }
}

package com.metfone.esport.ui.mychannel;

import com.blankj.utilcode.util.CollectionUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.ChannelHomeResponse;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.RxUtils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

class MyChannelPresenter extends BasePresenter<MyChannelFragment, BaseModel> {

    public MyChannelPresenter(MyChannelFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getData() {
        RxUtils.async(compositeDisposable, apiService.getMyChannels(AccountBusiness.getInstance().getPhoneNumberOrUserId(), getDefaultParam())//TODO phone number
                , new ICallBackResponse<ArrayList<ChannelInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<ChannelInfoResponse> response) {
                        if (view != null && CollectionUtils.isNotEmpty(response))
                            view.onGetDataSuccess(response.get(0));
                        else if (view != null) view.onGetDataFailure("");
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }
                });
    }

    public void loadChannelInfo(long channelId) {
        RxUtils.async(compositeDisposable, apiService.getChannelHome(channelId, 0, 0, getDefaultParam())
                , new ICallBackResponse<ArrayList<ChannelHomeResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<ChannelHomeResponse> response) {
                        if (view != null && CollectionUtils.isNotEmpty(response)) {
                            ChannelInfoResponse channelInfo = null;
                            ArrayList<VideoInfoResponse> pastBroadcast = null;
                            ArrayList<VideoInfoResponse> uploaded = null;
                            ArrayList<GameInfoResponse> listGame = null;
                            for (ChannelHomeResponse item : response) {
                                if (item != null && item.type != null) {
                                    switch (item.type) {
                                        case ChannelHomeResponse.TYPE_CHANNEL_INFO: {
                                            channelInfo = item.channelInfo;
                                        }
                                        break;
                                        case ChannelHomeResponse.TYPE_PAST_BROADCAST: {
                                            pastBroadcast = item.pastBroadcast;
                                        }
                                        break;
                                        case ChannelHomeResponse.TYPE_UPLOADED: {
                                            uploaded = item.uploaded;
                                        }
                                        break;
                                        case ChannelHomeResponse.TYPE_LIST_GAME_OF_CHANNEL: {
                                            listGame = item.listGame;
                                        }
                                        break;
                                    }
                                }
                            }
                            view.onGetDataSuccess(channelInfo, pastBroadcast, uploaded);
                        } else if (view != null) view.onGetDataFailure("");
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.onGetDataFailure(error);
                    }
                });
    }
}

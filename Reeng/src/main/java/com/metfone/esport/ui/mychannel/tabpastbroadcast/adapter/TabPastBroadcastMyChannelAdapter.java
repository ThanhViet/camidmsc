/*
 * Copyright (c) admin created on 2020-09-15 10:53:39 PM
 */

package com.metfone.esport.ui.mychannel.tabpastbroadcast.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HalfVideoWithOptionViewHolder;

import java.util.ArrayList;

public class TabPastBroadcastMyChannelAdapter extends BaseListAdapter<VideoInfoResponse, HalfVideoWithOptionViewHolder> {

    private Context context;
    private HalfVideoWithOptionViewHolder.IOnClickItem onClickItem;

    public void setOnClickItem(HalfVideoWithOptionViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public TabPastBroadcastMyChannelAdapter(Context context, ArrayList<VideoInfoResponse> items) {
        super(context);

        this.context = context;

        setNewData(items);
    }

    @NonNull
    @Override
    public HalfVideoWithOptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HalfVideoWithOptionViewHolder(LayoutInflater.from(context), parent, 0);
    }

    @Override
    public void onBindViewHolder(@NonNull HalfVideoWithOptionViewHolder holder, int position) {
        holder.binData(mData.get(position), onClickItem);
        if (holder.avAvatar != null) holder.avAvatar.setVisibility(View.INVISIBLE);
        if (holder.tvAccountName != null) holder.tvAccountName.setVisibility(View.INVISIBLE);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

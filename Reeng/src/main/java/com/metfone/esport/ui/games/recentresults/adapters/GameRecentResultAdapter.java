package com.metfone.esport.ui.games.recentresults.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.GameMatchResponse;
import com.metfone.esport.ui.viewholders.GameTournamentResultViewHolder;

import java.util.ArrayList;

public class GameRecentResultAdapter extends BaseListAdapter<GameMatchResponse, GameTournamentResultViewHolder> {

    private Context context;

    public GameRecentResultAdapter(Context context, ArrayList<GameMatchResponse> items) {
        super(context);

        this.context = context;

        setNewData(items);
    }

    @NonNull
    @Override
    public GameTournamentResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new GameTournamentResultViewHolder(LayoutInflater.from(context), parent, 0);
    }

    @Override
    public void onBindViewHolder(@NonNull GameTournamentResultViewHolder holder, int position) {
        holder.binData(mData.get(position), null);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

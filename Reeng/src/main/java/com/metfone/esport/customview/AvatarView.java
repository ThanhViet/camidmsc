package com.metfone.esport.customview;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.selfcare.R;

public class AvatarView extends AppCompatImageView {
    public AvatarView(@NonNull Context context) {
        super(context);
    }

    public AvatarView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    public AvatarView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public AvatarView setAvatarUrl(String url) {
        try {
            ImageLoader.setImage(
                    this,
                    url,
                    R.drawable.bg_avatar_default_es,
                    R.drawable.bg_avatar_default_es,
                    new CircleCrop());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public AvatarView setAvatarUri(Uri uri) {
        try {
            ImageLoader.setImageUri(
                    this,
                    uri,
                    R.drawable.bg_avatar_default_es,
                    R.drawable.bg_avatar_default_es,
                    new CircleCrop());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }
}

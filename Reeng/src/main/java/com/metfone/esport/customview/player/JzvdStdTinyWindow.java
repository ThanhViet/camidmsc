package com.metfone.esport.customview.player;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.esport.util.Log;

/**
 * Created by Nguyễn Thành Chung on 10/9/20.
 */
public class JzvdStdTinyWindow extends JzvdStd{

    public JzvdStdTinyWindow(Context context) {
        super(context);
    }

    public JzvdStdTinyWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setUp(JZDataSource jzDataSource, int screen, Class mediaInterface) {
        super.setUp(jzDataSource, screen, mediaInterface);
    }

    public void gotoTinyScreen() {
        Log.i(TAG, "startWindowTiny " + " [" + this.hashCode() + "] ");
        if (state == STATE_NORMAL || state == STATE_ERROR || state == STATE_AUTO_COMPLETE)
            return;
        ViewGroup vg = (ViewGroup) getParent();
        jzvdContext = vg.getContext();
        blockLayoutParams = getLayoutParams();
        blockIndex = vg.indexOfChild(this);
        blockWidth = getWidth();
        blockHeight = getHeight();

        vg.removeView(this);
        cloneAJzvd(vg);
        CONTAINER_LIST.add(vg);
        layoutLive.setVisibility(GONE);
        ViewGroup vgg = (ViewGroup) (JZUtils.scanForActivity(getContext())).getWindow().getDecorView();
        LayoutParams lp = new LayoutParams(ScreenUtils.getScreenWidth() / 2, 500);
        lp.bottomMargin = 30;
        lp.gravity = Gravity.RIGHT | Gravity.BOTTOM;
        vgg.addView(this, lp);
//        ivTinyFullScreen.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                removeView(vgg);
//            }
//        });
        setScreenTiny();
    }
}

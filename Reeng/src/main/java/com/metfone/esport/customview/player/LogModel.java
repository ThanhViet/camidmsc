package com.metfone.esport.customview.player;

public class LogModel {
    private String state="";
    private String timeLog = "";
    private String lagArray = "";
    private String playArray = "";
    private String bandwidthArray = "";
    private String networkArray  = "";
    private String errorDesc = "";
    private boolean log5 = false;
    private boolean log15 = false;
    private boolean log30 = false;
    private boolean isSeek = false;
    private long totalTimePlay = 0;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTimeLog() {
        return timeLog;
    }

    public void setTimeLog(String timeLog) {
        this.timeLog = timeLog;
    }

    public String getLagArray() {
        return lagArray;
    }

    public void setLagArray(String lagArray) {
        this.lagArray = lagArray;
    }

    public String getPlayArray() {
        return playArray;
    }

    public void setPlayArray(String playArray) {
        this.playArray = playArray;
    }

    public String getBandwidthArray() {
        return bandwidthArray;
    }

    public void setBandwidthArray(String bandwidthArray) {
        this.bandwidthArray = bandwidthArray;
    }

    public String getNetworkArray() {
        return networkArray;
    }

    public void setNetworkArray(String networkArray) {
        this.networkArray = networkArray;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
    public boolean isLog5() {
        return log5;
    }

    public void setLog5(boolean log5) {
        this.log5 = log5;
    }

    public boolean isLog15() {
        return log15;
    }

    public void setLog15(boolean log15) {
        this.log15 = log15;
    }

    public boolean isLog30() {
        return log30;
    }

    public void setLog30(boolean log30) {
        this.log30 = log30;
    }

    public boolean isSeek() {
        return isSeek;
    }

    public void setSeek(boolean seek) {
        isSeek = seek;
    }

    public long getTotalTimePlay() {
        return totalTimePlay;
    }

    public void setTotalTimePlay(long totalTimePlay) {
        this.totalTimePlay = totalTimePlay;
    }
}

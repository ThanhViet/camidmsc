package com.metfone.esport.customview.player;

import android.content.Context;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.metfone.selfcare.R;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class JzvdAutoPlay extends Jzvd {

    public JzvdAutoPlay(Context context) {
        super(context);
    }

    public JzvdAutoPlay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public int getLayoutId() {
        return R.layout.jz_layout_auto_player;
    }

    @Override
    public void init(Context context) {
        View.inflate(context, getLayoutId(), this);
        jzvdContext = context;
        textureViewContainer = findViewById(R.id.surface_container);
        mScreenWidth = getContext().getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getContext().getResources().getDisplayMetrics().heightPixels;
        state = STATE_IDLE;
    }

    @Override
    public void changeUrl(JZDataSource jzDataSource, long seekToInAdvance) {
        super.changeUrl(jzDataSource, seekToInAdvance);
    }

    @Override
    public void onStateNormal() {
        Log.i(TAG, "onStateNormal " + " [" + this.hashCode() + "] ");
        state = STATE_NORMAL;
        release();
    }

    @Override
    public void onStatePreparing() {
        Log.i(TAG, "onStatePreparing " + " [" + this.hashCode() + "] ");
        state = STATE_PREPARING;
    }

    public void onStatePreparingPlaying() {
        super.onStatePreparingPlaying();
    }

    public void onStatePreparingChangeUrl() {
        super.onStatePreparingChangeUrl();
    }

    @Override
    public void onStatePlaying() {
        state = STATE_PLAYING;
    }

    @Override
    public void onStatePause() {
        state = STATE_PAUSE;
    }

    @Override
    public void onStateError() {
        state = STATE_ERROR;
    }

    @Override
    public void onStateAutoComplete() {
        state = STATE_AUTO_COMPLETE;
    }

    @Override
    public void startVideo() {
        Log.d(TAG, "startVideo [" + this.hashCode() + "] ");
        setCurrentJzvd(this);
        try {
            Constructor<JZMediaInterface> constructor = mediaInterfaceClass.getConstructor(Jzvd.class);
            this.mediaInterface = constructor.newInstance(this);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        addTextureView();
        mAudioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(onAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
        onStatePreparing();
    }

    @Override
    public void gotoNormalScreen() {
        try {
            ViewGroup vg = (ViewGroup) (JZUtils.scanForActivity(jzvdContext)).getWindow().getDecorView();
            vg.removeView(this);
//        CONTAINER_LIST.getLast().removeAllViews();
            CONTAINER_LIST.getLast().removeViewAt(blockIndex);//remove block
            CONTAINER_LIST.getLast().addView(this, blockIndex, blockLayoutParams);
            CONTAINER_LIST.pop();
            setScreenNormal();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotoFullscreen() {
        try {
            gotoFullscreenTime = System.currentTimeMillis();
            ViewGroup vg = (ViewGroup) getParent();
            jzvdContext = vg.getContext();
            blockLayoutParams = getLayoutParams();
            blockIndex = vg.indexOfChild(this);
            blockWidth = getWidth();
            blockHeight = getHeight();
            isHorizontal = true;
            vg.removeView(this);
            cloneAJzvd(vg);
            CONTAINER_LIST.add(vg);
            vg = (ViewGroup) (JZUtils.scanForActivity(jzvdContext)).getWindow().getDecorView();

            ViewGroup.LayoutParams fullLayout = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            vg.addView(this, fullLayout);

            setScreenFullscreen();
            JZUtils.hideStatusBar(jzvdContext);
            JZUtils.setRequestedOrientation(jzvdContext, FULLSCREEN_ORIENTATION);
            JZUtils.hideSystemUI(jzvdContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setScreenNormal() {
        super.setScreenNormal();
    }

    @Override
    public void setScreenFullscreen() {
        super.setScreenFullscreen();
    }

    @Override
    public void setScreenTiny() {
        super.setScreenTiny();
    }

    @Override
    public void showWifiDialog() {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onProgress(int progress, long position, long duration) {

    }

    @Override
    public void setBufferProgress(int bufferProgress) {

    }

    @Override
    public void resetProgressAndTime() {

    }

    @Override
    public void showProgressDialog(float deltaX, String seekTime, long seekTimePosition, String totalTime, long totalTimeDuration) {

    }

    @Override
    public void dismissProgressDialog() {

    }

    @Override
    public void showVolumeDialog(float deltaY, int volumePercent) {

    }

    @Override
    public void dismissVolumeDialog() {

    }

    @Override
    public void showBrightnessDialog(int brightnessPercent) {

    }

    @Override
    public void dismissBrightnessDialog() {

    }

    @Override
    public void onCompletion() {
        Runtime.getRuntime().gc();
        Log.i(TAG, "onAutoCompletion " + " [" + this.hashCode() + "] ");
        dismissVolumeDialog();
        onStateAutoComplete();
        release();
        JZUtils.scanForActivity(getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (autoPlayListener != null) autoPlayListener.onComplete();
    }

    @Override
    public void reset() {
        Log.i(TAG, "reset " + " [" + this.hashCode() + "] ");
        if (state == STATE_PLAYING || state == STATE_PAUSE) {
            long position = getCurrentPositionWhenPlaying();
            JZUtils.saveProgress(getContext(), jzDataSource, position);
        }
        AudioManager mAudioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.abandonAudioFocus(onAudioFocusChangeListener);
        JZUtils.scanForActivity(getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        onStateNormal();
        textureViewContainer.removeAllViews();
        if (autoPlayListener != null) autoPlayListener.onComplete();
    }

}

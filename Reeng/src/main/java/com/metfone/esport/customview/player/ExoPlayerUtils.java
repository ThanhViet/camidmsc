/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/10/5
 *
 */

package com.metfone.esport.customview.player;

import android.content.res.Resources;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.metfone.selfcare.R;

import java.util.ArrayList;


public class ExoPlayerUtils {
    /**
     * Returns whether a track selection dialog will have content to display if initialized with the
     * specified {@link DefaultTrackSelector} in its current state.
     */
    public static boolean willHaveContent(DefaultTrackSelector trackSelector) {
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        return mappedTrackInfo != null && willHaveContent(mappedTrackInfo);
    }

    /**
     * Returns whether a track selection dialog will have content to display if initialized with the
     * specified {@link MappingTrackSelector.MappedTrackInfo}.
     */
    public static boolean willHaveContent(MappingTrackSelector.MappedTrackInfo mappedTrackInfo) {
        for (int i = 0; i < mappedTrackInfo.getRendererCount(); i++) {
            if (showTabForRenderer(mappedTrackInfo, i)) {
                return true;
            }
        }
        return false;
    }

    public static boolean showTabForRenderer(MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int rendererIndex) {
        TrackGroupArray trackGroupArray = mappedTrackInfo.getTrackGroups(rendererIndex);
        if (trackGroupArray.length == 0) {
            return false;
        }
        int trackType = mappedTrackInfo.getRendererType(rendererIndex);
        return isSupportedTrackType(trackType);
    }

    public static boolean isSupportedTrackType(int trackType) {
        /*|| trackType == C.TRACK_TYPE_AUDIO || trackType == C.TRACK_TYPE_TEXT*/
        return trackType == C.TRACK_TYPE_VIDEO;
    }

    public static ArrayList<Format> getResolution(DefaultTrackSelector trackSelector) {
        ArrayList<Format> list = new ArrayList<>();
        if (trackSelector != null) {
            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
            for (int i = 0; i < mappedTrackInfo.getRendererCount(); i++) {
                if (showTabForRenderer(mappedTrackInfo, i)) {
                    TrackGroupArray trackGroups = mappedTrackInfo.getTrackGroups(i);
                    for (int groupIndex = 0; groupIndex < trackGroups.length; groupIndex++) {
                        TrackGroup group = trackGroups.get(groupIndex);
                        for (int trackIndex = 0; trackIndex < group.length; trackIndex++) {
                            Format format = group.getFormat(trackIndex);
                            list.add(format);
                        }
                    }
                }
            }
        }
        return list;
    }

    public static void setupResolution(DefaultTrackSelector trackSelector, Format format, boolean isAuto) {
        if (trackSelector != null) {
            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
            DefaultTrackSelector.Parameters parameters = trackSelector.getParameters();
            DefaultTrackSelector.ParametersBuilder builder = parameters.buildUpon();
            for (int i = 0; i < mappedTrackInfo.getRendererCount(); i++) {
                builder.clearSelectionOverrides(/* rendererIndex= */ i).setRendererDisabled(/* rendererIndex= */ i, isAuto);
//                List<DefaultTrackSelector.SelectionOverride> overrides =
//                        trackSelectionDialog.getOverrides(/* rendererIndex= */ i);
//                if (!overrides.isEmpty()) {
//                    builder.setSelectionOverride(
//                            /* rendererIndex= */ i,
//                            mappedTrackInfo.getTrackGroups(/* rendererIndex= */ i),
//                            overrides.get(0));
//                }
            }
            trackSelector.setParameters(builder);
        }
    }

    public static String getTrackTypeString(Resources resources, int trackType) {
        switch (trackType) {
            case C.TRACK_TYPE_VIDEO:
                return resources.getString(R.string.exo_track_selection_title_video);
            case C.TRACK_TYPE_AUDIO:
                return resources.getString(R.string.exo_track_selection_title_audio);
            case C.TRACK_TYPE_TEXT:
                return resources.getString(R.string.exo_track_selection_title_text);
            default:
                throw new IllegalArgumentException();
        }
    }

}

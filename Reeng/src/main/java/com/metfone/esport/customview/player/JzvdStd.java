package com.metfone.esport.customview.player;

import static com.blankj.utilcode.util.ThreadUtils.runOnUiThread;
import static com.metfone.esport.ui.live.LiveVideoFragment.PRE_CODE_THANKMESSAGE;
import static com.metfone.esport.ui.live.LiveVideoFragment.TIME_REMAIN_SHOW;
import static com.metfone.esport.ui.live.LiveVideoFragment.TIME_SHOW_THANKMESSAGE;
import static com.metfone.esport.ui.live.LiveVideoFragment.isClickDonateHorizontal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.inputmethodservice.Keyboard;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.metfone.esport.common.Common;
import com.metfone.esport.entity.camid.CamIdUserResponse;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.ui.live.BottomFragment;
import com.metfone.esport.ui.live.LiveCommentAdapter;
import com.metfone.esport.ui.live.TopFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.ui.dialog.DialogMessage;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static java.lang.Math.abs;

/**
 * Created by Nathen
 * On 2016/04/18 16:15
 */
public class JzvdStd extends Jzvd {

    public static long LAST_GET_BATTERYLEVEL_TIME = 0;
    public static int LAST_GET_BATTERYLEVEL_PERCENT = 70;
    public static int STYLE_LIVE = 1;
    public static int STYLE_NORMAL = 0;
    public static int STYLE_UPLOAD = 2;
    protected static Timer DISMISS_CONTROL_VIEW_TIMER;
    public boolean isShowComment = false;
    public int type = STYLE_NORMAL;
    public ImageView backButton;
    public ProgressBar bottomProgressBar, loadingProgressBar;
    public TextView titleTextView;
    public ImageView posterImageView;
    public ImageView tinyBackImageView;
    public LinearLayout batteryTimeLayout;
    public ImageView batteryLevel;
    public ImageView ivComment;
    public TextView videoCurrentTime;
    public TextView replayTextView;
    public TextView clarity;
    public PopupWindow clarityPopWindow;
    public TextView mRetryBtn;
    public LinearLayout mRetryLayout;
    public LinearLayout layoutBottom;
    public RelativeLayout layoutLive;
    public TextView tvView;
    public ImageView ivFullScreenLive;
    public RecyclerView rvCommentLandscape;
    public RelativeLayout rlComment;
    public RelativeLayout layout_top;
    public ImageView ivMore, ivShareVideo, ivForward, ivReplay;
    public LinearLayout lnUploadOffline, lnShare, lnLike, lnComment;
    public ImageView back;
    public RelativeLayout rlOverLayTiny;
    public ImageView ivTinyFullScreen;
    public ImageView ivSend;
    //    public TextView edComment;
    public RelativeLayout layoutComment;
    public ImageView ivSticker;
    public ImageView ivDonateHorizontal;
    public ImageView iv_donate;
    public RelativeLayout rootView;
    public RecyclerView rcvCommentHorizontal;
    public EmojiconEditText edCommentHorizontal;
    public View startLayout;
    public Activity activity;
    public boolean isFullScreenLive = false;
    public boolean isLoading = true;
    public BroadcastReceiver battertReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
                int level = intent.getIntExtra("level", 0);
                int scale = intent.getIntExtra("scale", 100);
                int percent = level * 100 / scale;
                LAST_GET_BATTERYLEVEL_PERCENT = percent;
                setBatteryLevel();
                try {
                    jzvdContext.unregisterReceiver(battertReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
    protected DismissControlViewTimerTask mDismissControlViewTimerTask;
    protected Dialog mProgressDialog;
    protected ProgressBar mDialogProgressBar;
    protected TextView mDialogSeekTime;
    protected TextView mDialogTotalTime;
    protected ImageView mDialogIcon;
    protected Dialog mVolumeDialog;
    protected LinearLayout lnRoot;
    protected ProgressBar mDialogVolumeProgressBar;
    protected TextView mDialogVolumeTextView;
    protected ImageView mDialogVolumeImageView;
    protected Dialog mBrightnessDialog;
    protected ProgressBar mDialogBrightnessProgressBar;
    protected TextView mDialogBrightnessTextView;
    protected RelativeLayout rlRootComment;
    protected ImageView ivSendHorizontal;
    protected ImageView ivStickerHorizontal;
    protected boolean mIsWifi;
    protected long lastClickTime = 0;
    protected long doubleTime = 200;
    protected ArrayDeque<Runnable> delayTask = new ArrayDeque<>();
    private VideoInfoResponse modelData;
    private LiveCommentAdapter liveCommentAdapter;
    private LinearLayout ln_donate_overlay;
    private ImageView imgGiftDonate;
    private TextView tvDonateComment;
    private TextView tvThankMess;
    private boolean isRegisterWifi = false;
    public BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                boolean isWifi = JZUtils.isWifiConnected(context);
                if (mIsWifi == isWifi) return;
                mIsWifi = isWifi;
                if (!mIsWifi && !WIFI_TIP_DIALOG_SHOWED && state == STATE_PLAYING) {
                    startButton.performClick();
                    showWifiDialog();
                }
            }
        }
    };
    private EmojIconActions emojIcon, emojIconActions;
    private boolean isClickComment = false;

    public JzvdStd(Context context) {
        super(context);
    }

    public JzvdStd(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setData(VideoInfoResponse modelData) {
        this.modelData = modelData;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;

        stopHandlerMess = false;
        messQueue = new ArrayBlockingQueue<String>(100);
        new Thread(new ShowDonateMessage()).start();


        KeyboardUtils.registerSoftInputChangedListener(activity, new KeyboardUtils.OnSoftInputChangedListener() {
            @Override
            public void onSoftInputChanged(int height) {
                if (height > 0 && isClickComment) {
                    hideLayoutWhenComment();

                } else {
                    if (isClickComment) {
                        showLayoutWhenComment(false);
                        isClickComment = false;
                    }
                }

            }
        });
    }

    @Override
    public void gotoFullscreen() {
        super.gotoFullscreen();
        isHorizontal = true;
        KeyboardUtils.hideSoftInput(CURRENT_JZVD);
    }

    private void setKeyDown(){
//        try{
//            CURRENT_JZVD.setFocusableInTouchMode(true);
//            CURRENT_JZVD.requestFocus();
//            CURRENT_JZVD.setOnKeyListener((v, keyCode, event) -> {
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                    gotoNormalScreen();
//                    return true;
//                }
//                return false;
//            });
//        }catch (Exception e){
//            Common.handleException(e);
//        }
    }

    public void setType(int type) {
        this.type = type;
        if (type == STYLE_LIVE) {
            layoutLive.setVisibility(VISIBLE);
            ViewGroup.LayoutParams params = layoutBottom.getLayoutParams();
            params.height = 0;
            layoutBottom.setLayoutParams(params);
            isFullScreenLive = true;
            ivForward.setVisibility(GONE);
            ivReplay.setVisibility(GONE);

        } else if (type == STYLE_NORMAL || type == STYLE_UPLOAD) {
            layoutLive.setVisibility(GONE);
            ViewGroup.LayoutParams params = layoutLive.getLayoutParams();
            params.height = 0;
            layoutLive.setLayoutParams(params);
            isFullScreenLive = false;

//            ivForward.setVisibility(VISIBLE);
//            ivReplay.setVisibility(VISIBLE);
        }

        setScreenNormal();
    }


    public void setDataComment(List<ListCommentEntity> liveCommentEntities) {
        try {
            handlerDonateOverlay(liveCommentEntities);
            if (!modelData.isLive()) {
                Collections.sort(liveCommentEntities, (o1, o2) -> o2.currentTime.compareTo(o1.currentTime));
            }

            liveCommentAdapter.setData(liveCommentEntities);
            if (CollectionUtils.isNotEmpty(liveCommentAdapter.getData())) {
                rvCommentLandscape.scrollToPosition(liveCommentAdapter.getData().size() - 1);
                rcvCommentHorizontal.scrollToPosition(liveCommentAdapter.getData().size() - 1);
            }
        } catch (Exception e) {
            Common.handleException(e);
        }

    }


    private BlockingQueue<String> messQueue = new ArrayBlockingQueue<String>(100);
    final long timeDelay = TIME_REMAIN_SHOW;
    final int timeStep = TIME_SHOW_THANKMESSAGE;
    final String preCode = PRE_CODE_THANKMESSAGE;
    public boolean stopHandlerMess = false;
    private void handlerDonateOverlay(List<ListCommentEntity> liveCommentEntities) {

        long currentTime = System.currentTimeMillis();
        for (ListCommentEntity i: liveCommentEntities) {
            if (i.getTimestamp() > currentTime - timeDelay) {
                String pre = "";
                if (i.getContent().length()>preCode.length()-1) {
                    pre = i.getContent().substring(0,preCode.length());
                    if (pre.equals(preCode)) {
                        messQueue.offer(i.getContent().trim());
                    }
                }

            }
        }
    }

    private void handlerDonateOverlay(ListCommentEntity liveCommentEntitie) {
        String pre = "";
        if (liveCommentEntitie.getContent().length()>preCode.length()-1)
        {
            pre = liveCommentEntitie.getContent().substring(0,preCode.length());
            if (pre.equals(preCode)) {
                messQueue.offer(liveCommentEntitie.getContent().trim());
            }
        }


    }

    class ShowDonateMessage implements Runnable {
        @Override
        public void run() {
            while (!stopHandlerMess) {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showDonate();
                        }
                    });
                    Thread.sleep(timeStep+1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void showDonate() {
        String cmt = messQueue.poll();

        if (cmt == null) return;

        ln_donate_overlay.setVisibility(VISIBLE);
        ln_donate_overlay.animate().translationY(90);
        ln_donate_overlay.animate().setDuration(400);

        if (cmt.split("_").length > 1)
            tvThankMess.setText(cmt.split("_")[1]);
        if (cmt.split("_").length == 2) {
            tvDonateComment.setText("");
        } else if (cmt.split("_").length > 2)
            tvDonateComment.setText(cmt.split("_")[2]);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ln_donate_overlay.animate().setDuration(400);
                ln_donate_overlay.animate().translationY(-25);
            }
        }, timeStep);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ln_donate_overlay.setVisibility(GONE);
            }
        }, timeStep+400);
    }

    public void setMessage(ListCommentEntity entity) {
        try {
            handlerDonateOverlay(entity);
            liveCommentAdapter.setData(entity);
            if (CollectionUtils.isNotEmpty(liveCommentAdapter.getData())) {
                rvCommentLandscape.scrollToPosition(liveCommentAdapter.getData().size() - 1);
                rcvCommentHorizontal.scrollToPosition(liveCommentAdapter.getData().size() - 1);
            }
        } catch (Exception e) {
            Common.handleException(e);
        }

    }

    public void setViewLive(String view) {
        tvView.setText(view);
    }


    @Override
    public void init(Context context) {
        super.init(context);
        batteryTimeLayout = findViewById(R.id.battery_time_layout);
        bottomProgressBar = findViewById(R.id.bottom_progress);
        titleTextView = findViewById(R.id.title);
        backButton = findViewById(R.id.back);
        posterImageView = findViewById(R.id.poster);
        loadingProgressBar = findViewById(R.id.loading);
        tinyBackImageView = findViewById(R.id.back_tiny);
        batteryLevel = findViewById(R.id.battery_level);
        videoCurrentTime = findViewById(R.id.video_current_time);
        replayTextView = findViewById(R.id.replay_text);
        clarity = findViewById(R.id.clarity);
        mRetryBtn = findViewById(R.id.retry_btn);
        mRetryLayout = findViewById(R.id.retry_layout);
        layoutBottom = findViewById(R.id.layout_bottom);
        layoutLive = findViewById(R.id.rlBottomLive);
        tvView = findViewById(R.id.tvView);
        ivComment = findViewById(R.id.ivComment);
        ivFullScreenLive = findViewById(R.id.fullscreenLive);
        rvCommentLandscape = findViewById(R.id.rvCommentLandscape);
        rlComment = findViewById(R.id.rlComment);
        layout_top = findViewById(R.id.layout_top);
        back = findViewById(R.id.back);
        ivMore = findViewById(R.id.ivMore);
        ivShareVideo = findViewById(R.id.ivShareVideo);
        ivForward = findViewById(R.id.ivForward);
        ivReplay = findViewById(R.id.ivReplay);
        lnUploadOffline = findViewById(R.id.lnUploadOffline);
        lnComment = findViewById(R.id.lnComment);
        lnLike = findViewById(R.id.lnLike);
        lnShare = findViewById(R.id.lnShare);
        ivTinyFullScreen = findViewById(R.id.ivTinyFullScreen);
        rlOverLayTiny = findViewById(R.id.rlOverLayTiny);
        ivSend = findViewById(R.id.ivSend);
//        edComment = findViewById(R.id.edComment);
        layoutComment = findViewById(R.id.layoutComment);
        ivSticker = findViewById(R.id.ivSticker);
        rootView = findViewById(R.id.rootView);
        startLayout = findViewById(R.id.start_layout);
        lnRoot = findViewById(R.id.lnRoot);
        rlRootComment = findViewById(R.id.rlRootComment);
        rcvCommentHorizontal = findViewById(R.id.rcvCommentHorizontal);
        ivSendHorizontal = findViewById(R.id.ivSendHorizontal);
        ivDonateHorizontal = findViewById(R.id.ivDonateHorizontal);
        iv_donate = findViewById(R.id.iv_donate);
        edCommentHorizontal = findViewById(R.id.edCommentHorizontal);
        ivStickerHorizontal = findViewById(R.id.ivStickerHorizontal);
        ln_donate_overlay = findViewById(R.id.ln_donate_overlay);
        imgGiftDonate = findViewById(R.id.imgGiftDonate);
        tvDonateComment = findViewById(R.id.tvDonateComment);
        tvThankMess = findViewById(R.id.tvDesDonate);
//        cvItemSend.setBackgroundResource(R.drawable.style_comment_live_landscape_es);
        if (rvCommentLandscape != null)
            rvCommentLandscape.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CURRENT_JZVD != null) KeyboardUtils.hideSoftInput(CURRENT_JZVD);
                }
            });
        if (batteryTimeLayout == null) {
            batteryTimeLayout = new LinearLayout(context);
        }
        if (bottomProgressBar == null) {
            bottomProgressBar = new ProgressBar(context);
        }
        if (titleTextView == null) {
            titleTextView = new TextView(context);
        }
        if (backButton == null) {
            backButton = new ImageView(context);
        }
        if (posterImageView == null) {
            posterImageView = new ImageView(context);
        }
        if (loadingProgressBar == null) {
            loadingProgressBar = new ProgressBar(context);
        }
        if (tinyBackImageView == null) {
            tinyBackImageView = new ImageView(context);
        }
        if (batteryLevel == null) {
            batteryLevel = new ImageView(context);
        }
        if (videoCurrentTime == null) {
            videoCurrentTime = new TextView(context);
        }
        if (replayTextView == null) {
            replayTextView = new TextView(context);
        }
        if (clarity == null) {
            clarity = new TextView(context);
        }
        if (mRetryBtn == null) {
            mRetryBtn = new TextView(context);
        }
        if (mRetryLayout == null) {
            mRetryLayout = new LinearLayout(context);
        }

        if (ivSticker == null) {
            ivSticker = new ImageView(context);
        }
        initRecyclerCommentLive();
        posterImageView.setOnClickListener(this);
        backButton.setOnClickListener(this);
        tinyBackImageView.setOnClickListener(this);
        clarity.setOnClickListener(this);
        mRetryBtn.setOnClickListener(this);
        ivFullScreenLive.setOnClickListener(this);
        ivComment.setOnClickListener(this);
        ivMore.setOnClickListener(this);
        ivReplay.setOnClickListener(this);
        ivForward.setOnClickListener(this);
        lnShare.setOnClickListener(this);
        ivShareVideo.setOnClickListener(this);
        lnLike.setOnClickListener(this);
        lnComment.setOnClickListener(this);
        ivTinyFullScreen.setOnClickListener(this);
        ivSend.setOnClickListener(this);
        ivSticker.setOnClickListener(this);
        ivSendHorizontal.setOnClickListener(this);
        ivDonateHorizontal.setOnClickListener(this);
        iv_donate.setOnClickListener(this);
        ivSend.setOnClickListener(this);
//        emojIcon = new EmojIconActions(context, rootView, edComment, ivSticker);
//        emojIcon.ShowEmojIcon();
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(context);
        UserInfo currentUser = userInfoBusiness.getUser();
        String name = !SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false) ? currentUser.getFull_name() : AccountBusiness.getInstance().getChannelInfo().name;
        edCommentHorizontal.setHint(context.getString(R.string.send_a_message, name));
        checkKeyboardHeight();
        layoutComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AccountBusiness.getInstance().isLogin()) {
                    isClickComment = true;
                    hideLayoutWhenComment();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            edCommentHorizontal.requestFocus();
                            KeyboardUtils.showSoftInput();
                        }
                    }, 500);
                } else {
                    if (controllerListener != null) controllerListener.onLogin();
                }


            }
        });
        rlRootComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftInput(CURRENT_JZVD);
            }
        });
        rcvCommentHorizontal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftInput(CURRENT_JZVD);
            }
        });
        emojIconActions = new EmojIconActions(context, rlRootComment, edCommentHorizontal, ivStickerHorizontal);
        emojIconActions.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

            }

            @Override
            public void onKeyboardClose() {
//                showLayoutWhenComment();
            }
        });
        emojIconActions.ShowEmojIcon();
        rlComment.setVisibility(GONE);
        edCommentHorizontal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (StringUtils.isNotEmpty(s)) {
                    ivSendHorizontal.setEnabled(true);
                    ivSendHorizontal.setColorFilter(getResources().getColor(R.color.red));
                } else {
                    ivSendHorizontal.setEnabled(false);
                    ivSendHorizontal.setColorFilter(Color.parseColor("#898A8A"));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        KeyboardUtils.hideKeyboardWhenTouch(ivSendHorizontal, activity);

        tinyBackImageView.setOnClickListener(view -> {
            if (controllerListener != null) controllerListener.onCloseScreenTiny();
        });
//        layoutComment.setVisibility(AccountBusiness.getInstance().isLogin() ? View.VISIBLE : View.GONE);


        rootView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideLayoutWhenComment();
            }
        });
//        rlRootComment.setOnTouchListener(new OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                showLayoutWhenComment();
//                return false;
//            }
//        });

    }

    private void hideLayoutWhenComment() {
        try {
            if (CURRENT_JZVD.screen == Jzvd.SCREEN_FULLSCREEN || isHorizontal) {
                rlComment.setVisibility(GONE);
                ivFullScreenLive.setVisibility(GONE);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rootView.getLayoutParams();
                params.width = 0;
                params.height = heightVideoComment;
                rootView.setLayoutParams(params);

                LinearLayout.LayoutParams paramsComment = (LinearLayout.LayoutParams) rlRootComment.getLayoutParams();
                paramsComment.height = heightVideoComment;
                rlRootComment.setLayoutParams(paramsComment);
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    public void showLayoutWhenComment(boolean isClickContainer) {
        try {
            if (CURRENT_JZVD.screen == Jzvd.SCREEN_FULLSCREEN || isHorizontal) {
                if (!isClickContainer) {
                    rlComment.setVisibility(VISIBLE);
                }
                isShowComment = false;

                ivFullScreenLive.setVisibility(VISIBLE);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rootView.getLayoutParams();
                params.width = LinearLayout.LayoutParams.MATCH_PARENT;
                params.height = LinearLayout.LayoutParams.MATCH_PARENT;
                rootView.setLayoutParams(params);

                LinearLayout.LayoutParams paramsComment = (LinearLayout.LayoutParams) rlRootComment.getLayoutParams();
                paramsComment.height = LinearLayout.LayoutParams.MATCH_PARENT;
                rlRootComment.setLayoutParams(paramsComment);
                setShowComment();
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    int heightVideoComment = 0;

    private void checkKeyboardHeight() {
        try {
            lnRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Rect r = new Rect();
                    lnRoot.getWindowVisibleDisplayFrame(r);
                    int screenHeight = lnRoot.getRootView().getHeight();
                    int keyboardHeight = screenHeight - (r.bottom);
                    heightVideoComment = lnRoot.getHeight() - keyboardHeight;

                }
            });

        } catch (Exception e) {
            Common.handleException(e);
        }


    }

    private void initRecyclerCommentLive() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvCommentLandscape.setLayoutManager(linearLayoutManager);
        rvCommentLandscape.setHasFixedSize(true);
        liveCommentAdapter = new LiveCommentAdapter(getContext(), LiveCommentAdapter.LANDSCAPE);
        rvCommentLandscape.setAdapter(liveCommentAdapter);
        rvCommentLandscape.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rvCommentLandscape.getLayoutManager();
                if (isLoading && type == STYLE_NORMAL) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == liveCommentAdapter.getData().size() - 3) {
                        if (controllerListener != null) controllerListener.onGetComment();
                        Log.e("PlayerScroll", "hello");
                    }
                }
            }
        });

        rcvCommentHorizontal.setLayoutManager(new LinearLayoutManager(jzvdContext));
        rcvCommentHorizontal.setHasFixedSize(true);
        rcvCommentHorizontal.setAdapter(liveCommentAdapter);

    }

    public void setUp(JZDataSource jzDataSource, int screen, Class mediaInterfaceClass) {
        if ((System.currentTimeMillis() - gobakFullscreenTime) < 200) {
            return;
        }

        if ((System.currentTimeMillis() - gotoFullscreenTime) < 200) {
            return;
        }

        super.setUp(jzDataSource, screen, mediaInterfaceClass);
        titleTextView.setText(jzDataSource.title);
        setScreen(screen);
    }

    @Override
    public void changeUrl(JZDataSource jzDataSource, long seekToInAdvance) {
        super.changeUrl(jzDataSource, seekToInAdvance);
        titleTextView.setText(jzDataSource.title);
    }

    public void changeStartButtonSize(int size) {
        ViewGroup.LayoutParams lp = startButton.getLayoutParams();
        lp.height = size;
        lp.width = size;
        lp = loadingProgressBar.getLayoutParams();
        lp.height = size;
        lp.width = size;
    }

    @Override
    public int getLayoutId() {
        return R.layout.jz_layout_std;
    }

    @Override
    public void onStateNormal() {
        super.onStateNormal();
        changeUiToNormal();
    }

    @Override
    public void onStatePreparing() {
        super.onStatePreparing();
        changeUiToPreparing();
    }

    public void onStatePreparingPlaying() {
        super.onStatePreparingPlaying();
        changeUIToPreparingPlaying();
    }

    public void onStatePreparingChangeUrl() {
        super.onStatePreparingChangeUrl();
        changeUIToPreparingChangeUrl();
    }

    @Override
    public void onStatePlaying() {
        super.onStatePlaying();
        changeUiToPlayingClear();
    }

    @Override
    public void onStatePause() {
        super.onStatePause();
        changeUiToPauseShow();
        cancelDismissControlViewTimer();
    }

    @Override
    public void onStateError() {
        super.onStateError();
        changeUiToError();
    }


    @Override
    public void onStateAutoComplete() {
        super.onStateAutoComplete();
        changeUiToComplete();
        cancelDismissControlViewTimer();
        bottomProgressBar.setProgress(100);
    }

    @Override
    public void startVideo() {
        super.startVideo();
        registerWifiListener(getApplicationContext());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();
        if (id == R.id.surface_container) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_MOVE:
                    if (isHorizontal) {
                        JZUtils.hideSystemUI(jzvdContext);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    float x = event.getX();
                    float y = event.getY();
                    float deltaX = x - mDownX;
                    float deltaY = y - mDownY;
                    float absDeltaX = abs(deltaX);
                    float absDeltaY = abs(deltaY);
                    if (screen == SCREEN_FULLSCREEN) {
                        if (!mChangePosition && !mChangeVolume && !mChangeBrightness) {
                            if (absDeltaX > THRESHOLD || absDeltaY > THRESHOLD) {
                                cancelProgressTimer();
//                                if (absDeltaX >= THRESHOLD) {
//                                    if (state != STATE_ERROR) {
//                                        mChangePosition = true;
//                                        mGestureDownPosition = getCurrentPositionWhenPlaying();
//                                    }
//                                } else {
                                if (controllerListener != null)
                                    controllerListener.showDialogInfoPlayer();
//                        if (mDownX < mScreenHeight * 0.5f) {
////                            mChangeBrightness = true;
////                            WindowManager.LayoutParams lp = JZUtils.getWindow(getContext()).getAttributes();
////                            if (lp.screenBrightness < 0) {
////                                try {
////                                    mGestureDownBrightness = Settings.System.getInt(getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
////                                    Log.i(TAG, "current system brightness: " + mGestureDownBrightness);
////                                } catch (Settings.SettingNotFoundException e) {
////                                    e.printStackTrace();
////                                }
////                            } else {
////                                mGestureDownBrightness = lp.screenBrightness * 255;
////                                Log.i(TAG, "current activity brightness: " + mGestureDownBrightness);
////                            }
//                        } else {
//                            mChangeVolume = true;
//                            mGestureDownVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//                        }
//                                }
                            }
                        }
                    }

                    startDismissControlViewTimer();
//                    if (mChangePosition) {
//                        long duration = getDuration();
//                        int progress = (int) (mSeekTimePosition * 100 / (duration == 0 ? 1 : duration));
//                        bottomProgressBar.setProgress(progress);
//                    }

                    Runnable task = () -> {
                        if (!mChangePosition && !mChangeVolume) {
                            onClickUiToggle();
                        }
                    };
                    v.postDelayed(task, doubleTime + 20);
                    delayTask.add(task);
                    while (delayTask.size() > 2) {
                        delayTask.pollFirst();
                    }

                    long currentTimeMillis = System.currentTimeMillis();
                    if (currentTimeMillis - lastClickTime < doubleTime) {
                        for (Runnable taskItem : delayTask) {
                            v.removeCallbacks(taskItem);
                        }
                        if (state == STATE_PLAYING || state == STATE_PAUSE) {
                            Log.d(TAG, "doublClick [" + this.hashCode() + "] ");
                            startButton.performClick();
                        }
                    }
                    lastClickTime = currentTimeMillis;
                    break;
            }
        } else if (id == R.id.bottom_seek_progress) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    cancelDismissControlViewTimer();
                    break;
                case MotionEvent.ACTION_UP:
                    startDismissControlViewTimer();
                    break;
            }
        }
        return super.onTouch(v, event);
    }

    private void setShowComment() {
        rlComment.setVisibility(isShowComment ? View.VISIBLE : View.GONE);
        ivComment.setImageResource(isShowComment ? R.drawable.ic_comment_es : R.drawable.ic_off_comment_es);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int i = v.getId();
        if (i == R.id.poster) {
            clickPoster();
        } else if (i == R.id.surface_container) {
            if (CURRENT_JZVD != null) {
                KeyboardUtils.hideSoftInput(CURRENT_JZVD);
                if (CURRENT_JZVD.screen == Jzvd.SCREEN_FULLSCREEN) {
                    JZUtils.hideSystemUI(jzvdContext);
                }
            }
            showLayoutWhenComment(true);
            clickSurfaceContainer();
            if (clarityPopWindow != null) {
                clarityPopWindow.dismiss();
            }
        } else if (i == R.id.back) {
            clickBack();
//            JZUtils.showSystemUI(jzvdContext);
            isHorizontal = false;
            ivComment.setVisibility(GONE);
            rlComment.setVisibility(GONE);
        } else if (i == R.id.back_tiny) {
            clickBackTiny();
        } else if (i == R.id.clarity) {
            clickClarity();
        } else if (i == R.id.retry_btn) {

            clickRetryBtn();
        } else if (i == R.id.ivComment) {
            if (CURRENT_JZVD != null && CURRENT_JZVD.screen == Jzvd.SCREEN_FULLSCREEN) {
                JZUtils.hideSystemUI(jzvdContext);
            }
            isShowComment = !isShowComment;
            if (!modelData.isLive()) {
                gotoNormalScreen();
                if (controllerListener != null) controllerListener.onClickCommentUpload();
            } else {
                setShowComment();
            }

        } else if (i == R.id.ivMore) {
            if (controllerListener != null) controllerListener.onClickOption();
//            if (CURRENT_JZVD != null && !CURRENT_JZVD.isForcePortrait()) {
            if (!isForcePortrait()) {
                JZUtils.hideSystemUI(jzvdContext);
            }
//            }
        } else if (i == R.id.ivForward) {
            mediaInterface.seekTo(mCurrentPosition + 15000);
        } else if (i == R.id.ivReplay) {
            if (mCurrentPosition >= 15000) {
                mediaInterface.seekTo(mCurrentPosition - 15000);
            } else {
                mediaInterface.seekTo(0);
            }
        } else if (i == R.id.lnComment) {
            clickBack();
        } else if (i == R.id.lnShare || i == R.id.ivShareVideo) {
            if (CURRENT_JZVD != null && CURRENT_JZVD.screen == Jzvd.SCREEN_FULLSCREEN) {
                JZUtils.hideSystemUI(jzvdContext);
                gotoNormalScreen();
            }
            if (controllerListener != null) controllerListener.onClickShare();
        } else if (i == R.id.lnLike) {

        } else if (i == R.id.ivTinyFullScreen) {

            if (controllerListener != null) {
                controllerListener.onExpandScreen();
            }
        } else if (i == R.id.ivSendHorizontal) {
            if (CURRENT_JZVD != null)
//                KeyboardUtils.hideSoftInput(CURRENT_JZVD);
                if (controllerListener != null) {
                    controllerListener.onSendComment(edCommentHorizontal);
                }

        } else if (i == R.id.ivStickerHorizontal) {
//            if (CURRENT_JZVD != null && CURRENT_JZVD.screen == Jzvd.SCREEN_FULLSCREEN) {
//                JZUtils.hideSystemUI(jzvdContext);
//            }
//            emojIcon.setUseSystemEmoji(true);
        } else if (i == R.id.ivDonateHorizontal || i == R.id.iv_donate) {
            if (controllerListener != null) {
                isClickDonateHorizontal = true;
                controllerListener.onClickExitFullScreen();
                gotoNormalScreen();
            }
        }
        setKeyDown();
    }

    public long getPosition() {
        return mSeekTimePosition;
    }

    public void setPosition(long position) {
        mediaInterface.seekTo(position);
    }

    protected void clickRetryBtn() {
        if (jzDataSource == null) return;
        String currentUrl = getCurrentUrl();
        if (TextUtils.isEmpty(currentUrl)) {
            Toast.makeText(jzvdContext, getResources().getString(R.string.no_url), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!currentUrl.startsWith("file") && !currentUrl.startsWith("/") && !JZUtils.isWifiConnected(jzvdContext) && !WIFI_TIP_DIALOG_SHOWED) {
            showWifiDialog();
            return;
        }
        seekToInAdvance = mCurrentPosition;
        startVideo();
    }

    protected void clickClarity() {
        if (jzDataSource == null) return;
        onCLickUiToggleToClear();

        LayoutInflater inflater = (LayoutInflater) jzvdContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.jz_layout_clarity, null);

        OnClickListener mQualityListener = v1 -> {
            int index = (int) v1.getTag();

//                this.seekToInAdvance = getCurrentPositionWhenPlaying();
            jzDataSource.currentUrlIndex = index;
//                onStatePreparingChangeUrl();

            changeUrl(jzDataSource, getCurrentPositionWhenPlaying());

            clarity.setText(jzDataSource.getCurrentKey().toString());
            for (int j = 0; j < layout.getChildCount(); j++) {
                if (j == jzDataSource.currentUrlIndex) {
                    ((TextView) layout.getChildAt(j)).setTextColor(Color.parseColor("#fff85959"));
                } else {
                    ((TextView) layout.getChildAt(j)).setTextColor(Color.parseColor("#ffffff"));
                }
            }
            if (clarityPopWindow != null) {
                clarityPopWindow.dismiss();
            }
        };

        for (int j = 0; j < jzDataSource.urlsMap.size(); j++) {
            String key = jzDataSource.getKeyFromDataSource(j);
            TextView clarityItem = (TextView) View.inflate(jzvdContext, R.layout.jz_layout_clarity_item, null);
            clarityItem.setText(key);
            clarityItem.setTag(j);
            layout.addView(clarityItem, j);
            clarityItem.setOnClickListener(mQualityListener);
            if (j == jzDataSource.currentUrlIndex) {
                clarityItem.setTextColor(Color.parseColor("#fff85959"));
            }
        }

        clarityPopWindow = new PopupWindow(layout, JZUtils.dip2px(jzvdContext, 240), LayoutParams.MATCH_PARENT, true);
        clarityPopWindow.setContentView(layout);
        clarityPopWindow.setAnimationStyle(R.style.pop_animation);
        clarityPopWindow.showAtLocation(textureViewContainer, Gravity.END, 0, 0);
//            int offsetX = clarity.getMeasuredWidth() / 3;
//            int offsetY = clarity.getMeasuredHeight() / 3;
//            clarityPopWindow.update(clarity, -offsetX, -offsetY, Math.round(layout.getMeasuredWidth() * 2), layout.getMeasuredHeight());
    }

    protected void clickBackTiny() {
        clearFloatScreen();
    }

    protected void clickBack() {
        if (CONTAINER_LIST.size() != 0 && CURRENT_JZVD != null && !CURRENT_JZVD.isForcePortrait()) {
            CURRENT_JZVD.gotoNormalScreen();
            Log.e("clickBack", "gotoNormalScreen");
        }
        if (controllerListener != null) controllerListener.onClickBack();
    }

    protected void clickSurfaceContainer() {
        startDismissControlViewTimer();
    }

    protected void clickPoster() {
        if (jzDataSource == null) return;
        String currentUrl = getCurrentUrl();
        if (TextUtils.isEmpty(currentUrl)) {
            Toast.makeText(jzvdContext, getResources().getString(R.string.no_url), Toast.LENGTH_SHORT).show();
            return;
        }
        if (state == STATE_NORMAL) {
            if (!currentUrl.startsWith("file") && !currentUrl.startsWith("/") && !JZUtils.isWifiConnected(jzvdContext) && !WIFI_TIP_DIALOG_SHOWED) {
                showWifiDialog();
                return;
            }
            startVideo();
        } else if (state == STATE_AUTO_COMPLETE) {
            onClickUiToggle();
        }
    }

    @Override
    public void gotoNormalScreen() {
        super.gotoNormalScreen();
        isShowComment = false;
        setShowComment();
        ivComment.setVisibility(GONE);
        showLayoutWhenComment(false);
        isHorizontal = false;
    }

    @Override
    public void setScreenNormal() {
        super.setScreenNormal();

        rlOverLayTiny.setVisibility(GONE);
        fullscreenButton.setImageResource(R.drawable.jz_enlarge);
        lnUploadOffline.setVisibility(GONE);
        LinearLayout.LayoutParams paramsReplay = (LinearLayout.LayoutParams) ivReplay.getLayoutParams();
        paramsReplay.setMargins(0, 0, getResources().getDimensionPixelOffset(R.dimen._30sdp), 0);
        ivReplay.setLayoutParams(paramsReplay);

        LinearLayout.LayoutParams paramsForward = (LinearLayout.LayoutParams) ivForward.getLayoutParams();
        paramsForward.setMargins(getResources().getDimensionPixelOffset(R.dimen._30sdp), 0, 0, 0);
        ivForward.setLayoutParams(paramsForward);

        tinyBackImageView.setVisibility(View.INVISIBLE);
        if (state == STATE_AUTO_COMPLETE) {
            changeUiToComplete();
        }
        changeStartButtonSize((int) getResources().getDimension(R.dimen.jz_start_button_w_h_normal));
        batteryTimeLayout.setVisibility(View.GONE);
        clarity.setVisibility(View.GONE);
        ivFullScreenLive.setVisibility(isFullScreenLive ? View.VISIBLE : View.GONE);

        RelativeLayout.LayoutParams paramBack = (RelativeLayout.LayoutParams) back.getLayoutParams();
        paramBack.height = getResources().getDimensionPixelOffset(R.dimen._24sdp);
        paramBack.width = getResources().getDimensionPixelOffset(R.dimen._24sdp);
        paramBack.setMarginStart(getResources().getDimensionPixelOffset(R.dimen._16sdp));
        back.setLayoutParams(paramBack);

        RelativeLayout.LayoutParams paramMore = (RelativeLayout.LayoutParams) ivMore.getLayoutParams();
        paramMore.width = getResources().getDimensionPixelOffset(R.dimen._24sdp);
        paramMore.height = getResources().getDimensionPixelOffset(R.dimen._24sdp);
        paramMore.setMarginEnd(getResources().getDimensionPixelOffset(R.dimen._16sdp));
        ivMore.setLayoutParams(paramMore);

        RelativeLayout.LayoutParams paramShareVideo = (RelativeLayout.LayoutParams) ivShareVideo.getLayoutParams();
        paramShareVideo.width = getResources().getDimensionPixelOffset(R.dimen._24sdp);
        paramShareVideo.height = getResources().getDimensionPixelOffset(R.dimen._24sdp);
        ivShareVideo.setLayoutParams(paramShareVideo);

        ivMore.setPadding(getResources().getDimensionPixelOffset(R.dimen._4sdp),
                getResources().getDimensionPixelOffset(R.dimen._4sdp),
                getResources().getDimensionPixelOffset(R.dimen._4sdp),
                getResources().getDimensionPixelOffset(R.dimen._4sdp));

        RelativeLayout.LayoutParams paramComment = (RelativeLayout.LayoutParams) rlComment.getLayoutParams();
        paramComment.rightMargin = getResources().getDimensionPixelOffset(R.dimen._16sdp);
        rlComment.setLayoutParams(paramComment);

//        back.setPadding(getResources().getDimensionPixelOffset(R.dimen._4sdp),
//                getResources().getDimensionPixelOffset(R.dimen._4sdp),
//                getResources().getDimensionPixelOffset(R.dimen._4sdp),
//                getResources().getDimensionPixelOffset(R.dimen._4sdp));

        ivShareVideo.setPadding(getResources().getDimensionPixelOffset(R.dimen._4sdp),
                getResources().getDimensionPixelOffset(R.dimen._4sdp),
                getResources().getDimensionPixelOffset(R.dimen._4sdp),
                getResources().getDimensionPixelOffset(R.dimen._4sdp));

    }

    @Override
    public void setScreenFullscreen() {
        super.setScreenFullscreen();
        ivComment.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                JZUtils.hideSystemUI(jzvdContext);
                LinearLayout.LayoutParams paramsReplay = (LinearLayout.LayoutParams) ivReplay.getLayoutParams();
                paramsReplay.setMargins(0, 0, getResources().getDimensionPixelOffset(R.dimen._80sdp), 0);
                ivReplay.setLayoutParams(paramsReplay);

                LinearLayout.LayoutParams paramsForward = (LinearLayout.LayoutParams) ivForward.getLayoutParams();
                paramsForward.setMargins(getResources().getDimensionPixelOffset(R.dimen._80sdp), 0, 0, 0);
                ivForward.setLayoutParams(paramsForward);
            }
        }, 500);
//        if (autoPlay) {
//            hideAllControl();
//            return;
//        }
        setShowComment();
        if (type == STYLE_UPLOAD) {
            lnUploadOffline.setVisibility(VISIBLE);
            ivShareVideo.setVisibility(GONE);
        }

        if (controllerListener != null) controllerListener.showDialogInfoPlayer();
        fullscreenButton.setImageResource(R.drawable.jz_shrink);

        ivFullScreenLive.setVisibility(isFullScreenLive ? View.GONE : View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
        tinyBackImageView.setVisibility(View.INVISIBLE);
        batteryTimeLayout.setVisibility(View.GONE);

        RelativeLayout.LayoutParams paramBack = (RelativeLayout.LayoutParams) back.getLayoutParams();
        paramBack.height = getResources().getDimensionPixelOffset(R.dimen._30sdp);
        paramBack.width = getResources().getDimensionPixelOffset(R.dimen._30sdp);
        paramBack.setMarginStart(JZUtils.getNavigationBarHeight(jzvdContext) + getResources().getDimensionPixelOffset(R.dimen._10sdp));
        back.setLayoutParams(paramBack);

        RelativeLayout.LayoutParams paramMore = (RelativeLayout.LayoutParams) ivMore.getLayoutParams();
        paramMore.width = getResources().getDimensionPixelOffset(R.dimen._30sdp);
        paramMore.height = getResources().getDimensionPixelOffset(R.dimen._30sdp);
        paramBack.setMarginEnd(JZUtils.getNavigationBarHeight(jzvdContext) + getResources().getDimensionPixelOffset(R.dimen._10sdp));
        ivMore.setLayoutParams(paramMore);

        RelativeLayout.LayoutParams paramShareVideo = (RelativeLayout.LayoutParams) ivShareVideo.getLayoutParams();
        paramShareVideo.width = getResources().getDimensionPixelOffset(R.dimen._30sdp);
        paramShareVideo.height = getResources().getDimensionPixelOffset(R.dimen._30sdp);
        ivShareVideo.setLayoutParams(paramShareVideo);

        ivMore.setPadding(getResources().getDimensionPixelOffset(R.dimen._7sdp),
                getResources().getDimensionPixelOffset(R.dimen._7sdp),
                getResources().getDimensionPixelOffset(R.dimen._7sdp),
                getResources().getDimensionPixelOffset(R.dimen._7sdp));

        RelativeLayout.LayoutParams paramComment = (RelativeLayout.LayoutParams) rlComment.getLayoutParams();
        paramComment.rightMargin = JZUtils.getNavigationBarHeight(jzvdContext) + getResources().getDimensionPixelOffset(R.dimen._10sdp);
        rlComment.setLayoutParams(paramComment);

//        back.setPadding(getResources().getDimensionPixelOffset(R.dimen._7sdp),
//                getResources().getDimensionPixelOffset(R.dimen._7sdp),
//                getResources().getDimensionPixelOffset(R.dimen._7sdp),
//                getResources().getDimensionPixelOffset(R.dimen._7sdp));

        ivShareVideo.setPadding(getResources().getDimensionPixelOffset(R.dimen._7sdp),
                getResources().getDimensionPixelOffset(R.dimen._7sdp),
                getResources().getDimensionPixelOffset(R.dimen._7sdp),
                getResources().getDimensionPixelOffset(R.dimen._7sdp));

        if (jzDataSource == null || jzDataSource.urlsMap.size() <= 1) {
            clarity.setVisibility(GONE);
        } else {
            clarity.setText(jzDataSource.getCurrentKey().toString());
            clarity.setVisibility(View.VISIBLE);
        }
        changeStartButtonSize((int) getResources().getDimension(R.dimen.jz_start_button_w_h_fullscreen));
        setSystemTimeAndBattery();
    }

    float x1 = 0f;
    float x2 = 0f;
    int MIN_DISTANCE = 150;

    @Override
    public void setScreenTiny() {
        super.setScreenTiny();
        layoutLive.setVisibility(GONE);
        rlOverLayTiny.setVisibility(VISIBLE);
        ivTinyFullScreen.setVisibility(GONE);
        ivTinyFullScreen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                JZUtils.showSystemUI(getContext());
                if (controllerListener != null) controllerListener.onExpandScreen();
            }
        });
        rlOverLayTiny.setOnClickListener(view -> {
            ivTinyFullScreen.setVisibility(VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ivTinyFullScreen.setVisibility(GONE);
                }
            }, 1000);
        });
        rlOverLayTiny.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        float deltaX = x2 - x1;
                        if (abs(deltaX) > MIN_DISTANCE || abs(x1 - x2) > MIN_DISTANCE) {
                            if (controllerListener != null) {
                                controllerListener.onCloseScreenTiny();
                            }
                        }
                        break;
                }
                return false;
            }
        });
        tinyBackImageView.setVisibility(View.VISIBLE);
        setAllControlsVisiblity(View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
                View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE);
        batteryTimeLayout.setVisibility(View.GONE);
        clarity.setVisibility(View.GONE);
    }

    @Override
    public void showWifiDialog() {
        super.showWifiDialog();
        AlertDialog.Builder builder = new AlertDialog.Builder(jzvdContext);
        builder.setMessage(getResources().getString(R.string.tips_not_wifi));
        builder.setPositiveButton(getResources().getString(R.string.tips_not_wifi_confirm), (dialog, which) -> {
            dialog.dismiss();
            WIFI_TIP_DIALOG_SHOWED = true;
            if (state == STATE_PAUSE) {
                startButton.performClick();
            } else {
                startVideo();
            }

        });
        builder.setNegativeButton(getResources().getString(R.string.tips_not_wifi_cancel), (dialog, which) -> {
            dialog.dismiss();
            releaseAllVideos();
            clearFloatScreen();
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                releaseAllVideos();
                clearFloatScreen();
            }
        });

        builder.create().show();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        super.onStartTrackingTouch(seekBar);
        cancelDismissControlViewTimer();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        super.onStopTrackingTouch(seekBar);
        startDismissControlViewTimer();
    }

    public void onClickUiToggle() {
        if (bottomContainer.getVisibility() != View.VISIBLE) {
            setSystemTimeAndBattery();
            if (clarity != null && jzDataSource != null)
                clarity.setText(jzDataSource.getCurrentKey());
        }
        if (state == STATE_PREPARING) {
            changeUiToPreparing();
            if (bottomContainer.getVisibility() == View.VISIBLE) {
            } else {
                setSystemTimeAndBattery();
            }
        } else if (state == STATE_PLAYING) {
            if (bottomContainer.getVisibility() == View.VISIBLE) {
                changeUiToPlayingClear();
            } else {
                changeUiToPlayingShow();
            }
        } else if (state == STATE_PAUSE) {
            if (bottomContainer.getVisibility() == View.VISIBLE) {
                changeUiToPauseClear();
            } else {
                changeUiToPauseShow();
            }
        }
    }

    public void setSystemTimeAndBattery() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        videoCurrentTime.setText(dateFormater.format(date));
        if ((System.currentTimeMillis() - LAST_GET_BATTERYLEVEL_TIME) > 30000) {
            LAST_GET_BATTERYLEVEL_TIME = System.currentTimeMillis();
            jzvdContext.registerReceiver(
                    battertReceiver,
                    new IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            );
        } else {
            setBatteryLevel();
        }
    }

    public void setBatteryLevel() {
        int percent = LAST_GET_BATTERYLEVEL_PERCENT;
        if (percent < 15) {
            batteryLevel.setBackgroundResource(R.drawable.jz_battery_level_10);
        } else if (percent >= 15 && percent < 40) {
            batteryLevel.setBackgroundResource(R.drawable.jz_battery_level_30);
        } else if (percent >= 40 && percent < 60) {
            batteryLevel.setBackgroundResource(R.drawable.jz_battery_level_50);
        } else if (percent >= 60 && percent < 80) {
            batteryLevel.setBackgroundResource(R.drawable.jz_battery_level_70);
        } else if (percent >= 80 && percent < 95) {
            batteryLevel.setBackgroundResource(R.drawable.jz_battery_level_90);
        } else if (percent >= 95 && percent <= 100) {
            batteryLevel.setBackgroundResource(R.drawable.jz_battery_level_100);
        }
    }

    public void onCLickUiToggleToClear() {
        if (state == STATE_PREPARING) {
            if (bottomContainer.getVisibility() == View.VISIBLE) {
                changeUiToPreparing();
            } else {
            }
        } else if (state == STATE_PLAYING) {
            if (bottomContainer.getVisibility() == View.VISIBLE) {
                changeUiToPlayingClear();
            } else {
            }
        } else if (state == STATE_PAUSE) {
            if (bottomContainer.getVisibility() == View.VISIBLE) {
                changeUiToPauseClear();
            } else {
            }
        } else if (state == STATE_AUTO_COMPLETE) {
            if (bottomContainer.getVisibility() == View.VISIBLE) {
                changeUiToComplete();
            } else {
            }
        }
    }

    @Override
    public void onProgress(int progress, long position, long duration) {
        super.onProgress(progress, position, duration);
        if (progress != 0) bottomProgressBar.setProgress(progress);
    }

    @Override
    public void setBufferProgress(int bufferProgress) {
        super.setBufferProgress(bufferProgress);
        if (bufferProgress != 0) bottomProgressBar.setSecondaryProgress(bufferProgress);
    }

    @Override
    public void resetProgressAndTime() {
        super.resetProgressAndTime();
        bottomProgressBar.setProgress(0);
        bottomProgressBar.setSecondaryProgress(0);
    }

    public void changeUiToNormal() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.VISIBLE, View.INVISIBLE, View.VISIBLE,
                        View.INVISIBLE, View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
                updateStartImage();
                break;
            case SCREEN_TINY:
                break;
        }
    }

    public void changeUiToPreparing() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.VISIBLE, View.INVISIBLE, View.INVISIBLE,
                        View.VISIBLE, View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
                updateStartImage();

                break;
            case SCREEN_TINY:
                break;
        }
    }

    public void changeUIToPreparingPlaying() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.VISIBLE, View.VISIBLE, View.INVISIBLE,
                        View.VISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE);
                updateStartImage();
                break;
            case SCREEN_TINY:
                break;
        }
    }

    public void changeUIToPreparingChangeUrl() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
                        View.VISIBLE, View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
                updateStartImage();
                break;
            case SCREEN_TINY:
                break;
        }
    }

    public void changeUiToPlayingShow() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.VISIBLE, View.VISIBLE, View.VISIBLE,
                        View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE);
                updateStartImage();
                break;
            case SCREEN_TINY:
                break;
        }

    }

    public void hideAllControl() {
        topContainer.setVisibility(View.GONE);
        bottomContainer.setVisibility(View.GONE);
        startButton.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.GONE);
        posterImageView.setVisibility(View.GONE);
        bottomProgressBar.setVisibility(View.GONE);
        mRetryLayout.setVisibility(View.GONE);
        rlComment.setVisibility(View.GONE);
//        startLayout.setVisibility(View.GONE);
    }

    public void setAllControlsVisibility(int topCon, int bottomCon, int startBtn, int loadingPro,
                                         int posterImg, int bottomPro, int retryLayout) {
//        if (autoPlay) {
//            hideAllControl();
////            startButton.setVisibility(startBtn);
//            return;
//        }
        ivReplay.setVisibility(startBtn);
        ivForward.setVisibility(startBtn);
        topContainer.setVisibility(topCon);
        bottomContainer.setVisibility(bottomCon);
        startButton.setVisibility(startBtn);
        loadingProgressBar.setVisibility(loadingPro);
        posterImageView.setVisibility(posterImg);
        bottomProgressBar.setVisibility(bottomPro);
        mRetryLayout.setVisibility(retryLayout);
    }

    public void changeUiToPlayingClear() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisibility(View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
                        View.INVISIBLE, View.INVISIBLE, View.VISIBLE, View.INVISIBLE);
                break;
            case SCREEN_TINY:
                break;
        }

    }

    public void changeUiToPauseShow() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.VISIBLE, View.VISIBLE, View.VISIBLE,
                        View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE);
                updateStartImage();
                break;
            case SCREEN_TINY:
                break;
        }
    }

    public void changeUiToPauseClear() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
                        View.INVISIBLE, View.INVISIBLE, View.VISIBLE, View.INVISIBLE);
                break;
            case SCREEN_TINY:
                break;
        }

    }

    public void changeUiToComplete() {
        switch (screen) {
            case SCREEN_NORMAL:
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.VISIBLE, View.INVISIBLE, View.VISIBLE,
                        View.INVISIBLE, View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
                updateStartImage();
                break;
            case SCREEN_TINY:
                break;
        }

    }



    public void changeUiToError() {
        switch (screen) {
            case SCREEN_NORMAL:
                setAllControlsVisiblity(View.INVISIBLE, View.INVISIBLE, View.VISIBLE,
                        View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.VISIBLE);
                updateStartImage();
                break;
            case SCREEN_FULLSCREEN:
                setAllControlsVisiblity(View.VISIBLE, View.INVISIBLE, View.VISIBLE,
                        View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.VISIBLE);
                updateStartImage();
                break;
            case SCREEN_TINY:
                break;
        }

    }

    public void setAllControlsVisiblity(int topCon, int bottomCon, int startBtn, int loadingPro,
                                        int posterImg, int bottomPro, int retryLayout) {
//        if (autoPlay) {
//            hideAllControl();
//            return;
//        }
        setKeyDown();
        topContainer.setVisibility(topCon);
        bottomContainer.setVisibility(bottomCon);
//        rlLiveBottom.setVisibility(bottomCon);
        startButton.setVisibility(startBtn);
        if (type == STYLE_NORMAL || type == STYLE_UPLOAD) {
            ivForward.setVisibility(startBtn);
            ivReplay.setVisibility(startBtn);
        } else {
            ivForward.setVisibility(GONE);
            ivReplay.setVisibility(GONE);
        }

        loadingProgressBar.setVisibility(loadingPro);
        posterImageView.setVisibility(posterImg);
        if (isFullScreenLive) {
            if (type == NORMAL_ORIENTATION) {
                bottomProgressBar.setVisibility(GONE);
            } else {
                bottomProgressBar.setVisibility(bottomPro);
            }

        } else {
            bottomProgressBar.setVisibility(GONE);
        }
        mRetryLayout.setVisibility(retryLayout);
    }

    public void updateStartImage() {
//        if (autoPlay) {
//            return;
//        }
        if (state == STATE_PLAYING) {
            startButton.setVisibility(VISIBLE);
            ivReplay.setVisibility(isFullScreenLive ? GONE : VISIBLE);
            ivForward.setVisibility(isFullScreenLive ? GONE : VISIBLE);
            startButton.setImageResource(R.drawable.ic_pause_es);
            replayTextView.setVisibility(GONE);
        } else if (state == STATE_ERROR) {
            startButton.setVisibility(INVISIBLE);
            replayTextView.setVisibility(GONE);
            ivReplay.setVisibility(GONE);
            ivForward.setVisibility(GONE);
        } else if (state == STATE_AUTO_COMPLETE) {
            startButton.setVisibility(VISIBLE);
            ivReplay.setVisibility(isFullScreenLive ? GONE : VISIBLE);
            ivForward.setVisibility(isFullScreenLive ? GONE : VISIBLE);
            startButton.setImageResource(R.drawable.jz_click_replay_selector);
            replayTextView.setVisibility(VISIBLE);
            topContainer.setVisibility(VISIBLE);
        } else {
            startButton.setImageResource(R.drawable.ic_play_es);
            replayTextView.setVisibility(GONE);
        }
    }

    @Override
    public void showProgressDialog(float deltaX, String seekTime, long seekTimePosition, String totalTime, long totalTimeDuration) {
        super.showProgressDialog(deltaX, seekTime, seekTimePosition, totalTime, totalTimeDuration);

        if (mProgressDialog == null) {
            View localView = LayoutInflater.from(jzvdContext).inflate(R.layout.jz_dialog_progress, null);
            mDialogProgressBar = localView.findViewById(R.id.duration_progressbar);
            mDialogSeekTime = localView.findViewById(R.id.tv_current);
            mDialogTotalTime = localView.findViewById(R.id.tv_duration);
            mDialogIcon = localView.findViewById(R.id.duration_image_tip);
            mProgressDialog = createDialogWithView(localView);
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }

        mDialogSeekTime.setText(seekTime);
        mDialogTotalTime.setText(" / " + totalTime);
        mDialogProgressBar.setProgress(totalTimeDuration <= 0 ? 0 : (int) (seekTimePosition * 100 / totalTimeDuration));
        if (deltaX > 0) {
            mDialogIcon.setBackgroundResource(R.drawable.jz_forward_icon);
        } else {
            mDialogIcon.setBackgroundResource(R.drawable.jz_backward_icon);
        }
        onCLickUiToggleToClear();
    }

    @Override
    public void dismissProgressDialog() {
        super.dismissProgressDialog();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showVolumeDialog(float deltaY, int volumePercent) {
        super.showVolumeDialog(deltaY, volumePercent);
        if (mVolumeDialog == null) {
            View localView = LayoutInflater.from(jzvdContext).inflate(R.layout.jz_dialog_volume, null);
            mDialogVolumeImageView = localView.findViewById(R.id.volume_image_tip);
            mDialogVolumeTextView = localView.findViewById(R.id.tv_volume);
            mDialogVolumeProgressBar = localView.findViewById(R.id.volume_progressbar);
            mVolumeDialog = createDialogWithView(localView);
        }
        if (!mVolumeDialog.isShowing()) {
            mVolumeDialog.show();
        }
        if (volumePercent <= 0) {
            mDialogVolumeImageView.setBackgroundResource(R.drawable.jz_close_volume);
        } else {
            mDialogVolumeImageView.setBackgroundResource(R.drawable.jz_add_volume);
        }
        if (volumePercent > 100) {
            volumePercent = 100;
        } else if (volumePercent < 0) {
            volumePercent = 0;
        }
        mDialogVolumeTextView.setText(volumePercent + "%");
        mDialogVolumeProgressBar.setProgress(volumePercent);
        onCLickUiToggleToClear();
    }

    @Override
    public void dismissVolumeDialog() {
        super.dismissVolumeDialog();
        if (mVolumeDialog != null) {
            mVolumeDialog.dismiss();
        }
    }

    @Override
    public void showBrightnessDialog(int brightnessPercent) {
        super.showBrightnessDialog(brightnessPercent);
        if (mBrightnessDialog == null) {
            View localView = LayoutInflater.from(jzvdContext).inflate(R.layout.jz_dialog_brightness, null);
            mDialogBrightnessTextView = localView.findViewById(R.id.tv_brightness);
            mDialogBrightnessProgressBar = localView.findViewById(R.id.brightness_progressbar);
            mBrightnessDialog = createDialogWithView(localView);
        }
        if (!mBrightnessDialog.isShowing()) {
            mBrightnessDialog.show();
        }
        if (brightnessPercent > 100) {
            brightnessPercent = 100;
        } else if (brightnessPercent < 0) {
            brightnessPercent = 0;
        }
        mDialogBrightnessTextView.setText(brightnessPercent + "%");
        mDialogBrightnessProgressBar.setProgress(brightnessPercent);
        onCLickUiToggleToClear();
    }

    @Override
    public void dismissBrightnessDialog() {
        super.dismissBrightnessDialog();
        if (mBrightnessDialog != null) {
            mBrightnessDialog.dismiss();
        }
    }

    public Dialog createDialogWithView(View localView) {
        Dialog dialog = new Dialog(jzvdContext, R.style.jz_style_dialog_progress);
        dialog.setContentView(localView);
        Window window = dialog.getWindow();
        window.addFlags(Window.FEATURE_ACTION_BAR);
        window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        window.setLayout(-2, -2);
        WindowManager.LayoutParams localLayoutParams = window.getAttributes();
        localLayoutParams.gravity = Gravity.CENTER;
        window.setAttributes(localLayoutParams);
        return dialog;
    }

    public void startDismissControlViewTimer() {
        cancelDismissControlViewTimer();
        DISMISS_CONTROL_VIEW_TIMER = new Timer();
        mDismissControlViewTimerTask = new DismissControlViewTimerTask();
        DISMISS_CONTROL_VIEW_TIMER.schedule(mDismissControlViewTimerTask, 2500);
    }

    public void cancelDismissControlViewTimer() {
        if (DISMISS_CONTROL_VIEW_TIMER != null) {
            DISMISS_CONTROL_VIEW_TIMER.cancel();
        }
        if (mDismissControlViewTimerTask != null) {
            mDismissControlViewTimerTask.cancel();
        }

    }

    @Override
    public void onCompletion() {
        super.onCompletion();
        cancelDismissControlViewTimer();
    }

    @Override
    public void reset() {
        super.reset();
        cancelDismissControlViewTimer();
        unregisterWifiListener(getApplicationContext());
    }

    public void dissmissControlView() {
        if (state != STATE_NORMAL
                && state != STATE_ERROR
                && state != STATE_AUTO_COMPLETE) {
            post(() -> {
                bottomContainer.setVisibility(View.INVISIBLE);
//                rlLiveBottom.setVisibility(View.INVISIBLE);
                topContainer.setVisibility(View.INVISIBLE);
                startButton.setVisibility(View.INVISIBLE);
                ivReplay.setVisibility(GONE);
                ivForward.setVisibility(GONE);

                if (screen != SCREEN_TINY) {
                    if (type == STYLE_LIVE) {
                        bottomProgressBar.setVisibility(View.VISIBLE);
                    } else {
                        bottomProgressBar.setVisibility(GONE);
                    }
                }
            });
        }
    }

    public void registerWifiListener(Context context) {
        if (context == null) return;
        mIsWifi = JZUtils.isWifiConnected(context);
        isRegisterWifi = true;
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(wifiReceiver, intentFilter);
    }

    public void unregisterWifiListener(Context context) {
        if (context == null) return;
        try {
            if (mIsWifi) {
                if (wifiReceiver != null) {
                    context.unregisterReceiver(wifiReceiver);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissControlView() {
        if (state != STATE_NORMAL
                && state != STATE_ERROR
                && state != STATE_AUTO_COMPLETE) {
            post(() -> {
                bottomContainer.setVisibility(View.INVISIBLE);
                topContainer.setVisibility(View.INVISIBLE);
                startButton.setVisibility(View.INVISIBLE);

                if (screen != SCREEN_TINY ) {
                    bottomProgressBar.setVisibility(View.VISIBLE);
                } else {
                    bottomProgressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    public class DismissControlViewTimerTask extends TimerTask {

        @Override
        public void run() {
            dissmissControlView();
        }
    }
}

package com.metfone.esport.customview.player;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Surface;

import com.blankj.utilcode.util.NetworkUtils;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoListener;
import com.metfone.selfcare.BuildConfig;

public class JZMediaExo extends JZMediaInterface implements Player.EventListener, VideoListener {
    private SimpleExoPlayer simpleExoPlayer;
    private Runnable callback;
    private String TAG = "JZMediaExo";
    private long previousSeek = 0;
    private CustomBandwidthMeter bandwidthMeter;
    private DefaultTrackSelector trackSelector;
    private int currentState = 0;
    private long timeBufferFirst = 0;
    private long timeStart = 0;
    private long timeLagStart = 0;
    private long timePlayStart = 0;
    private long totalTimePlay = 0;
    private String lagArr = "";
    private String playArr = "";
    private String bandwidthArr = "";
    private String networkArr = "";
    private String errorDesc = "";
    private String msisdn = "";
    private boolean isSeekBeforeBuffering;
    private boolean isStartVideo = false;
    private boolean isVideoPlayed = false;
    private boolean isPlaying = false;

    public JZMediaExo(Jzvd jzvd) {
        super(jzvd);
    }

    @Override
    public void start() {
        if (simpleExoPlayer != null) simpleExoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void prepare() {
        Log.e(TAG, "prepare");
        Context context = jzvd.getContext();
        release();
        mMediaHandlerThread = new HandlerThread("JZVD");
        mMediaHandlerThread.start();
        mMediaHandler = new Handler(mMediaHandlerThread.getLooper());//主线程还是非主线程，就在这里
        handler = new Handler();
        mMediaHandler.post(() -> {
            if (bandwidthMeter == null)
                bandwidthMeter = new CustomBandwidthMeter.Builder(context).build();
            if (simpleExoPlayer == null) {
                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
                trackSelector = new DefaultTrackSelector(context, videoTrackSelectionFactory);

                LoadControl loadControl = new DefaultLoadControl.Builder()
                        .setAllocator(new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE))
                        .setBufferDurationsMs(10000, 600000, 1000, 5000)
                        .setTargetBufferBytes(C.LENGTH_UNSET)
                        .setPrioritizeTimeOverSizeThresholds(false)
                        .setBackBuffer(0, false).createDefaultLoadControl();

                // 2. Create the player
//                RenderersFactory renderersFactory = new DefaultRenderersFactory(context);
//            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context, renderersFactory, trackSelector, loadControl);
                simpleExoPlayer = new SimpleExoPlayer.Builder(context)
                        .setTrackSelector(trackSelector)
                        .setLoadControl(loadControl)
                        .build();
            }
            // Produces DataSource instances through which media data is loaded.
            String userAgent = Util.getUserAgent(context, "MetfonePlayer");
            DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
            if (msisdn != null)
                httpDataSourceFactory.getDefaultRequestProperties().set("msisdn", msisdn);
            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(context, bandwidthMeter, httpDataSourceFactory);
            String currUrl = jzvd.jzDataSource.getCurrentUrl().toString();
            MediaSource videoSource;
            if (currUrl.contains(".m3u8")) {
                videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.parse(currUrl));
            } else {
                videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.parse(currUrl));
            }
            if (BuildConfig.DEBUG) Log.e(TAG, "mediaUrl: " + currUrl);
            if (this instanceof VideoListener) simpleExoPlayer.addVideoListener(this);
            if (this instanceof Player.EventListener) simpleExoPlayer.addListener(this);

            boolean isLoop = jzvd.jzDataSource.looping;
            if (isLoop) {
                simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            } else {
                simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
            }
            simpleExoPlayer.prepare(videoSource);
            simpleExoPlayer.setPlayWhenReady(true);
            callback = new onBufferingUpdate();

            if (jzvd.textureView != null) {
                SurfaceTexture surfaceTexture = jzvd.textureView.getSurfaceTexture();
                if (surfaceTexture != null) {
                    simpleExoPlayer.setVideoSurface(new Surface(surfaceTexture));
                }
            }
        });
        isSeekBeforeBuffering = false;
        //logStart(currentLog);
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        handler.post(() -> jzvd.onVideoSizeChanged(width, height));
    }

    @Override
    public void onRenderedFirstFrame() {
        Log.e(TAG, "onRenderedFirstFrame");
    }

    @Override
    public void pause() {
        if (simpleExoPlayer != null) simpleExoPlayer.setPlayWhenReady(false);
    }

    @Override
    public boolean isPlaying() {
        return simpleExoPlayer != null && simpleExoPlayer.getPlayWhenReady();
    }

    @Override
    public void seekTo(long time) {
        Log.e(TAG, "seekTo: " + time);
        if (simpleExoPlayer == null) {
            return;
        }
        if (time != previousSeek) {
            if (time >= simpleExoPlayer.getBufferedPosition()) {
                jzvd.showProgress();
            }
            simpleExoPlayer.seekTo(time);
            previousSeek = time;
            jzvd.seekToInAdvance = time;

        }
        onHaveSeek(true);
    }

    @Override
    public void release() {
        if (mMediaHandler != null && mMediaHandlerThread != null && simpleExoPlayer != null) {//不知道有没有妖孽
            HandlerThread tmpHandlerThread = mMediaHandlerThread;
            SimpleExoPlayer tmpMediaPlayer = simpleExoPlayer;
            JZMediaInterface.SAVED_SURFACE = null;

            mMediaHandler.post(() -> {
                tmpMediaPlayer.release();//release就不能放到主线程里，界面会卡顿
                tmpHandlerThread.quit();
            });
            simpleExoPlayer = null;
            trackSelector = null;
        }
    }

    @Override
    public long getCurrentPosition() {
        if (simpleExoPlayer != null)
            return simpleExoPlayer.getCurrentPosition();
        else return 0;
    }

    @Override
    public long getDuration() {
        if (simpleExoPlayer != null)
            return simpleExoPlayer.getDuration();
        else return 0;
    }

    @Override
    public void setVolume(float leftVolume, float rightVolume) {
        Log.e(TAG, "leftVolume: " + leftVolume + " rightVolume: " + rightVolume);
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setVolume(leftVolume);
            simpleExoPlayer.setVolume(rightVolume);
        }
    }

    @Override
    public void setSpeed(float speed) {
        if (simpleExoPlayer != null) {
            PlaybackParameters playbackParameters = new PlaybackParameters(speed, 1.0F);
            simpleExoPlayer.setPlaybackParameters(playbackParameters);
        }
    }

    @Override
    public void onTimelineChanged(final Timeline timeline, Object manifest, final int reason) {
        Log.e(TAG, "onTimelineChanged");
//        JZMediaPlayer.instance().mainThreadHandler.post(() -> {
//                if (reason == 0) {
//
//                    JzvdMgr.getCurrentJzvd().onInfo(reason, timeline.getPeriodCount());
//                }
//        });
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        Log.e(TAG, "onLoadingChanged");
    }

    @Override
    public void onPlayerStateChanged(final boolean playWhenReady, final int playbackState) {
        Log.e(TAG, "onPlayerStateChanged" + playbackState + "/ready=" + String.valueOf(playWhenReady));
        if (isPlaying != playWhenReady) {
            isPlaying = playWhenReady;
            if (jzvd.autoPlayListener != null)
                jzvd.autoPlayListener.onPlayerStateChanged(isPlaying);
        }
        updateLog(playbackState);
        currentState = playbackState;
        handler.post(() -> {
            switch (playbackState) {
                case Player.STATE_IDLE: {
                }
                break;
                case Player.STATE_BUFFERING: {
                    jzvd.showProgress();
                    handler.post(callback);
                    isVideoPlayed = true;
                }
                break;
                case Player.STATE_READY: {
                    if (playWhenReady) {
                        jzvd.hideProgress();
                        jzvd.onStatePlaying();
                        if (jzvd.autoPlayListener != null) jzvd.autoPlayListener.onPrepared();
                    }
                    isVideoPlayed = true;
                }
                break;
                case Player.STATE_ENDED: {
                    jzvd.onCompletion();
                    logEnd(currentLog);
                }
                break;
            }
        });

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.e(TAG, "onPlayerError" + error.toString());
        try {
            String msg = error.getCause().toString();
            if (error.getCause().getStackTrace() != null && error.getCause().getStackTrace().length > 0) {
                String msgDetail = error.getCause().getStackTrace()[0].toString();
                msg += " , " + msgDetail;
            }
            errorDesc = msg;
        } catch (Exception e) {
            e.printStackTrace();
            errorDesc = "Error media";
        }
        if (currentLog != null) {
            logEnd(currentLog);
        }
        handler.post(() -> jzvd.onError(1000, 1000));
    }

    @Override
    public void onPositionDiscontinuity(int reason) {
        Log.i(TAG, "onPositionDiscontinuity reason: " + reason);
        switch (reason) {
            case Player.DISCONTINUITY_REASON_SEEK:
            case Player.DISCONTINUITY_REASON_SEEK_ADJUSTMENT:
                if (!isStartVideo) {
                    isSeekBeforeBuffering = true;
                }
                break;

            case Player.DISCONTINUITY_REASON_PERIOD_TRANSITION:
            case Player.DISCONTINUITY_REASON_AD_INSERTION:
            case Player.DISCONTINUITY_REASON_INTERNAL:
            default:
                isSeekBeforeBuffering = false;
                break;
        }
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {
        handler.post(() -> jzvd.onSeekComplete());
    }

    @Override
    public void setSurface(Surface surface) {
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setVideoSurface(surface);
        } else {
            Log.e(TAG, "simpleExoPlayer");
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (SAVED_SURFACE == null) {
            SAVED_SURFACE = surface;
            prepare();
        } else {
            jzvd.textureView.setSurfaceTexture(SAVED_SURFACE);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
    private LogModel currentLog;
    @Override
    public void logViewEnd() {
        logEnd(currentLog);
    }

    @Override
    public void logViewStart() {
        logStart(currentLog);
    }

    @Override
    public boolean isHasTrackSelection() {
        return trackSelector != null && ExoPlayerUtils.willHaveContent(trackSelector);
    }

    @Override
    public DefaultTrackSelector getTrackSelector() {
        return trackSelector;
    }

    private void logStart(LogModel model) {
        isSeekBeforeBuffering = false;
        model.setState("START");
        isStartVideo = true;
        timeStart = System.currentTimeMillis();
        callApiLogView(model);
        isVideoPlayed = false;
    }

    private void logEnd(LogModel model) {
        model.setState("END");
        if (currentState == Player.STATE_BUFFERING || currentState == Player.STATE_IDLE) {
            //todo update lagArr

            long timeLag = System.currentTimeMillis() - timeLagStart;
            if (timeLag > 0 && timeLagStart > 0) {
                lagArr += timeLag + ":" + (isSeekBeforeBuffering ? 1 : 0);
                Log.e(TAG, "lagArr: " + lagArr + " timeLagStart: " + timeLagStart);
                if (timeBufferFirst == 0) timeBufferFirst = timeLag;
            }
            isSeekBeforeBuffering = false;
        } else if (currentState == Player.STATE_READY) {
            //todo update playArr
            long timePlay = System.currentTimeMillis() - timePlayStart;
            if (timePlay > 0 && timePlayStart > 0) {
                playArr += timePlay;
                totalTimePlay += timePlay;
                Log.e(TAG, "totalTimePlay: " + totalTimePlay + ", playArr: " + playArr);
                updateBandwidthArr(true);
            }
        }
        callApiLogView(model);
    }

    private void callApiLogView(LogModel currentVideo) {
        if (currentVideo == null) return;
        long timePlay = 0;
        String timeLog;
        if (currentVideo.getState().equals("END")) {
            if (currentState == Player.STATE_ENDED)
                timePlay = getDuration();
            else
                timePlay = getCurrentPosition();
            long current = timePlay / 1000;
            if (current >= 5 && current < 15) {
                if (!currentVideo.isLog5()) {
                    currentVideo.setLog5(true);
                    currentVideo.setLog15(false);
                    currentVideo.setLog30(false);
                }
            } else if (current >= 15 && current < 30) {
                if (!currentVideo.isLog15()) {
                    currentVideo.setLog5(true);
                    currentVideo.setLog15(true);
                    currentVideo.setLog30(false);
                }
            } else if (current >= 30) {
                if (!currentVideo.isLog30()) {
                    currentVideo.setLog5(true);
                    currentVideo.setLog15(true);
                    currentVideo.setLog30(true);
                }
            } else {
                currentVideo.setLog5(false);
                currentVideo.setLog15(false);
                currentVideo.setLog30(false);
            }
            if (timePlay < 0) timePlay = 0;
            timeLog = timeBufferFirst + "|" + timePlay + "|" + (currentVideo.isLog5() ? 1 : 0) + "|" + (currentVideo.isLog15() ? 1 : 0) + "|" +
                    (currentVideo.isLog30() ? 1 : 0) + "|" + (currentVideo.isSeek() ? 1 : 0) + "|" + (isVideoPlayed ? 1 : 0);
        } else {
            timeLog = "0|0|0|0|0|0|0";
            lagArr = "";
            playArr = "";
            errorDesc = "";
            totalTimePlay = 0;
        }
        Log.e(TAG, currentVideo.getState() + "| " + "TimeBuffer: " + timeBufferFirst + "| TimePlay: " + timePlay + "| TimeLog: " + timeLog + "| PlayArr: " + playArr + "| LagArr: " + lagArr);

        currentVideo.setTimeLog(timeLog);
        currentVideo.setLagArray(lagArr);
        currentVideo.setBandwidthArray(bandwidthArr);
        currentVideo.setNetworkArray(networkArr);
        currentVideo.setPlayArray(playArr);
        currentVideo.setErrorDesc(errorDesc);
        currentVideo.setTotalTimePlay(totalTimePlay);
        //if (jzvd.logActionListener != null) jzvd.logActionListener.logView(currentVideo);
        resetParamLog();
    }

    private void resetParamLog() {
        //timePlay = 0;
        timeBufferFirst = 0;
        lagArr = "";
        bandwidthArr = "";
        networkArr = "";
        playArr = "";
        errorDesc = "";
        timeLagStart = 0;
        timePlayStart = 0;
        totalTimePlay = 0;
        isVideoPlayed = false;
//        if (mVideo != null)
//            mVideo.resetParam();
    }

    private void updateBandwidthArr(boolean flag) {
        if (flag) {
            bandwidthArr += bandwidthMeter.getBitrateEstimate();
        } else {
            bandwidthArr += bandwidthMeter.getBitrateEstimate() + "|";
        }
        Log.e(TAG, "bandwidthArr: " + bandwidthArr);
        networkArr += NetworkUtils.getNetworkSubType() + "|";
        Log.e(TAG, "networkArr: " + networkArr);
    }

    private void updateLog(int playbackState) {
        if (playbackState == Player.STATE_BUFFERING) {
            Log.e(TAG, "updateLog STATE_BUFFERING");
            timeLagStart = System.currentTimeMillis();
            Log.e(TAG, "timeLagStart: " + timeLagStart);
            if (currentState == Player.STATE_READY || currentState == Player.STATE_ENDED) {
                //todo update playArr
                long timePlay = timeLagStart - timePlayStart;
                if (timePlay > 0 && timePlayStart > 0) {
                    playArr += timePlay + "|";
                    totalTimePlay += timePlay;
                    Log.e(TAG, "totalTimePlay: " + totalTimePlay + ", playArr: " + playArr);
                    updateBandwidthArr(false);
                }
            }
        } else if (playbackState == Player.STATE_READY) {
            Log.e(TAG, "updateLog STATE_READY");
            if (currentState == Player.STATE_BUFFERING || currentState == Player.STATE_IDLE) {
                timePlayStart = System.currentTimeMillis();
                Log.e(TAG, "timePlayStart: " + timePlayStart + " timeStart: " + timeStart);
                if (isStartVideo) {
                    timeBufferFirst = timePlayStart - timeStart;
                    Log.e(TAG, "timeBufferFirst: " + timeBufferFirst);
                    updateBandwidthArr(false);
                    isStartVideo = false;

                    //Set lan dau tien
                    lagArr += timeBufferFirst + ":" + (isSeekBeforeBuffering ? 1 : 0) + "|";
                } else {
                    //todo update lagArr
                    long timeLag = timePlayStart - timeLagStart;
                    if (timeLag > 0 && timeLagStart > 0) {
                        lagArr += timeLag + ":" + (isSeekBeforeBuffering ? 1 : 0) + "|";
                    }
                }
                Log.e(TAG, "lagArr: " + lagArr);
                isSeekBeforeBuffering = false;
            }
        } else if (playbackState == Player.STATE_IDLE) {
            Log.e(TAG, "updateLog STATE_IDLE");
        } else if (playbackState == Player.STATE_ENDED) {
            Log.e(TAG, "updateLog STATE_ENDED");
        } else {
            Log.e(TAG, "updateLog STATE_OTHER ...");
        }
    }

    public void onHaveSeek(boolean flag) {
        if (currentLog != null)
            currentLog.setSeek(flag);
    }

    private class onBufferingUpdate implements Runnable {
        @Override
        public void run() {
            if (simpleExoPlayer != null) {
                final int percent = simpleExoPlayer.getBufferedPercentage();
                handler.post(() -> jzvd.setBufferProgress(percent));
                if (percent < 100) {
                    handler.postDelayed(callback, 300);
                } else {
                    handler.removeCallbacks(callback);
                }
            }
        }
    }
}

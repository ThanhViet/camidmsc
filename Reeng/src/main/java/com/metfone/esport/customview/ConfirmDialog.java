package com.metfone.esport.customview;

import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.selfcare.R;
import com.metfone.esport.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nguyễn Thành Chung on 9/17/20.
 */
public class ConfirmDialog extends BaseDialogFragment {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.tvAccept)
    TextView tvAccept;
    private OnClickAccept onClickAccept;
    private String title, message, textCancel, textAccept;

    public static ConfirmDialog newInstance(String title, String message, String textCancel, String textAccept, OnClickAccept onClickAccept) {
        ConfirmDialog dialog = new ConfirmDialog();
        dialog.title = title;
        dialog.message = message;
        dialog.textCancel = textCancel;
        dialog.textAccept = textAccept;
        dialog.onClickAccept = onClickAccept;
        return dialog;
    }

    public static ConfirmDialog newInstance(String message, OnClickAccept onClickAccept) {
        ConfirmDialog dialog = new ConfirmDialog();
        dialog.message = message;
        dialog.onClickAccept = onClickAccept;
        return dialog;
    }

    @Override
    protected void initView(View rootView) {
        tvTitle.setVisibility(title == null ? View.GONE : View.VISIBLE);
        tvTitle.setText(title == null ? "" : title);
        tvMessage.setText(message == null ? "" : message);
        tvCancel.setText(textCancel == null ? getString(R.string.cancel) : textCancel);
        tvAccept.setText(textAccept == null ? getString(R.string.un_follow) : textAccept);

    }

    @Override
    protected int getDialogWidth() {
        return ScreenUtils.getScreenWidth() - getResources().getDimensionPixelOffset(R.dimen._50sdp);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_dialog_confirm_es;
    }

    @Override
    public String getTAG() {
        return DialogFragment.class.getSimpleName();
    }

    @OnClick({R.id.tvCancel, R.id.tvAccept})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCancel:
                dismissAllowingStateLoss();
                break;
            case R.id.tvAccept:
                if (onClickAccept != null) {
                    onClickAccept.onClickAccept();
                }
                dismissAllowingStateLoss();
                break;
        }

    }

    public interface OnClickAccept {
        void onClickAccept();
    }
}

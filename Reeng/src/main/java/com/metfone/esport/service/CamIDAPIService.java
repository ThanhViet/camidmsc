package com.metfone.esport.service;

import com.metfone.esport.entity.camid.CamIdBaseResponse;
import com.metfone.esport.entity.camid.CamIdLoginParam;
import com.metfone.esport.entity.camid.CamIdLoginResponse;
import com.metfone.esport.entity.camid.CamIdRefreshTokenParam;
import com.metfone.esport.entity.camid.CamIdRefreshTokenResponse;
import com.metfone.esport.entity.camid.CamIdUserWrapper;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface CamIDAPIService {
    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("/camid-auth/api/v1/auth/signin")
    Observable<CamIdBaseResponse<CamIdLoginResponse>> login(@Header("deviceId") String deviceId,
                                                            @Body CamIdLoginParam param);

    @GET("/camid-auth/api/v1/user")
    Observable<CamIdBaseResponse<CamIdUserWrapper>> getUserId();

    @POST("camid-auth/api/v1/openid/refresh")
    Observable<CamIdBaseResponse<CamIdRefreshTokenResponse>> refreshToken(@Body CamIdRefreshTokenParam body);

}
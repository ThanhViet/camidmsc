package com.metfone.esport.service;

import com.metfone.esport.entity.vtcapi.VTCApiUseRoutingParam;
import com.metfone.esport.entity.vtcapi.VTCBaseResponse;
import com.metfone.esport.entity.vtcapi.VTCRankResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface VTCAPIService {
    @POST("/CoreService/UserRouting")
    Observable<VTCBaseResponse<VTCRankResponse>> wsAccountInfo(@Body VTCApiUseRoutingParam param);

}
package com.metfone.esport.service;

import com.blankj.utilcode.util.DeviceUtils;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.BuildConfig;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceRetrofit {
    private static final String TAG = ServiceRetrofit.class.getCanonicalName();
    private static Interceptor interceptor = chain -> {
        Request original = chain.request();
        Request request = null;
        request = original.newBuilder()
                .header("languageCode", AccountBusiness.getInstance().getLanguageCode())
                .method(original.method(), original.body()).build();

        return chain.proceed(request);
    };

    public static APIService getInstance() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Log.d(TAG, message));
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        httpClient.addInterceptor(logging);  // <-- this is the important line!
        httpClient.addInterceptor(interceptor);  // <-- this is the important line!

        //[210922 - Nod not use X509
//        try {
//            //todo support call api with domain https
//            // Create a trust manager that does not validate certificate chains
//            final TrustManager[] trustAllCerts = new TrustManager[]{
//                    new X509TrustManager() {
//                        @Override
//                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
//                            Log.i(TAG, "checkClientTrusted: " + authType);
//                        }
//
//                        @Override
//                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
//                            Log.i(TAG, "checkServerTrusted: " + authType);
//                        }
//
//                        @Override
//                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                            return new java.security.cert.X509Certificate[]{};
//                        }
//                    }
//            };
//            // Install the all-trusting trust manager
//            final SSLContext sslContext = SSLContext.getInstance("SSL");
//            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
//            // Create an ssl socket factory with our all-trusting manager
//            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
//            httpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
//            httpClient.hostnameVerifier((hostname, session) -> {
//                Log.i(TAG, "hostnameVerifier: " + hostname);
//                return true;
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        //]210922 - Do not use X509

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL_ESPORT)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Common.createGsonISODate()))
                .client(httpClient.build())
                .build();

        return retrofit.create(APIService.class);
    }

    public static String getDomain() {
        String baseUrl = Constant.BASE_URL_ESPORT;
        try {
            URL url = new URL(baseUrl);
            return url.getHost();
        } catch (Exception e) {
        }
        return baseUrl.replace("https://", "").replace("http://", "");
    }

    public static Map<String, Object> getDefaultParam() {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", AccountBusiness.getInstance().getUserId());
        hashMap.put("msisdn", AccountBusiness.getInstance().getPhoneNumber());
        hashMap.put("domain", getDomain());
        hashMap.put("clientType", Constant.CLIENT_TYPE);
        hashMap.put("revision", Constant.REVISION);
        hashMap.put("deviceId", DeviceUtils.getUniqueDeviceId());
        hashMap.put("languageCode", AccountBusiness.getInstance().getLanguageCode());
        return hashMap;
    }

    public static Map<String, Object> getDefaultParamRequestBody() {
        Map<String, Object> hashMap = new HashMap<>();
        RequestBody body;
        body = RequestBody.create(String.valueOf(AccountBusiness.getInstance().getUserId()), okhttp3.MultipartBody.FORM);
        hashMap.put("userId", body);
        body = RequestBody.create(String.valueOf(AccountBusiness.getInstance().getPhoneNumber()), okhttp3.MultipartBody.FORM);
        hashMap.put("msisdn", body);
        body = RequestBody.create(String.valueOf(getDomain()), okhttp3.MultipartBody.FORM);
        hashMap.put("domain", body);
        body = RequestBody.create(Constant.CLIENT_TYPE, okhttp3.MultipartBody.FORM);
        hashMap.put("clientType", body);
        body = RequestBody.create(Constant.REVISION, okhttp3.MultipartBody.FORM);
        hashMap.put("revision", body);
        body = RequestBody.create(String.valueOf(DeviceUtils.getUniqueDeviceId()), okhttp3.MultipartBody.FORM);
        hashMap.put("deviceId", body);
        body = RequestBody.create(String.valueOf(AccountBusiness.getInstance().getLanguageCode()), okhttp3.MultipartBody.FORM);
        hashMap.put("languageCode", body);
        return hashMap;
    }
}

package com.metfone.esport.service;

import com.metfone.esport.base.BaseResponse;
import com.metfone.esport.entity.repsonse.ChannelHomeResponse;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.ChannelInfoWrapperResponse;
import com.metfone.esport.entity.repsonse.CreateChannelResponse;
import com.metfone.esport.entity.repsonse.CreateVideoResponse;
import com.metfone.esport.entity.repsonse.DataCommentResponse;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.entity.repsonse.GameCalendarResponse;
import com.metfone.esport.entity.repsonse.GameInfoEntity;
import com.metfone.esport.entity.repsonse.GameInfoResponse;
import com.metfone.esport.entity.repsonse.GameRecentResultResponse;
import com.metfone.esport.entity.repsonse.HomeResponse;
import com.metfone.esport.entity.repsonse.SearchAllResponse;
import com.metfone.esport.entity.repsonse.TournamentInfoResponse;
import com.metfone.esport.entity.repsonse.UploadImageResponse;
import com.metfone.esport.entity.repsonse.UploadVideoResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;

import java.util.ArrayList;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface APIService {
    @POST("camidApiService/app/getDataHome")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<HomeResponse>>> getDataHome(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getVideoDetail")
    @FormUrlEncoded
    Observable<BaseResponse<DetailVideoResponse>> getVideoDetail(
            @Field("url") String url,
            @FieldMap Map<String, Object> defaultParam
    );

    @GET("camidApiService/app/getVideoDetail")
    Observable<BaseResponse<DetailVideoResponse>> getVideoDetailV2(
            @Query("url") String url,
            @QueryMap Map<String, Object> defaultParam
    );

    @GET("camidApiService/app/getVideoById")
    Observable<BaseResponse<DetailVideoResponse>> getVideoById(
            @Query("videoId") String videoId,
            @QueryMap Map<String, Object> defaultParam
    );

    //SEARCH //////////////////////////////////////////////////////////////////////////////////
    @GET("camidApiService/app/searchAll")
    Observable<BaseResponse<ArrayList<SearchAllResponse>>> searchAll(
            @Query("keySearch") String keySearch,
            @QueryMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/searchVideo")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> searchLiveStream(
            @Query("keySearch") String keySearch,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @GET("camidApiService/app/searchVideoEsport")
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> searchVideoEsport(
            @Query("keySearch") String keySearch,
            @Query("type") int type, //1: uploaded, 2: pastbroadcast, 3: livestream
            @Query("offset") int offset,
            @Query("limit") int limit,
            @QueryMap Map<String, Object> defaultParam
    );

    @GET("camidApiService/app/searchVideoUploaded")
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> searchVideoUploaded(
            @Query("keySearch") String keySearch,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/searchGame")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<GameInfoResponse>>> searchGame(
            @Query("keySearch") String keySearch,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/searchChannel")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<ChannelInfoResponse>>> searchChannel(
            @Query("keySearch") String keySearch,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/searchTournament")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<TournamentInfoResponse>>> searchTournament(
            @Query("keySearch") String keySearch,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getChannelSuggest")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<ChannelInfoResponse>>> searchGetChannelSuggest(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getVideoSuggest")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> searchGetVideoSuggest(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getDataSearch")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<SearchAllResponse>>> searchGetSuggest(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @Field("channelId") long channelId,
            @FieldMap Map<String, Object> defaultParam
    );

    //MYLIST //////////////////////////////////////////////////////////////////////////////////
    @POST("camidApiService/esport/getChannelFollowing")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<ChannelInfoResponse>>> myListChannel(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/esport/actionFollowChannel")
    @FormUrlEncoded
    Observable<BaseResponse<String>> followChannel(
            @Field("channelId") Long channelId,
            @Field("actionType") int actionType,//1:follow, 0:unfollow
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/esport/actionFollowGame")
    @FormUrlEncoded
    Observable<BaseResponse<String>> followGame(
            @Field("gameId") Long gameId,
            @Field("actionType") int actionType,//1:follow, 0:unfollow
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/esport/getListGameFollowing")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<GameInfoResponse>>> myListGame(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getVideoSaved")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> myListVideo(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @Field("typeSort") int typeSort,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/esport/likeAndShare")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> likeVideo(
            @Field("url") String url,
            @Field("actionType") String actionType,//LIKE, UNLIKE,SHARE
            @Field("userId") Long userId
    );

    //GAMES //////////////////////////////////////////////////////////////////////////////////
    @POST("camidApiService/app/getGameDetail")
    @FormUrlEncoded
    Observable<BaseResponse<GameInfoResponse>> getGameDetail(
            @FieldMap Map<String, Object> defaultParam,
            @Query("gameId") Long gameId
    );

    @POST("camidApiService/app/getChannelOfGame")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<ChannelInfoResponse>>> getChannelOfGame(
            @Query("gameId") Long gameId,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getVideoStreamOfGame")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> getVideoStreamOfGame(
            @Query("gameId") Long gameId,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/videoUploadOfGame")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> getVideoOfGame(
            @Query("gameId") Long gameId,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/videoPastBroadcastOfGame")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> getPastBroadcastOfGame(
            @Query("gameId") Long gameId,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getTournamentInfo")
    @FormUrlEncoded
    Observable<BaseResponse<TournamentInfoResponse>> getTournamentInfo(
            @Query("tournamentId") Long tournamentId,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getDataCalenderTourInfo")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<GameCalendarResponse>>> getGameCalendar(
            @Query("gameId") Long gameId,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/getCalender")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<GameRecentResultResponse>>> getGameRecentResult(
            @Query("gameId") Long gameId,
            @FieldMap Map<String, Object> defaultParam
    );

    //CHANNEL //////////////////////////////////////////////////////////////////////////////////
    @POST("camidApiService/app/getChannelInfo")
    @FormUrlEncoded
    Observable<BaseResponse<ChannelInfoWrapperResponse>> getChannelInfo(
            @FieldMap Map<String, Object> defaultParam,
            @Field("channelId") Long channelId
    );

    @POST("camidApiService/app/getDataHomeChannel")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<ChannelHomeResponse>>> getChannelHome(
            @Field("channelId") Long channelId,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @FieldMap Map<String, Object> defaultParam
    );

    @GET("camidApiService/app/getVideoCatGameOfChannel")
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> getVideoCatGameOfChannel(
            @Query("offset") int offset,
            @Query("limit") int limit,
            @Query("channelId") Long channelId,
            @Query("gameId") Long gameId,
            @QueryMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/actionNotify")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> notifyFromChannel(
            @Field("channelId") Long channelId,
            @Field("actionType") int actionType,//1 la bat, 2 la tat
            @Field("userId") Long userId
    );

    ////////////////////////////////////////////////////////////////////////////////////
    @POST("camidApiService/app/feedback")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> feedback(
            @Field("msisdn") String msisdn,
            @Field("videoId") long videoId,
            @Field("cateId") long cateId,
            @Field("channelId") long channelId,
            @Field("desc") String desc,
            @FieldMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/filterPastBroadcast")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> getPastBroadcastOfMyChannel(
            //filterType:Int_rq (order 1:mới nhât, 2: cũ nhất, 3: nhiều view nhất, 4: ít view nhất)
            @FieldMap Map<String, Object> defaultParam,
            @Field("channelId") long channelId,
            @Field("filterType") int filterType,
            @Field("offset") int offset,
            @Field("limit") int limit);

    @POST("camidApiService/app/filterUploaded")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> getVideoUploadOfMyChannel(
            //filterType:Int_rq (order 1:mới nhât, 2: cũ nhất, 3: nhiều view nhất, 4: ít view nhất)
            @FieldMap Map<String, Object> defaultParam,
            @Field("channelId") long channelId,
            @Field("filterType") int filterType,
            @Field("offset") int offset,
            @Field("limit") int limit);

    @POST("camidApiService/app/getChannelByUser")
    @FormUrlEncoded
    Observable<BaseResponse<ArrayList<ChannelInfoResponse>>> getMyChannels(
            @Field("msisdn") String msisdn,
            @FieldMap Map<String, Object> defaultParam);

    @GET("camidApiService/esport/getComments")
    Observable<BaseResponse<DataCommentResponse>> getComments(
            @Query("rowStart") String rowStart,
            @Query("num") Integer num,
            @Query("url") String url,
            @QueryMap Map<String, Object> defaultParam);

    @POST("camidApiService/app/pushAction")
    @FormUrlEncoded
    Observable<BaseResponse<DataCommentResponse>> pushAction(
            @Field("data") String data,
            @Field("secinf") String secinf,
            @Field("userId") String userId,
            @Field("security") String security,
            @Field("timestamp") Long timestamp);

    @POST("camidApiService/app/reportVideo")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> reportVideo(
            @Field("msisdn") String msisdn,
            @Field("name") String name,
            @Field("domain") String domain,
            @Field("networkType") String networkType,
            @Field("videoId") String videoId,
            @Field("videoName") String videoName,
            @Field("videoLink") String videoLink,
            @Field("videoPath") String videoPath,
            @Field("reportId") String reportId,
            @Field("clientType") String clientType,
            @Field("revision") String revision,
            @Field("reason") String reason
    );

    @Multipart
    @POST("camidApiService/app/createChannel")
    Observable<CreateChannelResponse> createChannelFull(
            @Query("msisdn") String msisdn,
            @Query("channelName") String channelName,
            @Query("channelDesc") String channelDesc,
            @Query("aboutTitle") String aboutTitle,
            @Query("about") String about,
            @Part MultipartBody.Part avatar,
            @Part MultipartBody.Part banner,
            @Part MultipartBody.Part aboutImage,
            @QueryMap Map<String, Object> defaultParam
    );


    @POST("camidApiService/app/createChannel")
    Observable<CreateChannelResponse> createChannelEmptyImage(
            @Query("msisdn") String msisdn,
            @Query("channelName") String channelName,
            @Query("channelDesc") String channelDesc,
            @Query("aboutTitle") String aboutTitle,
            @Query("about") String about,
            @QueryMap Map<String, Object> defaultParam
    );


    @Multipart
    @POST("camidApiService/app/createChannel")
    Observable<CreateChannelResponse> createChannelAlone(
            @Query("msisdn") String msisdn,
            @Query("channelName") String channelName,
            @Query("channelDesc") String channelDesc,
            @Query("aboutTitle") String aboutTitle,
            @Query("about") String about,
            @Part MultipartBody.Part avatar,
            @QueryMap Map<String, Object> defaultParam
    );

    @Multipart
    @POST("camidApiService/app/createChannel")
    Observable<CreateChannelResponse> createChannel2Image(
            @Query("msisdn") String msisdn,
            @Query("channelName") String channelName,
            @Query("channelDesc") String channelDesc,
            @Query("aboutTitle") String aboutTitle,
            @Query("about") String about,
            @Part MultipartBody.Part avatar,
            @Part MultipartBody.Part banner,
            @QueryMap Map<String, Object> defaultParam
    );



    @GET("camidApiService/app/getGameOfChannel")
    Observable<BaseResponse<ArrayList<GameInfoEntity>>> getGameOfChannel(
            @Query("channelId") long channelId,
            @Query("msisdn") String msisdn,
            @QueryMap Map<String, Object> defaultParam
    );

    @Multipart
    @POST("camidApiService/app/uploadVideo")
    Observable<BaseResponse<UploadVideoResponse>> uploadVideo(
            @Part("msisdn") RequestBody msisdn,
            @Part MultipartBody.Part file,
            @QueryMap Map<String, Object> defaultParam
    );

    @GET("camidApiService/app/getVideoRelated")
    Observable<BaseResponse<ArrayList<VideoInfoResponse>>> getVideoRelated(
            @Query("q") String q,
            @Query("videoId") Long videoId,
            @Query("channelId") String channelId,
            @Query("gameId") String gameId,
            @QueryMap Map<String, Object> defaultParam
    );

    @Multipart
    @POST("camidApiService/app/uploadImage")
    Observable<UploadImageResponse> upLoadImage(
            @Part("uploadFile") MultipartBody.Part uploadFile,
            @Part("fileName") RequestBody fileName
    );

    @POST("camidApiService/esport/actionApp")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> actionApp(
            @Field("userId") String userId,
            @Field("content") String content,
            @Field("url") String url,
            @Field("postActionFrom") String postActionFrom,
            @Field("status") String status,
            @Field("rowId") int rowId,
            @Field("tags") String tags,
            @Field("urlSubComment") String urlSubComment,
            @Field("isPrivate") int isPrivate,
            @Field("userInfo") String userInfo,
            @Field("channelId") String channelId,
            @Field("channelInfo") String channelInfo

    );

    @Multipart
    @POST("camidApiService/app/createChannel")
    Observable<CreateChannelResponse> editChannel(
            @Query("msisdn") String msisdn,
            @Query("channelId") String channelId,
            @Query("channelName") String channelName,
            @Query("channelDesc") String channelDesc,
            @Query("aboutTitle") String aboutTitle,
            @Query("avatarChannel") String avatarChannel,
            @Query("bannerChannel") String bannerChannel,
            @Query("aboutImg") String aboutImg,
            @Query("about") String about,
            @Query("createdDate") String createdDate,
            @Part("imagelead") RequestBody imageLead,
            @Part MultipartBody.Part avatar,
            @Part MultipartBody.Part banner,
            @Part MultipartBody.Part aboutImage,
            @QueryMap Map<String, Object> defaultParam
    );

    @Multipart
    @POST("camidApiService/app/createVideoUploadByUser")
    Observable<CreateVideoResponse> createVideoUploadByUser(
            @Query("channelId") long channelId,
            @Query("videoId") long videoId,
            @Query("categoryId") long categoryId,
            @Query("videoTitle") String videoTitle,
            @Query("videoDesc") String videoDesc,
            @Query("itemvideo") String itemVideo,
            @Query("schedule") long schedule,
            @Query("imagelead") String imagelead,
            @Part("imagelead") RequestBody imageLead,
            @Part MultipartBody.Part thumb,
            @QueryMap Map<String, Object> defaultParam
    );

    @Multipart
    @POST("camidApiService/app/createVideoUploadByUser")
    Observable<CreateVideoResponse> createVideoUploadByUser(
            @Part("channelId") RequestBody channelId,
            @Part("videoId") RequestBody videoId,
            @Part("categoryId") RequestBody categoryId,
            @Part("videoTitle") RequestBody videoTitle,
            @Part("videoDesc") RequestBody videoDesc,
            @Part("itemvideo") RequestBody itemVideo,
            @Part("schedule") RequestBody schedule,
            @Part("imagelead") RequestBody imageLead,
            @Part("createdDate") RequestBody createdDate,
            @Part MultipartBody.Part thumb,
            @PartMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/app/deleteVideo")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> deleteVideo(
            @Field("channelId") String channelId,
            @Field("gameId") String gameId,
            @Query("videoId") Long videoId,
            @Query("timestamp") Long timestamp,
            @QueryMap Map<String, Object> defaultParam
    );

    @POST("camidApiService/esportauth/regDevice")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> regDevice(
            @Query("pushId") String pushId,
            @Query("model") String model,
            @Query("osversion") String osVersion,
            @Query("lastseen") long lastSeen,
            @FieldMap Map<String, Object> defaultParam
    );

    @Multipart
    @POST("camidApiService/app/createVideoUploadByUser")
    Observable<CreateVideoResponse> editVideoUploadByUser(

            @Part("channelId") RequestBody channelId,
            @Part("videoId") RequestBody videoId,
            @Part("categoryId") RequestBody categoryId,
            @Part("videoTitle") RequestBody videoTitle,
            @Part("videoDesc") RequestBody videoDesc,
            @Part("itemvideo") RequestBody itemVideo,
            @Part("schedule") RequestBody schedule,
            @Part("createDate") RequestBody createDate,
            @Part MultipartBody.Part thumb,
            @PartMap Map<String, Object> defaultParam
    );


    @POST("camidApiService/log/logView")
    @FormUrlEncoded
    Observable<BaseResponse<Object>> logView(
            @Field("userId") String userId,
            @Field("domain") String domain,
            @Field("dateLog") long dateLog,
            @Field("cdr") String cdr,
            @Field("ipAddress") String ipAddress,
            @Field("videoId") String videoId,
            @Field("mediaLink") String mediaLink,
            @Field("timePlay") long timePlay,
            @Field("positionPlay") String positionPlay,
            @Field("revision") String revision,
            @Field("clientType") String clientType,
            @Field("isVideoLive") String isVideoLive,
            @Field("gameId") String gameId,
            @Field("networkType") String networkType
    );
}

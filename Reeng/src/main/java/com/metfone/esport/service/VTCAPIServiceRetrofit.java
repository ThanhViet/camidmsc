package com.metfone.esport.service;

import com.blankj.utilcode.util.StringUtils;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VTCAPIServiceRetrofit {
    private static final String TAG = VTCAPIServiceRetrofit.class.getCanonicalName();
    private static Interceptor interceptor = chain -> {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder()
                .addHeader("languageCode", AccountBusiness.getInstance().getLanguageCode())
                .addHeader("Content-Type", "application/json")
                .method(original.method(), original.body());

        if (!StringUtils.isTrimEmpty(AccountBusiness.getInstance().getToken())) {
            requestBuilder.addHeader("Authorization", "Bearer " + AccountBusiness.getInstance().getToken());

        }

        return chain.proceed(requestBuilder.build());
    };

    public static VTCAPIService getInstance() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Log.d(TAG, message));
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        httpClient.addInterceptor(logging);  // <-- this is the important line!
        httpClient.addInterceptor(interceptor);  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL_VTC_API)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Common.createGsonISODate()))
                .client(httpClient.build())
                .build();

        return retrofit.create(VTCAPIService.class);
    }

}

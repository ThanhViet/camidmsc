package com.metfone.esport.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import butterknife.ButterKnife;

/**
 * Created by Nguyễn Thành Chung  on 9/2/20
 */
public abstract class BasePopupWindow extends PopupWindow {

    public LayoutInflater inflater;
    public Context context;
    public View rootView;

    protected BasePopupWindow(Context context) {
        super(context);

        this.context = context;

        inflater = LayoutInflater.from(context);

        rootView = inflater.inflate(getLayout(), null, false);

        setContentView(rootView);

        ButterKnife.bind(this, rootView);

        setOutsideTouchable(true);

        setFocusable(true);

        setBackgroundDrawable(null);

        onCreate();
    }

    protected abstract void onCreate();

    protected void onViewCreated() {

    }

    protected abstract int getLayout();

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        onViewCreated();

        super.showAtLocation(parent, gravity, x, y);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff, int gravity) {
        onViewCreated();

        super.showAsDropDown(anchor, xoff, yoff, gravity);
    }
}

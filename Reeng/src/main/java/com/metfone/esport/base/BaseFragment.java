package com.metfone.esport.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.metfone.esport.common.Common;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.homes.HomeFragment;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Base fragment
 */
public abstract class BaseFragment<P> extends Fragment {

    // Khai báo interface prsenter.
    protected String TAG = BaseFragment.class.getCanonicalName();
    protected P presenter;
    protected CompositeDisposable compositeDisposable;
    private Unbinder unbinder;
    private WeakReference<BaseSlidingFragmentActivity> refActivity;

    public abstract P getPresenter();

    public abstract CompositeDisposable initCompositeDisposable();

    @Override
    public void onDestroy() {
        if (presenter != null && ((BasePresenter) presenter).isUseEventPost()) {
            EventBus.getDefault().unregister(presenter);
        }
        super.onDestroy();
    }

    protected void replaceFragment(BaseFragment baseFragment, String tag) {
        try {
            if (getActivity() instanceof BaseSlidingFragmentActivity) {
                ((BaseSlidingFragmentActivity) getActivity()).replaceFragment(baseFragment, tag);
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }



    /**
     * hàm chuyển fragment
     * created by ntchung1 on 12/02/2020
     *
     * @param fragment fragment
     */
    protected void addFragment(BaseFragment fragment, boolean isAddToBackStack, String TAG) {
        try {
            if (getActivity() instanceof BaseSlidingFragmentActivity) {
                ((BaseSlidingFragmentActivity) getActivity()).addFragment(fragment, isAddToBackStack, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void popFragment() {
        try {
            if (getActivity() instanceof BaseSlidingFragmentActivity) {

                if (getFragmentManager() != null && getFragmentManager().getBackStackEntryCount() == 0) {
                    getActivity().finish();
                    return;
                }
                ((BaseSlidingFragmentActivity) getActivity()).popFragment();
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    protected abstract int getLayoutId();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(getLayoutId(), container, false);
        unbinder = ButterKnife.bind(this, view);
        compositeDisposable = initCompositeDisposable();
        presenter = getPresenter();
        if (presenter != null) {
            ((BasePresenter) presenter).setContext(getContext());
//            if (((BasePresenter) presenter).isUseEventPost())
//                EventBus.getDefault().register(presenter);
        }
        Utilities.setSystemUiVisibilityHideNavigation(getActivity(), R.color.black);
        return view;
    }

    public void showToast(String message) {
        if (getParentActivity() != null) getParentActivity().showToast(message);
    }

    public void showToast(int resMsg) {
        if (getParentActivity() != null) getParentActivity().showToast(resMsg);
    }

    @Override
    public void onDestroyView() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
        unbinder.unbind();
        super.onDestroyView();
    }

    public void showDialog() {
        ((BaseActivity) getActivity()).showDialog();
    }

    public void hideDialog() {
        ((BaseActivity) getActivity()).hideDialog();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        setStatusBarColor(getActivity());
    }

    protected void setStatusBarColor(FragmentActivity activity) {
        if (activity != null) {
            if (this instanceof HomeFragment) {
                Common.setColorStatusBar(activity, ContextCompat.getColor(activity, R.color.colorAccent_es));
            } else {
                Common.setColorStatusBar(activity, ContextCompat.getColor(activity, R.color.colorPrimaryDark_es));
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof BaseSlidingFragmentActivity) {
            refActivity = new WeakReference<>((BaseSlidingFragmentActivity) context);
        }
    }

    public BaseSlidingFragmentActivity getParentActivity() {
        if (refActivity != null) return refActivity.get();
        return null;
    }

    public void onClickBack() {
        if (getParentActivity() != null) getParentActivity().onBackPressed();
        else if (getActivity() != null) getActivity().finish();
    }

    public void onClickShare(Object object) {
        if (getParentActivity() == null || object == null) return;
        if (isLogin()) {
            ShareContentBusiness shareBusiness = new ShareContentBusiness(getParentActivity(), object);
            shareBusiness.showPopupShareContent();
        } else {
            showDialogLogin();
        }
    }

    public void showDialogLogin() {
        if (getParentActivity() == null) return;
        DialogMessage dialogMessage = new DialogMessage(getParentActivity(), true);
        dialogMessage.setMessage(getResources().getString(R.string.content_popup_login));
        dialogMessage.setNegativeListener(result -> NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true));
        dialogMessage.show();
    }

    public boolean isLogin() {
        return AccountBusiness.getInstance().isLogin();
    }

}

package com.metfone.esport.base;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.blankj.utilcode.util.CollectionUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerBaseAdapter extends FragmentStatePagerAdapter {
    private List<BaseFragment<?>> fragments = new ArrayList<>();

    private List<String> titles = new ArrayList<>();

    public ViewPagerBaseAdapter(FragmentManager fragmentManager, List<? extends BaseFragment<?>> listFragment, List<String> titles) {
        super(fragmentManager);

        if (CollectionUtils.isNotEmpty(listFragment))
            fragments.addAll(listFragment);

        if (CollectionUtils.isNotEmpty(titles))
            this.titles.addAll(titles);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return fragments.size();
    }

    // Returns the fragment to display for that page
    @NotNull
    @Override
    public Fragment getItem(int position) {

        if (position < fragments.size()) {
            return fragments.get(position);
        }

        return new Fragment();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        if (position < titles.size()) {
            return titles.get(position);
        }

        return super.getPageTitle(position);
    }
}

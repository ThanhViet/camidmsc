package com.metfone.esport.base;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable {

    //    @SerializedName("code") public boolean Success;
    @SerializedName("code")
    public Integer code;
    @SerializedName("desc")
    public String desc;
    @SerializedName("data")
    public T data;

    public boolean isSuccess(){
        return code == 200;
    }
}

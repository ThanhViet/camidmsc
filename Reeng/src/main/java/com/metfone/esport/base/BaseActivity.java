package com.metfone.esport.base;

import android.graphics.Color;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Navigator;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.ui.dialog.DialogMessage;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Nguyễn Thành Chung  on 8/20/20
 *
 * @param <P>
 */
public abstract class BaseActivity<P> extends BaseSlidingFragmentActivity {
    protected final String TAG = BaseActivity.class.getCanonicalName();
    // Khai báo interface prsenter.
    protected P presenter;
    //    protected Navigator mNavigator;
    protected CompositeDisposable compositeDisposable;
    private ACProgressFlower dialog;
    private Unbinder unbinder;

    public abstract P getPresenter();

    public abstract CompositeDisposable initCompositeDisposable();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
//        setStatusBarColor();
        if (mNavigator == null)
            mNavigator = new Navigator(this);
        compositeDisposable = initCompositeDisposable();
        presenter = getPresenter();
        if (presenter != null) {
            ((BasePresenter) presenter).setContext(this);
            if (((BasePresenter) presenter).isUseEventPost())
                EventBus.getDefault().register(presenter);
        }
    }

    protected abstract int getLayoutId();

    @Override
    protected void onDestroy() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }

        if (presenter != null && ((BasePresenter) presenter).isUseEventPost()) {
            EventBus.getDefault().unregister(presenter);
        }
        unbinder.unbind();
        super.onDestroy();
    }

    /**
     * Show toast loi
     * Created_by @dchieu on 2020-02-25
     *
     * @param message
     */
    public void showToastError(String message) {
        if (StringUtils.isTrimEmpty(message)) return;

        //new CustomToast(this, message, Toast.LENGTH_SHORT, CustomToast.ToastType.ERROR).show();
        ToastUtils.showShort(message);
    }

    public void showDialog() {
        dialog = new ACProgressFlower.Builder(this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text(getString(R.string.please_waiting))
                .fadeColor(Color.DKGRAY).build();
        dialog.show();
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void showToast(String message) {
        if (StringUtils.isTrimEmpty(message)) return;

        //new CustomToast(this, message, Toast.LENGTH_SHORT, CustomToast.ToastType.NORMAL).show();
        ToastUtils.showShort(message);
    }

    public void showToast(int resMsg) {
        if (resMsg <= 0) return;

        //new CustomToast(this, message, Toast.LENGTH_SHORT, CustomToast.ToastType.NORMAL).show();
        ToastUtils.showShort(getString(resMsg));
    }

    protected void setStatusBarColor() {
        Common.setColorStatusBar(this, ContextCompat.getColor(this, R.color.colorPrimaryDark_es));
    }

    public boolean isLogin() {
        return AccountBusiness.getInstance().isLogin();
    }

    public void onClickShare(Object object) {
        if (object == null) return;
        if (isLogin()) {
            ShareContentBusiness shareBusiness = new ShareContentBusiness(this, object);
            shareBusiness.showPopupShareContent();
        } else {
            showDialogLogin();
        }
    }

    @Override
    public void showDialogLogin() {
        DialogMessage dialogMessage = new DialogMessage(this, true);
        dialogMessage.setMessage(getResources().getString(R.string.content_popup_login));
        dialogMessage.setNegativeListener(result -> NavigateActivityHelper.navigateToRegisterScreenActivity(this, true));
        dialogMessage.show();
    }
}

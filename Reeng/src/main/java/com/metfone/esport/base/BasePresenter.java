package com.metfone.esport.base;

import android.content.Context;

import com.blankj.utilcode.util.DeviceUtils;
import com.metfone.esport.common.Constant;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.service.APIService;
import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<V, M extends com.metfone.esport.base.BaseModel> {

    protected final String TAG = BasePresenter.class.getCanonicalName();

    public static final int DEFAULT_LIMIT = 20;

    protected Context context;
    protected V view;
    protected M model;
    protected CompositeDisposable compositeDisposable;
    protected APIService apiService;

    public BasePresenter(V view, CompositeDisposable disposable) {
        model = getModel();
        compositeDisposable = disposable;
        if (model != null)
            model.setContext(context);
        this.view = view;

        apiService = ServiceRetrofit.getInstance();
    }

    public abstract M getModel();

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public void setContext(Context context) {
        this.context = context;
        if (model != null)
            model.setContext(context);
    }

    public boolean isUseEventPost() {
        return false;
    }

    public String getDomain() {
        String baseUrl = Constant.BASE_URL_ESPORT;
        try {
            URL url = new URL(baseUrl);
            return url.getHost();
        } catch (Exception e) {
        }
        return baseUrl.replace("https://", "").replace("http://", "");
    }

    public Map<String, Object> getDefaultParam() {
        Map<String, Object> hashMap = new HashMap<>();
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(ApplicationController.getApp());
        hashMap.put("userId", String.valueOf(userInfoBusiness.getUser().getUser_id()));
        hashMap.put("domain", getDomain());
        hashMap.put("clientType", Constant.CLIENT_TYPE);
        hashMap.put("revision", Constant.REVISION);
        hashMap.put("deviceId", DeviceUtils.getUniqueDeviceId());
        hashMap.put("languageCode", AccountBusiness.getInstance().getLanguageCode());
        return hashMap;
    }
}
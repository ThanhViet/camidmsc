package com.metfone.esport.base;

public class ExtKeyPair<K, V> {
    public K key;
    public V value;
    public int iconId = 0;
    public boolean isSelected = false;

    public ExtKeyPair(K key) {
        this.key = key;
    }

    public ExtKeyPair(K key, V value) {
        this(key);
        this.value = value;
    }

    public ExtKeyPair(K key, V value, int iconId) {
        this(key, value);
        this.iconId = iconId;
    }

    public ExtKeyPair(K key, V value, int iconId, boolean isSelected) {
        this(key, value, iconId);
        this.isSelected = isSelected;
    }

}

package com.metfone.esport.base;

/**
 * Created by Nguyễn Thành Chung on 8/22/20.
 */
public interface ICallBackResponse<R> {
    void onSuccess(R response);

    void onFail(String error);

    default void onStart() {
    }

    default void onFinish() {
    }

    default void onError(Throwable e) {
    }
}

package com.metfone.esport.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseListAdapter<Object, Holder extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<Holder> implements Filterable {
    public Context context;
    public LayoutInflater mInflater;

    protected List<Object> mData = new ArrayList<>();

    public BaseListAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @NotNull
    @Override
    public abstract Holder onCreateViewHolder(@NotNull ViewGroup parent, int viewType);

    @Override
    public abstract void onBindViewHolder(@NotNull Holder holder, int position);

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * Lấy vị trí của item trong list
     * created_by nvnam on 14/11/2018
     *
     * @param data   data của list
     * @param entity data của item cần lấy vị trí
     * @return trả về vị trí của item nếu có hoặc -1 nếu không có
     */
    private int getPosition(List<Object> data, Object entity) {
        if (data != null) {
            for (int i = 0; i < data.size(); ++i) {
                Object newEntity = data.get(i);
                if (entity.equals(newEntity)) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Lấy data của 1 item trong list
     * created_by nvnam on 14/11/2018
     *
     * @param position vị trí cần lấy ra trong adapter
     * @return
     */
    public Object getItem(int position) {
        return mData.get(position);
    }

    /**
     * thêm item vào 1 vị trí trong list
     * created_by nvnam on 14/11/2018
     *
     * @param i      vị trí thêm
     * @param entity data thêm
     */
    public void add(int i, Object entity) {
        mData.add(i, entity);
        notifyItemInserted(i);
    }

    /**
     * Thêm data vào list
     * created_by nvnam on 14/11/2018
     *
     * @param entity data thêm
     */
    public void add(Object entity) {
        mData.add(entity);
    }

    /**
     * Xóa data trong list
     * created_by nvnam on 14/11/2018
     *
     * @param i vị trí xóa
     */
    public void delete(int i) {
        if (i < 0 || i >= getItemCount()) return;

        mData.remove(i);
        notifyItemRemoved(i);
        // notifyItemRangeChanged(i, getItemCount());
    }

    /**
     * Xóa data trong list
     * created_by nvnam on 14/11/2018
     *
     * @param o vị trí xóa
     */
    public void delete(Object o) {
        int index = mData.indexOf(o);

        if (index >= 0) delete(index);
        // notifyItemRangeChanged(i, getItemCount());
    }

    /**
     * Chuyển vị trí item trong list
     * created_by nvnam on 14/11/2018
     *
     * @param i   vị trí ban đầu
     * @param loc vị trí chuyển đến
     */
    public void moveEntity(int i, int loc) {
        move(mData, i, loc);
        notifyItemMoved(i, loc);
    }

    private void move(List<Object> data, int a, int b) {
        Object temp = data.remove(a);
        data.add(b, temp);
    }

    public void clear() {
        if (mData != null) {
            mData.clear();

            notifyDataSetChanged();
        }
    }

    public List<Object> getData() {
        if (mData != null) {
            return mData;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * set data cho list
     *
     * @param data data set vào list
     */
    public void setData(final List<Object> data) {
        if (data == null) return;
        // Remove all deleted data.
        for (int i = mData.size() - 1; i >= 0; --i) {
            if (getPosition(data, mData.get(i)) < 0) {
                delete(i);
            }
        }
        // Add and move data.
        for (int i = 0; i < data.size(); ++i) {
            Object entity = data.get(i);
            int loc = getPosition(mData, entity);
            if (loc < 0) {
                add(i, entity);
            } else if (loc != i) {
                moveEntity(i, loc);
            }
        }
    }

    /**
     * set data cho list
     *
     * @param data data set vào list
     */
    public void setData(final ArrayList<Object> data) {
        if (data == null) return;
        // Remove all deleted data.
        for (int i = mData.size() - 1; i >= 0; --i) {
            if (getPosition(data, mData.get(i)) < 0) {
                delete(i);
            }
        }
        // Add and move data.
        for (int i = 0; i < data.size(); ++i) {
            Object entity = data.get(i);
            int loc = getPosition(mData, entity);
            if (loc < 0) {
                add(i, entity);
            } else if (loc != i) {
                moveEntity(i, loc);
            }
        }
    }

    public void addAll(List<Object> data) {
        if (data != null) {
            int size = getItemCount();
            mData.addAll(data);
            notifyItemRangeInserted(size, data.size());
        }
    }

    public void setNewData(List<Object> data) {
        mData.clear();
        if (data != null) mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setNewData(ArrayList<Object> data) {
        mData.clear();
        if (data != null) mData.addAll(data);
        notifyDataSetChanged();
    }
}

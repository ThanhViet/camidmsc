package com.metfone.esport.base;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;

/**
 * Created by Nguyễn Thành Chung  on 8/20/20
 */
public abstract class BaseViewHolder<Object> extends RecyclerView.ViewHolder {

    protected Context context;

    public BaseViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }

    public abstract void binData(Object entity, int position);
}

package com.metfone.esport.base;

import android.content.Context;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;

/**
 * Created by Nguyễn Thành Chung on 8/20/20
 */
public class BaseModel {

    protected Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public void showToastError(String message) {
        if (StringUtils.isTrimEmpty(message)) return;
        //new CustomToast(context, message, Toast.LENGTH_SHORT, CustomToast.ToastType.ERROR).show();
        ToastUtils.showShort(message);
    }

//    public <P> void async(BasePresenter presenter, Observable<BaseDataResponse<P>> observable, ICallBackResponse<P> iCallBackResponse) {
//        try {
//            if (Common.checkNetworkWithToast(context)) {
//                iCallBackResponse.onFinish();
//                return;
//            }
//            iCallBackResponse.onStart();
//
//            Disposable disposable = observable
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeWith(new DisposableObserver<BaseDataResponse<P>>() {
//
//                        @Override
//                        public void onNext(BaseDataResponse<P> response) {
//                            if (response.isSuccess()) {
//                                iCallBackResponse.onSuccess(response.Data);
//                            } else {
//                                iCallBackResponse.onFail(context.getString(R.string.service_error));
//                            }
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                            iCallBackResponse.onFail(context.getString(R.string.service_error));
//                            iCallBackResponse.onFinish();
//                        }
//
//                        @Override
//                        public void onComplete() {
//                            iCallBackResponse.onFinish();
//                        }
//                    });
//            presenter.compositeDisposable.add(disposable);
//
//        } catch (Exception e) {
//            Common.handleException(e);
//        }
//    }

}

package com.metfone.esport.base;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.metfone.esport.common.Common.handleException;

public abstract class BaseDialogFragment extends DialogFragment{

    protected String TAG;
    private Unbinder unbinder;
    protected DialogInterface.OnDismissListener onDismissListener;

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getTAG();

    }

    @Override
    @Nullable
    public View onCreateView(@NotNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        View rootView = requireActivity().getLayoutInflater().inflate(getLayout(), container, false);
        try {
            unbinder = ButterKnife.bind(this,rootView);
            initView(rootView);
        } catch (Exception e) {
            handleException(e);
        }
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (getDialog() != null && getDialog().getWindow() != null) {
                getDialog().getWindow().setLayout(getDialogWidth(), LayoutParams.WRAP_CONTENT);
            }
        } catch (Exception e) {
            handleException(e);
        }
        setCancelable(true);
    }


    protected abstract void initView(View rootView);


    protected abstract int getDialogWidth();




    protected abstract int getLayout();


    public abstract String getTAG();

    public void show(FragmentManager manager) {
        super.show(manager, getTAG());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        if (onDismissListener != null) onDismissListener.onDismiss(dialog);
        super.onDismiss(dialog);
    }
}

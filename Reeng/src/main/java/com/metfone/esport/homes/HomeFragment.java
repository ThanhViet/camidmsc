package com.metfone.esport.homes;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.ViewUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.drakeet.multitype.MultiTypeAdapter;
import com.esport.widgets.MultipleStatusView;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.event.ChangeAccountEvent;
import com.metfone.esport.entity.event.CreateChannelEvent;
import com.metfone.esport.entity.event.OnChangeStateVideoTinyEvent;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.HomeResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.homes.adapters.ExtSaveStateMultiTypeAdapter;
import com.metfone.esport.homes.adapters.decorations.VerticalTopContentDecoration;
import com.metfone.esport.homes.adapters.games.HomeGameItemViewBinder;
import com.metfone.esport.homes.adapters.games.ObjectHomeGame;
import com.metfone.esport.homes.adapters.metfonrtouraments.HomeMetfoneItemViewBinder;
import com.metfone.esport.homes.adapters.metfonrtouraments.ObjectHomeMetfone;
import com.metfone.esport.homes.adapters.typelivechats.HomeLiveChatItemViewBinder;
import com.metfone.esport.homes.adapters.typelivechats.ObjectHomeLiveChat;
import com.metfone.esport.homes.adapters.typepopularlives.HomePopularLiveItemViewBinder;
import com.metfone.esport.homes.adapters.typepopularlives.ObjectHomePopularLive;
import com.metfone.esport.homes.adapters.typepopularuploaded.HomePopularUploadedItemViewBinder;
import com.metfone.esport.homes.adapters.typepopularuploaded.ObjectHomePopularUploaded;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.ui.account.AccountFragment;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.createchannel.CreateChannelFragment;
import com.metfone.esport.ui.games.GamesContainerFragment;
import com.metfone.esport.ui.mylists.MyListContainerFragment;
import com.metfone.esport.ui.searchs.SearchFragment;
import com.metfone.esport.ui.uploadVideo.choose.ChooseVideoActivity;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.esport.util.AutoPlayHorizontalUtils;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.util.ImageUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

public class HomeFragment extends BaseFragment<HomePresenter> {

    public static String lastUpdate = "24/12/20 23:49";

    @BindView(R.id.scroll_view)
    NestedScrollView scrollView;
    @BindView(R.id.recycler_view)
    RecyclerView rvHome;
    //    @BindView(R.id.tvAccountName)
//    AppCompatTextView tvAccountName;
//    @BindView(R.id.avAvatarHome)
//    AvatarView avAvatarHome;
//    @BindView(R.id.lnHomeLogin)
//    LinearLayout lnHomeLogin;
//    @BindView(R.id.clHomeAccount)
//    ConstraintLayout clHomeAccount;
    @BindView(R.id.tvAccountDescription)
    AppCompatTextView tvAccountDescription;
    //    @BindView(R.id.lnAccountDiamond)
//    LinearLayout lnAccountDiamond;
//    @BindView(R.id.ivAccountRank)
//    AppCompatImageView ivAccountRank;
//    @BindView(R.id.tvAccountRank)
//    AppCompatTextView tvAccountRank;
    @BindView(R.id.multi_status_view)
    MultipleStatusView multipleStatusView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
//    @BindView(R.id.ivVideoCamHome)
//    AppCompatImageView ivVideoCamHome;

    @BindView(R.id.layout_search)
    View layoutSearch;
    @BindView(R.id.btn_like)
    AppCompatImageView btnLike;
    @BindView(R.id.btn_camera)
    AppCompatImageView btnCamera;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.layout_guest)
    LinearLayout layoutGuest;
    @BindView(R.id.layout_user)
    LinearLayout layoutUser;
    @BindView(R.id.iv_avatar)
    AppCompatImageView ivAvatar;
    @BindView(R.id.iv_grade)
    AppCompatImageView ivCrown;
    @BindView(R.id.tvName)
    AppCompatTextView tvName;
    @BindView(R.id.tvRankName)
    AppCompatTextView tvRankName;
    @BindView(R.id.tvLastUpdate)
    AppCompatTextView tvLastUpdate;

    private MultiTypeAdapter adapter = new ExtSaveStateMultiTypeAdapter();
    private ArrayList<Object> items = new ArrayList<>();
    private HomeLiveChatItemViewBinder homeLiveChatItemViewBinder;
    private int currentPage = 0;
    private Runnable runnableResumeAutoPlaying = this::resumeAutoPlay;
    private Runnable runnablePauseAutoPlaying = () -> pauseAutoPlay(false);
    private KhHomeClient khHomeClient;

    private KhHomeClient getKhHomeClient(){
        if(khHomeClient == null){
            khHomeClient = new KhHomeClient();
        }
        return khHomeClient;
    }

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public HomePresenter getPresenter() {
        return new HomePresenter(this, compositeDisposable);
    }

    @Override
    public CompositeDisposable initCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvLastUpdate.setText(lastUpdate);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        presenter.getMyChannel();
        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.postDelayed(() -> {
                if (swipeRefresh != null) swipeRefresh.setRefreshing(false);
            }, 100);
            presenter.getMyChannel();
            refreshData();
        });
        initRecyclerView();
        btnCamera.setOnClickListener(new OnSingleClickListener() {
            @SuppressLint("CheckResult")
            @Override
            public void onSingleClick(View view) {
                logActiveEsport();
                if (isLogin()) {
                    if (SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) { // check trang thai la tai khoan channel
                        PermissionUtils.permission(PermissionConstants.UPLOAD_VIDEO)
                                .rationale((activity, shouldRequest) -> {
                                    shouldRequest.again(true);
                                })
                                .callback(new PermissionUtils.SimpleCallback() {
                                    @Override
                                    public void onGranted() {
                                        ChannelInfoResponse channel = AccountBusiness.getInstance().getChannelInfo();
                                        if (channel != null) {
                                            Intent intent = new Intent(getParentActivity(), ChooseVideoActivity.class);
                                            startActivity(intent);
                                        } else {
                                            AloneFragmentActivity.with(getContext()).start(CreateChannelFragment.class);
                                        }
                                    }

                                    @Override
                                    public void onDenied() {
                                        ToastUtils.showShort(R.string.msg_storage_permission);
                                    }
                                }).request();
                    } else { // la tai khoan camId
                        if (AccountBusiness.getInstance().getMyChannels() == null || CollectionUtils.isEmpty(AccountBusiness.getInstance().getMyChannels())) {
                            AloneFragmentActivity.with(getContext()).start(CreateChannelFragment.class);
                        } else {
                            AloneFragmentActivity.with(getContext()).start(AccountFragment.class);
                        }

                    }
                } else {
                    NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true);
                }
            }
        });
        btnLike.setOnClickListener(new OnSingleClickListener() {
            @SuppressLint("CheckResult")
            @Override
            public void onSingleClick(View view) {
                logActiveEsport();
                if (isLogin()) {
                    AloneFragmentActivity.with(getContext()).start(MyListContainerFragment.class);
                } else {
                    NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true);
                }
            }
        });
        layoutSearch.setOnClickListener(new OnSingleClickListener() {
            @SuppressLint("CheckResult")
            @Override
            public void onSingleClick(View view) {
                logActiveEsport();
                AloneFragmentActivity.with(getContext()).start(SearchFragment.class);
            }
        });
        layoutUser.setOnClickListener(new OnSingleClickListener() {
            @SuppressLint("CheckResult")
            @Override
            public void onSingleClick(View view) {
                logActiveEsport();
                if (isLogin()) {
                    if (CollectionUtils.isNotEmpty(AccountBusiness.getInstance().getMyChannels())) {
                        AloneFragmentActivity.with(getContext()).start(AccountFragment.class);
                    } else {
                        AloneFragmentActivity.with(getContext()).start(CreateChannelFragment.class);
                    }
                } else {
                    NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true);
                }
            }
        });
        layoutGuest.setOnClickListener(new OnSingleClickListener() {
            @SuppressLint("CheckResult")
            @Override
            public void onSingleClick(View view) {
                logActiveEsport();
                NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true);
            }
        });
        ivAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logActiveEsport();
                if (CollectionUtils.isNotEmpty(AccountBusiness.getInstance().getMyChannels())) {
                    AloneFragmentActivity.with(getContext()).start(AccountFragment.class);
                } else {
                    AloneFragmentActivity.with(getContext()).start(CreateChannelFragment.class);
                }
            }
        });
        com.metfone.selfcare.util.Utilities.adaptViewForInserts(header);
    }

    /*@Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        refreshData();
        if (isLogin()) {
            presenter.getUserId();
        }
        setupHeaderView();
    }*/

    @Override
    public void onStart() {
        super.onStart();
        refreshData();
        if (isLogin()) {
            presenter.getUserId();
        }
        setupHeaderView();
    }

    /**
     * refresh data
     * Created_by @dchieu on 9/7/20
     */
    private void refreshData() {
        currentPage = 0;

        getData();
    }

    /**
     * Call service lay du lieu man home
     * Created_by @dchieu on 9/7/20
     */
    private void getData() {
        if (CollectionUtils.isEmpty(items)) multipleStatusView.showLoading();

        presenter.getDataHome(currentPage++);
    }

    private void initRecyclerView() {
//        if (items == null)
//            items = new ArrayList<>();
//        else
//            items.clear();

        adapter.setItems(items);
        homeLiveChatItemViewBinder = new HomeLiveChatItemViewBinder();
        adapter.register(ObjectHomeLiveChat.class, homeLiveChatItemViewBinder);
        adapter.register(ObjectHomePopularLive.class, new HomePopularLiveItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickChannel(ChannelInfoResponse channelInfo) {
                logActiveEsport();
                navigateToFragmentChannel(channelInfo);
            }

            @Override
            public void onClickVideo(VideoInfoResponse video) {
                logActiveEsport();
                ApplicationController.self().getFirebaseEventBusiness().logLiveEsport(video.title, String.valueOf(video.id));
                Utilities.openPlayer(getParentActivity(), video, true);
            }
        }, new HomeMetfoneViewHolder.IScrollData() {
            @Override
            public void onScrollData() {
                logActiveEsport();
            }
        }));
        adapter.register(ObjectHomePopularUploaded.class, new HomePopularUploadedItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                logActiveEsport();
                ApplicationController.self().getFirebaseEventBusiness().logPopularEsport(video.title, String.valueOf(video.id));
                Utilities.openPlayer(getParentActivity(), video, true);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                logActiveEsport();
                navigateToFragmentChannel(channel);
            }
        }, new HomePopularUploadedItemViewBinder.OnScrollListener() {
            @Override
            public void onEndLessScroll() {
                if (!presenter.isLoading() && presenter.isCanLoadMore()) {
                    getData();
                }
            }
        }, new HomeMetfoneViewHolder.IScrollData() {
            @Override
            public void onScrollData() {
                logActiveEsport();
            }
        }));
        adapter.register(ObjectHomeMetfone.class, new HomeMetfoneItemViewBinder(new HomeMetfoneViewHolder.IOnClickItem() {
            @Override
            public void onClickVideo(VideoInfoResponse video) {
                logActiveEsport();
                Utilities.openPlayer(getParentActivity(), video, true);
            }

            @Override
            public void onClickChannel(ChannelInfoResponse channel) {
                logActiveEsport();
                navigateToFragmentChannel(channel);
            }
        }));
        adapter.register(ObjectHomeGame.class, new HomeGameItemViewBinder(game -> {
                    logActiveEsport();
                    if (game != null)
                        getKhHomeClient().wsAppLog(new MPApiCallback<BaseResponse>() {
                            @Override
                            public void onResponse(Response<BaseResponse> response) {
                                if(response.isSuccessful()){
                                    android.util.Log.e(TAG, String.format("wsLogApp => succeed [%s]", Constants.LOG_APP.GAME_CHANNEL));
                                }
                            }

                            @Override
                            public void onError(Throwable error) {
                                android.util.Log.e(TAG, "wsLogApp => failed" );
                            }
                        }, Constants.LOG_APP.GAME_CHANNEL);
                        AloneFragmentActivity.with(getActivity())
                                .parameters(GamesContainerFragment.newBundle(game.id))
                                .start(GamesContainerFragment.class);
                }, new HomeMetfoneViewHolder.IScrollData() {
                    @Override
                    public void onScrollData() {
                        logActiveEsport();
                    }
                })
        );
        rvHome.setHasFixedSize(false);
        rvHome.setNestedScrollingEnabled(false);
        if (Constant.FLAG_AUTO_PLAY_ESPORT) {
            scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                scrollView.removeCallbacks(runnableResumeAutoPlaying);
                scrollView.removeCallbacks(runnablePauseAutoPlaying);
                if (homeLiveChatItemViewBinder != null) {
                    boolean isVisible = ViewUtils.isVisible(homeLiveChatItemViewBinder.getRecyclerView());
                    Log.d(TAG, "onVisibilityChanged: " + isVisible);
                    //boolean isVisible = visibilityChecker.isVisible(homeLiveChatItemViewBinder.getRecyclerView(), 30);
                    Log.d(TAG, "scrollView.setOnScrollChangeListener -> scrollX: " + scrollX + ", scrollY: " + scrollY + ", isVisible: " + isVisible);
                    if (isVisible) {
                        scrollView.postDelayed(runnableResumeAutoPlaying, 350L);
                    } else {
                        scrollView.postDelayed(runnablePauseAutoPlaying, 250L);
                    }
                }
            });
        }
        rvHome.setAdapter(adapter);

        if (rvHome.getItemDecorationCount() <= 0)
            rvHome.addItemDecoration(new VerticalTopContentDecoration(getContext()));

        adapter.notifyDataSetChanged();
    }

    private void navigateToFragmentChannel(ChannelInfoResponse channel) {
        if (channel != null)
            AloneFragmentActivity.with(getActivity())
                    .parameters(ChannelContainerFragment.newBundle(channel.id))
                    .start(ChannelContainerFragment.class);
    }

//    @OnClick({R.id.lnHomeLogin, R.id.clHomeAccount, R.id.ivFavoriteHome, R.id.lnHomeSearch, R.id.avAvatarHome, R.id.tvAccountDescription, R.id.tvAccountName})
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.lnHomeLogin:
//                NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true);
//                break;
////
////            case R.id.clHomeAccount:
////                lnAccountDiamond.setVisibility(View.GONE);
////                tvAccountDescription.setVisibility(View.VISIBLE);
////                break;
//
//            case R.id.lnHomeSearch:
//                AloneFragmentActivity.with(getContext()).start(SearchFragment.class);
//                break;
//
//            case R.id.ivFavoriteHome:
//                if (isLogin()) {
//                    AloneFragmentActivity.with(getContext()).start(MyListContainerFragment.class);
//                } else {
//                    NavigateActivityHelper.navigateToRegisterScreenActivity(getParentActivity(), true);
//                }
//                break;
//
//            case R.id.tvAccountDescription:
//            case R.id.avAvatarHome:
//            case R.id.tvAccountName:
//                if (CollectionUtils.isNotEmpty(AccountBusiness.getInstance().getMyChannels())) {
//                    AloneFragmentActivity.with(getContext()).start(AccountFragment.class);
//                } else {
//                    AloneFragmentActivity.with(getContext()).start(CreateChannelFragment.class);
//                }
//                break;
//
//        }
//    }

    public void onGetDataHomeSuccess(ArrayList<HomeResponse> response, int currentPage) {
        try {
            if (currentPage == 0) {
                if (items == null) items = new ArrayList<>();
                else items.clear();
                if (CollectionUtils.isEmpty(response)) {
                    if (multipleStatusView != null) multipleStatusView.showEmpty();
                    if (rvHome != null) rvHome.setVisibility(View.INVISIBLE);
                } else {
                    if (multipleStatusView != null) multipleStatusView.showContent();
                    if (rvHome != null) rvHome.setVisibility(View.VISIBLE);

                    ObjectHomeLiveChat live = null;
                    ObjectHomePopularLive channel = null;
                    ObjectHomePopularUploaded uploaded = null;
                    ObjectHomeMetfone metfone = null;
                    ObjectHomeGame game = null;

                    for (HomeResponse res : response) {
                        if (res.type.equalsIgnoreCase("ListVideoLive")) {
                            live = new ObjectHomeLiveChat(res);
                        } else if (res.type.equalsIgnoreCase("PopularLiveChannel") && CollectionUtils.isNotEmpty(res.listVideo)) {
                            //if (BuildConfig.DEBUG && live == null) live = new ObjectHomeLiveChat(res);
                            channel = new ObjectHomePopularLive(res);
                        } else if (res.type.equalsIgnoreCase("PopularUploadedVideo") && CollectionUtils.isNotEmpty(res.listVideo)) {
                            uploaded = new ObjectHomePopularUploaded(res);

                            if (res.listVideo.size() >= presenter.PAGE_SIZE) {
                                presenter.canLoadMore = true;
                            }
                        } else if (res.type.equalsIgnoreCase("VideoTournament") && res.video != null) {
                            metfone = new ObjectHomeMetfone(res);
                        } else if (res.type.equalsIgnoreCase("ListGame") && CollectionUtils.isNotEmpty(res.listGame)) {
                            game = new ObjectHomeGame(res);
                        }
                    }

                    if (live != null) items.add(live);
                    if (channel != null) items.add(channel);
                    if (uploaded != null) items.add(uploaded);
                    if (metfone != null) items.add(metfone);
                    if (game != null) items.add(game);
                }
                if (adapter != null) {
                    adapter.setItems(items);
                    adapter.notifyDataSetChanged();
                }
            } else {
                for (HomeResponse res : response) {
                    if (res.type.equalsIgnoreCase("PopularUploadedVideo") && CollectionUtils.isNotEmpty(res.listVideo)) {
                        for (Object item : items) {
                            if (item instanceof ObjectHomePopularUploaded) {
                                ((ObjectHomePopularUploaded) item).homeResponse.listVideo.addAll(res.listVideo);
                            }
                        }

                        if (res.listVideo.size() >= presenter.PAGE_SIZE) {
                            presenter.canLoadMore = true;
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onGetDataHomeFailure(String message) {
        if (CollectionUtils.isEmpty(items)) {
            if (NetworkUtils.isConnected()) {
                if (multipleStatusView != null) multipleStatusView.showEmpty();
            } else {
                if (multipleStatusView != null) multipleStatusView.showNoNetwork();
            }
            if (rvHome != null) rvHome.setVisibility(View.INVISIBLE);

        }
        presenter.canLoadMore = true;

        currentPage--;
    }

    @Subscribe
    public void onChangeAccountEvent(ChangeAccountEvent event) {
        setupHeaderView();
    }

    @Subscribe
    public void onCreateChannelEvent(CreateChannelEvent event) {
        setupHeaderView();
        ImageLoader.setAvatarUrl(ivAvatar, AccountBusiness.getInstance().getChannelInfo().urlAvatar);

    }

    @Subscribe
    public void onChangeStateTinyScreen(OnChangeStateVideoTinyEvent event) {
        if (isResumed())
            new Handler().postDelayed(this::resumeAutoPlay, 350L);
    }

    @Override
    public void onDestroyView() {
        if (homeLiveChatItemViewBinder != null) {
            AutoPlayHorizontalUtils.stopPlaying(homeLiveChatItemViewBinder.getRecyclerView());
        }
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
/*        if (event.loadType == LoadInfoUser.LoadType.LOADING) {
            layout_user.setVisibility(View.GONE);
            layout_guest.setVisibility(View.GONE);
            layout_no_data.setVisibility(View.VISIBLE);
        } else if (event.loadType == LoadInfoUser.LoadType.NOT_LOGIN) {
            layout_user.setVisibility(View.GONE);
            layout_guest.setVisibility(View.VISIBLE);
            layout_no_data.setVisibility(View.GONE);
        } else */
        if (event.loadType == LoadInfoUser.LoadType.USER_INFO) {
//            layout_user.setVisibility(View.VISIBLE);
//            layout_guest.setVisibility(View.GONE);
//            layout_no_data.setVisibility(View.GONE);
            drawProfile(/*event.rankDTO, event.userInfo*/);
        }
    }

    public void setupHeaderView() {
        drawProfile();
    }

    public void checkUpdateChannel(ArrayList<ChannelInfoResponse> responses) {
        for (ChannelInfoResponse item: responses) {
            if (!isUpdate(item)) {
                presenter.updateChannel(item);
            }
        }
    }

    public boolean isUpdate(ChannelInfoResponse channel) {
        return SPUtils.getInstance().getBoolean("update_channel_"+channel.id, false);
    }


    private void drawProfile() {
        if (getParentActivity() == null || !isAdded()) return;
        if (isLogin()) {
            if (layoutGuest != null) layoutGuest.setVisibility(View.GONE);
            if (layoutUser != null) layoutUser.setVisibility(View.VISIBLE);
            UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getParentActivity());
            UserInfo currentUser = userInfoBusiness.getUser();
            String name = currentUser.getFull_name();
            ChannelInfoResponse channel = AccountBusiness.getInstance().getChannelInfo();
            //Neu co channel -> hien thi channel va descriptionChannel
            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_CACHE_CHANGE_ACCOUNT_CHANNEL, Boolean.class, false)) {
                //if (tvName != null) tvName.setText(TextUtils.isEmpty(name) ? getString(R.string.profile_chanel) : name);
                if (tvName != null) tvName.setText(TextUtils.isEmpty(name) ? "" : name);

                if (ivCrown != null) {
                    ivCrown.setVisibility(View.VISIBLE);
                }
                if (tvRankName != null) {
                    tvRankName.setVisibility(View.VISIBLE);
                }
                if (tvAccountDescription != null) {
                    tvAccountDescription.setVisibility(View.GONE);
                }
                //Neu khong co channel -> Hien thi rank va ten nguoi dung
                AccountRankDTO account = ApplicationController.self().getAccountRankDTO();
                initRankInfo(account);

                if (!TextUtils.isEmpty(currentUser.getAvatar())) {
                    Glide.with(this)
                            .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .transform(new MultiTransformation<Bitmap>(new CenterCrop(), new CircleCrop()))
                            .into(ivAvatar);
                }
            } else {
                if (channel != null) {
                    ImageLoader.setAvatarUrl(ivAvatar, channel.urlAvatar);
                    if (tvName != null) tvName.setText(channel.name);
                    if (tvAccountDescription != null) {
                        tvAccountDescription.setVisibility(View.VISIBLE);
                        tvAccountDescription.setText(TextUtils.isEmpty(channel.description) ? getString(R.string.profile_chanel) : channel.description);
                    }
                    if (ivCrown != null) {
                        ivCrown.setVisibility(View.GONE);
                    }
                    if (tvRankName != null) {
                        tvRankName.setVisibility(View.GONE);
                    }
                } else {
                    //if (tvName != null) tvName.setText(TextUtils.isEmpty(name) ? getString(R.string.profile_chanel) : name);
                    if (tvName != null) tvName.setText(TextUtils.isEmpty(name) ? "" : name);
                    if (ivCrown != null) {
                        ivCrown.setVisibility(View.VISIBLE);
                    }
                    if (tvRankName != null) {
                        tvRankName.setVisibility(View.VISIBLE);
                    }
                    if (tvAccountDescription != null) {
                        tvAccountDescription.setVisibility(View.GONE);
                    }
                    //Neu khong co channel -> Hien thi rank va ten nguoi dung
                    AccountRankDTO account = ApplicationController.self().getAccountRankDTO();
                    initRankInfo(account);
                    if (!TextUtils.isEmpty(currentUser.getAvatar())) {
                        Glide.with(this)
                                .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                                .transform(new MultiTransformation<Bitmap>(new CenterCrop(), new CircleCrop()))
                                .placeholder(R.drawable.ic_avatar_member)
                                .error(R.drawable.ic_avatar_member)
                                .into(ivAvatar);
                    }
                }
            }

        } else {
            if (layoutGuest != null) layoutGuest.setVisibility(View.VISIBLE);
            if (layoutUser != null) layoutUser.setVisibility(View.GONE);
        }
    }

    private void initRankInfo(AccountRankDTO account) {
        if (account != null) {
            KhUserRank myRank = KhUserRank.getById(account.rankId);
            if (myRank != null) {
                if (tvRankName != null) tvRankName.setText(getString(myRank.resRankName));

//            int rankID = account.rankId;
//            EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
//            EnumUtils.CrownRank crownRank = EnumUtils.CrownRank.getById(rankID);
                if (ivCrown != null) {
                    ivCrown.setVisibility(View.VISIBLE);

                    Glide.with(this)
                            .load(myRank.resRank)
                            .centerCrop()
                            .placeholder(R.drawable.ic_crown_member)
                            .error(R.drawable.ic_crown_member)
                            .into(ivCrown);
                }

                if (ivAvatar != null)
                    Glide.with(this)
                            .load(myRank.resAvatar)
                            .centerCrop()
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(ivAvatar);
            } else {
                if (ivAvatar != null)
                    ivAvatar.setImageDrawable(ContextCompat.getDrawable(getParentActivity(), R.drawable.ic_avatar_member));

                Glide.with(this)
                        .load(ContextCompat.getDrawable(getParentActivity(), R.drawable.ic_crown_member))
                        .centerCrop()
                        .placeholder(R.drawable.ic_crown_member)
                        .error(R.drawable.ic_crown_member)
                        .into(ivCrown);

                if (tvRankName != null) tvRankName.setText(R.string.kh_user_rank_member);
//            ivCrown.setVisibility(View.GONE);
            }
        } else {
            if (ivAvatar != null)
                ivAvatar.setImageDrawable(ContextCompat.getDrawable(getParentActivity(), R.drawable.ic_avatar_member));

            Glide.with(this)
                    .load(ContextCompat.getDrawable(getParentActivity(), R.drawable.ic_crown_member))
                    .centerCrop()
                    .placeholder(R.drawable.ic_crown_member)
                    .error(R.drawable.ic_crown_member)
                    .into(ivCrown);

            if (tvRankName != null) tvRankName.setText(R.string.kh_user_rank_member);
//            ivCrown.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeAutoPlay();
    }

    @Override
    public void onPause() {
        pauseAutoPlay(true);
        super.onPause();
    }

    public void pauseAutoPlay(boolean force) {
        if (homeLiveChatItemViewBinder != null && homeLiveChatItemViewBinder.getRecyclerView() != null) {
            boolean isVisible = ViewUtils.isVisible(homeLiveChatItemViewBinder.getRecyclerView());
            Log.d(TAG, "onVisibilityChanged: " + isVisible);
            //boolean isVisible = visibilityChecker.isVisible(homeLiveChatItemViewBinder.getRecyclerView(), 30);
            AutoPlayHorizontalUtils.pausePlaying(homeLiveChatItemViewBinder.getRecyclerView());
        }
    }

    public void resumeAutoPlay() {
        if (homeLiveChatItemViewBinder != null && homeLiveChatItemViewBinder.getRecyclerView() != null) {
            boolean isVisible = ViewUtils.isVisible(homeLiveChatItemViewBinder.getRecyclerView());
            Log.d(TAG, "onVisibilityChanged: " + isVisible);
            //boolean isVisible = visibilityChecker.isVisible(homeLiveChatItemViewBinder.getRecyclerView(), 30);
            if (isVisible)
                AutoPlayHorizontalUtils.resumePlaying(homeLiveChatItemViewBinder.getRecyclerView());
        }
    }

    @Subscribe
    public void onRequestRefreshInfo(RequestRefreshInfo event) {
        try {
            drawProfile();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    private void logActiveEsport(){
        ((HomeActivity)getParentActivity()).logActiveTab(FirebaseEventBusiness.TabLog.TAB_ESPORT);
    }
}

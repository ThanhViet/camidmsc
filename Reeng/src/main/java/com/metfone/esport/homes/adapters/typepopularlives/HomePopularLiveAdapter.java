package com.metfone.esport.homes.adapters.typepopularlives;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;

import java.util.ArrayList;

public class HomePopularLiveAdapter extends BaseListAdapter<VideoInfoResponse, HomeMetfoneViewHolder> {
    private HomeMetfoneViewHolder.IOnClickItem onClickItem;

    private int with;

    public HomePopularLiveAdapter(Context context, ArrayList<VideoInfoResponse> items, HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        super(context);

        setNewData(items);

        this.onClickItem = onClickItem;

        with = ScreenUtils.getScreenWidth() * 2 / 3;
    }

    @NonNull
    @Override
    public HomeMetfoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_live_chanel, parent, false);
//
//        ViewGroup.LayoutParams params = v.getLayoutParams();
//
//        params.width = with;
//
//        v.setLayoutParams(params);

        return new HomeMetfoneViewHolder(LayoutInflater.from(context), parent, with);

//        return new HomeLivePopularViewHolder(LayoutInflater.from(context), parent, with);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeMetfoneViewHolder holder, int position) {

        holder.binData(mData.get(position), onClickItem);

//        holder.binData(mData.get(position), onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}

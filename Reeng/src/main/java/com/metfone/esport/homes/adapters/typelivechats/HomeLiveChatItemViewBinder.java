package com.metfone.esport.homes.adapters.typelivechats;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.ViewUtils;
import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.util.AutoPlayHorizontalUtils;
import com.metfone.esport.util.Log;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeLiveChatItemViewBinder extends ExtSaveStateItemViewBinder<ObjectHomeLiveChat, HomeLiveChatItemViewBinder.HomeLiveChatBinderViewHolder> {

    @Override
    public int getItemLayoutId() {
        return R.layout.item_home_live_chat_binder;
    }

    @Override
    public HomeLiveChatBinderViewHolder onCreateBinderHolder(View view) {
        return new HomeLiveChatBinderViewHolder(view);
    }

    public RecyclerView getRecyclerView() {
        return getViewHolder() == null ? null : getViewHolder().recyclerView;
    }

    public class HomeLiveChatBinderViewHolder extends ExtSaveStateViewHolder<ObjectHomeLiveChat> {
        HomeLiveWithChatAdapter adapter = null;

/*        @BindView(R.id.rvOnlyRecyclerView)
        RecyclerView rvOnlyRecyclerView;*/

        @BindView(R.id.dsvHomeLiceChat)
        ExtDiscreteScrollView recyclerView;

        public HomeLiveChatBinderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            recyclerView.addOnItemChangedListener((viewHolder, adapterPosition) -> {
                boolean isVisible = ViewUtils.isVisible(recyclerView);
                Log.d("AutoPlayHorizontalUtils", "recyclerView Visibility: " + isVisible);

                AutoPlayHorizontalUtils.onScrollPlayVideo(recyclerView, adapterPosition, isVisible);
            });
        }

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return recyclerView.getLayoutManager();
        }

        @Override
        public void onHolderCreated() {
            adapter = new HomeLiveWithChatAdapter(itemView.getContext(), null);
//            adapter.setHasStableIds(true);
            recyclerView.setAdapter(adapter);
            recyclerView.setOffscreenItems(1);
            recyclerView.setSlideOnFling(true);
            recyclerView.setItemTransformer(new ExtScaleTransformer.Builder()
                    .setMaxScale(1f)
                    .setMinScale(0.85f)
                    /* .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                     .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one*/
                    .build());
        }

        @Override
        public void onBindData(ObjectHomeLiveChat o) {
            if (adapter != null) {
                if (o.homeResponse != null && CollectionUtils.isNotEmpty(o.homeResponse.listVideo)) {
                    adapter.setNewData(o.homeResponse.listVideo);

                    if (o.homeResponse.listVideo.size() >= 3)
                        recyclerView.post(() -> recyclerView.scrollToPosition(1));
                } else {
                    adapter.clear();
                }
            }
        }

        public RecyclerView getRecyclerView() {
            return recyclerView;
        }
    }
}

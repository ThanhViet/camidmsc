package com.metfone.esport.homes.adapters;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class ExtSaveStateViewHolder<T> extends RecyclerView.ViewHolder {
    public abstract RecyclerView.LayoutManager getLayoutManager();

    public abstract void onHolderCreated();

    public abstract void onBindData(T o);

    public ExtSaveStateViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}

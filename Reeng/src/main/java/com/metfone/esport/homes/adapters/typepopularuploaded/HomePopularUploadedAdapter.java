package com.metfone.esport.homes.adapters.typepopularuploaded;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;

import java.util.ArrayList;

public class HomePopularUploadedAdapter extends BaseListAdapter<VideoInfoResponse, HomeMetfoneViewHolder> {

    private int width = 0;

    private Context context;

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;

    public HomePopularUploadedAdapter(Context context, ArrayList<VideoInfoResponse> items, HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        super(context);

        this.context = context;

        this.onClickItem = onClickItem;

        setNewData(items);

        width = ScreenUtils.getScreenWidth() * 2 / 3;
    }

    @NonNull
    @Override
    public HomeMetfoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(context).inflate(R.layout.item_home_video_uploaded, parent, false);

        return new HomeMetfoneViewHolder(LayoutInflater.from(context), parent, width);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeMetfoneViewHolder holder, int position) {
        holder.binData(mData.get(position), onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

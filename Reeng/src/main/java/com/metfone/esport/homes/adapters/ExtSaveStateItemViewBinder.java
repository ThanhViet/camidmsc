package com.metfone.esport.homes.adapters;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drakeet.multitype.ItemViewBinder;

import org.jetbrains.annotations.NotNull;

public abstract class ExtSaveStateItemViewBinder<T, VH extends ExtSaveStateViewHolder<T>> extends ItemViewBinder<T, VH> {

    private VH viewHolder;

    public abstract int getItemLayoutId();

    public abstract VH onCreateBinderHolder(View view);

    public VH getViewHolder() {
        return viewHolder;
    }

    @NotNull
    @Override
    public VH onCreateViewHolder(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup) {
        viewHolder = onCreateBinderHolder(layoutInflater.inflate(getItemLayoutId(), viewGroup, false));

        viewHolder.onHolderCreated();

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NotNull VH vh, T t) {
        if (getAdapter() instanceof ExtSaveStateMultiTypeAdapter) {
            Parcelable state = ((ExtSaveStateMultiTypeAdapter) getAdapter()).scrollState.get(getAdapterItems().get(vh.getAdapterPosition()));

            if (state == null) {
                vh.onBindData(t);
            } else {
                vh.getLayoutManager().onRestoreInstanceState(state);
            }
        }
    }
}

package com.metfone.esport.homes.adapters.games;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.homes.adapters.decorations.InSpacelayoutDecoration;
import com.metfone.esport.ui.viewholders.HomeGameViewHolder;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeGameItemViewBinder extends ExtSaveStateItemViewBinder<ObjectHomeGame, HomeGameItemViewBinder.HomeGameBinderViewHolder> {

    private HomeGameViewHolder.IOnClickItem onClickItem;
    private HomeMetfoneViewHolder.IScrollData scrollData;

    public HomeGameItemViewBinder(HomeGameViewHolder.IOnClickItem onClickItem,HomeMetfoneViewHolder.IScrollData scrollData) {
        this.onClickItem = onClickItem;
        this.scrollData = scrollData;
    }

    @Override
    public int getItemLayoutId() {
        return R.layout.item_only_title_with_recycler_view;
    }

    @Override
    public HomeGameBinderViewHolder onCreateBinderHolder(View view) {
        return new HomeGameBinderViewHolder(view);
    }

    public class HomeGameBinderViewHolder extends ExtSaveStateViewHolder<ObjectHomeGame> {
        HomeGameAdapter adapter = null;

        @BindView(R.id.tvSingleTextTitle)
        AppCompatTextView tvOnlyTitle;

        @BindView(R.id.rvOnlyTitleRecyclerView)
        RecyclerView rvOnlyTitleRecyclerView;

        public HomeGameBinderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return rvOnlyTitleRecyclerView.getLayoutManager();
        }

        @Override
        public void onHolderCreated() {
            adapter = new HomeGameAdapter(itemView.getContext(), null, onClickItem);

            rvOnlyTitleRecyclerView.setAdapter(adapter);
            rvOnlyTitleRecyclerView.setHasFixedSize(true);
            rvOnlyTitleRecyclerView.addItemDecoration(new InSpacelayoutDecoration(itemView.getContext()));
            rvOnlyTitleRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                        scrollData.onScrollData();
                    }
                }
            });
        }

        @Override
        public void onBindData(ObjectHomeGame o) {
            tvOnlyTitle.setText(o.homeResponse.title);

            if (adapter != null) {
                adapter.setNewData(o.homeResponse.listGame);
            }
        }
    }
}

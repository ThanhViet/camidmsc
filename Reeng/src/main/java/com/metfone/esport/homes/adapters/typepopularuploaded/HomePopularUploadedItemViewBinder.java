package com.metfone.esport.homes.adapters.typepopularuploaded;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.homes.adapters.decorations.InSpaceContentDecoration;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomePopularUploadedItemViewBinder extends ExtSaveStateItemViewBinder<ObjectHomePopularUploaded, HomePopularUploadedItemViewBinder.HomePopularUploadedBinderViewHolder> {

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;
    private HomePopularUploadedItemViewBinder.OnScrollListener onScrollListener;
    private HomeMetfoneViewHolder.IScrollData scrollData;

    public HomePopularUploadedItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public HomePopularUploadedItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem, OnScrollListener onScrollListener,HomeMetfoneViewHolder.IScrollData scrollData) {
        this.onClickItem = onClickItem;
        this.onScrollListener = onScrollListener;
        this.scrollData = scrollData;
    }

    @Override
    public int getItemLayoutId() {
        return R.layout.item_only_title_with_recycler_view;
    }

    @Override
    public HomePopularUploadedBinderViewHolder onCreateBinderHolder(View view) {
        return new HomePopularUploadedBinderViewHolder(view);
    }

    public class HomePopularUploadedBinderViewHolder extends ExtSaveStateViewHolder<ObjectHomePopularUploaded> {

        private HomePopularUploadedAdapter adapter = null;

        @BindView(R.id.tvSingleTextTitle)
        AppCompatTextView tvOnlyTitle;

        @BindView(R.id.rvOnlyTitleRecyclerView)
        RecyclerView rvOnlyTitleRecyclerView;

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return rvOnlyTitleRecyclerView.getLayoutManager();
        }

        public HomePopularUploadedBinderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onHolderCreated() {
            adapter = new HomePopularUploadedAdapter(itemView.getContext(), null, onClickItem);

            rvOnlyTitleRecyclerView.setAdapter(adapter);
            rvOnlyTitleRecyclerView.setHasFixedSize(true);
            rvOnlyTitleRecyclerView.addItemDecoration(new InSpaceContentDecoration(itemView.getContext()));
            rvOnlyTitleRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                        scrollData.onScrollData();
                    }
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() >= (adapter.getItemCount() - 3)) {
                        //bottom of list!
                        if (onScrollListener != null)
                            onScrollListener.onEndLessScroll();
                    }
                }
            });
        }

        @Override
        public void onBindData(ObjectHomePopularUploaded o) {
            tvOnlyTitle.setText(o.homeResponse.title);

            if (adapter != null) {
                adapter.setNewData(o.homeResponse.listVideo);
            }
        }
    }

    public interface OnScrollListener {
        void onEndLessScroll();
    }
}

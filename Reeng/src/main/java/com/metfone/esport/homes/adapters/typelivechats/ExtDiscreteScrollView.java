package com.metfone.esport.homes.adapters.typelivechats;

import android.content.Context;
import android.util.AttributeSet;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.lang.reflect.Field;

public class ExtDiscreteScrollView extends DiscreteScrollView {
    public ExtDiscreteScrollView(Context context) {
        super(context);
    }

    public ExtDiscreteScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtDiscreteScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        LayoutManager layoutManager = getLayoutManager();
        if (layoutManager == null) return false;
        int count = layoutManager.getItemCount();
        int position = -1;
        try {
            Field field = layoutManager.getClass().getDeclaredField("currentPosition");
            field.setAccessible(true);
            position = (int) field.get(layoutManager);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (count == 0) {
            return false;
        } else {
            if (direction < 0) {
                return position != 0;
            } else {
                return position != count - 1;
            }
        }
    }
}

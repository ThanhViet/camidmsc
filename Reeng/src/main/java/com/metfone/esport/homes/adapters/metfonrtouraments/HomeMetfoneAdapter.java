package com.metfone.esport.homes.adapters.metfonrtouraments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;

import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;

import java.util.ArrayList;

public class HomeMetfoneAdapter extends BaseListAdapter<VideoInfoResponse, HomeMetfoneViewHolder> {
//    private int with = 0;

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;

    public HomeMetfoneAdapter(Context context, ArrayList<VideoInfoResponse> items, HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        super(context);

        setNewData(items);

        this.onClickItem = onClickItem;

//        with = ScreenUtils.getScreenWidth() * 2 / 3;
    }

    @NonNull
    @Override
    public HomeMetfoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HomeMetfoneViewHolder(LayoutInflater.from(context), parent, 0);

//        ViewGroup.LayoutParams params = v.getLayoutParams();
//
//        params.width = with;
//
//        v.setLayoutParams(params);

    }

    @Override
    public void onBindViewHolder(@NonNull HomeMetfoneViewHolder holder, int position) {
        holder.binData(mData.get(position), onClickItem);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

}

package com.metfone.esport.homes;

import static com.metfone.esport.ui.live.LiveVideoFragment.PRE_CODE_THANKMESSAGE;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.base.BaseModel;
import com.metfone.esport.base.BasePresenter;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Constant;
import com.metfone.esport.donate.fragment.DonateDialogOtpVerifyFragment;
import com.metfone.esport.entity.camid.CamIdLoginParam;
import com.metfone.esport.entity.camid.CamIdLoginResponse;
import com.metfone.esport.entity.camid.CamIdRefreshTokenParam;
import com.metfone.esport.entity.camid.CamIdRefreshTokenResponse;
import com.metfone.esport.entity.camid.CamIdUserWrapper;
import com.metfone.esport.entity.repsonse.ChannelInfoResponse;
import com.metfone.esport.entity.repsonse.HomeResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.network.donate.DonateESportApiCallback;
import com.metfone.esport.network.donate.DonateESportClient;
import com.metfone.esport.network.donate.request.SubDonateGiftPackageRequest;
import com.metfone.esport.network.donate.request.SubUpdateChannelInformationRequest;
import com.metfone.esport.network.donate.response.DonatePackageModel;
import com.metfone.esport.network.donate.response.WsDonateGiftPackageResponse;
import com.metfone.esport.network.donate.response.WsUpdateChannelInformationResponse;
import com.metfone.esport.service.CamIdServiceRetrofit;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.model.account.UserInfo;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;
import retrofit2.Response;

class HomePresenter extends BasePresenter<HomeFragment, BaseModel> {
    public int PAGE_SIZE = 5;

    private Disposable homeDispose = null;

    public boolean canLoadMore = false;

    public boolean isLoading() {
        return homeDispose != null && !homeDispose.isDisposed();
    }

    public boolean isCanLoadMore() {
        return canLoadMore;
    }

    public HomePresenter(HomeFragment view, CompositeDisposable disposable) {
        super(view, disposable);
    }

    @Override
    public BaseModel getModel() {
        return new BaseModel();
    }

    public void getDataHome(final int currentPage) {
        if (currentPage > 0 && isLoading()) return;

        if (currentPage == 0 && homeDispose != null)
            homeDispose.dispose();

        canLoadMore = false;

        homeDispose = RxUtils.async(compositeDisposable, apiService.getDataHome(currentPage * PAGE_SIZE, PAGE_SIZE, getDefaultParam()), new ICallBackResponse<ArrayList<HomeResponse>>() {
            @Override
            public void onSuccess(ArrayList<HomeResponse> response) {
                if (view != null) view.onGetDataHomeSuccess(response, currentPage);
            }

            @Override
            public void onFail(String error) {
                if (view != null) view.onGetDataHomeFailure(error);
            }

            @Override
            public void onFinish() {
                if (homeDispose != null)
                    homeDispose.dispose();
            }
        });
    }


    public void login() {
        if (!AccountBusiness.getInstance().isLogin()) {
            String phoneNumber = SPUtils.getInstance().getString(Constant.PREF_USERNAME_ACCOUNT_ESPORT);
            String otp = SPUtils.getInstance().getString(Constant.PREF_OTP_ACCOUNT_ESPORT);
            String password = SPUtils.getInstance().getString(Constant.PREF_PASSWORD_ACCOUNT_ESPORT);
            if (StringUtils.isEmpty(phoneNumber)) return;

            CamIdLoginParam param;
            if (StringUtils.isNotEmpty(password)) {
                param = CamIdLoginParam.initWithPassword(phoneNumber, password);
            } else if (StringUtils.isNotEmpty(otp)) {
                param = CamIdLoginParam.initWithOtp(phoneNumber, otp);
            } else {
                param = new CamIdLoginParam();
            }
            RxUtils.asyncCamId(
                    compositeDisposable,
                    CamIdServiceRetrofit.getInstance().login(DeviceUtils.getUniqueDeviceId(), param),
                    new ICallBackResponse<CamIdLoginResponse>() {
                        @Override
                        public void onSuccess(CamIdLoginResponse response) {
                            AccountBusiness.getInstance().saveAuthenInfo(response);
                            SPUtils.getInstance().remove(Constant.PREF_USERNAME_ACCOUNT_ESPORT);
                            SPUtils.getInstance().remove(Constant.PREF_OTP_ACCOUNT_ESPORT);
                            SPUtils.getInstance().remove(Constant.PREF_PASSWORD_ACCOUNT_ESPORT);
                            getUserId();
                        }

                        @Override
                        public void onFail(String error) {

                        }
                    }
            );
        } else {
            getUserId();
        }
    }

    public void getUserId() {
        RxUtils.asyncCamId(
                compositeDisposable,
                CamIdServiceRetrofit.getInstance().getUserId(),
                new ICallBackResponse<CamIdUserWrapper>() {
                    @Override
                    public void onSuccess(CamIdUserWrapper response) {
                        if (response != null && response.user != null)
                            AccountBusiness.getInstance().saveUserId(response.user);

                        //getRankInfo();

                        getMyChannel();
                    }

                    @Override
                    public void onFail(String error) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException && ((HttpException) e).code() == 401) {
                            refreshToken(new ICallBackResponse<Object>() {
                                @Override
                                public void onSuccess(Object response) {
                                    getUserId();
                                }

                                @Override
                                public void onFail(String error) {

                                }
                            });
                        }
                    }
                }
        );
    }

    private void refreshToken(ICallBackResponse<Object> callback) {
        CamIdRefreshTokenParam param = new CamIdRefreshTokenParam();

        if (param.wsRequest != null)
            param.wsRequest.refresh_token = AccountBusiness.getInstance().getRefreshToken();

        RxUtils.asyncCamId(
                compositeDisposable,
                CamIdServiceRetrofit.getInstance().refreshToken(param),
                new ICallBackResponse<CamIdRefreshTokenResponse>() {
                    @Override
                    public void onSuccess(CamIdRefreshTokenResponse response) {
                        if (response != null && response.accessToken != null) {
                            AccountBusiness.getInstance().saveToken(response.accessToken);
                            AccountBusiness.getInstance().saveRefreshToken(response.refreshToken);
                        }

                        if (callback != null) {
                            callback.onSuccess(null);
                        }
                    }

                    @Override
                    public void onFail(String error) {

                    }
                }
        );
    }

//    private void getRankInfo() {
//        //Get Rank Info
//        VTCApiUseRoutingParam vtcApiUseRoutingParam = new VTCApiUseRoutingParam();
//        vtcApiUseRoutingParam.wsCode = "wsGetAccountRankInfo";
//        vtcApiUseRoutingParam.wsRequest = new VTCWsRequest();
//        vtcApiUseRoutingParam.wsRequest.isdn = AccountBusiness.getInstance().getPhoneNumber();
//        RxUtils.asyncSimple(
//                compositeDisposable,
//                VTCAPIServiceRetrofit.getInstance().wsAccountInfo(vtcApiUseRoutingParam),
//                new ICallBackResponse<VTCBaseResponse<VTCRankResponse>>() {
//                    @Override
//                    public void onSuccess(VTCBaseResponse<VTCRankResponse> response) {
//                        if (response != null && response.result != null && response.result.accountRankDTO != null) {
//                            AccountBusiness.getInstance().saveAccountRank(response.result.accountRankDTO);
//
//                            if (view != null) view.setupHeaderView();
//                        }
//                    }
//
//                    @Override
//                    public void onFail(String error) {
//
//                    }
//                }
//        );
//    }

    public void getMyChannel() {
        //"84358427382"
        RxUtils.async(
                compositeDisposable,
                apiService.getMyChannels(AccountBusiness.getInstance().getPhoneNumberOrUserId(), getDefaultParam()),
                new ICallBackResponse<ArrayList<ChannelInfoResponse>>() {
                    @Override
                    public void onSuccess(ArrayList<ChannelInfoResponse> response) {
                        AccountBusiness.getInstance().saveMyChannel(response);
                        if (view != null) view.setupHeaderView();
                        if (view != null) view.checkUpdateChannel(response);
                    }

                    @Override
                    public void onFail(String error) {
                        if (view != null) view.setupHeaderView();
                    }
                });
    }


    private String getMsisdn() {
        if (!StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getJidNumber())) {
            return ApplicationController.self().getReengAccountBusiness().getJidNumber();
        }
        return "";
    }
    public void updateChannel(ChannelInfoResponse channel) {
        String camId = "";
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(view.getParentActivity());
        UserInfo currentUser = userInfoBusiness.getUser();
        camId = currentUser.getUser_id() + "";

        SubUpdateChannelInformationRequest param = new SubUpdateChannelInformationRequest();
        param.setId(channel.id+"");
        param.setChannelName(channel.name);
        param.setCamId(camId);
        param.setMsisdn(getMsisdn());
        param.setComment("");
        param.setCommentKh("");
        param.setEmoneyAccount("");
        param.setStatus("1");
        param.setImageUrl(channel.urlAvatar);
        param.setBannerUrl(channel.urlImages);
        param.setCreatedDate(channel.createdDate);

        new DonateESportClient().wsUpdateChannelInformation(param, new DonateESportApiCallback<WsUpdateChannelInformationResponse>() {
            @Override
            public void onResponse(Response<WsUpdateChannelInformationResponse> response) {
                if (response.body() != null && response.body().isSuccessful()) {
                    SPUtils.getInstance().put("update_channel_"+channel.id, true);
                } else
                    SPUtils.getInstance().put("update_channel_"+channel.id, false);
            }

            @Override
            public void onError(Throwable error) {
                SPUtils.getInstance().put("update_channel_"+channel.id, false);
            }
        });
    }

}

package com.metfone.esport.homes.adapters.typepopularlives;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.homes.adapters.decorations.InSpaceContentDecoration;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomePopularLiveItemViewBinder extends ExtSaveStateItemViewBinder<ObjectHomePopularLive, HomePopularLiveItemViewBinder.HomePopularLiveBinderViewHolder> {

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;
    private HomeMetfoneViewHolder.IScrollData scrollData;

    public HomePopularLiveItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem,HomeMetfoneViewHolder.IScrollData scrollData) {
        this.onClickItem = onClickItem;
        this.scrollData = scrollData;
    }

    @Override
    public int getItemLayoutId() {
        return R.layout.item_only_title_with_recycler_view;
    }

    @Override
    public HomePopularLiveBinderViewHolder onCreateBinderHolder(View view) {
        return new HomePopularLiveBinderViewHolder(view);
    }

    public class HomePopularLiveBinderViewHolder extends ExtSaveStateViewHolder<ObjectHomePopularLive> {
        HomePopularLiveAdapter adapter = null;

        @BindView(R.id.tvSingleTextTitle)
        AppCompatTextView tvOnlyTitle;

        @BindView(R.id.rvOnlyTitleRecyclerView)
        RecyclerView rvOnlyTitleRecyclerView;

        public HomePopularLiveBinderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return rvOnlyTitleRecyclerView.getLayoutManager();
        }

        @Override
        public void onHolderCreated() {
            adapter = new HomePopularLiveAdapter(itemView.getContext(), null, onClickItem);

            rvOnlyTitleRecyclerView.setAdapter(adapter);
            rvOnlyTitleRecyclerView.setHasFixedSize(true);
            rvOnlyTitleRecyclerView.addItemDecoration(new InSpaceContentDecoration(itemView.getContext()), 0);
            rvOnlyTitleRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                        scrollData.onScrollData();
                    }
                }
            });
        }

        @Override
        public void onBindData(ObjectHomePopularLive o) {
            tvOnlyTitle.setText(o.homeResponse.title);

            if (adapter != null) {
                adapter.setNewData(o.homeResponse.listVideo);
            }
        }

    }
}

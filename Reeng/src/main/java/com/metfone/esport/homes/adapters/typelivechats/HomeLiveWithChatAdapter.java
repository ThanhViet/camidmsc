package com.metfone.esport.homes.adapters.typelivechats;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.metfone.esport.base.BaseListAdapter;
import com.metfone.esport.base.BaseViewHolder;
import com.metfone.esport.common.Constant;
import com.metfone.esport.customview.player.JZDataSource;
import com.metfone.esport.customview.player.Jzvd;
import com.metfone.esport.customview.player.JzvdAutoPlay;
import com.metfone.esport.entity.enums.ViewsUnitEnum;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.ImageLoader;
import com.metfone.esport.listener.AutoPlayListener;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;

import java.util.ArrayList;

import butterknife.BindView;

public class HomeLiveWithChatAdapter extends BaseListAdapter<VideoInfoResponse, HomeLiveWithChatAdapter.HomeLiveWithChatViewHolder> {
    private int with = 0;

    public HomeLiveWithChatAdapter(Context context, ArrayList<VideoInfoResponse> items) {
        super(context);

        setNewData(items);

        with = ScreenUtils.getScreenWidth() * 3 / 4;
    }

    @NonNull
    @Override
    public HomeLiveWithChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(Constant.FLAG_AUTO_PLAY_ESPORT ? R.layout.item_home_live_with_player : R.layout.item_home_live_with_chat, parent, false);

        ViewGroup.LayoutParams params = v.getLayoutParams();

        params.width = with;

        v.setLayoutParams(params);

        return new HomeLiveWithChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeLiveWithChatViewHolder holder, int position) {
        holder.binData(mData.get(position), position);
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    class HomeLiveWithChatViewHolder extends BaseViewHolder<VideoInfoResponse> {

        @BindView(R.id.iv_cover)
        AppCompatImageView ivThumbVideo;

        @BindView(R.id.tvEyeViewCount)
        AppCompatTextView tvEyeViewCount;

        @BindView(R.id.layout_eye_viewer)
        View layoutEyeViewer;

        @BindView(R.id.video_player)
        @Nullable
        JzvdAutoPlay videoPlayer;
        @BindView(R.id.pb_loading)
        @Nullable
        ProgressBar pbLoading;
        @BindView(R.id.btn_mute)
        @Nullable
        View btnMute;

        public HomeLiveWithChatViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public void binData(VideoInfoResponse entity, int position) {
            try {
                if (entity.ccu <= 0) {
                    layoutEyeViewer.setVisibility(View.GONE);
                } else {
                    layoutEyeViewer.setVisibility(View.VISIBLE);
                    tvEyeViewCount.setText(ViewsUnitEnum.getTextDisplay(entity.ccu));
                }
                ImageLoader.setImage(
                        ivThumbVideo,
                        entity.image_path_thumb,
                        R.drawable.bg_image_placeholder_es,
                        R.drawable.bg_image_error_es,
                        new CenterCrop());
                itemView.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public boolean isCheckNetwork() {
                        return true;
                    }

                    @Override
                    public void onSingleClick(View view) {
                        if (context instanceof AppCompatActivity) {
                            Utilities.openPlayer((AppCompatActivity) context, entity, true);
                        }
                    }
                });
                if (Constant.FLAG_AUTO_PLAY_ESPORT && videoPlayer != null) {
//                    videoPlayer.autoPlay = true;
                    if (btnMute != null) {
                        btnMute.setSelected(ApplicationController.getApp().isEnableVolumeAutoPlay);
                        btnMute.setVisibility(View.GONE);
                        btnMute.setOnClickListener(v -> {
                            if (btnMute.isSelected()) {
                                btnMute.setSelected(false);
                                if (videoPlayer != null) videoPlayer.mute();
                                ApplicationController.getApp().isEnableVolumeAutoPlay = false;
                            } else {
                                btnMute.setSelected(true);
                                if (videoPlayer != null) videoPlayer.unmute();
                                ApplicationController.getApp().isEnableVolumeAutoPlay = true;
                            }
                        });
                    }
                    videoPlayer.setAutoPlayListener(new AutoPlayListener() {
                        @Override
                        public void onPrepared() {
                            if (btnMute != null) {
                                btnMute.setVisibility(View.VISIBLE);
                                btnMute.setSelected(ApplicationController.getApp().isEnableVolumeAutoPlay);
                            }
                            if (videoPlayer != null) {
                                if (ApplicationController.getApp().isEnableVolumeAutoPlay)
                                    videoPlayer.unmute();
                                else videoPlayer.mute();
                            }
                            ivThumbVideo.setVisibility(View.INVISIBLE);
                            if (pbLoading != null) pbLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if (btnMute != null) {
                                btnMute.setVisibility(View.GONE);
                            }
                            if (videoPlayer != null) {
                                videoPlayer.setVisibility(View.GONE);
                            }
                            ivThumbVideo.setVisibility(View.VISIBLE);
                            if (pbLoading != null) pbLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onComplete() {
                            if (btnMute != null) {
                                btnMute.setVisibility(View.GONE);
                            }
                            if (videoPlayer != null) {
                                videoPlayer.setVisibility(View.GONE);
                            }
                            ivThumbVideo.setVisibility(View.VISIBLE);
                            if (pbLoading != null) pbLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onPlayerStateChanged(boolean isPlaying) {
                            if (!isPlaying) {
                                if (btnMute != null) btnMute.setVisibility(View.GONE);
                            }
                        }
                    });

                    videoPlayer.textureViewContainer.setOnTouchListener((v, event) -> {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (videoPlayer != null) {
                                videoPlayer.pause();
                                videoPlayer.setVisibility(View.GONE);
                            }
                            ivThumbVideo.setVisibility(View.VISIBLE);
                            if (btnMute != null) {
                                btnMute.setVisibility(View.GONE);
                            }
                            if (pbLoading != null) pbLoading.setVisibility(View.GONE);
                            if (context instanceof AppCompatActivity) {
                                Utilities.openPlayer((AppCompatActivity) context, entity, true);
                            }
                        }
                        return true;
                    });
                    if (StringUtils.isNotEmpty(entity.original_path)) {
                        JZDataSource dataSource = new JZDataSource(entity.original_path);
                        videoPlayer.setUp(dataSource, Jzvd.SCREEN_NORMAL);
                        if (ApplicationController.getApp().isEnableVolumeAutoPlay)
                            videoPlayer.unmute();
                        else videoPlayer.mute();
                    } else {
                        videoPlayer.setUp(null, Jzvd.SCREEN_NORMAL);
                        videoPlayer.reset();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

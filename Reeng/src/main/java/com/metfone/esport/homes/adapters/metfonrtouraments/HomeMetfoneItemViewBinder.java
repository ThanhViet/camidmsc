package com.metfone.esport.homes.adapters.metfonrtouraments;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.homes.adapters.ExtSaveStateItemViewBinder;
import com.metfone.esport.homes.adapters.ExtSaveStateViewHolder;
import com.metfone.esport.homes.adapters.decorations.InSpacelayoutDecoration;
import com.metfone.esport.ui.viewholders.HomeMetfoneViewHolder;
import com.metfone.selfcare.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeMetfoneItemViewBinder extends ExtSaveStateItemViewBinder<ObjectHomeMetfone, HomeMetfoneItemViewBinder.HomeMetfoneBinderViewHolder> {

    private HomeMetfoneViewHolder.IOnClickItem onClickItem;

    public HomeMetfoneItemViewBinder(HomeMetfoneViewHolder.IOnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @Override
    public int getItemLayoutId() {
        return R.layout.item_only_title_with_recycler_view;
    }

    @Override
    public HomeMetfoneBinderViewHolder onCreateBinderHolder(View view) {
        return new HomeMetfoneBinderViewHolder(view);
    }

    public class HomeMetfoneBinderViewHolder extends ExtSaveStateViewHolder<ObjectHomeMetfone> {
        HomeMetfoneAdapter adapter = null;

        @BindView(R.id.tvSingleTextTitle)
        AppCompatTextView tvOnlyTitle;

        @BindView(R.id.rvOnlyTitleRecyclerView)
        RecyclerView rvOnlyTitleRecyclerView;

        public HomeMetfoneBinderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public RecyclerView.LayoutManager getLayoutManager() {
            return rvOnlyTitleRecyclerView.getLayoutManager();
        }

        @Override
        public void onHolderCreated() {
            adapter = new HomeMetfoneAdapter(itemView.getContext(), null, onClickItem);

            rvOnlyTitleRecyclerView.setAdapter(adapter);
            rvOnlyTitleRecyclerView.setHasFixedSize(true);
            rvOnlyTitleRecyclerView.addItemDecoration(new InSpacelayoutDecoration(itemView.getContext()));
        }

        @Override
        public void onBindData(ObjectHomeMetfone o) {
            tvOnlyTitle.setText(o.homeResponse.title);

            //Fake Item
            ArrayList<VideoInfoResponse> items = new ArrayList<>();
            items.add(o.homeResponse.video);

            if (adapter != null) {
                adapter.setNewData(items);
            }
        }
    }
}

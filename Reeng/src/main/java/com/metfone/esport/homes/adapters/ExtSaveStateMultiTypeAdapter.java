package com.metfone.esport.homes.adapters;

import android.os.Parcelable;

import androidx.recyclerview.widget.RecyclerView;

import com.drakeet.multitype.MultiTypeAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class ExtSaveStateMultiTypeAdapter extends MultiTypeAdapter {
    public HashMap<Object, Parcelable> scrollState = new HashMap<>();

    @Override
    public void onViewRecycled(@NotNull RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);

        RecyclerView.LayoutManager layoutManager = null;

        if (holder instanceof ExtSaveStateViewHolder) {
            layoutManager = ((ExtSaveStateViewHolder) holder).getLayoutManager();
        }

        if (layoutManager != null) {
            int itemCount = getItemCount();
            int position = holder.getAdapterPosition();
            if (position < itemCount && position >= 0) {
                try {
                    scrollState.put(getItems().get(holder.getAdapterPosition()), layoutManager.onSaveInstanceState());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

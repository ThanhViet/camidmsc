package com.metfone.esport.network.donate.response;

import com.metfone.esport.network.donate.BaseResponse;

import java.io.Serializable;

public class WsDonateGiftPackageResponse extends BaseResponse<DonateGiftPackageModel> implements Serializable {}
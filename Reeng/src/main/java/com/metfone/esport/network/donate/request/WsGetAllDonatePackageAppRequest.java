package com.metfone.esport.network.donate.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.esport.network.donate.BaseRequest;

public class WsGetAllDonatePackageAppRequest extends BaseRequest<WsGetAllDonatePackageAppRequest.Request> {
    public class Request {
        @SerializedName("language")
        private String language;
        @SerializedName("isdn")
        private String isdn;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }
    }
}

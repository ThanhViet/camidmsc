package com.metfone.esport.network.file;

public class RetryException extends Exception {

    public RetryException() {
        super("Maximum retry exceeded");
    }

    public RetryException(Throwable cause) {
        super(cause);
    }
}

package com.metfone.esport.network.file.download;

import android.text.TextUtils;

import com.metfone.esport.network.file.Constants;
import com.metfone.esport.network.file.Priority;
import com.metfone.esport.network.file.RetryPolicy;
import com.metfone.esport.util.Log;

import java.util.HashMap;

public class DownloadRequest implements Comparable<DownloadRequest> {
    private static final String TAG = DownloadRequest.class.getSimpleName();
    /**
     * Tells the current download state of this request
     */
    private int mDownloadState;
    /**
     * DownloadManager Id assigned to this request
     */
    private int mDownloadId;
    /**
     * The URL resource that this request is to download
     */
    private String mUrl;

    /**
     * The destination path on the device where the downloaded files needs to be put
     * It can be either External Directory ( SDcard ) or
     * internal app cache or files directory.
     * For using external SDCard access, application should have
     * this permission android.permission.WRITE_EXTERNAL_STORAGE declared.
     */
    private String filePath;
    private Object container;
    private RetryPolicy mRetryPolicy;
    /**
     * Whether or not this request has been canceled.
     */
    private boolean mCancelled = false;
    private boolean mDeleteDestinationFileOnFailure = true;
    private DownloadListener mDownloadListener;
    private HashMap<String, String> mCustomHeader;
    private Priority mPriority = Priority.HIGH;

    public DownloadRequest(String url) throws NullPointerException, IllegalArgumentException {
        if (TextUtils.isEmpty(url)) {
            throw new NullPointerException();
        }
        if ((!url.startsWith("http") && !url.startsWith("https"))) {
            Log.e(TAG, "exception");
            throw new IllegalArgumentException("Can only download HTTP/HTTPS URIs: " + url);
        }
        mCustomHeader = new HashMap<>();
        mDownloadState = Constants.STATUS_PENDING;
        mRetryPolicy = new RetryPolicy();
        mUrl = url;
    }

    /**
     * Returns the {@link Priority} of this request; {@link Priority#NORMAL} by default.
     */
    public Priority getPriority() {
        return mPriority;
    }

    /**
     * Set the {@link Priority}  of this request;
     *
     * @param priority
     * @return request
     */
    public DownloadRequest setPriority(Priority priority) {
        mPriority = priority;
        return this;
    }

    /**
     * Adds custom header to request
     *
     * @param key
     * @param value
     */
    public DownloadRequest addCustomHeader(String key, String value) {
        mCustomHeader.put(key, value);
        return this;
    }

    public RetryPolicy getRetryPolicy() {
        if (mRetryPolicy == null) {
            mRetryPolicy = new RetryPolicy();
        }
        return mRetryPolicy;
    }

    public DownloadRequest setRetryPolicy(RetryPolicy mRetryPolicy) {
        this.mRetryPolicy = mRetryPolicy;
        return this;
    }

    /**
     * Gets the download id.
     *
     * @return the download id
     */
    public final int getDownloadId() {
        return mDownloadId;
    }

    /**
     * Sets the download Id of this request.  Used by {@link DownloadQueue}.
     */
    final void setDownloadId(int downloadId) {
        mDownloadId = downloadId;
    }

    public int getDownloadState() {
        return mDownloadState;
    }

    public void setDownloadState(int mDownloadState) {
        this.mDownloadState = mDownloadState;
    }

    /**
     * Gets the status listener. For internal use.
     *
     * @return the status listener
     */
    public DownloadListener getDownloadListener() {
        return mDownloadListener;
    }

    /**
     * Sets the status listener for this download request. DownloadManager manager sends progress,
     * failure and completion updates to this listener for this download request.
     *
     * @param downloadListener the status listener for this download
     */
    public DownloadRequest setDownloadListener(DownloadListener downloadListener) {
        mDownloadListener = downloadListener;
        return this;
    }

    public String getUrl() {
        return mUrl;
    }

    public DownloadRequest setUrl(String url) {
        this.mUrl = url;
        return this;
    }

    public String getFilePath() {
        return filePath;
    }

    public DownloadRequest setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public Object getContainer() {
        return container;
    }

    public DownloadRequest setContainer(Object container) {
        this.container = container;
        return this;
    }

    public boolean getDeleteDestinationFileOnFailure() {
        return mDeleteDestinationFileOnFailure;
    }

    /**
     * Set if destination file should be deleted on download failure.
     * Use is optional: default is to delete.
     */
    public DownloadRequest setDeleteDestinationFileOnFailure(boolean deleteOnFailure) {
        this.mDeleteDestinationFileOnFailure = deleteOnFailure;
        return this;
    }

    /**
     * Mark this request as canceled.  No callback will be delivered.
     */
    public void cancel() {
        mCancelled = true;
    }

    //Package-private methods.

    /**
     * Returns true if this request has been canceled.
     */
    public boolean isCancelled() {
        return mCancelled;
    }

    /**
     * Returns all custom headers set by user
     *
     * @return
     */
    HashMap<String, String> getCustomHeaders() {
        return mCustomHeader;
    }

    @Override
    public int compareTo(DownloadRequest other) {
        Priority left = this.getPriority();
        Priority right = other.getPriority();
        // High-priority requests are "lesser" so they are sorted to the front.
        // Equal priorities are sorted by sequence number to provide FIFO ordering.
        return left == right ?
                this.mDownloadId - other.mDownloadId :
                right.ordinal() - left.ordinal();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
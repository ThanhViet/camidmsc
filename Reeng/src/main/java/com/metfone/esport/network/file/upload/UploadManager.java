package com.metfone.esport.network.file.upload;

public class UploadManager {
    private UploadQueue mRequestQueue;

    /**
     * Default constructor
     */
    public UploadManager() {
        mRequestQueue = new UploadQueue();
    }

    /**
     * Construct with provided callback handler
     *
     * @param
     */
    /*public UploadManager(Handler callbackHandler) throws InvalidParameterException {
        mRequestQueue = new UploadRequestQueue(callbackHandler);
        mRequestQueue.start();
    }

    public UploadManager(int threadPoolSize) {
        mRequestQueue = new UploadRequestQueue(threadPoolSize);
        mRequestQueue.start();
    }*/
    public void add(UploadRequest request) throws IllegalArgumentException {
        if (request == null) {
            throw new IllegalArgumentException("UploadRequest cannot be null");
        }
        mRequestQueue.addRequest(request);
    }

    public void cancel(int uploadId) {
        mRequestQueue.cancelRequest(uploadId);
    }

    public void cancelAll() {
        mRequestQueue.cancelAll();
    }
}
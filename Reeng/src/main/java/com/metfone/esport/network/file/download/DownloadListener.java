package com.metfone.esport.network.file.download;

public interface DownloadListener {
    void onDownloadStarted(DownloadRequest request);

    void onDownloadComplete(DownloadRequest downloadRequest);

    void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage);

    void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress);
}

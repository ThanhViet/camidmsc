package com.metfone.esport.network.donate;

import retrofit2.Response;

public interface DonateESportApiCallback<T> {
    void onResponse(Response<T> response);

    void onError(Throwable error);
}
package com.metfone.esport.network.donate;

import com.metfone.esport.network.donate.response.WsDonateGiftPackageResponse;
import com.metfone.esport.network.donate.response.WsGetAllDonatePackageAppResponse;
import com.metfone.esport.network.donate.response.WsUpdateChannelInformationResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.BaseDataRequestMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.HistoryRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.HistoryResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * MetfonePlus(selfcare) apis
 */
public interface DonateESportService {

    // 1. wsGetAllDonatePackageApp
//    @Headers({"Content-Type: application/json",
//    })
    @POST("UserRouting")
    Call<WsGetAllDonatePackageAppResponse> wsGetAllDonatePackageApp(@Body RequestBody requestBody);

    // 2. wsDonateGiftPackage
    @POST("UserRouting")
    Call<WsDonateGiftPackageResponse> wsDonateGiftPackage(@Body RequestBody requestBody);

    // 2. wsDonateGiftPackage
    @POST("UserRouting")
    Call<WsUpdateChannelInformationResponse> wsUpdateChannelInformation(@Body RequestBody requestBody);

}
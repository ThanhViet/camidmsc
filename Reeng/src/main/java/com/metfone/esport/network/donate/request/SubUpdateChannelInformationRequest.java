package com.metfone.esport.network.donate.request;

import com.google.gson.annotations.SerializedName;

public class SubUpdateChannelInformationRequest {
        @SerializedName("id")
        private String id = "";
        @SerializedName("channelName")
        private String channelName = "";
        @SerializedName("imageUrl")
        private String imageUrl = "";
        @SerializedName("camId")
        private String camId = "";
        @SerializedName("msisdn")
        private String msisdn = "";
        @SerializedName("status")
        private String status = "";
        @SerializedName("bannerUrl")
        private String bannerUrl = "";
        @SerializedName("createdDate")
        private String createdDate = "";
        @SerializedName("emoneyAccount")
        private String emoneyAccount = "";
        @SerializedName("commentKh")
        private String commentKh = "";
        @SerializedName("comment")
        private String comment = "";

        public SubUpdateChannelInformationRequest() {
        }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCamId() {
        return camId;
    }

    public void setCamId(String camId) {
        this.camId = camId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getEmoneyAccount() {
        return emoneyAccount;
    }

    public void setEmoneyAccount(String emoneyAccount) {
        this.emoneyAccount = emoneyAccount;
    }

    public String getCommentKh() {
        return commentKh;
    }

    public void setCommentKh(String commentKh) {
        this.commentKh = commentKh;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

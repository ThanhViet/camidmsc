package com.metfone.esport.network.donate;

import android.util.Log;

import com.metfone.esport.network.donate.request.SubDonateGiftPackageRequest;
import com.metfone.esport.network.donate.request.SubUpdateChannelInformationRequest;
import com.metfone.esport.network.donate.request.WsDonateGiftPackageRequest;
import com.metfone.esport.network.donate.request.WsGetAllDonatePackageAppRequest;
import com.metfone.esport.network.donate.request.WsUpdateChannelInformationRequest;
import com.metfone.esport.network.donate.response.WsDonateGiftPackageResponse;
import com.metfone.esport.network.donate.response.WsGetAllDonatePackageAppResponse;
import com.metfone.esport.network.donate.response.WsUpdateChannelInformationResponse;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.helper.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DonateESportClient {

    private static final String TAG = DonateESportClient.class.getSimpleName();
//    private static final String ENDPOINT = "http://36.37.242.104:8123/ApiGateway/CoreService/";
//    private static final String ENDPOINT_DEV = "http://36.37.242.104:8123/ApiGateway/CoreService/";
    private static final String ENDPOINT = "https://apigw.camid.app/CoreService/";
    private static final String ENDPOINT_DEV = "https://apigw.camid.app/CoreService/";

    public static CamIdUserBusiness mCamIdUserBusiness = null;
    public static String LANGUAGE = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
    private static Retrofit mRetrofit = null;
    private static DonateESportService mDonateESportService = null;
    private static String WS_SESSION_ID = "";
    private static String WS_TOKEN = "";
    private static DonateESportClient instance = null;

    public static DonateESportClient getInstance() {
        if (instance == null) {
            instance = new DonateESportClient();
        }
        return instance;
    }

    private static Retrofit getRetrofit() {
        if (mRetrofit == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                client.interceptors().add(logging);
            }

            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.DEBUG ? ENDPOINT_DEV : ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofit;
    }


    public static DonateESportService getDonateESportService() {
        if (mDonateESportService == null) {
            mDonateESportService = getRetrofit().create(DonateESportService.class);
        }
        return mDonateESportService;
    }


    /**
     * Create base request default
     *
     * @param t      object request extend BaseRequest
     * @param wsCode wsCode of request {@link Constants.WSCODE}
     * @param <T>    object type
     * @return BaseRequest has sessionId, wsCode, token, language
     */
    public static <T extends BaseRequest<?>> T createBaseRequest(T t, String wsCode) {
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        }
        WS_SESSION_ID = mCamIdUserBusiness.getMetfoneSessionId();
        WS_TOKEN = mCamIdUserBusiness.getMetfoneToken();
        t.setSessionId(WS_SESSION_ID);
        t.setToken(WS_TOKEN);
        t.setLanguage(LANGUAGE);
        t.setWsCode(wsCode);
        return t;
    }

    private void showResponseError(int code) {
        if (code == 401) {
            if (ApplicationController.self().getCurrentActivity() != null) {
                ApplicationController.self().getCurrentActivity().restartApp();
            }
            Log.e(TAG, "onResponse - unauthenticated: " + code);
        } else if (code >= 400 && code < 500) {
            Log.e(TAG, "onResponse - clientError:" + code);
        } else if (code >= 500 && code < 600) {
            Log.e(TAG, "onResponse - serverError:" + code);
        } else {
            Log.e(TAG, "onResponse - RuntimeException - unexpectedError:" + code);
        }
    }

    private void showFailure(Throwable t) {
        if (t instanceof IOException) {
            Log.e(TAG, "showFailure: Network error", t);
        } else {
            Log.e(TAG, "onFailure: UnexpectedError", t);
        }
    }


    public void wsGetAllDonatePackageApp(DonateESportApiCallback<WsGetAllDonatePackageAppResponse> callback) {
        WsGetAllDonatePackageAppRequest request = createBaseRequest(new WsGetAllDonatePackageAppRequest(), Constants.WSCODE.WS_GET_ALL_DONATE_PACKAGE_APP);
        WsGetAllDonatePackageAppRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        request.setWsRequest(subRequest);
        getDonateESportService()
                .wsGetAllDonatePackageApp(request.createRequestBody()).enqueue(new Callback<WsGetAllDonatePackageAppResponse>() {
            @Override
            public void onResponse(Call<WsGetAllDonatePackageAppResponse> call, Response<WsGetAllDonatePackageAppResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetAllDonatePackageAppResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsDonateGiftPackage(SubDonateGiftPackageRequest param, DonateESportApiCallback<WsDonateGiftPackageResponse> callback) {
        WsDonateGiftPackageRequest request = createBaseRequest(new WsDonateGiftPackageRequest(), Constants.WSCODE.WS_DONATE_GIFT_PACKAGE);
        param.setLanguage(LANGUAGE);
        request.setWsRequest(param);
        getDonateESportService()
                .wsDonateGiftPackage(request.createRequestBody()).enqueue(new Callback<WsDonateGiftPackageResponse>() {
            @Override
            public void onResponse(Call<WsDonateGiftPackageResponse> call, Response<WsDonateGiftPackageResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsDonateGiftPackageResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }


    public void wsUpdateChannelInformation(SubUpdateChannelInformationRequest param, DonateESportApiCallback<WsUpdateChannelInformationResponse> callback) {
        WsUpdateChannelInformationRequest request = createBaseRequest(new WsUpdateChannelInformationRequest(), Constants.WSCODE.WS_UPDATE_CHANNEL_INFORMATION);
        request.setWsRequest(param);
        getDonateESportService()
                .wsUpdateChannelInformation(request.createRequestBody()).enqueue(new Callback<WsUpdateChannelInformationResponse>() {
            @Override
            public void onResponse(Call<WsUpdateChannelInformationResponse> call, Response<WsUpdateChannelInformationResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsUpdateChannelInformationResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

}
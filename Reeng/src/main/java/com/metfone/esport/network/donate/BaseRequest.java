package com.metfone.esport.network.donate;

import com.google.gson.annotations.SerializedName;

public class BaseRequest<T> implements GenerateRequestBody {
    @SerializedName("sessionId")
    private String sessionId;

    @SerializedName("wsCode")
    private String wsCode;

    @SerializedName("token")
    private String token;

    @SerializedName("username")
    private String username = null;

    @SerializedName("apiKey")
    private String apiKey = "";

    @SerializedName("language")
    private String language;

    @SerializedName("wsRequest")
    private T wsRequest;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getWsCode() {
        return wsCode;
    }

    public void setWsCode(String wsCode) {
        this.wsCode = wsCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public T getWsRequest() {
        return wsRequest;
    }

    public void setWsRequest(T wsRequest) {
        this.wsRequest = wsRequest;
    }
}
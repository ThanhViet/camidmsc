package com.metfone.esport.network.donate.response;

import com.google.firebase.encoders.annotations.Encodable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.reactivex.annotations.Nullable;


public class DonateGiftPackageModel implements Serializable {
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("thankMessage")
    private String thankMessage;

    @Expose
    @SerializedName("remainTime")
    private String remainTime;


    public String getRemainTime() {
        return remainTime;
    }

    public void setRemainTime(String remainTime) {
        this.remainTime = remainTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThankMessage() {
        return thankMessage;
    }

    public void setThankMessage(String thankMessage) {
        this.thankMessage = thankMessage;
    }
}
package com.metfone.esport.network.file;

public class RetryPolicy {
    /**
     * The default number of retries
     */
    public static final int DEFAULT_MAX_RETRIES = 2;//3 lần(2 retry 1 lần đầu)
    /**
     * The default backoff multiplier
     */
    public static final float DEFAULT_BACKOFF_MULT = 1f;
    /**
     * The maximum number of attempts.
     */
    private final int mMaxNumRetries;

    /**
     * The backoff multiplier for for the policy.
     */
    private final float mBackoffMultiplier;
    /**
     * The current timeout in milliseconds.
     */
    private int mCurrentTimeoutMs;
    /**
     * The current retry count.
     */
    private int mCurrentRetryCount;

    /**
     * Constructs a new retry policy using the default timeouts.
     */
    public RetryPolicy() {
        this(Constants.DEFAULT_TIMEOUT, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT);
    }

    /**
     * Constructs a new retry policy.
     *
     * @param initialTimeoutMs  The initial timeout for the policy.
     * @param maxNumRetries     The maximum number of retries.
     * @param backoffMultiplier Backoff multiplier for the policy.
     */
    public RetryPolicy(int initialTimeoutMs, int maxNumRetries, float backoffMultiplier) {
        mCurrentTimeoutMs = initialTimeoutMs;
        mMaxNumRetries = maxNumRetries;
        mBackoffMultiplier = backoffMultiplier;
        mCurrentRetryCount = 0;
    }

    public float getBackOffMultiplier() {
        return mBackoffMultiplier;
    }

    public int getCurrentTimeout() {
        return mCurrentTimeoutMs;
    }

    public int getCurrentRetryCount() {
        return mCurrentRetryCount;
    }

    public void retry() throws RetryException {
        mCurrentRetryCount++;
        mCurrentTimeoutMs += (mCurrentTimeoutMs * mBackoffMultiplier);
        if (!hasAttemptRemaining()) {
            throw new RetryException();
        }
    }

    /**
     * Returns true if this policy has attempts remaining, false otherwise.
     */
    protected boolean hasAttemptRemaining() {
        return mCurrentRetryCount <= mMaxNumRetries;
    }
}
package com.metfone.esport.network.file.upload;

import com.metfone.esport.entity.repsonse.CreateVideoResponse;

public interface UploadListener {
    void onUploadStarted(UploadRequest uploadRequest);

    void onUploadComplete(UploadRequest uploadRequest, String response);

    void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage);

    void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress, long speed);

    void onCreateVideoSuccess(UploadRequest uploadRequest, CreateVideoResponse response);

    void onCreateVideoFailed(UploadRequest uploadRequest, String error);
}

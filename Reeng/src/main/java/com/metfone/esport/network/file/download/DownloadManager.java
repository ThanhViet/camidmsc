package com.metfone.esport.network.file.download;

public class DownloadManager {
    private DownloadQueue mRequestQueue;

    /**
     * Default constructor
     */
    public DownloadManager() {
        mRequestQueue = new DownloadQueue();
        // mRequestQueue.start();
    }

    /**
     * Construct with provided callback handler
     *
     * @param
     *//*
    public DownloadManager(App application,Handler callbackHandler) throws InvalidParameterException {
        mRequestQueue = new DownloadRequestQueue(callbackHandler);
        mRequestQueue.start();
    }

    public DownloadManager(App application,int threadPoolSize) {
        mRequestQueue = new DownloadRequestQueue(threadPoolSize);
        mRequestQueue.start();
    }*/
    public void add(DownloadRequest request) throws IllegalArgumentException {
        if (request == null) {
            throw new IllegalArgumentException("DownloadRequest cannot be null");
        }
        mRequestQueue.addRequest(request);
    }

   /* public int cancel(int downloadId) {
        return mRequestQueue.cancel(downloadId);
    }*/

    public void cancelAll() {
        mRequestQueue.cancelAll();
    }

    /*public int query(int downloadId) {
        return mRequestQueue.query(downloadId);
    }

    public void release() {
        if (mRequestQueue != null) {
            mRequestQueue.release();
            mRequestQueue = null;
        }
    }*/
}
package com.metfone.esport.network.donate.request;

import com.google.gson.annotations.SerializedName;

public class SubDonateGiftPackageRequest {
    @SerializedName("comment")
    private String comment = "";
    @SerializedName("donatePackageId")
    private String donatePackageId = "";
    @SerializedName("camId")
    private String camId = "";
    @SerializedName("customerName")
    private String customerName = "";
    @SerializedName("phoneNumber")
    private String phoneNumber = "";
    @SerializedName("channelId")
    private String channelId = "";
    @SerializedName("channelName")
    private String channelName = "";
    @SerializedName("paymentMethod")
    private String paymentMethod = "";
    @SerializedName("otp")
    private String otp = "";
    @SerializedName("language")
    private String language = "";

    public SubDonateGiftPackageRequest() {
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDonatePackageId() {
        return donatePackageId;
    }

    public void setDonatePackageId(String donatePackageId) {
        this.donatePackageId = donatePackageId;
    }

    public String getCamId() {
        return camId;
    }

    public void setCamId(String camId) {
        this.camId = camId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        return "Request{" +
                "comment='" + comment + '\'' +
                ", donatePackageId='" + donatePackageId + '\'' +
                ", camId='" + camId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", channelId='" + channelId + '\'' +
                ", channelName='" + channelName + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", otp='" + otp + '\'' +
                '}';
    }

}

package com.metfone.esport.network.donate.response;

import com.metfone.esport.network.donate.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class WsGetAllDonatePackageAppResponse extends BaseResponse<List<DonatePackageModel>> implements Serializable {}
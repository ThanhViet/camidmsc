package com.metfone.esport.network.donate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BaseResponse<T> {
    public static final String SUCCESS = "S200";
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("result")
    private ResultResponse<T> result;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccessful() {
        return SUCCESS.equals(errorCode);
    }

    public ResultResponse<T> getResult() {
        return result;
    }
    public void setResult(ResultResponse<T> result) {
        this.result = result;
    }
}
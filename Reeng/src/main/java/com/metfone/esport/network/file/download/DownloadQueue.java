package com.metfone.esport.network.file.download;

import android.os.Handler;
import android.os.Looper;
import android.os.Process;

import com.metfone.esport.util.Log;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public class DownloadQueue {
    private static final String TAG = DownloadQueue.class.getSimpleName();
    private static final int QUEUE_SIZE = 1000;
    private final BlockingQueue<DownloadRequest> mQueue;
    private final Set<DownloadRequest> mCurrentRequests = new HashSet<>();
    private Thread threadQueue;
    private CallBackDelivery mDelivery;
    private DownloadHandler downloadHandler;
    private AtomicInteger mSequenceGenerator = new AtomicInteger();
    private boolean done;

    public DownloadQueue() {
        this.mQueue = new ArrayBlockingQueue<>(QUEUE_SIZE, true);
        init();
    }

    protected void init() {
        done = false;
        mDelivery = new CallBackDelivery(new Handler(Looper.getMainLooper()));
        downloadHandler = new DownloadHandler(mDelivery);
        threadQueue = new Thread() {
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                processQueue(this);
            }
        };
        threadQueue.setName("Download queue");
        threadQueue.setDaemon(true);
        startup();
    }

    /**
     * start queue
     */
    public void startup() {
        if (threadQueue.isAlive()) {
            Log.i(TAG, "Return by writeThread existed");
            return;
        }
        threadQueue.start();
    }

    public void shutdown() {
        done = true;
        synchronized (mQueue) {
            mQueue.notifyAll();
        }
        threadQueue = null;
        mQueue.clear();
    }

    public void cancelRequest(DownloadRequest request) {
        synchronized (mCurrentRequests) {
            for (DownloadRequest item : mCurrentRequests) {
                if (item == request) {
                    request.cancel();
                    break;
                }
            }
            mCurrentRequests.remove(request);
        }
    }

    public void cancelAll() {
        synchronized (mCurrentRequests) {
            for (DownloadRequest request : mCurrentRequests) {
                request.cancel();
            }
            mCurrentRequests.clear();
        }
    }

    public void addRequest(DownloadRequest request) {
        if (request == null) {
            return;
        }
        if (!done) {
            try {
                request.setDownloadId(getDownloadId());
                synchronized (mCurrentRequests) {
                    mCurrentRequests.add(request);
                }
                synchronized (mQueue) {
                    mQueue.put(request);
                    mQueue.notifyAll();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private DownloadRequest nextRequest() {
        DownloadRequest request = null;
        // Wait until there's a packet or we're done.
        while (!done && (request = mQueue.poll()) == null) {
            try {
                synchronized (mQueue) {
                    mQueue.wait();
                }
            } catch (InterruptedException ie) {
                // Do nothing
            }
        }
        return request;
    }

    private void processQueue(Thread thisThread) {
        try {
            while (!done && (threadQueue != null && threadQueue == thisThread)) {
                DownloadRequest request = nextRequest();
                if (request != null) {
                    synchronized (downloadHandler) {
                        downloadHandler.download(request);
                    }
                }
            }
            Log.i(TAG, "download thread is died");
            // by the shutdown process.
            mQueue.clear();
            // Close the stream.
        } catch (Exception ioe) {
            Log.e(TAG, "Exception", ioe);
        }
    }

    private int getDownloadId() {
        return mSequenceGenerator.incrementAndGet();
    }

    class CallBackDelivery {

        /**
         * Used for posting responses, typically to the main thread.
         */
        private final Executor mCallBackExecutor;

        /**
         * Constructor taking a handler to main thread.
         */
        public CallBackDelivery(final Handler handler) {
            // Make an Executor that just wraps the handler.
            mCallBackExecutor = new Executor() {
                @Override
                public void execute(Runnable command) {
                    handler.post(command);
                }
            };
        }

        public void postDownloadStarted(final DownloadRequest request) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    if (request.getDownloadListener() != null) {
                        request.getDownloadListener().onDownloadStarted(request);
                    }
                }
            });
        }

        public void postDownloadComplete(final DownloadRequest request) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    synchronized (mCurrentRequests) {
                        mCurrentRequests.remove(request);
                    }
                    if (request.getDownloadListener() != null) {
                        request.getDownloadListener().onDownloadComplete(request);
                    }
                }
            });
        }

        public void postDownloadFailed(final DownloadRequest request, final int errorCode, final String errorMsg) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    synchronized (mCurrentRequests) {
                        mCurrentRequests.remove(request);
                    }
                    if (request.getDownloadListener() != null) {
                        request.getDownloadListener().onDownloadFailed(request, errorCode, errorMsg);
                    }
                }
            });
        }

        public void postProgressUpdate(final DownloadRequest request, final long totalBytes, final long
                downloadedBytes, final int progress) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    if (request.getDownloadListener() != null) {
                        request.getDownloadListener().onProgress(request, totalBytes, downloadedBytes, progress);
                    }
                }
            });
        }
    }
}
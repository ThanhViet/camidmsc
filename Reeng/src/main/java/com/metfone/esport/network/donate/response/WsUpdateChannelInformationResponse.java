package com.metfone.esport.network.donate.response;

import com.metfone.esport.network.donate.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class WsUpdateChannelInformationResponse extends BaseResponse<Object> implements Serializable {}
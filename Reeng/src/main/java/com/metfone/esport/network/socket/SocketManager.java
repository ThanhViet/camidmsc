package com.metfone.esport.network.socket;

import android.annotation.SuppressLint;

import com.blankj.utilcode.util.DeviceUtils;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.SocketModel;
import com.metfone.esport.entity.repsonse.ListCommentEntity;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.helper.ConvertHelper;
import com.metfone.esport.listener.PostMessageSocketListener;
import com.metfone.esport.util.Log;
import com.metfone.esport.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.CompletableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.StompHeader;
import ua.naiksoftware.stomp.dto.StompMessage;

public class SocketManager {
    private static final String TAG = "SocketManager";
    private final String PATH_TOPIC = "/user/chatlive/";
    private final String SEND_MESSAGE = "/app/chatlive/postmessage";
    StompClient webSocketClient;
    CompositeDisposable compositeDisposable;

    private SocketManager() {
    }

    public static SocketManager getInstance() {
        return SingletonHelper.INSTANCE;
    }

    @SuppressLint("CheckResult")
    public void connectWebSocket(String url, final String roomID) {
        Log.e(TAG, "connectWebSocket roomID: " + roomID + "\turl: " + url);
        try {
            webSocketClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, url);
            ArrayList<StompHeader> listHeader = new ArrayList();
            listHeader.add(new StompHeader("user", String.valueOf(AccountBusiness.getInstance().getUserId())));
            listHeader.add(new StompHeader("user_id", String.valueOf(AccountBusiness.getInstance().getUserId())));
            listHeader.add(new StompHeader("clientType", Constant.CLIENT_TYPE));
            listHeader.add(new StompHeader("revision", Constant.REVISION));
            listHeader.add(new StompHeader("deviceId", DeviceUtils.getUniqueDeviceId()));
            webSocketClient.withClientHeartbeat(1000).withServerHeartbeat(1000);
            resetSubscriptions();
            Disposable dispLifecycle = webSocketClient.lifecycle()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(lifecycleEvent -> {
                        switch (lifecycleEvent.getType()) {
                            case OPENED: {
                                Log.d(TAG, "Stomp connection open");
                                break;
                            }
                            case ERROR: {
                                Log.e(TAG, "Stomp connection error", lifecycleEvent.getException());
                                break;
                            }
                            case FAILED_SERVER_HEARTBEAT: {
                                Log.d(TAG, "Stomp failed server heartbeat");
                                break;
                            }
                            case CLOSED: {
                                Log.d(TAG, "Stomp connection closed");
                                break;
                            }
                            default: {
                                Log.d(TAG, "Stomp " + lifecycleEvent.getType());
                            }
                        }
                    });

            compositeDisposable.add(dispLifecycle);

            // Receive greetings
            String topic = PATH_TOPIC + roomID;
            Disposable disposableTopic = webSocketClient.topic(topic)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(stompMessage -> {
                        Log.d(TAG, "Received " + stompMessage);
                        onReceiveMessage(stompMessage);
                    }, throwable -> Log.e(TAG, "Error on subscribe topic: " + topic, throwable));
            compositeDisposable.add(disposableTopic);
            webSocketClient.connect(listHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onReceiveMessage(StompMessage message) {
        try {
            String payload = message.getPayload();
            JSONObject jso = new JSONObject(payload);
            int type = jso.optInt("type");
            long timestamp = jso.optLong("timestamp");
            long numberLive = jso.optLong("numberLive");
            String userId = jso.optString("user_id");
            String roomID = jso.optString("roomId");
            JSONObject chatMessage = jso.optJSONObject("chatMessage");
            JSONArray messageList = jso.optJSONArray("messageList");
            SocketModel model = new SocketModel();
            model.type = type;
            model.timestamp = timestamp;
            model.numberLive = numberLive;
            model.userId = userId;
            model.roomId = roomID;
            model.chatMessage = ConvertHelper.convertToLiveMessage(chatMessage);
            if (Utilities.notEmpty(messageList)) {
                ArrayList<ListCommentEntity> listComment = new ArrayList<>();
                for (int i = 0; i < messageList.length(); i++) {
                    ListCommentEntity item = ConvertHelper.convertToLiveMessage(messageList.getJSONObject(i));
                    if (item != null) listComment.add(item);
                }
                model.messageList = listComment;
            }
            EventBus.getDefault().postSticky(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetSubscriptions() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
        compositeDisposable = new CompositeDisposable();
    }

    public void sendMessage(String data, PostMessageSocketListener listener) {
        if (compositeDisposable == null) compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(webSocketClient.send(SEND_MESSAGE, data)
                .compose(applySchedulers())
                .subscribe(() -> {
                    Log.d(TAG, "STOMP echo send successfully");
                    if (listener != null) listener.onPostMessageSuccess();
                }, throwable -> {
                    Log.e(TAG, "Error send STOMP echo", throwable);
                    if (listener != null) listener.onPostMessageFailure();
                }));
    }

    private CompletableTransformer applySchedulers() {
        return upstream -> upstream.unsubscribeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void sendMessage(String message) {
        if (webSocketClient != null)
            webSocketClient.send(message);
    }

    public StompClient getWebSocketClient() {
        return webSocketClient;
    }

    public void disconnect() {
        if (webSocketClient != null)
            webSocketClient.disconnect();
    }

    public boolean isConnected() {
        if (webSocketClient != null)
            return webSocketClient.isConnected();
        return false;
    }

    private static class SingletonHelper {
        private static final SocketManager INSTANCE = new SocketManager();
    }
}

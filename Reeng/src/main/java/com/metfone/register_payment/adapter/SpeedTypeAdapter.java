package com.metfone.register_payment.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.register_payment.model.PackageInfoSpeed;
import com.metfone.selfcare.R;

import java.util.ArrayList;

public class SpeedTypeAdapter extends ArrayAdapter<PackageInfoSpeed> {

    int packageMonth = 0;

    public SpeedTypeAdapter(Context context, ArrayList<PackageInfoSpeed> type, int packageMonth) {
        super(context, R.layout.item_spinner_speed_type, type);
        this.packageMonth = packageMonth;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_spinner_speed_type, parent, false
            );
        }

        TextView tvSelect = convertView.findViewById(R.id.tvSelect);
        ImageView ivLineSpinner = convertView.findViewById(R.id.ivLineSpinner);
        PackageInfoSpeed speedType = getItem(position);
        if (speedType != null) {
            if (speedType.speed == 0) tvSelect.setText("Select speed local");
            else tvSelect.setText(speedType.speed + " Mbps speed local");
        }
        Log.d("Phan Tien", "initView: " + packageMonth);
        if (packageMonth != 0) {
            if (packageMonth == 3) {
                tvSelect.setTextColor(Color.rgb(216, 15, 44));
                ivLineSpinner.setBackgroundResource(R.color.color_pay_3);
            } else if (packageMonth == 5) {
                tvSelect.setTextColor(Color.rgb(97, 121, 211));
                ivLineSpinner.setBackgroundResource(R.color.color_pay_5);
            } else if (packageMonth == 10) {
                tvSelect.setTextColor(Color.rgb(243, 112, 33));
                ivLineSpinner.setBackgroundResource(R.color.color_pay_10);
            }
        } else tvSelect.setTextColor(Color.rgb(0, 0, 0));

        return convertView;
    }
}

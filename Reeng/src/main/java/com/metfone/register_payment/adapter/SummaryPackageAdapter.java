package com.metfone.register_payment.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.register_payment.model.SummaryPackage;
import com.metfone.selfcare.R;

import java.util.ArrayList;

public class SummaryPackageAdapter extends RecyclerView.Adapter<SummaryPackageAdapter.RecyclerViewHolder> {

    private OnItemClickListener onItemClick;
    Context context;
    ArrayList<SummaryPackage> arrayList;

//    public void setOnClickItem(OnItemClickListener onItemClick) {
//        this.onItemClick = onItemClick;
//    }

    public SummaryPackageAdapter(Context context, ArrayList<SummaryPackage> arrayList, OnItemClickListener onItemClick) {
        this.context = context;
        this.arrayList = arrayList;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(context).inflate(R.layout.item_summary_package, parent, false));
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        SummaryPackage entity = arrayList.get(position);
        Log.d("Phan Tien", "onBindViewHolder: " + entity.packageMonth + "\n" + entity.title + "\n" + entity.description);
        holder.itemView.setOnClickListener(v -> {
            notifyDataSetChanged();
            if (onItemClick != null) {
                onItemClick.onItemClick(entity);
            }
        });

        switch (entity.packageMonth) {
            case "3":
                holder.clItemPackage.setBackgroundResource(R.drawable.bg_border_gradient_item_1);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.tvItemPackageTitle.setTextColor(context.getColor(R.color.color_pay_3));
                }
                holder.ivShowMore.setBackgroundResource(R.drawable.ic_dropdown_3);
                break;
            case "5":
                holder.clItemPackage.setBackgroundResource(R.drawable.bg_border_gradient_item_2);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.tvItemPackageTitle.setTextColor(context.getColor(R.color.color_pay_5));
                }
                holder.ivShowMore.setBackgroundResource(R.drawable.ic_dropdown_5);
                break;
            case "10":
                holder.clItemPackage.setBackgroundResource(R.drawable.bg_border_gradient_item_3);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.tvItemPackageTitle.setTextColor(context.getColor(R.color.color_pay_10));
                }
                holder.ivShowMore.setBackgroundResource(R.drawable.ic_dropdown_10);
                break;
        }

        holder.tvItemPackageTitle.setText(entity.title);
        holder.tvItemPackageDescription.setText(entity.description);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout clItemPackage;
        TextView tvItemPackageTitle;
        TextView tvItemPackageDescription;
        ImageView ivShowMore;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            clItemPackage = itemView.findViewById(R.id.clItemPackage);
            tvItemPackageTitle = itemView.findViewById(R.id.tvItemPackageTitle);
            tvItemPackageDescription = itemView.findViewById(R.id.tvItemPackageDescription);
            ivShowMore = itemView.findViewById(R.id.ivShowMore);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(SummaryPackage item);
    }
}

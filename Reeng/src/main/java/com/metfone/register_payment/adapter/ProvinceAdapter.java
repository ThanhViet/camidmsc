package com.metfone.register_payment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.register_payment.model.Province;
import com.metfone.selfcare.R;

import java.util.ArrayList;

public class ProvinceAdapter extends ArrayAdapter<Province> {

    public ProvinceAdapter(Context context, ArrayList<Province> provinceList) {
        super(context, R.layout.item_spinner_dropdown, provinceList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_spinner_dropdown, parent, false
            );
        }

        TextView tvSelect = convertView.findViewById(R.id.tvSelectProvince);
        Province province = getItem(position);

        if (province != null) {
            String content = getContext().getResources().getString(R.string.register_province_city);
            String text = province.getProvinceCode();
            if (text.equals("0")) {
                tvSelect.setText(content);
            } else tvSelect.setText(province.getProvinceName());
        }

        return convertView;
    }
}

package com.metfone.register_payment.util;

enum TypePayment {
    BASIC,
    FAMILY,
    BUSINESS
}

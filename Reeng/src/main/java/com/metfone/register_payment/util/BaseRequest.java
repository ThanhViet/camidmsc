//package com.metfone.register_payment.util;
//
//import com.google.gson.annotations.SerializedName;
//
//public class BaseRequest<T> {
//
//    @SerializedName("wsCode")
//    private String wsCode = "string";
//
//    @SerializedName("wsRequest")
//    private T wsRequest = null;
//
//    public String getWsCode() {
//        return wsCode;
//    }
//
//    public void setWsCode(String wsCode) {
//        this.wsCode = wsCode;
//    }
//
//    public T getWsRequest() {
//        return wsRequest;
//    }
//
//    public void setWsRequest(T wsRequest) {
//        this.wsRequest = wsRequest;
//    }
//
//    @Override
//    public String toString() {
//        return "BaseRequest{" +
//                ", wsCode='" + wsCode + '\'' +
//                ", wsRequest=" + wsRequest +
//                '}';
//    }
//}

package com.metfone.register_payment.util;

public interface OnItemClickListener {
    void onItemClick(Object item);
}

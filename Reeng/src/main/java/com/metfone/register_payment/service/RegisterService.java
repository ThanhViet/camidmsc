package com.metfone.register_payment.service;

import com.metfone.register_payment.network.response.CommuneResponse;
import com.metfone.register_payment.network.response.DistrictResponse;
import com.metfone.register_payment.network.response.InternetRegisterResponse;
import com.metfone.register_payment.network.response.PackageInfoResponse;
import com.metfone.register_payment.network.response.ProvinceResponse;
import com.metfone.register_payment.network.response.SummaryPackageResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RegisterService {

    @POST("UserRouting")
    Call<SummaryPackageResponse> getSummaryPackage(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<PackageInfoResponse> getPackageInfo(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<InternetRegisterResponse> camIdInternetRegister(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<ProvinceResponse> getListProvince(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<DistrictResponse> getListDistrict(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<CommuneResponse> getListCommune(@Body RequestBody requestBody);


}

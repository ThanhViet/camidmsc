package com.metfone.register_payment.service;

import android.util.Log;

import com.metfone.register_payment.network.request.CommuneRequest;
import com.metfone.register_payment.network.request.DistrictRequest;
import com.metfone.register_payment.network.request.InternetRegisterRequest;
import com.metfone.register_payment.network.request.PackageInfoRequest;
import com.metfone.register_payment.network.request.ProvinceRequest;
import com.metfone.register_payment.network.request.SummaryPackageRequest;
import com.metfone.register_payment.network.response.CommuneResponse;
import com.metfone.register_payment.network.response.DistrictResponse;
import com.metfone.register_payment.network.response.InternetRegisterResponse;
import com.metfone.register_payment.network.response.PackageInfoResponse;
import com.metfone.register_payment.network.response.ProvinceResponse;
import com.metfone.register_payment.network.response.SummaryPackageResponse;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String TAG = RetrofitClient.class.getSimpleName();
    private static Retrofit mRetrofit = null;
    public static CamIdUserBusiness mCamIdUserBusiness = null;
    public static RetrofitClient instance = null;
    public static RegisterService registerService = null;
    private static MetfonePlusService mMetfonePlusService = null;

    private static String WS_SESSION_ID = "";
    private static String WS_TOKEN = "";
    public static String LANGUAGE = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
    //Dev
 //   private static final String ENDPOINT = "http://36.37.242.104:8123/ApiGateway/CoreService/";
  //  private static final String ENDPOINT_DEV = "http://36.37.242.104:8123/ApiGateway/CoreService/";
    // Production
    private static final String ENDPOINT = "https://apigw.camid.app/CoreService/";
    private static final String ENDPOINT_DEV = "https://apigw.camid.app/CoreService/";

    private static String getLanguageCode() {
        return LocaleManager.getLanguage(ApplicationController.self());
    }

    private static Retrofit getRetrofit() {
        if (mRetrofit == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                client.interceptors().add(logging);
            }

            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.DEBUG ? ENDPOINT_DEV : ENDPOINT)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofit;
    }

    public static RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();
        }
        return instance;
    }

    public static RegisterService getRegisterService() {
        if (registerService == null) {
            registerService = getRetrofit().create(RegisterService.class);
        }
        return registerService;
    }

    public static MetfonePlusService getMetfonePlusService() {
        if (mMetfonePlusService == null) {
            mMetfonePlusService = getRetrofit().create(MetfonePlusService.class);
        }
        return mMetfonePlusService;
    }

    public static <T extends BaseRequest<?>> T createBaseRequest(T t, String wsCode) {
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        }
        WS_SESSION_ID = mCamIdUserBusiness.getMetfoneSessionId();
        WS_TOKEN = mCamIdUserBusiness.getMetfoneToken();
        LANGUAGE = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
        t.setSessionId(WS_SESSION_ID);
        t.setToken(WS_TOKEN);
        t.setLanguage(LANGUAGE);
        t.setWsCode(wsCode);
        return t;
    }

    private void showResponseError(int code) {
        if (code == 401) {
            if (ApplicationController.self().getCurrentActivity() != null) {
                ApplicationController.self().getCurrentActivity().restartApp();
            }
            Log.e(TAG, "onResponse - unauthenticated: " + code);
        } else if (code >= 400 && code < 500) {
            Log.e(TAG, "onResponse - clientError:" + code);
        } else if (code >= 500 && code < 600) {
            Log.e(TAG, "onResponse - serverError:" + code);
        } else {
            Log.e(TAG, "onResponse - RuntimeException - unexpectedError:" + code);
        }
    }

    private void showFailure(Throwable t) {
        if (t instanceof IOException) {
            Log.e(TAG, "showFailure: Network error", t);
        } else {
            Log.e(TAG, "onFailure: UnexpectedError", t);
        }
    }

    public void wsGetSummaryPackage(MPApiCallback<SummaryPackageResponse> callback) {
        SummaryPackageRequest request = createBaseRequest(new SummaryPackageRequest(), Constants.WSCODE.WS_GET_SUMMARY_PACKAGE);
        SummaryPackageRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        request.setWsRequest(subRequest);
        RegisterService service = getRegisterService();
        service.getSummaryPackage(request.createRequestBody()).enqueue(new Callback<SummaryPackageResponse>() {
            @Override
            public void onResponse(Call<SummaryPackageResponse> call, Response<SummaryPackageResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<SummaryPackageResponse> call, Throwable t) {
                Log.e("ERROR", "error " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetPackageInfo(String month, MPApiCallback<PackageInfoResponse> callback) {
        PackageInfoRequest request = createBaseRequest(new PackageInfoRequest(), Constants.WSCODE.WS_GET_PACKAGE_INFO);
        PackageInfoRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        subRequest.setMonth(month);
        request.setWsRequest(subRequest);
        RegisterService service = getRegisterService();
        service.getPackageInfo(request.createRequestBody()).enqueue(new Callback<PackageInfoResponse>() {
            @Override
            public void onResponse(Call<PackageInfoResponse> call, Response<PackageInfoResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<PackageInfoResponse> call, Throwable t) {
                Log.e("ERROR", "error " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetListProvince(MPApiCallback<ProvinceResponse> callback) {
        ProvinceRequest request = createBaseRequest(new ProvinceRequest(), Constants.WSCODE.WS_GET_LIST_PROVINCE);
        ProvinceRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        request.setWsRequest(subRequest);
        RegisterService service = getRegisterService();
        service.getListProvince(request.createRequestBody()).enqueue(new Callback<ProvinceResponse>() {
            @Override
            public void onResponse(Call<ProvinceResponse> call, Response<ProvinceResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<ProvinceResponse> call, Throwable t) {
                Log.e("ERROR", "error " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetListDistrict(String provinceCode, MPApiCallback<DistrictResponse> callback) {
        DistrictRequest request = createBaseRequest(new DistrictRequest(), Constants.WSCODE.WS_GET_LIST_DISTRICT);
        DistrictRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        subRequest.setProvinceCode(provinceCode);
        request.setWsRequest(subRequest);
        RegisterService service = getRegisterService();
        service.getListDistrict(request.createRequestBody()).enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                Log.e("ERROR", "error " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetListCommune(String provinceCode, String districtCode, MPApiCallback<CommuneResponse> callback) {
        CommuneRequest request = createBaseRequest(new CommuneRequest(), Constants.WSCODE.WS_GET_LIST_PRECINCT);
        CommuneRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        subRequest.setProvinceCode(provinceCode);
        subRequest.setDistrictCode(districtCode);
        request.setWsRequest(subRequest);
        RegisterService service = getRegisterService();
        service.getListCommune(request.createRequestBody()).enqueue(new Callback<CommuneResponse>() {
            @Override
            public void onResponse(Call<CommuneResponse> call, Response<CommuneResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<CommuneResponse> call, Throwable t) {
                Log.e("ERROR", "error " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsCamIdInternetRegister(String name,
                                        String phone,
                                        String email,
                                        String province,
                                        String district,
                                        String commune,
                                        String village,
                                        String packageName,
                                        String introductionCode,
                                        String speed,
                                        String type,
                                        MPApiCallback<InternetRegisterResponse> callback) {
        InternetRegisterRequest request = createBaseRequest(new InternetRegisterRequest(), Constants.WSCODE.WS_CAMID_INTERNET_REGISTER);
        InternetRegisterRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        subRequest.setName(name);
        subRequest.setPhone(phone);
        subRequest.setEmail(email);
        subRequest.setProvince(province);
        subRequest.setDistrict(district);
        subRequest.setCommune(commune);
        subRequest.setVillage(village);
        subRequest.setPackageName(packageName);
        subRequest.setIntroductionCode(introductionCode);
        subRequest.setSpeed(speed);
        subRequest.setType(type);
        request.setWsRequest(subRequest);
        RegisterService service = getRegisterService();
        service.camIdInternetRegister(request.createRequestBody()).enqueue(new Callback<InternetRegisterResponse>() {
            @Override
            public void onResponse(Call<InternetRegisterResponse> call, Response<InternetRegisterResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<InternetRegisterResponse> call, Throwable t) {
                Log.e("ERROR", "error " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

}

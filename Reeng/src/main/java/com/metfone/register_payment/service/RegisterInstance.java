package com.metfone.register_payment.service;

import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterInstance {
    public static Retrofit mRetrofitInstance;
    public static String BASE_URL = "https://apigw.camid.app/";
    RegisterService service = getInstance().create(RegisterService.class);
    public static CamIdUserBusiness mCamIdUserBusiness;

    public static Retrofit getInstance() {
        if (mRetrofitInstance == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                client.interceptors().add(logging);
            }

            mRetrofitInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build();
        }

        return mRetrofitInstance;
    }

//    public void getSummaryPackage(ApiCallback<SummaryPackageResponse> callback) {
//        Call<SummaryPackageResponse> call = service.getSummaryPackage();
//        call.enqueue(new Callback<SummaryPackageResponse>() {
//            @Override
//            public void onResponse(Call<SummaryPackageResponse> call, Response<SummaryPackageResponse> response) {
//                callback.onResponse(response);
//            }
//
//            @Override
//            public void onFailure(Call<SummaryPackageResponse> call, Throwable t) {
//                callback.onError(t);
//            }
//        });
//    }
}

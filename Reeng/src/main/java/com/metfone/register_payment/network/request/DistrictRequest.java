package com.metfone.register_payment.network.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class DistrictRequest extends BaseRequest<DistrictRequest.Request> {

    public class Request {
        @SerializedName("language")
        String language;
        @SerializedName("provinceCode")
        String provinceCode;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getProvinceCode() {
            return provinceCode;
        }

        public void setProvinceCode(String provinceCode) {
            this.provinceCode = provinceCode;
        }
    }
}

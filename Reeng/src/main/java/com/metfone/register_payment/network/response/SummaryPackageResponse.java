package com.metfone.register_payment.network.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.register_payment.model.SummaryPackage;

import java.util.List;

public class SummaryPackageResponse {

    public String errorCode;
    public String errorMessage;

    @SerializedName("result")
    public Response response;

    public static class Response {
        @SerializedName("wsResponse")
        public List<SummaryPackage> result;
    }
}

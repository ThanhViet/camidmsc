package com.metfone.register_payment.network.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class PackageInfoRequest extends BaseRequest<PackageInfoRequest.Request> {

    public class Request {
        @SerializedName("language")
        String language;
        @SerializedName("month")
        String month;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }
    }
}

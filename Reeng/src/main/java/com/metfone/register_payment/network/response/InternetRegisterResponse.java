package com.metfone.register_payment.network.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.register_payment.model.CamIdResponse;

public class InternetRegisterResponse {

    public String errorCode;
    public String errorMessage;
    @SerializedName("result")
    public Response response;

    public static class Response {
        @SerializedName("errorCode")
        public String errorCode;
        @SerializedName("message")
        public String message;
        @SerializedName("object")
        public String object;
        @SerializedName("userMsg")
        public String userMsg;
        @SerializedName("wsResponse")
        public CamIdResponse result;
    }
}

package com.metfone.register_payment.network.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class InternetRegisterRequest extends BaseRequest<InternetRegisterRequest.Request> {

    public class Request {
        @SerializedName("language")
        String language;
        @SerializedName("name")
        String name;
        @SerializedName("phone")
        String phone;
        @SerializedName("email")
        String email;
        @SerializedName("province")
        String province;
        @SerializedName("district")
        String district;
        @SerializedName("commune")
        String commune;
        @SerializedName("village")
        String village;
        @SerializedName("package")
        String packageName;
        @SerializedName("introductionCode")
        String introductionCode;
        @SerializedName("speed")
        String speed;
        @SerializedName("type")
        String type;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getCommune() {
            return commune;
        }

        public void setCommune(String commune) {
            this.commune = commune;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getIntroductionCode() {
            return introductionCode;
        }

        public void setIntroductionCode(String introductionCode) {
            this.introductionCode = introductionCode;
        }

        public String getSpeed() {
            return speed;
        }

        public void setSpeed(String speed) {
            this.speed = speed;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}

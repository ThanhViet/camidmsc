package com.metfone.register_payment.network.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class ProvinceRequest extends BaseRequest<ProvinceRequest.Request> {

    public class Request {
        @SerializedName("language")
        String language;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

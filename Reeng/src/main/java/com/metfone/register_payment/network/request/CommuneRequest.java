package com.metfone.register_payment.network.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class CommuneRequest extends BaseRequest<CommuneRequest.Request> {

    public class Request {
        @SerializedName("language")
        String language;
        @SerializedName("provinceCode")
        String provinceCode;
        @SerializedName("districtCode")
        String districtCode;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getProvinceCode() {
            return provinceCode;
        }

        public void setProvinceCode(String provinceCode) {
            this.provinceCode = provinceCode;
        }

        public String getDistrictCode() {
            return districtCode;
        }

        public void setDistrictCode(String districtCode) {
            this.districtCode = districtCode;
        }
    }
}

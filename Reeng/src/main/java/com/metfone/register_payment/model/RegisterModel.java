package com.metfone.register_payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterModel {

    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("phone")
    @Expose
    String phone;
    @SerializedName("email")
    @Expose
    String email;
    @SerializedName("province")
    @Expose
    String province;
    @SerializedName("district")
    @Expose
    String district;
    @SerializedName("commune")
    @Expose
    String commune;
    @SerializedName("package")
    @Expose
    String packageName;
    @SerializedName("introductionCode")
    @Expose
    String introductionCode;
    @SerializedName("speed")
    @Expose
    String speed;
    @SerializedName("type")
    @Expose
    String type;

    public RegisterModel(
            String name,
            String phone,
            String email,
            String province,
            String district,
            String commune,
            String packageName,
            String introductionCode,
            String speed,
            String type
    ) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.province = province;
        this.district = district;
        this.commune = commune;
        this.packageName = packageName;
        this.introductionCode = introductionCode;
        this.speed = speed;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getIntroductionCode() {
        return introductionCode;
    }

    public void setIntroductionCode(String introductionCode) {
        this.introductionCode = introductionCode;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

public class District {

    @SerializedName("code")
    String districtCode;
    @SerializedName("name")
    String districtName;

    public District(String districtCode, String districtName) {
        this.districtCode = districtCode;
        this.districtName = districtName;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
}

package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PackageInfo {

    @SerializedName("id")
    public int id;
    @SerializedName("payAdvance")
    public int payAdvance;
    @SerializedName("speed")
    public int speed;
    @SerializedName("price")
    public String price;
    @SerializedName("name")
    public String name;
    @SerializedName("installation")
    public String installation;
    @SerializedName("deposit")
    public String deposit;
    @SerializedName("modemWifi")
    public String modemWifi;
    @SerializedName("specialPromotion")
    public String specialPromotion;

    public PackageInfo() {
    }
}

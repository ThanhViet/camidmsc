package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

public class Province {

    @SerializedName("code")
    String provinceCode;
    @SerializedName("name")
    String provinceName;

    public Province(String provinceCode, String provinceName) {
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }
}

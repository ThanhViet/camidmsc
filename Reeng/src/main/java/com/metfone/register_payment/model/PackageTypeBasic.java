package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PackageTypeBasic {

    @SerializedName("id")
    public int id;
    @SerializedName("payAdvance")
    public int payAdvance;
    @SerializedName("speed")
    public int speed;
    @SerializedName("price")
    public String price;
    @SerializedName("name")
    public String name;
    @SerializedName("installation")
    public String installation;
    @SerializedName("deposit")
    public String deposit;
    @SerializedName("modemWifi")
    public String modemWifi;
    @SerializedName("specialPromotion")
    public String specialPromotion;

    public PackageTypeBasic(int id, int payAdvance, int speed, String price, String name, String installation, String deposit, String modemWifi, String specialPromotion) {
        this.id = id;
        this.payAdvance = payAdvance;
        this.speed = speed;
        this.price = price;
        this.name = name;
        this.installation = installation;
        this.deposit = deposit;
        this.modemWifi = modemWifi;
        this.specialPromotion = specialPromotion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPayAdvance() {
        return payAdvance;
    }

    public void setPayAdvance(int payAdvance) {
        this.payAdvance = payAdvance;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstallation() {
        return installation;
    }

    public void setInstallation(String installation) {
        this.installation = installation;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getModemWifi() {
        return modemWifi;
    }

    public void setModemWifi(String modemWifi) {
        this.modemWifi = modemWifi;
    }

    public String getSpecialPromotion() {
        return specialPromotion;
    }

    public void setSpecialPromotion(String specialPromotion) {
        this.specialPromotion = specialPromotion;
    }
}

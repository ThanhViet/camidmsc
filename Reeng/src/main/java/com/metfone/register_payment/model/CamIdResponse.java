package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

public class CamIdResponse {
    @SerializedName("requestId")
    public String requestId;
}

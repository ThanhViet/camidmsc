package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

public class PackageInfoSpeed {

    @SerializedName("speed")
    public int speed;

    public PackageInfoSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}

package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SummaryPackage {

    @SerializedName("packageMonth")
    public String packageMonth;
    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;

    public SummaryPackage() {
    }
}

package com.metfone.register_payment.model;

import com.google.gson.annotations.SerializedName;

public class Commune {

    @SerializedName("code")
    String communeCode;
    @SerializedName("name")
    String communeName;

    public Commune(String communeCode, String communeName) {
        this.communeCode = communeCode;
        this.communeName = communeName;
    }

    public String getCommuneCode() {
        return communeCode;
    }

    public void setCommuneCode(String communeCode) {
        this.communeCode = communeCode;
    }

    public String getCommuneName() {
        return communeName;
    }

    public void setCommuneName(String communeName) {
        this.communeName = communeName;
    }
}

package com.metfone.register_payment.ui.fragment.package_info;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.metfone.register_payment.adapter.SpeedTypeAdapter;
import com.metfone.register_payment.model.PackageInfo;
import com.metfone.register_payment.model.PackageInfoSpeed;
import com.metfone.register_payment.model.PackageTypeBasic;
import com.metfone.register_payment.model.PackageTypeBusiness;
import com.metfone.register_payment.model.PackageTypeFamily;
import com.metfone.register_payment.network.response.PackageInfoResponse;
import com.metfone.register_payment.service.RetrofitClient;
import com.metfone.register_payment.ui.activity.ActivityRegister;
import com.metfone.register_payment.ui.fragment.register.RegisterFragment;
import com.metfone.register_payment.ui.fragment.summary_package.SummaryPackageFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class PackageInfoFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = PackageInfoFragment.class.getSimpleName();
    private FragmentManager mFragmentManager;

    private ConstraintLayout clPackageInfo;
    private ImageView ivBack;
    private TextView tvPackageInfoTitle;
    private RadioButton btnTypeBasic;
    private RadioButton btnTypeFamily;
    private RadioButton btnTypeBusiness;
    private TextView tvSpeed;
    private Spinner spSpeedType;
    private ImageView ivIconDropdown;
    private TextView tvSpinnerTitle;
    private TextView tvMonthlyChange;
    private TextView tvInstallation;
    private TextView tvDeposit;
    private TextView tvModemWifi;
    private TextView btnBuyNow;
    private TextView tvNoteTile;
    private TextView tvNote;
    private String packageMonth;
    private String packageTitle;
    private int packageId = 0;
    private int packageSpeed = 0;
    private int mType = 0;
    private int mColorPayNone = R.color.color_pay_none;
    private int mColorPay3 = R.color.color_pay_3;
    private int mColorPay5 = R.color.color_pay_5;
    private int mColorPay10 = R.color.color_pay_10;
    private int mColorPaySelect3 = R.drawable.bg_select_pay_3;
    private int mColorPaySelect5 = R.drawable.bg_select_pay_5;
    private int mColorPaySelect10 = R.drawable.bg_select_pay_10;

    private ArrayList<PackageInfo> mArrayList;
    private ArrayList<PackageTypeBasic> basicArrayList;
    private ArrayList<PackageTypeFamily> familyArrayList;
    private ArrayList<PackageTypeBusiness> businessArrayList;
    private RetrofitClient mRetrofitClient;
    private ApplicationController mApplication;
    private ArrayList<PackageInfoSpeed> mSpeedTypeValue;
    private SpeedTypeAdapter mSpeedTypeAdapter;
    private int speedType;
    private ActivityRegister acticity;

    private ProgressBar pbLoading;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    private static final String PACKAGE_MONTH = "packageMonth";
    private static final String PACKAGE_TITLE = "packageTitle";
    private static final String PACKAGE_ID = "packageId";
    private static final String PACKAGE_SPEED = "packageSpeed";

    public static PackageInfoFragment newInstance(String packageMonth, String packageTitle) {
        PackageInfoFragment fragment = new PackageInfoFragment();
        Bundle args = new Bundle();
        args.putString(PACKAGE_MONTH, packageMonth);
        args.putString(PACKAGE_TITLE, packageTitle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_package_info, container, false);

        mFragmentManager = getParentFragmentManager();
        acticity = (ActivityRegister) getActivity();
        initView(view);

        if (getArguments() != null) {
            packageMonth = getArguments().getString(PACKAGE_MONTH);
            packageTitle = getArguments().getString(PACKAGE_TITLE);
        }
        setupViewType(packageMonth);

        mRetrofitClient = new RetrofitClient();
        getDefaultPackage();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        acticity = (ActivityRegister) context;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void initView(View view) {
        clPackageInfo = view.findViewById(R.id.clPackageInfo);
        ivBack = view.findViewById(R.id.ivBack);
        tvPackageInfoTitle = view.findViewById(R.id.tvPackageInfoTitle);
        btnTypeBasic = view.findViewById(R.id.btnTypeBasic);
        btnTypeFamily = view.findViewById(R.id.btnTypeFamily);
        btnTypeBusiness = view.findViewById(R.id.btnTypeBusiness);
        tvSpeed = view.findViewById(R.id.tvSpeed);
        spSpeedType = view.findViewById(R.id.spSpeedType);
        ivIconDropdown = view.findViewById(R.id.ivIconDropdown);
        tvSpinnerTitle = view.findViewById(R.id.tvSpinnerTitle);
        tvMonthlyChange = view.findViewById(R.id.tvMonthlyChange);
        tvInstallation = view.findViewById(R.id.tvInstallation);
        tvDeposit = view.findViewById(R.id.tvDeposit);
        tvModemWifi = view.findViewById(R.id.tvModemWifi);
        btnBuyNow = view.findViewById(R.id.btnBuyNow);
        tvNoteTile = view.findViewById(R.id.tvNoteTitle);
        tvNote = view.findViewById(R.id.tvNote);
        pbLoading = view.findViewById(R.id.pbLoading);
        pbLoading.setVisibility(View.VISIBLE);

        ivBack.setOnClickListener(this);
        btnBuyNow.setOnClickListener(this);
        btnTypeBasic.setOnClickListener(this);
        btnTypeFamily.setOnClickListener(this);
        btnTypeBusiness.setOnClickListener(this);
    }

    public void getDefaultPackage() {
        try {
            getPackageInfo(packageMonth, new ApiCallbackV2<List<PackageInfo>>() {
                @Override
                public void onSuccess(String msg, List<PackageInfo> result) throws JSONException {
                    Log.d(TAG, "onSuccess: " + result);
                    mArrayList = new ArrayList<>();
                    basicArrayList = new ArrayList<>();
                    familyArrayList = new ArrayList<>();
                    businessArrayList = new ArrayList<>();
                    mSpeedTypeValue = new ArrayList<>();
                    mArrayList.addAll(result);
                    for (int i = 0; i < mArrayList.size(); i++) {
                        int id = mArrayList.get(i).id;
                        int payAdvance = mArrayList.get(i).payAdvance;
                        int speed = mArrayList.get(i).speed;
                        String price = mArrayList.get(i).price;
                        String name = mArrayList.get(i).name;
                        Log.d(TAG, "onSuccess: name: " + name);
                        String installation = mArrayList.get(i).installation;
                        String deposit = mArrayList.get(i).deposit;
                        String modemWifi = mArrayList.get(i).modemWifi;
                        String specialPromotion = mArrayList.get(i).specialPromotion;
                        switch (name) {
                            case "Basic":
                                basicArrayList.add(new PackageTypeBasic(id, payAdvance, speed, price, name, installation, deposit, modemWifi, specialPromotion));
                                Log.d(TAG, "onSuccess: basic: " + basicArrayList.size());
                                break;
                            case "Family":
                                familyArrayList.add(new PackageTypeFamily(id, payAdvance, speed, price, name, installation, deposit, modemWifi, specialPromotion));
                                Log.d(TAG, "onSuccess: family: " + familyArrayList.size());
                                break;
                            case "Business":
                                businessArrayList.add(new PackageTypeBusiness(id, payAdvance, speed, price, name, installation, deposit, modemWifi, specialPromotion));
                                Log.d(TAG, "onSuccess: business: " + businessArrayList.size());
                                break;
                        }
                    }
                    if (!basicArrayList.isEmpty() && basicArrayList.get(0).speed != 0) {
                        Log.d(TAG, "onSuccess: " + basicArrayList.get(0).speed);
                        initSpinnerType("Basic");
                    }
                }

                @Override
                public void onError(String s) {
                    Log.d(TAG, "onError: " + s);
                    pbLoading.setVisibility(View.GONE);
                }

                @Override
                public void onComplete() {

                }
            });
        } catch (
                Exception e) {
            pbLoading.setVisibility(View.GONE);
            e.printStackTrace();
        }

    }

    public void setupViewType(String packageMonth) {
        if (packageMonth != null && packageTitle != null) {
            try {
                tvPackageInfoTitle.setText(packageTitle);
                mType = Integer.parseInt(packageMonth);
                Log.d("Phan Tien", "onCreateView: " + packageMonth + "\n" + packageTitle);
                if (mType == 3) {
                    setUi(mColorPay3, mColorPaySelect3, R.drawable.ic_arrow_right_3, R.drawable.ic_arrow_left_3);
                } else if (mType == 5) {
                    setUi(mColorPay5, mColorPaySelect5, R.drawable.ic_arrow_right_5, R.drawable.ic_arrow_left_5);
                } else if (mType == 10) {
                    setUi(mColorPay10, mColorPaySelect10, R.drawable.ic_arrow_right_10, R.drawable.ic_arrow_left_10);
                } else clPackageInfo.setBackgroundResource(mColorPayNone);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setUi(int colorPay, int colorSelect, int icon, int image) {
        acticity.changeStatusBar(getResources().getColor(colorPay));
        clPackageInfo.setBackgroundResource(colorPay);
        btnTypeBasic.setBackgroundResource(colorSelect);
        btnTypeFamily.setBackgroundResource(colorSelect);
        btnTypeBusiness.setBackgroundResource(colorSelect);
        tvPackageInfoTitle.setTextColor(getResources().getColor(colorPay));
        ivIconDropdown.setBackgroundResource(icon);
        btnBuyNow.setTextColor(getResources().getColor(colorPay));
        tvSpinnerTitle.setTextColor(getResources().getColor(colorPay));
        ivBack.setBackgroundResource(image);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTypeBasic: {
                initSpinnerType("Basic");
                break;
            }
            case R.id.btnTypeFamily: {
                initSpinnerType("Family");
                break;
            }
            case R.id.btnTypeBusiness: {
                initSpinnerType("Business");
                break;
            }
            case R.id.btnBuyNow: {
                mFragmentManager.beginTransaction()
                        .add(R.id.container, RegisterFragment.newInstance(packageMonth, packageTitle, packageId, packageSpeed))
                        .addToBackStack("PackageInfoFragment")
                        .commit();
                break;
            }
            case R.id.ivBack: {
                mFragmentManager.beginTransaction()
                        .replace(R.id.container, SummaryPackageFragment.newInstance())
                        .commit();
            }
        }
    }

    public void getPackageInfo(String month, ApiCallbackV2<List<PackageInfo>> apiCallback) {
        try {
            mRetrofitClient.wsGetPackageInfo(month, new MPApiCallback<PackageInfoResponse>() {
                @Override
                public void onResponse(retrofit2.Response<PackageInfoResponse> response) {
                    List<PackageInfo> packages = new ArrayList<>();
                    if (response.body() != null && response.body().response != null && response.body().response.result != null) {
                        packages = response.body().response.result;
                    }
                    if (apiCallback != null) {
                        try {
                            apiCallback.onSuccess("", packages);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        apiCallback.onComplete();
                    }
                    pbLoading.setVisibility(View.GONE);
                }

                @Override
                public void onError(Throwable error) {
                    pbLoading.setVisibility(View.GONE);
                    acticity.connectionError();
                    if (apiCallback != null) {
                        apiCallback.onError(mApplication.getString(R.string.e601_error_but_undefined));
                        apiCallback.onComplete();
                    } else error.printStackTrace();
                }
            });
        } catch (Exception e) {
            pbLoading.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    public void setContentView(int speed, String price, String installation, String deposit, String modemWifi, String note) {
        tvSpeed.setText(String.valueOf(speed));
        tvMonthlyChange.setText(price);
        tvInstallation.setText(installation);
        tvDeposit.setText(deposit);
        tvModemWifi.setText(modemWifi);
        if (note != null) {
            tvNote.setText(note);
            tvNoteTile.setVisibility(View.VISIBLE);
            tvNote.setVisibility(View.VISIBLE);
        } else {
            tvNoteTile.setVisibility(View.GONE);
            tvNote.setVisibility(View.GONE);
            tvNote.setText("");
        }
    }

    public void initSpinnerType(String type) {
        mSpeedTypeValue = new ArrayList<>();
        if (type == "Basic") {
            for (int i = 0; i < basicArrayList.size(); i++) {
                mSpeedTypeValue.add(new PackageInfoSpeed(basicArrayList.get(i).speed));
            }
        } else if (type == "Family") {
            for (int i = 0; i < familyArrayList.size(); i++) {
                mSpeedTypeValue.add(new PackageInfoSpeed(familyArrayList.get(i).speed));
            }
        } else if (type == "Business") {
            for (int i = 0; i < businessArrayList.size(); i++) {
                mSpeedTypeValue.add(new PackageInfoSpeed(businessArrayList.get(i).speed));
            }
        }
        Log.d(TAG, "initSpinnerType: " + mSpeedTypeValue.size());
        mType = Integer.parseInt(packageMonth);
        mSpeedTypeAdapter = new SpeedTypeAdapter(getContext(), mSpeedTypeValue, mType);
        spSpeedType.setAdapter(mSpeedTypeAdapter);
        spSpeedType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PackageInfoSpeed infoSpeed = (PackageInfoSpeed) parent.getItemAtPosition(position);
                speedType = infoSpeed.getSpeed();
                switch (type) {
                    case "Basic":
                        packageId = basicArrayList.get(position).id;
                        packageSpeed = basicArrayList.get(position).speed;
                        setContentView(basicArrayList.get(position).speed, basicArrayList.get(position).price, basicArrayList.get(position).installation, basicArrayList.get(position).deposit, basicArrayList.get(position).modemWifi, basicArrayList.get(position).specialPromotion);
                        break;
                    case "Family":
                        packageId = familyArrayList.get(position).id;
                        packageSpeed = familyArrayList.get(position).speed;
                        setContentView(familyArrayList.get(position).speed, familyArrayList.get(position).price, familyArrayList.get(position).installation, familyArrayList.get(position).deposit, familyArrayList.get(position).modemWifi, familyArrayList.get(position).specialPromotion);
                        break;
                    case "Business":
                        packageId = businessArrayList.get(position).id;
                        packageSpeed = businessArrayList.get(position).speed;
                        setContentView(businessArrayList.get(position).speed, businessArrayList.get(position).price, businessArrayList.get(position).installation, businessArrayList.get(position).deposit, businessArrayList.get(position).modemWifi, businessArrayList.get(position).specialPromotion);
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
package com.metfone.register_payment.ui.fragment.mp_tab_internet;

import static com.metfone.selfcare.module.metfoneplus.fragment.MPRootFragment.statusLogin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.register_payment.ui.activity.ActivityRegister;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.fragment.history.HistoryFragment;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.addAccount.AddAccountFtthUserResponse;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.billpayment.MPBillPaymentFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddPhoneMetfoneFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPExchangeFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackUsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPSupportFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPTabMobileFragment;
import com.metfone.selfcare.module.metfoneplus.listener.OnClickShowFTTH;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Response;

public class MPTabInternetFragment extends MPBaseFragment implements View.OnClickListener {

    private static final String TAG = MPTabInternetFragment.class.getSimpleName();
    FrameLayout pbLoading;
    LinearLayout lnNotHaveFtth;
    LinearLayout lnFtth;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private FragmentManager mFragmentManager;
    private RadioButton btnSwitchMobile;
    private RadioButton btnSwitchInternet;
    private ConstraintLayout clAddAccount;
    private ConstraintLayout clRegisterService;
    private HomeActivity mParentActivity;
    private CamIdUserBusiness mCamIdUserBusiness;
    private ApplicationController mApplication;
    private ConstraintLayout clHistory, clRegister, clSupport, clPayment;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<AddAccountFtthUserResponse> items = new ArrayList<>();
    private MPTabInternetAdapter mpTabInternetAdapter;
    private RecyclerView rcInfor;
    private int serviceId = 2;

    public MPTabInternetFragment() {
        // Required empty public constructor
    }

    public static MPTabInternetFragment newInstance() {
        MPTabInternetFragment fragment = new MPTabInternetFragment();
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSystemBarTheme(getActivity(), true);
        mFragmentManager = getParentFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_m_p_tab_internet, container, false);
        initView(view);
        mFragmentManager = getParentFragmentManager();
        return view;
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    public void initView(View view) {
        clAddAccount = view.findViewById(R.id.clAddAccount);
        clRegisterService = view.findViewById(R.id.clRegisterService);
        btnSwitchMobile = view.findViewById(R.id.btnSwitchMobile);
        btnSwitchInternet = view.findViewById(R.id.btnSwitchInternet);
        pbLoading = view.findViewById(R.id.view_loading);
        lnFtth = view.findViewById(R.id.ln_have_ftth);
        lnNotHaveFtth = view.findViewById(R.id.ln_not_have_ftth);
        clHistory = view.findViewById(R.id.cl_history);
        clPayment = view.findViewById(R.id.cl_payment);
        clSupport = view.findViewById(R.id.cl_support);
        clRegister = view.findViewById(R.id.cl_register);
        rcInfor = view.findViewById(R.id.rc_number_infor);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        btnSwitchMobile.setOnClickListener(this::handleSwitchMobile);

        clAddAccount.setOnClickListener(this);
        clRegisterService.setOnClickListener(this);
        clHistory.setOnClickListener(v -> {
            mFragmentManager.beginTransaction()
                    .replace(R.id.root_frame, HistoryFragment.newInstance())
                    .addToBackStack(HistoryFragment.TAG)
                    .commit();
        });
        clRegister.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ActivityRegister.class);
            getActivity().startActivity(intent);
        });
        clPayment.setOnClickListener(v -> {
            replaceFragmentWithAnimationDefault(MPBillPaymentFragment.newInstance(items.get(0).getPhoneNumber()));
        });
        clSupport.setOnClickListener(v -> {
            mParentActivity.isFromInternet = true;
            mFragmentManager.beginTransaction()
                    .replace(R.id.root_frame, MPSupportFragment.newInstance())
                    .addToBackStack(MPSupportFragment.TAG)
                    .commit();
        });
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.red));
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (ApplicationController.self().isLogin() != FragmentTabHomeKh.LoginVia.NOT)
                getUserInformation(true);
        });
        if (ApplicationController.self().isLogin() != FragmentTabHomeKh.LoginVia.NOT)
            getUserInformation(false);
        rcInfor.setLayoutManager(new LinearLayoutManager(getContext()));
        mpTabInternetAdapter = new MPTabInternetAdapter(getContext(), items);
        rcInfor.setAdapter(mpTabInternetAdapter);

        lnFtth.setVisibility(View.GONE);
        lnNotHaveFtth.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static final void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void handleSwitchMobile(View view) {
        popBackStackFragment();
        if (statusLogin == 0) setSystemBarTheme(getActivity(), false);
        else setSystemBarTheme(getActivity(), true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clAddAccount: {
                if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
                    mParentActivity.displayRegisterScreenActivity(true);
                } else {
                    NavigateActivityHelper.navigateToAddAccountResultActivity(this, serviceId);
                }
                break;
            }
            case R.id.clRegisterService: {
//                if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
//                    mParentActivity.displayRegisterScreenActivity(true);
//                } else {
                    Intent intent = new Intent(getActivity(), ActivityRegister.class);
                    Objects.requireNonNull(getActivity()).startActivity(intent);
//                }
                break;
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        this.mParentActivity = (HomeActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mCamIdUserBusiness = mApplication.getCamIdUserBusiness();
        super.onAttach(context);
    }

    public void getUserInformation(boolean isRefresh) {
        if (!isRefresh) {
            pbLoading.setVisibility(View.VISIBLE);
        }
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.getFtthAccount(new ApiCallback<BaseResponse<ArrayList<AddAccountFtthUserResponse>>>() {
            @Override
            public void onResponse(Response<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> response) {
                pbLoading.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null) {
                    if (Integer.valueOf(response.body().getCode()) == 0) {
                        if (response.body().getData() == null || response.body().getData().size() == 0) {
                            lnFtth.setVisibility(View.GONE);
                            lnNotHaveFtth.setVisibility(View.VISIBLE);
                        } else {
                            lnNotHaveFtth.setVisibility(View.GONE);
                            lnFtth.setVisibility(View.VISIBLE);
                            items.clear();
                            items.addAll(response.body().getData());
                            mpTabInternetAdapter.notifyDataSetChanged();
                            String account = response.body().getData().get(0).getPhoneNumber();
                            mCamIdUserBusiness.setAccountFTTH(account);
                        }
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), error.getMessage());
                Log.e(TAG, "onError: ", error);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EnumUtils.MY_REQUEST_CODE) {
            getUserInformation(false);
        }
    }
}
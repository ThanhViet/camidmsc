package com.metfone.register_payment.ui.fragment.summary_package;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.ui.checkRequest.CheckRequestActivity;
import com.metfone.register_payment.adapter.SummaryPackageAdapter;
import com.metfone.register_payment.model.SummaryPackage;
import com.metfone.register_payment.network.response.SummaryPackageResponse;
import com.metfone.register_payment.service.RetrofitClient;
import com.metfone.register_payment.ui.activity.ActivityRegister;
import com.metfone.register_payment.ui.fragment.package_info.PackageInfoFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class SummaryPackageFragment extends Fragment {

    private static final String TAG = SummaryPackageFragment.class.getSimpleName();
    private FragmentManager mFragmentManager;
    private ImageView ivBack;
    private ProgressBar pbLoading;
    private RecyclerView recyclerView;
    private SummaryPackageAdapter adapter;
    private ArrayList<SummaryPackage> arrayList;
    private RetrofitClient retrofitClient;
    private ApplicationController application;
    private ActivityRegister activity;
    private ConstraintLayout clCheckRequest;

    public static SummaryPackageFragment newInstance() {
        SummaryPackageFragment fragment = new SummaryPackageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_summary_package, container, false);
        initView(view);

        mFragmentManager = getParentFragmentManager();
        arrayList = new ArrayList<>();
        retrofitClient = new RetrofitClient();

        activity = (ActivityRegister) getActivity();
        activity.changeStatusBar(getResources().getColor(R.color.thunderbird));
        fetchData();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (ActivityRegister) context;
    }

    public void initView(View view) {
        recyclerView = view.findViewById(R.id.rvSummaryPackage);
        ivBack = view.findViewById(R.id.ivBack);
        clCheckRequest = view.findViewById(R.id.includeLostConnect);
        ivBack.setOnClickListener(this::handleBack);
        pbLoading = view.findViewById(R.id.pbLoading);
        pbLoading.setVisibility(View.VISIBLE);
        clCheckRequest.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), CheckRequestActivity.class));
        });
    }

    private void handleBack(View view) {
        getActivity().finish();
    }

    public void getSummaryPackage(ApiCallbackV2<List<SummaryPackage>> apiCallback) {
        try {
            retrofitClient.wsGetSummaryPackage(new MPApiCallback<SummaryPackageResponse>() {
                @Override
                public void onResponse(retrofit2.Response<SummaryPackageResponse> response) {
                    List<SummaryPackage> packages = new ArrayList<>();
                    if (response.body() != null && response.body().response != null && response.body().response.result != null) {
                        packages = response.body().response.result;
                    }
                    if (apiCallback != null) {
                        try {
                            apiCallback.onSuccess("", packages);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        apiCallback.onComplete();
                    }
                    pbLoading.setVisibility(View.GONE);
                }

                @Override
                public void onError(Throwable error) {
                    activity.connectionError();
                    if (apiCallback != null) {
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                        apiCallback.onComplete();
                    } else error.printStackTrace();
                    pbLoading.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void fetchData() {
        try {
            getSummaryPackage(new ApiCallbackV2<List<SummaryPackage>>() {
                @Override
                public void onSuccess(String msg, List<SummaryPackage> result) throws JSONException {
                    if (result != null) {
                        arrayList.addAll(result);
                        adapter = new SummaryPackageAdapter(getContext(), arrayList, item -> onItemClick(item));
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(recyclerView.getContext(),
                                DividerItemDecoration.VERTICAL);
                        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.line_divider);
                        horizontalDecoration.setDrawable(horizontalDivider);
                        recyclerView.addItemDecoration(horizontalDecoration);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setLayoutManager(layoutManager);
                    } else Log.e(TAG, "onSuccess: " + msg);
                }

                @Override
                public void onError(String s) {
                    pbLoading.setVisibility(View.GONE);
                    Log.e(TAG, "onError: " + s);
                }

                @Override
                public void onComplete() {

                }

            });

        } catch (Exception e) {
            pbLoading.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    public void onItemClick(SummaryPackage item) {
        mFragmentManager.beginTransaction()
                .replace(R.id.container, PackageInfoFragment.newInstance(item.packageMonth, item.title))
                .commit();
    }

    public void displayAlertDialog(String title, String content) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_register_fail, null);
        TextView tvDialogTitle = alertLayout.findViewById(R.id.tvDialogFailTitle);
        TextView tvDialogContent = alertLayout.findViewById(R.id.tvDialogFailContent);
        tvDialogTitle.setText(title);
        tvDialogContent.setText(content);
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(alertLayout);
        alert.setCancelable(false);
        AlertDialog dialog = alert.create();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnCancelListener(dialog1 -> {
            dialog1.dismiss();
            getActivity().finish();
        });
        dialog.show();
    }
}
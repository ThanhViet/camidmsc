package com.metfone.register_payment.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import com.metfone.register_payment.ui.fragment.summary_package.SummaryPackageFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;

public class ActivityRegister extends BaseSlidingFragmentActivity implements OnInternetChangedListener {

    private AlertDialog alertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        changeStatusBar(true);
        changeStatusBar(Color.parseColor("#C41627"));
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new SummaryPackageFragment())
                    .commitNow();
        }
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else {
            connected = false;
            showDialog("Lost connect", "Lost connection. back to previous page!");
        }

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void showDialog(String title, String messages) {
        alertDialog = new AlertDialog.Builder(ActivityRegister.this)
                .setTitle(title)
                .setMessage(messages)
                .setNegativeButton("Yes", (dialog, which) -> {
                    alertDialog.dismiss();
                    finish();
                })
                .show();
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(ActivityRegister.this)) {
            EventBus.getDefault().postSticky(new LoadInfoUser(LoadInfoUser.LoadType.LOADING));
            Log.d(TAG, "Lost internet");
        } else {
            EventBus.getDefault().post(new RequestRefreshInfo());
            Log.d(TAG, "Connecting internet");
        }
    }

    public void showError(String message) {
        showError(message, getString(R.string.error));
    }

    public void connectionError() {
        showError(getString(R.string.connection_error));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
    }

    public void showKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
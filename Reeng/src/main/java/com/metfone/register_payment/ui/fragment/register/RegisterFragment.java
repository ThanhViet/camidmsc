package com.metfone.register_payment.ui.fragment.register;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.metfone.register_payment.adapter.CommuneAdapter;
import com.metfone.register_payment.adapter.DistrictAdapter;
import com.metfone.register_payment.adapter.ProvinceAdapter;
import com.metfone.register_payment.model.CamIdResponse;
import com.metfone.register_payment.model.Commune;
import com.metfone.register_payment.model.District;
import com.metfone.register_payment.model.Province;
import com.metfone.register_payment.network.response.CommuneResponse;
import com.metfone.register_payment.network.response.DistrictResponse;
import com.metfone.register_payment.network.response.InternetRegisterResponse;
import com.metfone.register_payment.network.response.ProvinceResponse;
import com.metfone.register_payment.service.RetrofitClient;
import com.metfone.register_payment.ui.activity.ActivityRegister;
import com.metfone.register_payment.ui.fragment.summary_package.SummaryPackageFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class RegisterFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = RegisterFragment.class.getSimpleName();
    private FragmentManager mFragmentManager;
    private ImageView ivBack;
    private AppCompatEditText etFullName;
    private AppCompatEditText etPhoneNumber;
    private AppCompatEditText etEmail;
    private Spinner spProvince;
    private Spinner spDistrict;
    private Spinner spCommune;
    private AppCompatEditText etVillage;
    private CheckBox cbStaff;
    private CheckBox cbAgent;
    private AppCompatEditText etStaff;
    private TextView btnRegister;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private String name, phone, email, province, district, commune, village, type, introductionCode, packageMonth, packageTitle;
    private String provinceCode, districtCode, communeCode;
    private int packageId, packageSpeed;

    private ArrayList<Province> mProvinces;
    private ArrayList<District> mDistrict;
    private ArrayList<Commune> mCommune;
    private ProvinceAdapter mProvinceAdapter;
    private DistrictAdapter mDistrictAdapter;
    private CommuneAdapter mCommuneAdapter;
    private AlertDialog alertDialog;
    private ProgressBar pbLoading;

    private RetrofitClient retrofitClient;
    private ApplicationController application;
    private ActivityRegister acticity;

    private static final String PACKAGE_MONTH = "packageMonth";
    private static final String PACKAGE_TITLE = "packageTitle";
    private static final String PACKAGE_ID = "packageId";
    private static final String PACKAGE_SPEED = "packageSpeed";

    public static RegisterFragment newInstance(String packageMonth, String packageTitle, int packageId, int packageSpeed) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(PACKAGE_MONTH, packageMonth);
        args.putString(PACKAGE_TITLE, packageTitle);
        args.putInt(PACKAGE_ID, packageId);
        args.putInt(PACKAGE_SPEED, packageSpeed);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registers, container, false);

        mFragmentManager = getParentFragmentManager();
        initView(view);
        if (getArguments() != null) {
            packageMonth = getArguments().getString(PACKAGE_MONTH);
            packageTitle = getArguments().getString(PACKAGE_TITLE);
            packageId = getArguments().getInt(PACKAGE_ID);
            packageSpeed = getArguments().getInt(PACKAGE_SPEED);
        }
        retrofitClient = new RetrofitClient();
        fetchDataProvince();
        acticity = (ActivityRegister) getActivity();
        acticity.changeStatusBar(getResources().getColor(R.color.woodsmoke));

        return view;
    }

    public void initView(View view) {
        ivBack = view.findViewById(R.id.ivBack);
        etFullName = view.findViewById(R.id.etFullName);
        etEmail = view.findViewById(R.id.etEmail);
        etPhoneNumber = view.findViewById(R.id.etPhoneNumber);
        spProvince = view.findViewById(R.id.spRegisterProvince);
        spDistrict = view.findViewById(R.id.spRegisterDistrict);
        spCommune = view.findViewById(R.id.spRegisterCommune);
        etVillage = view.findViewById(R.id.etVillage);
        cbStaff = view.findViewById(R.id.cbStaff);
        cbAgent = view.findViewById(R.id.cbAgent);
        etStaff = view.findViewById(R.id.etStaff);
        btnRegister = view.findViewById(R.id.btnRegister);
        pbLoading = view.findViewById(R.id.pbLoading);
        pbLoading.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pbLoading.setVisibility(View.GONE);
            }
        }, 2500);

        ivBack.setOnClickListener(this);
        cbStaff.setOnClickListener(this);
        cbAgent.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        acticity = (ActivityRegister) context;
    }

    public void cbStaffOnClick() {
        cbStaff.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) cbAgent.setChecked(false);
            if (!cbStaff.isChecked()) cbStaff.setChecked(false);
        });

        cbAgent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) cbStaff.setChecked(false);
            if (!cbAgent.isChecked()) cbAgent.setChecked(false);
        });
        setVisibleView();
    }

    public void setVisibleView() {
        if (cbStaff.isChecked()) etStaff.setHint(getString(R.string.register_staff_et));
        else if (cbAgent.isChecked()) etStaff.setHint(getString(R.string.register_agent_et));
        if (!cbAgent.isChecked() && !cbStaff.isChecked()) etStaff.setVisibility(View.GONE);
        else etStaff.setVisibility(View.VISIBLE);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cbStaff:
            case R.id.cbAgent: {
                cbStaffOnClick();
                break;
            }
            case R.id.btnRegister: {
                acticity.hideSoftKeyboard();
                verificationData();
                break;
            }
            case R.id.ivBack: {
                Log.d("Phan Tien", "onClick: " + packageMonth);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack("PackageInfoFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    public void getListProvince(ApiCallbackV2<List<Province>> apiCallback) {
        try {
            retrofitClient.wsGetListProvince(new MPApiCallback<ProvinceResponse>() {
                @Override
                public void onResponse(retrofit2.Response<ProvinceResponse> response) {
                    List<Province> packages = new ArrayList<>();
                    if (response.body() != null && response.body().response != null && response.body().response.result != null) {
                        packages = response.body().response.result;
                        Log.d("Phan Tien", "onResponse: " + packages);
                    }
                    if (apiCallback != null) {
                        try {
                            apiCallback.onSuccess("", packages);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        apiCallback.onComplete();
                    }
                }

                @Override
                public void onError(Throwable error) {
                    pbLoading.setVisibility(View.GONE);
                    acticity.connectionError();
                    if (apiCallback != null) {
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                        apiCallback.onComplete();
                    } else error.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchDataProvince() {
        try {
            getListProvince(new ApiCallbackV2<List<Province>>() {
                @Override
                public void onSuccess(String msg, List<Province> result) throws JSONException {
                    mProvinces = new ArrayList<>();
                    mProvinces.add(new Province("0", getString(R.string.register_province_city)));
                    if (result != null) {
                        mProvinces.addAll(result);
                    }
                    setSpinnerProvince();
                }

                @Override
                public void onError(String s) {
                    pbLoading.setVisibility(View.GONE);
                    Log.d("fetchData", "onError: " + s);
                }

                @Override
                public void onComplete() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSpinnerProvince() {
        mProvinceAdapter = new ProvinceAdapter(getContext(), mProvinces);
        spProvince.setAdapter(mProvinceAdapter);
        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Province clickedItem = (Province) parent.getItemAtPosition(position);
                province = clickedItem.getProvinceName();
                provinceCode = clickedItem.getProvinceCode();
                if (provinceCode != null) {
                    fetchDataDistrict(provinceCode);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListDistrict(String provinceCode, ApiCallbackV2<List<District>> apiCallback) {
        try {
            retrofitClient.wsGetListDistrict(provinceCode, new MPApiCallback<DistrictResponse>() {
                @Override
                public void onResponse(retrofit2.Response<DistrictResponse> response) {
                    List<District> packages = new ArrayList<>();
                    if (response.body() != null && response.body().response != null && response.body().response.result != null) {
                        packages = response.body().response.result;
                        Log.d("Phan Tien", "onResponse: " + packages);
                    }
                    if (apiCallback != null) {
                        try {
                            apiCallback.onSuccess("", packages);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        apiCallback.onComplete();
                    }
                }

                @Override
                public void onError(Throwable error) {
                    acticity.connectionError();
                    if (apiCallback != null) {
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                        apiCallback.onComplete();
                    } else error.printStackTrace();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchDataDistrict(String provinceCode) {
        try {
            getListDistrict(provinceCode, new ApiCallbackV2<List<District>>() {
                @Override
                public void onSuccess(String msg, List<District> result) throws JSONException {
                    mDistrict = new ArrayList<>();
                    mDistrict.add(new District("0", getString(R.string.register_district)));
                    if (result != null) {
                        mDistrict.addAll(result);
                    }
                    setSpinnerDistrict();
                }

                @Override
                public void onError(String s) {
                    Log.d("fetchData", "onError: " + s);
                }

                @Override
                public void onComplete() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSpinnerDistrict() {
        mDistrictAdapter = new DistrictAdapter(getContext(), mDistrict);
        spDistrict.setAdapter(mDistrictAdapter);
        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                District clickedItem = (District) parent.getItemAtPosition(position);
                district = clickedItem.getDistrictName();
                districtCode = clickedItem.getDistrictCode();
                if (districtCode != null) {
                    fetchDataCommune(provinceCode, districtCode);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListCommune(String provinceCode, String districtCode, ApiCallbackV2<List<Commune>> apiCallback) {
        try {

            retrofitClient.wsGetListCommune(provinceCode, districtCode, new MPApiCallback<CommuneResponse>() {
                @Override
                public void onResponse(Response<CommuneResponse> response) {
                    List<Commune> packages = new ArrayList<>();
                    if (response.body() != null && response.body().response != null && response.body().response.result != null) {
                        packages = response.body().response.result;
                        Log.d("Phan Tien", "onResponse: " + packages);
                    }
                    if (apiCallback != null) {
                        try {
                            apiCallback.onSuccess("", packages);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        apiCallback.onComplete();
                    }
                }

                @Override
                public void onError(Throwable error) {
                    acticity.connectionError();
                    if (apiCallback != null) {
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                        apiCallback.onComplete();
                    } else error.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchDataCommune(String provinceCode, String districtCode) {
        try {
            getListCommune(provinceCode, districtCode, new ApiCallbackV2<List<Commune>>() {
                @Override
                public void onSuccess(String msg, List<Commune> result) throws JSONException {
                    mCommune = new ArrayList<>();
                    mCommune.add(new Commune("0", getString(R.string.register_commune)));
                    if (result != null) {
                        mCommune.addAll(result);
                    }
                    setSpinnerCommune();
                }

                @Override
                public void onError(String s) {

                }

                @Override
                public void onComplete() {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSpinnerCommune() {
        mCommuneAdapter = new CommuneAdapter(getContext(), mCommune);
        spCommune.setAdapter(mCommuneAdapter);
        spCommune.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Commune clickedItem = (Commune) parent.getItemAtPosition(position);
                commune = clickedItem.getCommuneName();
                communeCode = clickedItem.getCommuneCode();
                Log.d("Phan Tien", "onItemSelected: " + communeCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void verificationData() {
        try {
            if (etFullName.getText().toString().length() != 0) {
                name = etFullName.getText().toString();
                if (etPhoneNumber.getText().toString().length() != 0) {
                    phone = etPhoneNumber.getText().toString();
                    if (etEmail.getText().toString().length() == 0 || (etEmail.getText().toString().length() != 0 && etEmail.getText().toString().trim().matches(emailPattern))) {
                        email = etEmail.getText().toString();
                        if (!provinceCode.equals("0")) {
                            if (!districtCode.equals("0")) {
                                if (!communeCode.equals("0")) {
                                    if (etVillage.getText().toString().length() != 0) {
                                        village = etVillage.getText().toString();
                                        checkValueStaff();
                                    } else {
                                        etVillage.setError(getString(R.string.register_required_village));
                                        etVillage.requestFocus();
                                        acticity.showKeyboard();
                                    }
                                } else
                                    showDialog(getString(R.string.register_required_commune), getString(R.string.register_required_commune_msg));
                            } else
                                showDialog(getString(R.string.register_required_district), getString(R.string.register_required_district_msg));
                        } else
                            showDialog(getString(R.string.register_required_province), getString(R.string.register_required_province_msg));
                    } else {
                        etEmail.setError(getString(R.string.register_required_email));
                        etEmail.requestFocus();
                        acticity.showKeyboard();
                    }
                } else {
                    etPhoneNumber.setError(getString(R.string.register_required_phone));
                    etPhoneNumber.requestFocus();
                    acticity.showKeyboard();
                }
            } else {
                etFullName.setError(getString(R.string.register_required_name));
                etFullName.requestFocus();
                acticity.showKeyboard();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDialog(String title, String content) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_register_verify, null);
        TextView tvDialogTitle = alertLayout.findViewById(R.id.tvDialogTitle);
        TextView tvDialogContent = alertLayout.findViewById(R.id.tvDialogContent);
        TextView btnDialog = alertLayout.findViewById(R.id.btnDialogVerify);
        tvDialogTitle.setText(title);
        tvDialogContent.setText(content);
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(alertLayout);
        alert.setCancelable(false);
        AlertDialog dialog = alert.create();
        dialog.setCanceledOnTouchOutside(true);
        btnDialog.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    public void checkValueStaff() {
        if (cbStaff.isChecked() || cbAgent.isChecked()) {
            if (cbStaff.isChecked()) type = "staff";
            if (cbAgent.isChecked()) type = "agent";
            if (etStaff.getText() != null) {
                introductionCode = String.valueOf(etStaff.getText());
            } else etStaff.setError(getString(R.string.register_required_staff_agent));
        } else {
            type = "";
            introductionCode = "";
        }
        fetchData();
    }

    public void fetchData() {
        pbLoading.setVisibility(View.VISIBLE);
        try {
            retrofitClient.wsCamIdInternetRegister(
                    name,
                    phone,
                    email,
                    provinceCode,
                    districtCode,
                    communeCode,
                    village,
                    String.valueOf(packageId),
                    introductionCode,
                    String.valueOf(packageSpeed),
                    type, new MPApiCallback<InternetRegisterResponse>() {
                        @Override
                        public void onResponse(retrofit2.Response<InternetRegisterResponse> response) {
                            if (response.body() != null) {
                                InternetRegisterResponse.Response internetRegisterResponse = response.body().response;
                                if (response.body().response.result != null) {
                                    CamIdResponse camIdResponse = new CamIdResponse();
                                    camIdResponse.requestId = response.body().response.result.requestId;
                                    if (response.body().response.message != null) {
                                        displayAlertDialog(response.body().response.userMsg, response.body().response.message);
                                    }
                                } else {
                                    String error = internetRegisterResponse.errorCode;
                                    String message = internetRegisterResponse.message;
//                                    showDialog("CODE: " + error, "MESSAGE: " + message);
                                    displayAlertDialogFail(error, message);
                                }
                            }
                            pbLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Throwable error) {
                            acticity.connectionError();
                            pbLoading.setVisibility(View.GONE);
                            error.printStackTrace();
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void displayAlertDialog(String title, String content) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_register_success, null);
        TextView tvDialogTitle = alertLayout.findViewById(R.id.tvDialogSuccessTitle);
        TextView tvDialogContent = alertLayout.findViewById(R.id.tvDialogSuccessContent);
        tvDialogTitle.setText(title);
        tvDialogContent.setText(content);
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(alertLayout);
        alert.setCancelable(false);
        AlertDialog dialog = alert.create();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnCancelListener(dialog1 -> {
            dialog1.dismiss();
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, SummaryPackageFragment.newInstance())
                    .commit();
        });
        dialog.show();
    }

    public void displayAlertDialogFail(String title, String content) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_register_fail, null);
        TextView tvDialogTitle = alertLayout.findViewById(R.id.tvDialogFailTitle);
        TextView tvDialogContent = alertLayout.findViewById(R.id.tvDialogFailContent);
        tvDialogTitle.setText(title);
        tvDialogContent.setText(content);
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(alertLayout);
        alert.setCancelable(false);
        AlertDialog dialog = alert.create();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnCancelListener(dialog1 -> {
            dialog.dismiss();
        });
        dialog.show();
    }
}
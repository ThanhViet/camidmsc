package com.metfone.register_payment.ui.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.metfone.selfcare.R;

public class ActivityAddAccountInternet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_account_internet);
    }
}
package com.metfone.register_payment.ui.fragment.mp_tab_internet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.addAccount.AddAccountFtthUserResponse;

import java.util.ArrayList;
import java.util.List;

class MPTabInternetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private List<AddAccountFtthUserResponse> itemList;
    private Context mContext;
    private final int TYPE_ITEM = 0;

    @Override
    public void onClick(View v) {

    }

    public MPTabInternetAdapter(Context context, ArrayList<AddAccountFtthUserResponse> itemList) {
        mContext = context;
        this.itemList = itemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.item_metfone_home_infor, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == TYPE_ITEM) {
            ((ItemHolder) holder).bindData(itemList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }


    public interface OnItemClickListener {
        void onItemClick(int position);

        void onExpandClick(int position);
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        private TextView tvStatus, tvName, tvAccount, tvBlockingDate, tvDebit, tvPoint, tvAccountName;
        private ConstraintLayout container;
        private ImageView ivStatus;

        public ItemHolder(View view) {
            super(view);
            tvStatus = view.findViewById(R.id.tv_status);
            tvName = view.findViewById(R.id.tv_name);
            tvAccount = view.findViewById(R.id.tv_account);
            tvBlockingDate = view.findViewById(R.id.tv_blocking_date);
            tvDebit = view.findViewById(R.id.tv_debit);
            tvPoint = view.findViewById(R.id.tv_contact_point);
            tvAccountName = view.findViewById(R.id.tv_name_ftth);
            container = view.findViewById(R.id.item_metfone_container);
            ivStatus = view.findViewById(R.id.iv_status);
        }

        public void bindData(final AddAccountFtthUserResponse response) {
            if (!response.getPackageMonth().isEmpty()) {
                int month = Integer.parseInt(response.getPackageMonth());
                if (month == 5) {
                    container.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_metfone_home_5month_gradient));
                } else if (month == 10) {
                    container.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_metfone_home_10month_gradient));
                } else {
                    container.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_metfone_home_3month_gradient));
                }
            } else {
                container.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_metfone_home_3month_gradient));
            }
            if (response.getLevel().equalsIgnoreCase("Normal")) {
                ivStatus.setBackground(ContextCompat.getDrawable(mContext,R.drawable.ic_circle_green));
            } else {
                ivStatus.setBackground(ContextCompat.getDrawable(mContext,R.drawable.ic_circle_grey));
            }
            tvStatus.setText(response.getLevel());
            tvName.setText(response.getCustomerName());
            tvAccount.setText(response.getPhoneNumber());
            tvBlockingDate.setText(response.getBlockDate());
            tvDebit.setText(response.getDebit());
            tvPoint.setText(response.getContractPoint());
            tvAccountName.setText(response.getFtthName());
        }

    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
